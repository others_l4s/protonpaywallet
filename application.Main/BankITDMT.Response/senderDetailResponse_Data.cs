﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class senderDetailResponse_Data
    {
        public string customerId { get; set; }
        public string name { get; set; }
        public int kycstatus { get; set; }
        public double walletbal { get; set; }
        public string dateOfBirth { get; set; }
    }
}