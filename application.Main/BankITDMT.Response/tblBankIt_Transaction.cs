﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class tblBankIt_Transaction
    {
        public long BankItTransactionId { get; set; }
        //public string CustomerId { get; set; }
        //public string TransferType { get; set; }
        //public string RecipientId { get; set; }

        //public string RecipientName { get; set; }

        //public string BankName { get; set; }
        //public string ClientRefId { get; set; }
        //public string TxnId { get; set; }
        //public string RequestApi { get; set; }
        //public string ResponseApi { get; set; }
        //public string Status { get; set; }
        //public System.DateTime CreatedDateTime { get; set; }
        //public long EntityId { get; set; }
        //public long EntityUserId { get; set; }

        //public decimal Amount { get; set; }

        //public string ParentClientRefId { get; set; }
        public string TransferType { get; set; }
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string BankName { get; set; }
        public string TxnId { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string AgencyName { get; set; }
        public string Telephone1 { get; set; }
        public string CustomerID { get; set; }
        public string ParentClientRefId { get; set; }
        public long EntityId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public decimal Amount { get; set; }
        public long EntityUserId { get; set; }
        public string ResponseApi { get; set; }
        public string RequestApi { get; set; }
        public string ClientRefId { get; set; }
        public string accountNumber { get; set; }
        public Nullable<bool> FromMobileDevice { get; set; }

        public string TransactionStatus { get; set; }
    }
}