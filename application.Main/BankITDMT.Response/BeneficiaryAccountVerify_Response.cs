﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.BankITDMT.Request;

namespace application.Main.BankITDMT.Response
{
    public class BeneficiaryAccountVerify_Response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public BeneficiaryAccountVerify_Request data { get; set; }
        public string Reason { get; set; }
    }
}