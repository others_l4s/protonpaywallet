﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class BankList
    {
        public string bankCode { get; set; }
        public string bankName { get; set; }
        public string channelsSupported { get; set; }
        public string accVerAvailabe { get; set; }
        public string ifsc { get; set; }
        public string ifscStatus { get; set; }
    }
}