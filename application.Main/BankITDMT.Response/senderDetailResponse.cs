﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class senderDetailResponse
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public senderDetailResponse_Data data { get; set; }
    }
}