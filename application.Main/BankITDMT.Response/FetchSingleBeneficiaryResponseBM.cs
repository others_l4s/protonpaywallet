﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Web;
using application.Main.BankITDMT.Request;
using application.Main.Models;

namespace application.Main.BankITDMT.Response
{
    public class FetchSingleBeneficiaryResponseBM
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public FetchSingleBeneficiaryData data { get; set; }
        public DMT_BankItInDB_Sender objSender { get; set; }

        public MoneyTransferRequest objRemit { get; set; }

        public List<DMT_TransactionType> objTransferType { get; set; }

        public decimal commission { get; set; } = 0;
        public string transferType { get; set; }
    }
}