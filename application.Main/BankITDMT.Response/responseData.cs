﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class responseData
    {
        public string customerId { get; set; }
        public string clientRefId { get; set; }
        public string txnId { get; set; }
        public string accountNumber { get; set; }
    }
}