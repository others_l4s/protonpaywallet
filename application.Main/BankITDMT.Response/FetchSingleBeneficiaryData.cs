﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class FetchSingleBeneficiaryData
    {
        public string recipientId { get; set; }
        public string udf2 { get; set; }
        public string udf1 { get; set; }
        public string recipientName { get; set; }
        public string bankName { get; set; }
        public string mobileNo { get; set; }
    }
}