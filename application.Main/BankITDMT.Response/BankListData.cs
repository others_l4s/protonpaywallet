﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class BankListData
    {
        public List<BankList> bankList { get; set; }
    }
}