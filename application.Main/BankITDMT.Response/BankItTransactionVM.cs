﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.BankITDMT.Response
{
    public class BankItTransactionVM:paginationProperties
    {
        public List<tblBankIt_Transaction> objBankITDMTTransactionsList { get; set; }
    }
}