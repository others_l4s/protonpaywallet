﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Response
{
    public class moneyTransferResponse
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public string Reason { get; set; }
        public responseData data { get; set; }
    }
}