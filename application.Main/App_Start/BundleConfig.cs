﻿using System.Web.Optimization;

namespace application.Main
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/vendor").Include(
                      "~/Content/vendors/js/vendors.min.js",
                      "~/Content/vendors/js/ui/jquery.sticky.js",
                        "~/Content/vendors/js/extensions/jquery.knob.min.js",
                      "~/Content/vendors/js/forms/icheck/icheck.min.js",
                      "~/Scripts/jquery-3.4.1.min.js",
                      "~/scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Content/js/core/app-menu.js",
                     "~/Content/js/core/app2.js",
                      "~/Content/js/toastr.js"));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include("~/Content/vendors/js/charts/chart.min.js",
                "~/Content/vendors/js/charts/chartist-plugin-tooltip.min.js",
                "~/Content/vendors/js/charts/raphael-min.js",
                "~/Content/vendors/js/charts/morris.min.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/vendors/css/material-vendors.min.css",
                      "~/Content/vendors/css/forms/icheck/icheck.css",
                      "~/Content/vendors/css/forms/icheck/custom.css",
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/bootstrap-extended1.css",
                      "~/Content/css/colors.css",
                      "~/Content/css/components.css",
                      "~/Content/css/core/menu/menu-types/horizontal-menu.css",
                      "~/Content/fonts/simple-line-icons/style.css",
                      "~/Content/css/core/colors/palette-gradient.css",
                      "~/Content/css/core/colors/palette-callout.css",
                      "~/Content/css/style.css",
                      "~/Content/css/plugins/extensions/toastr.css"));

            bundles.Add(new ScriptBundle("~/bundles/chartCSS").Include("~/Content/vendors/css/charts/jquery-jvectormap-2.0.3.css",
                      "~/Content/vendors/css/charts/morris.css",
                      "~/Content/vendors/css/charts/chartist.css",
                      "~/Content/vendors/css/charts/chartist-plugin-tooltip.css"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include("~/Content/vendors/js/tables/datatable/datatables.min.js"));
            //"~/Content/js/scripts/tables/datatables/datatable-basic.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatablescss").Include("~/Content/vendors/css/tables/datatable/datatables.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
        }
    }
}
