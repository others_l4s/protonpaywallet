﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace application.Main
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "index", id = UrlParameter.Optional }
            );
            //routes.MapRoute(name: "index", url: "DMR/index", defaults: new { controller = "bankITDMT", action = "index", id = UrlParameter.Optional });
            //routes.MapRoute(name: "getSenderList", url: "DMR/getSenderList", defaults: new { controller = "bankITDMT", action = "index", id = UrlParameter.Optional });
            //routes.MapRoute(name: "balanceEnquiry", url: "DMR/balanceEnquiry", defaults: new { controller = "MoneyTransfer", action = "balanceEnquiry", id = UrlParameter.Optional });
            ////routes.MapRoute(name: "AddSender", url: "DMR/customer/create", defaults: new { controller = "MoneyTransfer", action = "addSender", id = UrlParameter.Optional });
            //routes.MapRoute(name: "fetchSender", url: "DMR/customer/fetch", defaults: new { controller = "MoneyTransfer", action = "fetchSender", id = UrlParameter.Optional });
            //routes.MapRoute(name: "Dashboard", url: "DMR/Dashboard", defaults: new { controller = "MoneyTransfer", action = "Dashboard", id = UrlParameter.Optional });
            //routes.MapRoute(name: "addBenecify", url: "DMR/Recipient/add", defaults: new { controller = "MoneyTransfer", action = "AddBeneficiary", id = UrlParameter.Optional });
            //routes.MapRoute(name: "Benecifyfetchall", url: "DMR/Recipient/fetchall", defaults: new { controller = "MoneyTransfer", action = "AddBeneficiary", id = UrlParameter.Optional });
            //routes.MapRoute(name: "Transact", url: "DMR/Transact", defaults: new { controller = "MoneyTransfer", action = "Transfer", id = UrlParameter.Optional });
            //routes.MapRoute(name: "Transactpendingtxnlist", url: "DMR/transact/pendingtxnlist", defaults: new { controller = "MoneyTransfer", action = "pendingTransaction", id = UrlParameter.Optional });
            //routes.MapRoute(name: "Transactsearchtxn", url: "DMR/transact/searchtxn", defaults: new { controller = "MoneyTransfer", action = "searchTransaction", id = UrlParameter.Optional });
            //routes.MapRoute(name: "getBeneficiaryList", url: "DMR/getBeneficiaryList", defaults: new { controller = "MoneyTransfer", action = "getBeneficiaryList", id = UrlParameter.Optional });
            //routes.MapRoute(name: "BeneficiaryDelete", url: "DMR/recipient/delete", defaults: new { controller = "MoneyTransfer", action = "deleteBeneficiary", id = UrlParameter.Optional });
        }
    }
}
