﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using application.Main.Models;
using Newtonsoft.Json;
using RestSharp;
using System.Net.Http;
using System.Web.Http;

namespace application.Main
{
    public partial class ajax : System.Web.UI.Page
    {
        [WebMethod]
        public static string addSender(string CustomerId)
        {
            string webResponse;
            DMT_addSender_request objsender = new DMT_addSender_request();
            objsender.token = "12345";
            objsender.customerId = CustomerId;
            IRestResponse webAPIResponse = Helper.WebAPIBankIt_via_RestSharp("sendotp", Method.POST, objsender, objsender.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                DMT_addSender_response objsenderresponse = JsonConvert.DeserializeObject<DMT_addSender_response>(webAPIResponse.Content);
                if (objsenderresponse != null)
                {
                    if (objsenderresponse.errorCode == "00" && objsenderresponse.errorMsg.ToLower() == "success")
                    {
                        webResponse = "1|Success";

                    }
                    else
                    {
                        webResponse = "0|" + objsenderresponse.errorMsg.ToString();
                    }
                }else
                {
                    webResponse = "0|Error";
                }
            }
            else
            {
                webResponse = "0|Error";
            }           
            return webResponse;
        }
        [WebMethod]
        public static string Indo_Nepal_RemitterDetails(string MobileNo)
        {
            string webResponse;
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);           
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode == "TXN")
                    {
                        webResponse = "1|Success";
                    }
                    else
                    {
                        webResponse = "0|" + objremitterresponse.status.ToString();
                    }
                }
                else
                {
                    webResponse = "0|Error";
                }
            }
            else
            {
                webResponse = "0|Error";
            }
            return webResponse;
        }
    }
}