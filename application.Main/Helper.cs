﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using application.Main.ViewModels;
using application.Main.Models;
using System.Linq;
using RestSharp;
using System.Net.Mail;
using System.Net;
using System.Web.Security;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Security.Policy;
using System.Security.Cryptography;

namespace application.Main
{
    public static class Helper
    {
        public const string defaultActiveStatus = "Active";
        public const string recordSaved = "0|Record Added Successfully.";
        public const string recordUpdated = "0|Record Updated Successfully.";
        public const string recordAlreadyExists = "1|Record already exists or something went wrong.";
        public const string recordDeleted = "0|Record successfully deleted.";
        public const string responseStatus = "responseStatus";
        public const string regAlphaNumericOnly = "^[a-zA-Z0-9 ]*$";
        public const string regLoginNameOnly = "^[a-zA-Z0-9.]*$";
        public const string regPassword = @"^[a-zA-Z0-9.!@#]*$";
        public const string regAlphaNumericSpecialSymbols = @"^[a-zA-Z0-9.~!@#]*$";
        public const string regAlphabetOnly = "^[a-zA-Z ]*$";
        public const string regEmailId = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$";
        public const string regNumericOnly = "^[0-9]*$";
        public const string regDecimalOnly = @"\d+(\.\d{1,4})?";
        public const string regWebsiteAddress = @"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?";
        public const string regGSTNo = @"^([0]{1}[1-9]{1}|[1-2]{1}[0-9]{1}|[3]{1}[0-7]{1})([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$";
        public const string userAccessToken = "__userAccessToken";
        public const string regZeroOrOne = "^[0-1]*$";
        public const string apiErrorMsg = "Error at API End. Please contact technical support team.";
        public const string regDate = @"^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$";


        public static HttpClient WebAPIClient = new HttpClient();

        public enum entityTypes
        {
            company = 1,
            agent = 2,
            distributor = 3,
            masterDistributor = 4
        }

        public static string generateMD5HashString(string value2Hash)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value2Hash));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string getCompanyDomain()
        {
            string URL = HttpContext.Current.Request.ServerVariables["Server_Name"];
            URL = URL.ToLower().ToString();
            if (URL.Contains("www"))
            {
                int index = URL.IndexOf(".");
                index = index + 1;
                int length = URL.Length;
                URL = URL.Substring(index);
            }
            return URL;
        }

        public static DateTime getIndianTime()
        {
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime;
        }

        public static List<string> commissionOnList()
        {
            List<string> commissionOnList = new List<string>();
            commissionOnList.Add("Basic Amount");
            commissionOnList.Add("Net Amount");
            commissionOnList.Add("Service Charge Amount");
            return commissionOnList;
        }

        public static List<serviceChargeType> serviceChargeType()
        {
            List<serviceChargeType> objServiceChargeTypeList = new List<serviceChargeType>();
            objServiceChargeTypeList.Add(new serviceChargeType { TypeName = "Fix", TypeId = 1 });
            objServiceChargeTypeList.Add(new serviceChargeType { TypeName = "Per(%)", TypeId = 0 });
            return objServiceChargeTypeList;
        }

        public static List<utilityType> utilityType()
        {
            List<utilityType> objUtilityTypeList = new List<utilityType>();
            objUtilityTypeList.Add(new utilityType { ServiceType = "PREPAID" });
            objUtilityTypeList.Add(new utilityType { ServiceType = "POSTPAID" });
            objUtilityTypeList.Add(new utilityType { ServiceType = "DTH" });
            return objUtilityTypeList;
        }


        static Helper()
        {
            WebAPIClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["webAPIURL"]);
            WebAPIClient.DefaultRequestHeaders.Clear();
            WebAPIClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static commonStatusList getStatusList(IRestResponse response)
        {
            commonStatusList objStatusList = new commonStatusList();
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objStatusList.objStatusList = JsonConvert.DeserializeObject<List<tblStatus_Master>>(response.Content);
                if (objStatusList.objStatusList != null)
                    objStatusList.defaultActiveStatusId = objStatusList.objStatusList.Count > 0 ? Convert.ToInt32(objStatusList.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == defaultActiveStatus.ToLower()).StatusId) : 0;
                else
                    objStatusList.defaultActiveStatusId = 0;
            }
            return objStatusList;
        }

        public static IRestResponse WebAPIClient_via_RestSharp(string apiName, RestSharp.Method methodName, object objClass, string token = "")
        {
            string hostName = ConfigurationManager.AppSettings["webAPIURL"].Replace("http://", "").Replace("https://", "");
            var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"] + "api/" + apiName);
            var request = new RestRequest(methodName);
            request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("Connection", "keep-alive");
            //request.AddHeader("Accept-Encoding", "gzip, deflate");
            //request.AddHeader("Host", hostName.Replace("/", ""));
            //request.AddHeader("Cache-Control", "no-cache");
            //request.AddHeader("Accept", "*/*");
            if (!string.IsNullOrEmpty(token))
            {
                token = "Bearer " + token.Replace(@"\", "") + "";
                request.AddHeader("Authorization", token);
            }
            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse WebAPIBankIt_via_RestSharp(string apiName, RestSharp.Method methodName, object objClass, string token = "")
        {

            var client = new RestClient(ConfigurationManager.AppSettings["webAPIBankItURL"] + apiName);
            var request = new RestRequest(methodName);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", "application.main.bankit.dmt.pixlpower.com");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", " application/json");
            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse WebAPIBankIt_AEPS_via_RestSharp(object objClass)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["BankITAEPSURL"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(objClass);
            return client.Execute(request);
        }

        public static IRestResponse WebAPIINDONepal_via_RestSharp(string apiName, RestSharp.Method methodName, object objClass, string token = "")
        {

            var client = new RestClient(ConfigurationManager.AppSettings["webAPIIndoINepalURL"] + apiName);
            var request = new RestRequest(methodName);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            //request.AddHeader("Host", "instapaynepaldmt.pixlpower.com");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", " application/json");
            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse WebAPIInstantPayDMT_via_RestSharp(string apiName, RestSharp.Method methodName, object objClass, string token = "")
        {

            var client = new RestClient(ConfigurationManager.AppSettings["webAPIInstantPayDMTURL"] + apiName);
            var request = new RestRequest(methodName);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", "instapaydmt.pixlpower.com"); //localhost: 58962
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", " application/json");
            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse validateAdminLogin(tblAdminLogin objAdminLogin)
        {
            string headerHost = ConfigurationManager.AppSettings["webAPIURL"].Replace("/api/", "").Replace("http://", "");
            var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"].Replace("/api/", "") + "/validateadminlogin");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", headerHost);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("bodyParam", "username=" + objAdminLogin.LoginName + "&password=" + objAdminLogin.Password + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static void Send_Email_Custom_Sender(string sTo, string sHTMLBody, string sSubject, string sBCC, string sCC, string sFromEmailId)
        {
            try
            {
                MailMessage mailMsg = new MailMessage();
                mailMsg.Subject = sSubject;
                mailMsg.Body = sHTMLBody;
                mailMsg.To.Add(new MailAddress(sTo));
                if (sFromEmailId.Length == 0)
                    mailMsg.From = new MailAddress(Convert.ToString(ConfigurationManager.AppSettings["SMTP_FromEmail"]));
                else
                    mailMsg.From = new MailAddress(sFromEmailId);

                if (!String.IsNullOrEmpty(sCC))
                {
                    String[] arrCC = sCC.Split(';');
                    for (int i = 0; i < arrCC.Length; i++)
                        if (!String.IsNullOrEmpty(arrCC[i]))
                            mailMsg.CC.Add(new MailAddress(arrCC[i]));
                }

                if (!String.IsNullOrEmpty(sBCC))
                {
                    String[] arrBCC = sBCC.Split(';');
                    for (int i = 0; i < arrBCC.Length; i++)
                        if (!String.IsNullOrEmpty(arrBCC[i]))
                            mailMsg.Bcc.Add(new MailAddress(arrBCC[i]));
                }

                mailMsg.IsBodyHtml = true;
                mailMsg.Priority = MailPriority.High;

                SendEmail(mailMsg);
            }
            catch (Exception ex)
            {
                //tblSendMail_Statistics sendMail = new tblSendMail_Statistics();
                //sendMail.sTo = sTo;
                //sendMail.Subject = sSubject;
                //sendMail.HTMLBody = sHTMLBody;
                //sendMail.SMTPHost = ConfigurationManager.AppSettings["SMTP_Host"];
                //sendMail.SMTPPort = Convert.ToInt16(ConfigurationManager.AppSettings["SMTP_Port"]);
                //sendMail.SMTPEnableSSL = ConfigurationManager.AppSettings["SMTP_SSL"] == "true" ? true : false;
                //sendMail.SMTPUsername = ConfigurationManager.AppSettings["SMTP_Username"];
                //sendMail.SMTPPassword = ConfigurationManager.AppSettings["SMTP_Password"];
                //sendMail.iEmailSent = 0;
                //sendMail.Remark = ex.Message.ToString();
                //new SendMail_Statistics().SendMail_Statistics_Save(sendMail);
                throw new Exception(ex.Message.ToString());
            }
        }

        public static void SendEmail(Object mailMsg)
        {
            MailMessage mailMessage = (MailMessage)mailMsg;
            try
            {
                /* Setting should be kept somewhere so no need to 
                   pass as a parameter (might be in web.config)       */
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["SMTP_Host"], Convert.ToInt16(ConfigurationManager.AppSettings["SMTP_Port"]));
                NetworkCredential networkCredential = new NetworkCredential();
                networkCredential.UserName = Convert.ToString(ConfigurationManager.AppSettings["SMTP_Username"]);
                networkCredential.Password = Convert.ToString(ConfigurationManager.AppSettings["SMTP_Password"]);
                smtpClient.Credentials = networkCredential;

                //If you are using gmail account then
                smtpClient.EnableSsl = ConfigurationManager.AppSettings["SMTP_SSL"] == "true" ? true : false;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Send(mailMessage);
            }
            catch (SmtpException ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public static string protectString(string value2Protect)
        {
            byte[] byteString = Encoding.UTF8.GetBytes(value2Protect);
            byte[] protectedString = MachineKey.Protect(byteString, "ProtectString");
            return Convert.ToBase64String(protectedString);
        }

        public static string unProtectString(string value2UnProtect)
        {
            byte[] byteString = Convert.FromBase64String(value2UnProtect);
            byte[] protectedString = MachineKey.Unprotect(byteString, "ProtectString");
            return Encoding.UTF8.GetString(protectedString);
        }
        public static String RemoveWhiteSpace(string sString)
        {
            return Regex.Replace(sString, @"\s+", "");
        }
        public static String Format_Name(String _Name)
        {
            string sFormatedName = _Name.Replace(" ", "-").Replace(":", "").Replace("'", "-").Replace("&", "").Replace(".", "-").Replace("?", "").Replace("+", "");
            return sFormatedName.ToLower();
        }

        public static String Format_Image_Name(String _Name)
        {
            string sFormatedName = _Name.Replace(" ", "-").Replace(":", "").Replace("'", "-").Replace("&", "").Replace("?", "").Replace("+", "").Replace("#", "");
            return sFormatedName.ToLower();
        }

        public static bool doesCookieExist(string cookieName)
        {
            return HttpContext.Current.Request.Cookies[cookieName] == null ? false : true;
        }

        public static string getCookieValueIfItExists(string cookieName)
        {
            return doesCookieExist(cookieName) ? decryptAndRetrieveCookieValue(cookieName) : null;
        }

        // Creating Cookie by adding expiry of applicable.
        public static void createCookie(string cookieName, string cookieValue, bool addExpiry)
        {
            System.Web.HttpCookie cookie = new System.Web.HttpCookie(cookieName);
            cookie.Value = cookieValue;
            if (addExpiry)
            {
                cookie.Expires = System.DateTime.Today.AddDays(1);
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        // Creating Cookie by adding expiry of applicable.
        public static string createEncryptedCookie(string cookieName, string cookieValue, bool addExpiry)
        {
            System.Web.HttpCookie cookie = new System.Web.HttpCookie(cookieName);
            cookie.Value = protectString(cookieValue);
            if (addExpiry)
            {
                cookie.Expires = System.DateTime.Today.AddDays(1);
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie.Value;
        }

        public static string decryptAndRetrieveCookieValue(string cookieName)
        {
            if (doesCookieExist(cookieName))
                return unProtectString(System.Convert.ToString(HttpContext.Current.Request.Cookies[cookieName].Value));
            else
                return null;
        }

        public static List<tblExternal_Link_Target> bindExternalTargetTypeList()
        {
            List<tblExternal_Link_Target> objExternalTargetList = new List<tblExternal_Link_Target>();
            tblExternal_Link_Target objExternalTarget = new tblExternal_Link_Target();
            objExternalTarget.targetTypeName = "New Window";
            objExternalTarget.targetTypeValue = "_blank";
            objExternalTargetList.Add(objExternalTarget);
            objExternalTarget = new tblExternal_Link_Target();
            objExternalTarget.targetTypeName = "Self Window";
            objExternalTarget.targetTypeValue = "_self";
            objExternalTargetList.Add(objExternalTarget);
            return objExternalTargetList;
        }

        public static void clearCookie(string cookieName)
        {
            if (HttpContext.Current.Request.Cookies[cookieName] != null)
            {
                HttpContext.Current.Response.Cookies[cookieName].Value = "";
                HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddYears(-1);
            }
        }

        public static string GenerateRandomClientRefId()
        {
            string numbers = "12345678901234567890";
            string characters = numbers;
            int length = 9;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }
        public static string GenerateRandomClientRefId3()
        {
            string numbers = "12345678901234567890";
            string characters = numbers;
            int length = 2;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, characters.Length);
                    character = characters.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }
        public static String GetRandomPasswordUsingGUID(int length)
        {
            //Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();

            //Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);

            //Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
            {
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            }

            //Return the first length bytes
            return guidResult.Substring(0, length);
        }

        public static T Call_Rest_API<T>(string apiName, object objClass, Method method = Method.POST, string token = "") where T : class
        {
            string hostName = "";
            try
            {
                hostName = ConfigurationManager.AppSettings["webAPIURL"].Replace("http://", "").Replace("https://", "");
                var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"] + "api/" + apiName);
                var request = new RestRequest(method);
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", hostName.Replace("/", ""));
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                if (!string.IsNullOrEmpty(token))
                {
                    token = "Bearer " + token.Replace(@"\", "") + "";
                    request.AddHeader("Authorization", token);
                }
                if (method == Method.POST)
                    request.AddJsonBody(objClass);

                return (T)Convert.ChangeType(client.Execute<object>(request).Data, typeof(T));
            }
            catch (Exception ex)
            {
                Hendle_Error(ex, "Helper Class/Call_Rest_API Method/" + hostName);
                throw new Exception(ex.Message);
            }
        }

        public static async Task<T> Call_Rest_API_Ayncronously<T>(string apiName, object objClass, Method method = Method.POST, string token = "") where T : class
        {
            string hostName = "";
            try
            {
                hostName = ConfigurationManager.AppSettings["webAPIURL"].Replace("http://", "").Replace("https://", "");
                var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"] + "api/" + apiName);
                var request = new RestRequest(method);
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", hostName.Replace("/", ""));
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                if (!string.IsNullOrEmpty(token))
                {
                    token = "Bearer " + token.Replace(@"\", "") + "";
                    request.AddHeader("Authorization", token);
                }
                if (method == Method.POST)
                    request.AddJsonBody(objClass);

                var API_Execution_Task = Task.Run(() => client.Execute<object>(request).Data);

                return await (Task<T>)Convert.ChangeType(await API_Execution_Task, typeof(Task<T>));
            }
            catch (Exception ex)
            {
                Hendle_Error(ex, "Helper Class/Call_Rest_API_Ayncronously Method/" + hostName);
                throw new Exception(ex.Message);
            }
        }
        public static void Hendle_Error(Exception ex, string ExceptionNameAndPath)
        {
            // Add Logic to handle Exception 
        }

        public static string validateEntityMasterBeforeInsert(tblEntity_Master objEntityMaster)
        {
            customResponse objCustomResponse = new customResponse();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityMaster/validateentitydata", Method.POST, objEntityMaster);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "OK";
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return "1|" + objCustomResponse.Message;
            }
        }

        public static string validateEntityUserBeforeInsert(tblEntity_User objEntityUser)
        {
            customResponse objCustomResponse = new customResponse();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUser/validateentityuserdata", Method.POST, objEntityUser, null);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "OK";
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return "1|" + objCustomResponse.Message;
            }
        }

        public static webLoginVM createEntityLeadUser(tblEntity_User objEntityUser)
        {
            webLoginVM objWebLoginVM = new webLoginVM();
            customResponse objCustomResponse = new customResponse();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUser/createleaduser", Method.POST, objEntityUser);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(response.Content);
                objWebLoginVM.statusMsg = "success";
                objWebLoginVM.responseContent = response.Content;
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                objWebLoginVM.statusMsg = "1|" + objCustomResponse.Message;
                objWebLoginVM.OTPVerificationRequired = true;
            }
            return objWebLoginVM;
        }

        public static string createCompanyLeadUser(tblAdminLogin objAdminLogin, string accessToken)
        {
            customResponse objCustomResponse = new customResponse();
            if (objAdminLogin.AdminLoginId == 0)
            {
                objAdminLogin.Password = Helper.protectString(objAdminLogin.Password);
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins", Method.POST, objAdminLogin, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return "OK";
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return "1|" + objCustomResponse.Message;
                }
            }
            else
                return "OK";
        }

        public static Boolean hasEnoughWalletBalance(decimal amount, string accessToken)
        {
            IRestResponse response = WebAPIClient_via_RestSharp("checkWalletBalance?transactAmount=" + amount + "&checkBalanceFor=dmt", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<Boolean>(response.Content);
            }
            else
            {
                return false;
            }
        }

        public static List<DMT_TransactionType> TransferType()
        {
            List<DMT_TransactionType> objTransacationtypeList = new List<DMT_TransactionType>();
            objTransacationtypeList.Add(new DMT_TransactionType { TransType = "IMPS", TypeName = "IMPS" });
            objTransacationtypeList.Add(new DMT_TransactionType { TransType = "NEFT", TypeName = "NEFT" });
            return objTransacationtypeList;
        }


        public static List<DMT_TransactionType> TransferType_PayoutDirect()
        {
            List<DMT_TransactionType> objTransacationtypeList = new List<DMT_TransactionType>();
            objTransacationtypeList.Add(new DMT_TransactionType { TransType = "DPN", TypeName = "IMPS" });
            objTransacationtypeList.Add(new DMT_TransactionType { TransType = "BPN", TypeName = "NEFT" });
            objTransacationtypeList.Add(new DMT_TransactionType { TransType = "CPN", TypeName = "RTGS" });
            return objTransacationtypeList;
        }

        public static string bankITDMT_ClientRefId()
        {
            return "10084" + DateTime.Now.ToString("ddMMyyyyHHmmss"); //GetRandomPasswordUsingGUID(10).ToUpper();
        }

        public static string instantPayDMT_ClientRefId()
        {
            return "341Y78581" + DateTime.Now.ToString("ddMMyyyyHHmmssffff");
        }

        public static string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
    }
}