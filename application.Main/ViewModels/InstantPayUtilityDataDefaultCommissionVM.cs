﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class InstantPayUtilityDataDefaultCommissionVM
    {
        public List<tblInstantPay_UtilityData_Default_Commission> objDefaultCommission { get; set; }
    }
}