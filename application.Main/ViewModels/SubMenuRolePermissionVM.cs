﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class SubMenuRolePermissionVM
    {
        public List<tblCompany_Master> objCompanyList { get; set; }
        public List<tblSubMenu_Role_Permission> objMainMenuList { get; set; }
        public List<tblSubMenu_Role_Permission> objSubMenuList { get; set; }
        public tblSubMenu_Role_Permission objSubMenuRolePermissions { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }

        public int roleId { get; set; }
    }
}