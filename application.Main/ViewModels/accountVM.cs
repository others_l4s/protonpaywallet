﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class accountVM : paginationProperties
    {
        public List<tblEntity_Accounts> objEntityAccountList { get; set; }

        public double totalAmount { get; set; }
    }
}