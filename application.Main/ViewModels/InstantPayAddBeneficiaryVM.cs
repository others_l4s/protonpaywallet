﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
using application.Main.instantpay.dmt.response;
using application.Main.instantpay.dmt.request;

namespace application.Main.ViewModels
{
    public class InstantPayAddBeneficiaryVM
    {
        public benefical_register_req objBeneficiary { get; set; }

        public List<bank_details_data_resp> objBankList { get; set; }

        public List<DMT_TransactionType> objTransferType { get; set; }
    }
}