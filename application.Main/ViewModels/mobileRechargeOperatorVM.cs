﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileRechargeOperatorVM : paginationProperties
    {
        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }
        public List<tblMobileRecharge_Operator> objMobileRechargeOperatorList { get; set; }
        public tblMobileRecharge_Operator objMobileRechargeOperator { get; set; }

        public HttpPostedFileBase HPOperatorImage { get; set; }
    }
}