﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mainMenuMasterViewModel : paginationProperties
    {
        public List<tblMainMenu_Master> objMainMenuList { get; set; }
        public tblMainMenu_Master objMainMenu { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblExternal_Link_Target> objExternalLinkTarget { get; set; }
    }
}