﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class mdViewModel : entityMasterViewModel
    {
        public List<tblEntity_Master> objEntityMasterList { get; set; }

        public tblEntity_Master objEntityMaster { get; set; }

        public long MDParentEntityId { get; set; } = 0;

    }
}