﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class MenuEntityTypePermissionVM
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblMenu_EntityType_Permission> objMainMenuList { get; set; }
        public List<tblMenu_EntityType_Permission> objSubMenuList { get; set; }
        public tblMenu_EntityType_Permission objMenuMenuPermissions { get; set; }

        public List<tblMenu_EntityType_Permission> objLoopList { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }

        [Display(Name = "Select Type")]
        public int entityTypeId { get; set; }
    }
}