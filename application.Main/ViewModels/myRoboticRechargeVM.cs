﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class myRoboticRechargeVM
    {
        public List<tblMyRoboticRechargeUtilityData> objMyRoboticUtilityDataList { get; set; }
        public tblMyRobotic_Recharge_Transaction objMyRoboticRechargeTransaction { get; set; }

        public string Title { get; set; }
    }
}