﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace application.Main.ViewModels
{
    public class subscriptionProductViewModel:paginationProperties
    {
        public List<tblsubScriptionProduct_Master> objSubscriptionProductList { get; set; }
        public tblsubScriptionProduct_Master objSubscriptionProduct { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblSubscription_Master> objSubscriptionList { get; set; }
    }
}