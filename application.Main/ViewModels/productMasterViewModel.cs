﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class productMasterViewModel : paginationProperties
    {
        public List<tblProduct_Master> objProductList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public tblProduct_Master objProduct { get; set; }
        public List<string> objCommissionOn { get; set; }
        public List<serviceChargeType> objServiceChargeTypeList { get; set; }
    }
}