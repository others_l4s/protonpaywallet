﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class AnnouncementViewModel:paginationProperties
    {
        public List<tblAnnouncement_Master> objAnnouncementMasterList { get; set; }
        public tblAnnouncement_Master objAnnouncementMaster { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblCompany_Master> objCompanyMasterList { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}