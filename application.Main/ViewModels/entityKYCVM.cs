﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entityKYCVM : paginationProperties
    {
        public List<tblEntityKYC> objEntityKYCList { get; set; }
        public tblEntityKYC objEntityKYC { get; set; }

        [Required(ErrorMessage = "Please select PanCard Photo Side 1")]
        public HttpPostedFileBase HPPanCardPhoto { get; set; }

        [Required(ErrorMessage = "Please select PanCard Photo Side 2")]
        public HttpPostedFileBase HPPanCardPhoto2 { get; set; }
        public HttpPostedFileBase HPGSTNoPhoto { get; set; }
        public HttpPostedFileBase HPGSTNoPhoto2 { get; set; }
        public HttpPostedFileBase HPAadhaarCardPhoto { get; set; }
        public HttpPostedFileBase HPAadhaarCardPhoto2 { get; set; }
    }
}