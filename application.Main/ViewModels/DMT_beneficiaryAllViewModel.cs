﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class DMT_beneficiaryAllViewModel
    {
        public DMT_recipient_all_response objallrecipient { get; set; }
        public DMT_IMPS_remit_request IMPSRequest { get; set; }
    }
}