﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entitySlabCommissionVM : paginationProperties
    {
        public List<tblEntitySlabCommission> objEntitySlabcommissionList { get; set; }

        public List<tblEntitySlabCommission> objEntityDownlineSlabCommissionList { get; set; }
        public tblEntitySlabCommission objEntitySlabCommission { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblEntity_Master> objEntityList { get; set; }
        public int CommissionTypeId { get; set; }
    }
}