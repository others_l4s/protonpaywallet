﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class utilityPaymentVM : paginationProperties
    {
        public List<tblMobileRecharge_Vendor> objMobileRechargeVendorList { get; set; }
        public tblUtilityPaymentRequest objUtilityPaymentRequest { get; set; }
        public string Title { get; set; }
    }
}