﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
namespace application.Main.ViewModels
{
    public class NepalTransferViewModel
    {
        public List<Nepal_remitter_details_beneficiary_resp> objBeneficiryList { get; set; }
        public Nepal_transfer_request objtransfer { get; set; }
    }
}