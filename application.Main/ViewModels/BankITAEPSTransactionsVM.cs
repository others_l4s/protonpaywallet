﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class BankITAEPSTransactionsVM : paginationProperties
    {
        public List<BankIT_AEPS_MicroATM_Transactions> objBankITAEPSTransactionsList { get; set; }
    }
}