﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class subscriptionMasterViewModel : paginationProperties
    {
        public List<tblSubscription_Master> objSubscriptionList { get; set; }

        public tblSubscription_Master objSubscriptionMaster { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}