﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class subNavBarViewModel
    {
        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }
        public string MainMenuName { get; set; }
        public string ExternalURL { get; set; }
        public string ExternalURLTarget { get; set; }
        public string IconCSS { get; set; }
    }
}