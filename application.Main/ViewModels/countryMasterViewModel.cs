﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class countryMasterViewModel : paginationProperties
    {
        public List<tblCountry_Master> objCountryMasterList { get; set; }
        public tblCountry_Master objCountryMaster { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }    }
}