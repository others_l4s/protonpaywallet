﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class BankITSenderViewModel : paginationProperties
    {
        public List<DMT_BankItInDB_Sender> clsBankItSenderList { get; set; }
        public DMT_BankItInDB_Sender clsBankItSender { get; set; }
    }
}