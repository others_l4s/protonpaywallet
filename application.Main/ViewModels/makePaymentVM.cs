﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class makePaymentVM : paginationProperties
    {
        public tblEntity_Payments objEntityPayments { get; set; }

        public List<tblEntity_Bank_Details> objEntityBankList { get; set; }

        public List<tblCompany_Bank_Details> objCompanyBankList { get; set; }
        public List<tblPaymentMode_Master> objPaymentModeList { get; set; }

        public List<tblEntity_Payments> objEntityPaymentList { get; set; }
        public List<tblEntity_Payments> objEntityPaymentListHO { get; set; }

        [Display(Name = "Make Payment To :")]
        public string parentAgencyName { get; set; }

        [Required(ErrorMessage = "Please upload Payment Slip")]
        public HttpPostedFileBase ImageFile { get; set; }

        public string makePaymentURL { get; set; }
    }
}