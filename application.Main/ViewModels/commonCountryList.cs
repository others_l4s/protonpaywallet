﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class commonCountryList
    {
        public List<tblCountry_Master> objCountryList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public tblStatus_Master objStatusMaster { get; set; }
        public int defaultActiveCountryId { get; set; }
    }
}