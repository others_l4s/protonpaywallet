﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class entityUserMasterViewModel : paginationProperties
    {
        public List<tblEntity_User> objEntityUserList { get; set; }

        public tblEntity_User objEntityUser { get; set; }

        public List<tblRole_Master> objRoleList { get; set; }

        public List<tblCountry_Master> objCountryList { get; set; }

        public List<tblState_Master> objStateList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblEntity_Master> objEntityMasterList { get; set; }

        public List<tblDepartment_Master> objDepartmentList { get; set; }

        public List<tblDesignation_Master> objDesignationList { get; set; }

        public tblEntityKYC objEntityKYC { get; set; }
        public string StatusRemark { get; set; }

        public HttpPostedFileBase HPPanCardPhoto { get; set; }
        public HttpPostedFileBase HPAadhaarCardPhoto { get; set; }

        [Required(ErrorMessage = "Please enter Login Name")]
        [StringLength(100, ErrorMessage = "Please enter Login Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Login Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        [StringLength(15, ErrorMessage = "Please enter Password Minimum 6 Character", MinimumLength = 6)]
        [Display(Name = "Enter Password")]
        public string Password { get; set; }

        [Display(Name = "Account Verified")]
        public bool IsAccountVerified { get; set; } = false;

        [Display(Name = "Email Verified")]
        public bool IsEmailVerified { get; set; } = false;

        [Display(Name = "Login Name Verified")]
        public bool IsLoginNameVerified { get; set; } = false;
        public long EntityUserLoginId { get; set; } = 0;
    }
}