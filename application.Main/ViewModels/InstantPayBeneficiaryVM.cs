﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
using application.Main.instantpay.dmt.response;

namespace application.Main.ViewModels
{
    public class InstantPayBeneficiaryVM : paginationProperties
    {
        public remitter_resp objRemitterDetails { get; set; }
        public List<beneficiary_resp> objbeneficiaryList { get; set; }
    }
}