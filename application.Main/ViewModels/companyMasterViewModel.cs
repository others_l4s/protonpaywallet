﻿using System.Collections.Generic;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class companyMasterViewModel : paginationProperties
    {

        public List<tblCompany_Master> objCompanyList { get; set; }
        public tblAdminLogin objCompanyUser { get; set; }
        public tblCompany_Master objCompany { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblBankAccountType> objBankAccountTypeList { get; set; }

        public List<tblCompany_Type> objCompanyTypeList { get; set; }

        public HttpPostedFileBase ImageFile { get; set; }
    }
}