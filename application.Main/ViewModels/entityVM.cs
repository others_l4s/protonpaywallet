﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entityVM
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public tblEntity_Master objEntity { get; set; }

        public List<tblEntity_Master> objEntityList { get; set; }
    }
}