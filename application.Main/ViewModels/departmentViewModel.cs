﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class departmentViewModel:paginationProperties
    {
        public List<tblDepartment_Master> objDepartmentList { get; set; }
        public tblDepartment_Master objDepartment { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}