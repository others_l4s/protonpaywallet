﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class companySubscriptionViewModel : paginationProperties
    {
        public List<tblCompany_Subscription> objCompanySubscriptionList { get; set; }
        public tblCompany_Subscription objCompanySubscription { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblsubScriptionProduct_Master> objSubscriptionList { get; set; }
    }
}