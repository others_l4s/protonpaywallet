﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace application.Main.ViewModels
{
    public class helpLineViewModel:paginationProperties
    {
        public List<tblHelpLineNumber_Master> objHelpLineList { get; set; }
        public tblHelpLineNumber_Master objHelpLine { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
        public List<tblCompany_Master> objCompanyMasterList { get; set; }        
    }
}