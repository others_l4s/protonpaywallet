﻿using application.Main.Models;
using System.Collections.Generic;

namespace application.Main.ViewModels
{
    public class stateViewModel : paginationProperties
    {
        public List<tblCountry_Master> objCountryMasterList { get; set; }
        public List<tblState_Master> objStateMasterList { get; set; }
        public tblState_Master objStateMaster { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }

    }
}