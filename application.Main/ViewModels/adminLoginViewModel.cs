﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class adminLoginViewModel : paginationProperties
    {
        public List<tblAdminLogin> objAdminLoginList { get; set; }

        public tblAdminLogin objAdminLogin { get; set; }

        public List<tblDepartment_Master> objDepartmentList { get; set; }

        public List<tblDesignation_Master> objDesignationList { get; set; }

        public List<tblState_Master> objStateList { get; set; }

        public List<tblCountry_Master> objCountryList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }

        public List<tblRole_Master> objRoleList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblCompany_Master> objCompanyList { get; set; }
    }
}