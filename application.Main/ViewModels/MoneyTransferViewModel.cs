﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class MoneyTransferViewModel
    {
        public DMT_banklist_response banklistdetailsMaster { get; set; }
        public DMT_banklist_resp bankMaster { get; set; }
        public List<DMT_banklist_resp> banklistMaster { get; set; }

    }
}