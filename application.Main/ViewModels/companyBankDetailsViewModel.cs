﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class companyBankDetailsViewModel: paginationProperties
    {
        public List<tblCompany_Bank_Details> objCompanyBankDetailsList { get; set; }
        public tblCompany_Bank_Details objCompanyBankDetails { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblBankAccountType> objBankAccountTypeList { get; set; }
    }
}