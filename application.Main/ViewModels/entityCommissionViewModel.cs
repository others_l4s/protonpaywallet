﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class entityCommissionViewModel : paginationProperties
    {
        public List<tblEntityCommision_Master> objEntityCommisionMasterList { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }

        public tblEntityCommision_Master objEntityCommisionMaster { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }

        [Display(Name = "Select Product")]
        [Required(ErrorMessage = "Please select Product")]
        public int productId { get; set; }

        public int CommissionTypeId { get; set; }
    }
}