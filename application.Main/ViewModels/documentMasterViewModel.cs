﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
using application.Main.ViewModels;

namespace application.Main.ViewModels
{
    public class documentMasterViewModel : paginationProperties
    {
        public List<tblDocument_Master> objDocumentMasterList { get; set; }

        public tblDocument_Master objDocumentMaster { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}