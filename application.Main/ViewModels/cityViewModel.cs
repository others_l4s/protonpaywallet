﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class cityViewModel : paginationProperties
    {
        public List<tblCity_Master> objCityList { get; set; }
        public tblCity_Master objCity { get; set; }
        public List<tblState_Master> objStateList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
    }
}