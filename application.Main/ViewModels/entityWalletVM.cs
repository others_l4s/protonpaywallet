﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entityWalletVM : paginationProperties
    {
        public List<tblEntity_Wallet> objEntityWalletList { get; set; }
    }
}