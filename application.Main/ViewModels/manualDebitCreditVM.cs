﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class manualDebitCreditVM : paginationProperties
    {
        public List<tblManual_DebitCredit> objManualDebitCreditList { get; set; }
        public tblManual_DebitCredit objManualDebitCredit { get; set; }

        public List<tblEntity_Type> objEntityTypeList { get; set; }

        public List<tblEntity_Master> objEntityList { get; set; }

        public List<tblProduct_Master> objProductList { get; set; }
    }
}