﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entityDashboardVM
    {
        public decimal WalletBalance { get; set; }
        public List<tblEntity_Accounts> objEntityAccountList { get; set; }

        public tblEntityKYC objEntityKYC { get; set; }

        public outletKYCStatus objOutletKYCStatus { get; set; }
    }
}