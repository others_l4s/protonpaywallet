﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileOTPVM
    {
        public long entityUserId { get; set; }
        public string OTP { get; set; }
        public string mobileNo { get; set; }
        public string userName { get; set; }
    }
}