﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileRechargeCircleVM : paginationProperties
    {
        public List<tblMobileRecharge_Circle> objMobileRechargeCircleList { get; set; }
        public tblMobileRecharge_Circle objMobileRechargeCircle { get; set; }
    }
}