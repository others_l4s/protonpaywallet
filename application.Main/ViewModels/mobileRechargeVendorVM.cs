﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileRechargeVendorVM : paginationProperties
    {
        public List<tblMobileRecharge_Operator> objMobileRechargeOperatorList { get; set; }
        public List<tblMobileRecharge_Vendor> objMobileRechargeVendorList { get; set; }
        public tblMobileRecharge_Vendor objMobileRechargeVendor { get; set; }

        public string Title { get; set; }
    }
}