﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class editProfileKYCVM
    {
        public editProfileKYC objEditProfile { get; set; }
        public List<tblState_Master> objStateList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }
        public List<tblCountry_Master> objCountryList { get; set; }

        public HttpPostedFileBase HPPanCardPhoto { get; set; }
        public HttpPostedFileBase HPAadhaarCardPhoto { get; set; }
    }

    public class editProfileKYC
    {
        public long EntityUserId { get; set; }
        public long EntityId { get; set; }

        [Required(ErrorMessage = "Please enter valid Full Name")]
        [StringLength(100, ErrorMessage = "Please enter Full Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Full Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string PersonName { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No. (Login Id)")]
        [StringLength(10, ErrorMessage = "Please enter valid only 10 digit mobile no.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Please enter only 10 digit mobile no.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(20, ErrorMessage = "Please enter valid Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Password")]
        [RegularExpression(Helper.regAlphaNumericSpecialSymbols, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter EmailId")]
        [StringLength(100, ErrorMessage = "Please enter valid EmailId", MinimumLength = 6)]
        [Display(Name = "Enter Email Id")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No. (Login Id)")]
        [StringLength(10, ErrorMessage = "Please enter valid only 10 digit mobile no.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Please enter only 10 digit mobile no.")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Please enter Address 1")]
        [StringLength(500, ErrorMessage = "Please enter Address 1")]
        [Display(Name = "Enter Address 1")]
        public string Address1 { get; set; }

        [StringLength(500)]
        [Display(Name = "Enter Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "Select State")]
        [Required(ErrorMessage = "Please select State")]
        public long StateId { get; set; }

        [Display(Name = "Select City")]
        [Required(ErrorMessage = "Please select City")]
        public long CityId { get; set; }

        [Display(Name = "Select Country")]
        [Required(ErrorMessage = "Please select Country")]
        public long CountryId { get; set; }

        [Display(Name = "Enter PinCode")]
        public string PinCode { get; set; }
        public string DOB { get; set; }
        public DateTime CreatedDateTime { get; set; }

        [Required(ErrorMessage = "Please enter OTP")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string MobileOTP { get; set; }
        public string PanDocumentId { get; set; }

        [StringLength(15)]
        [Display(Name = "PAN No.")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Invalid PAN No. format.")]
        [Required(ErrorMessage = "Please enter Pan No.")]
        public string PanCardNo { get; set; }
        public string PanCardPhoto { get; set; }
        public string AadhaarCardPhoto { get; set; }
        public string AadhaarCardDocumentId { get; set; }

        [StringLength(12)]
        [Display(Name = "Aadhaar Card")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Invalid Aadhaar Card No.")]
        [Required(ErrorMessage = "Please enter Aadhaar Card No.")]
        public string AadhaarCardNo { get; set; }
        public bool IsKYCStatus { get; set; }
        public string KYCStatusRemark { get; set; }
        public bool FromMobileDevice { get; set; }
        public string OutletId { get; set; }
        public bool OutletMobileVerified { get; set; }
        public string OutletDocumentGetStatusResponse { get; set; }

        public bool OutletRegistrationMobileOTPSent { get; set; }

        public long EntityKYCId { get; set; }
    }
}