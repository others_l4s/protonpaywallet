﻿using application.Main.Models;
using System.Collections.Generic;

namespace application.Main.ViewModels
{
    public class entityTypeViewModel:paginationProperties
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public tblEntity_Type objEntityType { get; set; }
    }
}