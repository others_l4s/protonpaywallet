﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class BankITAEPSRequestQueueVM : paginationProperties
    {
        public List<BankITAEPSRequestQueue> objBankITAEPSRequestList { get; set; }
    }
}