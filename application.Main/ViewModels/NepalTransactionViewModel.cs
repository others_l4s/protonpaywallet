﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class NepalTransactionViewModel
    {
        public NepalTransactionView_Request objTransactionView {get;set;}
        public Nepal_remitter_details_response objRemitter { get; set; }
    }
}