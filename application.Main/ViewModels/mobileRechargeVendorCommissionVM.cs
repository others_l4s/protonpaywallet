﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileRechargeVendorCommissionVM
    {
        public List<string> objMobileRechargeDistinctVendorList { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblEntity_Master> objEntityList { get; set; }
        public List<tblMobileRecharge_VendorCommission> objMobileRechargeVendorCommissionList { get; set; }
        public IEnumerable<tblCommissionType> objCommissionTypeList { get; set; }

        [Display(Name = "Select Vendor")]
        [Required(ErrorMessage = "Please select Vendor")]
        public string VendorName { get; set; }

        [Display(Name = "Select Agency Type")]
        public Int32 EntityTypeId { get; set; }

        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }

        [Display(Name = "Select Operator Type")]
        public int OperatorTypeId { get; set; }

        [Display(Name = "Select Agency")]
        public long EntityId { get; set; }
    }
}