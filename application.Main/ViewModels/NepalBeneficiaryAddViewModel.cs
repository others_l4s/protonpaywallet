﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class NepalBeneficiaryAddViewModel
    {
        public Nepal_beneficiary_register_request objBeneficiaryMaster  { get; set; }
        public List<Nepal_bank_details_data_resp> objBankDetailsList { get; set; }
    }
}