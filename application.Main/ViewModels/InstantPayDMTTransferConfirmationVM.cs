﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.BankITDMT.Request;
using application.Main.instantpay.dmt.request;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class InstantPayDMTTransferConfirmationVM
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public benefical_register_req objBeneficiary { get; set; }
        public remitter_req objRemitter { get; set; }

        public MoneyTransferRequest objRemit { get; set; }

        public List<DMT_TransactionType> objTransferType { get; set; }

        public decimal commission { get; set; } = 0;

        public int remaininglimit { get; set; } = 0;
    }
}