﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class NepalremitterAddViewModel
    {
        public Nepal_remitter_request objRemitterRequest { get; set; }
        public List<Nepal_DocumentType> document_typeList = new List<Nepal_DocumentType>();
      
    }
}