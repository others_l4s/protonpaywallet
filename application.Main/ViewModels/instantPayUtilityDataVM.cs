﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class instantPayUtilityDataVM : paginationProperties
    {
        public List<tblInstantPay_UtilityData> objInstantPayUtilityDataList { get; set; }
        public tblInstantPay_UtilityData objInstantPayUtilityData { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }

        public List<utilityType> objUtilityTypeList { get; set; }
    }
}