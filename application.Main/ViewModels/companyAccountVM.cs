﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class companyAccountVM : paginationProperties
    {
        public List<tblCompany_Account> objCompanyAccountList { get; set; }
    }
}