﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class entityBankDetailsViewModel : paginationProperties
    {
        public List<tblEntity_Bank_Details> objEntityBankDetailsList { get; set; }
        public tblEntity_Bank_Details objEntityBankDetails { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblBankAccountType> objBankAccountTypeList { get; set; }
    }
}