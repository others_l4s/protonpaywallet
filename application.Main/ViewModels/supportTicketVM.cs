﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class supportTicketVM : paginationProperties
    {
        public List<tblSupportTicket> objSupportTicketList { get; set; }
        public tblSupportTicket objSupportTicket { get; set; }
        public List<tblSupportSystemProblem_Master> objSupportProblemList { get; set; }
        public List<tblSupportSystem_Details> objSupportTicketDetail { get; set; }

        public HttpPostedFileBase HPAttachment { get; set; }
    }
}