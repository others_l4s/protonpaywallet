﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mainNavBarViewModel
    {
        public int CompanyId { get; set; }
        public int MainMenuId { get; set; }
        public string MainMenuName { get; set; }
        public string ExternalURL { get; set; }
        public string ExternalURLTarget { get; set; }
        public string IconCSS { get; set; }
        public List<subNavBarViewModel> objSubNavBarList { get; set; }
    }
}