﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class commonStatusList
    {
        public List<tblStatus_Master> objStatusList { get; set; }
        public int defaultActiveStatusId { get; set; }
    }
}