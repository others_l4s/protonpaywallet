﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class rechargeVM : paginationProperties
    {
        public List<tblInstantPay_UtilityData> objInstantPayUtilityDataList { get; set; }
        public tblInstantPay_Recharge_Transaction objRechargeTransaction { get; set; }

        public string Title { get; set; }

        public List<tblInstantPay_Recharge_Transaction> objRechargeTransactionVM { get; set; }
    }
}