﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class InstantPayDMTTransactionsVM : paginationProperties
    {
        public List<tblInstantPay_DMT_Transactions> objInstantPayDMTTransactionsList { get; set; }
    }
}