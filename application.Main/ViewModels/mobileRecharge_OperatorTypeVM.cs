﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class mobileRecharge_OperatorTypeVM : paginationProperties
    {
        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }
        public tblMobileRecharge_OperatorType objMobileRechargeOperatorType { get; set; }
        public HttpPostedFileBase HPOperatorTypeImage { get; set; }
    }
}