﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.ViewModels;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class BankITRecipientViewModel : paginationProperties
    {
        public List<DMT_recipient_fetch_data_resp> clsRecipientResponseList { get; set; }
    }
}