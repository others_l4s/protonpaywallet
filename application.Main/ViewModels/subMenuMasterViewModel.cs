﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class subMenuMasterViewModel : paginationProperties
    {
        public List<tblSubMenu_Master> objSubMenuList { get; set; }
        public tblSubMenu_Master objSubMenu { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblExternal_Link_Target> objExternalLinkTarget { get; set; }

        public List<tblMainMenu_Master> objMainMenuList { get; set; }
       
    }
}