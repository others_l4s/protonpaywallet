﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class companyTypeMasterViewModel : paginationProperties
    {
        public List<tblCompany_Type> objComapanyTypeMasterList { get; set; }
        public tblCompany_Type objCompanyType { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}