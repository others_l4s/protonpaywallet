﻿using System.Collections.Generic;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class StatusMasterViewModel:paginationProperties
    {
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public tblStatus_Master objStatusMaster { get; set; }
    }
}