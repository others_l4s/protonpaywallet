﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class entityCommissionCustomViewModel :paginationProperties
    {
        public List<tblEntityCommissionCustom_Master> objEntityCommisionCustomMasterList { get; set; }
        public tblEntityCommissionCustom_Master objEntityCommisionCustomMaster { get; set; }
        public List<tblEntity_Master> objEntityMasterList { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblEntityCommissionDetails_Master> objEntityCommissionList { get; set; }
    }
}