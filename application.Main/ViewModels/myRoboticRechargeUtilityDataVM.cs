﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class myRoboticRechargeUtilityDataVM : paginationProperties
    {
        public List<tblMyRoboticRechargeUtilityData> objMyRoboticRechargeUtilityDataList { get; set; }
        public tblMyRoboticRechargeUtilityData objMyRoboticRechargeUtilityData { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }

        public List<utilityType> objUtilityTypeList { get; set; }
    }
}