﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
using application.Main.ViewModels;
namespace application.Main.ViewModels
{
    public class supportSystemProblemViewModel : paginationProperties
    {

        public List<tblSupportSystemProblem_Master> objSupportSystemProblemList { get; set; }
        public tblSupportSystemProblem_Master objSupportSystemProblem { get; set; }
        public List<tblDepartment_Master> objDepartmentList { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
    }
}