﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;

namespace application.Main.ViewModels
{
    public class SubMenuCompanyPermissionVM
    {
        public List<tblCompany_Master> objCompanyList { get; set; }
        public List<tblSubMenu_Company_Permission> objMainMenuList { get; set; }
        public tblSubMenu_Company_Permission objSubMenuCompanyPermissions { get; set; }
        public int CompanyId { get; set; }
        public int MainMenuId { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }

        public List<tblSubMenu_Company_Permission> objSubMenuList { get; set; }
    }
}