﻿using application.Main.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.ViewModels
{
    public class designationViewModel : paginationProperties
    {
        public List<tblDesignation_Master> objDesignationList { get; set; }
        public tblDesignation_Master objDesignation { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}