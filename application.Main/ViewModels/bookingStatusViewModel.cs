﻿using application.Main.Models;
using System.Collections.Generic;
namespace application.Main.ViewModels
{
    public class bookingStatusViewModel:paginationProperties
    {
       
        public List<tblBookingStatus_Master> objBookingStatusMasterList { get; set; }
        public tblBookingStatus_Master objBookingStatusMaster { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}