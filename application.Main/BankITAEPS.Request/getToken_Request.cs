﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITAEPS.Request
{
    public class getToken_Request
    {
        public string agentAuthId { get; set; }
        public string agentAuthPassword { get; set; }
        public string retailerId { get; set; }
        public string apiId { get; set; }
    }
}