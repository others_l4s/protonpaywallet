﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITAEPS.Response
{
    public class Get_Token_Data_resp
    {
        public string token { get; set; }
    }
}