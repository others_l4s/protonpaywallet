﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class StatusController : Controller
    {
        // GET: Status
        public ActionResult Index(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            tblStatus_Master objStatus = new tblStatus_Master();
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("status/" + id, Method.GET, null, accessToken);
                objStatus = JsonConvert.DeserializeObject<tblStatus_Master>(response1.Content);
            }
            var statusMasterViewModel = new StatusMasterViewModel
            {
                objStatusMaster = objStatus
            };
            return View(statusMasterViewModel);
        }

        [HttpPost]
        // [ValidateAntiForgeryToken]
        public JsonResult saveStatus(StatusMasterViewModel objStatusViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objStatusViewModel.objStatusMaster.StatusId == 0)
                {
                    if (!string.IsNullOrEmpty(objStatusViewModel.objStatusMaster.StatusName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("status", Method.POST, objStatusViewModel.objStatusMaster, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/status") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StatusName", "Please enter Status Name");
                        return Json(new { success = false, responseText = "1|Please enter Status Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("status/" + objStatusViewModel.objStatusMaster.StatusId, Method.PUT, objStatusViewModel.objStatusMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/status") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteStatus(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("status/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getStatusList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            StatusMasterViewModel statusList = new StatusMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("status?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                statusList = JsonConvert.DeserializeObject<StatusMasterViewModel>(response.Content);
            }
            return Json(new { data = statusList.objStatusMasterList, draw = Request["draw"], recordsTotal = statusList.totalRecords, recordsFiltered = statusList.filterRecords }, JsonRequestBehavior.AllowGet);
        }

    }
}