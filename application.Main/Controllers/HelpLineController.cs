﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class HelpLineController : Controller
    {
        public ActionResult Index(int Id = 0)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            helpLineViewModel objhelpLineViewModel = new helpLineViewModel();
            IRestResponse helplineResponse = Helper.WebAPIClient_via_RestSharp("Helpline", Method.GET, null, accessToken);
            objhelpLineViewModel = JsonConvert.DeserializeObject<helpLineViewModel>(helplineResponse.Content);

            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("Helpline/" + id, Method.GET, null, accessToken);
                objhelpLineViewModel.objHelpLine = JsonConvert.DeserializeObject<tblHelpLineNumber_Master>(response1.Content);
            }
            else
            {
                objhelpLineViewModel.objHelpLine.StatusId = objhelpLineViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
                objhelpLineViewModel.objHelpLine.HelplineId = 0;
            }
            var helpLineViewModel = new helpLineViewModel
            {
                objHelpLine = objhelpLineViewModel.objHelpLine,
                objHelpLineList = objhelpLineViewModel.objHelpLineList,
                objStatusList = objhelpLineViewModel.objStatusList,
                objProductList = objhelpLineViewModel.objProductList,
                objCompanyMasterList = objhelpLineViewModel.objCompanyMasterList
            };
            return View("index", helpLineViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveHelpLineNumber(helpLineViewModel objhelpLineViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objhelpLineViewModel.objHelpLine.HelplineId == 0)
                {
                    if (objhelpLineViewModel.objHelpLine.ProductId != 0 || objhelpLineViewModel.objHelpLine.CompanyId != 0)
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("Helpline", Method.POST, objhelpLineViewModel.objHelpLine, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return Json(new { success = true, redirectURL = Url.Content("~/helpline") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Please enter HelpLine Number" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("Helpline/" + objhelpLineViewModel.objHelpLine.HelplineId, Method.PUT, objhelpLineViewModel.objHelpLine, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/helpline") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteHelpLineNumber(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Helpline/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getHelpLineNumberList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            helpLineViewModel objhelpLineViewModel = new helpLineViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Helpline?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objhelpLineViewModel = JsonConvert.DeserializeObject<helpLineViewModel>(response.Content);
                return Json(new { data = objhelpLineViewModel.objHelpLineList, draw = Request["draw"], recordsTotal = objhelpLineViewModel.totalRecords, recordsFiltered = objhelpLineViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objhelpLineViewModel = new helpLineViewModel();
                return Json(new { data = objhelpLineViewModel.objHelpLineList, draw = Request["draw"], recordsTotal = objhelpLineViewModel.totalRecords, recordsFiltered = objhelpLineViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}