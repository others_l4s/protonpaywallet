﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.ViewModels;
using application.Main.Models;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class rechargeController : Controller
    {
        // GET: recharge
        public ActionResult Prepaid()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            rechargeVM objRechargeVM = new rechargeVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getRechargeServiceList?serviceType=PREPAID", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRechargeVM = JsonConvert.DeserializeObject<rechargeVM>(webAPIResponse.Content);
            }
            objRechargeVM.Title = "Prepaid Mobile Recharge";
            return View("index", objRechargeVM);
        }

        public ActionResult Postpaid()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            rechargeVM objRechargeVM = new rechargeVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getRechargeServiceList?serviceType=POSTPAID", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRechargeVM = JsonConvert.DeserializeObject<rechargeVM>(webAPIResponse.Content);
            }
            objRechargeVM.Title = "Postpaid Mobile Recharge";
            return View("index", objRechargeVM);
        }

        public ActionResult DTH()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            rechargeVM objRechargeVM = new rechargeVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getRechargeServiceList?serviceType=DTH", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRechargeVM = JsonConvert.DeserializeObject<rechargeVM>(webAPIResponse.Content);
            }
            objRechargeVM.Title = "DTH Recharge";
            return View("dthRecharge", objRechargeVM);
        }

        public ActionResult history()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            return View("rechargeHistory");
        }

        private Boolean hasAvailableBalance(double amount, string accessToken)
        {
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayRecharge/checkavailableBalance", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                double availableBalance = JsonConvert.DeserializeObject<double>(response.Content);
                if (availableBalance > amount)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transactRecharge(rechargeVM objRecharge)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objRecharge.objRechargeTransaction.Amount > 0)
                {
                    if (hasAvailableBalance(Convert.ToDouble(objRecharge.objRechargeTransaction.Amount), accessToken))
                    {
                        if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objRecharge.objRechargeTransaction.Amount), accessToken))
                        {
                            objRecharge.objRechargeTransaction.customermobile = objRecharge.objRechargeTransaction.MobileNo;
                            IRestResponse rechargeReponse = Helper.WebAPIClient_via_RestSharp("instantPayRecharge/rechargeMobile", Method.POST, objRecharge.objRechargeTransaction, accessToken);
                            if (rechargeReponse.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                TempData[Helper.responseStatus] = "Transaction has been successfully processed.";
                                return Json(new { success = true, responseText = "0|Recharge Successfully Done" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(rechargeReponse.Content);
                                return Json(new { success = true, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops! There is some issue with Recharge. Kindly contact administrator. ErrorCode:(balanceAvailibility)" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transactDTHRecharge(rechargeVM objRecharge)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objRecharge.objRechargeTransaction.Amount > 0)
                {
                    if (hasAvailableBalance(Convert.ToDouble(objRecharge.objRechargeTransaction.Amount), accessToken))
                    {
                        if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objRecharge.objRechargeTransaction.Amount), accessToken))
                        {
                            IRestResponse rechargeReponse = Helper.WebAPIClient_via_RestSharp("instantPayRecharge/rechargeMobile", Method.POST, objRecharge.objRechargeTransaction, accessToken);
                            if (rechargeReponse.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                TempData[Helper.responseStatus] = "Transaction has been successfully processed.";
                                return Json(new { success = true, responseText = "0|Recharge Successfully Done" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(rechargeReponse.Content);
                                return Json(new { success = true, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops! There is some issue with Recharge. Kindly contact administrator. ErrorCode:(balanceAvailibility)" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getRechargeHistory(string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            rechargeVM objRechargeTransactionVM = new rechargeVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayRecharge/rechargeHistory?searchEntityId=0&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRechargeTransactionVM = JsonConvert.DeserializeObject<rechargeVM>(webAPIResponse.Content);
                return Json(new { data = objRechargeTransactionVM.objRechargeTransactionVM, draw = Request["draw"], recordsTotal = objRechargeTransactionVM.totalRecords, recordsFiltered = objRechargeTransactionVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objRechargeTransactionVM = new rechargeVM();
                return Json(new { data = objRechargeTransactionVM.objRechargeTransactionVM, draw = Request["draw"], recordsTotal = objRechargeTransactionVM.totalRecords, recordsFiltered = objRechargeTransactionVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}