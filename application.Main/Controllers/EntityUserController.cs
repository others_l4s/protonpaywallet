﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;


namespace application.Main.Controllers
{
    public class EntityUserController : Controller
    {
        // GET: EntityUser
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });

        }
        public ActionResult addEdit(int id = 0, int EntityId = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });


            entityUserMasterViewModel objentityUserMasterViewModel = new entityUserMasterViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityUsers/" + id + "?entityId=" + EntityId, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objentityUserMasterViewModel = JsonConvert.DeserializeObject<entityUserMasterViewModel>(webAPIResponse.Content);
                if (id > 0)
                {
                    if (objentityUserMasterViewModel.Password != null)
                        objentityUserMasterViewModel.objEntityUser.Password = Helper.unProtectString(objentityUserMasterViewModel.Password);
                }
            }
            if (id == 0)
            {
                objentityUserMasterViewModel.objEntityUser.StatusId = (objentityUserMasterViewModel.objStatusList.Count() > 0 ? objentityUserMasterViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objentityUserMasterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveEntityUser(entityUserMasterViewModel objentityUserMasterViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objentityUserMasterViewModel.objEntityUser.EntityUserId == 0)
            {
                if (!string.IsNullOrEmpty(objentityUserMasterViewModel.objEntityUser.UserName))
                {

                    objentityUserMasterViewModel.objEntityUser.Password = Helper.protectString(objentityUserMasterViewModel.objEntityUser.Password);
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUsers", Method.POST, objentityUserMasterViewModel.objEntityUser, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/entityuser?entityId=" + Request["entityId"]) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("UserName", "Please enter User Name");
                    return Json(new { success = false, responseText = "1|Please enter User Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (objentityUserMasterViewModel.objEntityUser.EntityId > 0)
                {
                    if (!string.IsNullOrEmpty(objentityUserMasterViewModel.objEntityUser.Password))
                        objentityUserMasterViewModel.objEntityUser.Password = Helper.protectString(objentityUserMasterViewModel.objEntityUser.Password);
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUsers/" + objentityUserMasterViewModel.objEntityUser.EntityUserId, Method.PUT, objentityUserMasterViewModel.objEntityUser, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/entityuser?entityId=" + Request["entityId"]) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Parent Agency selection is missing." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult deleteEntityUser(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUsers/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getEntityUserList(long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityUserMasterViewModel objentityUserMasterViewModel = new entityUserMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUsers?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&entityId=" + entityId, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objentityUserMasterViewModel = JsonConvert.DeserializeObject<entityUserMasterViewModel>(response.Content);
                return Json(new { data = objentityUserMasterViewModel.objEntityUserList, draw = Request["draw"], recordsTotal = objentityUserMasterViewModel.totalRecords, recordsFiltered = objentityUserMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objentityUserMasterViewModel = new entityUserMasterViewModel();
                return Json(new { data = objentityUserMasterViewModel.objEntityUserList, draw = Request["draw"], recordsTotal = objentityUserMasterViewModel.totalRecords, recordsFiltered = objentityUserMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}