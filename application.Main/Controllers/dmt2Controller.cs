﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.BankITDMT.Response;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class dmt2Controller : Controller
    {
        // GET: dmt2
        public ActionResult transactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entityMasterViewModel objEntityMasterVM = new entityMasterViewModel();
                List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getAgencyDetail", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(webAPIResponse.Content);
                    objEntityMasterVM.objEntityMasterList = objEntityMasterList;
                }
                return View("history", objEntityMasterVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }


        [HttpGet]
        public JsonResult showTransactionHistory(long entityId, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/retailerTransactionHistory?searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<InstantPayDMTTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objInstantPayDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        #region BankIT DMT Transaction History
        public ActionResult DMTTransactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entityMasterViewModel objEntityMasterVM = new entityMasterViewModel();
                List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getAgencyDetail", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(webAPIResponse.Content);
                    objEntityMasterVM.objEntityMasterList = objEntityMasterList;
                }
                return View("DMThistory", objEntityMasterVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showDMTTransactionHistory(long entityId, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankItTransactionVM objTransactions = new BankItTransactionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/retailerTransactionHistory?searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankItTransactionVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AEPS Transaction History
        public ActionResult AEPSTransactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entityMasterViewModel objEntityMasterVM = new entityMasterViewModel();
                List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getAgencyDetail", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(webAPIResponse.Content);
                    objEntityMasterVM.objEntityMasterList = objEntityMasterList;
                }
                return View("AEPShistory", objEntityMasterVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showAEPSTransactionHistory(long entityId, string fromDate, string toDate)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("AEPS/retailerTransactionHistory?searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankITAEPSTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITAEPSTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Micro ATM Transaction History
        public ActionResult MicroATMTransactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entityMasterViewModel objEntityMasterVM = new entityMasterViewModel();
                List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getAgencyDetail", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(webAPIResponse.Content);
                    objEntityMasterVM.objEntityMasterList = objEntityMasterList;
                }
                return View("MicroATMhistory", objEntityMasterVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showMicroATMTransactionHistory(long entityId, string fromDate, string toDate)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MicroATM/MicroRetailerTransactionHistory?searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankITAEPSTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITAEPSTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}