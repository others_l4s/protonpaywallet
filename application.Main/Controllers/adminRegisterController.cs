﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class adminRegisterController : Controller
    {
        // GET: adminRegister
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult editProfile()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogin/getuserdata", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                tblLogin_Session objLoginSession = JsonConvert.DeserializeObject<tblLogin_Session>(response.Content);
                return RedirectToActionPermanent("addEdit", new { id = objLoginSession.LoginId });
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }



        public ActionResult addEdit(int id = 0)
        {
            adminLoginViewModel objAdminLoginList = new adminLoginViewModel();
            try
            {
                string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
                if (!String.IsNullOrEmpty(accessToken))
                {
                    IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("adminLogins", Method.GET, null, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objAdminLoginList = JsonConvert.DeserializeObject<adminLoginViewModel>(webAPIResponse.Content);
                    }
                    else
                    {
                        return RedirectToAction("index", new { Controller = "unauthorized" });
                    }
                    if (id > 0)
                    {
                        stateViewModel objStateMaster = new stateViewModel();
                        IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("adminLogins/" + id, Method.GET, null, accessToken);
                        objAdminLoginList.objAdminLogin = JsonConvert.DeserializeObject<tblAdminLogin>(response1.Content);
                        if (objAdminLoginList.objAdminLogin != null)
                        {
                            objAdminLoginList.objAdminLogin.Password = Helper.unProtectString(objAdminLoginList.objAdminLogin.Password);
                            IRestResponse stateListResponse = Helper.WebAPIClient_via_RestSharp("state?countryId=" + objAdminLoginList.objAdminLogin.CountryId, Method.GET, null, accessToken);
                            objStateMaster = JsonConvert.DeserializeObject<stateViewModel>(stateListResponse.Content);
                            if (objStateMaster != null)
                                objAdminLoginList.objStateList = objStateMaster.objStateMasterList;
                        }
                    }
                    else
                    {
                        objAdminLoginList.objAdminLogin.StatusId = (objAdminLoginList.objStatusList.Count() > 0 ? objAdminLoginList.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
                    }
                }
                else
                {
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                }
            }
            catch (Exception)
            {
            }
            return View("index", objAdminLoginList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveAdminUser(adminLoginViewModel objadminLoginViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!string.IsNullOrEmpty(accessToken))
            {
                objadminLoginViewModel.objAdminLogin.Password = Helper.protectString(objadminLoginViewModel.objAdminLogin.Password);
                if (objadminLoginViewModel.objAdminLogin.AdminLoginId == 0)
                {
                    if (!string.IsNullOrEmpty(objadminLoginViewModel.objAdminLogin.LoginName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins", Method.POST, objadminLoginViewModel.objAdminLogin, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/adminregister") }, JsonRequestBehavior.AllowGet);

                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("LoginName", "Please enter Login Name");
                        return Json(new { success = false, responseText = "1|Please enter Login Name" }, JsonRequestBehavior.AllowGet);

                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins/" + objadminLoginViewModel.objAdminLogin.AdminLoginId, Method.PUT, objadminLoginViewModel.objAdminLogin, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminregister") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }

                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteAdminUser(int id)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins/" + id, Method.DELETE, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Success");
                }
                else
                {
                    return Json(response.StatusCode.ToString());
                }
            }
            else
            {
                return Json(Response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getadminLoginList()
        {
            adminLoginViewModel adminLoginViewModel = new adminLoginViewModel();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                //Server Side Parameter
                int pageIndex = Convert.ToInt32(Request["start"]);
                int pageSize = Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    adminLoginViewModel = JsonConvert.DeserializeObject<adminLoginViewModel>(response.Content);
                    return Json(new { data = adminLoginViewModel.objAdminLoginList, draw = Request["draw"], recordsTotal = adminLoginViewModel.totalRecords, recordsFiltered = adminLoginViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    adminLoginViewModel = new adminLoginViewModel();
                    return Json(new { data = adminLoginViewModel.objAdminLoginList, draw = Request["draw"], recordsTotal = adminLoginViewModel.totalRecords, recordsFiltered = adminLoginViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { data = adminLoginViewModel.objAdminLoginList, draw = Request["draw"], recordsTotal = adminLoginViewModel.totalRecords, recordsFiltered = adminLoginViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}