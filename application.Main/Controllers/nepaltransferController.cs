﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class nepaltransferController : Controller
    {
        // GET: nepaltransfer
        public ActionResult Index()
        {
            return View("Index");
        }
        private string getRemitterIdFromMobileNo(string MobileNo)
        {
            string RemitterId = "";
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            ViewBag.MobileNo = MobileNo;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode == "TXN")
                    {
                        RemitterId = "0|" + objremitterresponse.data.remitter.id;
                    }
                    else
                    {
                        RemitterId = "1|" + "No Remitter Found";
                    }
                }
                else
                {
                    return RemitterId = "1|" + "No Remitter Found";
                }
            }
            else
            {
                return RemitterId = "1|" + "No Remitter Found";
            }
            return RemitterId;
        }
        public ActionResult Dashboard(string MobileNo)
        {
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode.ToLower() == "txn")
                    {
                        return View("Dashboard", objremitterresponse);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|"+ objremitterresponse.status;
                        return RedirectToAction("index", "nepaltransfer");
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Remitter Details Not Found.";
                    return RedirectToAction("index", "nepaltransfer");
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|Remitter Details Not Found.";
                return RedirectToAction("index", "nepaltransfer");
            }
        }

        public ActionResult AddRemitter(string MobileNo)
        {

            NepalremitterAddViewModel objremitterViewModel = new NepalremitterAddViewModel()
            {
                objRemitterRequest = new Nepal_remitter_request(),
            };
            objremitterViewModel.objRemitterRequest.request = new Nepal_remitter_req();
            objremitterViewModel.objRemitterRequest.request.mobile = MobileNo;

            List<Nepal_Remitter_Gender> objGenderList = new List<Nepal_Remitter_Gender>();
            objGenderList.Add(new Nepal_Remitter_Gender { Id = "M", Name = "Male" });
            objGenderList.Add(new Nepal_Remitter_Gender { Id = "F", Name = "Female" });
            ViewBag.objGenderList = objGenderList;

            List<Nepal_DocumentType> document_typeList = new List<Nepal_DocumentType>()
            {new Nepal_DocumentType(){ID=1,Name="Aadhaar Card"},
            new Nepal_DocumentType(){ID=2,Name="Driving License"},
            new Nepal_DocumentType() {ID=3,Name="Indian Citizenship" },
            new Nepal_DocumentType() {ID=4,Name="Nepalese Citizenship" },
            new Nepal_DocumentType() {ID=5,Name="Pan Card" },
            new Nepal_DocumentType() {ID=6,Name="Passport" },
            new Nepal_DocumentType() {ID=7,Name="Rasan Card"},
            new Nepal_DocumentType() {ID=8,Name="Voter ID" }
            };
            objremitterViewModel.document_typeList = document_typeList;
            ViewBag.MobileNo = MobileNo;
            return View("AddRemitter", objremitterViewModel);
        }
        [HttpPost]
        public ActionResult saveRemitter(NepalremitterAddViewModel objremiteerViewModel, HttpPostedFileBase ImageFile)
        {
            string FileName = Path.GetFileNameWithoutExtension(ImageFile.FileName);
            string FileExtension = Path.GetExtension(ImageFile.FileName);
            FileName = DateTime.Now.ToString("yyyyMMddmmss") + FileExtension;
            string UploadPath = ConfigurationManager.AppSettings["UserImagePath"].ToString();
            objremiteerViewModel.objRemitterRequest.request.document_url = UploadPath + FileName;
            ImageFile.SaveAs(objremiteerViewModel.objRemitterRequest.request.document_url);
            Nepal_remitter_request objremitterRequest = new Nepal_remitter_request();
            objremitterRequest = objremiteerViewModel.objRemitterRequest;
            objremitterRequest.token = "12345";
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("addremitter", Method.POST, objremitterRequest, objremitterRequest.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_register_Response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_register_Response>(webAPIResponse.Content);
                if (objremitterresponse.statuscode.ToLower() == "txn")
                {
                    Nepal_RemitterInDB_save objremitterSave = new Nepal_RemitterInDB_save();
                    objremitterSave.token = "12345";

                    objremitterSave.Name = objremitterRequest.request.name;
                    objremitterSave.Address = objremitterRequest.request.address;
                    objremitterSave.DateOfBirth = Convert.ToDateTime(objremitterRequest.request.dob);
                    objremitterSave.RemitterId = objremitterresponse.data.remitter.id;
                    objremitterSave.MobileNo = objremitterRequest.request.mobile;
                    objremitterSave.DocumentType = objremitterRequest.request.document_type;
                    objremitterSave.DocumentNo = objremitterRequest.request.document_number;
                    objremitterSave.DocumentUrl = objremitterRequest.request.document_url;
                    objremitterSave.Gender = objremitterRequest.request.gender;
                    objremitterSave.Pincode = objremitterRequest.request.pincode;
                    IRestResponse webAPIResponseSaveInDB = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_saveRemitter", Method.POST, objremitterSave, objremitterSave.token);
                    Nepal_RemitterInDB_save objremitterresponsesave = JsonConvert.DeserializeObject<Nepal_RemitterInDB_save>(webAPIResponse.Content);
                    if (webAPIResponseSaveInDB.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + "Remitter Not Added";
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objremitterresponse.status;
                }
            }

            return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = objremiteerViewModel.objRemitterRequest.request.mobile });
        }

        public ActionResult AddBeneficiary(string MobileNo)
        {
            string RemitterDetails = getRemitterIdFromMobileNo(MobileNo);
            string[] arrayRemitter = RemitterDetails.Split('|');
            if (arrayRemitter[0] == "0")
            {
                string RemitterId = arrayRemitter[1];
                if (RemitterId.Length > 0)
                {
                    Nepal_bank_details_request objbankDetails = new Nepal_bank_details_request();
                    objbankDetails.token = "12345";
                    IRestResponse webAPIResponseBankDetails = Helper.WebAPIINDONepal_via_RestSharp("bank_details", Method.POST, objbankDetails, objbankDetails.token);
                    NepalBeneficiaryAddViewModel objBeneficiaryAddViewModel = new NepalBeneficiaryAddViewModel();
                    if (webAPIResponseBankDetails.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Nepal_bank_details_Response objbankdetailsResponse = JsonConvert.DeserializeObject<Nepal_bank_details_Response>(webAPIResponseBankDetails.Content);
                        objBeneficiaryAddViewModel.objBankDetailsList = objbankdetailsResponse.data;

                    }
                    objBeneficiaryAddViewModel.objBeneficiaryMaster = new Nepal_beneficiary_register_request();
                    objBeneficiaryAddViewModel.objBeneficiaryMaster.request = new Nepal_beneficiary_register_req();
                    List<Nepal_Remitter_Gender> objGenderList = new List<Nepal_Remitter_Gender>();
                    objGenderList.Add(new Nepal_Remitter_Gender { Id = "MALE", Name = "Male" });
                    objGenderList.Add(new Nepal_Remitter_Gender { Id = "FEMALE", Name = "Female" });
                    ViewBag.objGenderList = objGenderList;

                    objBeneficiaryAddViewModel.objBeneficiaryMaster.request.remitterid = RemitterId;
                    objBeneficiaryAddViewModel.objBeneficiaryMaster.request.mode = "1";
                    objBeneficiaryAddViewModel.objBeneficiaryMaster.request.MobileNo = MobileNo;
                    return View("AddBeneficiary", objBeneficiaryAddViewModel);
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + "Please Check Your Remitter Details";
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + "Please Check Your Remitter Details";
                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
            }

        }

        public ActionResult BeneficiaryVerifiedDirect(string id, string MobileNo)
        {
            string BeneficiaryId = id;
            string RemitterDetails = getRemitterIdFromMobileNo(MobileNo);
            string[] arrayRemitter = RemitterDetails.Split('|');
            if (arrayRemitter[0] == "0")
            {
                string RemitterId = arrayRemitter[1];
                if (RemitterId.Length > 0)
                {
                    Nepal_beneficiary_resend_otp_request objbeneficiaryresendRequest = new Nepal_beneficiary_resend_otp_request();
                    Nepal_beneficiary_resend_otp_req objResendRequest = new Nepal_beneficiary_resend_otp_req();
                    objResendRequest.beneficiaryid = BeneficiaryId;
                    objResendRequest.remitterid = RemitterId;
                    objbeneficiaryresendRequest.request = objResendRequest;
                    objbeneficiaryresendRequest.token = "12345";
                    IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("beneficiary_resend_otp", Method.POST, objbeneficiaryresendRequest, objbeneficiaryresendRequest.token);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Nepal_beneficiary_register_otp_resend_response objresendResponse = JsonConvert.DeserializeObject<Nepal_beneficiary_register_otp_resend_response>(webAPIResponse.Content);
                        if (objresendResponse.statuscode.ToLower() == "txn")
                        {
                            Nepal_beneficiary_register_validate_request objbeneficiaryVerifyRequest = new Nepal_beneficiary_register_validate_request();
                            Nepal_beneficiary_register_validate_req objRequest = new Nepal_beneficiary_register_validate_req();
                            objRequest.beneficiaryid = BeneficiaryId;
                            objRequest.remitterid = RemitterId;
                            objbeneficiaryVerifyRequest.token = "12345";
                            objbeneficiaryVerifyRequest.request = objRequest;
                            objbeneficiaryVerifyRequest.MobileNo = MobileNo;
                            return View("BeneficiaryVerify", objbeneficiaryVerifyRequest);
                        }
                        else
                        {
                            TempData[Helper.responseStatus] = "1|" + "OTP NOT SEND.";
                            return RedirectToAction("AllBeneficiary", "nepaltransfer", new { MobileNo = MobileNo });
                        }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + webAPIResponse.StatusCode;
                        return RedirectToAction("AllBeneficiary", "nepaltransfer", new { MobileNo = MobileNo });
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + "Please Check Your Remitter";
                    return RedirectToAction("AllBeneficiary", "nepaltransfer", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + "Please Check Your Remitter";
                return RedirectToAction("AllBeneficiary", "nepaltransfer", new { MobileNo = MobileNo });
            }
        }

        [HttpPost]
        public ActionResult BeneficiaryValidate(Nepal_beneficiary_register_validate_request objRequestValidate)
        {
            objRequestValidate.token = "12345";
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("beneficiary_register_validate", Method.POST, objRequestValidate, objRequestValidate.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_beneficiary_register_validate_Response objbankdetailsResponse = JsonConvert.DeserializeObject<Nepal_beneficiary_register_validate_Response>(webAPIResponse.Content);
                if (objbankdetailsResponse.statuscode.ToLower() == "txn")
                {
                    TempData[Helper.responseStatus] = "0|" + "Beneficiary Successfully Verified";
                    return RedirectToAction("Dashboard", new { MobileNo = objRequestValidate.MobileNo });
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objbankdetailsResponse.status;
                    return RedirectToAction("Dashboard", new { MobileNo = objRequestValidate.MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + "Beneficiary Not Verified";
                return RedirectToAction("Dashboard", new { MobileNo = objRequestValidate.MobileNo });
            }
        }


        public ActionResult accountVerifiedDirect(string id, string MobileNo)
        {
            string RemitterDetails = getRemitterIdFromMobileNo(MobileNo);
            string[] arrayRemitter = RemitterDetails.Split('|');
            if (arrayRemitter[0] == "0")
            {
                Nepal_BeneficiaryInDB_save objBeneficiaryInDB = new Nepal_BeneficiaryInDB_save();
                objBeneficiaryInDB.token = "12345";
                objBeneficiaryInDB.beneficiaryid = id;
                string RemitterId = arrayRemitter[1];
                objBeneficiaryInDB.remitterid = RemitterId;
                ViewBag.MobileNo = MobileNo;
                IRestResponse webAPIResponseInDb = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_getBeneficiary", Method.POST, objBeneficiaryInDB, objBeneficiaryInDB.token);
                if (webAPIResponseInDb.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Nepal_BeneficiaryInDB_save objbeneficiaryresponse = JsonConvert.DeserializeObject<Nepal_BeneficiaryInDB_save>(webAPIResponseInDb.Content);
                    if (objbeneficiaryresponse.AccountNo != null)
                    {
                        Nepal_validate_bank_account_request objBankVerify = new Nepal_validate_bank_account_request();
                        Nepal_validate_bank_account_req objRequest = new Nepal_validate_bank_account_req();
                        objBankVerify.token = "12345";
                        objRequest.agentid = id;
                        objRequest.account_number = objbeneficiaryresponse.AccountNo;
                        objRequest.remittermobile = MobileNo;
                        objBankVerify.request = objRequest;
                        IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("beneficiary_bank_account_verify", Method.POST, objBankVerify, objBankVerify.token);
                        Nepal_validate_bank_account_Response objaccountverifiedresponse = JsonConvert.DeserializeObject<Nepal_validate_bank_account_Response>(webAPIResponse.Content);
                        if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {

                            if (objaccountverifiedresponse.statuscode.ToLower() == "txn")
                            {
                                TempData[Helper.responseStatus] = "0|" + "Account Successfully Verified.";
                                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                            }
                            else
                            {
                                TempData[Helper.responseStatus] = "1|" + objaccountverifiedresponse.status;
                                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                            }

                        }
                        else
                        {
                            TempData[Helper.responseStatus] = "1|" + objaccountverifiedresponse.status;
                            return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                        }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + "No Beneficiary Account Found.";
                        return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + "No Beneficiary Account Found.";
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + "Please Check Your Remitter";
                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
            }
        }

        [HttpPost]
        public ActionResult saveBeneficiary(NepalBeneficiaryAddViewModel objBeneficiaryViewModel)   
        {
            Nepal_beneficiary_register_request objbeneficiaryRequest = new Nepal_beneficiary_register_request();
            objbeneficiaryRequest = objBeneficiaryViewModel.objBeneficiaryMaster;
            objbeneficiaryRequest.token = "12345";
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("addbeneficiary", Method.POST, objbeneficiaryRequest, objbeneficiaryRequest.token);
            Nepal_beneficiary_register_Response objcreaterecipient = JsonConvert.DeserializeObject<Nepal_beneficiary_register_Response>(webAPIResponse.Content);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (objcreaterecipient.statuscode.ToLower() == "txn")
                {
                    TempData[Helper.responseStatus] = Helper.recordSaved;
                    Nepal_BeneficiaryInDB_save objBeneficiarySave = new Nepal_BeneficiaryInDB_save();
                    objBeneficiarySave.token = "12345";
                    objBeneficiarySave.AccountNo = objbeneficiaryRequest.request.account;
                    objBeneficiarySave.Name = objbeneficiaryRequest.request.name;
                    objBeneficiarySave.Address = objbeneficiaryRequest.request.address;
                    objBeneficiarySave.BranchId = objbeneficiaryRequest.request.branchid;
                    objBeneficiarySave.Mode = objbeneficiaryRequest.request.mode;
                    objBeneficiarySave.Gender = objbeneficiaryRequest.request.gender;
                    objBeneficiarySave.remitterid = objcreaterecipient.data.remitter.id;
                    objBeneficiarySave.beneficiaryid = Convert.ToString(objcreaterecipient.data.beneficiary.id);
                    IRestResponse webAPIResponseSaveInDB = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_saveBeneficiary", Method.POST, objBeneficiarySave, objBeneficiarySave.token);
                    Nepal_BeneficiaryInDB_save objbeneficiaryresponsesave = JsonConvert.DeserializeObject<Nepal_BeneficiaryInDB_save>(webAPIResponse.Content);
                    if (webAPIResponseSaveInDB.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Nepal_beneficiary_register_validate_request objbeneficiaryVerifyRequest = new Nepal_beneficiary_register_validate_request();
                        Nepal_beneficiary_register_validate_req objRequest = new Nepal_beneficiary_register_validate_req();
                        objRequest.beneficiaryid = Convert.ToString(objcreaterecipient.data.beneficiary.id);
                        objRequest.remitterid = objcreaterecipient.data.remitter.id;
                        objbeneficiaryVerifyRequest.token = "12345";
                        objbeneficiaryVerifyRequest.request = objRequest;
                        objbeneficiaryVerifyRequest.MobileNo = objbeneficiaryRequest.request.MobileNo;
                        return View("BeneficiaryVerify", objbeneficiaryVerifyRequest);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + "Beneficiary not Added";
                    }
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = objbeneficiaryRequest.request.MobileNo });
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objcreaterecipient.status;
                    return RedirectToAction("Dashboard", "nepaltransfer", new { CustomerId = objbeneficiaryRequest.request.MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + objcreaterecipient.status;
                return RedirectToAction("Dashboard", "nepaltransfer", new { CustomerId = objbeneficiaryRequest.request.MobileNo });
            }
        }

        public ActionResult AllBeneficiary(string MobileNo)
        {
            ViewBag.MobileNo = MobileNo;
            return View("AllBeneficiary");
        }

        public string deleteBeneficiary(string id, string MobileNo)
        {
            string RemitterId = getRemitterIdFromMobileNo(MobileNo);
            string[] arrayRemitter = RemitterId.Split('|');
            string webResponse;
            if (arrayRemitter[0] == "0")
            {
                Nepal_beneficiary_remove_req objrequest = new Nepal_beneficiary_remove_req();
                objrequest.beneficiaryid = id;
                objrequest.remitterid = arrayRemitter[1];
                Nepal_beneficiary_remove_request objRemoveRequest = new Nepal_beneficiary_remove_request();
                objRemoveRequest.token = "12345";
                objRemoveRequest.request = objrequest;
                ViewBag.MobileNo = MobileNo;
                IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("beneficiary_remove", Method.POST, objRemoveRequest, objRemoveRequest.token);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Nepal_beneficiary_remove_Response objremitterresponse = JsonConvert.DeserializeObject<Nepal_beneficiary_remove_Response>(webAPIResponse.Content);
                    if (objremitterresponse.data != null)
                    {
                        if (objremitterresponse.data.otp == 1)
                        {
                            Nepal_beneficiary_remove_validate_request objremovevalidate = new Nepal_beneficiary_remove_validate_request();
                            Nepal_beneficiary_remove_validate_req request = new Nepal_beneficiary_remove_validate_req();
                            request.beneficiaryid = objrequest.beneficiaryid;
                            request.remitterid = objrequest.remitterid;
                            objremovevalidate.request = request;
                            return webResponse = "Success|" + request.beneficiaryid + "|" + request.remitterid;
                        }
                        else
                        {
                            return webResponse = "Success";
                        }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + objremitterresponse.status;
                        return webResponse = "Error";
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + "Beneficiary Details Not Removed.";
                    return webResponse = "Error";
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + "Remitter Details Not Found.";
                return webResponse = "Error";
            }

        }

        public ActionResult RemoveValidateBeneficiary(string MobileNo, string RemitterId, string BeneficiaryId, string OTP)
        {
            Nepal_beneficiary_remove_validate_request objRequest = new Nepal_beneficiary_remove_validate_request();
            Nepal_beneficiary_remove_validate_req request = new Nepal_beneficiary_remove_validate_req();
            request.beneficiaryid = BeneficiaryId;
            request.remitterid = RemitterId;
            request.otp = OTP;
            objRequest.token = "12345";
            objRequest.request = request;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("beneficiary_remove_validate", Method.POST, objRequest, objRequest.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_beneficiary_remove_validate_Response objremitterresponse = JsonConvert.DeserializeObject<Nepal_beneficiary_remove_validate_Response>(webAPIResponse.Content);
                if (objremitterresponse.statuscode.ToLower() == "txn")
                {
                    Nepal_BeneficiaryInDB_save objbeneficiaryrequest = new Nepal_BeneficiaryInDB_save();
                    objbeneficiaryrequest.beneficiaryid = BeneficiaryId;
                    objbeneficiaryrequest.remitterid = RemitterId;
                    objbeneficiaryrequest.token = "12345";
                    IRestResponse webAPIResponseINDB = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_deleteBeneficiary", Method.POST, objbeneficiaryrequest, objbeneficiaryrequest.token);
                    Nepal_BeneficiaryInDB_save objdeleteBeneficiaryINDBresponse = JsonConvert.DeserializeObject<Nepal_BeneficiaryInDB_save>(webAPIResponseINDB.Content);
                    TempData[Helper.responseStatus] = "0|" + "Beneficiary Remove Successfully";
                    return RedirectToAction("AllBeneficiary", new { MobileNo = MobileNo });
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objremitterresponse.status.ToLower();
                    return RedirectToAction("AllBeneficiary", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + webAPIResponse.StatusCode;
                return RedirectToAction("AllBeneficiary", new { MobileNo = MobileNo });
            }
        }

        public JsonResult getAllBeneficiaryList(string MobileNo)
        {
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            ViewBag.MobileNo = MobileNo;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode.ToLower() == "txn")
                    {
                        return Json(new { data = objremitterresponse.data.beneficiary, draw = Request["draw"], recordsTotal = objremitterresponse.data.beneficiary.Count, recordsFiltered = objremitterresponse.data.beneficiary.Count }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(null);
                    }
                }
                else
                {
                    return Json(null);
                }
            }
            else
            {
                return Json(null);
            }

        }

        public ActionResult Transfer(string MobileNo)
        {
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            ViewBag.MobileNo = MobileNo;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode.ToLower() == "txn")
                    {
                        NepalTransferViewModel objtransviewmodel = new NepalTransferViewModel();
                        Nepal_transfer_request objNepal_transfer_request = new Nepal_transfer_request();
                        Nepal_transfer_req objrequest = new Nepal_transfer_req();
                        objtransviewmodel.objBeneficiryList = objremitterresponse.data.beneficiary;
                        objtransviewmodel.objBeneficiryList.Select(e => e.status == "1");
                        objrequest.remitterid = objremitterresponse.data.remitter.id;
                        objrequest.remittermobile = objremitterresponse.data.remitter.mobile;
                        objrequest.mode = "1";
                        objNepal_transfer_request.request = objrequest;
                        objtransviewmodel.objtransfer = objNepal_transfer_request;
                        return View(objtransviewmodel);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|"+objremitterresponse.status;
                        return RedirectToAction("Dashboard", new { MobileNo = MobileNo });
                    }
                }
                else
                {

                    TempData[Helper.responseStatus] = "1|Beneficiary Details Not Found";
                    return RedirectToAction("Dashboard", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + webAPIResponse.StatusCode;
                return RedirectToAction("Dashboard", new { MobileNo = MobileNo });
            }
        }

        [HttpPost]
        public ActionResult saveTransfer(NepalTransferViewModel objNepalTransferViewModel)
        {
            Nepal_transfer_request objtransferRequest = new Nepal_transfer_request();
            objtransferRequest = objNepalTransferViewModel.objtransfer;
            objtransferRequest.token = "12345";
            objtransferRequest.request.agentid = objNepalTransferViewModel.objtransfer.request.beneficiaryid;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("transfer", Method.POST, objtransferRequest, objtransferRequest.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_transfer_Response objtransferresponse = JsonConvert.DeserializeObject<Nepal_transfer_Response>(webAPIResponse.Content);
                Nepal_SaveTransactionInDb_request objTransactionRequest = new Nepal_SaveTransactionInDb_request();
                if (objtransferresponse.data != null)
                {
                    objTransactionRequest.token = "12345";
                    objTransactionRequest.RemitterId = objNepalTransferViewModel.objtransfer.request.remitterid;
                    objTransactionRequest.Mode = "1";
                    objTransactionRequest.BeneficiaryId = objNepalTransferViewModel.objtransfer.request.beneficiaryid;
                    objTransactionRequest.ipay_id = objtransferresponse.data.ipay_id;
                    objTransactionRequest.RequestApi = JsonConvert.SerializeObject(objtransferRequest);
                    objTransactionRequest.ResponseApi = JsonConvert.SerializeObject(objtransferresponse);
                    objTransactionRequest.StatusCode = objtransferresponse.statuscode;
                    objTransactionRequest.Transfer_Amount = objtransferresponse.data.transfer_amount;
                    objTransactionRequest.ExchangeRate = objtransferresponse.data.exchangerate;
                    objTransactionRequest.PayoutAmount = objtransferresponse.data.payoutamount;
                    objTransactionRequest.ServiceCharge = objtransferresponse.data.servicecharge;
                    objTransactionRequest.TransactionAmount = objtransferresponse.data.transaction_amount;
                    objTransactionRequest.ChargedAmount = objtransferresponse.data.charged_amount;
                    if (objtransferresponse.statuscode.ToLower() == "tup")
                    {
                        objTransactionRequest.Status = false;
                    }
                    IRestResponse webAPIResponsesaveInDB = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_saveTransaction", Method.POST, objTransactionRequest, objTransactionRequest.token);
                    Nepal_SaveTransactionInDb_request objtransfersaveresponse = JsonConvert.DeserializeObject<Nepal_SaveTransactionInDb_request>(webAPIResponsesaveInDB.Content);

                    TempData[Helper.responseStatus] = "0|" + objtransferresponse.status;
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = objtransferRequest.request.remittermobile });
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objtransferresponse.status;
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = objtransferRequest.request.remittermobile });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|" + webAPIResponse.StatusCode;
                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = objtransferRequest.request.remittermobile });
            }
        }

        public ActionResult ViewTransaction(string MobileNo)
        {
            Nepal_Remitter_Details_Request objremitter = new Nepal_Remitter_Details_Request();
            objremitter.token = "12345";
            Nepal_Remitter_Details_req nepal_Remitter_Details_Req = new Nepal_Remitter_Details_req();
            nepal_Remitter_Details_Req.mobile = MobileNo;
            ViewBag.MobileNo = MobileNo;
            objremitter.request = nepal_Remitter_Details_Req;
            IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("getremitter_details", Method.POST, objremitter, objremitter.token);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Nepal_remitter_details_response objremitterresponse = JsonConvert.DeserializeObject<Nepal_remitter_details_response>(webAPIResponse.Content);
                if (objremitterresponse != null)
                {
                    if (objremitterresponse.statuscode.ToLower() == "txn")
                    {
                        NepalTransactionViewModel objTransactionViewModel = new NepalTransactionViewModel();
                        NepalTransactionView_Request objNepalTransactionView_Request = new NepalTransactionView_Request();
                        objTransactionViewModel.objRemitter = objremitterresponse;
                        objNepalTransactionView_Request.RemitterID = objremitterresponse.data.remitter.id;
                        objTransactionViewModel.objTransactionView = objNepalTransactionView_Request;

                        return View("ViewTransaction", objTransactionViewModel);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|"+ objremitterresponse.status;
                        return RedirectToAction("Dashboard", "nepaltransfer",new { MobileNo = MobileNo });
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|No Transaction Found";
                    return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|Remitter Not Found.";
                return RedirectToAction("Dashboard", "nepaltransfer", new { MobileNo = MobileNo });
            }
            
        }        
        public JsonResult getTransactionList(string RemitterId,string BeneficiaryId)
        {
            NepalTransactionView_Request objTransactionrequest = new NepalTransactionView_Request();
            objTransactionrequest.token = "12345";
            objTransactionrequest.RemitterID = RemitterId;
            objTransactionrequest.BeneficiaryID = BeneficiaryId;
            if (objTransactionrequest.RemitterID!=null)
            {
                IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("IndoNepalINDB_getTransaction", Method.POST, objTransactionrequest, objTransactionrequest.token);
                List<NepalTransactionView_Request> objTransactionresponse = JsonConvert.DeserializeObject<List<NepalTransactionView_Request>>(webAPIResponse.Content);
                if (objTransactionresponse != null)
                {
                    return Json(new { data = objTransactionresponse, draw = Request["draw"], recordsTotal = objTransactionresponse.Count, recordsFiltered = objTransactionresponse.Count }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + "Beneficiary Transaction Not Found.";
                    return Json(null);
                }
            }
            else
            {
               return Json(null);
            }           
        }

        public ActionResult ViewTransactionStatus(string id, string MobileNo)
        {
            if (id.Length > 0)
            {
                Nepal_transfer_status_request objtransactionrequest = new Nepal_transfer_status_request();
                Nepal_transfer_status_req request = new Nepal_transfer_status_req();
                request.order_id = id;
                objtransactionrequest.token = "12345";
                objtransactionrequest.request = request;

                IRestResponse webAPIResponse = Helper.WebAPIINDONepal_via_RestSharp("transfer_status", Method.POST, objtransactionrequest, objtransactionrequest.token);
                Nepal_transfer_Response objTransactionresponse = JsonConvert.DeserializeObject<Nepal_transfer_Response>(webAPIResponse.Content);
                if (objTransactionresponse != null)
                {
                    if(objTransactionresponse.statuscode.ToLower()=="txn")
                    {
                        Nepal_Transaction_Status_UpdateINDB_Request objStatusUpdateINDB = new Nepal_Transaction_Status_UpdateINDB_Request();
                        objStatusUpdateINDB.Ipayid = id;
                        objStatusUpdateINDB.StatusCode = objTransactionresponse.data.transaction_status;
                        objStatusUpdateINDB.token = objtransactionrequest.token;
                        IRestResponse webAPIResponseInDB = Helper.WebAPIINDONepal_via_RestSharp("transfer_status_updateINDB", Method.POST, objStatusUpdateINDB, objStatusUpdateINDB.token);
                        NepalTransactionView_Request objTransactionresponseINDB = JsonConvert.DeserializeObject<NepalTransactionView_Request>(webAPIResponseInDB.Content);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|" + objTransactionresponse.status;
                    }
                    return RedirectToAction("ViewTransaction","nepaltransfer", new { MobileNo = MobileNo });
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|" + objTransactionresponse.status;
                    return RedirectToAction("ViewTransaction", "nepaltransfer", new { MobileNo = MobileNo });
                }
            }
            else
            {
                TempData[Helper.responseStatus] = "1|Please Check your Transaction PayID";
                return RedirectToAction("ViewTransaction", "nepaltransfer", new { MobileNo = MobileNo });
            }
        }
    }
}