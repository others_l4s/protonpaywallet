﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;

namespace application.Main.Controllers
{
    public class ticketController : Controller
    {
        // GET: ticket
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult raiseTicket()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            supportTicketVM objSupportTicketVM = new supportTicketVM();
            IRestResponse webAPIResponse;
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("supportTicket", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSupportTicketVM = JsonConvert.DeserializeObject<supportTicketVM>(webAPIResponse.Content);
                if (objSupportTicketVM.objSupportTicket != null)
                {
                    objSupportTicketVM.objSupportTicket.AttachmentName = "../KYCImages/NoImages.png";
                }
            }
            return View("index", objSupportTicketVM);
        }

        public ActionResult viewTicket(long id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            supportTicketVM objSupportTicketVM = new supportTicketVM();
            IRestResponse webAPIResponse;
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("supportTicket/" + id, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSupportTicketVM = JsonConvert.DeserializeObject<supportTicketVM>(webAPIResponse.Content);
                if (objSupportTicketVM.objSupportTicketList.Count > 0)
                {
                    objSupportTicketVM.objSupportTicket = objSupportTicketVM.objSupportTicketList[0];
                    if (!String.IsNullOrEmpty(objSupportTicketVM.objSupportTicket.AttachmentName))
                    {
                        objSupportTicketVM.objSupportTicket.AttachmentName = (objSupportTicketVM.objSupportTicket.FromMobileDevice == true ?
                            ConfigurationManager.AppSettings["webAPIURL"] + "supportTicketAttachments/" :
                            "../supportTicketAttachments/") + "" + objSupportTicketVM.objSupportTicket.AttachmentName.ToString();
                    }
                    else
                    {
                        objSupportTicketVM.objSupportTicket.AttachmentName = "../KYCImages/NoImages.png";
                    }
                    objSupportTicketVM.objSupportTicket.displayTicketId = string.Format("{0:D5}", objSupportTicketVM.objSupportTicket.TicketId);
                    objSupportTicketVM.objSupportTicket.Comments = objSupportTicketVM.objSupportTicket.Comments.Replace(Environment.NewLine, "<br />");
                    objSupportTicketVM.objSupportTicket.displayCreatedDateTime = objSupportTicketVM.objSupportTicket.CreatedDateTime.Value.ToShortDateString();
                }
            }
            return View("detail", objSupportTicketVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveRaiseTicket(supportTicketVM objSupportTicketVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            objSupportTicketVM.objSupportTicket.FromMobileDevice = false;
            if (objSupportTicketVM.HPAttachment != null)
            {
                string attachmentName = Path.GetFileNameWithoutExtension(objSupportTicketVM.HPAttachment.FileName);
                string attachmentExtension = Path.GetExtension(objSupportTicketVM.HPAttachment.FileName);
                attachmentName = "ticketAttachment_" + Helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                objSupportTicketVM.HPAttachment.SaveAs(Path.Combine(Server.MapPath("../supportTicketAttachments/"), attachmentName));
                objSupportTicketVM.objSupportTicket.AttachmentName = attachmentName;
            }
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("supportTicket", Method.POST, objSupportTicketVM.objSupportTicket, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                TempData[Helper.responseStatus] = "0|Ticket has been successfully raised";
                return Json(new { success = true, redirectURL = Url.Content("~/ticket") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getTicketList(int ticketType, string fromDate, string toDate)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            Boolean isClosed = ticketType == 2 ? true : false;
            Boolean isRaised = ticketType == 1 ? true : false;
            supportTicketVM objSupportTicketVM = new supportTicketVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("supportTicket?searchEntityId=0&isClosed=" + isClosed + "&isRaised=" + isRaised + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSupportTicketVM = JsonConvert.DeserializeObject<supportTicketVM>(response.Content);
                return Json(new { data = objSupportTicketVM.objSupportTicketList, draw = Request["draw"], recordsTotal = objSupportTicketVM.totalRecords, recordsFiltered = objSupportTicketVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                //objSupportTicketVM = new supportTicketVM();
                return Json(new { data = objSupportTicketVM.objSupportTicketList, draw = Request["draw"], recordsTotal = objSupportTicketVM.totalRecords, recordsFiltered = objSupportTicketVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}