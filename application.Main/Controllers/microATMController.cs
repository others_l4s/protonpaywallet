﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.BankITAEPS.Request;
using application.Main.BankITAEPS.Response;
using Newtonsoft.Json;
using RestSharp;
using System.Configuration;
using application.Main.ViewModels;

namespace application.Main.Controllers
{
    public class microATMController : Controller
    {
        // GET: microATM
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblBankITMicroATM_Request objRequest = new tblBankITMicroATM_Request();
                IRestResponse apiResponse = Helper.WebAPIClient_via_RestSharp("getBankITMicroATMRequest?id=0", Method.GET, null, accessToken);
                if (apiResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objRequest = JsonConvert.DeserializeObject<tblBankITMicroATM_Request>(apiResponse.Content);
                }
                return View("index", objRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult requestMicroATM(tblBankITMicroATM_Request objRequest)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objRequest.AccessAmount), accessToken))
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BankITMicroATMRequestSave", Method.POST, objRequest, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = "0|Request successfully placed.";
                    return Json(new { success = true, redirectURL = Url.Content("~/microatm") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Micro ATM Transaction History
        public ActionResult MicroATMTransactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                return View("MicroATMhistory");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showMicroATMTransactionHistory(string fromDate, string toDate)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MicroATM/MicroATMTransactionsHistory?searchEntityId=0&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankITAEPSTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITAEPSTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}