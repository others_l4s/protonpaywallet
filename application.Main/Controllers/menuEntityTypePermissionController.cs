﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class menuEntityTypePermissionController : Controller
    {
        // GET: menuEntityTypePermission
        public ActionResult Index()
        {
            MenuEntityTypePermissionVM objPermissionVM = new MenuEntityTypePermissionVM();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityType", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                entityTypeViewModel objEntityTypeVM = JsonConvert.DeserializeObject<entityTypeViewModel>(webAPIResponse.Content);
                objPermissionVM.objEntityTypeList = objEntityTypeVM.objEntityTypeList;
            }
            return View("index", objPermissionVM);
        }

        public ActionResult getMenuList(int entityTypeId)
        {
            MenuEntityTypePermissionVM objPermissionVM = new MenuEntityTypePermissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("Menu_EntityType_Permission?entityTypeId=" + entityTypeId, Method.GET, null, accessToken);
            objPermissionVM = JsonConvert.DeserializeObject<MenuEntityTypePermissionVM>(webAPIResponseMenu.Content);
            objPermissionVM.entityTypeId = entityTypeId;
            return PartialView("_partial_EntityType_Permission", objPermissionVM);
        }

        [HttpPost]
        public JsonResult SaveMenuEntityTypePermission(MenuEntityTypePermissionVM objPermissionVM)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse bulkDeleteResponse = Helper.WebAPIClient_via_RestSharp("Menu_EntityType_Permission/" + objPermissionVM.entityTypeId, Method.DELETE, null, accessToken);
            if (bulkDeleteResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                tblMenu_EntityType_Permission objEntityTypeMenuPermission;
                foreach (tblMenu_EntityType_Permission objData in objPermissionVM.objMainMenuList)
                {
                    if (objData.CanView == true)
                    {
                        objEntityTypeMenuPermission = new tblMenu_EntityType_Permission();
                        objEntityTypeMenuPermission.EntityTypeId = objPermissionVM.entityTypeId;
                        objEntityTypeMenuPermission.MainMenuId = objData.MainMenuId;
                        objEntityTypeMenuPermission.CanView = objData.CanView;
                        objEntityTypeMenuPermission.SubMenuId = 0;
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("Menu_EntityType_Permission", Method.POST, objEntityTypeMenuPermission, accessToken);
                    }
                }

                foreach (tblMenu_EntityType_Permission objData in objPermissionVM.objSubMenuList)
                {
                    if (objData.CanView == true)
                    {
                        objEntityTypeMenuPermission = new tblMenu_EntityType_Permission();
                        objEntityTypeMenuPermission.EntityTypeId = objPermissionVM.entityTypeId;
                        objEntityTypeMenuPermission.MainMenuId = objData.MainMenuId;
                        objEntityTypeMenuPermission.CanView = objData.CanView;
                        objEntityTypeMenuPermission.SubMenuId = objData.SubMenuId;
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("Menu_EntityType_Permission", Method.POST, objEntityTypeMenuPermission, accessToken);
                    }
                }
                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                customResponse objCustomResponse = new customResponse();
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(bulkDeleteResponse.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + bulkDeleteResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}