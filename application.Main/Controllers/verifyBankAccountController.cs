﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.ViewModels;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class verifyBankAccountController : Controller
    {
        // GET: verifyBankAccount
       public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult MD()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("md");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        public JsonResult pendingAccountVerificationListMD()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityMasterViewModel entityMasterViewModel = new entityMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("pendingAccountVerificationListMD?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                entityMasterViewModel = JsonConvert.DeserializeObject<entityMasterViewModel>(response.Content);
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                entityMasterViewModel = new mdViewModel();
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult pendingAccountVerificationList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityMasterViewModel entityMasterViewModel = new entityMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("pendingAccountVerificationList?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                entityMasterViewModel = JsonConvert.DeserializeObject<entityMasterViewModel>(response.Content);
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                entityMasterViewModel = new mdViewModel();
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}