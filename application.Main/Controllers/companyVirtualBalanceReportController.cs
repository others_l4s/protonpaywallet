﻿using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class companyVirtualBalanceReportController : Controller
    {
        // GET: companyVirtualBalanceReport
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getVirtualBalanceReport(string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            companyAccountVM objcompanyAccount = new companyAccountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getVirtualBalanceAccount?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objcompanyAccount = JsonConvert.DeserializeObject<companyAccountVM>(webAPIResponse.Content);
                return Json(new { data = objcompanyAccount.objCompanyAccountList, draw = Request["draw"], recordsTotal = objcompanyAccount.totalRecords, recordsFiltered = objcompanyAccount.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objcompanyAccount = new companyAccountVM();
                return Json(new { data = objcompanyAccount.objCompanyAccountList, draw = Request["draw"], recordsTotal = objcompanyAccount.totalRecords, recordsFiltered = objcompanyAccount.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}