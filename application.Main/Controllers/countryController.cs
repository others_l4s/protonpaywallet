﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class countryController : Controller
    {
        // GET: country
        public ActionResult Index(int id = 0)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            tblCountry_Master objCountry = new tblCountry_Master();
            IRestResponse statusResponse = Helper.WebAPIClient_via_RestSharp("status", Method.GET, null, accessToken);
            commonStatusList objCommonStatusList = Helper.getStatusList(statusResponse);
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("country/" + id, Method.GET, null, accessToken);
                objCountry = JsonConvert.DeserializeObject<tblCountry_Master>(response1.Content);
            }
            else
            {
                objCountry.StatusId = objCommonStatusList.defaultActiveStatusId;
            }

            var countryMasterViewModel = new countryMasterViewModel
            {
                objCountryMaster = objCountry,
                objStatusMasterList = objCommonStatusList.objStatusList
            };
            return View("index", countryMasterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCountry(countryMasterViewModel objCountryViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objCountryViewModel.objCountryMaster.CountryId == 0)
                {
                    if (!string.IsNullOrEmpty(objCountryViewModel.objCountryMaster.CountryName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("country", Method.POST, objCountryViewModel.objCountryMaster, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/country") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StatusName", "1|Please enter Status Name");
                        return Json(new { success = false, responseText = "1|Please enter Status Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("country/" + objCountryViewModel.objCountryMaster.CountryId, Method.PUT, objCountryViewModel.objCountryMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/country") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteCountry(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);

            IRestResponse response = Helper.WebAPIClient_via_RestSharp("country/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getCountryList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            countryMasterViewModel countryList = new countryMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("country?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                countryList = JsonConvert.DeserializeObject<countryMasterViewModel>(response.Content);
            }
            return Json(new { data = countryList.objCountryMasterList, draw = Request["draw"], recordsTotal = countryList.totalRecords, recordsFiltered = countryList.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}