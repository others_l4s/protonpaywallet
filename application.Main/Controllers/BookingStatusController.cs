﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using RestSharp;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class BookingStatusController : Controller
    {
        // GET: BookingStatus
        public ActionResult Index(int Id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            bookingStatusViewModel objbookingstatusviewmodel = new bookingStatusViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus", Method.GET, null, accessToken);
            objbookingstatusviewmodel = JsonConvert.DeserializeObject<bookingStatusViewModel>(webAPIResponse.Content);
            if (Id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus/" + Id, Method.GET, null, accessToken);
                objbookingstatusviewmodel.objBookingStatusMaster = JsonConvert.DeserializeObject<tblBookingStatus_Master>(webAPIResponse.Content);
            }
            else
            {
                objbookingstatusviewmodel.objBookingStatusMaster.StatusId = objbookingstatusviewmodel.objStatusMasterList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
            }

            var bookstatusViewModel = new bookingStatusViewModel
            {

                objStatusMasterList = objbookingstatusviewmodel.objStatusMasterList,
                objBookingStatusMaster = objbookingstatusviewmodel.objBookingStatusMaster
            };

            return View(bookstatusViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveBookingStatus(bookingStatusViewModel objbookingstatusviewmodel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse webAPIResponse;
            if (ModelState.IsValid)
            {
                if (objbookingstatusviewmodel.objBookingStatusMaster.BookingStatusId == 0)
                {
                    if (!string.IsNullOrEmpty(objbookingstatusviewmodel.objBookingStatusMaster.BookingStatusName) && objbookingstatusviewmodel.objBookingStatusMaster.BookingStatusId == 0 && objbookingstatusviewmodel.objBookingStatusMaster.StatusId != 0)
                    {
                        webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus", Method.POST, objbookingstatusviewmodel.objBookingStatusMaster, accessToken);
                        if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/bookingstatus") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("BookingStatusName", "Please enter BookingStatus Name");
                        return Json(new { success = false, responseText = "1|Please enter BookingStatus Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus/" + objbookingstatusviewmodel.objBookingStatusMaster.BookingStatusId, Method.PUT, objbookingstatusviewmodel.objBookingStatusMaster, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/bookingstatus") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteBookingStatus(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getBookingStatusList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            bookingStatusViewModel BookingStatusList = new bookingStatusViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BookingStatus?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                BookingStatusList = JsonConvert.DeserializeObject<bookingStatusViewModel>(webAPIResponse.Content);
            }
            return Json(new { data = BookingStatusList.objBookingStatusMasterList, draw = Request["draw"], recordsTotal = BookingStatusList.totalRecords, recordsFiltered = BookingStatusList.filterRecords }, JsonRequestBehavior.AllowGet);
        }

    }
}