﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using application.Main.Models;
using application.Main.ViewModels;

namespace application.Main.Controllers
{
    public class companyBankDetailsController : Controller
    {
        // GET: companyBankDetails
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            companyBankDetailsViewModel objCompanyBankDetailsViewModel = new companyBankDetailsViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objCompanyBankDetailsViewModel = JsonConvert.DeserializeObject<companyBankDetailsViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails/" + id, Method.GET, null, accessToken);
                objCompanyBankDetailsViewModel.objCompanyBankDetails = JsonConvert.DeserializeObject<tblCompany_Bank_Details>(webAPIResponse.Content);
            }
            else
            {
                objCompanyBankDetailsViewModel.objCompanyBankDetails.StatusId = (objCompanyBankDetailsViewModel.objStatusList.Count() > 0 ? objCompanyBankDetailsViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objCompanyBankDetailsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCompanyBankDetails(companyBankDetailsViewModel objCompanyBankViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse webAPIResponse;
            if (objCompanyBankViewModel.objCompanyBankDetails.BankId == 0)
            {
                if (!string.IsNullOrEmpty(objCompanyBankViewModel.objCompanyBankDetails.BankName))
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails", Method.POST, objCompanyBankViewModel.objCompanyBankDetails, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/companyBankDetails") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter Bank Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails/" + objCompanyBankViewModel.objCompanyBankDetails.BankId, Method.PUT, objCompanyBankViewModel.objCompanyBankDetails, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/companyBankDetails") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult deleteCompanyBankDetails(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getCompanyBankDetails()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            companyBankDetailsViewModel companyBankDetailsViewModel = new companyBankDetailsViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("companyBankDetails?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companyBankDetailsViewModel = JsonConvert.DeserializeObject<companyBankDetailsViewModel>(webAPIResponse.Content);
                return Json(new { data = companyBankDetailsViewModel.objCompanyBankDetailsList, draw = Request["draw"], recordsTotal = companyBankDetailsViewModel.totalRecords, recordsFiltered = companyBankDetailsViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                companyBankDetailsViewModel = new companyBankDetailsViewModel();
                return Json(new { data = companyBankDetailsViewModel.objCompanyBankDetailsList, draw = Request["draw"], recordsTotal = companyBankDetailsViewModel.totalRecords, recordsFiltered = companyBankDetailsViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}