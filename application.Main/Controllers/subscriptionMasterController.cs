﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class subscriptionMasterController : Controller
    {
        // GET: subscriptionMaster
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            subscriptionMasterViewModel objSubscriptionMasterViewModel = new subscriptionMasterViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("subscriptionMaster", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSubscriptionMasterViewModel = JsonConvert.DeserializeObject<subscriptionMasterViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("subscriptionMaster/" + id, Method.GET, null, accessToken);
                objSubscriptionMasterViewModel.objSubscriptionMaster = JsonConvert.DeserializeObject<tblSubscription_Master>(response1.Content);
            }
            else
            {
                objSubscriptionMasterViewModel.objSubscriptionMaster.StatusId = (objSubscriptionMasterViewModel.objStatusList.Count() > 0 ? objSubscriptionMasterViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objSubscriptionMasterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSubscription(subscriptionMasterViewModel objSubscriptionMasterViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionRemark = (objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionRemark == null ? "" : objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionRemark);
                if (objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionId == 0)
                {
                    if (!string.IsNullOrEmpty(objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("subscriptionMaster", Method.POST, objSubscriptionMasterViewModel.objSubscriptionMaster, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/subscriptionmaster") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StatusName", "Please enter Subscription Name");
                        return Json(new { success = false, responseText = "1|Please enter Subscription Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("subscriptionMaster/" + objSubscriptionMasterViewModel.objSubscriptionMaster.SubscriptionId, Method.PUT, objSubscriptionMasterViewModel.objSubscriptionMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/subscriptionmaster") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSubscription(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("subscriptionMaster/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getSubscriptionList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            subscriptionMasterViewModel subscriptionViewModel = new subscriptionMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("subscriptionMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                subscriptionViewModel = JsonConvert.DeserializeObject<subscriptionMasterViewModel>(response.Content);
            }
            return Json(new { data = subscriptionViewModel.objSubscriptionList, draw = Request["draw"], recordsTotal = subscriptionViewModel.totalRecords, recordsFiltered = subscriptionViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}