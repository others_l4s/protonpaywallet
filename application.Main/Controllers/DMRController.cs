﻿using application.Main.BankITDMT.Request;
using application.Main.BankITDMT.Response;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class DMRController : Controller
    {
        // GET: DMR

        #region Sender
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addNewSender(string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                DMT_BankItInDB_Sender clsBankItSender = new DMT_BankItInDB_Sender();
                clsBankItSender.CustomerID = mobileNo;
                return View("addSender", clsBankItSender);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSender(DMT_BankItInDB_Sender objBankITDMTSender)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objBankITDMTSender.BankitSenderId == 0)
            {
                if (!string.IsNullOrEmpty(objBankITDMTSender.CustomerID))
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT", Method.POST, objBankITDMTSender, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/DMR/beneficiary?mobileNo=" + objBankITDMTSender.CustomerID) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter CustomerID");
                    return Json(new { success = false, responseText = "1|Please enter CustomerID" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/" + objBankITDMTSender.BankitSenderId, Method.PUT, objBankITDMTSender, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/DMR/beneficiary") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public JsonResult getSenderList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITSenderViewModel objBankITSenderVM = new BankITSenderViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITSenderVM = JsonConvert.DeserializeObject<BankITSenderViewModel>(response.Content);
            }
            return Json(new { data = objBankITSenderVM.clsBankItSenderList, draw = Request["draw"], recordsTotal = objBankITSenderVM.totalRecords, recordsFiltered = objBankITSenderVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult verifyMobileOrAccountNumber(DMT_addSender_request objSenderRequest)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);

            if (String.IsNullOrEmpty(objSenderRequest.accountNumber) && string.IsNullOrEmpty(objSenderRequest.customerId))
                return Json(new { success = false, responseText = "1|Please enter Mobile Number or Account Number." }, JsonRequestBehavior.AllowGet);

            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/verifySenderMobileNoOrAccountNumberInDb", Method.POST, objSenderRequest, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (response.Content.Contains("Error:"))
                {
                    if (!String.IsNullOrEmpty(objSenderRequest.customerId))
                    {
                        DMT_BankItInDB_Sender objBankITSender = new DMT_BankItInDB_Sender();
                        objBankITSender.MobileNo = objSenderRequest.customerId;
                        IRestResponse VerifySenderMobileResponse = Helper.WebAPIClient_via_RestSharp("bankITDMT/verifySenderMobileOTP", Method.POST, objBankITSender, accessToken);
                        if (VerifySenderMobileResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (VerifySenderMobileResponse.Content.Contains("Saved"))
                                return Json(new { success = "Saved", redirectURL = Url.Content("~/DMR") }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR/addNewSender?mobileNo=" + objSenderRequest.customerId) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "1|Sender Mobile No. not found. Please enter Mobile No." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string senderMobileNo = JsonConvert.DeserializeObject<String>(response.Content);
                    return Json(new { success = true, redirectURL = Url.Content("~/DMR/beneficiary?mobileNo=" + senderMobileNo) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult newSenderVerifyOTP(string mobileNo)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            DMT_BankItInDB_Sender objBankITSender = new DMT_BankItInDB_Sender();
            objBankITSender.MobileNo = mobileNo;
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/verifySenderMobileOTP", Method.POST, objBankITSender, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (response.Content.Contains("Saved"))
                    return Json(new { success = "Saved", redirectURL = Url.Content("~/DMR") }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, redirectURL = Url.Content("~/DMR/addNewSender?mobileNo=" + mobileNo) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Beneficiary
        public ActionResult beneficiary(string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                DMT_BankItInDB_Sender clsBankItSender = new DMT_BankItInDB_Sender();
                clsBankItSender.CustomerID = mobileNo;
                return View("listBeneficiary", clsBankItSender);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addBeneficiary(string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                AddBeneficiaryVM clsBeneficiaryRequest = new AddBeneficiaryVM();
                clsBeneficiaryRequest.objBeneficiary = new DMT_recipient_fetch_data_resp();
                clsBeneficiaryRequest.objBeneficiary.IMPSAdditionCharge = Convert.ToInt32(ConfigurationManager.AppSettings["IMPSBeneficiaryCharge"]);

                clsBeneficiaryRequest.objBeneficiary.customerId = mobileNo;
                clsBeneficiaryRequest.objBankList = new List<BankList>();
                clsBeneficiaryRequest.objTransferType = Helper.TransferType();
                return View("addBeneficiary", clsBeneficiaryRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult listBeneficiary(string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITRecipientViewModel objBankITRecipientVM = new BankITRecipientViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/recipientFetchAll?customerId=" + customerId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITRecipientVM = JsonConvert.DeserializeObject<BankITRecipientViewModel>(response.Content);
            }
            return Json(new { data = objBankITRecipientVM.clsRecipientResponseList, draw = Request["draw"], recordsTotal = objBankITRecipientVM.totalRecords, recordsFiltered = objBankITRecipientVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveBeneficiary(AddBeneficiaryVM objBeneficiaryReq)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objBeneficiaryReq.objBeneficiary.customerId != "0")
            {
                if (!string.IsNullOrEmpty(objBeneficiaryReq.objBeneficiary.recipientName))
                {
                    objBeneficiaryReq.objBeneficiary.IMPSAdditionCharge = Convert.ToInt32(ConfigurationManager.AppSettings["IMPSBeneficiaryCharge"]);
                    if (Helper.hasEnoughWalletBalance(objBeneficiaryReq.objBeneficiary.IMPSAdditionCharge, accessToken))
                    {
                        objBeneficiaryReq.objBeneficiary.bankName = objBeneficiaryReq.objBeneficiary.bankCode.Split('|')[0];
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/saveBeneficiaryDetails", Method.POST, objBeneficiaryReq.objBeneficiary, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR/beneficiary?mobileNo=" + objBeneficiaryReq.objBeneficiary.customerId) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            if (objCustomResponse.Message.Contains("SUCCESS"))
                            {
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR/beneficiary?mobileNo=" + objBeneficiaryReq.objBeneficiary.customerId) }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter Beneficiary Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult populateBankList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/getBankList", Method.POST, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    BankListVM objBankListVM = JsonConvert.DeserializeObject<BankListVM>(response.Content);
                    if (objBankListVM.errorCode == "00")
                    {
                        return Json(objBankListVM.data.bankList);
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                    return Json("unauthorized");
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public ActionResult fetchSenderDetail(string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                senderDetailRequest objSenderRequestDetail = new senderDetailRequest();
                objSenderRequestDetail.customerId = customerId;
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/getSenderDetail", Method.POST, objSenderRequestDetail, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    senderDetailResponse objSenderDetailResponse = JsonConvert.DeserializeObject<senderDetailResponse>(response.Content);
                    if (objSenderDetailResponse.errorCode == "00")
                    {
                        return PartialView("_senderAccountBalance", objSenderDetailResponse);
                    }
                    else
                    {
                        return PartialView("_senderAccountBalance");
                    }
                }
                else
                    return RedirectToAction("index", new { Controller = "unauthorized" });
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }

        public JsonResult deleteBeneficiary(string recipientId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/getBankList", Method.POST, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    BankListVM objBankListVM = JsonConvert.DeserializeObject<BankListVM>(response.Content);
                    if (objBankListVM.errorCode == "00")
                    {
                        return Json(objBankListVM.data.bankList);
                    }
                    else
                    {
                        return Json("");
                    }
                }
                else
                    return Json("unauthorized");
            }
            else
            {
                return Json("Unauthorized");
            }
        }
        #endregion

        #region Transfer Money

        public ActionResult Transac(string recipient, string customerId, string transferType)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                //Fetching Single Beneficiary Data by recipientId and Customer Id
                FetchSingleBeneficiaryRequest objFetchSingleBeneficiaryRequest = new FetchSingleBeneficiaryRequest();
                objFetchSingleBeneficiaryRequest.customerId = customerId;
                objFetchSingleBeneficiaryRequest.recipientId = recipient;
                FetchSingleBeneficiaryResponseBM objSingleBeneficiaryResponseVM = getTransacDetails(objFetchSingleBeneficiaryRequest, transferType, accessToken);
                if (objSingleBeneficiaryResponseVM != null)
                {
                    objSingleBeneficiaryResponseVM.transferType = transferType;
                    return View("transfer", objSingleBeneficiaryResponseVM);
                }
                else
                    return View("beneficiary", new { mobileNo = customerId });
            }
            else
                return Json("unauthorized");
        }

        public ActionResult TransacConfirmation(FetchSingleBeneficiaryResponseBM objTransferRequest)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                return View("transferconfirm", objTransferRequest);
            }
            else
                return Json("unauthorized");
        }

        private FetchSingleBeneficiaryResponseBM getTransacDetails(FetchSingleBeneficiaryRequest objFetchSingleBeneficiaryRequest, string transferType, string accessToken)
        {
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/getSingleBeneficiaryData", Method.POST, objFetchSingleBeneficiaryRequest, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                FetchSingleBeneficiaryResponseBM objSingleBeneficiaryResponseVM = JsonConvert.DeserializeObject<FetchSingleBeneficiaryResponseBM>(response.Content);
                if (objSingleBeneficiaryResponseVM.errorCode == "00")
                {
                    objSingleBeneficiaryResponseVM.objTransferType = (transferType == "ALL" ? Helper.TransferType() : Helper.TransferType().Where(x => x.TransType == "NEFT").ToList());
                    objSingleBeneficiaryResponseVM.objRemit = new MoneyTransferRequest();
                    objSingleBeneficiaryResponseVM.objRemit.recipientId = objFetchSingleBeneficiaryRequest.recipientId;
                    objSingleBeneficiaryResponseVM.objRemit.customerId = objFetchSingleBeneficiaryRequest.customerId;
                    objSingleBeneficiaryResponseVM.objRemit.recipientName = objSingleBeneficiaryResponseVM.data.recipientName;
                    objSingleBeneficiaryResponseVM.objRemit.bankName = objSingleBeneficiaryResponseVM.data.bankName;
                    objSingleBeneficiaryResponseVM.objRemit.clientRefId = Helper.bankITDMT_ClientRefId();
                    return objSingleBeneficiaryResponseVM;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }

        [HttpPost]
        public ActionResult transferMoneyConfirmation(FetchSingleBeneficiaryResponseBM objTransferRequest)
        {
            try
            {
                FetchSingleBeneficiaryRequest objFetchSingleBeneficiaryRequest = new FetchSingleBeneficiaryRequest();
                objFetchSingleBeneficiaryRequest.customerId = objTransferRequest.objRemit.customerId;
                objFetchSingleBeneficiaryRequest.recipientId = objTransferRequest.objRemit.recipientId;
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                FetchSingleBeneficiaryResponseBM objSingleBeneficiaryResponseVM = getTransacDetails(objFetchSingleBeneficiaryRequest, objTransferRequest.transferType, accessToken);
                if (objTransferRequest.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransferRequest.objRemit.amount) && Convert.ToDouble(objTransferRequest.objRemit.amount) >= 100)
                    {
                        if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransferRequest.objRemit.amount), accessToken))
                        {
                            double dTransferAmount = Convert.ToDouble(objTransferRequest.objRemit.amount) + (Convert.ToDouble(objTransferRequest.objRemit.amount) * 0.01);
                            objTransferRequest.objRemit.amount = dTransferAmount.ToString();
                            objSingleBeneficiaryResponseVM.objRemit.amount = objTransferRequest.objRemit.amount;
                            objSingleBeneficiaryResponseVM.objRemit.TransType = objTransferRequest.objRemit.TransType;
                            objSingleBeneficiaryResponseVM.objRemit.clientRefId = objTransferRequest.objRemit.clientRefId;
                            return View("transferconfirm", objSingleBeneficiaryResponseVM);
                        }
                        else { return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet); }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|Invalid Amount to Transfer. Minimum Amount to Transfer is 100";
                        return View("transfer", objSingleBeneficiaryResponseVM);
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary";
                    return View("transfer", objSingleBeneficiaryResponseVM);
                }
            }
            catch (Exception ex)
            {
                TempData[Helper.responseStatus] = "1|" + ex.Message.ToString();
                return View("transfer");
            }
        }

        private Boolean hasAvailableBalance(double amount, string accessToken)
        {
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/checkavailableBalance", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                double availableBalance = JsonConvert.DeserializeObject<double>(response.Content);
                if (availableBalance > amount)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transferMoney(FetchSingleBeneficiaryResponseBM objTransferRequest)
        {
            try
            {
                string transferRespnose = "";
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objTransferRequest.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransferRequest.objRemit.amount) && Convert.ToDouble(objTransferRequest.objRemit.amount) >= 100)
                    {
                        if (Convert.ToDouble(objTransferRequest.objRemit.amount) > 25000)
                            return Json(new { success = false, responseText = "1|Oops! Maximum Amount Transfer Limit is 25000" }, JsonRequestBehavior.AllowGet);

                        if (hasAvailableBalance(Convert.ToDouble(objTransferRequest.objRemit.amount), accessToken))
                        {
                            if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransferRequest.objRemit.amount), accessToken))
                            {
                                if (objTransferRequest.objRemit.TransType == "NEFT")
                                {
                                    objTransferRequest.objRemit.agentCode = "1";
                                    transferRespnose = transferViaNEFT(objTransferRequest.objRemit, accessToken);
                                }
                                else
                                {
                                    objTransferRequest.objRemit.agentCode = "1";
                                    transferRespnose = transferViaIMPS(objTransferRequest.objRemit, accessToken);
                                }
                                if (transferRespnose.Contains("success"))
                                    TempData[Helper.responseStatus] = "0|Transaction has been successfully processed";
                                else
                                    TempData[Helper.responseStatus] = "1|" + transferRespnose;
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR/showTransactions?mobileNo=" + objTransferRequest.objRemit.customerId) }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Oops! Transfer Balance is too Low. Please contact Administrator" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        protected string transferViaNEFT(MoneyTransferRequest objRemit, string accessToken)
        {
            customResponse objCustomResponse = new customResponse();
            objRemit.fromMobileDevice = false;
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/transacNEFT", Method.POST, objRemit, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //return "0|~/DMR/showTransactions?mobileNo=" + objRemit.customerId;
                string transferResponse = JsonConvert.DeserializeObject<string>(response.Content);
                return transferResponse;
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode;
            }
        }

        protected string transferViaIMPS(MoneyTransferRequest objRemit, string accessToken)
        {
            customResponse objCustomResponse = new customResponse();
            objRemit.fromMobileDevice = false;
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/transacIMPS", Method.POST, objRemit, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                string transferResponse = JsonConvert.DeserializeObject<string>(response.Content);
                //return "0|~/DMR/showTransactions?mobileNo=" + objRemit.customerId;
                return transferResponse;
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode;
            }
        }

        #endregion

        #region  Transaction

        public ActionResult showTransactions(string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                DMT_BankItInDB_Sender clsBankItSender = new DMT_BankItInDB_Sender();
                clsBankItSender.CustomerID = mobileNo;
                return View("transactions", clsBankItSender);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult showTransactionReceipt(long id)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (id > 0)
                {
                    tblBankIt_Transaction objTransactionReceipt = new tblBankIt_Transaction();
                    BankItTransactionVM objTransactions = new BankItTransactionVM();
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/transactionsReceipt?BankItTransactionId=" + id, Method.GET, null, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objTransactions = JsonConvert.DeserializeObject<BankItTransactionVM>(response.Content);
                        foreach (tblBankIt_Transaction objData in objTransactions.objBankITDMTTransactionsList)
                        {
                            objTransactionReceipt = objData;
                        }

                        moneyTransferResponse objMoneyTransferResponse = JsonConvert.DeserializeObject<moneyTransferResponse>(objTransactionReceipt.ResponseApi);
                        if (objMoneyTransferResponse != null)
                            objTransactionReceipt.accountNumber = objMoneyTransferResponse.data.accountNumber;
                        return View("transactionReceipt", objTransactionReceipt);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|Unable to Fetch Data";
                        return View("transactions");
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Invalid Client RefId";
                    return View("transactions");
                }
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult listTransactions(string customerId, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankItTransactionVM objTransactions = new BankItTransactionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/transactions?customerId=" + customerId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankItTransactionVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bank Status

        public string bankStatus(string bankName, string bankCode, string status)
        {
            return "Bank Name:" + bankName;
        }

        #endregion

        #region check pending transaction

        public string checkPendingTransaction(string ClientRefID, string Status, string BankRefNo)
        {
            return "Status:" + Status;
        }

        #endregion

        /// <summary>
        /// This will show DMT Transaction History done by nimesh on 05 06 2020
        /// </summary>
        /// <returns></returns>
        #region Show Transaction History
        public ActionResult transactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                return View("history");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showTransactionHistory(string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankItTransactionVM objTransactions = new BankItTransactionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/transactionsHistory?searchEntityId=0&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankItTransactionVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}