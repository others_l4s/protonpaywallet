﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Fluentx.Mvc;

namespace application.Main.Controllers
{
    public class utilityPaymentController : Controller
    {
        public Dictionary<string, string> parameterList { get; private set; } = new Dictionary<string, string>();
        // GET: utilityPayment
        public ActionResult Index()
        {
            string result;

            // Order of Keys is important. Don't change it.
            parameterList.Add("ALLOWED_SERVICES", "");
            parameterList.Add("APP_ID", "310");
            parameterList.Add("BLOCKED_SERVICES", "");
            parameterList.Add("LANDING_SERVICE", ""); // optional, service to show first after login
            parameterList.Add("PAN", "AAHCV3636R");

            string checksumString = "";
            foreach (var item in parameterList)
            {
                if (checksumString != "")
                {
                    checksumString += "|";
                }
                checksumString += (item.Key) + ":" + (item.Value);
            }

            string iv = "1234567812345678";
            checksumString = iv + checksumString;
            string keyhmac = "3b96e13ed3b3759863d96a3625557332a6b1706652a478918d575a755db92eb0";
            byte[] _keyhmac = Encoding.UTF8.GetBytes(keyhmac);
            string key = keyhmac.Substring(0, 16);

            byte[] _plainText = Encoding.UTF8.GetBytes(checksumString);
            byte[] _key = Encoding.UTF8.GetBytes(key);
            byte[] _iv = Encoding.UTF8.GetBytes(iv);

            byte[] output_buffer = new byte[_plainText.Length];

            using (AesManaged aesAlg = new AesManaged())
            {
                //If CBC, must initialize IV = O_{128}
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.IV = _iv;
                //aesAlg.Mode = CipherMode.ECB;

                aesAlg.BlockSize = 128;
                aesAlg.KeySize = 128;
                //aesAlg.Padding = PaddingMode.None;

                aesAlg.Key = _key;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                output_buffer = encryptor.TransformFinalBlock(_plainText, 0, _plainText.Length);
            }

            Byte[] hashBytes;

            using (HMACSHA256 hash = new HMACSHA256(_keyhmac))
                hashBytes = hash.ComputeHash(output_buffer);

            Byte[] b = Combine(_iv, hashBytes, output_buffer);

            parameterList.Add("CHECKSUMHASH", Convert.ToBase64String(b));

            // POST the parameterList to the CNAME with name = params
            result = Newtonsoft.Json.JsonConvert.SerializeObject(parameterList);
            result = WebUtility.UrlEncode(result);
            Dictionary<string, object> postData = new Dictionary<string, object>();
            postData.Add("params", result);
            return this.RedirectAndPost("https://prime.protonpay.net", postData);
        }

        public static byte[] Combine(byte[] first, byte[] second, byte[] third)
        {
            byte[] ret = new byte[first.Length + second.Length + third.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            Buffer.BlockCopy(third, 0, ret, first.Length + second.Length,
                            third.Length);
            return ret;
        }
    }
}