﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class DepartmentController : Controller
    {
        // GET: Department
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            departmentViewModel objDepartmentViewModel = new departmentViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("Department", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objDepartmentViewModel = JsonConvert.DeserializeObject<departmentViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("Department/" + id, Method.GET, null, accessToken);
                objDepartmentViewModel.objDepartment = JsonConvert.DeserializeObject<tblDepartment_Master>(response1.Content);
            }
            else
            {
                objDepartmentViewModel.objDepartment.StatusId = (objDepartmentViewModel.objStatusList.Count() > 0 ? objDepartmentViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objDepartmentViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveDepartment(departmentViewModel objDepartmentViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objDepartmentViewModel.objDepartment.DepartmentId == 0)
                {
                    if (!string.IsNullOrEmpty(objDepartmentViewModel.objDepartment.DepartmentName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("Department", Method.POST, objDepartmentViewModel.objDepartment, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/department") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("DepartmentName", "Please enter Department Name");
                        return Json(new { success = false, responseText = "1|Please enter Department Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("Department/" + objDepartmentViewModel.objDepartment.DepartmentId, Method.PUT, objDepartmentViewModel.objDepartment, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/department") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteDepartment(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Department/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getDepartmentList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            departmentViewModel departmentViewModel = new departmentViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Department?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                departmentViewModel = JsonConvert.DeserializeObject<departmentViewModel>(response.Content);
                return Json(new { data = departmentViewModel.objDepartmentList, draw = Request["draw"], recordsTotal = departmentViewModel.totalRecords, recordsFiltered = departmentViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                departmentViewModel = new departmentViewModel();
                return Json(new { data = departmentViewModel.objDepartmentList, draw = Request["draw"], recordsTotal = departmentViewModel.totalRecords, recordsFiltered = departmentViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}