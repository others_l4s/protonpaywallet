﻿using application.Main.Models;
using Fluentx.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class androidWebViewController : Controller
    {
        // GET: androidWebView
        public ActionResult Index(long entityId)
        {
            primeURLs objPrimeURLs = new primeURLs();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("prime/accessPrimeAndroidWebView?entityId=" + entityId, Method.GET, null, "");
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objPrimeURLs = JsonConvert.DeserializeObject<primeURLs>(webAPIResponse.Content);
                Dictionary<string, object> postData = new Dictionary<string, object>();
                postData.Add("params", objPrimeURLs.primePostData);
                return this.RedirectAndPost("https://prime.protonpay.net", postData);
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }
    }
}