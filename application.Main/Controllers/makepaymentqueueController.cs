﻿using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class makepaymentqueueController : Controller
    {
        // GET: makepaymentqueue
        public ActionResult paymentQueue()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult HO()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("ho");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getPaymentQueueList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            makePaymentVM objMakePaymentVM = new makePaymentVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("paymentQueue?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMakePaymentVM = JsonConvert.DeserializeObject<makePaymentVM>(webAPIResponse.Content);
                return Json(new { data = objMakePaymentVM.objEntityPaymentList, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMakePaymentVM = new makePaymentVM();
                return Json(new { data = objMakePaymentVM.objEntityPaymentList, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult getPaymentQueueList4Company()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            makePaymentVM objMakePaymentVM = new makePaymentVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("paymentQueueHO?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMakePaymentVM = JsonConvert.DeserializeObject<makePaymentVM>(webAPIResponse.Content);
                return Json(new { data = objMakePaymentVM.objEntityPaymentListHO, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMakePaymentVM = new makePaymentVM();
                return Json(new { data = objMakePaymentVM.objEntityPaymentListHO, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}