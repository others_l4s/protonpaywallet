﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class SupportSystemProblemController : Controller
    {
        // GET: SupportSystemProblem
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            supportSystemProblemViewModel objsupportSystemProblemViewModel = new supportSystemProblemViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objsupportSystemProblemViewModel = JsonConvert.DeserializeObject<supportSystemProblemViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster/" + id, Method.GET, null, accessToken);
                objsupportSystemProblemViewModel.objSupportSystemProblem = JsonConvert.DeserializeObject<tblSupportSystemProblem_Master>(response1.Content);
            }
            else
            {
                objsupportSystemProblemViewModel.objSupportSystemProblem.StatusId = (objsupportSystemProblemViewModel.objStatusMasterList.Count() > 0 ? objsupportSystemProblemViewModel.objStatusMasterList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objsupportSystemProblemViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSupportSystemProblem(supportSystemProblemViewModel objsupportSystemProblemViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objsupportSystemProblemViewModel.objSupportSystemProblem.ProblemId == 0)
                {
                    if (!string.IsNullOrEmpty(objsupportSystemProblemViewModel.objSupportSystemProblem.ProblemName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster", Method.POST, objsupportSystemProblemViewModel.objSupportSystemProblem, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/supportsystemproblem") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("ProblemName", "Please enter Problem Name");
                        return Json(new { success = false, responseText = "1|Please enter Problem Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster/" + objsupportSystemProblemViewModel.objSupportSystemProblem.ProblemId, Method.PUT, objsupportSystemProblemViewModel.objSupportSystemProblem, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/supportsystemproblem") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSupportSystemProblem(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getSupportSystemProblemList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            supportSystemProblemViewModel objsupportSystemProblemViewModel = new supportSystemProblemViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("SupportSystemProblemMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objsupportSystemProblemViewModel = JsonConvert.DeserializeObject<supportSystemProblemViewModel>(response.Content);
            }
            return Json(new { data = objsupportSystemProblemViewModel.objSupportSystemProblemList, draw = Request["draw"], recordsTotal = objsupportSystemProblemViewModel.totalRecords, recordsFiltered = objsupportSystemProblemViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}