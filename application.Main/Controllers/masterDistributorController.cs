﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using application.Main.Models;
using application.Main.ViewModels;

namespace application.Main.Controllers
{
    public class masterDistributorController : Controller
    {
        // GET: masterDistributor
        public ActionResult Index(string type)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            mdViewModel objEntityMasterList = new mdViewModel();
            IRestResponse webAPIResponse;
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityMaster/" + id, Method.GET, null, accessToken);
                objEntityMasterList = JsonConvert.DeserializeObject<mdViewModel>(webAPIResponse.Content);
                if (objEntityMasterList.objEntityUser != null)
                    objEntityMasterList.objEntityUser.Password = Helper.unProtectString(objEntityMasterList.objEntityUser.Password);
            }
            else
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityMaster", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityMasterList = JsonConvert.DeserializeObject<mdViewModel>(webAPIResponse.Content);
                }
                objEntityMasterList.objEntityMaster.StatusId = (objEntityMasterList.objStatusList.Count() > 0 ? objEntityMasterList.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objEntityMasterList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveMasterDistributor(mdViewModel objentityMasterViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            Boolean recordSaved = false;
            tblEntity_Master objEntityMaster = new tblEntity_Master();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objentityMasterViewModel.objEntityMaster.EntityId == 0)
            {
                if (!string.IsNullOrEmpty(objentityMasterViewModel.objEntityMaster.AgencyName))
                {
                    objentityMasterViewModel.objEntityMaster.EntityTypeId = Convert.ToInt32(Helper.entityTypes.masterDistributor);
                    string validateEntityMasterDataBeforeInsertStatus = Helper.validateEntityMasterBeforeInsert(objentityMasterViewModel.objEntityMaster);
                    if (validateEntityMasterDataBeforeInsertStatus == "OK")
                    {
                        string validateEntityUserDataBeforeInsertStatus = Helper.validateEntityUserBeforeInsert(objentityMasterViewModel.objEntityUser);
                        if (validateEntityUserDataBeforeInsertStatus == "OK")
                        {
                            objentityMasterViewModel.objEntityMaster.IsBankVerified = false;
                            IRestResponse response = Helper.WebAPIClient_via_RestSharp("saveMasterDistributor", Method.POST, objentityMasterViewModel.objEntityMaster, accessToken);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                recordSaved = true;
                                objEntityMaster = JsonConvert.DeserializeObject<tblEntity_Master>(response.Content);
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = validateEntityUserDataBeforeInsertStatus }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = validateEntityMasterDataBeforeInsertStatus }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Agency Name");
                    return Json(new { success = false, responseText = "1|Please enter Agency Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityMaster/" + objentityMasterViewModel.objEntityMaster.EntityId, Method.PUT, objentityMasterViewModel.objEntityMaster, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    recordSaved = true;
                    objEntityMaster = JsonConvert.DeserializeObject<tblEntity_Master>(response.Content);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }

            if (recordSaved)
            {
                //Createing lead user 
                if (objEntityMaster != null)
                {
                    objentityMasterViewModel.objEntityUser.EntityId = objEntityMaster.EntityId;
                    objentityMasterViewModel.objEntityUser.StatusId = objentityMasterViewModel.objEntityMaster.StatusId;
                    webLoginVM objWebLoginVM = new webLoginVM();
                    objWebLoginVM = Helper.createEntityLeadUser(objentityMasterViewModel.objEntityUser);
                    if (objWebLoginVM.statusMsg == "OK")
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/masterdistributor") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = objWebLoginVM.statusMsg }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Oops something went wrong while creating Lead User" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Oops something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteMasterDistributor(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityMaster/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getMasterDistributorList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            mdViewModel entityMasterViewModel = new mdViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&EntityTypeId=" + Convert.ToInt32(Helper.entityTypes.masterDistributor), Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                entityMasterViewModel = JsonConvert.DeserializeObject<mdViewModel>(response.Content);
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                entityMasterViewModel = new mdViewModel();
                return Json(new { data = entityMasterViewModel.objEntityMasterList, draw = Request["draw"], recordsTotal = entityMasterViewModel.totalRecords, recordsFiltered = entityMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}