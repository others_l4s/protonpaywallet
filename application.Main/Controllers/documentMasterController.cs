﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class documentMasterController : Controller
    {
        // GET: documentMaster
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            documentMasterViewModel objdocumentMasterViewModel = new documentMasterViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("documentMaster", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objdocumentMasterViewModel = JsonConvert.DeserializeObject<documentMasterViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("documentMaster/" + id, Method.GET, null, accessToken);
                objdocumentMasterViewModel.objDocumentMaster = JsonConvert.DeserializeObject<tblDocument_Master>(response1.Content);
            }
            else
            {
                objdocumentMasterViewModel.objDocumentMaster.StatusId = (objdocumentMasterViewModel.objStatusList.Count() > 0 ? objdocumentMasterViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objdocumentMasterViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveDocumentMaster(documentMasterViewModel objdocumentMasterViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objdocumentMasterViewModel.objDocumentMaster.DocumentId == 0)
            {
                if (!string.IsNullOrEmpty(objdocumentMasterViewModel.objDocumentMaster.DocumentName))
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("documentMaster", Method.POST, objdocumentMasterViewModel.objDocumentMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/documentmaster") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter Document Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("documentMaster/" + objdocumentMasterViewModel.objDocumentMaster.DocumentId, Method.PUT, objdocumentMasterViewModel.objDocumentMaster, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, redirectURL = Url.Content("~/documentmaster") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult deleteDocumentMaster(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("documentMaster/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getDocumentMasterList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            documentMasterViewModel documentMasterViewModel = new documentMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("documentMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                documentMasterViewModel = JsonConvert.DeserializeObject<documentMasterViewModel>(response.Content);
                return Json(new { data = documentMasterViewModel.objDocumentMasterList, draw = Request["draw"], recordsTotal = documentMasterViewModel.totalRecords, recordsFiltered = documentMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                documentMasterViewModel = new documentMasterViewModel();
                return Json(new { data = documentMasterViewModel.objDocumentMasterList, draw = Request["draw"], recordsTotal = documentMasterViewModel.totalRecords, recordsFiltered = documentMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}