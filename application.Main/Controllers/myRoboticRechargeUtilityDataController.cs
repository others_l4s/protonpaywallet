﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class myRoboticRechargeUtilityDataController : Controller
    {
        // GET: myRoboticRechargeUtilityData
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            myRoboticRechargeUtilityDataVM objMyRoboticRechargeUtilityData = new myRoboticRechargeUtilityDataVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMyRoboticRechargeUtilityData = JsonConvert.DeserializeObject<myRoboticRechargeUtilityDataVM>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData/" + id, Method.GET, null, accessToken);
                objMyRoboticRechargeUtilityData.objMyRoboticRechargeUtilityData = JsonConvert.DeserializeObject<tblMyRoboticRechargeUtilityData>(response1.Content);
            }
            objMyRoboticRechargeUtilityData.objUtilityTypeList = Helper.utilityType();
            return View("index", objMyRoboticRechargeUtilityData);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveUtilityData(myRoboticRechargeUtilityDataVM objMyRoboticRechargeUtilityDataVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityData.MyRoboticRechargeDataId == 0)
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData", Method.POST, objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityData, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/myRoboticRechargeUtilityData") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData/" + objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityData.MyRoboticRechargeDataId, Method.PUT, objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityData, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/myRoboticRechargeUtilityData") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteUtilityData(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getUtilityData()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            myRoboticRechargeUtilityDataVM objUtilityDataVM = new myRoboticRechargeUtilityDataVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("myRoboticRechargeUtilityData?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objUtilityDataVM = JsonConvert.DeserializeObject<myRoboticRechargeUtilityDataVM>(response.Content);
                if (objUtilityDataVM.objMyRoboticRechargeUtilityDataList.Count == 0)
                    objUtilityDataVM.objMyRoboticRechargeUtilityDataList = new List<tblMyRoboticRechargeUtilityData>();
            }
            return Json(new { data = objUtilityDataVM.objMyRoboticRechargeUtilityDataList, draw = Request["draw"], recordsTotal = objUtilityDataVM.totalRecords, recordsFiltered = objUtilityDataVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}