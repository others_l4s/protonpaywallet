﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class AnnouncementController : Controller
    {
        // GET: Announcement
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            IRestResponse webAPIResponse;
            AnnouncementViewModel objAnnouncementViewModel = new AnnouncementViewModel();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAnnouncementViewModel = JsonConvert.DeserializeObject<AnnouncementViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster/" + id, Method.GET, null, accessToken);
                objAnnouncementViewModel.objAnnouncementMaster = JsonConvert.DeserializeObject<tblAnnouncement_Master>(webAPIResponse.Content);
            }
            else
            {
                objAnnouncementViewModel.objAnnouncementMaster.StatusId = (objAnnouncementViewModel.objStatusMasterList.Count() > 0 ? objAnnouncementViewModel.objStatusMasterList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objAnnouncementViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveAnnouncement(AnnouncementViewModel objAnnouncementViewModel)
        {
            customResponse objCustomResponse;
            IRestResponse webAPIResponse;
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objAnnouncementViewModel.objAnnouncementMaster.AnnouncementId == 0)
                {
                    if (!string.IsNullOrEmpty(objAnnouncementViewModel.objAnnouncementMaster.AnnouncementText))
                    {
                        webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster", Method.POST, objAnnouncementViewModel.objAnnouncementMaster, accessToken);
                        if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/announcement") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("AnnouncementTitle", "Please enter Announcement Title");
                        return Json(new { success = false, responseText = "1|Please enter Announcement Title" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster/" + objAnnouncementViewModel.objAnnouncementMaster.AnnouncementId, Method.PUT, objAnnouncementViewModel.objAnnouncementMaster, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/announcement") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteAnnouncement(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getAnnouncementList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            AnnouncementViewModel objAnnouncementViewModel = new AnnouncementViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("AnnouncementMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAnnouncementViewModel = JsonConvert.DeserializeObject<AnnouncementViewModel>(webAPIResponse.Content);
                return Json(new { data = objAnnouncementViewModel.objAnnouncementMasterList, draw = Request["draw"], recordsTotal = objAnnouncementViewModel.totalRecords, recordsFiltered = objAnnouncementViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAnnouncementViewModel = new AnnouncementViewModel();
                return Json(new { data = objAnnouncementViewModel.objAnnouncementMasterList, draw = Request["draw"], recordsTotal = objAnnouncementViewModel.totalRecords, recordsFiltered = objAnnouncementViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}