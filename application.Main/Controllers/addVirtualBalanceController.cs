﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class addVirtualBalanceController : Controller
    {
        // GET: addVirtualBalance
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                tblCompany_Wallet objCompanyWallet = new tblCompany_Wallet();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getVirtualBalance", Method.GET, null, accessToken); //CompanyWalletController
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objCompanyWallet = JsonConvert.DeserializeObject<tblCompany_Wallet>(webAPIResponse.Content);
                }
                return View("index", objCompanyWallet);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveVirtualBalance(tblCompany_Wallet objCompanyWallet)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objCompanyWallet.WalletBalance > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("addVirtualBalance", Method.POST, objCompanyWallet, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json(new { success = true, responseText = "0|Virtual Balance successfully Added." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please add Virtual Balance" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}