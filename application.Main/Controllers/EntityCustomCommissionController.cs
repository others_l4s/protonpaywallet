﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class EntityCustomCommissionController : Controller
    {
        // GET: EntityCustomCommission
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            entityCommissionCustomViewModel objentityCommissionCustomViewModel = new entityCommissionCustomViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objentityCommissionCustomViewModel = JsonConvert.DeserializeObject<entityCommissionCustomViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom/" + id, Method.GET, null, accessToken);
                objentityCommissionCustomViewModel.objEntityCommisionCustomMaster = JsonConvert.DeserializeObject<tblEntityCommissionCustom_Master>(response1.Content);
            }
            else
            {
                objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.StatusId = (objentityCommissionCustomViewModel.objStatusMasterList.Count() > 0 ? objentityCommissionCustomViewModel.objStatusMasterList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);

            }
            return View("index", objentityCommissionCustomViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveEntityCommissionCustom(entityCommissionCustomViewModel objentityCommissionCustomViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.CommissionCustomId == 0)
                {
                    if (objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.CommissionValue >= 0 && objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.CommissionId != 0 && objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.EntityId != 0)
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom", Method.POST, objentityCommissionCustomViewModel.objEntityCommisionCustomMaster, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/entitycustomcommission") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "Please enter Commission Value" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom/" + objentityCommissionCustomViewModel.objEntityCommisionCustomMaster.CommissionCustomId, Method.PUT, objentityCommissionCustomViewModel.objEntityCommisionCustomMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/entitycustomcommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteEntityCommissionCustom(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getEntityCommissionCustomList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityCommissionCustomViewModel objentityCommissionCustomViewModel = new entityCommissionCustomViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("EntityCommissionCustom?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objentityCommissionCustomViewModel = JsonConvert.DeserializeObject<entityCommissionCustomViewModel>(response.Content);
                return Json(new { data = objentityCommissionCustomViewModel.objEntityCommisionCustomMasterList, draw = Request["draw"], recordsTotal = objentityCommissionCustomViewModel.totalRecords, recordsFiltered = objentityCommissionCustomViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objentityCommissionCustomViewModel = new entityCommissionCustomViewModel();
                return Json(new { data = objentityCommissionCustomViewModel.objEntityCommisionCustomMasterList, draw = Request["draw"], recordsTotal = objentityCommissionCustomViewModel.totalRecords, recordsFiltered = objentityCommissionCustomViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}