﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class companysubmenupermissionController : Controller
    {
        // GET: companysubmenupermission
        // GET: companyMenuPermission
        public ActionResult Index()
        {
            SubMenuCompanyPermissionVM objSubMenuCompanyPermissionVM = new SubMenuCompanyPermissionVM();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("company", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companyMasterViewModel objCompanyList = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
                objSubMenuCompanyPermissionVM.objCompanyList = objCompanyList.objCompanyList;
                objSubMenuCompanyPermissionVM.clsMainMenuList = new List<tblMainMenu_Master>();
            }
            return View("index", objSubMenuCompanyPermissionVM);
        }



        public ActionResult getSubMenuList(int companyId, int MainMenuId)
        {
            SubMenuCompanyPermissionVM objCompanyMenuPermission = new SubMenuCompanyPermissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseSubMenuRole = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission/getSubMenuCompanyMainMenuList?companyId=" + companyId + "&mainMenuId=" + MainMenuId, Method.GET, null, accessToken);
            if (webAPIResponseSubMenuRole.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objCompanyMenuPermission = JsonConvert.DeserializeObject<SubMenuCompanyPermissionVM>(webAPIResponseSubMenuRole.Content);
                objCompanyMenuPermission.CompanyId = companyId;
                objCompanyMenuPermission.MainMenuId = MainMenuId;
                return PartialView("_partial_companySubMenuPermission", objCompanyMenuPermission);
            }
            else
            {
                return PartialView("_partial_companySubMenuPermission");
            }
        }

        [HttpPost]
        public JsonResult saveSubMenuCompanyPermission(SubMenuCompanyPermissionVM subMenuCompanyPermission)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse bulkDeleteResponse = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission/deleteSubMenuByCompanyId?companyId=" + subMenuCompanyPermission.CompanyId + "&mainMenuId=" + subMenuCompanyPermission.MainMenuId, Method.DELETE, null, accessToken);
            if (bulkDeleteResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                tblSubMenu_Company_Permission objCompanyMenuPermission;
                foreach (tblSubMenu_Company_Permission objData in subMenuCompanyPermission.objSubMenuList)
                {
                    if (objData.CanView == true)
                    {
                        objCompanyMenuPermission = new tblSubMenu_Company_Permission();
                        objCompanyMenuPermission.MenuPermissionId = objData.MenuPermissionId;
                        objCompanyMenuPermission.CompanyId = subMenuCompanyPermission.CompanyId;
                        objCompanyMenuPermission.MainMenuId = subMenuCompanyPermission.MainMenuId;
                        objCompanyMenuPermission.CanView = objData.CanView;
                        objCompanyMenuPermission.CanAdd = false;
                        objCompanyMenuPermission.CanDelete = false;
                        objCompanyMenuPermission.CanEdit = false;
                        objCompanyMenuPermission.SubMenuId = objData.SubMenuId;
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission", Method.POST, objCompanyMenuPermission, accessToken);
                    }
                }

                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                customResponse objCustomResponse = new customResponse();
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(bulkDeleteResponse.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + bulkDeleteResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}