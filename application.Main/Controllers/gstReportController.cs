﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class gstReportController : Controller
    {
        // GET: gstReport
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                //entityReportVM objEntityReportVM = new entityReportVM();
                //IRestResponse objEntityResponse = Helper.WebAPIClient_via_RestSharp("entity/getEntityType4Report", Method.GET, null, accessToken);
                //if (objEntityResponse.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    objEntityReportVM = JsonConvert.DeserializeObject<entityReportVM>(objEntityResponse.Content);
                //}
                //objEntityReportVM.objEntityReport = new entityReport();
                return View("index");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getGSTReport(string fromDate, string toDate, string entityId = "0")
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            accountVM objAccountVM = new accountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getGSTReport?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&searchEntityId=" + entityId, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAccountVM = JsonConvert.DeserializeObject<accountVM>(webAPIResponse.Content);
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAccountVM = new accountVM();
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}