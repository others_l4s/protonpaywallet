﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class mdDashboardController : Controller
    {
        // GET: mdDashboard
        public ActionResult Index()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }
    }
}