﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.BankITAEPS.Request;
using application.Main.BankITAEPS.Response;
using Newtonsoft.Json;
using RestSharp;
using System.Configuration;
using application.Main.ViewModels;

namespace application.Main.Controllers
{
    public class aepsController : Controller
    {
        // GET: aeps
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblBankITAEPS_Request objAEPSRequest = new tblBankITAEPS_Request();
                IRestResponse apiResponse = Helper.WebAPIClient_via_RestSharp("getBankITAEPSRequest?id=0", Method.GET, null, accessToken);
                if (apiResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objAEPSRequest = JsonConvert.DeserializeObject<tblBankITAEPS_Request>(apiResponse.Content);
                    if (objAEPSRequest.RetailerId != null)
                    {
                        //Generating AEPS Token
                        getToken_Request objAEPSTokenRequest = new getToken_Request();
                        objAEPSTokenRequest.retailerId = objAEPSRequest.RetailerId;
                        objAEPSTokenRequest.agentAuthId = Helper.generateMD5HashString(objAEPSRequest.AgentAuthId);
                        objAEPSTokenRequest.agentAuthPassword = Helper.generateMD5HashString(objAEPSRequest.AgentAuthPassword);
                        objAEPSTokenRequest.apiId = objAEPSRequest.APIId;
                        IRestResponse tokenResponse = Helper.WebAPIBankIt_AEPS_via_RestSharp(objAEPSTokenRequest);
                        if (tokenResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            Get_Token_Response objresponse = JsonConvert.DeserializeObject<Get_Token_Response>(tokenResponse.Content);
                            if (objresponse.data != null)
                                objAEPSRequest.AEPSLIVEURL = ConfigurationManager.AppSettings["BankITAEPSURLLogin"] + "?token=" + objresponse.data.token;
                            else
                            {
                                TempData[Helper.responseStatus] = "1|" + objresponse.errorMsg;
                                return View("index", objAEPSRequest);
                            }
                        }
                    }
                }
                return View("index", objAEPSRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult requestAEPS(tblBankITAEPS_Request objAEPSRequest)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objAEPSRequest.AccessAmount), accessToken))
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BankITAEPSRequestSave", Method.POST, objAEPSRequest, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = "0|AEPS Request successfully placed.";
                    return Json(new { success = true, redirectURL = Url.Content("~/aeps") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This will show AEPS Transaction History done by nimesh on 05 06 2020
        /// </summary>
        /// <returns></returns>
        #region Show Transaction History
        public ActionResult AEPSTransactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                return View("AEPShistory");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult showAEPSTransactionHistory(string fromDate, string toDate, string transactionType)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("AEPS/transactionsHistory?searchEntityId=0&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<BankITAEPSTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objBankITAEPSTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}