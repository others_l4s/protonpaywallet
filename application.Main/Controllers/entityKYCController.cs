﻿using application.Main.ViewModels;
using application.Main.Models;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Configuration;

namespace application.Main.Controllers
{
    public class entityKYCController : Controller
    {
        // GET: entityKYC
        public ActionResult Index(long id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            entityKYCVM objEntityKYCVM = new entityKYCVM();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("getEntityKYC?Id=" + id, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objEntityKYCVM = JsonConvert.DeserializeObject<entityKYCVM>(webAPIResponse.Content);
                if (id > 0)
                    objEntityKYCVM.objEntityKYC.EntityId = id;
                if (objEntityKYCVM.objEntityKYC != null)
                {
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.PanCardPhoto))
                        objEntityKYCVM.objEntityKYC.PanCardPhoto = "../KYCImages/NoImages.png";
                    else
                    {
                        objEntityKYCVM.objEntityKYC.PanCardPhoto = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.PanCardPhoto;
                    }
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.PanCardPhoto2))
                        objEntityKYCVM.objEntityKYC.PanCardPhoto2 = "../KYCImages/NoImages.png";
                    else
                    {
                        objEntityKYCVM.objEntityKYC.PanCardPhoto2 = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.PanCardPhoto2;
                    }
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.GSTNoPhoto))
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto = "../KYCImages/NoImages.png";
                    else
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.GSTNoPhoto;
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.GSTNoPhoto2))
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto2 = "../KYCImages/NoImages.png";
                    else
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto2 = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.GSTNoPhoto2;
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.AadhaarCardPhoto))
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto = "../KYCImages/NoImages.png";
                    else
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.AadhaarCardPhoto;
                    if (String.IsNullOrEmpty(objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2))
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2 = "../KYCImages/NoImages.png";
                    else
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2 = (objEntityKYCVM.objEntityKYC.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["webAPIURL"] + "KYCImages/" : "../KYCImages/") + objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2;
                }
                else
                {
                    objEntityKYCVM = new entityKYCVM();
                    objEntityKYCVM.objEntityKYC = new tblEntityKYC();
                }
            }
            return View("index", objEntityKYCVM);
        }

        [HttpPost]
        public JsonResult saveKYC(entityKYCVM objEntityKYCVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                string panCardNoPhotoName = "", panCardNoPhotoExtension = "", panCardNoPhoto2Name = "", panCardNoPhoto2Extension = "";
                string GSTNoPhotoName = "", GSTNoPhotoExtension = "", GSTNoPhoto2Name = "", GSTNoPhoto2Extension = "";
                string aadhaarNoPhotoName = "", aadhaarNoPhotoExtension = "", aadhaarNoPhoto2Name = "", aadhaarNoPhoto2Extension = "";
                if (objEntityKYCVM.objEntityKYC.EntityKYCId == 0)
                {
                    if (objEntityKYCVM.HPPanCardPhoto != null)
                    {
                        panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPPanCardPhoto.FileName);
                        panCardNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPPanCardPhoto.FileName);
                        panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                        objEntityKYCVM.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                    }
                    else
                        return Json(new { success = false, responseText = "1|Please upload PanCard Photo Side 1" }, JsonRequestBehavior.AllowGet);

                    objEntityKYCVM.objEntityKYC.PanCardPhoto = panCardNoPhotoName;
                    if (objEntityKYCVM.HPPanCardPhoto2 != null)
                    {
                        panCardNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPPanCardPhoto2.FileName);
                        panCardNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPPanCardPhoto2.FileName);
                        panCardNoPhoto2Name = "pancard_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPPanCardPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhoto2Name));
                    }
                    else
                        return Json(new { success = false, responseText = "1|Please upload PanCard Photo Side 2" }, JsonRequestBehavior.AllowGet);

                    objEntityKYCVM.objEntityKYC.PanCardPhoto2 = panCardNoPhoto2Name;
                    if (objEntityKYCVM.HPGSTNoPhoto != null)
                    {
                        GSTNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPGSTNoPhoto.FileName);
                        GSTNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPGSTNoPhoto.FileName);
                        GSTNoPhotoName = "gst_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + GSTNoPhotoExtension.ToString();
                        objEntityKYCVM.HPGSTNoPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), GSTNoPhotoName));
                    }
                    objEntityKYCVM.objEntityKYC.GSTNoPhoto = GSTNoPhotoName;
                    if (objEntityKYCVM.HPGSTNoPhoto2 != null)
                    {
                        GSTNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPGSTNoPhoto2.FileName);
                        GSTNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPGSTNoPhoto2.FileName);
                        GSTNoPhoto2Name = "gst_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + GSTNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPGSTNoPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), GSTNoPhoto2Name));
                    }
                    objEntityKYCVM.objEntityKYC.GSTNoPhoto2 = GSTNoPhoto2Name;
                    if (objEntityKYCVM.HPAadhaarCardPhoto != null)
                    {
                        aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPAadhaarCardPhoto.FileName);
                        aadhaarNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPAadhaarCardPhoto.FileName);
                        aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                        objEntityKYCVM.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                    }
                    else
                        return Json(new { success = false, responseText = "1|Please upload Aadhaar Card Photo Side 1" }, JsonRequestBehavior.AllowGet);
                    objEntityKYCVM.objEntityKYC.AadhaarCardPhoto = aadhaarNoPhotoName;
                    if (objEntityKYCVM.HPAadhaarCardPhoto2 != null)
                    {
                        aadhaarNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPAadhaarCardPhoto2.FileName);
                        aadhaarNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPAadhaarCardPhoto2.FileName);
                        aadhaarNoPhoto2Name = "aadhaar_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPAadhaarCardPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhoto2Name));
                    }
                    objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2 = aadhaarNoPhoto2Name;

                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("saveEntityKYC", Method.POST, objEntityKYCVM.objEntityKYC, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = "0|KYC Document Successfully Uploaded";
                        return Json(new { success = true, responseText = "0|KYC Document Successfully Uploaded" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (objEntityKYCVM.HPPanCardPhoto != null)
                    {
                        panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPPanCardPhoto.FileName);
                        panCardNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPPanCardPhoto.FileName);
                        panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                        objEntityKYCVM.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                        objEntityKYCVM.objEntityKYC.PanCardPhoto = panCardNoPhotoName;
                    }

                    if (objEntityKYCVM.HPPanCardPhoto2 != null)
                    {
                        panCardNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPPanCardPhoto2.FileName);
                        panCardNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPPanCardPhoto2.FileName);
                        panCardNoPhoto2Name = "pancard_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPPanCardPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhoto2Name));
                        objEntityKYCVM.objEntityKYC.PanCardPhoto2 = panCardNoPhoto2Name;
                    }

                    if (objEntityKYCVM.HPGSTNoPhoto != null)
                    {
                        GSTNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPGSTNoPhoto.FileName);
                        GSTNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPGSTNoPhoto.FileName);
                        GSTNoPhotoName = "gst_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + GSTNoPhotoExtension.ToString();
                        objEntityKYCVM.HPGSTNoPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), GSTNoPhotoName));
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto = GSTNoPhotoName;
                    }

                    if (objEntityKYCVM.HPGSTNoPhoto2 != null)
                    {
                        GSTNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPGSTNoPhoto2.FileName);
                        GSTNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPGSTNoPhoto2.FileName);
                        GSTNoPhoto2Name = "gst_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + GSTNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPGSTNoPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), GSTNoPhoto2Name));
                        objEntityKYCVM.objEntityKYC.GSTNoPhoto2 = GSTNoPhoto2Name;
                    }

                    if (objEntityKYCVM.HPAadhaarCardPhoto != null)
                    {
                        aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPAadhaarCardPhoto.FileName);
                        aadhaarNoPhotoExtension = Path.GetExtension(objEntityKYCVM.HPAadhaarCardPhoto.FileName);
                        aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                        objEntityKYCVM.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto = aadhaarNoPhotoName;
                    }

                    if (objEntityKYCVM.HPAadhaarCardPhoto2 != null)
                    {
                        aadhaarNoPhoto2Name = Path.GetFileNameWithoutExtension(objEntityKYCVM.HPAadhaarCardPhoto2.FileName);
                        aadhaarNoPhoto2Extension = Path.GetExtension(objEntityKYCVM.HPAadhaarCardPhoto2.FileName);
                        aadhaarNoPhoto2Name = "aadhaar_side_2_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhoto2Extension.ToString();
                        objEntityKYCVM.HPAadhaarCardPhoto2.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhoto2Name));
                        objEntityKYCVM.objEntityKYC.AadhaarCardPhoto2 = aadhaarNoPhoto2Name;
                    }
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityKYC/" + objEntityKYCVM.objEntityKYC.EntityKYCId, Method.PUT, objEntityKYCVM.objEntityKYC, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = "0|KYC Document Successfully Updated";
                        return Json(new { success = true, responseText = "0|KYC Document Successfully Updated" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|Oops something went wrong, kindly contact Technical Support Team. Status Code :" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}