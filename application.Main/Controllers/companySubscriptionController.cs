﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class companySubscriptionController : Controller
    {
        // GET: companySubscription
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            companySubscriptionViewModel objCompanySubscription = new companySubscriptionViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objCompanySubscription = JsonConvert.DeserializeObject<companySubscriptionViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription/" + id, Method.GET, null, accessToken);
                objCompanySubscription.objCompanySubscription = JsonConvert.DeserializeObject<tblCompany_Subscription>(webAPIResponse.Content);
            }
            else
            {
                objCompanySubscription.objCompanySubscription.StatusId = (objCompanySubscription.objStatusList.Count() > 0 ? objCompanySubscription.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objCompanySubscription);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCompanySubscription(companySubscriptionViewModel objcompanySubscriptionViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse webAPIResponse;
            if (ModelState.IsValid)
            {
                if (objcompanySubscriptionViewModel.objCompanySubscription.CompanySubscriptionId == 0)
                {
                    if (objcompanySubscriptionViewModel.objCompanySubscription.SubscriptionId > 0 && objcompanySubscriptionViewModel.objCompanySubscription.CompanyId > 0)
                    {
                        webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription", Method.POST, objcompanySubscriptionViewModel.objCompanySubscription, accessToken);
                        if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/companySubscription") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("CompanyName", "Please select Subscription");
                        return Json(new { success = false, responseText = "Please select Subscription" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription/" + objcompanySubscriptionViewModel.objCompanySubscription.CompanySubscriptionId, Method.PUT, objcompanySubscriptionViewModel.objCompanySubscription, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/companySubscription") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteCompanySubscription(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getCompanySubscriptionList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            companySubscriptionViewModel companySubscriptionViewModel = new companySubscriptionViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("companySubscription?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companySubscriptionViewModel = JsonConvert.DeserializeObject<companySubscriptionViewModel>(webAPIResponse.Content);
                return Json(new { data = companySubscriptionViewModel.objCompanySubscriptionList, draw = Request["draw"], recordsTotal = companySubscriptionViewModel.totalRecords, recordsFiltered = companySubscriptionViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                companySubscriptionViewModel = new companySubscriptionViewModel();
                return Json(new { data = companySubscriptionViewModel.objCompanySubscriptionList, draw = Request["draw"], recordsTotal = companySubscriptionViewModel.totalRecords, recordsFiltered = companySubscriptionViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}