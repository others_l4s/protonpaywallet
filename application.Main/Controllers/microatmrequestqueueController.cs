﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class microatmrequestqueueController : Controller
    {
        // GET: microatmrequestqueue
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        public JsonResult getMicroATM_Request_Queue()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITMicroATMRequestQueueVM objBankITMicroATMRequestVM = new BankITMicroATMRequestQueueVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("getBnkITMicroATMRequestQueue?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITMicroATMRequestVM = JsonConvert.DeserializeObject<BankITMicroATMRequestQueueVM>(response.Content);
                return Json(new { data = objBankITMicroATMRequestVM.objBankItMicroATMRequestList, draw = Request["draw"], recordsTotal = objBankITMicroATMRequestVM.totalRecords, recordsFiltered = objBankITMicroATMRequestVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objBankITMicroATMRequestVM = new BankITMicroATMRequestQueueVM();
                return Json(new { data = objBankITMicroATMRequestVM.objBankItMicroATMRequestList, draw = Request["draw"], recordsTotal = objBankITMicroATMRequestVM.totalRecords, recordsFiltered = objBankITMicroATMRequestVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult updateResponse(long id)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblBankITMicroATM_Request objRequest = new tblBankITMicroATM_Request();
                IRestResponse apiResponse = Helper.WebAPIClient_via_RestSharp("getBankITMicroATMRequest?id=" + id, Method.GET, null, accessToken);
                if (apiResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objRequest = JsonConvert.DeserializeObject<tblBankITMicroATM_Request>(apiResponse.Content);
                }
                return View("processQueue", objRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult updateBankITResponse(tblBankITMicroATM_Request objRequest)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            if (objRequest.BankItMicroATMId > 0)
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BankItMicroATMRequest/" + objRequest.BankItMicroATMId, Method.PUT, objRequest, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = "0|Request successfully Activated.";
                    return Json(new { success = true, redirectURL = Url.Content("~/microatmrequestqueue") }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, responseText = "1|" + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { success = false, responseText = "1|Unable to update Response. MicroATMId missing" }, JsonRequestBehavior.AllowGet); }

        }
    }
}