﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class entityTypeController : Controller
    {
        // GET: entityType
        public ActionResult Index(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            
            entityTypeViewModel entityTypeVM = new entityTypeViewModel();
            IRestResponse listResponse = Helper.WebAPIClient_via_RestSharp("entityType", Method.GET, null, accessToken);
            entityTypeVM = JsonConvert.DeserializeObject<entityTypeViewModel>(listResponse.Content);
            if (id > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityType/" + id, Method.GET, null, accessToken);
                entityTypeVM.objEntityType = JsonConvert.DeserializeObject<tblEntity_Type>(response.Content);
            }
            else
            {
                entityTypeVM.objEntityType.StatusId = entityTypeVM.objStatusList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
            }
            var objentityTypeViewModel = new entityTypeViewModel
            {
                objStatusList = entityTypeVM.objStatusList,
                objEntityType = entityTypeVM.objEntityType,
                objEntityTypeList = entityTypeVM.objEntityTypeList
            };
            return View(objentityTypeViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveEntityStatus(tblEntity_Type objEntityType)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objEntityType.EntityTypeId == 0)
                {
                    if (!string.IsNullOrEmpty(objEntityType.EntityTypeName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityType", Method.POST, objEntityType, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/entitytype") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StatusName", "Please enter Status Name");
                        return Json(new { success = false, responseText = "1|Please enter Status Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityType/" + objEntityType.EntityTypeId, Method.PUT, objEntityType, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/entitytype") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteEntityType(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityType/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getEntityTypeList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityTypeViewModel objentityTypeViewModel = new entityTypeViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityType?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objentityTypeViewModel = JsonConvert.DeserializeObject<entityTypeViewModel>(response.Content);
            }
            return Json(new { data = objentityTypeViewModel.objEntityTypeList, draw = Request["draw"], recordsTotal = objentityTypeViewModel.totalRecords, recordsFiltered = objentityTypeViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}