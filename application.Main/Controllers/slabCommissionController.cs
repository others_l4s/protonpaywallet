﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;


namespace application.Main.Controllers
{
    public class slabCommissionController : Controller
    {
        // GET: slabCommission
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
                }
                return View("index", objEntitySlabCommission);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.GET, null, accessToken);
                objEntitySlabCommission.objEntitySlabCommission = JsonConvert.DeserializeObject<tblEntitySlabCommission>(response1.Content);
            }
            return View("index", objEntitySlabCommission);
        }

        public ActionResult getCommissionValue(long entityId, int productId)
        {
            Session["entityId"] = entityId;
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0&searchProductId=" + productId + "&searchEntityId=" + entityId, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSlabCommissionVM = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response.Content);
                objSlabCommissionVM.objEntitySlabcommissionList.Select(x => { x.DefaultCommissionValue = x.CommissionValue; return x; }).ToList();
                if (objSlabCommissionVM.objEntityDownlineSlabCommissionList.Count > 0)
                {
                    foreach (tblEntitySlabCommission objData in objSlabCommissionVM.objEntitySlabcommissionList)
                    {
                        tblEntitySlabCommission fromDownlineSlab = objSlabCommissionVM.objEntityDownlineSlabCommissionList.Where(x => x.ParentSlabCommissionId == objData.SlabCommissionId && x.EntityId == entityId).SingleOrDefault();
                        if (fromDownlineSlab != null)
                        {
                            objData.CommissionValue = fromDownlineSlab.CommissionValue;
                        }
                        else
                            objData.CommissionValue = 0;
                    }
                    //objSlabCommissionVM.objEntityDownlineSlabCommissionList = objSlabCommissionVM.objEntitySlabcommissionList;
                    //objSlabCommissionVM.objEntityDownlineSlabCommissionList.Select(x => { x.DefaultCommissionValue = x.CommissionValue; return x; }).ToList();
                }
                else
                {
                    objSlabCommissionVM.objEntitySlabcommissionList.Select(x => { x.DefaultCommissionValue = x.CommissionValue; return x; }).ToList();
                    objSlabCommissionVM.objEntitySlabcommissionList.Select(x => { x.CommissionValue = 0; return x; }).ToList();
                }
                return PartialView("_partial_slabWise_Commission", objSlabCommissionVM);
            }
            else
                return PartialView("_partial_slabWise_Commission");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSlabWiseCommission(entitySlabCommissionVM objEntitySlabCommissionVM)
        {
            try
            {
                if (Convert.ToInt64(Session["entityId"]) == 0)
                    return Json(new { success = false, responseText = "1|Please select Agency from given list" }, JsonRequestBehavior.AllowGet);

                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                //tblEntitySlabCommission objSlabCommission;
                foreach (tblEntitySlabCommission objData in objEntitySlabCommissionVM.objEntitySlabcommissionList)
                {
                    if (objData.CommissionValue > objData.DefaultCommissionValue)
                        throw new Exception("Commission value cannot exceed Default Commission value.");
                    objData.EntityId = Convert.ToInt64(Session["entityId"]);
                    objData.ParentSlabCommissionId = objData.SlabCommissionId;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission", Method.POST, objData, accessToken);
                }
                //TempData[Helper.responseStatus] = Helper.recordSaved;
                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSlab(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getDefaultCommission(int productId, long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0&searchProductId=" + productId + "&searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSlabCommissionVM = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response.Content);
                if (objSlabCommissionVM.objEntitySlabcommissionList.Count == 0)
                    objSlabCommissionVM.objEntitySlabcommissionList = new List<tblEntitySlabCommission>();
            }
            return Json(new { data = objSlabCommissionVM.objEntitySlabcommissionList, draw = Request["draw"], recordsTotal = objSlabCommissionVM.totalRecords, recordsFiltered = objSlabCommissionVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}