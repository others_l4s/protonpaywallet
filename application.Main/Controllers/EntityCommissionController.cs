﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class EntityCommissionController : Controller
    {
        // GET: EntityCommission
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=1", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.GET, null, accessToken);
                objEntitySlabCommission.objEntitySlabCommission = JsonConvert.DeserializeObject<tblEntitySlabCommission>(response1.Content);
            }
            return View("index", objEntitySlabCommission);
        }

        public ActionResult Custom()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                entityCommissionViewModel objEntityCommissionVM = new entityCommissionViewModel();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("entityCommissionCustom", Method.GET, null, accessToken);
                objEntityCommissionVM = JsonConvert.DeserializeObject<entityCommissionViewModel>(webAPIResponseMenu.Content);
                return View("custom", objEntityCommissionVM);
            }
            else
                return RedirectToAction("list", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSlabWiseCommission(entitySlabCommissionVM objEntitySlabCommissionVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objEntitySlabCommissionVM.objEntitySlabCommission.CommissionValue == 0)
                    return Json(new { success = false, responseText = "1|Commission value should be greater than zero(0)" }, JsonRequestBehavior.AllowGet);
                if (objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId == 0)
                {
                    objEntitySlabCommissionVM.objEntitySlabCommission.FromAmount = 0;
                    objEntitySlabCommissionVM.objEntitySlabCommission.ToAmount = 0;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission", Method.POST, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/entitycommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId, Method.PUT, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/entitycommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getDefaultCommissionList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=1&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSlabCommissionVM = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response.Content);
                if (objSlabCommissionVM.objEntitySlabcommissionList.Count == 0)
                    objSlabCommissionVM.objEntitySlabcommissionList = new List<tblEntitySlabCommission>();
            }
            return Json(new { data = objSlabCommissionVM.objEntitySlabcommissionList, draw = Request["draw"], recordsTotal = objSlabCommissionVM.totalRecords, recordsFiltered = objSlabCommissionVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}