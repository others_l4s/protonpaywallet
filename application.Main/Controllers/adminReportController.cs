﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class adminReportController : Controller
    {
        // GET: adminReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult wallet()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("walletHistory");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getEntityWalletState()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityWalletVM objEntityWalletList = new entityWalletVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getAgencyWalletList?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objEntityWalletList = JsonConvert.DeserializeObject<entityWalletVM>(webAPIResponse.Content);
                return Json(new { data = objEntityWalletList.objEntityWalletList, draw = Request["draw"], recordsTotal = objEntityWalletList.totalRecords, recordsFiltered = objEntityWalletList.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objEntityWalletList = new entityWalletVM();
                return Json(new { data = objEntityWalletList.objEntityWalletList, draw = Request["draw"], recordsTotal = objEntityWalletList.totalRecords, recordsFiltered = objEntityWalletList.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}