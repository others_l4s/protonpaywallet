﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class adminSlabCommissionController : Controller
    {
        // GET: adminSlabCommission
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
                }
                return View("list", objEntitySlabCommission);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.GET, null, accessToken);
                objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response1.Content);
            }
            else
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
                }
            }
            return View("index", objEntitySlabCommission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSlabWiseCommission(entitySlabCommissionVM objEntitySlabCommissionVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objEntitySlabCommissionVM.objEntitySlabCommission.CommissionValue == 0)
                    return Json(new { success = false, responseText = "1|Commission value should be greater than zero(0)" }, JsonRequestBehavior.AllowGet);
                if (objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId == 0)
                {
                    if (objEntitySlabCommissionVM.objEntitySlabCommission.FromAmount > objEntitySlabCommissionVM.objEntitySlabCommission.ToAmount)
                        return Json(new { success = false, responseText = "1|To Amount cannot be greater than From Amount" }, JsonRequestBehavior.AllowGet);
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission", Method.POST, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminslabcommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId, Method.PUT, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminslabcommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSlab(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getDefaultCommission(int productId, Int32 entityTypeId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0&searchProductId=" + productId + "&searchEntityTypeId=" + entityTypeId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSlabCommissionVM = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response.Content);
                if (objSlabCommissionVM.objEntitySlabcommissionList.Count == 0)
                    objSlabCommissionVM.objEntitySlabcommissionList = new List<tblEntitySlabCommission>();
            }
            return Json(new { data = objSlabCommissionVM.objEntitySlabcommissionList, draw = Request["draw"], recordsTotal = objSlabCommissionVM.totalRecords, recordsFiltered = objSlabCommissionVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        #region Custom Commission Setting by Admin

        public ActionResult customCommission()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
                }
                return View("customCommissionList", objEntitySlabCommission);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getCustomCommissionList(int productId, long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/getCustomCommissionList?searchProductId=" + productId + "&searchEntityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objSlabCommissionVM = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response.Content);
                if (objSlabCommissionVM.objEntitySlabcommissionList.Count == 0)
                    objSlabCommissionVM.objEntitySlabcommissionList = new List<tblEntitySlabCommission>();
            }
            return Json(new { data = objSlabCommissionVM.objEntitySlabcommissionList, draw = Request["draw"], recordsTotal = objSlabCommissionVM.totalRecords, recordsFiltered = objSlabCommissionVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult addEditCustom(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            entitySlabCommissionVM objEntitySlabCommission = new entitySlabCommissionVM();
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + id, Method.GET, null, accessToken);
                objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(response1.Content);
            }
            else
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entitySlabCommission?commissionBy=0", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntitySlabCommission = JsonConvert.DeserializeObject<entitySlabCommissionVM>(webAPIResponse.Content);
                }
            }
            objEntitySlabCommission.objEntitySlabCommission.IsCustom = true;
            return View("indexCustom", objEntitySlabCommission);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSlabWiseCustomCommission(entitySlabCommissionVM objEntitySlabCommissionVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objEntitySlabCommissionVM.objEntitySlabCommission.CommissionValue == 0)
                    return Json(new { success = false, responseText = "1|Commission value should be greater than zero(0)" }, JsonRequestBehavior.AllowGet);
                if (objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId == 0)
                {
                    if (objEntitySlabCommissionVM.objEntitySlabCommission.FromAmount > objEntitySlabCommissionVM.objEntitySlabCommission.ToAmount)
                        return Json(new { success = false, responseText = "1|To Amount cannot be greater than From Amount" }, JsonRequestBehavior.AllowGet);
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission", Method.POST, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminslabcommission/customCommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("entitySlabCommission/" + objEntitySlabCommissionVM.objEntitySlabCommission.SlabCommissionId, Method.PUT, objEntitySlabCommissionVM.objEntitySlabCommission, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminslabcommission/customCommission") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}