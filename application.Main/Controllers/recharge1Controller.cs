﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.ViewModels;
using application.Main.Models;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class recharge1Controller : Controller
    {
        // GET: recharge1
        public ActionResult Index()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            myRoboticRechargeVM objRechargeVM = new myRoboticRechargeVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("myRoboticRecharge/getRechargeServiceList?serviceType=PREPAID", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRechargeVM = JsonConvert.DeserializeObject<myRoboticRechargeVM>(webAPIResponse.Content);
            }
            objRechargeVM.Title = "Prepaid/PostPaid/DTH Recharge";
            return View("index", objRechargeVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transactRecharge(myRoboticRechargeVM objRecharge)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objRecharge.objMyRoboticRechargeTransaction.amount > 0)
                {
                    if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objRecharge.objMyRoboticRechargeTransaction.amount), accessToken))
                    {
                        IRestResponse rechargeReponse = Helper.WebAPIClient_via_RestSharp("myRoboticRecharge/rechargeMobile", Method.POST, objRecharge.objMyRoboticRechargeTransaction, accessToken);
                        if (rechargeReponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = "Transaction has been successfully processed.";
                            return Json(new { success = true, responseText = "0|Recharge Successfully Done" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(rechargeReponse.Content);
                            return Json(new { success = true, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}