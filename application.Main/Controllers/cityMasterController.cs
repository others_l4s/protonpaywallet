﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class cityMasterController : Controller
    {
        // GET: cityMaster
        public ActionResult Index(int id = 0)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            cityViewModel objCityViewModel = new cityViewModel();
            IRestResponse stateResponse = Helper.WebAPIClient_via_RestSharp("cityMaster", Method.GET, null, accessToken);
            objCityViewModel = JsonConvert.DeserializeObject<cityViewModel>(stateResponse.Content);
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("cityMaster/" + id, Method.GET, null, accessToken);
                objCityViewModel.objCity = JsonConvert.DeserializeObject<tblCity_Master>(response1.Content);
            }
            else
            {
                objCityViewModel.objCity.StatusId = objCityViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
            }

            var cityViewModel = new cityViewModel
            {
                objCity = objCityViewModel.objCity,
                objCityList = objCityViewModel.objCityList,
                objStateList = objCityViewModel.objStateList,
                objStatusList = objCityViewModel.objStatusList
            };
            return View("index", cityViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCity(cityViewModel objCityViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objCityViewModel.objCity.CityId == 0)
                {
                    if (!string.IsNullOrEmpty(objCityViewModel.objCity.CityName) && objCityViewModel.objCity.StateId != 0)
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("cityMaster", Method.POST, objCityViewModel.objCity, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/cityMaster") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Please enter City Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("cityMaster/" + objCityViewModel.objCity.CityId, Method.PUT, objCityViewModel.objCity, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/cityMaster") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteCity(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("cityMaster/" + id, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getCityList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);

            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            cityViewModel stateList = new cityViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("cityMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                stateList = JsonConvert.DeserializeObject<cityViewModel>(response.Content);
            }
            return Json(new { data = stateList.objCityList, draw = Request["draw"], recordsTotal = stateList.totalRecords, recordsFiltered = stateList.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}