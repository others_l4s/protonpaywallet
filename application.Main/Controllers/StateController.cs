﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class StateController : Controller
    {
        // GET: country
        public ActionResult Index(int id = 0)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            stateViewModel objStateViewModel = new stateViewModel();
            IRestResponse stateResponse = Helper.WebAPIClient_via_RestSharp("state", Method.GET, null, accessToken);
            objStateViewModel = JsonConvert.DeserializeObject<stateViewModel>(stateResponse.Content);
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("state/" + id, Method.GET, null, accessToken);
                objStateViewModel.objStateMaster = JsonConvert.DeserializeObject<tblState_Master>(response1.Content);
            }
            else
            {
                objStateViewModel.objStateMaster.StatusId = objStateViewModel.objStatusMasterList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
                objStateViewModel.objStateMaster.CountryId = 0;
            }

            var stateViewModel = new stateViewModel
            {
                objCountryMasterList = objStateViewModel.objCountryMasterList,
                objStatusMasterList = objStateViewModel.objStatusMasterList,
                objStateMaster = objStateViewModel.objStateMaster
            };
            return View("index", stateViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveState(stateViewModel objStateViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objStateViewModel.objStateMaster.StateId == 0)
                {
                    if (!string.IsNullOrEmpty(objStateViewModel.objStateMaster.StateName) && objStateViewModel.objStateMaster.CountryId != 0 && objStateViewModel.objStateMaster.StatusId != 0)
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("state", Method.POST, objStateViewModel.objStateMaster, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/state") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Please enter State Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("state/" + objStateViewModel.objStateMaster.StateId, Method.PUT, objStateViewModel.objStateMaster, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/state") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteState(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("state/" + id, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getStateList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            stateViewModel stateList = new stateViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("state?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                stateList = JsonConvert.DeserializeObject<stateViewModel>(response.Content);
            }
            return Json(new { data = stateList.objStateMasterList, draw = Request["draw"], recordsTotal = stateList.totalRecords, recordsFiltered = stateList.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}