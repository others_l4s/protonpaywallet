﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class manualDebitCreditController : Controller
    {
        // GET: manualDebitCredit
        public ActionResult Index(int id = 0)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                entityVM objEntityVM = new entityVM();
                IRestResponse entityVMResponse = Helper.WebAPIClient_via_RestSharp("getEntityVM", Method.GET, null, accessToken);
                if (entityVMResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityVM = JsonConvert.DeserializeObject<entityVM>(entityVMResponse.Content);
                }
                objEntityVM.objEntityList = new List<tblEntity_Master>();
                return View("list", objEntityVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            manualDebitCreditVM objManualDebitCreditVM = new manualDebitCreditVM();
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("manualDebitCredit/" + id, Method.GET, null, accessToken);
                objManualDebitCreditVM.objManualDebitCredit = JsonConvert.DeserializeObject<tblManual_DebitCredit>(response1.Content);
            }
            else
            {
                IRestResponse stateResponse = Helper.WebAPIClient_via_RestSharp("manualDebitCredit", Method.GET, null, accessToken);
                objManualDebitCreditVM = JsonConvert.DeserializeObject<manualDebitCreditVM>(stateResponse.Content);
                objManualDebitCreditVM.objEntityList = new List<tblEntity_Master>();
            }
            return View("index", objManualDebitCreditVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveManualDebitCreditEntry(manualDebitCreditVM objManualDebitCreditVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("manualDebitCredit", Method.POST, objManualDebitCreditVM.objManualDebitCredit, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                TempData[Helper.responseStatus] = Helper.recordSaved;
                return Json(new { success = true, redirectURL = Url.Content("~/manualDebitCredit") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getManualDebitCreditList(long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);

            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            manualDebitCreditVM objManualDebitCreditVM = new manualDebitCreditVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("manualDebitCredit?entityId=" + entityId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objManualDebitCreditVM = JsonConvert.DeserializeObject<manualDebitCreditVM>(response.Content);
            }
            return Json(new { data = objManualDebitCreditVM.objManualDebitCreditList, draw = Request["draw"], recordsTotal = objManualDebitCreditVM.totalRecords, recordsFiltered = objManualDebitCreditVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}