﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class loginController : Controller
    {
        // GET: login
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult validateCredentials(tblEntity_User objEntityUser)
        {
            if (objEntityUser.termsCondition)
            {
                tokenViewModel objToken = new tokenViewModel();
                customResponse objCustomResponse = new customResponse();
                errorDetail objError = new errorDetail();
                string headerHost = ConfigurationManager.AppSettings["webAPIURL"].Replace("http://", "").Replace("https://", "");
                var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"] + "validateuserlogin");
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", headerHost.Replace("/", ""));
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("bodyParam", "username=" + objEntityUser.UserName + "&password=" + objEntityUser.Password + "&grant_type=password", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    objToken = JsonConvert.DeserializeObject<tokenViewModel>(response.Content);
                    tblLogin_Session objLoginSession = new tblLogin_Session();
                    objLoginSession.accessToken = objToken.access_token;
                    objLoginSession.accessTokenValidity = Helper.getIndianTime();
                    objLoginSession.accessTokenValidity = objLoginSession.accessTokenValidity.AddDays(1);
                    objLoginSession.accessTokenGeneratedDateTime = Helper.getIndianTime();
                    objLoginSession.FromMobileDevice = false;
                    objLoginSession.IPAddress = Helper.GetIp(); //Convert.ToString(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);
                    IRestResponse restResponse = Helper.WebAPIClient_via_RestSharp("LoginSession", Method.POST, objLoginSession, objToken.access_token);
                    if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Helper.createEncryptedCookie(Helper.userAccessToken, objToken.access_token, true);
                        webLoginVM objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(restResponse.Content);
                        if (objWebLoginVM.OTPVerificationRequired)
                        {
                            Session["webLoginVM"] = restResponse.Content;
                            return Json(new { success = false, responseText = "1|OTP" }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            string dashboardURL = "";
                            if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.agent))
                                dashboardURL = Url.Content("~/retailerDashboard");
                            else if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.distributor))
                                dashboardURL = Url.Content("~/distributorDashboard");
                            else if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.masterDistributor))
                                dashboardURL = Url.Content("~/mdDashboard");
                            return Json(new { success = true, redirectURL = dashboardURL }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + restResponse.StatusCode.ToString() }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    string errorResponse = response.Content.ToString();
                    if (!errorResponse.Contains("<!DOCTYPE html>"))
                    {
                        objError = JsonConvert.DeserializeObject<errorDetail>(response.Content);
                        if (objError != null)
                        {
                            return Json(new { success = false, responseText = "1|" + objError.error_Description }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode.ToString() }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|" + Helper.apiErrorMsg }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please accept Terms and Conditions" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult validateLoginOTP(tblEntity_User objEntityUser)
        {
            mobileOTPVM objMobileOTPVM = new mobileOTPVM();
            if (objEntityUser.OTP.Trim().Length > 0)
            {
                string webLoginVM = Convert.ToString(Session["webLoginVM"]);
                if (webLoginVM == "")
                {
                    return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                    webLoginVM objWebLoginVM = new webLoginVM();
                    objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                    if (objWebLoginVM != null)
                    {
                        objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                        objMobileOTPVM.OTP = objEntityUser.OTP;
                        objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                        objMobileOTPVM.userName = objWebLoginVM.userName;
                        IRestResponse OTPVerificationResponse = Helper.WebAPIClient_via_RestSharp("verifyLoginOTP", Method.POST, objMobileOTPVM, accessToken);
                        if (OTPVerificationResponse.StatusCode == HttpStatusCode.OK)
                        {
                            string dashboardURL = "";
                            if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.agent))
                                dashboardURL = Url.Content("~/retailerDashboard");
                            else if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.distributor))
                                dashboardURL = Url.Content("~/distributorDashboard");
                            else if (objWebLoginVM.entityTypeId == Convert.ToInt32(Helper.entityTypes.masterDistributor))
                                dashboardURL = Url.Content("~/mdDashboard");
                            return Json(new { success = true, redirectURL = dashboardURL }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Invalid OTP" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please enter OTP. Value cannot be blank" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult resendLoginOTP()
        {
            string webLoginVM = Convert.ToString(Session["webLoginVM"]);
            if (webLoginVM == "")
            {
                return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mobileOTPVM objMobileOTPVM = new mobileOTPVM();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                webLoginVM objWebLoginVM = new webLoginVM();
                objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                if (objWebLoginVM != null)
                {
                    objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                    objMobileOTPVM.OTP = "";
                    objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                    objMobileOTPVM.userName = objWebLoginVM.userName;
                    IRestResponse resendOTPResponse = Helper.WebAPIClient_via_RestSharp("resendLoginOTP", Method.POST, objMobileOTPVM, accessToken);
                    if (resendOTPResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return Json(new { success = true, responseText = "OTP successfully resend" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Unable to send OTP. Please try again later" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult TermsConditions()
        {
            return View("termsConditions");
        }

        public ActionResult AboutUs()
        { return View("about"); }

        public ActionResult PrivacyPolicy()
        {
            return View("privacypolicy");
        }

        public ActionResult refundcancellation()
        {
            return View("refundcancellation");
        }

        #region Forgot Password

        public ActionResult forgotPassword()
        {
            return View("forgotPassword");
        }

        public JsonResult validateForgotPasswordCredentials(tblEntity_User objEntityUser)
        {
            customResponse objCustomResponse = new customResponse();
            if (objEntityUser.UserName.Trim().Length == 0)
                return Json(new { success = false, responseText = "1|Username cannot be blank" }, JsonRequestBehavior.AllowGet);
            IRestResponse forgotPasswordValidateUsernameResponse = Helper.WebAPIClient_via_RestSharp("validateUsername4ForgotPassword?userName=" + objEntityUser.UserName + "&fromMobileDevice=0", Method.GET, null);
            if (forgotPasswordValidateUsernameResponse.StatusCode == HttpStatusCode.OK)
            {
                Session["forgotPasswordOTPVM"] = forgotPasswordValidateUsernameResponse.Content;
                return Json(new { success = false, responseText = "1|FPOTP" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(forgotPasswordValidateUsernameResponse.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult resendForgotPasswordOTP()
        {
            string webLoginVM = Convert.ToString(Session["forgotPasswordOTPVM"]);
            if (webLoginVM == "")
            {
                return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mobileOTPVM objMobileOTPVM = new mobileOTPVM();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                webLoginVM objWebLoginVM = new webLoginVM();
                objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                if (objWebLoginVM != null)
                {
                    objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                    objMobileOTPVM.OTP = "";
                    objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                    objMobileOTPVM.userName = objWebLoginVM.userName;
                    IRestResponse resendOTPResponse = Helper.WebAPIClient_via_RestSharp("resendForgotPasswordOTP", Method.POST, objMobileOTPVM, accessToken);
                    if (resendOTPResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return Json(new { success = true, responseText = "OTP successfully resend" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Unable to send OTP. Please try again later" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult validateForgotPasswordOTP(tblEntity_User objEntityUser)
        {
            mobileOTPVM objMobileOTPVM = new mobileOTPVM();
            if (objEntityUser.OTP.Trim().Length > 0)
            {
                string webLoginVM = Convert.ToString(Session["forgotPasswordOTPVM"]);
                if (webLoginVM == "")
                {
                    return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                    webLoginVM objWebLoginVM = new webLoginVM();
                    objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                    if (objWebLoginVM != null)
                    {
                        objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                        objMobileOTPVM.OTP = objEntityUser.OTP;
                        objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                        objMobileOTPVM.userName = objWebLoginVM.userName;
                        IRestResponse OTPVerificationResponse = Helper.WebAPIClient_via_RestSharp("verifyForgotPasswordOTP", Method.POST, objMobileOTPVM, accessToken);
                        if (OTPVerificationResponse.StatusCode == HttpStatusCode.OK)
                        {
                            return Json(new { success = true, responseText = "0|Login details successfully send to you registered Mobile No." }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Invalid OTP" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please enter OTP. Value cannot be blank" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Register

        public ActionResult register()
        {
            return View("registerView");
        }

        public JsonResult registerUser(tblEntity_User objEntityUser)
        {
            if (objEntityUser.termsCondition)
            {
                if (objEntityUser.Password == objEntityUser.ConfirmPassword)
                {
                    customResponse objCustomResponse = new customResponse();
                    Boolean recordSaved = false;
                    tblEntity_Master objEntityMaster = new tblEntity_Master();
                    objEntityMaster.EntityTypeId = Convert.ToInt32(Helper.entityTypes.agent);
                    objEntityMaster.AgencyName = objEntityUser.UserName;
                    objEntityMaster.Mobile = objEntityUser.UserName;
                    objEntityMaster.Telephone1 = objEntityUser.UserName;
                    objEntityUser.Password = objEntityUser.Password;
                    objEntityUser.Mobile = objEntityUser.UserName;
                    string validateEntityUserDataBeforeInsertStatus = Helper.validateEntityUserBeforeInsert(objEntityUser);
                    if (validateEntityUserDataBeforeInsertStatus == "OK")
                    {
                        objEntityMaster.IsBankVerified = true;
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("saveAgent", Method.POST, objEntityMaster);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            recordSaved = true;
                            objEntityMaster = JsonConvert.DeserializeObject<tblEntity_Master>(response.Content);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = validateEntityUserDataBeforeInsertStatus }, JsonRequestBehavior.AllowGet);
                    }

                    if (recordSaved)
                    {
                        //Createing lead user 
                        if (objEntityMaster != null)
                        {
                            webLoginVM objWebLoginVM = new webLoginVM();
                            objEntityUser.EntityId = objEntityMaster.EntityId;
                            objEntityUser.StatusId = 1;
                            objEntityUser.UserName = objEntityUser.UserName;
                            objEntityUser.Mobile = objEntityUser.UserName;
                            objWebLoginVM = Helper.createEntityLeadUser(objEntityUser);
                            if (objWebLoginVM.statusMsg == "success")
                            {
                                Session["webLoginVM"] = objWebLoginVM.responseContent;
                                return Json(new { success = false, responseText = "1|OTP" }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = false, responseText = objWebLoginVM.statusMsg }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Oops something went wrong while creating Lead User" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Password and Confirm Password does not match" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please accept Terms and Conditions" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult validateRegisterOTP(tblEntity_User objEntityUser)
        {
            mobileOTPVM objMobileOTPVM = new mobileOTPVM();
            if (objEntityUser.OTP.Trim().Length > 0)
            {
                string webLoginVM = Convert.ToString(Session["webLoginVM"]);
                if (webLoginVM == "")
                {
                    return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                    webLoginVM objWebLoginVM = new webLoginVM();
                    objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                    if (objWebLoginVM != null)
                    {
                        objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                        objMobileOTPVM.OTP = objEntityUser.OTP;
                        objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                        objMobileOTPVM.userName = objWebLoginVM.userName;
                        IRestResponse OTPVerificationResponse = Helper.WebAPIClient_via_RestSharp("verifyRegisterOTP", Method.POST, objMobileOTPVM, accessToken);
                        if (OTPVerificationResponse.StatusCode == HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = "Registration successfully done.";
                            return Json(new { success = true, redirectURL = Url.Content("~/login") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Invalid OTP" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Please enter OTP. Value cannot be blank" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult resendRegisterOTP()
        {
            string webLoginVM = Convert.ToString(Session["webLoginVM"]);
            if (webLoginVM == "")
            {
                return Json(new { success = false, responseText = "1|Invalid access module" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mobileOTPVM objMobileOTPVM = new mobileOTPVM();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                webLoginVM objWebLoginVM = new webLoginVM();
                objWebLoginVM = JsonConvert.DeserializeObject<webLoginVM>(webLoginVM);
                if (objWebLoginVM != null)
                {
                    objMobileOTPVM.entityUserId = objWebLoginVM.entityUserId;
                    objMobileOTPVM.OTP = "";
                    objMobileOTPVM.mobileNo = objWebLoginVM.mobileNo;
                    objMobileOTPVM.userName = objWebLoginVM.userName;
                    IRestResponse resendOTPResponse = Helper.WebAPIClient_via_RestSharp("resendRegisterOTP", Method.POST, objMobileOTPVM, accessToken);
                    if (resendOTPResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return Json(new { success = true, responseText = "OTP successfully resend" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Unable to send OTP. Please try again later" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                    return Json(new { success = false, responseText = "1|Something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}