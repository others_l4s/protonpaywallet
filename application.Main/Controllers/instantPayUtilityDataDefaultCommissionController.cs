﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class instantPayUtilityDataDefaultCommissionController : Controller
    {
        // GET: instantPayUtilityDataDefaultCommission
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                List<tblInstantPay_UtilityData_Default_Commission> objDefaultCommission = new List<tblInstantPay_UtilityData_Default_Commission>();
                InstantPayUtilityDataDefaultCommissionVM objDefaultCommissionVM = new InstantPayUtilityDataDefaultCommissionVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayUtilityDataDefaultCommission", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objDefaultCommission = JsonConvert.DeserializeObject<List<tblInstantPay_UtilityData_Default_Commission>>(webAPIResponse.Content);
                    objDefaultCommissionVM.objDefaultCommission = objDefaultCommission;
                }
                return View("index", objDefaultCommissionVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveDefaultCommission(InstantPayUtilityDataDefaultCommissionVM objDefaultCommissionVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
                Boolean dataSaved = false;
                IRestResponse deleteResponse = Helper.WebAPIClient_via_RestSharp("instantPayUtilityDataDefaultCommission/0", Method.DELETE, objDefaultCommissionVM.objDefaultCommission, accessToken);
                if (deleteResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    foreach (tblInstantPay_UtilityData_Default_Commission objData in objDefaultCommissionVM.objDefaultCommission)
                    {
                        if (objData.MDCommissionValue > 0 && objData.DistCommissionValue > 0 && objData.AgentCommissionValue > 0)
                        {
                            objData.CommissionTypeId = 1;
                            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayUtilityDataDefaultCommission", Method.POST, objData, accessToken);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                dataSaved = true;
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                dataSaved = false;
                                break;
                            }
                        }
                    }
                    if (dataSaved)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, responseText = "0|Default commission successfully saved." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                { return Json(new { success = false, responseText = "1|Unable to delete existing records." }, JsonRequestBehavior.AllowGet); }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}