﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class productMasterController : Controller
    {
        // GET: productMaster
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            tblProduct_Master objProduct = new tblProduct_Master();
            IRestResponse statusResponse = Helper.WebAPIClient_via_RestSharp("status", Method.GET, null, accessToken);
            commonStatusList objCommonStatusList = Helper.getStatusList(statusResponse);
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("productMaster/" + id, Method.GET, null, accessToken);
                objProduct = JsonConvert.DeserializeObject<tblProduct_Master>(response1.Content);
            }
            else
            {
                objProduct.StatusId = objCommonStatusList.defaultActiveStatusId;
            }

            productMasterViewModel objProductMasterList = new productMasterViewModel();
            objProductMasterList.objProduct = objProduct;
            objProductMasterList.objStatusList = objCommonStatusList.objStatusList;
            objProductMasterList.objCommissionOn = Helper.commissionOnList();
            objProductMasterList.objServiceChargeTypeList = Helper.serviceChargeType();

            return View("index", objProductMasterList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveProduct(productMasterViewModel objProductMasterViewModel)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (ModelState.IsValid)
                {
                    if (objProductMasterViewModel.objProduct.IsServiceLIVE == true && objProductMasterViewModel.objProduct.CredentialFileName.Trim().Length == 0)
                        throw new Exception("Please configure and enter Credentials FileName");

                    if (!objProductMasterViewModel.objProduct.CredentialFileName.Trim().ToLower().Contains(".json"))
                        throw new Exception("Invalid file extension, please enter only .json extension file.");
                    if (objProductMasterViewModel.objProduct.ProductId == 0)
                    {
                        if (!string.IsNullOrEmpty(objProductMasterViewModel.objProduct.ProductCode))
                        {
                            IRestResponse response = Helper.WebAPIClient_via_RestSharp("productMaster", Method.POST, objProductMasterViewModel.objProduct, accessToken);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                TempData[Helper.responseStatus] = Helper.recordSaved;
                                return Json(new { success = true, redirectURL = Url.Content("~/productmaster") }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("StatusName", "Please enter Product Code");
                            return Json(new { success = false, responseText = "Please enter Product Code" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("productMaster/" + objProductMasterViewModel.objProduct.ProductId, Method.PUT, objProductMasterViewModel.objProduct, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordUpdated;
                            return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/productmaster") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteProduct(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("productMaster/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getProductList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            productMasterViewModel CompanyTypeList = new productMasterViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("productMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                CompanyTypeList = JsonConvert.DeserializeObject<productMasterViewModel>(response.Content);
            }
            return Json(new { data = CompanyTypeList.objProductList, draw = Request["draw"], recordsTotal = CompanyTypeList.totalRecords, recordsFiltered = CompanyTypeList.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}