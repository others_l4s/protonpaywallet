﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class mainMenuMasterController : Controller
    {
        // GET: mainMenuMaster
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            IRestResponse webAPIResponse;
            if (!String.IsNullOrEmpty(accessToken))
            {
                mainMenuMasterViewModel objMainMenuMasterViewModel = new mainMenuMasterViewModel();
                if (id > 0)
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("mainMenuMaster/" + id, Method.GET, null, accessToken);
                    objMainMenuMasterViewModel = JsonConvert.DeserializeObject<mainMenuMasterViewModel>(webAPIResponse.Content);
                    objMainMenuMasterViewModel.objExternalLinkTarget = Helper.bindExternalTargetTypeList();
                }
                else
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("mainMenuMaster", Method.GET, null, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objMainMenuMasterViewModel = JsonConvert.DeserializeObject<mainMenuMasterViewModel>(webAPIResponse.Content);
                    }

                    objMainMenuMasterViewModel.objExternalLinkTarget = Helper.bindExternalTargetTypeList();
                    objMainMenuMasterViewModel.objMainMenu.StatusId = (objMainMenuMasterViewModel.objStatusList.Count() > 0 ? objMainMenuMasterViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
                    objMainMenuMasterViewModel.objMainMenu.ExternalURLTarget = objMainMenuMasterViewModel.objExternalLinkTarget.SingleOrDefault(s => s.targetTypeValue.Trim().ToLower() == "_self").targetTypeValue;
                }
                return View("index", objMainMenuMasterViewModel);
            }
            else
            {
                return View("index", new { Controller = "unauthorized" });

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveMainMenu(mainMenuMasterViewModel objmainMenuMasterViewModel)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                if (objmainMenuMasterViewModel.objMainMenu.MainMenuId == 0)
                {
                    if (!string.IsNullOrEmpty(objmainMenuMasterViewModel.objMainMenu.MainMenuName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenuMaster", Method.POST, objmainMenuMasterViewModel.objMainMenu, accessToken);

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/mainMenuMaster") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new { success = false, responseText = Helper.recordAlreadyExists + " " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ModelState.AddModelError("MainMenuName", "Please enter Main Manu Name");
                        return Json(new { success = false, responseText = "Please enter Main Manu Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenuMaster/" + objmainMenuMasterViewModel.objMainMenu.MainMenuId, Method.PUT, objmainMenuMasterViewModel.objMainMenu, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/mainMenuMaster") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false, responseText = Helper.recordAlreadyExists + " " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteMainMenu(int id)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenuMaster/" + id, Method.DELETE, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Success");
                }
                else
                {
                    return Json(response.StatusCode.ToString());
                }
            }
            else
            {
                return Json("UnAuthorized");
            }
        }

        [HttpPost]
        public JsonResult getMainMenuList()
        {
            //Server Side Parameter
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                int pageIndex = Convert.ToInt32(Request["start"]);
                int pageSize = Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                mainMenuMasterViewModel mainMenuMasterViewModel = new mainMenuMasterViewModel();
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenuMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    mainMenuMasterViewModel = JsonConvert.DeserializeObject<mainMenuMasterViewModel>(response.Content);
                    return Json(new { data = mainMenuMasterViewModel.objMainMenuList, draw = Request["draw"], recordsTotal = mainMenuMasterViewModel.totalRecords, recordsFiltered = mainMenuMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    mainMenuMasterViewModel = new mainMenuMasterViewModel();
                    return Json(new { data = mainMenuMasterViewModel.objMainMenuList, draw = Request["draw"], recordsTotal = mainMenuMasterViewModel.totalRecords, recordsFiltered = mainMenuMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("UnAuthorized");
            }
        }
    }
}