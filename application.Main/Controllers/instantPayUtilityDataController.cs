﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class instantPayUtilityDataController : Controller
    {
        // GET: instantPayUtilityData
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            instantPayUtilityDataVM objInstantPayUtilityDataVM = new instantPayUtilityDataVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objInstantPayUtilityDataVM = JsonConvert.DeserializeObject<instantPayUtilityDataVM>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData/" + id, Method.GET, null, accessToken);
                objInstantPayUtilityDataVM.objInstantPayUtilityData = JsonConvert.DeserializeObject<tblInstantPay_UtilityData>(response1.Content);
            }
            objInstantPayUtilityDataVM.objUtilityTypeList = Helper.utilityType();
            return View("index", objInstantPayUtilityDataVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveUtilityData(instantPayUtilityDataVM objinstantPayUtilityDataVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objinstantPayUtilityDataVM.objInstantPayUtilityData.InstantPayRechargeDataId == 0)
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData", Method.POST, objinstantPayUtilityDataVM.objInstantPayUtilityData, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/instantpayutilitydata") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.ExceptionMessage + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData/" + objinstantPayUtilityDataVM.objInstantPayUtilityData.InstantPayRechargeDataId, Method.PUT, objinstantPayUtilityDataVM.objInstantPayUtilityData, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/instantpayutilitydata") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteUtilityData(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getUtilityData()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];

            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            instantPayUtilityDataVM objUtilityDataVM = new instantPayUtilityDataVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayUtilityData?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objUtilityDataVM = JsonConvert.DeserializeObject<instantPayUtilityDataVM>(response.Content);
                if (objUtilityDataVM.objInstantPayUtilityDataList.Count == 0)
                    objUtilityDataVM.objInstantPayUtilityDataList = new List<tblInstantPay_UtilityData>();
            }
            return Json(new { data = objUtilityDataVM.objInstantPayUtilityDataList, draw = Request["draw"], recordsTotal = objUtilityDataVM.totalRecords, recordsFiltered = objUtilityDataVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}