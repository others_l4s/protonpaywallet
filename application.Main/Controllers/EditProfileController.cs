﻿using application.Main.Models;
using application.Main.ViewModels;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;


namespace application.Main.Controllers
{
    public class EditProfileController : Controller
    {
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                editProfileKYCVM objEditProfileKYCVM = new editProfileKYCVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("editProfile", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEditProfileKYCVM = JsonConvert.DeserializeObject<editProfileKYCVM>(webAPIResponse.Content);
                    objEditProfileKYCVM.objEditProfile = (objEditProfileKYCVM.objEditProfile == null ? new editProfileKYC() : objEditProfileKYCVM.objEditProfile);
                    if (String.IsNullOrEmpty(objEditProfileKYCVM.objEditProfile.PanCardPhoto))
                        objEditProfileKYCVM.objEditProfile.PanCardPhoto = "../KYCImages/NoImages.png";
                    else
                    {
                        objEditProfileKYCVM.objEditProfile.PanCardPhoto = objEditProfileKYCVM.objEditProfile.PanCardPhoto;
                    }
                    if (String.IsNullOrEmpty(objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto))
                        objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto = "../KYCImages/NoImages.png";
                    else
                        objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto = objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto;
                }
                //Checking whether outlet Registration has been done or not
                if (!objEditProfileKYCVM.objEditProfile.OutletMobileVerified)
                {
                    IRestResponse outletRegistrationMobileOTP = Helper.WebAPIClient_via_RestSharp("prime/outletRegistrationOTP", Method.GET, null, accessToken);
                    if (outletRegistrationMobileOTP.StatusCode == System.Net.HttpStatusCode.OK)
                        objEditProfileKYCVM.objEditProfile.OutletRegistrationMobileOTPSent = true;
                    else
                        objEditProfileKYCVM.objEditProfile.OutletRegistrationMobileOTPSent = false;
                }
                else
                    objEditProfileKYCVM.objEditProfile.OutletRegistrationMobileOTPSent = false;
                return View("index", objEditProfileKYCVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveProfile(editProfileKYCVM objEditProfileKYC)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            string panCardNoPhotoName = "", panCardNoPhotoExtension = "";
            string aadhaarNoPhotoName = "", aadhaarNoPhotoExtension = "";
            objEditProfileKYC.objEditProfile.FromMobileDevice = false;
            if (objEditProfileKYC.objEditProfile.EntityKYCId == 0)
            {
                if (objEditProfileKYC.HPPanCardPhoto != null)
                {
                    panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEditProfileKYC.HPPanCardPhoto.FileName);
                    panCardNoPhotoExtension = Path.GetExtension(objEditProfileKYC.HPPanCardPhoto.FileName);
                    panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                    objEditProfileKYC.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                    objEditProfileKYC.objEditProfile.PanCardPhoto = panCardNoPhotoName;
                }
                else
                    return Json(new { success = false, responseText = "1|Please upload Pancard Photo" }, JsonRequestBehavior.AllowGet);

                if (objEditProfileKYC.HPAadhaarCardPhoto != null)
                {
                    aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEditProfileKYC.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoExtension = Path.GetExtension(objEditProfileKYC.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                    objEditProfileKYC.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                    objEditProfileKYC.objEditProfile.AadhaarCardPhoto = aadhaarNoPhotoName;
                }
                else
                    return Json(new { success = false, responseText = "1|Please upload Aadhaar Card Photo" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (objEditProfileKYC.HPPanCardPhoto != null)
                {
                    panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEditProfileKYC.HPPanCardPhoto.FileName);
                    panCardNoPhotoExtension = Path.GetExtension(objEditProfileKYC.HPPanCardPhoto.FileName);
                    panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                    objEditProfileKYC.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                    objEditProfileKYC.objEditProfile.PanCardPhoto = panCardNoPhotoName;
                }
                if (objEditProfileKYC.HPAadhaarCardPhoto != null)
                {
                    aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEditProfileKYC.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoExtension = Path.GetExtension(objEditProfileKYC.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                    objEditProfileKYC.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                    objEditProfileKYC.objEditProfile.AadhaarCardPhoto = aadhaarNoPhotoName;
                }
            }
            IRestResponse editProfileResponse = Helper.WebAPIClient_via_RestSharp("editProfileKYCSave", Method.POST, objEditProfileKYC.objEditProfile, accessToken);
            if (editProfileResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {

                return Json(new { success = true, responseText = "0|Profile successfully updated" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (editProfileResponse.Content != "")
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(editProfileResponse.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "1|" + editProfileResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        protected string updateEntityKYC(entityUserMasterViewModel objEntityUserVM, string accessToken)
        {
            string KYCResponse = ""; customResponse objCustomResponse = new customResponse();
            string panCardNoPhotoName = "", panCardNoPhotoExtension = "";
            string aadhaarNoPhotoName = "", aadhaarNoPhotoExtension = "";
            objEntityUserVM.objEntityKYC.FromMobileDevice = false;
            if (objEntityUserVM.objEntityKYC.EntityKYCId == 0)
            {
                if (objEntityUserVM.HPPanCardPhoto != null)
                {
                    panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEntityUserVM.HPPanCardPhoto.FileName);
                    panCardNoPhotoExtension = Path.GetExtension(objEntityUserVM.HPPanCardPhoto.FileName);
                    panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                    objEntityUserVM.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                    objEntityUserVM.objEntityKYC.PanCardPhoto = panCardNoPhotoName;
                }
                else
                    return "Please upload PanCard Photo";

                if (objEntityUserVM.HPAadhaarCardPhoto != null)
                {
                    aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEntityUserVM.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoExtension = Path.GetExtension(objEntityUserVM.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                    objEntityUserVM.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                    objEntityUserVM.objEntityKYC.AadhaarCardPhoto = aadhaarNoPhotoName;
                }
                else
                    return "Please upload Aadhaar Card Photo";

                IRestResponse response = Helper.WebAPIClient_via_RestSharp("saveEntityKYC", Method.POST, objEntityUserVM.objEntityKYC, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    KYCResponse = "0";
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    KYCResponse = objCustomResponse.Message;
                }
            }
            else
            {
                if (objEntityUserVM.HPPanCardPhoto != null)
                {
                    panCardNoPhotoName = Path.GetFileNameWithoutExtension(objEntityUserVM.HPPanCardPhoto.FileName);
                    panCardNoPhotoExtension = Path.GetExtension(objEntityUserVM.HPPanCardPhoto.FileName);
                    panCardNoPhotoName = "pancard_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + panCardNoPhotoExtension.ToString();
                    objEntityUserVM.HPPanCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), panCardNoPhotoName));
                    objEntityUserVM.objEntityKYC.PanCardPhoto = panCardNoPhotoName;
                }
                if (objEntityUserVM.HPAadhaarCardPhoto != null)
                {
                    aadhaarNoPhotoName = Path.GetFileNameWithoutExtension(objEntityUserVM.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoExtension = Path.GetExtension(objEntityUserVM.HPAadhaarCardPhoto.FileName);
                    aadhaarNoPhotoName = "aadhaar_side_1_" + Helper.GetRandomPasswordUsingGUID(10) + aadhaarNoPhotoExtension.ToString();
                    objEntityUserVM.HPAadhaarCardPhoto.SaveAs(Path.Combine(Server.MapPath("../KYCImages/"), aadhaarNoPhotoName));
                    objEntityUserVM.objEntityKYC.AadhaarCardPhoto = aadhaarNoPhotoName;
                }
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityKYC/" + objEntityUserVM.objEntityKYC.EntityKYCId, Method.PUT, objEntityUserVM.objEntityKYC, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    KYCResponse = "0";
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    KYCResponse = objCustomResponse.Message;
                }
            }
            return KYCResponse;
        }

        #region Change Password

        public ActionResult changePassword()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                changePassword objChangePassword = new changePassword();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                return View("indexChangePassword", objChangePassword);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult changePasswordCommit(changePassword objChangePassword)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            if (objChangePassword.newPassword == objChangePassword.confirmPassword)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entityUser/changePassword", Method.POST, objChangePassword, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string message = JsonConvert.DeserializeObject<string>(response.Content);
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = "0|" + message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|New Password and Confirm Password does not match" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string outletRegistration_Get_KYCDocumentStatus(string accessToken)
        {
            customResponse objCustomResponse = new customResponse();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("prime/outletGetKYCDocumentStatus", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "";
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return objCustomResponse.Message;
            }
        }
    }
}