﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class CompanyTypeController : Controller
    {
        // GET: CompanyType

        public ActionResult Index(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            tblCompany_Type objCompanyType = new tblCompany_Type();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("status", Method.GET, null, accessToken);
            commonStatusList objCommonStatusList = Helper.getStatusList(webAPIResponse);
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("CompanyType/" + id, Method.GET, null, accessToken);
                objCompanyType = JsonConvert.DeserializeObject<tblCompany_Type>(response1.Content);
            }
            else
            {
                objCompanyType.StatusId = objCommonStatusList.defaultActiveStatusId;
            }
            var objCompanyModelView = new companyTypeMasterViewModel
            {
                objCompanyType = objCompanyType,
                objStatusMasterList = objCommonStatusList.objStatusList
            };

            return View(objCompanyModelView);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult saveCompanyType(companyTypeMasterViewModel objCompanytypeViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objCompanytypeViewModel.objCompanyType.CompanyTypeId == 0)
                {
                    if (!string.IsNullOrEmpty(objCompanytypeViewModel.objCompanyType.CompanyTypeName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("CompanyType", Method.POST, objCompanytypeViewModel.objCompanyType, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/companytype") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("StatusName", "Please enter Status Name");
                        return Json(new { success = false, responseText = "1|Please enter Status Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("CompanyType/" + objCompanytypeViewModel.objCompanyType.CompanyTypeId, Method.PUT, objCompanytypeViewModel.objCompanyType, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/companytype") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteCompanyType(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("CompanyType/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }
        [HttpPost]
        public JsonResult getCompanyTypeList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            companyTypeMasterViewModel CompanyTypeList = new companyTypeMasterViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("CompanyType?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                CompanyTypeList = JsonConvert.DeserializeObject<companyTypeMasterViewModel>(webAPIResponse.Content);
            }
            return Json(new { data = CompanyTypeList.objComapanyTypeMasterList, draw = Request["draw"], recordsTotal = CompanyTypeList.totalRecords, recordsFiltered = CompanyTypeList.filterRecords }, JsonRequestBehavior.AllowGet);
        }
    }
}