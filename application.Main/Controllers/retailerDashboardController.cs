﻿using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fluentx.Mvc;
using System.Text;
using System.Security.Cryptography;
using System.Net;
using application.Main.Models;

namespace application.Main.Controllers
{
    public class retailerDashboardController : Controller
    {
        public Dictionary<string, string> parameterList { get; private set; } = new Dictionary<string, string>();
        // GET: retailerDashboard
        public ActionResult Index(string type)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                entityDashboardVM objEntityDashboardVM = new entityDashboardVM();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityMaster/getDashboardDetails", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityDashboardVM = JsonConvert.DeserializeObject<entityDashboardVM>(webAPIResponse.Content);
                }
                //Adding user onboarding process
                objEntityDashboardVM.objOutletKYCStatus = new Models.outletKYCStatus();
                IRestResponse objUserOnboardingStatus = Helper.WebAPIClient_via_RestSharp("prime/outletGetKYCDocumentApprovalStatus", Method.GET, null, accessToken);
                if (objUserOnboardingStatus.StatusCode == HttpStatusCode.OK)
                {
                    objEntityDashboardVM.objOutletKYCStatus = JsonConvert.DeserializeObject<Models.outletKYCStatus>(objUserOnboardingStatus.Content);
                }
                else {
                    objEntityDashboardVM.objOutletKYCStatus.StatusCode = "Pending";
                    objEntityDashboardVM.objOutletKYCStatus.AadhaarCardRequired = true;
                    objEntityDashboardVM.objOutletKYCStatus.PanCardRequired = true;
                    
                }
                return View("index", objEntityDashboardVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }


        public ActionResult bbps()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                entityDashboardVM objEntityDashboardVM = new entityDashboardVM();
                primeURLs objPrimeURLs = new primeURLs();
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("prime/accessPrime", Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objPrimeURLs = JsonConvert.DeserializeObject<primeURLs>(webAPIResponse.Content);
                    Dictionary<string, object> postData = new Dictionary<string, object>();
                    postData.Add("params", objPrimeURLs.primePostData);
                    return this.RedirectAndPost("https://prime.protonpay.net", postData);
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|You are not authorized to use payment service. Please update your KYC Document.";
                    IRestResponse dashboardResponse = Helper.WebAPIClient_via_RestSharp("entityMaster/getDashboardDetails", Method.GET, null, accessToken);
                    if (dashboardResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objEntityDashboardVM = JsonConvert.DeserializeObject<entityDashboardVM>(dashboardResponse.Content);
                    }
                    objEntityDashboardVM.objOutletKYCStatus = new Models.outletKYCStatus();
                    IRestResponse objUserOnboardingStatus = Helper.WebAPIClient_via_RestSharp("prime/outletGetKYCDocumentApprovalStatus", Method.GET, null, accessToken);
                    if (objUserOnboardingStatus.StatusCode == HttpStatusCode.OK)
                    {
                        objEntityDashboardVM.objOutletKYCStatus = JsonConvert.DeserializeObject<Models.outletKYCStatus>(objUserOnboardingStatus.Content);
                    }
                    return View("index", objEntityDashboardVM);
                }
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });

        }

        public static byte[] Combine(byte[] first, byte[] second, byte[] third)
        {
            byte[] ret = new byte[first.Length + second.Length + third.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            Buffer.BlockCopy(third, 0, ret, first.Length + second.Length,
                            third.Length);
            return ret;
        }
    }
}