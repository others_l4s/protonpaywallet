﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            roleViewModel objRoleViewModel = new roleViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("role", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objRoleViewModel = JsonConvert.DeserializeObject<roleViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("role/" + id, Method.GET, null, accessToken);
                objRoleViewModel.objRole = JsonConvert.DeserializeObject<tblRole_Master>(response1.Content);
            }
            else
            {
                objRoleViewModel.objRole.StatusId = (objRoleViewModel.objStatusList.Count() > 0 ? objRoleViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objRoleViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveRole(roleViewModel objroleViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objroleViewModel.objRole.RoleId == 0)
                {
                    if (!string.IsNullOrEmpty(objroleViewModel.objRole.RoleName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("role", Method.POST, objroleViewModel.objRole, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/role") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("CompanyName", "Please enter Role Name");
                        return Json(new { success = false, responseText = "Please enter Role Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("role/" + objroleViewModel.objRole.RoleId, Method.PUT, objroleViewModel.objRole, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/role") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteRole(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("role/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getRoleList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            roleViewModel roleViewModel = new roleViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("role?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                roleViewModel = JsonConvert.DeserializeObject<roleViewModel>(response.Content);
                return Json(new { data = roleViewModel.objRoleList, draw = Request["draw"], recordsTotal = roleViewModel.totalRecords, recordsFiltered = roleViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                roleViewModel = new roleViewModel();
                return Json(new { data = roleViewModel.objRoleList, draw = Request["draw"], recordsTotal = roleViewModel.totalRecords, recordsFiltered = roleViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}