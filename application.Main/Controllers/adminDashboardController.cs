﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class adminDashboardController : Controller
    {
        // GET: adminDashboard
        public ActionResult Index()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
                return View();
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }
    }
}