﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class BankItAEPSController : Controller
    {
        // GET: BankItAEPS
        public ActionResult Index()
        {
            BANKIT_AEPS_getToken_Request objRequest = new BANKIT_AEPS_getToken_Request();
            objRequest.token = "12345";
            //objRequest.agentAuthId = "21293ec1a16e9409eb97c8fa11035302";
            //objRequest.agentAuthPassword = "c1e63f47d97ee7454578113b6d0d4c44";
            //objRequest.retailerId = "AG2190";
            //objRequest.apiId = "10093";
            objRequest.agentAuthId = "78b40146ec267f53e00a23acbd34342b";
            objRequest.agentAuthPassword = "366ae8e0223cee880d723dc063e71b2a";
            objRequest.retailerId = "AG2190";
            objRequest.apiId = "10084";
            
            IRestResponse webAPIResponse = Helper.WebAPIBankIt_AEPS_via_RestSharp(objRequest);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                BANKIT_AEPS_Get_Token_Response objbeneficiaryresponse = JsonConvert.DeserializeObject<BANKIT_AEPS_Get_Token_Response>(webAPIResponse.Content);
                if (objbeneficiaryresponse.data.token.Length > 0)
                {
                    ViewBag.Url = "https://red.bankit.in/AEPS/login?token=" + objbeneficiaryresponse.data.token;
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Please Check Your Token";
                }
            }
            return View("index");
        }
    }
}