﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class SubscriptionProductController : Controller
    {

        public ActionResult Index(int Id = 0, int subScriptionId = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            subscriptionProductViewModel objsubscriptionProductViewModel = new subscriptionProductViewModel();
            IRestResponse subscriptionProductResponse = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts", Method.GET, null, accessToken);
            objsubscriptionProductViewModel = JsonConvert.DeserializeObject<subscriptionProductViewModel>(subscriptionProductResponse.Content);
            if (Id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts/" + Id, Method.GET, null, accessToken);
                objsubscriptionProductViewModel.objSubscriptionProduct = JsonConvert.DeserializeObject<tblsubScriptionProduct_Master>(response1.Content);
            }
            else
            {
                objsubscriptionProductViewModel.objSubscriptionProduct.StatusId = objsubscriptionProductViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim() == Helper.defaultActiveStatus).StatusId;
                objsubscriptionProductViewModel.objSubscriptionProduct.SubscriptionProductId = 0;
                objsubscriptionProductViewModel.objSubscriptionProduct.SubscriptionId = subScriptionId;
            }

            var subscriptionProductViewModel = new subscriptionProductViewModel
            {
                objSubscriptionProduct = objsubscriptionProductViewModel.objSubscriptionProduct,
                objStatusList = objsubscriptionProductViewModel.objStatusList,
                objProductList = objsubscriptionProductViewModel.objProductList,
                objSubscriptionList = objsubscriptionProductViewModel.objSubscriptionList
            };
            return View(subscriptionProductViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSubscriptionProduct(subscriptionProductViewModel objsubscriptionProductViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objsubscriptionProductViewModel.objSubscriptionProduct.SubscriptionProductId == 0)
                {
                    if (objsubscriptionProductViewModel.objSubscriptionProduct.ProductId != 0 || objsubscriptionProductViewModel.objSubscriptionProduct.SubscriptionId != 0)
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts", Method.POST, objsubscriptionProductViewModel.objSubscriptionProduct, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/subscriptionproduct?subScriptionId=" + Request["subScriptionId"]) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("SubscriptionProducts", "Please enter Subscription Products");
                        return Json(new { success = false, responseText = "1|Please enter Subscription Products" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts/" + objsubscriptionProductViewModel.objSubscriptionProduct.SubscriptionProductId, Method.PUT, objsubscriptionProductViewModel.objSubscriptionProduct, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, redirectURL = Url.Content("~/subscriptionproduct?subScriptionId=" + Request["subScriptionId"]) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSubscriptionProduct(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getSubscriptionProductList(int subscriptionId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            subscriptionProductViewModel objsubscriptionProductViewModel = new subscriptionProductViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubscriptionProducts?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&subScriptionId=" + subscriptionId, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objsubscriptionProductViewModel = JsonConvert.DeserializeObject<subscriptionProductViewModel>(response.Content);
                return Json(new { data = objsubscriptionProductViewModel.objSubscriptionProductList, draw = Request["draw"], recordsTotal = objsubscriptionProductViewModel.totalRecords, recordsFiltered = objsubscriptionProductViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objsubscriptionProductViewModel = new subscriptionProductViewModel();
                return Json(new { data = objsubscriptionProductViewModel.objSubscriptionProductList, draw = Request["draw"], recordsTotal = objsubscriptionProductViewModel.totalRecords, recordsFiltered = objsubscriptionProductViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}