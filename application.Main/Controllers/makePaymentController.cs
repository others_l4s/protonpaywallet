﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using System.IO;

namespace application.Main.Controllers
{
    public class makePaymentController : Controller
    {
        // GET: makePayment
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addBalance()
        {
            makePaymentVM objMakePaymentVM = new makePaymentVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });
            return View("index", objMakePaymentVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult saveMakePayment(makePaymentVM objMakePaymentVM)
        {
            customResponse objCustomResponse;
            IRestResponse webAPIResponse;
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            try
            {
                //Accessing API from paymentGatewayController
                string IPAddress = Helper.GetIp();
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("paymentGateway/makePaymentRequestURL?amount=" + objMakePaymentVM.objEntityPayments.Amount + "&fromMobileDevice=false&IPAddress=" + IPAddress, Method.GET, null, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objMakePaymentVM.makePaymentURL = JsonConvert.DeserializeObject<string>(webAPIResponse.Content);
                    return View("makePaymentView", objMakePaymentVM);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }

        }

        protected Boolean validate_Property_By_PaymentMode(tblEntity_Payments objEntityPayments)
        {
            Boolean isPropertyValidated = false;
            switch (objEntityPayments.PaymentModeId)
            {
                case 1:
                    if (!String.IsNullOrEmpty(objEntityPayments.BankBranchCode) && !String.IsNullOrEmpty(objEntityPayments.BankTransactionId))
                        isPropertyValidated = true;
                    break;
                case 2:
                    if (!string.IsNullOrEmpty(objEntityPayments.ChequeNo))
                        isPropertyValidated = true;
                    break;
                case 3:
                case 4:
                    if (!String.IsNullOrEmpty(objEntityPayments.BankTransactionId))
                        isPropertyValidated = true;
                    break;
            }

            return isPropertyValidated;
        }

        [HttpGet]
        public JsonResult getPaymentList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            makePaymentVM objMakePaymentVM = new makePaymentVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getPaymentHistory?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMakePaymentVM = JsonConvert.DeserializeObject<makePaymentVM>(webAPIResponse.Content);
                return Json(new { data = objMakePaymentVM.objEntityPaymentList, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMakePaymentVM = new makePaymentVM();
                return Json(new { data = objMakePaymentVM.objEntityPaymentList, draw = Request["draw"], recordsTotal = objMakePaymentVM.totalRecords, recordsFiltered = objMakePaymentVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult paymentGateway(string makePaymentURL)
        {
            makePaymentVM objMakePaymentVM = new makePaymentVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            objMakePaymentVM.makePaymentURL = makePaymentURL;
            return View("makePaymentView", objMakePaymentVM);
        }

    }
}