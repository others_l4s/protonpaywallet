﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class companyMenuPermissionController : Controller
    {
        // GET: companyMenuPermission
        public ActionResult Index()
        {
            SubMenuCompanyPermissionVM objSubMenuCompanyPermissionVM = new SubMenuCompanyPermissionVM();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("company", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companyMasterViewModel objCompanyList = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
                objSubMenuCompanyPermissionVM.objCompanyList = objCompanyList.objCompanyList;
            }
            return View("index", objSubMenuCompanyPermissionVM);
        }



        public ActionResult getMainMenuList(int companyId)
        {

            SubMenuCompanyPermissionVM objCompanyMenuPermission = new SubMenuCompanyPermissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseSubMenuRole = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission?CompanyID=" + companyId, Method.GET, null, accessToken);
            objCompanyMenuPermission = JsonConvert.DeserializeObject<SubMenuCompanyPermissionVM>(webAPIResponseSubMenuRole.Content);
            objCompanyMenuPermission.CompanyId = companyId;
            return PartialView("__partial_companyMainMenuPermission", objCompanyMenuPermission);
        }

        [HttpPost]
        public JsonResult SaveMainMenuCompanyPermission(SubMenuCompanyPermissionVM mainMenuCompanyPermission)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse bulkDeleteResponse = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission/" + mainMenuCompanyPermission.CompanyId, Method.DELETE, null, accessToken);
            if (bulkDeleteResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                tblSubMenu_Company_Permission objCompanyMenuPermission;
                foreach (tblSubMenu_Company_Permission objData in mainMenuCompanyPermission.objMainMenuList)
                {
                    if (objData.CanView == true)
                    {
                        objCompanyMenuPermission = new tblSubMenu_Company_Permission();
                        objCompanyMenuPermission.MenuPermissionId = objData.MenuPermissionId;
                        objCompanyMenuPermission.CompanyId = mainMenuCompanyPermission.CompanyId;
                        objCompanyMenuPermission.MainMenuId = objData.MainMenuId;
                        objCompanyMenuPermission.CanView = objData.CanView;
                        objCompanyMenuPermission.CanAdd = false;
                        objCompanyMenuPermission.CanDelete = false;
                        objCompanyMenuPermission.CanEdit = false;
                        objCompanyMenuPermission.SubMenuId = 0;
                        objCompanyMenuPermission.SubMenuId = 0;
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission", Method.POST, objCompanyMenuPermission, accessToken);
                    }
                }
                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                customResponse objCustomResponse = new customResponse();
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(bulkDeleteResponse.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + bulkDeleteResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}