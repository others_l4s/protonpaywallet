﻿using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class ledgerReportController : Controller
    {
        // GET: ledgerReport
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getLedgerReport(string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            accountVM objAccountVM = new accountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getLedgerReport?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAccountVM = JsonConvert.DeserializeObject<accountVM>(webAPIResponse.Content);
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAccountVM = new accountVM();
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}