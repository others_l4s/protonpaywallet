﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class subMenuMasterController : Controller
    {
        // GET: subMenuMaster
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0, int mainMenuId = 0)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            IRestResponse webAPIResponse;
            if (!String.IsNullOrEmpty(accessToken))
            {
                subMenuMasterViewModel objViewModel = new subMenuMasterViewModel();
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("subMenuMaster/" + id + "?mainMenuId=" + mainMenuId, Method.GET, null, accessToken);
                objViewModel = JsonConvert.DeserializeObject<subMenuMasterViewModel>(webAPIResponse.Content);
                objViewModel.objExternalLinkTarget = Helper.bindExternalTargetTypeList();
                objViewModel.objSubMenu.MainMenuId = mainMenuId;
                objViewModel.objExternalLinkTarget = Helper.bindExternalTargetTypeList();
                if (id == 0)
                {
                    objViewModel.objSubMenu.StatusId = (objViewModel.objStatusList.Count() > 0 ? objViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
                    objViewModel.objSubMenu.ExternalURLTarget = objViewModel.objExternalLinkTarget.SingleOrDefault(s => s.targetTypeValue.Trim().ToLower() == "_self").targetTypeValue;
                }
                return View("index", objViewModel);
            }
            else
            {
                return View("index", new { Controller = "unauthorized" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSubMenu(subMenuMasterViewModel objViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                if (ModelState.IsValid)
                {
                    if (objViewModel.objSubMenu.SubMenuId == 0)
                    {
                        if (!string.IsNullOrEmpty(objViewModel.objSubMenu.SubMenuName))
                        {
                            IRestResponse response = Helper.WebAPIClient_via_RestSharp("subMenuMaster", Method.POST, objViewModel.objSubMenu, accessToken);
                            if (response.StatusCode == System.Net.HttpStatusCode.OK)
                            {
                                TempData[Helper.responseStatus] = Helper.recordSaved;
                                return Json(new { success = true, redirectURL = Url.Content("~/subMenuMaster?mainMenuId=" + Request["mainMenuId"]) }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("MainMenuName", "Please enter Sub Manu Name");
                            return Json(new { success = false, responseText = "1|Please enter Sub Manu Name" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("subMenuMaster/" + objViewModel.objSubMenu.SubMenuId, Method.PUT, objViewModel.objSubMenu, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordUpdated;
                            return Json(new { success = true, redirectURL = Url.Content("~/subMenuMaster?mainMenuId=" + Request["mainMenuId"]) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteSubMenu(int id)
        {
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("subMenuMaster/" + id, Method.DELETE, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Success");
                }
                else
                {
                    return Json(response.StatusCode.ToString());
                }
            }
            else
            {
                return Json("UnAuthorized");
            }
        }

        [HttpPost]
        public JsonResult getSubMenuList(int mainMenuId)
        {
            //Server Side Parameter
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                int pageIndex = Convert.ToInt32(Request["start"]);
                int pageSize = Convert.ToInt32(Request["length"]);
                string searchValue = Request["search[value]"];
                string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
                string sortDirection = Request["order[0][dir]"];
                subMenuMasterViewModel objViewModel = new subMenuMasterViewModel();
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("subMenuMaster?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&mainMenuId=" + mainMenuId, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objViewModel = JsonConvert.DeserializeObject<subMenuMasterViewModel>(response.Content);
                    return Json(new { data = objViewModel.objSubMenuList, draw = Request["draw"], recordsTotal = objViewModel.totalRecords, recordsFiltered = objViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objViewModel = new subMenuMasterViewModel();
                    return Json(new { data = objViewModel.objSubMenuList, draw = Request["draw"], recordsTotal = objViewModel.totalRecords, recordsFiltered = objViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("UnAuthorized");
            }
        }
    }
}