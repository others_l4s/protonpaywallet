﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;

namespace application.Main.Controllers
{
    public class aepsQueueController : Controller
    {
        // GET: aepsQueue
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        public JsonResult getAEPS_Request_Queue()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITAEPSRequestQueueVM objBankITAEPSRequestVM = new BankITAEPSRequestQueueVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("getBnkITAEPSRequestQueue?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITAEPSRequestVM = JsonConvert.DeserializeObject<BankITAEPSRequestQueueVM>(response.Content);
                return Json(new { data = objBankITAEPSRequestVM.objBankITAEPSRequestList, draw = Request["draw"], recordsTotal = objBankITAEPSRequestVM.totalRecords, recordsFiltered = objBankITAEPSRequestVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objBankITAEPSRequestVM = new BankITAEPSRequestQueueVM();
                return Json(new { data = objBankITAEPSRequestVM.objBankITAEPSRequestList, draw = Request["draw"], recordsTotal = objBankITAEPSRequestVM.totalRecords, recordsFiltered = objBankITAEPSRequestVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult updateResponse(long id)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblBankITAEPS_Request objAEPSRequest = new tblBankITAEPS_Request();
                IRestResponse apiResponse = Helper.WebAPIClient_via_RestSharp("getBankITAEPSRequest?id=" + id, Method.GET, null, accessToken);
                if (apiResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objAEPSRequest = JsonConvert.DeserializeObject<tblBankITAEPS_Request>(apiResponse.Content);
                }
                return View("processQueue", objAEPSRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult updateBankITResponse(tblBankITAEPS_Request objAEPSRequest)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            if (objAEPSRequest.BankItAEPSId > 0)
            {
                IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("BankITAEPSRequest/" + objAEPSRequest.BankItAEPSId, Method.PUT, objAEPSRequest, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = "0|AEPS Request successfully Activated.";
                    return Json(new { success = true, redirectURL = Url.Content("~/aepsQueue") }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false, responseText = "1|" + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }
            else { return Json(new { success = false, responseText = "1|Unable to update Response. AEPSRequestId missing" }, JsonRequestBehavior.AllowGet); }

        }
    }
}