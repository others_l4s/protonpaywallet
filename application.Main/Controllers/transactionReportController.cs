﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class transactionReportController : Controller
    {
        // GET: transactionReport
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getTransactionReport(string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            accountVM objAccountVM = new accountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getTransactionReport?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAccountVM = JsonConvert.DeserializeObject<accountVM>(webAPIResponse.Content);
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAccountVM = new accountVM();
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }


        #region Transaction Report for Admin

        public ActionResult transactionReportByAdmin()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                entityVM objEntityVM = new entityVM();
                IRestResponse entityVMResponse = Helper.WebAPIClient_via_RestSharp("getEntityVM", Method.GET, null, accessToken);
                if (entityVMResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityVM = JsonConvert.DeserializeObject<entityVM>(entityVMResponse.Content);
                }
                objEntityVM.objEntityList = new List<tblEntity_Master>();
                return View("transactionReport4Admin", objEntityVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult getTransactionReportByAdmin(long entityId, string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            accountVM objAccountVM = new accountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getTransactionReport?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&searchEntityId=" + entityId, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAccountVM = JsonConvert.DeserializeObject<accountVM>(webAPIResponse.Content);
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAccountVM = new accountVM();
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult transactionReportByParent()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return RedirectToAction("index", new { Controller = "unauthorized" });

                entityVM objEntityVM = new entityVM();
                IRestResponse entityVMResponse = Helper.WebAPIClient_via_RestSharp("getEntityVM", Method.GET, null, accessToken);
                if (entityVMResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objEntityVM = JsonConvert.DeserializeObject<entityVM>(entityVMResponse.Content);
                }
                objEntityVM.objEntityList = new List<tblEntity_Master>();
                return View("transactionReport4Parent", objEntityVM);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        //This report will get access by Master Distributor or Distributor
        [HttpGet]
        public JsonResult getTransactionReportByParent(long entityId, string fromDate, string toDate)
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            accountVM objAccountVM = new accountVM();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("getTransactionReport4Parent?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&searchEntityId=" + entityId, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objAccountVM = JsonConvert.DeserializeObject<accountVM>(webAPIResponse.Content);
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objAccountVM = new accountVM();
                return Json(new { data = objAccountVM.objEntityAccountList, draw = Request["draw"], recordsTotal = objAccountVM.totalRecords, recordsFiltered = objAccountVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion
    }
}