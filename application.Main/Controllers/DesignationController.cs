﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;

namespace application.Main.Controllers
{
    public class DesignationController : Controller
    {// GET: Designation
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }
        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            designationViewModel objDesignationViewModel = new designationViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("Designation", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objDesignationViewModel = JsonConvert.DeserializeObject<designationViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("Designation/" + id, Method.GET, null, accessToken);
                objDesignationViewModel.objDesignation = JsonConvert.DeserializeObject<tblDesignation_Master>(response1.Content);
            }
            else
            {
                objDesignationViewModel.objDesignation.StatusId = (objDesignationViewModel.objStatusList.Count() > 0 ? objDesignationViewModel.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objDesignationViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveDesignation(designationViewModel objDesignationViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                if (objDesignationViewModel.objDesignation.DesignationId == 0)
                {
                    if (!string.IsNullOrEmpty(objDesignationViewModel.objDesignation.DesignationName))
                    {
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("Designation", Method.POST, objDesignationViewModel.objDesignation, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/designation") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("DesignationName", "Please enter Designation Name");
                        return Json(new { success = false, responseText = "1|Please enter Designation Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("Designation/" + objDesignationViewModel.objDesignation.DesignationId, Method.PUT, objDesignationViewModel.objDesignation, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, redirectURL = Url.Content("~/designation") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteDesignation(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Designation/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        [HttpGet]
        public JsonResult getDesignationList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            designationViewModel DesignationViewModel = new designationViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Designation?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                DesignationViewModel = JsonConvert.DeserializeObject<designationViewModel>(response.Content);
                return Json(new { data = DesignationViewModel.objDesignationList, draw = Request["draw"], recordsTotal = DesignationViewModel.totalRecords, recordsFiltered = DesignationViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                DesignationViewModel = new designationViewModel();
                return Json(new { data = DesignationViewModel.objDesignationList, draw = Request["draw"], recordsTotal = DesignationViewModel.totalRecords, recordsFiltered = DesignationViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}