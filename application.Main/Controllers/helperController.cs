﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using application.Main.Models;
using application.Main.ViewModels;
using RestSharp;
using Newtonsoft.Json;
using System.Text;

namespace application.Main.Controllers
{
    public class helperController : Controller
    {
        // GET: helper
        public JsonResult populateState(int countryId)
        {
            stateViewModel objStateMaster = new stateViewModel();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("state?countryId=" + countryId, Method.GET, null, accessToken);
                objStateMaster = JsonConvert.DeserializeObject<stateViewModel>(response.Content);
                return Json(objStateMaster.objStateMasterList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateCity(int stateId)
        {
            cityViewModel objCityViewModel = new cityViewModel();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("cityMaster?stateId=" + stateId, Method.GET, null, accessToken);
                objCityViewModel = JsonConvert.DeserializeObject<cityViewModel>(response.Content);
                return Json(objCityViewModel.objCityList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateDistributorList(long masterDistributorId)
        {
            List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entity/getDistributorListByMD?masterDistributorId=" + masterDistributorId, Method.GET, null, accessToken);
                objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(response.Content);
                return Json(objEntityMasterList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateEntityListByEntityTypeId(Int32 entityTypeId)
        {
            List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("entity/getEntityByEntityTypeId?entityTypeId=" + entityTypeId, Method.GET, null, accessToken);
                objEntityMasterList = JsonConvert.DeserializeObject<List<tblEntity_Master>>(response.Content);
                return Json(objEntityMasterList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateMainMenuByCompanyId(int companyId)
        {
            List<tblMainMenu_Master> objMainMasterList = new List<tblMainMenu_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenu/getMainMenuByCompanyId?companyId=" + companyId, Method.GET, null, accessToken);
                objMainMasterList = JsonConvert.DeserializeObject<List<tblMainMenu_Master>>(response.Content);
                return Json(objMainMasterList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateRoleByCompanyId(int companyId)
        {
            List<tblRole_Master> objRoleMasterList = new List<tblRole_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("role/getrolebycompanyid?companyId=" + companyId, Method.GET, null, accessToken);
                objRoleMasterList = JsonConvert.DeserializeObject<List<tblRole_Master>>(response.Content);
                return Json(objRoleMasterList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateMainMenuByRole(int roleId)
        {
            List<tblMainMenu_Master> objMainMenuList = new List<tblMainMenu_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("mainMenu/getMainMenuByRoleId?roleId=" + roleId, Method.GET, null, accessToken);
                objMainMenuList = JsonConvert.DeserializeObject<List<tblMainMenu_Master>>(response.Content);
                return Json(objMainMenuList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult populateMainMenuByCompanyPermission(int companyId)
        {
            List<tblMainMenu_Master> objMainMenuList = new List<tblMainMenu_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("SubMenu_Company_Permission/getMainMenuByCompanyId?CompanyId=" + companyId, Method.GET, null, accessToken);
                objMainMenuList = JsonConvert.DeserializeObject<List<tblMainMenu_Master>>(response.Content);
                return Json(objMainMenuList);
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult getEntityAvailableWalletBalance(long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("getWalletBalance4Internal?entityId=" + entityId, Method.GET, null, accessToken);
                string availableBalance = JsonConvert.DeserializeObject<string>(response.Content);
                if (!String.IsNullOrEmpty(availableBalance))
                    return Json(availableBalance);
                else
                    return Json("0");
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public ActionResult populateSubMenuByMainMenuId(int MainMenuId)
        {
            List<tblSubMenu_Master> objSubMenuList = new List<tblSubMenu_Master>();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("subMenuMaster/GettblSubMenu_Master?Id=0&mainMenuId" + MainMenuId, Method.GET, null, accessToken);
                objSubMenuList = JsonConvert.DeserializeObject<List<tblSubMenu_Master>>(response.Content);
                return Json(objSubMenuList);
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }

        public ActionResult populate_HorizontalNavBar()
        {
            horizontalNavBarViewModel objHorizontalNavBar = new horizontalNavBarViewModel();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("Menu_EntityType_Permission/horizontalNavBar", Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objHorizontalNavBar = JsonConvert.DeserializeObject<horizontalNavBarViewModel>(response.Content);
                return PartialView("_HorizontalMenu1", objHorizontalNavBar);
            }
            else { return PartialView("_HorizontalMenu1"); }
        }

        public ActionResult populate_NavBar_Elements()
        {
            string domainName = Helper.getCompanyDomain();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
            HeaderNav objHeaderNav = new HeaderNav();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("getNavBarItems?domainName=" + domainName, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objHeaderNav = JsonConvert.DeserializeObject<HeaderNav>(response.Content);
                return PartialView("_HeaderNav", objHeaderNav);
            }
            else { return PartialView("_HeaderNav"); }
        }

        public ActionResult userLogout()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("loginSession/logoutUser", Method.PUT, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (Helper.doesCookieExist(Helper.userAccessToken))
                    {
                        Helper.clearCookie(Helper.userAccessToken);
                    }
                }
            }
            return RedirectToAction("index", "login");
        }

        public JsonResult verifyBankAccount(long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("verifyBankAccountApprove?entityId=" + entityId, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Ok");
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult checkDMT2PendingTransactionStatus(string clientTransactionId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/checkNUpdatePendingTransactions?orderId=" + clientTransactionId, Method.PUT, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string transactionStatus = JsonConvert.DeserializeObject<string>(response.Content);
                    return Json(transactionStatus);
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult checkDMTPendingTransactionStatus(string clientTransactionId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/checkNUpdatePendingTransactions?clientRefId=" + clientTransactionId, Method.POST, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string transactionStatus = JsonConvert.DeserializeObject<string>(response.Content);
                    return Json(transactionStatus);
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult verifyBankAccountMD(long entityId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("verifyBankAccountApproveMD?entityId=" + entityId, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Ok");
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult approvePayment(long entityPaymentId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("approvePayment?entityPaymentId=" + entityPaymentId, Method.GET, null, accessToken); // Api in entityAccount controller
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Ok");
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult rejectPayment(long entityPaymentId, string rejectReason)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                tblEntity_Payments objEntityPayment = new tblEntity_Payments();
                objEntityPayment.EntityPaymentId = entityPaymentId;
                objEntityPayment.RejectionRemark = rejectReason;
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("rejectPayment", Method.POST, objEntityPayment, accessToken); // Api in entityAccount controller
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Ok");
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }
    }
}