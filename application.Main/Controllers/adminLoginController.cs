﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class adminLoginController : Controller
    {
        // GET: adminLogin
        public ActionResult Index()
        {

            return View();
        }

        public JsonResult validateCredentials(tblAdminLogin objAdminLogin)
        {
            tokenViewModel objToken = new tokenViewModel();
            customResponse objCustomResponse = new customResponse();
            errorDetail objError = new errorDetail();
            string headerHost = ConfigurationManager.AppSettings["webAPIURL"].Replace("http://", "").Replace("https://", "");
            var client = new RestClient(ConfigurationManager.AppSettings["webAPIURL"] + "validateadminlogin");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", headerHost.Replace("/", ""));
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("bodyParam", "username=" + objAdminLogin.LoginName + "&password=" + objAdminLogin.Password + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                objToken = JsonConvert.DeserializeObject<tokenViewModel>(response.Content);
                tblLogin_Session objLoginSession = new tblLogin_Session();
                objLoginSession.accessToken = objToken.access_token;
                objLoginSession.accessTokenValidity = Helper.getIndianTime();
                objLoginSession.accessTokenValidity = objLoginSession.accessTokenValidity.AddDays(1);
                objLoginSession.accessTokenGeneratedDateTime = Helper.getIndianTime();
                IRestResponse restResponse = Helper.WebAPIClient_via_RestSharp("LoginSession", Method.POST, objLoginSession, objToken.access_token);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Helper.createEncryptedCookie(Helper.userAccessToken, objToken.access_token, true);
                    tblLogin_Session objLoginSessionResponse = JsonConvert.DeserializeObject<tblLogin_Session>(restResponse.Content);
                    string dashboardURL = "";
                    if (objLoginSessionResponse.EntityTypeId == Convert.ToInt32(Helper.entityTypes.company) && objLoginSessionResponse.LoginId == 1)
                        dashboardURL = Url.Content("~/adminDashboard");
                    else
                        dashboardURL = Url.Content("~/companyDashboard");
                    return Json(new { success = true, redirectURL = dashboardURL }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + restResponse.StatusCode.ToString() }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                string errorResponse = response.Content.ToString();
                if (!errorResponse.Contains("<!DOCTYPE html"))
                {
                    objError = JsonConvert.DeserializeObject<errorDetail>(response.Content);
                    if (objError != null)
                    {
                        return Json(new { success = false, responseText = "1|" + objError.error_Description }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode.ToString() }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|" + Helper.apiErrorMsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}