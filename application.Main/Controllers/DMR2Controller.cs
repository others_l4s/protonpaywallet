﻿using application.Main.instantpay.dmt.response;
using application.Main.instantpay.dmt.request;
using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using application.Main.BankITDMT.Request;
using application.Main.BankITDMT.Response;
using Microsoft.Ajax.Utilities;
using Fluentx;

namespace application.Main.Controllers
{
    public class DMR2Controller : Controller
    {
        // GET: DMR2
        #region Sender
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("index");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addNewSender(string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                remitter_req objRemitterReq = new remitter_req();
                objRemitterReq.mobile = mobileNo;
                return View("addSender", objRemitterReq);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveSender(remitter_req objRemitterReq)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (!string.IsNullOrEmpty(objRemitterReq.mobile))
            {
                remitter_register_request objremitterRequest = new remitter_register_request();
                objremitterRequest.token = "";
                objremitterRequest.request = objRemitterReq;
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/saveRemitterDetails", Method.POST, objremitterRequest, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string remitterResponse = JsonConvert.DeserializeObject<string>(response.Content);
                    string[] arrRemitterResponse = remitterResponse.Split('|');
                    if (arrRemitterResponse.Length == 1)
                    {
                        TempData[Helper.responseStatus] = arrRemitterResponse[0];
                        return Json(new { success = false, responseText = "1|" + arrRemitterResponse[0] }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (arrRemitterResponse[1] == "1" && arrRemitterResponse[0] == "1")
                        {
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiary?mobileNo=" + objremitterRequest.request.mobile + "&remitterid=" + arrRemitterResponse[2]) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR2/remitterValidate?mobileNo=" + objremitterRequest.request.mobile + "&remitterid=" + arrRemitterResponse[2]) }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ModelState.AddModelError("CompanyName", "Please enter CustomerID");
                return Json(new { success = false, responseText = "1|Please enter CustomerID" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult getSenderList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            BankITSenderViewModel objBankITSenderVM = new BankITSenderViewModel();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITSenderVM = JsonConvert.DeserializeObject<BankITSenderViewModel>(response.Content);
            }
            return Json(new { data = objBankITSenderVM.clsBankItSenderList, draw = Request["draw"], recordsTotal = objBankITSenderVM.totalRecords, recordsFiltered = objBankITSenderVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult verifyMobileOrAccountNumber(DMT_addSender_request objSenderRequest)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);

            if (String.IsNullOrEmpty(objSenderRequest.accountNumber) && string.IsNullOrEmpty(objSenderRequest.customerId))
                return Json(new { success = false, responseText = "1|Please enter Mobile Number or Account Number." }, JsonRequestBehavior.AllowGet);

            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterDetail?mobileno=" + objSenderRequest.customerId + "&accountno=" + objSenderRequest.accountNumber, Method.GET, objSenderRequest, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (response.Content.Contains("Error:"))
                {
                    if (!String.IsNullOrEmpty(objSenderRequest.customerId))
                    {
                        DMT_BankItInDB_Sender objBankITSender = new DMT_BankItInDB_Sender();
                        objBankITSender.MobileNo = objSenderRequest.customerId;
                        IRestResponse VerifySenderMobileResponse = Helper.WebAPIClient_via_RestSharp("bankITDMT/verifySenderMobileOTP", Method.POST, objBankITSender, accessToken);
                        if (VerifySenderMobileResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            if (VerifySenderMobileResponse.Content.Contains("Saved"))
                                return Json(new { success = "Saved", redirectURL = Url.Content("~/DMR2") }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR2/addNewSender?mobileNo=" + objSenderRequest.customerId) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                        return Json(new { success = false, responseText = "1|Sender Mobile No. not found. Please enter Mobile No." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    string senderMobileNo = JsonConvert.DeserializeObject<String>(response.Content);
                    if (senderMobileNo.Split('|')[0] == "True")
                    {
                        if (senderMobileNo.Split('|')[2] == "True")
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiary?mobileNo=" + objSenderRequest.customerId + "&remitterid=" + senderMobileNo.Split('|')[1]) }, JsonRequestBehavior.AllowGet);
                        else
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR2/remitterValidate?mobileNo=" + objSenderRequest.customerId + "&remitterid=" + senderMobileNo.Split('|')[1]) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = true, redirectURL = Url.Content("~/DMR2/addNewSender?mobileNo=" + objSenderRequest.customerId) }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult newSenderVerifyOTP(string mobileNo)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            DMT_BankItInDB_Sender objBankITSender = new DMT_BankItInDB_Sender();
            objBankITSender.MobileNo = mobileNo;
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("bankITDMT/verifySenderMobileOTP", Method.POST, objBankITSender, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (response.Content.Contains("Saved"))
                    return Json(new { success = "Saved", redirectURL = Url.Content("~/DMR") }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, redirectURL = Url.Content("~/DMR/addNewSender?mobileNo=" + mobileNo) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult remitterValidate(string mobileNo, string remitterId)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblInstantPay_Remitter objRemitterData = new tblInstantPay_Remitter();
                IRestResponse remitterDataResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterDetailByRemitterId?remitterId=" + remitterId, Method.GET, null, accessToken);
                if (remitterDataResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objRemitterData = JsonConvert.DeserializeObject<tblInstantPay_Remitter>(remitterDataResponse.Content);
                    objRemitterData.OTP = (objRemitterData.OTP == "0" ? "" : objRemitterData.OTP);
                    return View("remitterVerifyMobile", objRemitterData);
                }
                else
                    return View("remitterVerifyMobile");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult remitterVerifyMobileOTP(tblInstantPay_Remitter objRemitter)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (!String.IsNullOrEmpty(objRemitter.RemitterId))
            {
                if (!string.IsNullOrEmpty(objRemitter.OTP) && objRemitter.OTP != "0")
                {
                    remitter_validate_request objremitterValidateRequest = new remitter_validate_request();
                    objremitterValidateRequest.request = new remitter_validate_req();
                    objremitterValidateRequest.token = "";
                    objremitterValidateRequest.request.remitterid = objRemitter.RemitterId;
                    objremitterValidateRequest.request.mobile = objRemitter.MobileNo;
                    objremitterValidateRequest.request.otp = objRemitter.OTP;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/RemitterValidated", Method.POST, objremitterValidateRequest, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        string validateResponse = JsonConvert.DeserializeObject<string>(response.Content);
                        if (validateResponse.Trim().ToLower().Contains("txn"))
                        {
                            TempData[Helper.responseStatus] = "Beneficiary successfully validated";
                            return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiary?mobileNo=" + objRemitter.MobileNo + "&remitterid=" + objRemitter.RemitterId) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Unable to validate OTP : " + validateResponse + ". Please try again next day" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter OTP" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Invalid Remitter Details." }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Beneficiary
        public ActionResult beneficiary(string mobileNo, string remitterId)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                DMT_BankItInDB_Sender clsBankItSender = new DMT_BankItInDB_Sender();
                clsBankItSender.CustomerID = mobileNo;
                clsBankItSender.remitterId = remitterId;
                return View("listBeneficiary", clsBankItSender);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult beneficiaryVerifyMobile(string beneficiaryId, string mobileNo)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                tblInstantPay_Beneficiary objBeneficiaryData = new tblInstantPay_Beneficiary();
                IRestResponse beneficiaryDataResponse = Helper.WebAPIBankIt_via_RestSharp("instantPayDMT/getBeneficiaryDetailByBeneficiaryId?beneficiaryId=" + beneficiaryId, Method.GET, null, accessToken);
                if (beneficiaryDataResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objBeneficiaryData = JsonConvert.DeserializeObject<tblInstantPay_Beneficiary>(beneficiaryDataResponse.Content);
                    return View("beneficiaryVerifyMobile", objBeneficiaryData);
                }
                else
                    return View("beneficiaryVerifyMobile");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addBeneficiary(string mobileNo, string remitterId)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                InstantPayAddBeneficiaryVM clsBeneficiaryRequest = new InstantPayAddBeneficiaryVM();
                clsBeneficiaryRequest.objBeneficiary = new benefical_register_req();
                clsBeneficiaryRequest.objBeneficiary.IMPSAdditionCharge = Convert.ToInt32(ConfigurationManager.AppSettings["IMPSBeneficiaryCharge"]);
                clsBeneficiaryRequest.objBeneficiary.remitterid = remitterId;
                clsBeneficiaryRequest.objBeneficiary.customerId = mobileNo;
                clsBeneficiaryRequest.objBankList = new List<bank_details_data_resp>();
                clsBeneficiaryRequest.objTransferType = Helper.TransferType();
                return View("addBeneficiary", clsBeneficiaryRequest);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public JsonResult getBankList(string accountNumber)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                InstantPayAddBeneficiaryVM clsBeneficiaryRequest = new InstantPayAddBeneficiaryVM();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                IRestResponse bankListResponse = Helper.WebAPIClient_via_RestSharp("instantPayDMT/GetBankDetails?accountNumber=" + accountNumber, Method.GET, null, accessToken);
                if (bankListResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    bank_details_response bankResponse = JsonConvert.DeserializeObject<bank_details_response>(bankListResponse.Content);
                    clsBeneficiaryRequest.objBankList = bankResponse.data;
                    clsBeneficiaryRequest.objBankList.Select(x => { x.bank_name = x.bank_name + " (" + x.branch_ifsc + ")"; return x; }).ToList();
                    return Json(clsBeneficiaryRequest.objBankList);
                }
                else
                    return Json("");
            }
            else
            {
                return Json("Unauthorized");
            }

        }

        [HttpGet]
        public JsonResult listBeneficiary(string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            InstantPayBeneficiaryVM objBankITRecipientVM = new InstantPayBeneficiaryVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getBeneficiaryFetchAll?RemitterMobileNo=" + customerId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objBankITRecipientVM = JsonConvert.DeserializeObject<InstantPayBeneficiaryVM>(response.Content);
            }
            else
            {
                objBankITRecipientVM.objbeneficiaryList = new List<beneficiary_resp>();
            }
            return Json(new { data = objBankITRecipientVM.objbeneficiaryList, draw = Request["draw"], recordsTotal = objBankITRecipientVM.totalRecords, recordsFiltered = objBankITRecipientVM.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveBeneficiary(InstantPayAddBeneficiaryVM objBeneficiaryReq)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objBeneficiaryReq.objBeneficiary.customerId != "0")
            {
                if (!string.IsNullOrEmpty(objBeneficiaryReq.objBeneficiary.name))
                {
                    if (Helper.hasEnoughWalletBalance(Convert.ToInt32(ConfigurationManager.AppSettings["IMPSBeneficiaryCharge"]), accessToken))
                    {
                        beneficiary_register_request objbeneficiaryRequest = new beneficiary_register_request();
                        objbeneficiaryRequest.token = "";
                        objbeneficiaryRequest.request = objBeneficiaryReq.objBeneficiary;
                        objBeneficiaryReq.objBeneficiary.IMPSAdditionCharge = Convert.ToInt32(ConfigurationManager.AppSettings["IMPSBeneficiaryCharge"]);
                        IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/saveBeneficiaryDetails", Method.POST, objbeneficiaryRequest, accessToken);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            string beneficiaryAddResponse = JsonConvert.DeserializeObject<string>(response.Content);
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            if (beneficiaryAddResponse.Split('|')[0] == "1")
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiary?mobileNo=" + objBeneficiaryReq.objBeneficiary.customerId + "&remitterid=" + objBeneficiaryReq.objBeneficiary.remitterid) }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiaryVerifyMobile?beneficiaryId=" + beneficiaryAddResponse.Split('|')[1] + "&mobileNo=" + objBeneficiaryReq.objBeneficiary.customerId) }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter Beneficiary Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult beneficiaryVerifyMobileOTP(tblInstantPay_Beneficiary objBeneficiary)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (!String.IsNullOrEmpty(objBeneficiary.RemitterId) && !String.IsNullOrEmpty(objBeneficiary.BeneficiaryId))
            {
                if (!string.IsNullOrEmpty(objBeneficiary.OTP) && objBeneficiary.OTP != "0")
                {
                    beneficiary_register_validate_request objBeneficiaryValidateRequest = new beneficiary_register_validate_request();
                    objBeneficiaryValidateRequest.token = "";
                    objBeneficiaryValidateRequest.request.beneficiaryid = objBeneficiary.BeneficiaryId;
                    objBeneficiaryValidateRequest.request.remitterid = objBeneficiary.RemitterId;
                    objBeneficiaryValidateRequest.request.otp = objBeneficiary.OTP;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/BeneficiaryValidated", Method.POST, objBeneficiaryValidateRequest, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = "Beneficiary successfully validated";
                        return Json(new { success = true, redirectURL = Url.Content("~/DMR2/beneficiary?mobileNo=" + objBeneficiary.customerId + "&remitterid=" + objBeneficiary.RemitterId) }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Please enter OTP" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Invalid " }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult fetchSenderDetail(string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterDetailByMobile?mobileno=" + customerId, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (!response.Content.ToString().Contains("false"))
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(response.Content);
                        if (RemitterFetchAllResp.data.remitter_limit != null)
                        {
                            return PartialView("_senderAccountBalance", RemitterFetchAllResp);
                        }
                        else
                        {
                            return PartialView("_senderAccountBalance");
                        }
                    }
                    else
                        return PartialView("_senderAccountBalance");
                }
                else
                    return RedirectToAction("index", new { Controller = "unauthorized" });
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }

        public ActionResult fetchPayoutSenderDetail(string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterPayoutDetailPerMonth?mobileno=" + customerId, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (!response.Content.ToString().Contains("false"))
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(response.Content);
                        if (RemitterFetchAllResp.data.remitter_limit != null)
                        {
                            return PartialView("_senderAccountBalance", RemitterFetchAllResp);
                        }
                        else
                        {
                            return PartialView("_senderAccountBalance");
                        }
                    }
                    else
                        return PartialView("_senderAccountBalance");
                }
                else
                    return RedirectToAction("index", new { Controller = "unauthorized" });
            }
            else
            {
                return RedirectToAction("index", new { Controller = "unauthorized" });
            }
        }

        [HttpPost]
        public JsonResult deleteBeneficiary(string beneficiaryID, string remitterID)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                if (!String.IsNullOrEmpty(beneficiaryID) && !String.IsNullOrEmpty(remitterID))
                {
                    beneficiary_remove_request objBeneficiaryRequest = new beneficiary_remove_request();
                    objBeneficiaryRequest.token = "";
                    objBeneficiaryRequest.request.beneficiaryid = beneficiaryID;
                    objBeneficiaryRequest.request.remitterid = remitterID;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/removeBeneficiary", Method.POST, objBeneficiaryRequest, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        Boolean beneficiaryDeleteResponse = JsonConvert.DeserializeObject<Boolean>(response.Content);
                        if (beneficiaryDeleteResponse)
                        {
                            return Json("true");
                        }
                        else
                        {
                            return Json("false");
                        }
                    }
                    else
                        return Json("unauthorized");
                }
                else
                {
                    return Json("Invalid Beneficiary ID or RemitterID");
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        public JsonResult verifyBeneficiaryAccount(string remitterId, string benAccNo)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantpayDMT/verifyBeneficiaryAccount?remitterId=" + remitterId + "&beneficiaryAccountNo=" + benAccNo, Method.GET, null, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return Json("Ok");
                }
                else
                {
                    return Json(response.Content.ToString());
                }
            }
            else
            {
                return Json("Unauthorized");
            }
        }

        #endregion

        #region Transfer Money

        public ActionResult Transac(string recipient, string customerId, string imps)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                //Fetching Single Beneficiary Data by recipientId and Customer Id
                FetchSingleBeneficiaryRequest objFetchSingleBeneficiaryRequest = new FetchSingleBeneficiaryRequest();
                objFetchSingleBeneficiaryRequest.customerId = customerId;
                objFetchSingleBeneficiaryRequest.recipientId = recipient;
                InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = getRemitterBeneficiaryDetails(recipient, customerId, accessToken);
                if (objSingleBeneficiaryResponseVM != null)
                {
                    if (imps == "0")
                        objSingleBeneficiaryResponseVM.objTransferType.RemoveAt(0);
                    return View("transfer", objSingleBeneficiaryResponseVM);
                }
                else
                    return View("beneficiary", new { mobileNo = customerId, remitterId = recipient });
            }
            else
                return Json("unauthorized");
        }

        public ActionResult TransacConfirmation(FetchSingleBeneficiaryResponseBM objTransferRequest)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                double transferAmount = Convert.ToDouble(objTransferRequest.objRemit.amount);
                transferAmount = transferAmount + (transferAmount * 0.01);
                objTransferRequest.objRemit.amount = transferAmount.ToString();
                return View("transferconfirm", objTransferRequest);
            }
            else
                return Json("unauthorized");
        }

        private InstantPayDMTTransferConfirmationVM getRemitterBeneficiaryDetails(string beneficiaryId, string mobileNo, string accessToken)
        {
            InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = new InstantPayDMTTransferConfirmationVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterBeneficiaryDetail4Transfer?RemitterMobileNo=" + mobileNo, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                InstantPayBeneficiaryVM objInstantPayBeneficiaryVM = JsonConvert.DeserializeObject<InstantPayBeneficiaryVM>(response.Content);
                if (objInstantPayBeneficiaryVM != null)
                {
                    objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType();
                    objSingleBeneficiaryResponseVM.objRemit = new MoneyTransferRequest();
                    objSingleBeneficiaryResponseVM.objRemit.recipientId = objInstantPayBeneficiaryVM.objRemitterDetails.id;
                    objSingleBeneficiaryResponseVM.objRemit.customerId = mobileNo;
                    objSingleBeneficiaryResponseVM.objRemit.recipientName = objInstantPayBeneficiaryVM.objRemitterDetails.name;
                    objSingleBeneficiaryResponseVM.objRemit.bankName = "";
                    objSingleBeneficiaryResponseVM.objRemit.clientRefId = Helper.instantPayDMT_ClientRefId();
                    objSingleBeneficiaryResponseVM.objBeneficiary = new benefical_register_req();
                    objSingleBeneficiaryResponseVM.objRemitter = new remitter_req();
                    if (objInstantPayBeneficiaryVM.objRemitterDetails != null)
                    {
                        objSingleBeneficiaryResponseVM.objRemitter.name = objInstantPayBeneficiaryVM.objRemitterDetails.name;
                        objSingleBeneficiaryResponseVM.objRemitter.mobile = objInstantPayBeneficiaryVM.objRemitterDetails.mobile;
                        objSingleBeneficiaryResponseVM.remaininglimit = objInstantPayBeneficiaryVM.objRemitterDetails.remaininglimit;
                    }
                    if (objInstantPayBeneficiaryVM.objbeneficiaryList.Count > 0)
                    {
                        beneficiary_resp objBeneficiary = new beneficiary_resp();
                        objBeneficiary = objInstantPayBeneficiaryVM.objbeneficiaryList.Where(x => x.id == beneficiaryId).SingleOrDefault();
                        if (objBeneficiary != null)
                        {
                            objSingleBeneficiaryResponseVM.objBeneficiary.beneficiaryid = objBeneficiary.id;
                            objSingleBeneficiaryResponseVM.objBeneficiary.name = objBeneficiary.name;
                            objSingleBeneficiaryResponseVM.objBeneficiary.mobile = objBeneficiary.mobile;
                            objSingleBeneficiaryResponseVM.objBeneficiary.account = objBeneficiary.account;
                            objSingleBeneficiaryResponseVM.objBeneficiary.bank = objBeneficiary.bank;
                            objSingleBeneficiaryResponseVM.objBeneficiary.ifsc = objBeneficiary.ifsc;
                            objSingleBeneficiaryResponseVM.objBeneficiary.imps = objBeneficiary.imps;
                            objSingleBeneficiaryResponseVM.objBeneficiary.remitterid = objInstantPayBeneficiaryVM.objRemitterDetails.id;
                            if (objBeneficiary.imps == "0")
                                objSingleBeneficiaryResponseVM.objTransferType.RemoveAt(0);
                        }
                    }

                    return objSingleBeneficiaryResponseVM;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }



        [HttpPost]
        public ActionResult transferMoneyConfirmation(InstantPayDMTTransferConfirmationVM objTransferRequest)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = getRemitterBeneficiaryDetails(objTransferRequest.objBeneficiary.beneficiaryid, objTransferRequest.objRemit.customerId, accessToken);
                if (objTransferRequest.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransferRequest.objRemit.amount) && Convert.ToDouble(objTransferRequest.objRemit.amount) >= 1)
                    {
                        if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransferRequest.objRemit.amount), accessToken))
                        {
                            double dServiceCharge = (Convert.ToDouble(objTransferRequest.objRemit.amount) * 0.01);
                            double dTransferAmount = Convert.ToDouble(objTransferRequest.objRemit.amount) + (dServiceCharge <= 10 ? 10 : dServiceCharge);
                            objTransferRequest.objRemit.amount = dTransferAmount.ToString();
                            objSingleBeneficiaryResponseVM.objRemit.amount = objTransferRequest.objRemit.amount;
                            objSingleBeneficiaryResponseVM.objRemit.TransType = objTransferRequest.objRemit.TransType;
                            objSingleBeneficiaryResponseVM.objRemit.clientRefId = objTransferRequest.objRemit.clientRefId;
                            objSingleBeneficiaryResponseVM.objRemit.beneficiaryId = objTransferRequest.objBeneficiary.beneficiaryid;
                            return View("transferconfirm", objSingleBeneficiaryResponseVM);
                        }
                        else
                        {
                            //return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                            TempData[Helper.responseStatus] = "1|Oops! Wallet Balance is too Low";
                            return View("transfer", objSingleBeneficiaryResponseVM);
                        }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|Invalid Amount to Transfer. Minimum Amount to Transfer is 100";
                        return View("transfer", objSingleBeneficiaryResponseVM);
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary";
                    return View("transfer", objSingleBeneficiaryResponseVM);
                }
            }
            catch (Exception ex)
            {
                TempData[Helper.responseStatus] = "1|" + ex.Message.ToString();
                return View("transfer");
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transferMoney(InstantPayDMTTransferConfirmationVM objTransfer)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objTransfer.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransfer.objRemit.amount) && Convert.ToDouble(objTransfer.objRemit.amount) >= 50)
                    {
                        if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransfer.objRemit.amount), accessToken))
                        {
                            if (Convert.ToInt64(objTransfer.objRemit.amount) <= 25000)
                            {
                                transfer_request objtransferRequest = new transfer_request();
                                objtransferRequest.token = "";
                                objtransferRequest.request = new transfer_req();
                                objtransferRequest.request.beneficiaryid = objTransfer.objBeneficiary.beneficiaryid;
                                objtransferRequest.request.agentid = objTransfer.objRemit.clientRefId;
                                objtransferRequest.request.remittermobile = objTransfer.objRemit.customerId;
                                objtransferRequest.request.amount = objTransfer.objRemit.amount;
                                objtransferRequest.request.mode = objTransfer.objRemit.TransType.ToUpper();
                                objtransferRequest.fromMobileDevice = false;
                                IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/FundTransfer", Method.POST, objtransferRequest, accessToken);
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    string moneyTransferResponse = JsonConvert.DeserializeObject<string>(response.Content);
                                    if (moneyTransferResponse.Trim().ToLower().Contains("successful"))
                                        TempData[Helper.responseStatus] = "0|Transaction has been successfully processed.";
                                    else
                                        TempData[Helper.responseStatus] = "1|" + moneyTransferResponse;
                                    return Json(new { success = true, redirectURL = Url.Content("~/DMR2/showTransactions?mobileNo=" + objTransfer.objRemit.customerId + "&remitterId=" + objTransfer.objBeneficiary.remitterid) }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code:" + response.StatusCode }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { success = false, responseText = "1|Oops! Max Transfer limit is 5000" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        protected string instantPayMoneyTransfer(transfer_request objtransferRequest, string accessToken)
        {
            customResponse objCustomResponse = new customResponse();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/FundTransfer", Method.POST, objtransferRequest, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return "0|success";
            }
            else
            {
                objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                return "1|" + objCustomResponse.Message + " Status Code:" + response.StatusCode;
            }
        }


        #endregion

        #region  Transaction

        public ActionResult showTransactions(string mobileNo, string remitterId)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                DMT_BankItInDB_Sender clsBankItSender = new DMT_BankItInDB_Sender();
                clsBankItSender.CustomerID = mobileNo;
                clsBankItSender.remitterId = remitterId;
                return View("transactions", clsBankItSender);
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult showTransactionReceipt(string Id)
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (!String.IsNullOrEmpty(Id))
                {
                    transactionReceipt objTransactionReceipt = new transactionReceipt();
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/transactionsReceipt?instantPayTransactionId=" + Id, Method.GET, null, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objTransactionReceipt = JsonConvert.DeserializeObject<transactionReceipt>(response.Content);
                        return View("transactionReceipt", objTransactionReceipt);
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|Unable to Fetch Data";
                        return View("transactions");
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Invalid Client RefId";
                    return View("transactions");
                }
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult transactionHistory()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
            {
                return View("history");
            }
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        [HttpGet]
        public JsonResult listTransactions(string customerId, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/transactions?customerId=" + customerId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<InstantPayDMTTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objInstantPayDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult showTransactionHistory(string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/transactionsHistory?searchEntityId=0&pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection + "&fromDate=" + fromDate + "&toDate=" + toDate + "&transactionType=" + transactionType + "&transactionStatus=" + transactionStatus, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objTransactions = JsonConvert.DeserializeObject<InstantPayDMTTransactionsVM>(response.Content);
            }
            return Json(new { data = objTransactions.objInstantPayDMTTransactionsList, draw = Request["draw"], recordsTotal = objTransactions.totalRecords, recordsFiltered = objTransactions.filterRecords }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region check pending transaction

        public string checkPendingTransaction(string ClientRefID, string Status, string BankRefNo)
        {
            return "Status:" + Status;
        }

        #endregion

        #region Transfer Via Payout Direct
        public ActionResult PayoutDirect(string recipient, string customerId)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (!String.IsNullOrEmpty(accessToken))
            {
                //Fetching Single Beneficiary Data by recipientId and Customer Id
                FetchSingleBeneficiaryRequest objFetchSingleBeneficiaryRequest = new FetchSingleBeneficiaryRequest();
                objFetchSingleBeneficiaryRequest.customerId = customerId;
                objFetchSingleBeneficiaryRequest.recipientId = recipient;
                InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = getRemitterBeneficiaryDetails4PayoutDirect(recipient, customerId, accessToken);
                if (objSingleBeneficiaryResponseVM != null)
                {
                    objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType_PayoutDirect();
                    return View("transferPayoutDirect", objSingleBeneficiaryResponseVM);
                }
                else
                    return View("beneficiary", new { mobileNo = customerId, remitterId = recipient });
            }
            else
                return Json("unauthorized");
        }

        public ActionResult PayoutDirectConfirmation(InstantPayDMTTransferConfirmationVM objTransferRequest)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = getRemitterBeneficiaryDetails4PayoutDirect(objTransferRequest.objBeneficiary.beneficiaryid, objTransferRequest.objRemit.customerId, accessToken);
                if (objTransferRequest.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransferRequest.objRemit.amount) && Convert.ToDouble(objTransferRequest.objRemit.amount) >= 100)
                    {
                        if (!string.IsNullOrEmpty(objTransferRequest.objBeneficiary.Latitude) && !string.IsNullOrEmpty(objTransferRequest.objBeneficiary.Longitude))
                        {
                            if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransferRequest.objRemit.amount), accessToken))
                            {
                                objSingleBeneficiaryResponseVM.objRemit.amount = objTransferRequest.objRemit.amount;
                                objSingleBeneficiaryResponseVM.objRemit.TransType = objTransferRequest.objRemit.TransType;
                                objSingleBeneficiaryResponseVM.objRemit.TypeName = Helper.TransferType_PayoutDirect().SingleOrDefault(x => x.TransType == objTransferRequest.objRemit.TransType).TypeName;
                                objSingleBeneficiaryResponseVM.objRemit.clientRefId = objTransferRequest.objRemit.clientRefId;
                                objSingleBeneficiaryResponseVM.objRemit.beneficiaryId = objTransferRequest.objBeneficiary.beneficiaryid;
                                return View("transferConfirmationPayoutDirect", objSingleBeneficiaryResponseVM);
                            }
                            else
                            {
                                TempData[Helper.responseStatus] = "1|Oops! Wallet Balance is too Low";
                                objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType_PayoutDirect();
                                return View("transferPayoutDirect", objSingleBeneficiaryResponseVM);
                            }
                        }
                        else
                        {
                            TempData[Helper.responseStatus] = "1|Invalid Location. Please Allow Location Access if you want to use Payout Direct Service";
                            objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType_PayoutDirect();
                            return View("transferPayoutDirect", objSingleBeneficiaryResponseVM);
                        }
                    }
                    else
                    {
                        TempData[Helper.responseStatus] = "1|Invalid Amount to Transfer. Minimum Amount to Transfer is 100";
                        objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType_PayoutDirect();
                        return View("transferPayoutDirect", objSingleBeneficiaryResponseVM);
                    }
                }
                else
                {
                    TempData[Helper.responseStatus] = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary";
                    objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType_PayoutDirect();
                    return View("transferPayoutDirect", objSingleBeneficiaryResponseVM);
                }
            }
            catch (Exception ex)
            {
                TempData[Helper.responseStatus] = "1|" + ex.Message.ToString();
                return View("transferPayoutDirect");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult transferMoneyPayoutDirect(InstantPayDMTTransferConfirmationVM objTransfer)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                if (objTransfer.objRemit.customerId != "0")
                {
                    if (!string.IsNullOrEmpty(objTransfer.objRemit.amount) && Convert.ToDouble(objTransfer.objRemit.amount) >= 100)
                    {
                        if (!string.IsNullOrEmpty(objTransfer.objBeneficiary.Latitude) && !String.IsNullOrEmpty(objTransfer.objBeneficiary.Longitude))
                        {
                            string latitude = objTransfer.objBeneficiary.Latitude.Split('.')[0] + "." + objTransfer.objBeneficiary.Latitude.Split('.')[1].Substring(0, (objTransfer.objBeneficiary.Latitude.Split('.')[1].Length > 4 ? 4 : objTransfer.objBeneficiary.Latitude.Split('.')[1].Length));
                            string longitude = objTransfer.objBeneficiary.Longitude.Split('.')[0] + "." + objTransfer.objBeneficiary.Longitude.Split('.')[1].Substring(0, (objTransfer.objBeneficiary.Longitude.Split('.')[1].Length > 4 ? 4 : objTransfer.objBeneficiary.Longitude.Split('.')[1].Length));
                            objTransfer.objBeneficiary.Latitude = latitude;
                            objTransfer.objBeneficiary.Longitude = longitude;
                            if (Helper.hasEnoughWalletBalance(Convert.ToDecimal(objTransfer.objRemit.amount), accessToken))
                            {
                                if (Convert.ToInt64(objTransfer.objRemit.amount) <= 49999)
                                {
                                    payoutDirect_Request objtransferRequest = new payoutDirect_Request();
                                    objtransferRequest.Token = "";
                                    objtransferRequest.Request = new Request();
                                    objtransferRequest.Request.sp_key = objTransfer.objRemit.TransType;
                                    objtransferRequest.Request.TransType = objTransfer.objRemit.TypeName;
                                    objtransferRequest.Request.external_ref = objTransfer.objRemit.clientRefId;
                                    objtransferRequest.Request.credit_account = objTransfer.objBeneficiary.account;
                                    objtransferRequest.Request.credit_amount = objTransfer.objRemit.amount;
                                    objtransferRequest.Request.ifs_code = objTransfer.objBeneficiary.ifsc;
                                    objtransferRequest.Request.bene_name = objTransfer.objBeneficiary.name;
                                    objtransferRequest.Request.latitude = objTransfer.objBeneficiary.Latitude;
                                    objtransferRequest.Request.longitude = objTransfer.objBeneficiary.Longitude;
                                    objtransferRequest.Request.endpoint_ip = Helper.GetIp();
                                    objtransferRequest.Request.alert_email = string.Empty;
                                    objtransferRequest.Request.alert_mobile = string.Empty;
                                    objtransferRequest.Request.beneficiaryId = objTransfer.objBeneficiary.beneficiaryid;
                                    objtransferRequest.Request.remitterMobileNo = objTransfer.objRemit.customerId;
                                    objtransferRequest.Request.fromMobileDevice = false;
                                    objtransferRequest.Request.remarks = "Payment Transfer to " + objTransfer.objBeneficiary.name;
                                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/PayoutDirect", Method.POST, objtransferRequest, accessToken);
                                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                    {
                                        string moneyTransferResponse = JsonConvert.DeserializeObject<string>(response.Content);
                                        if (moneyTransferResponse.Trim().ToLower().Contains("transaction successful"))
                                            TempData[Helper.responseStatus] = "0|Transaction has been successfully processed.";
                                        else
                                            TempData[Helper.responseStatus] = "1|" + moneyTransferResponse;
                                        return Json(new { success = true, redirectURL = Url.Content("~/DMR2/showTransactions?mobileNo=" + objTransfer.objRemit.customerId + "&remitterId=" + objTransfer.objBeneficiary.remitterid) }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                    {
                                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code:" + response.StatusCode }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                                else
                                {
                                    return Json(new { success = false, responseText = "1|Oops! Max Transfer limit is 4999" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { success = false, responseText = "1|Oops! Wallet Balance is too Low" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new { success = false, responseText = "1|Invalid Location Access. Please allow browser to Access your Location if you want to use Pay Direct Service." }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Invalid Amount to Transfer. Kindly input proper amount" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Invalid Sender. Kindly select Sender in order to Add Beneficiary" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }

        private InstantPayDMTTransferConfirmationVM getRemitterBeneficiaryDetails4PayoutDirect(string beneficiaryId, string mobileNo, string accessToken)
        {
            InstantPayDMTTransferConfirmationVM objSingleBeneficiaryResponseVM = new InstantPayDMTTransferConfirmationVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("instantPayDMT/getRemitterBeneficiaryDetail4DirectPayout?RemitterMobileNo=" + mobileNo, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                InstantPayBeneficiaryVM objInstantPayBeneficiaryVM = JsonConvert.DeserializeObject<InstantPayBeneficiaryVM>(response.Content);
                if (objInstantPayBeneficiaryVM != null)
                {
                    objSingleBeneficiaryResponseVM.objTransferType = Helper.TransferType();
                    objSingleBeneficiaryResponseVM.objRemit = new MoneyTransferRequest();
                    objSingleBeneficiaryResponseVM.objRemit.recipientId = objInstantPayBeneficiaryVM.objRemitterDetails.id;
                    objSingleBeneficiaryResponseVM.objRemit.customerId = mobileNo;
                    objSingleBeneficiaryResponseVM.objRemit.recipientName = objInstantPayBeneficiaryVM.objRemitterDetails.name;
                    objSingleBeneficiaryResponseVM.objRemit.bankName = "";
                    objSingleBeneficiaryResponseVM.objRemit.clientRefId = Helper.instantPayDMT_ClientRefId();
                    objSingleBeneficiaryResponseVM.objBeneficiary = new benefical_register_req();
                    objSingleBeneficiaryResponseVM.objRemitter = new remitter_req();
                    if (objInstantPayBeneficiaryVM.objRemitterDetails != null)
                    {
                        objSingleBeneficiaryResponseVM.objRemitter.name = objInstantPayBeneficiaryVM.objRemitterDetails.name;
                        objSingleBeneficiaryResponseVM.objRemitter.mobile = objInstantPayBeneficiaryVM.objRemitterDetails.mobile;
                        objSingleBeneficiaryResponseVM.remaininglimit = objInstantPayBeneficiaryVM.objRemitterDetails.remaininglimit;
                    }
                    if (objInstantPayBeneficiaryVM.objbeneficiaryList.Count > 0)
                    {
                        beneficiary_resp objBeneficiary = new beneficiary_resp();
                        objBeneficiary = objInstantPayBeneficiaryVM.objbeneficiaryList.Where(x => x.id == beneficiaryId).SingleOrDefault();
                        if (objBeneficiary != null)
                        {
                            objSingleBeneficiaryResponseVM.objBeneficiary.beneficiaryid = objBeneficiary.id;
                            objSingleBeneficiaryResponseVM.objBeneficiary.name = objBeneficiary.name;
                            objSingleBeneficiaryResponseVM.objBeneficiary.mobile = objBeneficiary.mobile;
                            objSingleBeneficiaryResponseVM.objBeneficiary.account = objBeneficiary.account;
                            objSingleBeneficiaryResponseVM.objBeneficiary.bank = objBeneficiary.bank;
                            objSingleBeneficiaryResponseVM.objBeneficiary.ifsc = objBeneficiary.ifsc;
                            objSingleBeneficiaryResponseVM.objBeneficiary.imps = objBeneficiary.imps;
                            objSingleBeneficiaryResponseVM.objBeneficiary.remitterid = objInstantPayBeneficiaryVM.objRemitterDetails.id;
                            if (objBeneficiary.imps == "0")
                                objSingleBeneficiaryResponseVM.objTransferType.RemoveAt(0);
                        }
                    }

                    return objSingleBeneficiaryResponseVM;
                }
                else
                {
                    return null;
                }
            }
            else
                return null;
        }
        #endregion
    }
}