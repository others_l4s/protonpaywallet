﻿using application.Main.Models;
using application.Main.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using System.IO;

namespace application.Main.Controllers
{
    public class companyController : Controller
    {
        // GET: company
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            companyMasterViewModel objCompanyMasterList = new companyMasterViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("company", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objCompanyMasterList = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/" + id, Method.GET, null, accessToken);
                objCompanyMasterList = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
                if (objCompanyMasterList.objCompanyUser != null)
                    objCompanyMasterList.objCompanyUser.Password = Helper.unProtectString(objCompanyMasterList.objCompanyUser.Password);
            }
            else
            {
                objCompanyMasterList.objCompany.StatusId = (objCompanyMasterList.objStatusList.Count() > 0 ? objCompanyMasterList.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objCompanyMasterList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCompany(companyMasterViewModel objCompanyMasterViewModel)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            Boolean recordSaved = false;
            tblCompany_Master objCompany = new tblCompany_Master();
            IRestResponse webAPIResponse;
            if (objCompanyMasterViewModel.objCompany.CompanyId == 0)
            {
                if (!string.IsNullOrEmpty(objCompanyMasterViewModel.objCompany.CompanyName))
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("company", Method.POST, objCompanyMasterViewModel.objCompany, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        recordSaved = true;
                        //TempData[Helper.responseStatus] = Helper.recordSaved;
                        //return Json(new { success = true, redirectURL = Url.Content("~/company") }, JsonRequestBehavior.AllowGet);
                        objCompany = JsonConvert.DeserializeObject<tblCompany_Master>(webAPIResponse.Content);
                    }
                    else
                        return Json(new { success = false, responseText = Helper.recordAlreadyExists + " " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Company Name");
                    return Json(new { success = false, responseText = "Please enter Company Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/" + objCompanyMasterViewModel.objCompany.CompanyId, Method.PUT, objCompanyMasterViewModel.objCompany, accessToken);
                if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    recordSaved = true;
                    //TempData[Helper.responseStatus] = Helper.recordUpdated;
                    //return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/company") }, JsonRequestBehavior.AllowGet);
                    objCompany = JsonConvert.DeserializeObject<tblCompany_Master>(webAPIResponse.Content);
                }
                else
                    return Json(new { success = false, responseText = Helper.recordAlreadyExists + " " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
            }

            if (recordSaved)
            {
                if (objCompany != null)
                {
                    objCompanyMasterViewModel.objCompanyUser.CompanyId = objCompany.CompanyId;
                    objCompanyMasterViewModel.objCompanyUser.StateId = objCompany.StatusId;
                    string leadUserCreatedStatus = Helper.createCompanyLeadUser(objCompanyMasterViewModel.objCompanyUser, accessToken);
                    if (leadUserCreatedStatus == "OK")
                    {
                        return Json(new { success = true, redirectURL = Url.Content("~/company") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = false, responseText = leadUserCreatedStatus }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, responseText = "1|Oops something went wrong while creating Lead User" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Oops something went wrong. Please try again later." }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteCompany(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        public JsonResult getCompanyList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            companyMasterViewModel companyMasterViewModel = new companyMasterViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("company?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                companyMasterViewModel = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
                return Json(new { data = companyMasterViewModel.objCompanyList, draw = Request["draw"], recordsTotal = companyMasterViewModel.totalRecords, recordsFiltered = companyMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                companyMasterViewModel = new companyMasterViewModel();
                return Json(new { data = companyMasterViewModel.objCompanyList, draw = Request["draw"], recordsTotal = companyMasterViewModel.totalRecords, recordsFiltered = companyMasterViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }

        #region Company Profile
        public ActionResult companyProfile()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            companyMasterViewModel objCompanyMasterList = new companyMasterViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("company", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objCompanyMasterList = JsonConvert.DeserializeObject<companyMasterViewModel>(webAPIResponse.Content);
            }
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/companyprofile", Method.GET, null, accessToken);
            objCompanyMasterList.objCompany = JsonConvert.DeserializeObject<tblCompany_Master>(webAPIResponse.Content);
            objCompanyMasterList.objCompany.StatusId = (objCompanyMasterList.objStatusList.Count() > 0 ? objCompanyMasterList.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            return View("companyprofile", objCompanyMasterList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult companyProfileSave(companyMasterViewModel objCompanyMasterViewModel)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse webAPIResponse;
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/" + objCompanyMasterViewModel.objCompany.CompanyId, Method.PUT, objCompanyMasterViewModel.objCompany, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                TempData[Helper.responseStatus] = "0|Company Profile Successfully Updated";
                return Json(new { success = true, responseText = "0|Company Profile Successfully Updated" }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false, responseText = Helper.recordAlreadyExists + " " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Company Logo

        public ActionResult companyLogo()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            companyMasterViewModel objCompanyMasterVM = new companyMasterViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("company/companyprofile", Method.GET, null, accessToken);
            objCompanyMasterVM.objCompany = JsonConvert.DeserializeObject<tblCompany_Master>(webAPIResponse.Content);
            if (objCompanyMasterVM.objCompany.CompanyLogo == null)
                objCompanyMasterVM.objCompany.CompanyLogo = "../CompanyLogo/NoImages.png";
            else
                objCompanyMasterVM.objCompany.CompanyLogo = "../CompanyLogo/" + objCompanyMasterVM.objCompany.CompanyLogo;
            return View("companyLogo", objCompanyMasterVM);
        }

        [HttpPost]
        public JsonResult saveCompanyLogo(companyMasterViewModel objCompanyVM)
        {
            try
            {
                customResponse objCustomResponse = new customResponse();
                string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
                if (String.IsNullOrEmpty(accessToken))
                    return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

                string FileName = Path.GetFileNameWithoutExtension(objCompanyVM.ImageFile.FileName);
                string Extension = Path.GetExtension(objCompanyVM.ImageFile.FileName);
                FileName = objCompanyVM.objCompany.CompanyId + "_logo" + Extension.ToString();

                objCompanyVM.objCompany.CompanyLogo = FileName;
                FileName = Path.Combine(Server.MapPath("../CompanyLogo/"), FileName);
                objCompanyVM.ImageFile.SaveAs(FileName);
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("company/saveCompanyLogo", Method.PUT, objCompanyVM.objCompany, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = "0|Company Logo Successfully Uploaded";
                    return Json(new { success = true, responseText = "0|Company Logo Successfully Uploaded" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseText = "1|Oops something went wrong, kindly contact Technical Support Team. Status Code :" + ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Company User Edit Profile

        public ActionResult editprofile()
        {
            adminLoginViewModel objAdminLoginList = new adminLoginViewModel();
            try
            {
                string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
                if (!String.IsNullOrEmpty(accessToken))
                {
                    stateViewModel objStateMaster = new stateViewModel();
                    IRestResponse response1 = Helper.WebAPIClient_via_RestSharp("getEntityProfile", Method.GET, null, accessToken);
                    if (response1.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objAdminLoginList = JsonConvert.DeserializeObject<adminLoginViewModel>(response1.Content);
                        if (objAdminLoginList.objAdminLogin != null)
                        {
                            objAdminLoginList.objAdminLogin.Password = Helper.unProtectString(objAdminLoginList.objAdminLogin.Password);
                        }
                    }
                }
                else
                {
                    return RedirectToAction("index", new { Controller = "unauthorized" });
                }
            }
            catch (Exception)
            {
            }
            return View("profile", objAdminLoginList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult updateProfile(adminLoginViewModel objadminLoginViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.getCookieValueIfItExists(Helper.userAccessToken);
            if (!string.IsNullOrEmpty(accessToken))
            {
                objadminLoginViewModel.objAdminLogin.Password = Helper.protectString(objadminLoginViewModel.objAdminLogin.Password);
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("adminLogins/" + objadminLoginViewModel.objAdminLogin.AdminLoginId, Method.PUT, objadminLoginViewModel.objAdminLogin, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = "0|Profile successfully Saved" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}