﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Newtonsoft.Json;
using application.Main.Models;
using application.Main.ViewModels;

namespace application.Main.Controllers
{
    public class entityBankController : Controller
    {
        // GET: entityBank
        public ActionResult Index()
        {
            if (Helper.doesCookieExist(Helper.userAccessToken))
                return View("list");
            else
                return RedirectToAction("index", new { Controller = "unauthorized" });
        }

        public ActionResult addEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponse;
            entityBankDetailsViewModel objEntityBankDetails = new entityBankDetailsViewModel();
            webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails", Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objEntityBankDetails = JsonConvert.DeserializeObject<entityBankDetailsViewModel>(webAPIResponse.Content);
            }
            if (id > 0)
            {
                webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails/" + id, Method.GET, null, accessToken);
                objEntityBankDetails.objEntityBankDetails = JsonConvert.DeserializeObject<tblEntity_Bank_Details>(webAPIResponse.Content);
            }
            else
            {
                objEntityBankDetails.objEntityBankDetails.StatusId = (objEntityBankDetails.objStatusList.Count() > 0 ? objEntityBankDetails.objStatusList.SingleOrDefault(s => s.StatusName.Trim().ToLower() == Helper.defaultActiveStatus.Trim().ToLower()).StatusId : 0);
            }
            return View("index", objEntityBankDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveEntityBankDetail(entityBankDetailsViewModel objEntityBankViewModel)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            IRestResponse webAPIResponse;
            if (ModelState.IsValid)
            {
                if (objEntityBankViewModel.objEntityBankDetails.BankId == 0)
                {
                    if (!string.IsNullOrEmpty(objEntityBankViewModel.objEntityBankDetails.BankName))
                    {
                        webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails", Method.POST, objEntityBankViewModel.objEntityBankDetails, accessToken);
                        if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            TempData[Helper.responseStatus] = Helper.recordSaved;
                            return Json(new { success = true, redirectURL = Url.Content("~/entityBank") }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                            return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, responseText = "1|Please enter Bank Name" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails/" + objEntityBankViewModel.objEntityBankDetails.BankId, Method.PUT, objEntityBankViewModel.objEntityBankDetails, accessToken);
                    if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordUpdated;
                        return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/entityBank") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(webAPIResponse.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + webAPIResponse.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json(new { success = false, responseText = "1|Unable to Update Data. ModelStatus is Invalid" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult deleteEntityBankDetail(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails/" + id, Method.DELETE, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(webAPIResponse.StatusCode.ToString());
            }
        }

        [HttpPost]
        public JsonResult getEntityBankDetailList()
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            //Server Side Parameter
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            entityBankDetailsViewModel entityBankDetailsViewModel = new entityBankDetailsViewModel();
            IRestResponse webAPIResponse = Helper.WebAPIClient_via_RestSharp("entityBankDetails?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (webAPIResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                entityBankDetailsViewModel = JsonConvert.DeserializeObject<entityBankDetailsViewModel>(webAPIResponse.Content);
                return Json(new { data = entityBankDetailsViewModel.objEntityBankDetailsList, draw = Request["draw"], recordsTotal = entityBankDetailsViewModel.totalRecords, recordsFiltered = entityBankDetailsViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                entityBankDetailsViewModel = new entityBankDetailsViewModel();
                return Json(new { data = entityBankDetailsViewModel.objEntityBankDetailsList, draw = Request["draw"], recordsTotal = entityBankDetailsViewModel.totalRecords, recordsFiltered = entityBankDetailsViewModel.filterRecords }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}