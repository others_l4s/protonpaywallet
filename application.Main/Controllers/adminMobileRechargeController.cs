﻿using application.Main.Models;
using application.Main.ViewModels;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Controllers
{
    public class adminMobileRechargeController : Controller
    {
        // GET: adminMobileRecharge
        public ActionResult Index()
        {
            return View();
        }

        #region OperatorType
        public ActionResult operatorType()
        {
            return View("operatorTypeListView");
        }

        public ActionResult operatorType_AddEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            mobileRecharge_OperatorTypeVM objMobileRechargeOperatorTypeVM = new mobileRecharge_OperatorTypeVM();
            if (id > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_OperatorType/" + id, Method.GET, null, accessToken);
                objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType = JsonConvert.DeserializeObject<tblMobileRecharge_OperatorType>(response.Content);
                if (String.IsNullOrEmpty(objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeImage))
                    objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeImage = Url.Content("~/KYCImages/NoImages.png");
                else
                    objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeImage = Url.Content("~/UtilityPayments/" + objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeImage);
            }
            else
            {
                objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorType = new tblMobileRecharge_OperatorType();
            }
            return View("operatorTypeIndex", objMobileRechargeOperatorTypeVM);
        }

        [HttpGet]
        public JsonResult getOperatorTypeList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            mobileRecharge_OperatorTypeVM mobileRechargeOperatorTypeVM = new mobileRecharge_OperatorTypeVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_OperatorType?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                mobileRechargeOperatorTypeVM = JsonConvert.DeserializeObject<mobileRecharge_OperatorTypeVM>(response.Content);
                return Json(new { data = mobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList, draw = Request["draw"], recordsTotal = mobileRechargeOperatorTypeVM.totalRecords, recordsFiltered = mobileRechargeOperatorTypeVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mobileRechargeOperatorTypeVM = new mobileRecharge_OperatorTypeVM();
                return Json(new { data = mobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList, draw = Request["draw"], recordsTotal = mobileRechargeOperatorTypeVM.totalRecords, recordsFiltered = mobileRechargeOperatorTypeVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveOperatorType(mobileRecharge_OperatorTypeVM mobileRechargeOperatorTypeVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            string operatorTypePhotoName = "", operatorTypePhotoExtension = "";
            if (mobileRechargeOperatorTypeVM.HPOperatorTypeImage != null)
            {
                operatorTypePhotoName = Path.GetFileNameWithoutExtension(mobileRechargeOperatorTypeVM.HPOperatorTypeImage.FileName);
                operatorTypePhotoExtension = Path.GetExtension(mobileRechargeOperatorTypeVM.HPOperatorTypeImage.FileName);
                operatorTypePhotoName = Helper.GetRandomPasswordUsingGUID(10) + operatorTypePhotoExtension;
                mobileRechargeOperatorTypeVM.HPOperatorTypeImage.SaveAs(Path.Combine(Server.MapPath("../UtilityPayments/"), operatorTypePhotoName));
                mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeImage = operatorTypePhotoName;
            }

            if (mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeId == 0)
            {
                if (!string.IsNullOrEmpty(mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeName))
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_OperatorType", Method.POST, mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminMobileRecharge/operatorType") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Operator Type Name");
                    return Json(new { success = false, responseText = "Please enter Operator Type Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_OperatorType/" + mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType.OperatorTypeId, Method.PUT, mobileRechargeOperatorTypeVM.objMobileRechargeOperatorType, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminMobileRecharge/operatorType") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult deleteOperatorType(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_OperatorType/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }
        #endregion

        #region Circle

        public ActionResult circle()
        {
            return View("circleListView");
        }

        public ActionResult circleAddEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            mobileRechargeCircleVM objMobileRechargeCircleVM = new mobileRechargeCircleVM();
            if (id > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Circle/" + id, Method.GET, null, accessToken);
                objMobileRechargeCircleVM.objMobileRechargeCircle = JsonConvert.DeserializeObject<tblMobileRecharge_Circle>(response.Content);
            }
            else
            {
                objMobileRechargeCircleVM.objMobileRechargeCircle = new tblMobileRecharge_Circle();
            }
            return View("circleIndex", objMobileRechargeCircleVM);
        }

        [HttpGet]
        public JsonResult getCircleList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            mobileRechargeCircleVM objMobileRechargeCircleVM = new mobileRechargeCircleVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Circle?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMobileRechargeCircleVM = JsonConvert.DeserializeObject<mobileRechargeCircleVM>(response.Content);
                return Json(new { data = objMobileRechargeCircleVM.objMobileRechargeCircleList, draw = Request["draw"], recordsTotal = objMobileRechargeCircleVM.totalRecords, recordsFiltered = objMobileRechargeCircleVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMobileRechargeCircleVM = new mobileRechargeCircleVM();
                return Json(new { data = objMobileRechargeCircleVM.objMobileRechargeCircleList, draw = Request["draw"], recordsTotal = objMobileRechargeCircleVM.totalRecords, recordsFiltered = objMobileRechargeCircleVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveCircle(mobileRechargeCircleVM objMobileRechargeCircleVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objMobileRechargeCircleVM.objMobileRechargeCircle.CircleId == 0)
            {
                if (!string.IsNullOrEmpty(objMobileRechargeCircleVM.objMobileRechargeCircle.CircleName))
                {
                    objMobileRechargeCircleVM.objMobileRechargeCircle.OperatorId = 0;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Circle", Method.POST, objMobileRechargeCircleVM.objMobileRechargeCircle, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminMobileRecharge/circle") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Circle Name");
                    return Json(new { success = false, responseText = "Please enter Circle Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Circle/" + objMobileRechargeCircleVM.objMobileRechargeCircle.CircleId, Method.PUT, objMobileRechargeCircleVM.objMobileRechargeCircle, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminMobileRecharge/circle") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult deleteCircle(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Circle/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        #endregion

        #region Operator

        public ActionResult mobileOperator()
        {
            return View("mobileOperatorListView");
        }

        public ActionResult mobileOperatorAddEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            mobileRechargeOperatorVM objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
            if (id > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator/" + id, Method.GET, null, accessToken);
                objMobileRechargeOperatorVM = JsonConvert.DeserializeObject<mobileRechargeOperatorVM>(response.Content);
                if (String.IsNullOrEmpty(objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorImage))
                {
                    objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorImage = Url.Content("~/KYCImages/NoImages.png");
                }
                else
                {
                    objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorImage = Url.Content("~/UtilityPayments/" + objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorImage);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator", Method.GET, null, accessToken);
                objMobileRechargeOperatorVM = JsonConvert.DeserializeObject<mobileRechargeOperatorVM>(response.Content);
            }
            return View("mobileOperatorIndex", objMobileRechargeOperatorVM);
        }

        [HttpGet]
        public JsonResult getMobileOperatorList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            mobileRechargeOperatorVM objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMobileRechargeOperatorVM = JsonConvert.DeserializeObject<mobileRechargeOperatorVM>(response.Content);
                return Json(new { data = objMobileRechargeOperatorVM.objMobileRechargeOperatorList, draw = Request["draw"], recordsTotal = objMobileRechargeOperatorVM.totalRecords, recordsFiltered = objMobileRechargeOperatorVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
                return Json(new { data = objMobileRechargeOperatorVM.objMobileRechargeOperatorList, draw = Request["draw"], recordsTotal = objMobileRechargeOperatorVM.totalRecords, recordsFiltered = objMobileRechargeOperatorVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveMobileOperator(mobileRechargeOperatorVM objMobileRechargeOperatorVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            string operatorPhotoName = "", operatorPhotoExtension = "";
            if (objMobileRechargeOperatorVM.HPOperatorImage != null)
            {
                operatorPhotoName = Path.GetFileNameWithoutExtension(objMobileRechargeOperatorVM.HPOperatorImage.FileName);
                operatorPhotoExtension = Path.GetExtension(objMobileRechargeOperatorVM.HPOperatorImage.FileName);
                operatorPhotoName = Helper.GetRandomPasswordUsingGUID(10) + operatorPhotoExtension;
                objMobileRechargeOperatorVM.HPOperatorImage.SaveAs(Path.Combine(Server.MapPath("../UtilityPayments/"), operatorPhotoName));
                objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorImage = operatorPhotoName;
            }

            if (objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorId == 0)
            {
                if (!string.IsNullOrEmpty(objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorName))
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator", Method.POST, objMobileRechargeOperatorVM.objMobileRechargeOperator, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminMobileRecharge/mobileOperator") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Operator Name");
                    return Json(new { success = false, responseText = "Please enter Operator Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator/" + objMobileRechargeOperatorVM.objMobileRechargeOperator.OperatorId, Method.PUT, objMobileRechargeOperatorVM.objMobileRechargeOperator, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminMobileRecharge/mobileOperator") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult deleteMobileOperator(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Operator/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        #endregion

        #region Operator Vendor
        public ActionResult vendor()
        {
            return View("vendorListView");
        }

        public ActionResult vendorAddEdit(int id = 0)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            mobileRechargeVendorVM objMobileRechargeVendorVM = new mobileRechargeVendorVM();
            if (id > 0)
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor/" + id, Method.GET, null, accessToken);
                objMobileRechargeVendorVM = JsonConvert.DeserializeObject<mobileRechargeVendorVM>(response.Content);
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor", Method.GET, null, accessToken);
                objMobileRechargeVendorVM = JsonConvert.DeserializeObject<mobileRechargeVendorVM>(response.Content);
            }
            return View("vendorIndex", objMobileRechargeVendorVM);
        }

        [HttpGet]
        public JsonResult getVendorList()
        {
            //Server Side Parameter
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            int pageIndex = Convert.ToInt32(Request["start"]);
            int pageSize = Convert.ToInt32(Request["length"]);
            string searchValue = Request["search[value]"];
            string sortColumnName = Request["columns[" + Request["order[0][column]"] + "][name]"];
            string sortDirection = Request["order[0][dir]"];
            mobileRechargeVendorVM objMobileRechargeVendorVM = new mobileRechargeVendorVM();
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&searchValue=" + searchValue + "&sortDirection=" + sortDirection, Method.GET, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                objMobileRechargeVendorVM = JsonConvert.DeserializeObject<mobileRechargeVendorVM>(response.Content);
                return Json(new { data = objMobileRechargeVendorVM.objMobileRechargeVendorList, draw = Request["draw"], recordsTotal = objMobileRechargeVendorVM.totalRecords, recordsFiltered = objMobileRechargeVendorVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                objMobileRechargeVendorVM = new mobileRechargeVendorVM();
                return Json(new { data = objMobileRechargeVendorVM.objMobileRechargeVendorList, draw = Request["draw"], recordsTotal = objMobileRechargeVendorVM.totalRecords, recordsFiltered = objMobileRechargeVendorVM.filterRecords }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult saveMobileVendor(mobileRechargeVendorVM objMobileRechargeVendorVM)
        {
            customResponse objCustomResponse = new customResponse();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            if (objMobileRechargeVendorVM.objMobileRechargeVendor.OperatorVendorId == 0)
            {
                if (!string.IsNullOrEmpty(objMobileRechargeVendorVM.objMobileRechargeVendor.VendorName))
                {
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor", Method.POST, objMobileRechargeVendorVM.objMobileRechargeVendor, accessToken);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        TempData[Helper.responseStatus] = Helper.recordSaved;
                        return Json(new { success = true, redirectURL = Url.Content("~/adminMobileRecharge/vendor") }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                        return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    ModelState.AddModelError("CompanyName", "Please enter Vendor Name");
                    return Json(new { success = false, responseText = "Please enter Vendor Name" }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor/" + objMobileRechargeVendorVM.objMobileRechargeVendor.OperatorVendorId, Method.PUT, objMobileRechargeVendorVM.objMobileRechargeVendor, accessToken);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    TempData[Helper.responseStatus] = Helper.recordUpdated;
                    return Json(new { success = true, responseText = Helper.recordUpdated, redirectURL = Url.Content("~/adminMobileRecharge/vendor") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objCustomResponse = JsonConvert.DeserializeObject<customResponse>(response.Content);
                    return Json(new { success = false, responseText = "1|" + objCustomResponse.Message + " Status Code : " + response.StatusCode }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public JsonResult deleteVendor(int id)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_Vendor/" + id, Method.DELETE, null, accessToken);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return Json("Success");
            }
            else
            {
                return Json(response.StatusCode.ToString());
            }
        }

        #endregion

        #region Default Commission

        public ActionResult defaultCommission()
        {
            mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission", Method.GET, null, accessToken);
            objMobileRechargeVendorCommissionVM = JsonConvert.DeserializeObject<mobileRechargeVendorCommissionVM>(webAPIResponseMenu.Content);
            objMobileRechargeVendorCommissionVM.VendorName = "";
            return View("defaultCommissionView", objMobileRechargeVendorCommissionVM);
        }

        public ActionResult getVendorCommissionDetail(int entityTypeId, string vendorName, int operatorType)
        {
            mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission?vendorName=" + vendorName + "&entityTypeId=" + entityTypeId + "&operatorTypeId=" + operatorType, Method.GET, null, accessToken);
            objMobileRechargeVendorCommissionVM = JsonConvert.DeserializeObject<mobileRechargeVendorCommissionVM>(webAPIResponseMenu.Content);
            objMobileRechargeVendorCommissionVM.EntityTypeId = entityTypeId;
            objMobileRechargeVendorCommissionVM.VendorName = vendorName;
            return PartialView("_partial_Vendor_Default_Commission", objMobileRechargeVendorCommissionVM);
        }

        [HttpPost]
        public JsonResult saveDefaultCommission(mobileRechargeVendorCommissionVM objCommissionVM)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            Boolean recordInserted = false;
            tblMobileRecharge_VendorCommission objVendorCommission;
            foreach (tblMobileRecharge_VendorCommission objData in objCommissionVM.objMobileRechargeVendorCommissionList)
            {
                if (objData.CommissionTypeId > 0 || objData.CommissionValue > 0)
                {
                    objVendorCommission = new tblMobileRecharge_VendorCommission();
                    objVendorCommission.OperatorVendorCommissionId = objData.OperatorVendorCommissionId;
                    objVendorCommission.OperatorId = objData.OperatorId;
                    objVendorCommission.OperatorVendorId = objData.OperatorVendorId;
                    objVendorCommission.CommissionTypeId = objData.CommissionTypeId;
                    objVendorCommission.CommissionValue = objData.CommissionValue;
                    objVendorCommission.EntityTypeId = objCommissionVM.EntityTypeId;
                    objVendorCommission.EntityId = 0;
                    objVendorCommission.IsCustom = false;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission", Method.POST, objVendorCommission, accessToken);
                    recordInserted = true;
                }
            }
            if (recordInserted)
                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "1|Enter atleast 1 Operator Commission" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Custom Commission

        public ActionResult customCommission()
        {
            mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission", Method.GET, null, accessToken);
            objMobileRechargeVendorCommissionVM = JsonConvert.DeserializeObject<mobileRechargeVendorCommissionVM>(webAPIResponseMenu.Content);
            objMobileRechargeVendorCommissionVM.VendorName = "";
            objMobileRechargeVendorCommissionVM.objEntityList = new List<tblEntity_Master>();
            return View("customCommissionView", objMobileRechargeVendorCommissionVM);
        }

        public ActionResult getVendorCustomCommissionDetail(int entityTypeId, string vendorName, int operatorType, long entityId)
        {
            mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return RedirectToAction("index", new { Controller = "unauthorized" });

            IRestResponse webAPIResponseMenu = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission/getVendorCommissionListByVendorEntityCustom?vendorName=" + vendorName + "&operatorTypeId=" + operatorType + "&entityId=" + entityId + "&entityTypeId=" + entityTypeId, Method.GET, null, accessToken);
            objMobileRechargeVendorCommissionVM = JsonConvert.DeserializeObject<mobileRechargeVendorCommissionVM>(webAPIResponseMenu.Content);
            objMobileRechargeVendorCommissionVM.EntityTypeId = entityTypeId;
            objMobileRechargeVendorCommissionVM.VendorName = vendorName;
            objMobileRechargeVendorCommissionVM.EntityId = entityId;
            return PartialView("_partial_Vendor_Custom_Commission", objMobileRechargeVendorCommissionVM);
        }

        [HttpPost]
        public JsonResult saveCustomCommission(mobileRechargeVendorCommissionVM objCommissionVM)
        {
            string accessToken = Helper.decryptAndRetrieveCookieValue(Helper.userAccessToken);
            if (String.IsNullOrEmpty(accessToken))
                return Json(new { success = false, responseText = "Unauthorized", redirectURL = Url.Content("~/Unauthorized") }, JsonRequestBehavior.AllowGet);

            Boolean recordInserted = false;
            tblMobileRecharge_VendorCommission objVendorCommission;
            foreach (tblMobileRecharge_VendorCommission objData in objCommissionVM.objMobileRechargeVendorCommissionList)
            {
                if (objData.CommissionTypeId > 0 || objData.CommissionValue > 0)
                {
                    objVendorCommission = new tblMobileRecharge_VendorCommission();
                    objVendorCommission.OperatorVendorCommissionId = objData.OperatorVendorCommissionId;
                    objVendorCommission.OperatorId = objData.OperatorId;
                    objVendorCommission.OperatorVendorId = objData.OperatorVendorId;
                    objVendorCommission.CommissionTypeId = objData.CommissionTypeId;
                    objVendorCommission.CommissionValue = objData.CommissionValue;
                    objVendorCommission.EntityTypeId = objCommissionVM.EntityTypeId;
                    objVendorCommission.EntityId = objCommissionVM.EntityId;
                    objVendorCommission.IsCustom = true;
                    IRestResponse response = Helper.WebAPIClient_via_RestSharp("MobileRecharge_VendorCommission", Method.POST, objVendorCommission, accessToken);
                    recordInserted = true;
                }
            }
            if (recordInserted)
                return Json(new { success = true, responseText = Helper.recordSaved }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = false, responseText = "1|Enter atleast 1 Operator Commission" }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}