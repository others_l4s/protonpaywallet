﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.response
{
    public class bank_details_data_resp
    {
        public string id { get; set; }
        public string bank_name { get; set; }
        public string imps_enabled { get; set; }
        public string aeps_enabled { get; set; }
        public string bank_sort_name { get; set; }
        public string branch_ifsc { get; set; }
        public string ifsc_alias { get; set; }
        public string bank_iin { get; set; }
        public string is_down { get; set; }
    }
}