﻿/*=========================================================================================
  File Name: app.js
  Description: Template related app JS.
  ----------------------------------------------------------------------------------------
  Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
 Version: 3.0
  Author: Pixinvent
  Author URL: hhttp://www.themeforest.net/user/pixinvent
==========================================================================================*/

const loadingDataInDataGrid = "Loading Data Please Wait";
const gridButtonPrimaryCSS = "btn btn-outline-primary btn-md waves-effect waves-light";
const gridButtonEditCSS = "btn btn-outline-primary btn-sm waves-effect waves-light";
const gridButtonDeleteCSS = "btn btn-outline-danger btn-sm waves-effect waves-light js-delete";
const gridButtonDangerCSS = "btn btn-danger btn-sm waves-effect waves-light";
const gridButtonPrimary = "btn btn-primary btn-sm waves-effect waves-light";
const gridDateTimeFormat = "mmm dS, yyyy, h:MM TT";
const gridDateFormat = "mmm dS, yyyy";
const gridDateFormatNormal = "mm/dd/yyyy";


var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(9); //Tab
specialKeys.push(46); //Delete
specialKeys.push(36); //Home
specialKeys.push(35); //End
specialKeys.push(37); //Left
specialKeys.push(39); //Right
function IsAlphaNumeric(e) {
    var keyCode = e.keyCode === 0 ? e.charCode : e.keyCode;
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) !== -1);
    return ret;
}

(function (window, document, $) {
    'use strict';
    var $html = $('html');
    var $body = $('body');


    $(window).on('load', function () {
        setTimeout(function () {
            $html.removeClass('loading').addClass('loaded');
        }, 1200);
        // Tooltip Initialization
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });

        // Top Navbars - Hide on Scroll
        if ($(".navbar-hide-on-scroll").length > 0) {
            $(".navbar-hide-on-scroll.fixed-top").headroom({
                "offset": 205,
                "tolerance": 5,
                "classes": {
                    // when element is initialised
                    initial: "headroom",
                    // when scrolling up
                    pinned: "headroom--pinned-top",
                    // when scrolling down
                    unpinned: "headroom--unpinned-top",
                }
            });
            // Bottom Navbars - Hide on Scroll
            $(".navbar-hide-on-scroll.fixed-bottom").headroom({
                "offset": 205,
                "tolerance": 5,
                "classes": {
                    // when element is initialised
                    initial: "headroom",
                    // when scrolling up
                    pinned: "headroom--pinned-bottom",
                    // when scrolling down
                    unpinned: "headroom--unpinned-bottom"
                }
            });
        }

        //Match content & menu height for content menu
        setTimeout(function () {
            if ($('body').hasClass('vertical-content-menu')) {
                setContentMenuHeight();
            }
        }, 500);
        function setContentMenuHeight() {
            var menuHeight = $('.main-menu').height();
            var bodyHeight = $('.content-body').height();
            if (bodyHeight < menuHeight) {
                $('.content-body').css('height', menuHeight);
            }
        }

        // Collapsible Card
        $('a[data-action="collapse"]').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.card').children('.card-content').collapse('toggle');
            $(this).closest('.card').find('[data-action="collapse"] i').toggleClass('ft-plus ft-minus');

        });

        // Toggle fullscreen
        $('a[data-action="expand"]').on('click', function (e) {
            e.preventDefault();
            $(this).closest('.card').find('[data-action="expand"] i').toggleClass('ft-maximize ft-minimize');
            $(this).closest('.card').toggleClass('card-fullscreen');
        });

        //  Notifications & messages scrollable
        if ($('.scrollable-container').length > 0) {
            $('.scrollable-container').each(function () {
                var scrollable_container = new PerfectScrollbar($(this)[0]);
            });
        }

        // Reload Card
        $('a[data-action="reload"]').on('click', function () {
            var block_ele = $(this).closest('.card');

            // Block Element
            block_ele.block({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                timeout: 2000, //unblock after 2 seconds
                overlayCSS: {
                    backgroundColor: '#FFF',
                    cursor: 'wait',
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'none'
                }
            });
        });

        // Close Card
        $('a[data-action="close"]').on('click', function () {
            $(this).closest('.card').removeClass().slideUp('fast');
        });

        // Match the height of each card in a row
        setTimeout(function () {
            $('.row.match-height').each(function () {
                $(this).find('.card').not('.card .card').matchHeight(); // Not .card .card prevents collapsible cards from taking height
            });
        }, 500);


        $('.card .heading-elements a[data-action="collapse"]').on('click', function () {
            var $this = $(this),
                card = $this.closest('.card');
            var cardHeight;

            if (parseInt(card[0].style.height, 10) > 0) {
                cardHeight = card.css('height');
                card.css('height', '').attr('data-height', cardHeight);
            }
            else {
                if (card.data('height')) {
                    cardHeight = card.data('height');
                    card.css('height', cardHeight).attr('data-height', '');
                }
            }
        });

        //// Add open class to parent list item if subitem is active except compact menu
        //var menuType = $body.data('menu');
        //if (menuType !== 'vertical-compact-menu' && menuType !== 'horizontal-menu') {
        //    if ($body.data('menu') === 'vertical-menu-modern') {
        //        if (localStorage.getItem("menuLocked") === "true") {
        //            $(".main-menu-content").find('li.active').parents('li').addClass('open');
        //        }
        //    }
        //    else {
        //        $(".main-menu-content").find('li.active').parents('li').addClass('open');
        //    }
        //}
        //if (menuType === 'vertical-compact-menu' || menuType === 'horizontal-menu') {
        //    $(".main-menu-content").find('li.active').parents('li:not(.nav-item)').addClass('open');
        //    $(".main-menu-content").find('li.active').parents('li').addClass('active');
        //}

        //card heading actions buttons small screen support
        $(".heading-elements-toggle").on("click", function () {
            $(this).parent().children(".heading-elements").toggleClass("visible");
        });

        //  Dynamic height for the chartjs div for the chart animations to work
        var chartjsDiv = $('.chartjs'),
            canvasHeight = chartjsDiv.children('canvas').attr('height');
        chartjsDiv.css('height', canvasHeight);

        //if ($body.hasClass('boxed-layout')) {
        //    if ($body.hasClass('vertical-overlay-menu') || $body.hasClass('vertical-compact-menu')) {
        //        var menuWidth = $('.main-menu').width();
        //        var contentPosition = $('.app-content').position().left;
        //        var menuPositionAdjust = contentPosition - menuWidth;
        //        if ($body.hasClass('menu-flipped')) {
        //            $('.main-menu').css('right', menuPositionAdjust + 'px');
        //        } else {
        //            $('.main-menu').css('left', menuPositionAdjust + 'px');
        //        }
        //    }
        //}

        $('.nav-link-search').on('click', function () {
            var $this = $(this),
                searchInput = $(this).siblings('.search-input');

            if (searchInput.hasClass('open')) {
                searchInput.removeClass('open');
            }
            else {
                searchInput.addClass('open');
            }
        });
    });

    // Hide overlay menu on content overlay click on small screens
    $(document).on('click', '.sidenav-overlay', function (e) {
        // Hide menu
        $.app.menu.hide();
        return false;
    });

    // Execute below code only if we find hammer js for touch swipe feature on small screen
    if (typeof Hammer !== 'undefined') {

        // Swipe menu gesture
        var swipeInElement = document.querySelector('.drag-target');

        if ($(swipeInElement).length > 0) {
            var swipeInMenu = new Hammer(swipeInElement);

            swipeInMenu.on("panright", function (ev) {
                if ($body.hasClass('vertical-overlay-menu')) {
                    $.app.menu.open();
                    return false;
                }
            });
        }

        // menu swipe out gesture
        setTimeout(function () {
            var swipeOutElement = document.querySelector('.main-menu');
            var swipeOutMenu;

            if ($(swipeOutElement).length > 0) {
                swipeOutMenu = new Hammer(swipeOutElement);

                swipeOutMenu.get('pan').set({ direction: Hammer.DIRECTION_ALL, threshold: 100 });

                swipeOutMenu.on("panleft", function (ev) {
                    if ($body.hasClass('vertical-overlay-menu')) {
                        $.app.menu.hide();
                        return false;
                    }
                });
            }
        }, 300);

        // menu overlay swipe out gestrue
        var swipeOutOverlayElement = document.querySelector('.sidenav-overlay');

        if ($(swipeOutOverlayElement).length > 0) {

            var swipeOutOverlayMenu = new Hammer(swipeOutOverlayElement);

            swipeOutOverlayMenu.on("panleft", function (ev) {
                if ($body.hasClass('vertical-overlay-menu')) {
                    $.app.menu.hide();
                    return false;
                }
            });
        }
    }

    $(document).on('click', '.menu-toggle, .modern-nav-toggle', function (e) {
        e.preventDefault();

        // Hide dropdown of user profile section for material templates
        if ($('.user-profile .user-info .dropdown').hasClass('show')) {
            $('.user-profile .user-info .dropdown').removeClass('show');
            $('.user-profile .user-info .dropdown .dropdown-menu').removeClass('show');
        }

        // Toggle menu
        $.app.menu.toggle();

        setTimeout(function () {
            $(window).trigger("resize");
        }, 200);

        if ($('#collapsed-sidebar').length > 0) {
            setTimeout(function () {
                if ($body.hasClass('menu-expanded') || $body.hasClass('menu-open')) {
                    $('#collapsed-sidebar').prop('checked', false);
                }
                else {
                    $('#collapsed-sidebar').prop('checked', true);
                }
            }, 1000);
        }

        // Hides dropdown on click of menu toggle
        // $('[data-toggle="dropdown"]').dropdown('hide');

        // Hides collapse dropdown on click of menu toggle
        if ($('.vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse').hasClass('show')) {
            $('.vertical-overlay-menu .navbar-with-menu .navbar-container .navbar-collapse').removeClass('show');
        }

        return false;
    });

    $(document).on('click', '.open-navbar-container', function (e) {

        var currentBreakpoint = Unison.fetch.now();

        // Init drilldown on small screen
        $.app.menu.drillDownMenu(currentBreakpoint.name);

        // return false;
    });

    $(document).on('click', '.main-menu-footer .footer-toggle', function (e) {
        e.preventDefault();
        $(this).find('i').toggleClass('pe-is-i-angle-down pe-is-i-angle-up');
        $('.main-menu-footer').toggleClass('footer-close footer-open');
        return false;
    });

    // Add Children Class
    $('.navigation').find('li').has('ul').addClass('has-sub');

    $('.carousel').carousel({
        interval: 2000
    });

    // Page full screen
    $('.nav-link-expand').on('click', function (e) {
        if (typeof screenfull !== 'undefined') {
            if (screenfull.enabled) {
                screenfull.toggle();
            }
        }
    });
    if (typeof screenfull !== 'undefined') {
        if (screenfull.enabled) {
            $(document).on(screenfull.raw.fullscreenchange, function () {
                if (screenfull.isFullscreen) {
                    $('.nav-link-expand').find('i').toggleClass('ft-minimize ft-maximize');
                }
                else {
                    $('.nav-link-expand').find('i').toggleClass('ft-maximize ft-minimize');
                }
            });
        }
    }

    $(document).on('click', '.mega-dropdown-menu', function (e) {
        e.stopPropagation();
    });

    $(document).ready(function () {

        /**********************************
        *   Form Wizard Step Icon
        **********************************/
        $('.step-icon').each(function () {
            var $this = $(this);
            if ($this.siblings('span.step').length > 0) {
                $this.siblings('span.step').empty();
                $(this).appendTo($(this).siblings('span.step'));
            }
        });
    });

    // Update manual scroller when window is resized
    $(window).resize(function () {
        $.app.menu.manualScroller.updateHeight();
    });

    $('#sidebar-page-navigation').on('click', 'a.nav-link', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this),
            href = $this.attr('href');
        var offset = $(href).offset();
        var scrollto = offset.top - 80; // minus fixed header height
        $('html, body').animate({ scrollTop: scrollto }, 0);
        setTimeout(function () {
            $this.parent('.nav-item').siblings('.nav-item').children('.nav-link').removeClass('active');
            $this.addClass('active');
        }, 100);
    });

})(window, document, jQuery);

// Custom Configuration
function notifyMessage(message) {
    if (message.split('|').length === 2) {
        if (message.split('|')[0] === "0")
            toastr.success(message.split('|')[1]);
        else
            toastr.error(message.split('|')[1]);
    }
    else {
        toastr.success(message);
    }
}

// Country / State cascading dropdownlists //objAdminLogin_StateId
function populateStateByCountryId(countryId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateState",
        data: { countryId: countryId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select State</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"bindCityOnStateChange(this.value)\">";
            stateList = stateList + '<option value="">--- Select State ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].StateId + '>' + data[i].StateName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
        }
    });
}

function populateCityByStateId(stateID, dropdownMarkUpId2Populate, cityMarkUpId, cityObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateCity",
        data: { stateId: stateID },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var cityList = "<label class='" + labelClass + "'>Select City</label><select id='" + cityMarkUpId + "' class='form-control' name='" + cityObjectName + "'>";
            cityList = cityList + '<option value="">--- Select City ---</option>';
            for (var i = 0; i < data.length; i++) {
                cityList = cityList + '<option value=' + data[i].CityId + '>' + data[i].CityName + '</option>';
            }
            cityList = cityList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(cityList);
        }
    });
}

function populateMainMenuByCompanyId(companyId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateMainMenuByCompanyId",
        data: { companyId: companyId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select Main Menu</label>";
            stateList += "<select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"javascript:populateDropDownData()\">";
            stateList = stateList + '<option value="0">--- Select Main Menu ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].MainMenuId + '>' + data[i].MainMenuName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
        }
    });
}

function populateRoleByCompanyId(companyId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateRoleByCompanyId",
        data: { companyId: companyId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select Role</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"javascript:populateDropDownData()\">";
            stateList = stateList + '<option value="0">--- Select Role ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].RoleId + '>' + data[i].RoleName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
        }
    });
}

function populateMainMenuByCompanyPer(companyId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateMainMenuByCompanyPermission",
        data: { companyId: companyId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select Main Menu</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"javascript:populateDropDownData()\">";
            stateList = stateList + '<option value="0">--- Select Main Menu ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].MainMenuId + '>' + data[i].MainMenuName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
        }
    });
}

function populateMainMenuByRole(roleId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateMainMenuByRole",
        data: { roleId: roleId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select Main Menu</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "'>";
            stateList = stateList + '<option value="0">--- Select Main Menu ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].MainMenuId + '>' + data[i].MainMenuName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
        }
    });
}

function populateDistributorByMD(masterDistributorID, dropdownMarkUpId2Populate, distributorMarkUpId, distributorObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateDistributorList",
        data: { masterDistributorId: masterDistributorID },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var distList = "<label class='" + labelClass + "'>Select Distributor</label><select id='" + distributorMarkUpId + "' class='form-control' name='" + distributorObjectName + "'>";
            distList = distList + '<option value="0">--- Select Distributor ---</option>';
            for (var i = 0; i < data.length; i++) {
                distList = distList + '<option value=' + data[i].EntityId + '>' + data[i].AgencyName + '</option>';
            }
            distList = distList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(distList);
        }
    });
}

function populateBankITDMT_BankList(dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "POST",
        url: "/DMR/populateBankList",
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + labelClass + "'>Select Bank</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"bindIFSCOnChange(this.value)\">";
            stateList = stateList + '<option value="">--- Select Bank List ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].bankCode + "|" + data[i].ifsc + '>' + data[i].bankName + " - (" + data[i].ifsc + ')</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
            $("#" + stateMarkUpId).select2();
        }
    });
}

function populateInstantPayDMT_BankList(dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, accountNumber) {
    $.ajax({
        type: "POST",
        url: "/DMR2/getBankList",
        data: { accountNumber: accountNumber },
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label class='" + stateObjectName + "'>Select Bank</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"bindIFSCOnChange(this.value)\">";
            stateList = stateList + '<option value="">--- Select Bank List ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].branch_ifsc + '>' + data[i].bank_name + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
            $("#" + stateMarkUpId).select2();
        }
    });
}

function populate_EntityMasterByEntityTypeID(entityTypeId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateEntityListByEntityTypeId?entityTypeId=" + entityTypeId,
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"javascript:getEntityAvailableBalance(this.value,'lblAvailableBalance');\">";
            stateList = stateList + '<option value="0">--- Select Agency ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].EntityId + '>' + data[i].AgencyName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
            $("#" + stateMarkUpId).select2();
        }
    });
}

function populate_EntityMasterByEntityTypeID_4_Report(entityTypeId, dropdownMarkUpId2Populate, stateMarkUpId, stateObjectName, labelClass) {
    $.ajax({
        type: "post",
        url: "/helper/populateEntityListByEntityTypeId?entityTypeId=" + entityTypeId,
        datatype: "json",
        traditional: true,
        success: function (data) {
            var stateList = "<label>Agency</label><select id='" + stateMarkUpId + "' class='form-control' name='" + stateObjectName + "' onchange=\"javascript:getEntityAvailableBalance(this.value,'lblAvailableBalance');\">";
            stateList = stateList + '<option value="0">--- Select Agency ---</option>';
            for (var i = 0; i < data.length; i++) {
                stateList = stateList + '<option value=' + data[i].EntityId + '>' + data[i].AgencyName + '</option>';
            }
            stateList = stateList + '</select>';
            $('#' + dropdownMarkUpId2Populate).html(stateList);
            $("#" + stateMarkUpId).select2({ containerCssClass: 'select-sm' });
            //$("#" + stateMarkUpId).select2().trigger('change');
        }
    });
}

function getEntityAvailableBalance(entityId, control2Display) {
    $.ajax({
        type: "post",
        url: "/helper/getEntityAvailableWalletBalance?entityId=" + entityId,
        datatype: "json",
        traditional: true,
        success: function (data) {
            $("#divAvailableBalance").show();
            $("#" + control2Display).html("Avail Wallet Balance: ₹" + data);
        }
    });
}


var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length === 1 && Object.prototype.toString.call(date) === "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) === "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 !== 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

function OnSuccess(response) {
    if (response.success === true) {
        if (typeof response.redirectURL === "undefined")
            notifyMessage(response.responseText);
        else {
            window.location.href = response.redirectURL;
        }
    }
    else {
        if (response.responseText === "Unauthorized") {
            window.location.href = response.redirectURL;
        }
        else if (response.responseText === "1|OTP") {
            $("#divLogin").hide();
            $("#divOTP").show();
        }
        else if (response.responseText === "1|FPOTP") // Forgot Password OTP Process
        {
            $("#divLogin").hide();
            $("#divForgotPasswordOTP").show();
        }
        else {
            notifyMessage(response.responseText);
        }
    }
}
function OnFailure(response) {
    if (response.responseText === "OTP") {
        $("#divLogin").hide();
        $("#divOTP").show();
    }
    else
        notifyMessage("0|" + response.responseText);
}
function beginSubmit(buttonId) {
    $("#" + buttonId).attr("disabled", "disabled");
    $("#" + buttonId).html("processing...");
}
function completeSubmit(buttonId, buttonText) {
    $("#" + buttonId).removeAttr("disabled");
    if (buttonText === "Save")
        $("#" + buttonId).html("<i class=\"la la-check-square-o\"></i> " + buttonText);
    else if (buttonText === "Login")
        $("#" + buttonId).html("<i class=\"ft-user\"></i> " + buttonText);
    else
        $("#" + buttonId).html(buttonText);
}

/* login page */
$('#txtLoginName').on("keypress", function (e) {
    if (e.keyCode === 13) {
        $("#btnLogin").click();
    }
});

$('#txtPassword').on("keypress", function (e) {
    if (e.keyCode === 13) {
        $("#btnLogin").click();
    }
});

/* Make Payment module show hide fields */
$('#cboPaymentMode').change(function () {
    switch ($('#cboPaymentMode').val()) {
        case "1":
            $("#divBankBranchCode").show();
            $("#divCheque").hide();
            $("#divPaymentSlip").show();
            $("#divBankTransactionId").show();
            break;
        case "2":
            $("#divBankBranchCode").hide();
            $("#divCheque").show();
            $("#divPaymentSlip").hide();
            $("#divBankTransactionId").hide();
            break;
        case "3":
            $("#divBankBranchCode").hide();
            $("#divCheque").hide();
            $("#divPaymentSlip").show();
            $("#divBankTransactionId").show();
            break;
        case "4":
            $("#divBankBranchCode").hide();
            $("#divCheque").hide();
            $("#divPaymentSlip").show();
            $("#divBankTransactionId").show();
            break;
    }
});

function accessHeaderNav() {
    $.ajax({
        type: "GET",
        url: "/helper/populate_NavBar_Elements",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) { },
        error: function (response) {
            $("#mainNavBar").html(response.responseText);
        }
    });
}

function accessMenuBar() {
    $.ajax({
        type: "GET",
        url: "/helper/populate_HorizontalNavBar",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) { },
        error: function (response) {
            $("#navbarCollapse").html(response.responseText);
            //activateNewMenu();
        }
    });
}

function approveBankAccount(entityId) {
    $.ajax({
        type: "post",
        url: "/helper/verifyBankAccount",
        data: { entityId: entityId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            if (data === "Ok") {
                notifyMessage("0|Account successfully verified");
                window.location.reload();
            }
            else {
                notifyMessage("1|" + data);
            }
        }
    });
}

function approveBankAccountMD(entityId) {
    $.ajax({
        type: "post",
        url: "/helper/verifyBankAccountMD",
        data: { entityId: entityId },
        datatype: "json",
        traditional: true,
        success: function (data) {
            if (data === "Ok") {
                notifyMessage("0|Account successfully verified");
                window.location.reload();
            }
            else {
                notifyMessage("1|" + data);
            }
        }
    });
}

function approvePayment(entityPaymentId) {
    if (confirm("Are you sure want to Approve Payment?")) {
        $.ajax({
            type: "post",
            url: "/helper/approvePayment",
            data: { entityPaymentId: entityPaymentId },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data === "Ok") {
                    window.location.reload();
                }
                else {
                    notifyMessage("1|" + data);
                }
            }
        });
    }
}

function rejectPayment(entityPaymentId, rejectReason) {
    if ((entityPaymentId !== "" || entityPaymentId !== "0") && rejectReason !== "") {
        if (confirm("Are you sure want to Reject Payment?")) {
            $.ajax({
                type: "post",
                url: "/helper/rejectPayment",
                data: {
                    entityPaymentId: entityPaymentId, rejectReason: rejectReason
                },
                datatype: "json",
                traditional: true,
                success: function (data) {
                    if (data === "Ok") {
                        window.location.reload();
                    }
                    else {
                        notifyMessage("1|" + data);
                    }
                }
            });
        }
    }
    else {
        notifyMessage("1|Invalid Data.");
    }

}

function formatIndianDate(date, previousDay) {
    var monthNames = [
        "01", "02", "03",
        "04", "05", "06", "07",
        "08", "09", "10",
        "11", "12"
    ];

    var day = date.getDate();//(previousDay ? date.getDate() - 1 : date.getDate());
    if (day < 10) {
        day = '0' + day;
    }
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + '-' + monthNames[monthIndex] + '-' + year;
}

function formatTicketNo(str, max) {
    str = str.toString();
    return str.length < max ? formatTicketNo("0" + str, max) : str;
}

function show50Characters(text) {
    text = text.replace("\r", "").replace("\n", "");
    return text.length > 50 ? text.substr(0, text.lastIndexOf(' ', 45)) + '...' : text;

}

function verifyDMT2BeneficiaryAccount(remitterId, beneAccountNo) {
    if (confirm("Are you sure want to Verify Account?")) {
        $.ajax({
            type: "post",
            url: "/DMR2/verifyBeneficiaryAccount",
            data: { remitterId: remitterId, benAccNo: beneAccountNo },
            datatype: "json",
            traditional: true,
            success: function (data) {
                if (data === "Ok") {
                    window.location.reload();
                }
                else {
                    notifyMessage("1|" + data);
                }
            }
        });
    }
}

function checkDMT2TransactionStatus(orderId) {
    //$('#modalDMTTransactionStatus').modal({ keyboard: false, show: true });
    $.ajax({
        type: "post",
        url: "/helper/checkDMT2PendingTransactionStatus?clientTransactionId=" + orderId,
        datatype: "json",
        traditional: true,
        success: function (data) {
            notifyMessage(data);
            $('#btnsearch').click();
            //$('#modalDMTTransactionStatus').modal('hide');
        }
    });
}

function checkDMT1TransactionStatus(orderId) {
    //$('#modalDMTTransactionStatus').modal({ keyboard: false, show: true });
    $.ajax({
        type: "post",
        url: "/helper/checkDMTPendingTransactionStatus?clientTransactionId=" + orderId,
        datatype: "json",
        traditional: true,
        success: function (data) {
            notifyMessage(data);
            $('#btnsearch').click();
            //$('#modalDMTTransactionStatus').modal('hide');
        }
    });
}

function activateNewMenu() {
    $('.navbar .dropdown-item').on('click', function (e) {
        var $el = $(this).children('.dropdown-toggle');
        var $parent = $el.offsetParent(".dropdown-menu");
        $(this).parent("li").toggleClass('open');

        if (!$parent.parent().hasClass('navbar-nav')) {
            if ($parent.hasClass('show')) {
                $parent.removeClass('show');
                $el.next().removeClass('show');
                $el.next().css({ "top": -999, "left": -999 });
            } else {
                $parent.parent().find('.show').removeClass('show');
                $parent.addClass('show');
                $el.next().addClass('show');
                $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 });
            }
            e.preventDefault();
            e.stopPropagation();
        }
    });

    $('.navbar .dropdown').on('hidden.bs.dropdown', function () {
        $(this).find('li.dropdown').removeClass('show open');
        $(this).find('ul.dropdown-menu').removeClass('show open');
    });
}