﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMyRoboticRechargeUtilityData
    {
        public int MyRoboticRechargeDataId { get; set; }
        public string OperatorName { get; set; }
        public string OperatorCode { get; set; }
        public decimal Margin { get; set; }
        public int ProductId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public int CompanyId { get; set; }
        public string ProductCode { get; set; }
        public string AgencyName { get; set; }
        public string ContactPersonName1 { get; set; }
    }
}