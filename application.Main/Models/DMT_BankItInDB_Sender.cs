﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_BankItInDB_Sender
    {
        public string token { get; set; }
        public long BankitSenderId { get; set; }

        [Required(ErrorMessage = "Please enter Full Name")]
        [StringLength(100, ErrorMessage = "Please enter Full Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Full Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string Name { get; set; }

        [StringLength(500, ErrorMessage = "Please enter Address")]
        [Display(Name = "Enter Address")]
        public string Address { get; set; }

        [Display(Name = "Enter Date of Birth (dd/MM/yyyy)")]
        [Required(ErrorMessage = "Please enter Date of Birth (dd/MM/yyyy)")]
        [RegularExpression(Helper.regDate, ErrorMessage = "Invalid date format.")]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No. registered with your Bank")]
        [StringLength(15, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [Display(Name = "Please enter Mobile No. Registered with your Bank")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string CustomerID { get; set; }
        public string MobileNo { get; set; }
        public Nullable<bool> IsOTPverified { get; set; }
        public Nullable<int> EntityId { get; set; }
        public Nullable<decimal> WalletBalance { get; set; }
        public Nullable<bool> KYCStatus { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;

        public Nullable<long> EntityUserId { get; set; }

        [Required(ErrorMessage = "Please enter valid OTP Code")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        [Display(Name = "Please enter OTP Code")]

        public string OTP { get; set; }

        public string recipientName { get; set; }
        public string bankName { get; set; }

        public string remitterId { get; set; }
    }
}