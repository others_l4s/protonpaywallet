﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class BANKIT_AEPS_Get_Token_Response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public BANKIT_AEPS_Get_Token_Data_resp data { get; set; }
    }
}