﻿using System.ComponentModel.DataAnnotations;

namespace application.Main.Models
{
    public partial class tblStatus_Master:commonProperties
    {
        [Required(ErrorMessage = "Please enter Status Name")]
        [StringLength(15)]
        [Display(Name = "Status Name")]
        public string StatusName { get; set; }
    }
}