﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMobileRecharge_Circle : commonProperties
    {
        public long CircleId { get; set; }

        [Required(ErrorMessage = "Please enter Circle")]
        [StringLength(50, ErrorMessage = "Length should not exceed 50 characters")]
        [Display(Name = "Enter Circle")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Characters allowed.")]
        public string CircleName { get; set; }
        public long OperatorId { get; set; } = 0;
    }
}