﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_Remitter_Details_req
    {
        [Display(Name = "Mobile No")]
        [Required(ErrorMessage = "Please Enter Mobile No.")]
        public string mobile { get; set; }
    }
}