﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblBankAccountType : commonProperties
    {
        public int BankAccountTypeId { get; set; }
        public string BankAccountTypeName { get; set; }
    }
}