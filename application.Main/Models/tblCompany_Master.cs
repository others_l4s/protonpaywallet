﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompany_Master : commonProperties
    {
        public int CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Company Name")]
        [StringLength(100, ErrorMessage = "Please enter Company Name", MinimumLength = 3)]
        [Display(Name = "Company Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Please enter Company Address 1")]
        [StringLength(500, ErrorMessage = "Please enter Company Address 1")]
        [Display(Name = "Company Address 1")]
        public string CompanyAddress1 { get; set; }

        [StringLength(500)]
        [Display(Name = "Company Address 2")]
        public string CompanyAddress2 { get; set; }

        [Required(ErrorMessage = "Please enter Company Telephone 1")]
        [StringLength(15, ErrorMessage = "Please enter valid Telephone No. 1", MinimumLength = 10)]
        [Display(Name = "Company Telephone 1")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string CompanyTelephone1 { get; set; }

        [StringLength(15, ErrorMessage = "Please enter valid Telephone 2", MinimumLength = 10)]
        [Display(Name = "Company Telephone 2")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string CompanyTelephone2 { get; set; }

        [Required(ErrorMessage = "Please enter Company EmailId 1")]
        [StringLength(100, ErrorMessage = "Please enter valid Company EmailId 1", MinimumLength = 6)]
        [Display(Name = "Company Email Id 1")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string CompanyEmailId1 { get; set; }

        [StringLength(100, ErrorMessage = "Please enter valid Company EmailId 2", MinimumLength = 6)]
        [Display(Name = "Company Email Id 2")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string CompanyEmailId2 { get; set; }

        [StringLength(100)]
        [Display(Name = "Company Website")]
        [RegularExpression(Helper.regWebsiteAddress, ErrorMessage = "Invalid Website format.")]
        public string CompanyWebsite { get; set; }

        [StringLength(100)]
        [Display(Name = "Company Domain")]
        public string CompanyDomain { get; set; }

        [StringLength(15)]
        [Display(Name = "Company GST No.")]
        [RegularExpression(Helper.regGSTNo, ErrorMessage = "Invalid GST No. format.")]
        public string CompanyGSTNo { get; set; }

        [StringLength(15)]
        [Display(Name = "Company PAN No.")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Invalid PAN No. format.")]
        public string CompanyPANNo { get; set; }

        public Nullable<int> CompanyTypeId { get; set; }

        [Display(Name = "Company Type")]
        public string CompanyTypeName { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Account Title")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string BankAccountTitle { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string BankAccountNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankName { get; set; }

        [StringLength(20)]
        [Display(Name = "Bank IFSC Code")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankIFSCCode { get; set; }

        [StringLength(100)]
        [Display(Name = "Bank Branch")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankBranch { get; set; }

        [Display(Name = "Bank Account Type")]
        public Nullable<int> BankAccountTypeId { get; set; }

        [Display(Name = "Bank Account Type")]
        public string BankAccountTypeName { get; set; }

        [Display(Name = "Status")]
        public string StatusName { get; set; }
        public Nullable<bool> IsBankVerified { get; set; } = false;
        public Nullable<int> BankVerifiedByUserId { get; set; } = 0;
        public Nullable<bool> IsDefaultCompany { get; set; } = false;


        [Required(ErrorMessage = "Please enter Contact Person Name")]
        [StringLength(100, ErrorMessage = "Please enter valid Person Name", MinimumLength = 3)]
        [Display(Name = "Contact Person Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string PersonName { get; set; }

        [Required(ErrorMessage = "Please enter Contact Person EmailId")]
        [StringLength(100, ErrorMessage = "Please enter valid Email Id", MinimumLength = 6)]
        [Display(Name = "Contact Person EmailId")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please enter Contact Person Mobile (as per aadhaar)")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [Display(Name = "Contact Person Mobile (as per aadhaar)")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Mobile { get; set; }
    }
}