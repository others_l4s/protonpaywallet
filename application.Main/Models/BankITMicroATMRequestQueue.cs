﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class BankITMicroATMRequestQueue
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string AgencyName { get; set; }
        public string OfficeAddress1 { get; set; }
        public string OfficeAddress2 { get; set; }
        public string Landmark { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string PinCode { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string EmailId1 { get; set; }
        public string PersonName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string DOB { get; set; }
        public string PanCardNo { get; set; }
        public string PanCardPhoto { get; set; }
        public string PanCardPhoto2 { get; set; }
        public string AadhaarCardNo { get; set; }
        public string AadhaarCardPhoto { get; set; }
        public Nullable<int> IsKYCStatus { get; set; }
        public string KYCStatusRemark { get; set; }
        public long BankItMicroATMId { get; set; }
        public long EntityId { get; set; }
        public string partnerAgentId { get; set; }
        public System.DateTime RequestDateTime { get; set; }
        public Nullable<System.DateTime> ApprovedDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsRejected { get; set; }
        public int AccessAmount { get; set; }
    }
}