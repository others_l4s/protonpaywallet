﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblsubScriptionProduct_Master : commonProperties
    {
        public int SubscriptionProductId { get; set; }
        [Required(ErrorMessage = "Please Select Subccription")]
        public int SubscriptionId { get; set; } = 0;
        [Required(ErrorMessage = "Please Select Product")]

        public int ProductId { get; set; } = 0;

        [Required(ErrorMessage = "Please Enter Quantity")]
        public int Quantity { get; set; } = 1;
        public string ProductCode { get; set; }
        public string SubscriptionName { get; set; }
        public string StatusName { get; set; }
    }
}