﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_remove_request
    {
        public string token { get; set; }
        public Nepal_beneficiary_remove_req request { get; set; }
    }
}