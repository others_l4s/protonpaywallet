﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_bank_details_Response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public List<Nepal_bank_details_data_resp> data { get; set; }
    }
}