﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMyRoboticRechargeUtilityDataDefaultCommission
    {
        public int MyRoboticRechargeDataId { get; set; }
        public string OperatorName { get; set; }
        public string OperatorCode { get; set; }
        public decimal Margin { get; set; }
        public int ProductId { get; set; }
        public int CompanyId { get; set; }
        public long MyRoboticRechargeDataCommissionId { get; set; }
        public int CommissionTypeId { get; set; } = 1;
        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal MDCommissionValue { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal DistCommissionValue { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal AgentCommissionValue { get; set; }
        public string AgencyName { get; set; }
        public long EntityId { get; set; }
        public bool IsCustom { get; set; }
    }
}