﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntityKYC : commonProperties
    {
        public long BankItAEPSId { get; set; }
        public string AgencyName { get; set; }
        public long EntityId { get; set; }
        public string AgentAuthId { get; set; }
        public string AgentAuthPassword { get; set; }
        public string RetailerId { get; set; }
        public string APIId { get; set; }
        public System.DateTime AEPSRequestedOnDateTime { get; set; }
        public Nullable<System.DateTime> AEPSApprovedOnDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> AEPSRejected { get; set; }
        public int CompanyId { get; set; }

        [StringLength(15)]
        [Display(Name = "PAN No.")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Invalid PAN No. format.")]
        [Required(ErrorMessage = "Please enter Pan No.")]
        public string PanCardNo { get; set; }
        public string PanCardPhoto { get; set; }
        public string PanCardPhoto2 { get; set; }
        public Nullable<bool> IsKYCStatus { get; set; }
        public long EntityKYCId { get; set; }

        [StringLength(15)]
        [Display(Name = "GST No.")]
        [RegularExpression(Helper.regGSTNo, ErrorMessage = "Invalid GST No. format.")]
        public string GSTNo { get; set; }
        public string GSTNoPhoto { get; set; }
        public string GSTNoPhoto2 { get; set; }

        [StringLength(12)]
        [Display(Name = "Aadhaar Card")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Invalid Aadhaar Card No.")]
        [Required(ErrorMessage = "Please enter Aadhaar Card No.")]
        public string AadhaarCardNo { get; set; }
        public string AadhaarCardPhoto { get; set; }
        public string AadhaarCardPhoto2 { get; set; }
        public long EntityUserId { get; set; }
        public string KYCStatusRemark { get; set; }
        public Nullable<long> AdminLoginId { get; set; } = 0;

        public Nullable<bool> FromMobileDevice { get; set; } = false;

        public string OutletId { get; set; }
        public string DocumentId { get; set; }
        public Nullable<bool> OutletMobileVerified { get; set; }
        public string OutletDocumentGetStatusResponse { get; set; }
        public string PanDocumentId { get; set; }
        public string GSTDocumentId { get; set; }
        public string AadhaarCardDocumentId { get; set; }

        public bool OTPSendInMobileForOutletRegistration { get; set; }

        public string MobileOTP { get; set; }
    }
}