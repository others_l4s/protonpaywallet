﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_Type : commonProperties
    {
        [Display(Name = "Entity Type")]
        public int EntityTypeId { get; set; }

        [Required(ErrorMessage = "Please enter Entity Type")]
        [StringLength(20)]
        [Display(Name = "Enter Entity Type")]
        public string EntityTypeName { get; set; }
        public string StatusName { get; set; }
    }
}