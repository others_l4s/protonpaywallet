﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSupportSystemProblem_Master : commonProperties
    {
        public int ProblemId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Problem Name")]
        [StringLength(100, ErrorMessage = "Please enter Problem Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Problem Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string ProblemName { get; set; }

        [Required(ErrorMessage = "Please Select Product")]
        public int ProductId { get; set; } = 0;
        [Required(ErrorMessage = "Please Select Department")]
        public int DepartmentId { get; set; } = 0;

        public string DepartmentName { get; set; }
        public string ProductCode { get; set; }
        public string StatusName { get; set; }
    }
}