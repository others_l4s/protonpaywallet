﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSubscription_Master : commonProperties
    {
        public int SubscriptionId { get; set; }

        [Display(Name = "Select Company Name")]
        [Required(ErrorMessage = "Please select Company")]
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmailId { get; set; }
        public string ContactPersonMobile { get; set; }
        public bool IsDefaultCompany { get; set; }

        [Required(ErrorMessage = "Please enter Subscription Name")]
        [StringLength(100, ErrorMessage = "Please enter Subscription Name", MinimumLength = 3)]
        [Display(Name = "Enter Subscription Name")]
        //[RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string SubscriptionName { get; set; }

        [Required(ErrorMessage = "Please enter Subscription Amount")]
        [Display(Name = "Enter Subscription Amount")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal SubscriptionAmount { get; set; }

        [Display(Name = "Enter Subscription Remark")]
        [StringLength(500, ErrorMessage = "Please enter Subscription Remark", MinimumLength = 5)]
        public string SubscriptionRemark { get; set; }

        [Display(Name = "Select Status")]
        public string StatusName { get; set; }

        [Required(ErrorMessage = "Please enter Validity in Days")]
        [Display(Name = "Enter Subscription Validity")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public int ValidTillDays { get; set; }
    }
}