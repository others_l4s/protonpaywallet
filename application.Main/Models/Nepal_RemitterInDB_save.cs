﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_RemitterInDB_save
    {
        public string token { get; set; }
        public long NepalRemitterID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string RemitterId { get; set; }
        public string MobileNo { get; set; }
        public Nullable<bool> IsOTPVerified { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentUrl { get; set; }
        public string Gender { get; set; }
        public string Pincode { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;
    }
}