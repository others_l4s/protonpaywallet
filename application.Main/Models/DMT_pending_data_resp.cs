﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_pending_data_resp
    {
        public List<DMT_TransactionDetail_pending_resp> transactionDetails { get; set; }
    }
}