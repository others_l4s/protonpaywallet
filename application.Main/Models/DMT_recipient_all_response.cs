﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_recipient_all_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public DMT__recipient_all_data_resp data { get; set; }
    }
}