﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblHelpLineNumber_Master:commonProperties
    {
        public int HelplineId { get; set; }
        [Required(ErrorMessage = "Please select Company")]
        public int CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter helpline number")]
        [StringLength(10, ErrorMessage = "Please enter valid helpline number.", MinimumLength = 10)]
        [Display(Name = "Enter Helpline Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string HelplineNumber { get; set; }

        [Required(ErrorMessage = "Please enter Help Line EmailId")]
        [StringLength(100, ErrorMessage = "Please enter valid Email Id", MinimumLength = 6)]
        [Display(Name = "Enter Contact Person EmailId")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string HelplineEmailId { get; set; }

        [StringLength(100)]
        [Display(Name = "Enter Helpline Type")]
        public string HelplineType { get; set; }
        
        public int ProductId { get; set; } = 0;
        public string CompanyName { get; set; }
        public string ProductCode { get; set; }
        public string StatusName { get; set; }
    }
}