﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_register_Response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public Nepal_beneficiary_register_data_resp data { get; set; }
    }
}