﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_register_validate_req
    {
        public string remitterid { get; set; }
        public string beneficiaryid { get; set; }
        [Display(Name ="Enter OTP")]
        public string otp { get; set; }
    }
}