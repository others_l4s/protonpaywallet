﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompanyWiseMenuSubMenuRole : commonProperties
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string MainMenuName { get; set; }
        public int MainMenuId { get; set; }
        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }
        public int SubMenuPermissionId { get; set; }
        public int RoleId { get; set; }
        public bool CanView { get; set; } = false;
        public bool CanEdit { get; set; } = false;
        public bool CanAdd { get; set; } = false;
        public bool CanDelete { get; set; } = false;
    }
}