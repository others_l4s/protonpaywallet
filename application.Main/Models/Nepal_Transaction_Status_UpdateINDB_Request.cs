﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_Transaction_Status_UpdateINDB_Request
    {
        public string token { get; set; }
        public string Ipayid { get; set; }
        public string StatusCode { get; set; }
    }
}