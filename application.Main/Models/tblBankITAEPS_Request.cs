﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblBankITAEPS_Request
    {
        public long BankItAEPSId { get; set; }
        public long EntityId { get; set; }

        [Required(ErrorMessage = "Please enter APIId")]
        [StringLength(100, ErrorMessage = "Please enter APIId", MinimumLength = 3)]
        [Display(Name = "Enter APIId")]
        public string AgentAuthId { get; set; }

        [Required(ErrorMessage = "Please enter Agent Auth Password")]
        [StringLength(200, ErrorMessage = "Please enter Agent Auth Password", MinimumLength = 5)]
        [Display(Name = "Enter AgentAuth Password")]
        public string AgentAuthPassword { get; set; }

        [Required(ErrorMessage = "Please enter RetailerId")]
        [StringLength(100, ErrorMessage = "Please enter RetailerId", MinimumLength = 3)]
        [Display(Name = "Enter RetailerId")]
        public string RetailerId { get; set; }

        [Required(ErrorMessage = "Please enter APIId")]
        [StringLength(20, ErrorMessage = "Please enter APIId")]
        public string APIId { get; set; }
        public System.DateTime AEPSRequestedOnDateTime { get; set; }
        public Nullable<System.DateTime> AEPSApprovedOnDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> AEPSRejected { get; set; }
        public int AccessAmount { get; set; }

        public string AEPSLIVEURL { get; set; }
    }
}