﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntitySlabCommission : commonProperties
    {
        public long SlabCommissionId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Please Select Product")]
        [Display(Name = "Select Product")]
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string CommissionTypeName { get; set; }

        [Required(ErrorMessage = "Please select Commission Type")]
        [Display(Name = "Select Commission Type")]
        public int CommissionTypeId { get; set; }

        [Required(ErrorMessage = "Please enter From Amont")]
        [Display(Name = "Enter From Amount")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers allowed.")]
        public decimal FromAmount { get; set; }

        [Required(ErrorMessage = "Please enter To Amont")]
        [Display(Name = "Enter To Amount")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers allowed.")]
        public decimal ToAmount { get; set; }

        [Required(ErrorMessage = "Please enter Commission Value")]
        [Display(Name = "Enter Commission Value")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        public decimal CommissionValue { get; set; }

        [Required(ErrorMessage = "Please select Agency")]
        [Display(Name = "Select Agency")]
        public long EntityId { get; set; }
        public string AgencyName { get; set; }
        public bool IsCustom { get; set; } = false;

        [Display(Name = "Agency Type")]
        public int EntityTypeId { get; set; } = 0;
        public string EntityTypeName { get; set; }

        public decimal DefaultCommissionValue { get; set; }

        public long ParentSlabCommissionId { get; set; } = 0;
    }
}