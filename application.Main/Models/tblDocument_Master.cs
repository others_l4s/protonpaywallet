﻿using System;
using System.ComponentModel.DataAnnotations;

namespace application.Main.Models
{
    public class tblDocument_Master : commonProperties
    {
        public int DocumentId { get; set; } = 0;

        [Required(ErrorMessage = "Please select Company")]
        public int CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Document Name")]
        [StringLength(15, ErrorMessage = "Length should not exceed 15 characters", MinimumLength = 2)]
        [Display(Name = "Enter Document Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Characters allowed.")]
        public string DocumentName { get; set; }

        [Required(ErrorMessage = "Please enter Document Length")]
        [Display(Name = "Enter Document Length")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public int DocumentLength { get; set; }

        public string CompanyName { get; set; }

        public string StatusName { get; set; }
    }
}