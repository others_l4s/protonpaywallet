﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_remitter
    {
        public string id { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string kycid { get; set; }
        public List<string> document_list { get; set; }
    }
}