﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_validate_bank_account_req
    {
        public string remittermobile { get; set; }
        public string account_number { get; set; }
        public string agentid { get; set; }
    }
}