﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_transfer_Response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public Nepal_transfer_data_resp data { get; set; }
    }
}