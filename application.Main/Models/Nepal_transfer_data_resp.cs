﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_transfer_data_resp
    {
        public string ipay_id { get; set; }
        public object ref_no { get; set; }
        public object opr_id { get; set; }
        public string opening_bal { get; set; }
        public string transfer_amount { get; set; }
        public string exchangerate { get; set; }
        public string payoutamount { get; set; }
        public string servicecharge { get; set; }
        public string transaction_amount { get; set; }
        public string transaction_status { get; set; }
        public string charged_amount { get; set; }
    }
}