﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class changePassword
    {
        [Required(ErrorMessage = "Please enter Old Password")]
        [StringLength(20, ErrorMessage = "Please enter valid Old Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Old Password")]
        [RegularExpression(Helper.regAlphaNumericSpecialSymbols, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string oldPassword { get; set; }

        [Required(ErrorMessage = "Please enter New Password")]
        [StringLength(20, ErrorMessage = "Please enter valid New Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter New Password")]
        public string newPassword { get; set; }

        [Required(ErrorMessage = "Please enter Confirm Password")]
        [StringLength(20, ErrorMessage = "Please enter valid New Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Confirm Password")]
        public string confirmPassword { get; set; }
    }
}