﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_BeneficiaryInDB_save
    {
        public string token { get; set; }
        public long NepalBeneficiaryId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string AccountNo { get; set; }
        public string BranchId { get; set; }
        public string Mode { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;
        public Nullable<bool> IsOTPVerified { get; set; } = false;
        public string remitterid { get; set; }
        public string beneficiaryid { get; set; }
    }
}