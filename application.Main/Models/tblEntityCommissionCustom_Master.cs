﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntityCommissionCustom_Master:commonProperties
    {
        public int ProductId { get; set; }
        public int CommissionCustomId { get; set; }

        [Required(ErrorMessage = "Please Select Commission")]
        [Display(Name = "Select Commission")]
        public int CommissionId { get; set; }

        [Required(ErrorMessage = "Please Select Agency")]
        [Display(Name = "Select Agency")]
        public int EntityId { get; set; } = 0;

        [Required(ErrorMessage = "Please Select Commission Type")]
        [Display(Name = "Select Commission Type")]
        public int CommissionTypeId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Commission Value")]
        [Display(Name = "Custom Commission Value")]
        public decimal CommissionValue { get; set; } = 0;

        public string StatusName { get; set; }
        public string AgencyName { get; set; }
        public string CommissionTypeName { get; set; }
        public string ProductCode { get; set; }
        
    }
}