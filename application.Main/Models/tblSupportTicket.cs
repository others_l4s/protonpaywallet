﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSupportTicket : commonProperties
    {
        public int TicketId { get; set; }
        public long EntityUserId { get; set; } = 0;
        public Nullable<int> DepartmentId { get; set; } = 0;

        [Required(ErrorMessage = "Please select Problem")]
        [Display(Name = "Select Problem")]
        public int ProblemId { get; set; }

        [Required(ErrorMessage = "Please enter Comment")]
        [StringLength(1000)]
        [Display(Name = "Enter Comment")]
        public string Comments { get; set; }

        [Display(Name = "Attachment")]
        public string AttachmentName { get; set; }

        public bool IsRaised { get; set; }

        [Display(Name = "Close Ticket")]
        public Nullable<bool> IsClosed { get; set; }
        [Display(Name = "Re-Open Ticket")]
        public Nullable<bool> IsReopened { get; set; }
        public Nullable<System.DateTime> ReOpenedDateTime { get; set; }
        public Nullable<System.DateTime> RaisedDateTime { get; set; }
        public Nullable<System.DateTime> ClosedDateTime { get; set; }
        public string ProblemName { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string AgencyName { get; set; }
        public int EntityTypeId { get; set; } = 0;
        public string EntityTypeName { get; set; }
        public int CompanyId { get; set; } = 0;
        public bool FromMobileDevice { get; set; } = false;
        public long EntityId { get; set; } = 0;
        public string displayTicketId { get; set; }

        public string displayCreatedDateTime { get; set; }
    }
}