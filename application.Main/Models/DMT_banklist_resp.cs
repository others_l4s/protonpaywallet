﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_banklist_resp
    {
        public string bankCode { get; set; }
        [Display(Name="BANKNAME")]
        public string bankName { get; set; }
        public string channelsSupported { get; set; }
        public string accVerAvailabe { get; set; }
        public string ifsc { get; set; }
        public string ifscStatus { get; set; }
    }
}