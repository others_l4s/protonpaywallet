﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSubMenu_Company_Permission : commonProperties
    {
        [Display(Name = "Select Menu")]
        [Required(ErrorMessage = "Please Select Menu")]
        public int MainMenuId { get; set; } = 0;
        public string MainMenuName { get; set; }
        public long MenuPermissionId { get; set; } = 0;

        [Display(Name = "Select Company")]
        [Required(ErrorMessage = "Please Select Company")]
        public int CompanyId { get; set; } = 0;
        public bool CanView { get; set; } = false;
        public bool CanEdit { get; set; } = false;
        public bool CanAdd { get; set; } = false;
        public bool CanDelete { get; set; } = false;

        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }
    }
}