﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class BANKIT_AEPS_getToken_Request
    {
        public string token { get; set; }
        public string agentAuthId { get; set; }
        public string agentAuthPassword { get; set; }
        public string retailerId { get; set; }
        public string apiId { get; set; }
    }
}