﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public partial class tblCompany_Type : commonProperties
    {
        public int CompanyTypeId { get; set; } = 0;
        [Required(ErrorMessage = "Please enter Company Name")]
        [StringLength(50)]
        [Display(Name = "Company Type")]
        public string CompanyTypeName { get; set; }
        public string StatusName { get; set; }
    }   
}