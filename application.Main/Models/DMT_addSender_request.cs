﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_addSender_request
    {
        public string token { get; set; }
        public string agentCode { get; set; }
        [Display(Name = "Enter Mobile No.")]
        //[Required(ErrorMessage = "Please Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string customerId { get; set; }

        [Display(Name = "Enter Beneficiary Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string accountNumber { get; set; }
    }
}