﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMenu_EntityType_Permission
    {
        public int MainMenuId { get; set; }
        public string MainMenuName { get; set; }
        public string ExternalURL { get; set; }
        public string ExternalURLTarget { get; set; }
        public int DisplayOrder { get; set; }
        public string IconCSS { get; set; }
        public bool CanView { get; set; }
        public int EntityTypeId { get; set; }

        public int SubMenuId { get; set; }

        public string SubMenuName { get; set; }
    }
}