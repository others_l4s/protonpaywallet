﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_register_req
    {
        public string remitterid { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name")]
        public string name { get; set; }
        [Required(ErrorMessage ="Please Select Gender")]
        [Display(Name = "Gender")]
        public string gender { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please Enter Address")]
        [StringLength(200, ErrorMessage = "Please Enter Valid Address", MinimumLength = 10)]
        public string address { get; set; }

        public string mode { get; set; }
        [Display(Name = "Account Number")]
        [StringLength(20,ErrorMessage ="Please Enter 20 Character Only",MinimumLength =20)]
        [Required(ErrorMessage = "Please Enter Account")]
        public string account { get; set; }
        [Display(Name ="Branch")]        
        [Required(ErrorMessage = "Please Select Branch")]
        public string branchid { get; set; }
        public string MobileNo { get; set; }
    }
}