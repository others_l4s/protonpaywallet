﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_remitter_details_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public Nepal_remitter_details_data data { get; set; }
    }
}