﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCommissionType : commonProperties
    {
        public int CommissionTypeId { get; set; }
        public string CommissionTypeName { get; set; }        
    }
}