﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class BankIT_AEPS_MicroATM_Transactions : commonProperties
    {
        public long BankITAEPSTransactionId { get; set; }
        public string rrn { get; set; }
        public string stanNo { get; set; }
        public string txnId { get; set; }
        public string aepstxnId { get; set; }
        public string action { get; set; }
        public string device { get; set; }
        public string status { get; set; }
        public string txnStatus { get; set; }
        public string bankName { get; set; }
        public string uId { get; set; }
        public string Agent_Id { get; set; }
        public string Service { get; set; }
        public decimal Amount { get; set; }
        public long EntityId { get; set; }
        public bool AmountCredited { get; set; }
        public string ClientRefID { get; set; }
        public Nullable<bool> IsMicroATMService { get; set; }
        public string ResponseAPI { get; set; }
        public int CompanyId { get; set; }
        public string AgencyName { get; set; }
        public string Telephone1 { get; set; }
        public string EmailId1 { get; set; }
        public long ParentEntityId { get; set; }
        public string IPAddress { get; set; }
    }
}