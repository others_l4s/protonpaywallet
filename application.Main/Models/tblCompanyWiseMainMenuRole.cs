﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompanyWiseMainMenuRole : commonProperties
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string MainMenuName { get; set; }
        public int MainMenuId { get; set; }
        public int SubMenuPermissionId { get; set; }
        public int RoleId { get; set; }
        public bool CanView { get; set; }
        public bool CanEdit { get; set; }
        public bool CanAdd { get; set; }
        public bool CanDelete { get; set; }
    }
}