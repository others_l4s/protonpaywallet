﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class outletKYCStatus
    {
        public string StatusCode { get; set; }
        public bool PanCardRequired { get; set; }
        public bool AadhaarCardRequired { get; set; }
    }

    public class primeURLs
    {
        public string primeCompleteURL { get; set; }
        public string primePostData { get; set; }
    }
}