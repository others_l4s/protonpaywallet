﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Models
{
    public class tblMainMenu_Master : commonProperties
    {
        public int MainMenuId { get; set; } = 0;
      
        [Required(ErrorMessage = "Please select Company")]
        [Display(Name = "Select Company")]
        public string ContactPersonName { get; set; }
       
        [Required(ErrorMessage = "Please enter Menu Name")]
        [StringLength(100, ErrorMessage = "Please enter Menu Name", MinimumLength = 3)]
        [Display(Name = "Enter Menu Name")]
        //[RegularExpression(Helper.regAlphaNumericSpecialSymbols, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string MainMenuName { get; set; }

        [StringLength(500, ErrorMessage = "Please enter Menu URL", MinimumLength = 5)]
        [Display(Name = "Enter Menu URL")]
        public string ExternalURL { get; set; }

        [Display(Name = "Select Target Window")]
        public string ExternalURLTarget { get; set; }
        public Nullable<int> RoleStatusId { get; set; }
        public string RoleStatusName { get; set; }
        public string StatusName { get; set; }
        public int DisplayOrder { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Enter ICON CSS")]
        [Display(Name = "Enter ICON CSS")]
        public string IconCSS { get; set; }
    }
}