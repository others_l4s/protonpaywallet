﻿using System;
using System.ComponentModel.DataAnnotations;

namespace application.Main.Models
{
    public class tblRole_Master : commonProperties
    {
        public int RoleId { get; set; } = 0;

        [Required(ErrorMessage = "Please select Company")]
        public Nullable<int> CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Role Name")]
        [StringLength(50, ErrorMessage = "Length should not exceed 50 characters", MinimumLength = 2)]
        [Display(Name = "Enter Role Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Characters allowed.")]
        public string RoleName { get; set; }

        public string CompanyName { get; set; }

        public string StatusName { get; set; }

    }
}
