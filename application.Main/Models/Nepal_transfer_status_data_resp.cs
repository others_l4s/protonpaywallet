﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_transfer_status_data_resp
    {
        public string transaction_dt { get; set; }
        public string external_id { get; set; }
        public string order_id { get; set; }
        public string serviceprovider_id { get; set; }
        public string product_key { get; set; }
        public string transaction_account { get; set; }
        public string transaction_amount { get; set; }
        public string transaction_status { get; set; }
        public string transaction_description { get; set; }
        public Nepal_transfer_status_AdditionalDetails_resp additional_details { get; set; }
    }
}