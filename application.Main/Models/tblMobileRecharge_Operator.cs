﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMobileRecharge_Operator : commonProperties
    {
        public long OperatorId { get; set; }

        [Required(ErrorMessage = "Please enter Operator Name")]
        [StringLength(200, ErrorMessage = "Length should not exceed 200 characters")]
        [Display(Name = "Enter Operator")]
        public string OperatorName { get; set; }

        [Required(ErrorMessage = "Please select Operator Type")]
        [Display(Name = "Select Operator Type")]
        public long OperatorTypeId { get; set; }
        public string OperatorTypeName { get; set; }
        public string OperatorNType { get; set; }

        [Display(Name = "Upload Image")]
        public string OperatorImage { get; set; }
    }
}