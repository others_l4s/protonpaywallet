﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblManual_DebitCredit : commonProperties
    {
        public long ManualWalletUpdateId { get; set; }
        public int CompanyId { get; set; }

        [Display(Name = "Select Agency")]
        [Required(ErrorMessage = "Please select Agency")]
        public long EntityId { get; set; }

        [Required(ErrorMessage = "Please enter Amount in Numeric Format")]
        [Display(Name = "Amount")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Please enter valid value in numbers.")]
        public decimal Amount { get; set; }

        [Display(Name = "Select Product")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Please enter Remark")]
        [StringLength(100, ErrorMessage = "Please enter Remark", MinimumLength = 5)]
        [Display(Name = "Remark")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string Remark { get; set; }

        [Display(Name = "Apply Credit")]
        public bool IsCredit { get; set; }
        public string AgencyName { get; set; }

        [Display(Name = "Select Agency Type")]
        public int EntityTypeId { get; set; }
        public long ParentEntityId { get; set; }
        public bool DoneByParent { get; set; }
        public string ParentAgencyName { get; set; }
    }
}