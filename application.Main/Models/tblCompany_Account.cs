﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompany_Account : commonProperties
    {
        public long AccountId { get; set; }
        public int CompanyId { get; set; }
        public decimal Amount { get; set; }
        public decimal RunningBalance { get; set; }
        public string AccountTransactionId { get; set; }
        public string Description { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionType { get; set; }
        public bool IsCredit { get; set; }
        public long AdminUserId { get; set; }
        public long EntityId { get; set; }
        public string AgencyName { get; set; }
    }
}