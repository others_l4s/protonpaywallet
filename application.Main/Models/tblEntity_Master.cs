﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_Master : commonProperties
    {
        public long EntityId { get; set; } = 0;

        [Display(Name = "Select Company")]
        [Required(ErrorMessage = "Please select Company")]
        public int CompanyId { get; set; } = 0;
        public string CompanyName { get; set; }
        public Nullable<bool> IsDefaultCompany { get; set; }

        [Required(ErrorMessage = "Please enter Agency Name")]
        [StringLength(100, ErrorMessage = "Please enter Agency Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Agency Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string AgencyName { get; set; }

        [StringLength(100)]
        [Display(Name = "Enter Company Website")]
        [RegularExpression(Helper.regWebsiteAddress, ErrorMessage = "Invalid Website format.")]
        //[Required(ErrorMessage = "Please enter Website")]
        public string Website { get; set; }

        [StringLength(100)]
        [Display(Name = "Enter Company Domain")]
        //[Required(ErrorMessage = "Please enter Domain")]
        public string Domain { get; set; }

        [Required(ErrorMessage = "Please enter Office Address 1")]
        [StringLength(500, ErrorMessage = "Please enter Office Address 1")]
        [Display(Name = "Enter Company Address 1")]
        public string OfficeAddress1 { get; set; }

        [StringLength(500)]
        [Display(Name = "Enter Office Address 2")]
        public string OfficeAddress2 { get; set; }

        [StringLength(200)]
        [Display(Name = "Enter Landmark")]
        public string Landmark { get; set; }

        [Display(Name = "Select State")]
        //[Required(ErrorMessage = "Please select State")]
        public Nullable<int> StateId { get; set; } = 0;
        public string StateName { get; set; }

        [Display(Name = "Select City")]
        //[Required(ErrorMessage = "Please select City")]
        public Nullable<int> CityId { get; set; } = 0;
        public string CityName { get; set; }

        [Display(Name = "Select Country")]
        // [Required(ErrorMessage = "Please select Country")]
        public Nullable<int> CountryId { get; set; } = 0;
        public string CountryName { get; set; }

        [StringLength(15)]
        [Display(Name = "Enter PinCode")]
        public string PinCode { get; set; }

        [Required(ErrorMessage = "Please enter Telephone 1")]
        [StringLength(15, ErrorMessage = "Please enter valid Telephone No. 1", MinimumLength = 10)]
        [Display(Name = "Enter Company Telephone 1")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Telephone1 { get; set; }

        [StringLength(15, ErrorMessage = "Please enter valid Telephone No. 2", MinimumLength = 10)]
        [Display(Name = "Enter Company Telephone 2")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Telephone2 { get; set; }

        [Required(ErrorMessage = "Please enter EmailId 1")]
        [StringLength(100, ErrorMessage = "Please enter valid EmailId 1", MinimumLength = 6)]
        [Display(Name = "Enter EmailId 1")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId1 { get; set; }

        [StringLength(100, ErrorMessage = "Please enter valid EmailId 2", MinimumLength = 6)]
        [Display(Name = "Enter EmailId 2")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId2 { get; set; }

        [StringLength(50)]
        [Display(Name = "Enter Bank Account Title")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string BankAccountTitle { get; set; }

        [StringLength(50)]
        [Display(Name = "Enter Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string BankAccountNumber { get; set; }

        [StringLength(20)]
        [Display(Name = "Enter Bank IFSC Code")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankIFSCCode { get; set; }

        [StringLength(100)]
        [Display(Name = "Enter Bank Branch")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankBranch { get; set; }

        [Display(Name = "Bank Account Type")]
        public Nullable<int> BankAccountTypeId { get; set; }
        public string BankAccountTypeName { get; set; }
        public int EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public string StatusName { get; set; }

        [Display(Name = "Account Handler")]
        [Required(ErrorMessage = "Please select Account Handler")]
        public Nullable<int> AccountHandlerUserId { get; set; }

        [Required(ErrorMessage = "Please enter Person Name")]
        [StringLength(100, ErrorMessage = "Please enter Person Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Person Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string PersonName { get; set; }
        public string LoginName { get; set; }
        public Nullable<bool> IsBankVerified { get; set; }
        public Nullable<System.DateTime> BankVerifiedDateTime { get; set; }
        public string AgencyLogoName { get; set; }

        [StringLength(50)]
        [Display(Name = "Enter Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankName { get; set; }

        [Display(Name = "Select Parent")]
        [Required(ErrorMessage = "Please select Parent")]
        public long ParentEntityId { get; set; }
        public string ParentAgencyName { get; set; }

        [Display(Name = "Select Master Distributor")]
        [Required(ErrorMessage = "Please select Master Distributor")]
        public long MDParentEntityId { get; set; }


        [Display(Name = "Select Distributor")]
        [Required(ErrorMessage = "Please select Distributor")]
        public long DParentEntityId { get; set; }

        [Display(Name = "Minimum Balance Main")]
        [Required(ErrorMessage = "Please Enter Minimum Balance Maintain")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public Nullable<int> MinimumBalanceMaintain { get; set; }

        public string Mobile { get; set; }
        public string EmailId { get; set; }
    }
}