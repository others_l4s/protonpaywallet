﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_BankItINDB_SaveTransaction
    {
        public string token { get; set; }
        public long BankItTransactionId { get; set; }
        public string CustomerId { get; set; }
        public string TransferType { get; set; }
        public string RecipientId { get; set; }
        public string ClientRefId { get; set; }
        public string TxnId { get; set; } = null;
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;
    }
}