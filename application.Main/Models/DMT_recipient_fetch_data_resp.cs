﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_recipient_fetch_data_resp
    {
        [StringLength(50)]
        [Display(Name = "Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        [Required(ErrorMessage = "Please Enter Account Number")]
        public string udf2 { get; set; }

        [StringLength(20)]
        [Display(Name = "Bank IFSC Code")]
        [Required(ErrorMessage = "Please Enter IFSC Code")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string udf1 { get; set; }
        public string recipientId { get; set; }

        [Required(ErrorMessage = "Please enter Beneficiary Name")]
        [StringLength(100, ErrorMessage = "Please enter Beneficiary Name", MinimumLength = 3)]
        [Display(Name = "Beneficary Name as per Bank Register")]
        public string recipientName { get; set; }

        [StringLength(50)]
        [Required(ErrorMessage = "Please select Bank")]
        [Display(Name = "Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string bankName { get; set; }
        public string channel { get; set; }
        public string bankCode { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No.")]
        [StringLength(15, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string mobileNo { get; set; }
        public string customerId { get; set; }

        [Required(ErrorMessage = "Please select Transfer Type (NEFT or IMPS)")]
        public string transferType { get; set; }

        public int IMPSAdditionCharge { get; set; } = 0;

        [Display(Name = "Verify Account")]
        public Boolean verifyAccount { get; set; }
    }
}