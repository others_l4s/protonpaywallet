﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class BankITAEPSRequestQueue
    {
        public string BCAAgentId { get; set; }
        public long BankItAEPSId { get; set; }
        public long EntityId { get; set; }
        public string AgentAuthId { get; set; }
        public string AgentAuthPassword { get; set; }
        public string RetailerId { get; set; }
        public string APIId { get; set; }
        public System.DateTime AEPSRequestedOnDateTime { get; set; }
        public Nullable<System.DateTime> AEPSApprovedOnDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> AEPSRejected { get; set; }
        public int AccessAmount { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string AgencyName { get; set; }
        public string OfficeAddress1 { get; set; }
        public string OfficeAddress2 { get; set; }
        public string Landmark { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string CountryName { get; set; }
        public string PinCode { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string EmailId1 { get; set; }

        public string PersonName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public string DOB { get; set; }
        public string PanCardNo { get; set; }
        public string PanCardPhoto { get; set; }
        public string PanCardPhoto2 { get; set; }
        public string AadhaarCardNo { get; set; }
        public string AadhaarCardPhoto { get; set; }
        public Nullable<int> IsKYCStatus { get; set; }
        public string KYCStatusRemark { get; set; }

        public string AlternateMobile { get; set; }

        public string District { get; set; }
    }
}