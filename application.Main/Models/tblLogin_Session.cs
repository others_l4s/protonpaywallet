﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblLogin_Session
    {
        public long LoginSessionId { get; set; }
        public long LoginId { get; set; }
        public string accessToken { get; set; }
        public System.DateTime accessTokenValidity { get; set; }
        public System.DateTime accessTokenGeneratedDateTime { get; set; }

        public int EntityTypeId { get; set; }

        public string IPAddress { get; set; }
        public string OTP { get; set; }
        public Nullable<bool> OTPVerified { get; set; }
        public string MobileNo { get; set; }
        public bool FromMobileDevice { get; set; }

        public bool OTPVerificationRequired { get; set; }
    }
}