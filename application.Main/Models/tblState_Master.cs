﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblState_Master:commonProperties
    {
        public int StateId { get; set; }

        [Required(ErrorMessage = "Please select Country")]
        public Nullable<int> CountryId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter State Name")]
        [StringLength(50)]
        [Display(Name = "Enter State Name")]
        public string StateName { get; set; }
        public string CountryName { get; set; }        
        public string StatusName { get; set; }
    }
}