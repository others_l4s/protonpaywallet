﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_searchtxn_data_resp
    {
        public string clientRefId { get; set; }
        public string transactionDate { get; set; }
        public string amount { get; set; }
    }
}