﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_remove_validate_req
    {
        public string beneficiaryid { get; set; }
        public string remitterid { get; set; }

        [Required(ErrorMessage ="Please Enter OTP")]
        [Display(Name ="OTP")]
        public string otp { get; set; }
    }
}