﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_createSender_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        
        public string token { get; set; }
        public string agentCode { get; set; }

        [StringLength(100, ErrorMessage = "Please enter MobileNo", MinimumLength = 10)]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Digit allowed.")]
        [Required(ErrorMessage = "Please Enter MobileNo")]
        [Display(Name =("Mobile NO"))]
        public string customerId { get; set; }

        [StringLength(100, ErrorMessage = "Please enter Name", MinimumLength = 5)]
        [Required(ErrorMessage = "Please Enter Name")]
        [Display(Name = ("Name"))]
        public string name { get; set; }

        [Required(ErrorMessage = "Please Enter Address")]
        [Display(Name = ("Address"))]
        public string address { get; set; }

        [Required(ErrorMessage = "Please Enter Date Of Birth")]
        [Display(Name = ("Date Of Birth"))]
        public string dateOfBirth { get; set; }

        [Required(ErrorMessage = "Please Enter OTP")]
        [Display(Name = ("OTP"))]
        public string otp { get; set; }
    }
}