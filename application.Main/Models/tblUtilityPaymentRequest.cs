﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblUtilityPaymentRequest
    {
        [Required(ErrorMessage = "Please select Operator")]
        [Display(Name = "Select Provider")]
        public long OperatorVendorId { get; set; }

        [Required(ErrorMessage = "Please enter Mobile")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string MobileNo { get; set; }

        [Display(Name = "Enter Amount")]
        [Required(ErrorMessage = "Please enter Amount")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public Int32 Amount { get; set; }

        [Required(ErrorMessage = "Please enter Mobile")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string customermobile { get; set; }


        [Required(ErrorMessage = "Please enter DTH Number")]
        [StringLength(14, ErrorMessage = "Please enter valid DTH Number", MinimumLength = 10)]
        [Display(Name = "Enter DTH Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string DTHNumber { get; set; }

    }
}