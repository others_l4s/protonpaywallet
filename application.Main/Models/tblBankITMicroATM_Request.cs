﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblBankITMicroATM_Request
    {
        public long BankItMicroATMId { get; set; }
        public long EntityId { get; set; }
        public string partnerAgentId { get; set; }
        public System.DateTime RequestDateTime { get; set; }
        public Nullable<System.DateTime> ApprovedDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> IsRejected { get; set; }
        public int AccessAmount { get; set; }
    }
}