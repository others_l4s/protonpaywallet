﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_User : commonProperties
    {
        public string AgencyName { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string StatusName { get; set; }
        public string RoleName { get; set; }
        public string DepartmentName { get; set; }
        public string DesignationName { get; set; }

        [Display(Name = "Agency")]
        public long EntityId { get; set; } = 0;

        public long EntityUserId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Mobile No. (Login Id)")]
        [StringLength(10, ErrorMessage = "Please enter valid only 10 digit mobile no.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Please enter only 10 digit mobile no.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter EmailId")]
        [StringLength(100, ErrorMessage = "Please enter valid EmailId", MinimumLength = 6)]
        [Display(Name = "Enter Email Id")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId { get; set; }

        [Required(ErrorMessage = "Please enter 10 digit mobile no.")]
        [StringLength(10, ErrorMessage = "Please enter 10 digit mobile no.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Please enter Address 1")]
        [StringLength(500, ErrorMessage = "Please enter Address 1")]
        [Display(Name = "Enter Address 1")]
        public string Address1 { get; set; }

        [StringLength(500)]
        [Display(Name = "Enter Address 2")]
        public string Address2 { get; set; }

        [Display(Name = "Select State")]
        [Required(ErrorMessage = "Please select State")]
        public int StateId { get; set; }

        [Display(Name = "Select City")]
        [Required(ErrorMessage = "Please select City")]
        public int CityId { get; set; }

        [Display(Name = "Select Country")]
        [Required(ErrorMessage = "Please select Country")]
        public int CountryId { get; set; }

        [Display(Name = "Enter PinCode")]
        public string PinCode { get; set; }
        public string DOB { get; set; }

        [Display(Name = "Select Department")]
        [Required(ErrorMessage = "Please select Department")]
        public int DepartmentId { get; set; } = 0;

        [Display(Name = "Select Designation")]
        [Required(ErrorMessage = "Please select Designation")]
        public int DesignationId { get; set; } = 0;

        [Display(Name = "Select Role")]
        [Required(ErrorMessage = "Please select Role")]
        public int RoleId { get; set; } = 0;

        [Display(Name = "Enter Remark")]
        public string StatusRemark { get; set; }
        public string CityName { get; set; }
        public bool IsEmailVerified { get; set; }
        public Nullable<System.DateTime> EmailVerifiedDateTime { get; set; }
        public Nullable<bool> LeadUser { get; set; }
        public int Pin { get; set; }


        [Required(ErrorMessage = "Please enter Person Name")]
        [StringLength(100, ErrorMessage = "Please enter Person Name Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Enter Person Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string PersonName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(20, ErrorMessage = "Please enter valid Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Password")]
        [RegularExpression(Helper.regAlphaNumericSpecialSymbols, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please enter Confirm Password")]
        [StringLength(20, ErrorMessage = "Please enter valid Password (Minimum length is 4 and Max length is 20", MinimumLength = 4)]
        [Display(Name = "Enter Confirm Password")]
        [RegularExpression(Helper.regAlphaNumericSpecialSymbols, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string ConfirmPassword { get; set; }

        public Boolean termsCondition { get; set; }

        [Required(ErrorMessage = "Please enter OTP")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string OTP { get; set; }
    }
}