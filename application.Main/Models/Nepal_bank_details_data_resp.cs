﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_bank_details_data_resp
    {
        public int branchid { get; set; }
        public string bank_name { get; set; }
        public string branch_name { get; set; }
        public string address { get; set; }
        public int is_down { get; set; }
    }
}