﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMobileRecharge_Vendor : commonProperties
    {
        public int OperatorVendorId { get; set; }

        [Display(Name = "Select Operator")]
        public long OperatorId { get; set; }

        public string OperatorName { get; set; }
        public long OperatorTypeId { get; set; }
        public string OperatorTypeName { get; set; }

        [Required(ErrorMessage = "Please enter Vendor Name")]
        [Display(Name = "Enter Vendor Name")]
        public string VendorName { get; set; }

        [Required(ErrorMessage = "Please enter Operator Alias")]
        [Display(Name = "Enter Operator Alias")]
        public string OperatorAlias { get; set; }

        [Required(ErrorMessage = "Please enter Key 1")]
        [Display(Name = "Enter Key 1")]
        public string Key1 { get; set; }

        [Display(Name = "Enter Key 2")]
        public string Key2 { get; set; }

        [Display(Name = "Enter Key 3")]
        public string Key3 { get; set; }

        [Display(Name = "Enter Key 4")]
        public string Key4 { get; set; }

        [Display(Name = "Enter Key 5")]
        public string Key5 { get; set; }

        [Display(Name = "Enter Key 6")]
        public string Key6 { get; set; }

        [Display(Name = "Enter Key 7")]
        public string Key7 { get; set; }

        [Display(Name = "Enter Key 8")]
        public string Key8 { get; set; }

        [Display(Name = "Enter Key 9")]
        public string Key9 { get; set; }

        [Display(Name = "Enter Key 10")]
        public string Key10 { get; set; }

        [Display(Name = "Enter API URL")]
        public string APIURL { get; set; }

        [Display(Name = "Activate Default Gateway")]
        public bool DefaultGateway { get; set; }

        [Required(ErrorMessage = "Please enter Default Commission")]
        [Display(Name = "Enter Default Commission")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers & Decimal allowed.")]
        public decimal DefaultCommissionValue { get; set; }

        [Display(Name = "Fetch Bill")]
        public bool FetchBill { get; set; }

        [Display(Name = "BBPS Enabled")]
        public bool BBPSEnabled { get; set; }

        [Display(Name = "Outlet Verification")]
        public bool OutletVerification { get; set; }
    }
}