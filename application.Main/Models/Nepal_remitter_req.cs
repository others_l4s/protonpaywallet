﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_remitter_req
    {
        public string mobile { get; set; }
        [Display(Name ="Name")]
        [Required(ErrorMessage ="Please Enter Name")]
        public string name { get; set; }
        [Display(Name = "Pincode")]
        [Required(ErrorMessage = "Please Enter Pincode")]
        public string pincode { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please Select Gender")]
        public string gender { get; set; }

        [Display(Name = "Date Of Birth")]
        [Required(ErrorMessage = "Please Select Date of Birth")]
        public string dob { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please Enter Address")]
        public string address { get; set; }

        [Display(Name = "Document Type")]
        [Required(ErrorMessage = "Please Select Document Type")]
        public string document_type { get; set; }

        [Display(Name = "Document Number")]
        [Required(ErrorMessage = "Please Enter Document Number")]
        public string document_number { get; set; }
        [Display(Name = "Document")]
        [Required(ErrorMessage = "Please Select Document")]
        public string document_url { get; set; }      
        
    }
}