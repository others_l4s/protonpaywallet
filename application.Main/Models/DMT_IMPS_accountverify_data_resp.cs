﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_IMPS_accountverify_data_resp
    {
        public string customerId { get; set; }
        public string name { get; set; }
        public string bankName { get; set; }
        public string clientRefId { get; set; }
        public string txnId { get; set; }
        public string impsRespCode { get; set; }
        public string txnStatus { get; set; }
    }
}