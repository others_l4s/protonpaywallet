﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace application.Main.Models
{
    public class tblSubMenu_Master : commonProperties
    {
        public int SubMenuId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Sub Menu Name")]
        [StringLength(100, ErrorMessage = "Please enter Sub Menu Name", MinimumLength = 3)]
        [Display(Name = "Enter Sub Menu Name")]
        //[RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string SubMenuName { get; set; }

        [Required(ErrorMessage = "Please select Main Menu Name")]
        [Display(Name = "Select Main Menu")]
        public Nullable<int> MainMenuId { get; set; }

        [Required(ErrorMessage = "Please select Company")]
        [Display(Name = "Select Company")]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Please select Role")]
        [Display(Name = "Select Role")]
        public int RoleId { get; set; } = 0;
        public string MainMenuName { get; set; }

        [StringLength(500, ErrorMessage = "Please enter Menu URL", MinimumLength = 2)]
        [Display(Name = "Enter Menu URL")]
        public string ExternalURL { get; set; }

        [Display(Name = "Select Target Window")]
        public string ExternalURLTarget { get; set; }
        public string StatusName { get; set; }

        public int DisplayOrder { get; set; }

        [AllowHtml]
        [Display(Name = "Enter ICON CSS")]
        public string IconCSS { get; set; }
    }
}