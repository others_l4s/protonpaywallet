﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class NepalTransactionView_Request
    {
        public string token { get; set; }
        public string BeneficiaryName { get; set; }
        public string RemitterName { get; set; }
        public string IpayID { get; set; }
        public string Statuscode { get; set; }
        public Nullable<System.DateTime> createddatetime { get; set; }
        public Nullable<bool> status { get; set; }
        public string RemitterID { get; set; }
        public string BeneficiaryID { get; set; }
        public string TransferAmount { get; set; }
        public long NepalTransactionID { get; set; }
    }
}