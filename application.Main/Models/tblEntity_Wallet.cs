﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_Wallet
    {
        public long WalletId { get; set; }
        public decimal WalletBalance { get; set; }
        public long EntityId { get; set; }
        public string AgencyName { get; set; }
        public int CompanyId { get; set; }
        public long ParentEntityId { get; set; }
        public Nullable<int> MinimumBalanceMaintain { get; set; }
    }
}