﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_TransactionDetail_pending_resp
    {
        public string customerId { get; set; }
        public string clientRefId { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string transactionDate { get; set; }
        public string amount { get; set; }
    }
}