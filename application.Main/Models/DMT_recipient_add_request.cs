﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_recipient_add_request
    {
        [Required(ErrorMessage = "Please Enter UserToken.")]
        public string token { get; set; }
        public string agentCode { get; set; }
        [Display(Name ="Bank Name")]
        [Required(ErrorMessage ="Please Select Bank Name.")]
        public string bankName { get; set; }
        public string customerId { get; set; }

        [Display(Name = "Bank Account No")]
        [Required(ErrorMessage = "Please Enter Bank Acccount No.")]
        [StringLength(20, ErrorMessage = "Please enter valid Bank Account No.", MinimumLength = 10)]
        public string accountNo { get; set; }

        [Display(Name = "Bank IFSC Code")]
        [StringLength(15, ErrorMessage = "Please enter valid Bank IFSC Code.", MinimumLength = 5)]
        [Required(ErrorMessage = "Please Enter Bank IFSC Code.")]
        public string ifsc { get; set; }

        [Display(Name = "Mobile No")]
        [Required(ErrorMessage = "Please Enter Mobile No.")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string mobileNo { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please Enter Name.")]
        [StringLength(100, ErrorMessage = "Please enter valid Person Name", MinimumLength = 3)]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string recipientName { get; set; }
    }
}