﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_remitter_details_beneficiary_resp
    {

        public string id { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string account { get; set; }
        public string bank { get; set; }
        public string status { get; set; }
        public string last_success_date { get; set; }
        public string last_success_name { get; set; }
        public string last_success_imps { get; set; }
        public string branchid { get; set; }
        public string branch { get; set; }
        public string gender { get; set; }
        public string address { get; set; }
        public string mode { get; set; }
    }
}