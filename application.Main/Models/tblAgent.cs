﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblAgent : tblEntity_Master
    {
        [Display(Name = "Under Distributor")]
        [Required(ErrorMessage = "Please select Distributor")]
        public int ParentEntityId { get; set; }
    }
}