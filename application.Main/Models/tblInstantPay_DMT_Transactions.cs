﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblInstantPay_DMT_Transactions
    {
        public long InstantPayTransactionId { get; set; }
        public string RemitterMobileNo { get; set; }
        public string Name { get; set; }
        public string Mode { get; set; }
        public string BeneficiaryId { get; set; }
        public string ipay_id { get; set; }
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string StatusCode { get; set; }
        public string APIStatus { get; set; }
        public string Amount { get; set; }
        public string Ref_no { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string Transaction_Status { get; set; }
        public Nullable<long> EntityId { get; set; }
        public Nullable<long> EntityUserId { get; set; }
        public string RemitterName { get; set; }
        public string Surname { get; set; }
        public string RemitterId { get; set; }
        public string AgencyName { get; set; }
        public string Telephone1 { get; set; }
        public string ClientRefId { get; set; }
        public string AccountNo { get; set; }
        public string ifsc { get; set; }
        public int CompanyId { get; set; }

        public string Bank { get; set; }
        public string PayoutDirectRemark { get; set; }
        public bool IsPayoutDirect { get; set; }
        public string UpdateResponseApi { get; set; }
    }
}