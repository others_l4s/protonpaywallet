﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_recipient_delete_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        public string token { get; set; }
        public string agentCode { get; set; }
        public string customerId { get; set; }
        public string recipientId { get; set; }
    }
}