﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompany_Subscription : commonProperties
    {
        public int CompanySubscriptionId { get; set; } = 0;

        [Display(Name = "Company Name")]
        public int CompanyId { get; set; } = 0;
        public string CompanyName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmailId { get; set; }
        public string ContactPersonMobile { get; set; }

        [Display(Name = "Subscription")]
        public int SubscriptionId { get; set; }
        public string SubscriptionName { get; set; }
        public decimal SubscriptionAmount { get; set; }
        public int ValidTillDays { get; set; }

        [Display(Name = "Subscription Validity")]
        public int SubscriptionValidity { get; set; } = 0;
        public string StatusName { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string ProductCode { get; set; }
        public string AgencyName { get; set; }
        public string ContactPersonName1 { get; set; }
        public string EmailId1 { get; set; }
    }
}