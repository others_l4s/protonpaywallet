﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblPaymentMode_Master : commonProperties
    {
        public int PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
    }
}