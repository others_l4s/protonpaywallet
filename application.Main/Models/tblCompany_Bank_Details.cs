﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompany_Bank_Details : commonProperties
    {
        public long BankId { get; set; } = 0;
        public int CompanyId { get; set; } = 0;

        [StringLength(50)]
        [Required(ErrorMessage = "Please enter Bank Account Title")]
        [Display(Name = "Enter Bank Account Title")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string BankAccountTitle { get; set; }

        [Display(Name = "Enter Bank Account Number")]
        [Required(ErrorMessage = "Please enter Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string BankAccountNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Enter Bank Name")]
        [Required(ErrorMessage = "Please enter Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankName { get; set; }

        [StringLength(20)]
        [Display(Name = "Enter Bank IFSC Code")]
        [Required(ErrorMessage = "Please enter Bank IFSC Code")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankIFSCCode { get; set; }

        [StringLength(100)]
        [Display(Name = "Enter Bank Branch")]
        [Required(ErrorMessage = "Please enter Bank Branch")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankBranch { get; set; }

        [Display(Name = "Select Bank Account Type")]
        [Required(ErrorMessage = "Please select Account Type")]
        public int BankAccountTypeId { get; set; }

        [Display(Name = "Select Bank Account Type")]
        [Required(ErrorMessage = "Please select Account Type")]
        public string BankAccountTypeName { get; set; }

        [StringLength(15)]
        [Display(Name = "Enter PAN No.")]
        [Required(ErrorMessage = "Please enter Company Pan Card No.")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Invalid PAN No. format.")]
        public string PanCard { get; set; }
        public string StatusName { get; set; }
        public string CompanyName { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonEmailId { get; set; }
        public string ContactPersonMobile { get; set; }
    }
}