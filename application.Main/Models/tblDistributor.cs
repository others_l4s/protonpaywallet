﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblDistributor : tblEntity_Master
    {
        [Display(Name = "Under Master Distributor")]
        [Required(ErrorMessage = "Please select Master Distributor")]
        public int ParentEntityId { get; set; }
    }
}