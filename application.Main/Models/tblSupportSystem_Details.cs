﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSupportSystem_Details : commonProperties
    {
        public int TicketDetailId { get; set; }
        public int TicketId { get; set; }
        public string Comments { get; set; }
        public bool IsResponse { get; set; }
        public long AdminUserId { get; set; }
    }
}