﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblBookingStatus_Master:commonProperties
    {
        public int BookingStatusId { get; set; }
        
        [Required(ErrorMessage = "Please enter BookingStatusName")]
        [StringLength(50)]
        [Display(Name = "Booking Status")]
        public string BookingStatusName { get; set; }
        public string StatusName { get; set; }
    }
}