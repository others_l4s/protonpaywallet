﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMobileRecharge_VendorCommission
    {
        public long OperatorVendorCommissionId { get; set; }
        public int OperatorVendorId { get; set; }
        public string VendorName { get; set; }
        public string OperatorAlias { get; set; }
        public long OperatorId { get; set; }
        public int CommissionTypeId { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        public decimal CommissionValue { get; set; }
        public int EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public long CreatedBy { get; set; }
        public int CreatedByEntityTypeId { get; set; }
        public long EntityId { get; set; }
        public bool IsCustom { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public string AgencyName { get; set; }
        public string Key1 { get; set; }
        public string Key2 { get; set; }
        public string Key3 { get; set; }
        public string Key4 { get; set; }
        public string Key5 { get; set; }
        public string Key6 { get; set; }
        public string Key7 { get; set; }
        public string Key8 { get; set; }
        public string Key9 { get; set; }
        public string Key10 { get; set; }
        public string APIURL { get; set; }
        public bool DefaultGateway { get; set; }
        public bool Status { get; set; }
        public decimal DefaultCommissionValue { get; set; }
        public string OperatorName { get; set; }
        public string OperatorTypeName { get; set; }
        public Nullable<bool> FetchBill { get; set; }
        public Nullable<bool> BBPSEnabled { get; set; }
        public Nullable<bool> OutletVerification { get; set; }
    }
}