﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblAnnouncement_Master : commonProperties
    {
        public int AnnouncementId { get; set; } = 0;

        [Required(ErrorMessage = "Please Select Company")]
        [Display(Name = "Select Company")]
        public int CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Announcement Title")]
        [StringLength(100, ErrorMessage = "Please enter Announcement Title Minimum 4 Character", MinimumLength = 4)]
        [Display(Name = "Announcement Title")]
        public string AnnouncementTitle { get; set; }

        [Required(ErrorMessage = "Please enter Announcement Details")]
        //[StringLength(200, ErrorMessage = "Please enter Announcement Details Minimum 10 Character", MinimumLength = 10)]
        [Display(Name = "Announcement Details")]
        public string AnnouncementText { get; set; }


        [Required(ErrorMessage = "Please enter Announcement Valid Date")]
        [Display(Name = "Valid Till Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> AnnouncementValidTill { get; set; }

        public string AgencyName { get; set; }
        public string StatusName { get; set; }
        public string CompanyName { get; set; }

        [Display(Name = "Select Entity")]
        public bool ForCompany2MD { get; set; }
        public Nullable<long> EntityId { get; set; }
    }
}