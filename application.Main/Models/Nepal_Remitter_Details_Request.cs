﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_Remitter_Details_Request
    {
        public string token { get; set; }

        public Nepal_Remitter_Details_req request { get; set; }
    }
}