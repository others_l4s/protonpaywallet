﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_IMPS_remit_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public DMT_IMPS_remit_data_resp data { get; set; }
        public string Reason { get; set; }
    }
}