﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT__recipient_all_data_resp
    {
        public List<DMT_recipient_fetch_data_resp> recipientList { get; set; }
    }
}