﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMobileRecharge_OperatorType : commonProperties
    {
        public long OperatorTypeId { get; set; }

        [Required(ErrorMessage = "Please enter Operator Type")]
        [StringLength(50, ErrorMessage = "Length should not exceed 50 characters")]
        [Display(Name = "Enter Operator Type")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Characters allowed.")]
        public string OperatorTypeName { get; set; }

        [Display(Name = "Enter Operator Type Image")]
        public string OperatorTypeImage { get; set; }
    }
}