﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblDepartment_Master:commonProperties
    {
        public int DepartmentId { get; set; } = 0;

        [Required(ErrorMessage = "Please select Company")]
        public Nullable<int> CompanyId { get; set; } = 0;

        [Required(ErrorMessage = "Please enter Department Name")]
        [StringLength(50, ErrorMessage = "Length should not exceed 50 characters", MinimumLength = 2)]
        [Display(Name = "Enter Department Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Characters allowed.")]
        public string DepartmentName { get; set; }

        public string CompanyName { get; set; }

        public string StatusName { get; set; }
    }
}