﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblMyRobotic_Recharge_Transaction
    {
        public long MyRoboticRechargeId { get; set; }
        public long EntityId { get; set; }
        public long EntityUserId { get; set; }
        public string txid { get; set; }
        public string status { get; set; }
        public string opid { get; set; }
        public string mobileno { get; set; }
        public int amount { get; set; }
        public string orderid { get; set; }
        public decimal r_offer { get; set; }
        public decimal balance { get; set; }
        public string robotic_id { get; set; }
        public string lapu_no { get; set; }
        public System.DateTime rechargedatetime { get; set; }
        public string rechargeRequest { get; set; }
        public string rechargeResponse { get; set; }
        public int MyRoboticRechargeDataId { get; set; }
        public string OperatorCode { get; set; }
    }
}