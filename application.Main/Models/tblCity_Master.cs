﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCity_Master : commonProperties
    {
        public int CityId { get; set; } = 0;
        [Display(Name = "State")]
        public Nullable<int> StateId { get; set; }
        public string StateName { get; set; }

        [Required(ErrorMessage = "Please enter City Name")]
        [StringLength(50)]
        [Display(Name = "Enter City Name")]
        public string CityName { get; set; }
        public string StatusName { get; set; }
    }
}