﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCompany_Wallet
    {
        public int WalletId { get; set; }

        [Required(ErrorMessage = "Please enter Amount in Numeric Format")]
        [Display(Name = "Add Virtual Balance")]
        [RegularExpression(@"^(?!00000)[0-9]{3,8}$", ErrorMessage = "Please enter valid value in numbers.")]
        public decimal WalletBalance { get; set; }
        public int CompanyId { get; set; }
    }
}