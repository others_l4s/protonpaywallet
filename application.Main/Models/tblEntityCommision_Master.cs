﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntityCommision_Master : commonProperties
    {
        public string ProductCode { get; set; }
        public string AgencyName { get; set; }
        public long CommissionId { get; set; }

        public long EntityId { get; set; } = 0;

        [Required(ErrorMessage = "Please Select Product")]
        [Display(Name = "Select Product")]
        public int ProductId { get; set; } = 0;
        [Required(ErrorMessage = "Please Select Commission Type")]
        [Display(Name = "Select Commission Type")]
        public int CommissionTypeId { get; set; } = 0;
        [Required(ErrorMessage = "Please enter Commission Value")]
        [Display(Name = "Commission Value")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Decimal Values Allowed")]
        public decimal CommissionValue { get; set; } = 0;
        public string CommissionTypeName { get; set; }
        public bool IsCustom { get; set; }
        public int CompanyId { get; set; }

        [Display(Name = "Enter Commission Slab Wise")]
        public string CommissionSlabRangeValue { get; set; }
        public string CompanyName { get; set; }
        public int EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
    }
}