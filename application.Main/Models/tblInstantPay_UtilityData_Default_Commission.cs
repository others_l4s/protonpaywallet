﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblInstantPay_UtilityData_Default_Commission
    {
        public long InstantPayRechargeDataCommissionId { get; set; }
        public int InstantPayRechargeDataId { get; set; }
        public int CommissionTypeId { get; set; } = 1;
        public bool IsCustom { get; set; }
        public string AgencyName { get; set; }
        public string ServiceName { get; set; }
        public string ServiceType { get; set; }
        public decimal Margin { get; set; }
        public string ServiceKey { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal MDCommissionValue { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal DistCommissionValue { get; set; }

        [DisplayFormat(DataFormatString = "{0:n3}", ApplyFormatInEditMode = true)]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal AgentCommissionValue { get; set; }
        public int CompanyId { get; set; }
    }
}