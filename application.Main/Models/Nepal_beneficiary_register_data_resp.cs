﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_register_data_resp
    {
        public Nepal_beneficiary_register_remitter_resp remitter { get; set; }
        public Nepal_beneficiary_register_beneficiary_resp beneficiary { get; set; }
    }
}