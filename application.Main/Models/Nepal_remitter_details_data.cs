﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_remitter_details_data
    {
        public Nepal_remitter remitter { get; set; }
        public List<Nepal_remitter_details_beneficiary_resp> beneficiary { get; set; }
    }
}