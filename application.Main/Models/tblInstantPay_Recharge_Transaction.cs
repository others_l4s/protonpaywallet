﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblInstantPay_Recharge_Transaction : commonProperties
    {
        
        public long InstantPayRechargeId { get; set; }

        public long EntityId { get; set; }
        public long EntityUserId { get; set; }

        [Required(ErrorMessage = "Please select Service Provider")]
        public int InstantPayRechargeDataId { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        public string MobileNo { get; set; }
        public string RechargeRequest { get; set; }
        public string RechargeResponse { get; set; }
        public string status { get; set; }
        public string res_code { get; set; }
        public string res_msg { get; set; }

        [Required(ErrorMessage = "Please enter Amount")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public int Amount { get; set; }
        public string AgencyName { get; set; }
        public string ServiceName { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }
        public int CompanyId { get; set; }
        public int EntityTypeId { get; set; }
        public string ServiceType { get; set; }
        public string customermobile { get; set; }
    }
}