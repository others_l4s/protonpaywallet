﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblCountry_Master : commonProperties
    {
        public int CountryId { get; set; }

        [Required(ErrorMessage = "Please enter Country Name")]
        [StringLength(50)]
        [Display(Name = "Enter Country Name")]
        public string CountryName { get; set; }

        [Required(ErrorMessage = "Please enter Country Code")]
        [StringLength(15)]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        public string StatusName { get; set; }
    }

}