﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblInstantPay_UtilityData : commonProperties
    {
        public int InstantPayRechargeDataId { get; set; }

        [Required(ErrorMessage = "Please enter Service Name")]
        [StringLength(100, ErrorMessage = "Please enter Service Name", MinimumLength = 3)]
        [Display(Name = "Service Name")]
        public string ServiceName { get; set; }

        [Required(ErrorMessage = "Please enter Service Type")]
        [Display(Name = "Service Type")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string ServiceType { get; set; }

        [Required(ErrorMessage = "Please enter Service Key")]
        [StringLength(100, ErrorMessage = "Please enter Service Key", MinimumLength = 2)]
        [Display(Name = "Service Key")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string ServiceKey { get; set; }

        [Required(ErrorMessage = "Please enter Margin")]
        [Display(Name = "Margin")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers allowed.")]
        public decimal Margin { get; set; }

        [Display(Name = "Bill Fetch")]
        public bool BillFetch { get; set; }

        [Display(Name = "BBPS Enabled")]
        public bool BBPSEnabled { get; set; }

        [Display(Name = "Amount Range")]
        public string AmountRange { get; set; }

        [Display(Name = "Min Entry Length")]
        public int MinLength { get; set; }

        [Display(Name = "Max Entry Length")]
        public int MaxLength { get; set; }
        public int ProductId { get; set; }

        [Display(Name = "Select Product")]
        public int CompanyId { get; set; }
        public int Expr1 { get; set; }
        public string ProductCode { get; set; }
        public string AgencyName { get; set; }
        public string ContactPersonName1 { get; set; }
    }
}