﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_Payments : commonProperties
    {
        public long EntityPaymentId { get; set; }
        public long EntityId { get; set; }
        public string AgencyName { get; set; }
        public string EntityTypeName { get; set; }
        public long ParentEntityId { get; set; }

        [Required(ErrorMessage = "Please Select Mode of Payment")]
        [Display(Name = "Payment Mode")]
        public int PaymentModeId { get; set; }

        [Required(ErrorMessage = "Please enter Amount in Numeric Format")]
        [Display(Name = "Amount")]
        [RegularExpression(@"^(?!00000)[0-9]{3,8}$", ErrorMessage = "Please enter valid value in numbers.")]
        public decimal Amount { get; set; }

        [Required(ErrorMessage = "Please Select Depositing Bank")]
        [Display(Name = "Select Bank")]
        public long BankId { get; set; }

        [Required(ErrorMessage = "Please Enter Branch Code")]
        [Display(Name = "Enter Branch Code")]
        public string BankBranchCode { get; set; }

        [Required(ErrorMessage = "Please enter Bank TransactionId")]
        [StringLength(100, ErrorMessage = "Please enter Bank TransactionId", MinimumLength = 3)]
        [Display(Name = "Bank TransactionId")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankTransactionId { get; set; }

        [Required(ErrorMessage = "Please enter Cheque No")]
        [StringLength(100, ErrorMessage = "Please enter Cheque No", MinimumLength = 3)]
        [Display(Name = "Enter Cheque No")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string ChequeNo { get; set; }

        [Required(ErrorMessage = "Please upload Payment Slip")]
        [Display(Name = "Upload Payment Slip")]
        public string PaymentSlip { get; set; }

        [Display(Name = "Enter Remark")]
        public string Remark { get; set; }
        public bool IsPaymentReceived { get; set; }
        public bool IsPaymentCredited2Wallet { get; set; }
        public bool IsPaymentMade2Company { get; set; }
        public string PaymentMode { get; set; }
        public Nullable<System.DateTime> PaymentReceivedDateTime { get; set; }
        public Nullable<System.DateTime> PaymentCredited2WalletDateTime { get; set; }
        public string ParentAgencyName { get; set; }
        public long EntityUserId { get; set; }
        public long ParentEntityUserId { get; set; } = 0;
        public string PersonName { get; set; }
        public string ParentPersonName { get; set; }

        public string CompanyName { get; set; }

        public string BankAccountTitle { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string RejectionRemark { get; set; }
        public Nullable<System.DateTime> RejectionDateTime { get; set; }
        public string AccountingEntryStatus { get; set; }
        public string WalletUpdateStatus { get; set; }

        public Nullable<bool> FromMobileDevice { get; set; } = false;

        public string PaymentTransactionId { get; set; }
        public string ClientCode { get; set; }
        public string PaymentRequestURL { get; set; }
        public string PaymentRequestMessage { get; set; }
        public string PaymentRequestString { get; set; }
        public string PaymentResponseOrderId { get; set; }
        public string PaymentResponseString { get; set; }
        public string IPAddress { get; set; }
    }
}