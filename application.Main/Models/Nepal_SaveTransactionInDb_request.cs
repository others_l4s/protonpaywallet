﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_SaveTransactionInDb_request
    {
        public string token { get; set; }
        public long NepalTransactionId { get; set; }
        public string RemitterId { get; set; }
        public string Mode { get; set; }
        public string BeneficiaryId { get; set; }
        public string ipay_id { get; set; }
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string StatusCode { get; set; }
        public string Transfer_Amount { get; set; }
        public string ExchangeRate { get; set; }
        public string PayoutAmount { get; set; }
        public string ServiceCharge { get; set; }
        public string TransactionAmount { get; set; }
        public string ChargedAmount { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;
        public Nullable<bool> Status { get; set; }
    }
}