﻿using System;
using System.ComponentModel.DataAnnotations;

namespace application.Main.Models
{
    public class commonProperties
    {
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = DateTime.Now;
        public Nullable<Int64> CreatedBy { get; set; } = 0;

        [Required(ErrorMessage = "Please select Status")]
        [Display(Name = "Status")]
        public Nullable<int> StatusId { get; set; } = 0;

        [Display(Name = "Upload Company Logo")]
        public string CompanyLogo { get; set; }

        [Display(Name = "Select Status")]
        public bool Status { get; set; }
    }
}