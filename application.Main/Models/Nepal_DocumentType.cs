﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_DocumentType
    {
        public int ID{ get; set; }
        public string Name { get; set; }
    }
}