﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace application.Main.Models
{
    public interface IentityType
    {
        int totalRecords { get; set; }
        int filterRecords { get; set; }
    }
}
