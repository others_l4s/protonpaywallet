﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_searchtxn_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        public string token { get; set; }
        [Display(Name ="Transaction Id")]
        [Required(ErrorMessage ="Please Enter Transaction Id")]
        [StringLength(20,ErrorMessage =("Transaction Id must be 20 digit only"), MinimumLength = 20)]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string clientRefId { get; set; }
        public string customerId { get; set; }
    }
}