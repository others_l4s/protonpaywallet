﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace application.Main.Models
{
    public class tblProduct_Master : commonProperties
    {
        public int ProductId { get; set; } = 0;
        [Required(ErrorMessage = "Please enter Product Code")]
        [StringLength(20, ErrorMessage = "Please enter Product Code", MinimumLength = 5)]
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "Please enter Agency Name")]
        [StringLength(100, ErrorMessage = "Please enter Agency Name", MinimumLength = 3)]
        [Display(Name = "Agency Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string AgencyName { get; set; }

        [Required(ErrorMessage = "Please enter Contact Person Name 1")]
        [StringLength(100, ErrorMessage = "Please enter valid Person Name", MinimumLength = 3)]
        [Display(Name = "Contact Person Name 1")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string ContactPersonName1 { get; set; }

        [Display(Name = "Contact Person Name 2")]
        [StringLength(100, ErrorMessage = "Please enter valid Person Name 2", MinimumLength = 3)]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string ContactPersonName2 { get; set; }

        [Required(ErrorMessage = "Please enter Email Id 1")]
        [StringLength(100, ErrorMessage = "Please enter valid Email Id 1", MinimumLength = 6)]
        [Display(Name = "Email Id 1")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid Email Id format.")]
        public string EmailId1 { get; set; }

        [StringLength(100, ErrorMessage = "Please enter valid Email Id 2", MinimumLength = 6)]
        [Display(Name = "Email ID 2")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid Email Id format.")]
        public string EmailId2 { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "Please enter Telephone 1")]
        [StringLength(15, ErrorMessage = "Please enter valid Telephone No. 1", MinimumLength = 10)]
        [Display(Name = "Telephone No. 1")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Telephone1 { get; set; }

        [StringLength(15, ErrorMessage = "Please enter valid Telephone No. 2", MinimumLength = 10)]
        [Display(Name = "Telephone No. 2")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Telephone2 { get; set; }

        [StringLength(100)]
        [Display(Name = "Website")]
        [RegularExpression(Helper.regWebsiteAddress, ErrorMessage = "Invalid Website format.")]
        public string Website { get; set; }

        [StringLength(15)]
        [Display(Name = "GST No")]
        [RegularExpression(Helper.regGSTNo, ErrorMessage = "Invalid GST No. format.")]
        public string GSTNumber { get; set; }

        [StringLength(15)]
        [Display(Name = "PAN No.")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Invalid PAN No. format.")]
        public string PanCardNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Account Title")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string BankAccountTitle { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string BankAccountNumber { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string BankName { get; set; }
        public string StatusName { get; set; }

        [Display(Name = "Select Commissin On")]
        public string CommissionOn { get; set; }

        [Display(Name = "Markup Type (0=Per(%), 1 = Fixed Amount)")]
        [RegularExpression(Helper.regZeroOrOne, ErrorMessage = "Enter 0 for Per (%) or 1 for Fixed Amount")]
        public Nullable<int> MarkUpType { get; set; } = 0;

        [Display(Name = "Markup Value")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> MarkUpValue { get; set; } = 0;

        [Display(Name = "Service on Live Mode")]
        public bool IsServiceLIVE { get; set; }

        [Display(Name = "Please enter Credentials JSON File")]
        [Required(ErrorMessage = "Please enter Credentials JSON File")]
        public string CredentialFileName { get; set; }

        [Display(Name = "GST Rate on Commission (if applicable)")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal GSTRateOnCommission { get; set; } = 0;

        [Display(Name = "TDS Rate on Commission (if applicable)")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public decimal TDSRateOnCommission { get; set; } = 0;

        [Display(Name = "Apply MarkUp")]
        public bool MarkUpApplicable { get; set; } = false;

        [Display(Name = "GST Rate on MarkUp")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> GSTRateOnMarkUp { get; set; } = 0;

        [Display(Name = "TDS Rate on MarkUp")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> TDSRateOnMarkUp { get; set; } = 0;

        [Display(Name = "Apply Service Charge")]
        public bool ServiceChargeApplicable { get; set; } = false;

        [Display(Name = "Select Service Charge Type")]
        public Nullable<int> ServiceChargeType { get; set; } = 0;

        [Display(Name = "Enter Service Charge Value")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> ServiceChargeValue { get; set; } = 0;

        [Display(Name = "GST On Service Charge")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> GSTOnServiceCharge { get; set; } = 0;

        [Display(Name = "TDS On Service Charge")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> TDSOnServiceCharge { get; set; } = 0;

        [Display(Name = "Apply Commission")]
        public bool CommissionApplicable { get; set; }

        [Display(Name = "Other Fix Charges if Applicable")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Only Numbers with decimal allowed.")]
        public Nullable<decimal> OtherChargesByVendor { get; set; }

        [Display(Name = "Default Commission Applicable")]
        public bool DefaultCommissionApplicable { get; set; } = false;

        [Display(Name = "Custom Commission Applicable")]
        public bool CustomCommissionApplicable { get; set; } = false;

        [Display(Name = "Slab Wise Commission Applicable")]
        public bool SlabCommissionApplicable { get; set; }

        [Display(Name = "Partial Stop Service")]
        public bool PartialStop { get; set; }

        [Display(Name = "Full Stop Service")]
        public bool FullStop { get; set; }
    }
}