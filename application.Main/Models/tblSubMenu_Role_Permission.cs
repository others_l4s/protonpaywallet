﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblSubMenu_Role_Permission
    {
        public int MainMenuId { get; set; }
        public string MainMenuName { get; set; }
        public string ExternalURL { get; set; }
        public string ExternalURLTarget { get; set; }
        public int StatusId { get; set; }
        public int DisplayOrder { get; set; }
        public string IconCSS { get; set; }
        public int RoleId { get; set; }
        public bool CanView { get; set; }
        public bool CanEdit { get; set; }
        public bool CanAdd { get; set; }
        public bool CanDelete { get; set; }

        public int SubMenuId { get; set; }
        public string SubMenuName { get; set; }

        public int CompanyId { get; set; }
    }
}