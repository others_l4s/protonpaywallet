﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblEntity_Accounts
    {
        public long AccountId { get; set; }
        public long EntityId { get; set; }
        public decimal Amount { get; set; }
        public decimal RunningBalance { get; set; }
        public string ClientTransactionId { get; set; }
        public string AccountTransactionId { get; set; }
        public string Description { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionType { get; set; }
        public bool IsCredit { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string ProductCode { get; set; }
        public Nullable<bool> IsCommission { get; set; }
        public Nullable<bool> IsWalletTopUp { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public Nullable<int> CompanyId { get; set; }
        public Nullable<int> EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public string AgencyName { get; set; }
        public Nullable<long> EntityUserId { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }

        public Nullable<decimal> BookingAmount { get; set; }
    }
}