﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class tblAdminLogin : commonProperties
    {
        [Required(ErrorMessage = "Please enter Login Name")]
        [StringLength(100, ErrorMessage = "Please enter Login Name", MinimumLength = 4)]
        [Display(Name = "Enter Login Name")]
        [RegularExpression(Helper.regLoginNameOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Please enter Password")]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Please enter Password between 6 to 10 characters")]
        [Display(Name = "Enter Password")]
        [RegularExpression(Helper.regPassword, ErrorMessage = "Only passwords like abc123, 123XYZ, or AB*&^# would still be accepted.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please select Department")]
        [Display(Name = "Select Department")]
        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }

        [Required(ErrorMessage = "Please select Designation")]
        [Display(Name = "Select Designation")]
        public int DesignationId { get; set; }
        public string DesignationName { get; set; }

        [Required(ErrorMessage = "Please enter Person Name")]
        [StringLength(100, ErrorMessage = "Please enter valid Person Name", MinimumLength = 3)]
        [Display(Name = "Enter Person Name")]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Only Alphabets allowed.")]
        public string PersonName { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No.")]
        [StringLength(10, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "Please enter EmailId")]
        [StringLength(100, ErrorMessage = "Please enter valid Email Id", MinimumLength = 6)]
        [Display(Name = "Enter EmailId")]
        [RegularExpression(Helper.regEmailId, ErrorMessage = "Invalid EmailId format.")]
        public string EmailId { get; set; }

        [StringLength(500, ErrorMessage = "Please enter Address 1")]
        [Display(Name = "Enter Address 1")]
        public string Address { get; set; }

        [Display(Name = "Select State")]
        public Nullable<int> StateId { get; set; }
        public string StateName { get; set; }

        [Display(Name = "Select Country")]
        public Nullable<int> CountryId { get; set; }
        public string CountryName { get; set; }

        [Display(Name = "Select City")]
        public Nullable<int> CityId { get; set; }
        public string CityName { get; set; }

        [Display(Name = "Select Pin Code")]
        public string PinCode { get; set; }

        [Required(ErrorMessage = "Select Role")]
        [Display(Name = "Select Role")]

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        [Display(Name = "Status")]
        public string StatusName { get; set; }
        public int AdminLoginId { get; set; }
        public string loginErrorMessage { get; set; }

        [Required(ErrorMessage = "Please select Company")]
        [Display(Name = "Select Company")]
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public bool IsLeadUser { get; set; }
    }
}