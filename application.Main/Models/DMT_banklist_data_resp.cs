﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_banklist_data_resp
    {

        public List<DMT_banklist_resp> bankList { get; set; }
    }
}