﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_transfer_req
    {
        public string remittermobile { get; set; }
        public string remitterid { get; set; }
        [Required(ErrorMessage = "Please Select Beneficiary")]
        [Display(Name ="Beneficiary")]
        public string beneficiaryid { get; set; }
        
        public string agentid { get; set; }

        [Display(Name ="Transfer Amount")]
        [Required(ErrorMessage = "Please Enter Transfer Amount")]
        public string amount { get; set; }
        public string mode { get; set; }
    }
}