﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class HeaderNav
    {
        public string companyLogo { get; set; }
        public string companyDomain { get; set; }
        public decimal walletBalance { get; set; } = 0;
        public string GSTNumber { get; set; }
        public string userName { get; set; }
        public Boolean isCompanyProfile { get; set; }

        public Boolean lowBalanceAlert { get; set; }

        public Boolean isLeadUser { get; set; }
    }
}