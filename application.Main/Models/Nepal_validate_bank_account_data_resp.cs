﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_validate_bank_account_data_resp
    {
        public string accountnumber { get; set; }
        public string account_holder_name { get; set; }
    }
}