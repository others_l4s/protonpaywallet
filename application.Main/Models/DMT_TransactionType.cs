﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_TransactionType
    {
        public string TransType { get;set;}
        public string TypeName { get; set; }
    }
}