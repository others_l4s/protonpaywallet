﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class Nepal_beneficiary_register_otp_resend_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public string data { get; set; }
    }
}