﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class paginationProperties
    {
        public int totalRecords { get; set; }
        public int filterRecords { get; set; }
    }
}