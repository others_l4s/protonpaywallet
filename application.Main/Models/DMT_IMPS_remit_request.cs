﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.Models
{
    public class DMT_IMPS_remit_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        public string token { get; set; }
        public string agentCode { get; set; }
        [Display(Name = "Recipient")]
        [Required(ErrorMessage = "Please Select Recipient")]
        public string recipientId { get; set; }
        public string customerId { get; set; }
        [Required(ErrorMessage ="Please Enter Amount")]        
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Enter only Decimal Value")]
        [Display(Name = "Amount")]       
        public string amount { get; set; }
        public string clientRefId { get; set; }
        [Display(Name ="Transfer Type")]

        [Required(ErrorMessage = "Please Select Transfer Type")]
        public string TransType { get; set; }
    }
}