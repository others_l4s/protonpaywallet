﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Request
{
    public class MoneyTransferRequest
    {
        public string agentCode { get; set; } = "1";

        public string recipientId { get; set; }

        public string recipientName { get; set; }

        public string bankName { get; set; }
        public string customerId { get; set; }

        [Required(ErrorMessage = "Please Enter Amount")]
        [RegularExpression(Helper.regDecimalOnly, ErrorMessage = "Enter only Decimal Value")]
        [Display(Name = "Amount")]
        public string amount { get; set; }
        public string clientRefId { get; set; }

        [Required(ErrorMessage = "Please Select Transfer Via")]
        public string TransType { get; set; }

        public string TypeName { get; set; }

        public string ParentClientRefId { get; set; } = "";

        public string beneficiaryId { get; set; }

        public Boolean fromMobileDevice { get; set; }
    }
}