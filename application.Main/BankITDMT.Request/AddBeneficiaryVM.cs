﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.Main.Models;
using application.Main.BankITDMT.Response;

namespace application.Main.BankITDMT.Request
{
    public class AddBeneficiaryVM
    {
        public DMT_recipient_fetch_data_resp objBeneficiary { get; set; }

        public List<BankList> objBankList { get; set; }

        public List<DMT_TransactionType> objTransferType { get; set; }
    }
}