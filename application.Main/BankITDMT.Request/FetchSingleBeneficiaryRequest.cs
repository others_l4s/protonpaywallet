﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.BankITDMT.Request
{
    public class FetchSingleBeneficiaryRequest
    {
        public string agentCode { get; set; } = "1";
        public string customerId { get; set; }
        public string recipientId { get; set; }
    }
}