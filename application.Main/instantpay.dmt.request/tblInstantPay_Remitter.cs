﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class tblInstantPay_Remitter
    {
        public long InstantPayRemitterId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MobileNo { get; set; }
        public string Pincode { get; set; }
        public string RemitterId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public int IsVerified { get; set; }
        public string ipay_uuid { get; set; }

        [Required(ErrorMessage = "Please enter OTP")]
        [Display(Name = "Enter Mobile OTP")]
        public string OTP { get; set; }
        public long EntityUserId { get; set; }
        public long EntityId { get; set; }
    }
}