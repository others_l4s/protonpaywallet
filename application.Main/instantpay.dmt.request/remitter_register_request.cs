﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class remitter_register_request
    {
        public string token { get; set; }
        public remitter_req request { get; set; }
    }
}