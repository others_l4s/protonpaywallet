﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class tblInstantPay_Beneficiary
    {
        public long instantPayBeneficiaryId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string ifsc { get; set; }
        public string AccountNo { get; set; }
        public string RemitterId { get; set; }
        public string BeneficiaryId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }

        [Required(ErrorMessage = "Please enter OTP")]
        [Display(Name = "Enter Mobile OTP")]
        public string OTP { get; set; }
        public System.DateTime OTPVerifiedDateTime { get; set; }
        public long EntityId { get; set; }
        public long EntityUserId { get; set; }
        public bool IsOTPVerified { get; set; }

        public string customerId { get; set; }
    }
}