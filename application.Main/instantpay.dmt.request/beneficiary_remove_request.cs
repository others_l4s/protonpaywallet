﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class beneficiary_remove_request
    {
        public string token { get; set; }
        public beneficiary_remove_req request { get; set; }
    }
}