﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class benefical_register_req
    {
        public string remitterid { get; set; }
        public string beneficiaryid { get; set; }

        [Required(ErrorMessage = "Please enter Beneficiary Name")]
        [StringLength(100, ErrorMessage = "Please enter Beneficiary Name", MinimumLength = 3)]
        [Display(Name = "Beneficary Name as per Bank Register")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please enter Mobile No.")]
        [StringLength(15, ErrorMessage = "Please enter valid Mobile No.", MinimumLength = 10)]
        [Display(Name = "Enter Mobile No.")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        public string mobile { get; set; }

        [StringLength(20)]
        [Display(Name = "Bank IFSC Code")]
        [Required(ErrorMessage = "Please Enter IFSC Code")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string ifsc { get; set; }

        [StringLength(50)]
        [Display(Name = "Bank Account Number")]
        [RegularExpression(Helper.regNumericOnly, ErrorMessage = "Only Numbers allowed.")]
        [Required(ErrorMessage = "Please Enter Account Number")]
        public string account { get; set; }
        public int outletid { get; set; }

        public string customerId { get; set; }

        [Required(ErrorMessage = "Please select Transfer Type (NEFT or IMPS)")]
        public string transferType { get; set; }

        public int IMPSAdditionCharge { get; set; } = 0;

        [StringLength(50)]
        [Required(ErrorMessage = "Please select Bank")]
        [Display(Name = "Bank Name")]
        [RegularExpression(Helper.regAlphaNumericOnly, ErrorMessage = "Only Alphabets and Numbers allowed.")]
        public string bankCode { get; set; }

        public string bank { get; set; }

        public string imps { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}