﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class transfer_req
    {
        public string remittermobile { get; set; }
        public string beneficiaryid { get; set; }
        public string agentid { get; set; }
        public string amount { get; set; }
        public string mode { get; set; }
    }
}