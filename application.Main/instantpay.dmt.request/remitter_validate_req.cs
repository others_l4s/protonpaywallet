﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class remitter_validate_req
    {
        public string remitterid { get; set; }
        public string mobile { get; set; }
        public string otp { get; set; }
        public int outletid { get; set; }
    }
}