﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class beneficiary_remove_req
    {
        public string beneficiaryid { get; set; }
        public string remitterid { get; set; }
        public int outletid { get; set; } = 1;
    }
}