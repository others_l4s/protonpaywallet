﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class payoutDirect_Request
    {
        public string Token { get; set; }

        public Request Request { get; set; }
    }

    public partial class Request
    {
        public string sp_key { get; set; }

        public string external_ref { get; set; }

        public string credit_account { get; set; }

        public string ifs_code { get; set; }

        public string bene_name { get; set; }

        public string credit_amount { get; set; }

        public string latitude { get; set; }

        public string longitude { get; set; }

        public string endpoint_ip { get; set; }

        public string alert_mobile { get; set; }

        public string alert_email { get; set; }

        public string remarks { get; set; }

        public string beneficiaryId { get; set; }
        public Boolean fromMobileDevice { get; set; }

        public string remitterMobileNo { get; set; }
        public string TransType { get; set; }
    }
}