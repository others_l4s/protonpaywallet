﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.Main.instantpay.dmt.request
{
    public class remitter_req
    {
        public string mobile { get; set; }

        [Required(ErrorMessage = "Please enter First Name")]
        [StringLength(100, ErrorMessage = "Please enter First Name Minimum 4 Character", MinimumLength = 4)]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Invalid format")]
        [Display(Name = "Enter First Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please enter Second Name")]
        [StringLength(100, ErrorMessage = "Please enter Second Name Minimum 4 Character", MinimumLength = 4)]
        [RegularExpression(Helper.regAlphabetOnly, ErrorMessage = "Invalid format")]
        [Display(Name = "Enter Second Name")]
        public string surname { get; set; }

        [Display(Name = "Enter Pincode")]
        [Required(ErrorMessage = "Please enter Pincode")]
        public string pincode { get; set; }
        public int outletid { get; set; } = 1;
    }
}