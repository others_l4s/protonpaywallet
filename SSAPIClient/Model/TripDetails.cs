﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSAPICore.Model
{
    public class Seat
    {
        //public string available { get; set; }
        private string available;
        public string Available
        {
            get { return available; }
            set { available = value; }
        }
        //public string column { get; set; }
        private string column;
        public string Column
        {
            get { return column; }
            set { column = value; }
        }
        //public string fare { get; set; }
        private string fare;
        public string Fare
        {
            get { return fare; }
            set { fare = value; }
        }
        //public string ladiesSeat { get; set; }
        private string ladiesSeat;
        public string LadiesSeat
        {
            get { return ladiesSeat; }
            set { ladiesSeat = value; }
        }
        //public string length { get; set; }
        private string length;
        public string Length
        {
            get { return length; }
            set { length = value; }
        }
        //public string name { get; set; }
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        //public string row { get; set; }
        private string row;
        public string Row
        {
            get { return row; }
            set { row = value; }
        }
        //public string width { get; set; }
        private string width;
        public string Width
        {
            get { return width; }
            set { width = value; }
        }
        //public string zIndex { get; set; }
        private string zIndex;
        public string ZIndex
        {
            get { return zIndex; }
            set { zIndex = value; }
        }
    }

    public class TripDetails
    {
        //public List<Seat> seats { get; set; }
        //private List<Seat> seats;
        private Seat[] seats;
        public Seat[] Seats
        {
            get { return seats; }
            set { seats = value; }
        }
    }
}
