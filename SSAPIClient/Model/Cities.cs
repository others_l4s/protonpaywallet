﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace SSAPICore.Model
{
    public class City
    {
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

    }
    public class CityList
    {
        private List<City> cities;
        public List<City> Cities
        {
            get { return cities; }
            set { cities = value; }
        }
        //JsonSerializer scities = new JsonSerializer(); 
    }

}

