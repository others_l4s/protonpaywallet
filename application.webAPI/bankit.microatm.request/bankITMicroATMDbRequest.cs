﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.microatm.request
{
    public class bankITMicroATMDbRequest
    {
        public long EntityId { get; set; }
        public string partnerAgentId { get; set; }
        public System.DateTime RequestDateTime { get; set; }
        public Nullable<System.DateTime> ApprovedDateTime { get; set; }
        public string Remark { get; set; }
        public int AccessAmount { get; set; }

        public string mobileNo { get; set; }
        public string agent_id { get; set; }
        public string developer_id { get; set; }
        public string password { get; set; }
    }
}