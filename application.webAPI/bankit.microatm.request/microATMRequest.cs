﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.microatm.request
{
    public class microATMRequest
    {
        public string partnerAgentId { get; set; }
        public string Operator { get; set; }
        public string ClientRefID { get; set; }
        public string txnStatus { get; set; }
        public string RRN { get; set; }
        public string amount { get; set; }
        public string errorCode { get; set; }
        public string service { get; set; }
    }
}