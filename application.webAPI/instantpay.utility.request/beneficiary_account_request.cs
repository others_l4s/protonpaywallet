﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.utility.request
{
    public class beneficiary_account_request
    {
        public string remittermobile { get; set; }
        public string account { get; set; }
        public string agentid { get; set; }
        public string ifsc { get; set; }
    }
}