﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.utility.request
{
    public class account_verification_request
    {
        public string token { get; set; }
        public beneficiary_account_request request { get; set; }
    }
}