﻿using application.webAPI.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.prime.sdk
{
    public class verifyTransactionWebhook
    {
        public string request_id { get; set; }
        public string session_id { get; set; }
        public string app_id { get; set; }
        public string outlet_pan { get; set; }
        public List<verifyTransactions> transactions { get; set; }
        public string hash { get; set; }

    }

    public class verifyTransactions
    {
        public string sp_key { get; set; }
        public double amount { get; set; }
        public string[] customer_params { get; set; }
    }

    public class verifyTransactionResponse
    {
        public string response_code { get; set; }
        public string response_msg { get; set; }
        public List<transactionsResponseAgentId> transactions { get; set; }
    }

    public class transactionsResponseAgentId
    {
        public string agent_id { get; set; }
    }
}