﻿using application.webAPI.viewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace application.webAPI.prime.sdk
{
    public class registrationOTP
    {
        public string token { get; set; }
        public registrationOTPRequest request { get; set; }
    }

    public class registrationOTPRequest
    {
        public string mobile { get; set; }
    }

    public class registrationOTPResponse
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public registrationOTPResponseData data { get; set; }
        public string ipay_uuid { get; set; }
    }

    public class registrationOTPResponseData
    {
        public string mobile { get; set; }
    }

    public class registerOutlet
    {
        public string token { get; set; }
        public registerOutletRequest request { get; set; }
    }

    public class registerOutletRequest
    {
        public string mobile { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public string name { get; set; }
        public string pan { get; set; }
        public string pincode { get; set; }
        public string address { get; set; }
        public string otp { get; set; }
    }

    public class registerOutletResponse
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public registerOutletResponseData data { get; set; }
    }

    public class registerOutletResponseData
    {
        public string outlet_id { get; set; }
        public string mobile_number { get; set; }
        public string email_id { get; set; }
        public string outlet_name { get; set; }
        public string contact_person { get; set; }
        public string pan_no { get; set; }
        public string kyc_status { get; set; }
        public string outlet_status { get; set; }
    }

    public class requiredDocs
    {
        public string token { get; set; }
        public requiredDocsRequest request { get; set; }
    }

    public class requiredDocsRequest
    {
        public string outletid { get; set; }
        public string pan_no { get; set; }
    }

    public class requiredDocsResponse
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public requiredDocsResponseData data { get; set; }
    }

    public class requiredDocsResponseData
    {
        public string[][] APPROVED { get; set; }
        public string[][] SCREENING { get; set; }
        public string[][] REQUIRED { get; set; }
    }

    public class uploadOutletDocument
    {
        public string token { get; set; }
        public uploadOutletDocumentRequest request { get; set; }
    }

    public class uploadOutletDocumentRequest
    {
        public string outletid { get; set; }
        public string pan_no { get; set; }
        public uploadOutletDocuments document { get; set; }
    }

    public class uploadOutletDocuments
    {
        public string id { get; set; }
        public string link { get; set; }
        public string filename { get; set; }
    }

    public class outletKYCStatus
    {
        public string StatusCode { get; set; }
        public bool PanCardRequired { get; set; }
        public bool AadhaarCardRequired { get; set; }
    }

    public class primeURLs
    {
        public string primeCompleteURL { get; set; }
        public string primePostData { get; set; }
    }
}