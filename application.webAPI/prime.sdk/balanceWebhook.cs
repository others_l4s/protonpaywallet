﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.prime.sdk
{
    public class balanceWebhook
    {
        public string request_id { get; set; }
        public string session_id { get; set; }

        public string app_id { get; set; }

        public string outlet_pan { get; set; }

        public transactions transactions { get; set; }

        public string hash { get; set; }

    }

    public class transactions
    {
        public string outlet_pan { get; set; }
    }

    public class balanceWebhookResponse
    {
        public string response_code { get; set; }
        public string response_msg { get; set; }
        public List<transactionsResponse> transactions { get; set; }
    }

    public class transactionsResponse
    {
        public double balance { get; set; }
    }
}