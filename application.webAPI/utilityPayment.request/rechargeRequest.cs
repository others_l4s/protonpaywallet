﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.utilityPayment.request
{
    public class rechargeRequest
    {
        public int OperatorVendorId { get; set; }
        public string MobileNo { get; set; }
        public int Amount { get; set; }
        public string DTHNumber { get; set; }
        public string customermobile { get; set; }

        public string IPAddress { get; set; }
    }
}