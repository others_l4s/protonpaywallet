﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.microatm.response
{
    public class microATMResponse
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}