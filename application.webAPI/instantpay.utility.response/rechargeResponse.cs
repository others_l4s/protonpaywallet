﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.utility.response
{
    public class rechargeResponse
    {
        public string ipay_id { get; set; }
        public string agent_id { get; set; }
        public string opr_id { get; set; }
        public string account_no { get; set; }
        public string sp_key { get; set; }
        public int trans_amt { get; set; }
        public double charged_amt { get; set; }
        public string opening_bal { get; set; }
        public string datetime { get; set; }
        public string status { get; set; }
        public string res_code { get; set; }
        public string res_msg { get; set; }
    }
}