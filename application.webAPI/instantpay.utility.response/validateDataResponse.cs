﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.utility.response
{
    public class validateDataResponse
    {
        public string ipay_errorcode { get; set; }
        public string ipay_errordesc { get; set; }
    }
}