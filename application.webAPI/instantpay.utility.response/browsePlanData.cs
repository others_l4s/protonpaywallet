﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.utility.response
{
    public class browsePlanData
    {
        public string id { get; set; }
        public string sp_key { get; set; }
        public string sp_circle { get; set; }
        public string recharge_value { get; set; }
        public string recharge_talktime { get; set; }
        public string recharge_validity { get; set; }
        public string recharge_short_description { get; set; }
        public string recharge_description { get; set; }
        public string last_updated_dt { get; set; }
    }
}