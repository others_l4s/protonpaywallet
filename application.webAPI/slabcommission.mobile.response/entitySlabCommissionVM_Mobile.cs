﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.slabcommission.mobile.response
{
    public class entitySlabCommissionVM_Mobile
    {
        public List<tblEntitySlabCommission_Mobile> objEntitySlabcommissionList { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblEntity_Master> objEntityList { get; set; }
    }
}