﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.slabcommission.mobile.response
{
    public class tblEntitySlabCommission_Mobile
    {
        public long SlabCommissionId { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string CommissionTypeName { get; set; }
        public int CommissionTypeId { get; set; }
        public decimal FromAmount { get; set; }
        public decimal ToAmount { get; set; }
        public decimal CommissionValue { get; set; }
        public decimal DefaultCommissionValue { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public long EntityId { get; set; }
        public string AgencyName { get; set; }
        public bool IsCustom { get; set; }
        public int EntityTypeId { get; set; }
        public string EntityTypeName { get; set; }
        public bool SlabCommissionApplicable { get; set; }
        public bool CustomCommissionApplicable { get; set; }
        public long ParentSlabCommissionId { get; set; }
    }
}