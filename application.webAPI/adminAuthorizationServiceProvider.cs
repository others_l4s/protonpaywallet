﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Owin.Security.OAuth;
using application.webAPI.viewModel;
using application.webAPI.Models;

namespace application.webAPI
{
    public class adminAuthorizationServiceProvider : OAuthAuthorizationServerProvider
    {
        private applicationDbEntities db = new applicationDbEntities();

        //Use for validate client authentication (Override)
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // have validated the client
        }

        //Validate user credentials
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            adminLoginViewModel objAdminLoginViewModel = new adminLoginViewModel();
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            var validateInDb = db.QryAdminLogins.Where(c => c.LoginName.Trim().ToLower() == context.UserName.Trim().ToLower() || c.EmailId.Trim().ToLower() == context.UserName.Trim().ToLower()).SingleOrDefault();
            if (validateInDb != null)
            {
                string unProtectedPassword = helper.unProtectString(validateInDb.Password);
                if (unProtectedPassword.Trim() == context.Password.Trim())
                {
                    if (validateInDb.StatusName.Trim().ToLower() == "active")
                    {
                        identity.AddClaim(new Claim(ClaimTypes.Role, validateInDb.RoleId.ToString()));
                        identity.AddClaim(new Claim("adminLoginId", validateInDb.AdminLoginId.ToString()));
                        identity.AddClaim(new Claim("entityType", helper.entityTypeCompany));
                        identity.AddClaim(new Claim("companyId", validateInDb.CompanyId.ToString()));
                        context.Validated(identity);
                        context.Request.Context.Authentication.SignIn();
                    }
                    else
                    {
                        context.SetError("Error", "Your status is " + validateInDb.StatusName + ", kindly contact administrator.");
                        return;
                    }
                }
                else
                {
                    context.SetError("Error", "Invalid Username or Password");
                    return;
                }
            }
            else
            {
                context.SetError("Error", "Invalid Username or Password");
                return;
            }
        }
    }
}