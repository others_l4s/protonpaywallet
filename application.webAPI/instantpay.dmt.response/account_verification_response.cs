﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class account_verification_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public application.webAPI.instantpay.dmt.response.Data data { get; set; }
    }

    public partial class Data
    {
        public string remarks { get; set; }
        public string bankrefno { get; set; }
        public string ipay_id { get; set; }
        public string benename { get; set; }
        public int locked_amt { get; set; }
        public decimal charged_amt { get; set; }
        public string verification_status { get; set; }
    }
}