﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitter_validate_remitter_resp
    {
        public int is_verified { get; set; }
        public string id { get; set; }
    }
}