﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_register_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public beneficiary_register_data_resp data { get; set; }
    }
}