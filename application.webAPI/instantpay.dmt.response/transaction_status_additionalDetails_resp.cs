﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transaction_status_additionalDetails_resp
    {
        public string status_request_dt { get; set; }
        public string status_request_id { get; set; }
        public string bank { get; set; }
        public string account { get; set; }
        public string ifsc { get; set; }
        public string mode { get; set; }
    }
}