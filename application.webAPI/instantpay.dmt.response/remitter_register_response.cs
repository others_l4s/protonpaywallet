﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitter_register_response
    {
       
        public remitter_register_data_resp data { get; set; }
        public string statuscode { get; set; }
        public string status { get; set; }       
        public string timestamp { get; set; }
        public string ipay_uuid { get; set; }
        public string orderid { get; set; }
        public string environment { get; set; }
    }
}