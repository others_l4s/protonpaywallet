﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_register_data_resp
    {
        public beneficiary_register_remitter_res remitter { get; set; }
        public beneficiary_register_beneficiary_resp beneficiary { get; set; }
    }
}