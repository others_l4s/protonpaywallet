﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class mode_resp
    {
        public int imps { get; set; }
        public int neft { get; set; }
    }
}