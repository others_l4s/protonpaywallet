﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_resp
    {
        public string id { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string account { get; set; }
        public string bank { get; set; }
        public string status { get; set; }
        public string last_success_date { get; set; }
        public string last_success_name { get; set; }
        public string last_success_imps { get; set; }
        public string ifsc { get; set; }
        public string imps { get; set; }
    }
}