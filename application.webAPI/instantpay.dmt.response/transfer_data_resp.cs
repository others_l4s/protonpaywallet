﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transfer_data_resp
    {
        public string ipay_id { get; set; }
        public string ref_no { get; set; }
        public string opr_id { get; set; }
        public string name { get; set; }
        public string opening_bal { get; set; }
        public int amount { get; set; }
        public double charged_amt { get; set; }
        public int locked_amt { get; set; }
        public int ccf_bank { get; set; }
        public string bank_alias { get; set; }
    }
}