﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_remove_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public beneficiary_remove_data_resp data { get; set; }
    }
}