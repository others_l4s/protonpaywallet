﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class bank_details_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public List<bank_details_data_resp> data { get; set; }
    }
}