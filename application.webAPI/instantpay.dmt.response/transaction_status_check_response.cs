﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transaction_status_check_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public transaction_status_data_resp data { get; set; }
    }
}