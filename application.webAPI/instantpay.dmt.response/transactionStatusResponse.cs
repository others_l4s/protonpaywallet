﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transactionStatusResponse
    {
        public string statuscode { get; set; }

        public string status { get; set; }

        public Data data { get; set; }

        public DateTime timestamp { get; set; }

        public string ipay_uuid { get; set; }

        public string orderid { get; set; }

        public string environment { get; set; }
    }

    public partial class Data
    {
        public DateTime transaction_dt { get; set; }

        public string external_id { get; set; }

        public string order_id { get; set; }

        public string serviceprovider_id { get; set; }

        public string product_key { get; set; }

        public string transaction_account { get; set; }

        public string transaction_amount { get; set; }

        public string transaction_status { get; set; }

        public string transaction_description { get; set; }

        public AdditionalDetails additional_details { get; set; }
    }

    public partial class AdditionalDetails
    {
        public DateTime status_request_dt { get; set; }

        public string status_request_id { get; set; }

        public string bank { get; set; }

        public string account { get; set; }

        public string ifsc { get; set; }

        public string mode { get; set; }
    }
}