﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitter_details_data_resp
    {
        public remitter_resp remitter { get; set; }
        public List<beneficiary_resp> beneficiary { get; set; }
        public List<remitterlimit_resp> remitter_limit { get; set; }
    }
}