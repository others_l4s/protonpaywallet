﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitterlimit_resp
    {
        public Nullable<int> code { get; set; }
        public int status { get; set; }
        public mode_resp mode { get; set; }
        public limit_resp limit { get; set; }
    }
}