﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_register_beneficiary_resp
    {
        public int status { get; set; }
        public string id { get; set; }
    }
}