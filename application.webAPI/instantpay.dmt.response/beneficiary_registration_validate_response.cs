﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_registration_validate_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public string data { get; set; }
    }
}