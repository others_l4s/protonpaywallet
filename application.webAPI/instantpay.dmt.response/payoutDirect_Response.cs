﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class payoutDirect_Response
    {
        public string statuscode { get; set; }

        public string status { get; set; }

        public application.webAPI.instantpay.dmt.response.Data data { get; set; }

        public string timestamp { get; set; }

        public string ipay_uuid { get; set; }

        public string orderid { get; set; }

        public string environment { get; set; }
    }

    public partial class Data
    {
        public string Ipay_id { get; set; }

        public string transfer_value { get; set; }

        public string type_pricing { get; set; }

        public string commercial_value { get; set; }

        public string value_tds { get; set; }

        public string ccf { get; set; }

        public string vendor_ccf { get; set; }

        public string Charged_amt { get; set; }

        public application.webAPI.instantpay.dmt.response.Payout payout { get; set; }
    }

    public partial class Payout
    {
        public string credit_refid { get; set; }

        public string account { get; set; }

        public string ifsc { get; set; }

        public string name { get; set; }
    }
}