﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitter_validate_data_resp
    {
        public remitter_validate_remitter_resp remitter { get; set; }
    }
}