﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class limit_resp
    {
        public int total { get; set; }
        public int consumed { get; set; }
        public int remaining { get; set; }
    }
}