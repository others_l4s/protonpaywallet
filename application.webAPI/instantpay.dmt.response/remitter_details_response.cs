﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class remitter_details_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }

        public string ipay_uuid { get; set; }
        public remitter_details_data_resp data { get; set; }
    }
}