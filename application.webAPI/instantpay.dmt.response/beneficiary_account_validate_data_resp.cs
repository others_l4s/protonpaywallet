﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class beneficiary_account_validate_data_resp
    {
        public string remarks { get; set; }
        public long bankrefno { get; set; }
        public string ipay_id { get; set; }
        public string benename { get; set; }
        public int locked_amt { get; set; }
        public double charged_amt { get; set; }
        public string verification_status { get; set; }
    }
}