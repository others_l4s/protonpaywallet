﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class checkWallet
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public availableBalance data { get; set; }
        public string timestamp { get; set; }
        public string ipay_uuid { get; set; }
        public string orderid { get; set; }
        public string environment { get; set; }
    }

    public class availableBalance
    {
        public string main_bal { get; set; }
        public string lien_bal { get; set; }
        public string available { get; set; }
    }
}