﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transfer_response
    {
        public string statuscode { get; set; }
        public string status { get; set; }
        public transfer_data_resp data { get; set; }
    }
}