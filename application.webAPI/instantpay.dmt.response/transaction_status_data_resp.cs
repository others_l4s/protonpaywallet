﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.response
{
    public class transaction_status_data_resp
    {
        public string transaction_dt { get; set; }
        public string external_id { get; set; }
        public string order_id { get; set; }
        public string serviceprovider_id { get; set; }
        public string product_key { get; set; }
        public string transaction_account { get; set; }
        public string transaction_amount { get; set; }
        public string transaction_status { get; set; }
        public string transaction_description { get; set; }
        public transaction_status_additionalDetails_resp additional_details { get; set; }
    }
}