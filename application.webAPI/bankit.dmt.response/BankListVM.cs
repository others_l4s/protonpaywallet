﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class BankListVM
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public BankListData data { get; set; }
    }
}