﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class MoneyTransferResponseVM
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public string Reason { get; set; }
        public MoneyTransferResponse data { get; set; }
    }
}