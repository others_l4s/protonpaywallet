﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class pending_data_resp
    {
        public List<TransactionDetail_pending_resp> transactionDetails { get; set; }
    }
}