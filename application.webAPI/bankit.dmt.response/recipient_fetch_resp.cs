﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class recipient_fetch_resp
    {
        public string udf2 { get; set; }
        public string udf1 { get; set; }
        public string recipientId { get; set; }
        public string recipientName { get; set; }
        public string bankName { get; set; }
        public string channel { get; set; }
        public string bankCode { get; set; }
        public string mobileNo { get; set; }

        public string customerId { get; set; }

        public string transferType { get; set; }

        public int IMPSAdditionCharge { get; set; }

        public Boolean verifyAccount { get; set; }
    }
}