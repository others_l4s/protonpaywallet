﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class TransactionDetail_pending_resp
    {
        public string customerId { get; set; }
        public string clientRefId { get; set; }
        public string transactionDate { get; set; }
        public string amount { get; set; }
    }
}