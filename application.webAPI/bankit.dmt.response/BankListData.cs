﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class BankListData
    {
        public List<BankList> bankList { get; set; }
    }
}