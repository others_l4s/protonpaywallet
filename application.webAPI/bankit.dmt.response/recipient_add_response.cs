﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class recipient_add_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public recipient_add_data_resp data { get; set; }
    }
}