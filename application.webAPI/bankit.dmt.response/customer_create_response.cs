﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class customer_create_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public string token { get; set; }
        public customer_create_data_resp data { get; set; }
    }
}