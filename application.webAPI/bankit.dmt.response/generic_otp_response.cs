﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class generic_otp_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public generic_otp_data_resp data { get; set; }
    }
}