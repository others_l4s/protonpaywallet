﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class MoneyTransferResponse
    {
        public string customerId { get; set; }
        public string clientRefId { get; set; }
        public string txnId { get; set; }
        public string accountNumber { get; set; }
    }
}