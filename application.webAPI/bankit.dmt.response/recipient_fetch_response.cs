﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.bankit.dmt.response
{
    public class recipient_fetch_response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public recipient_fetch_resp data { get; set; }

        public tblBankit_Sender objSender { get; set; }
    }
}