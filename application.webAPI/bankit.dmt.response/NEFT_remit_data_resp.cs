﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class NEFT_remit_data_resp
    {
        public string customerId { get; set; }
        public string name { get; set; }
        public string bankName { get; set; }
        public string clientRefId { get; set; }
        public string impsRespCode { get; set; }
        public string accountNumber { get; set; }
        public string ifscCode { get; set; }
    }
}