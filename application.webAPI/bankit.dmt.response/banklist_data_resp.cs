﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class banklist_data_resp
    {
        public List<banklist_resp> bankList { get; set; }
    }
}