﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class customer_fetch_data_resp
    {
        public string customerId { get; set; }
        public string name { get; set; }
        public int kycstatus { get; set; }
        public double walletbal { get; set; }
        public string dateOfBirth { get; set; }
    }
}