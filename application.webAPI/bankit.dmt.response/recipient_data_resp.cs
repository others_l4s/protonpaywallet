﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{

    public class recipient_data_resp
    {
        public List<recipient_fetch_resp> recipientList { get; set; }
    }

}