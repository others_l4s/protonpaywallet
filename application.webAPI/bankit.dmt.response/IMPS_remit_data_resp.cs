﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class IMPS_remit_data_resp
    {
        public string customerId { get; set; }
        public string clientRefId { get; set; }
        public string txnId { get; set; }
    }
}