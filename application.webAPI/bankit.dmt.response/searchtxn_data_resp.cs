﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.response
{
    public class searchtxn_data_resp
    {
        public string clientRefId { get; set; }
        public string transactionDate { get; set; }
        public string amount { get; set; }

        public string txnId { get; set; }
    }
}