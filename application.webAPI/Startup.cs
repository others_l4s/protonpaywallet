﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;

namespace application.webAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            //Enabling cross origin request which all allow request from anywhere.
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            //Configuration OAuth Authorization Server for Admin
            var adminProvider = new adminAuthorizationServiceProvider();
            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/validateadminlogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = adminProvider
            };

            //Configuration OAuth Authorization Server for User
            var userProvider = new userAuthorizationServiceProvider();
            OAuthAuthorizationServerOptions userOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/validateuserlogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = userProvider
            };

            var userFingerPrintProvider = new userFingerPrintAuthorizationServiceProvider();
            OAuthAuthorizationServerOptions userFPOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/validateuserviafingerprintlogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = userFingerPrintProvider
            };

            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthAuthorizationServer(userOptions);
            app.UseOAuthAuthorizationServer(userFPOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}