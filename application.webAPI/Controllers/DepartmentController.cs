﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class DepartmentController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/Department
        [Authorize]
        [ResponseType(typeof(departmentViewModel))]
        [HttpGet]
        public IHttpActionResult getDepartmentList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    departmentViewModel objDepartmentViewModel = new departmentViewModel()
                    {
                        objDepartment = new tblDepartment_Master(),
                        objStatusList = db.tblStatus_Master.ToList()
                    };
                    return Ok(objDepartmentViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/Department/5
        [Authorize]
        [ResponseType(typeof(tblDepartment_Master))]
        public IHttpActionResult getDepartmentById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDepartment_Master tblDepartment_Master = db.tblDepartment_Master.Find(id);
                    if (tblDepartment_Master == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblDepartment_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(departmentViewModel))]
        [HttpGet]
        public IHttpActionResult getDepartmentListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    departmentViewModel objdepartmentViewModel = new departmentViewModel();
                    objdepartmentViewModel.totalRecords = db.tblDepartment_Master.Where(c => c.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objdepartmentViewModel.objDepartmentList = db.QryDepartment.Where(s => s.CompanyId == CompanyId && (s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) || s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();
                        objdepartmentViewModel.filterRecords = objdepartmentViewModel.objDepartmentList.Count();
                    }
                    else
                        objdepartmentViewModel.filterRecords = objdepartmentViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objdepartmentViewModel.objDepartmentList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDepartment.Where(c => c.CompanyId == CompanyId).OrderBy(s => s.DepartmentName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QryDepartment>() :
                            objdepartmentViewModel.objDepartmentList.OrderBy(s => s.DepartmentName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QryDepartment>();
                    else
                        objdepartmentViewModel.objDepartmentList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDepartment.Where(c => c.CompanyId == CompanyId).OrderByDescending(s => s.DepartmentName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QryDepartment>() :
                            objdepartmentViewModel.objDepartmentList.OrderByDescending(s => s.DepartmentName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QryDepartment>();

                    return Ok(objdepartmentViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/Department/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateDepartment(int id, tblDepartment_Master objdepartment)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objdepartment.DepartmentId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                        var departmentInDb = db.tblDepartment_Master.SingleOrDefault(d => d.DepartmentName.Trim().ToLower() == objdepartment.DepartmentName.Trim().ToLower() &&
                        d.CompanyId == CompanyId &&
                        d.DepartmentId != objdepartment.DepartmentId);

                        if (departmentInDb != null)
                            return BadRequest("Department Name already exists for selected Company");

                        db.Entry(objdepartment).State = EntityState.Modified;
                        db.Entry(objdepartment).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objdepartment).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblDepartment_MasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok(objdepartment);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/Department
        [Authorize]
        [ResponseType(typeof(tblDepartment_Master))]
        [HttpPost]
        public IHttpActionResult saveDepartment(tblDepartment_Master objDepartment)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(identityAdminLoginId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    var DepartmentInDb = db.tblDepartment_Master.SingleOrDefault(d => d.DepartmentName.Trim().ToLower() == objDepartment.DepartmentName.Trim().ToLower() && d.CompanyId == CompanyId && d.DepartmentId != objDepartment.DepartmentId);
                    if (DepartmentInDb != null)
                        return BadRequest("Department Name already exists for selected Company");

                    objDepartment.CompanyId = CompanyId;
                    objDepartment.CreatedBy = adminLoginId;
                    db.tblDepartment_Master.Add(objDepartment);
                    db.SaveChanges();

                    return Ok(objDepartment);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/Department/5
        [Authorize]
        [ResponseType(typeof(tblDepartment_Master))]
        [HttpDelete]
        public IHttpActionResult deleteDepartment(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDepartment_Master tblDepartment_Master = db.tblDepartment_Master.Find(id);
                    if (tblDepartment_Master == null)
                    {
                        return NotFound();
                    }

                    db.tblDepartment_Master.Remove(tblDepartment_Master);
                    db.SaveChanges();

                    return Ok(tblDepartment_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblDepartment_MasterExists(int id)
        {
            return db.tblDepartment_Master.Count(e => e.DepartmentId == id) > 0;
        }
    }
}