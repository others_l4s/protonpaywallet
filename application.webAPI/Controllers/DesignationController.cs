﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class DesignationController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/Designation
        [Authorize]
        [ResponseType(typeof(designationViewModel))]
        [HttpGet]
        public IHttpActionResult getDesignationList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    designationViewModel designationViewModel = new designationViewModel()
                    {
                        objDesignation = new tblDesignation_Master(),
                        objStatusList = db.tblStatus_Master.ToList()
                    };
                    return Ok(designationViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/Designation/5
        [Authorize]
        [ResponseType(typeof(tblDesignation_Master))]
        public IHttpActionResult getDesignationById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDesignation_Master objDesignationMaster = db.tblDesignation_Master.Find(id);
                    if (objDesignationMaster == null)
                    {
                        return NotFound();
                    }
                    return Ok(objDesignationMaster);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(designationViewModel))]
        [HttpGet]
        public IHttpActionResult getDesignationListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    designationViewModel objdesignationViewModel = new designationViewModel();
                    objdesignationViewModel.totalRecords = db.tblDesignation_Master.Where(c => c.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objdesignationViewModel.objDesignationList = db.QryDesignation.Where(d => d.CompanyId == CompanyId && (d.DesignationName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) || d.DesignationName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();
                        objdesignationViewModel.filterRecords = objdesignationViewModel.objDesignationList.Count();
                    }
                    else
                        objdesignationViewModel.filterRecords = objdesignationViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objdesignationViewModel.objDesignationList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDesignation.Where(d => d.CompanyId == CompanyId).OrderBy(d => d.DesignationName).ThenBy(d => d.DesignationName).Skip(pageIndex).Take(pageSize).ToList<QryDesignation>() :
                            objdesignationViewModel.objDesignationList.OrderBy(s => s.DesignationName).ThenBy(s => s.DesignationName).Skip(pageIndex).Take(pageSize).ToList<QryDesignation>();
                    else
                        objdesignationViewModel.objDesignationList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDesignation.Where(d => d.CompanyId == CompanyId).OrderByDescending(d => d.DesignationName).ThenBy(d => d.DesignationName).Skip(pageIndex).Take(pageSize).ToList<QryDesignation>() :
                            objdesignationViewModel.objDesignationList.OrderByDescending(d => d.DesignationName).ThenBy(d => d.DesignationName).Skip(pageIndex).Take(pageSize).ToList<QryDesignation>();

                    return Ok(objdesignationViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/Designation/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateDesignation(int id, tblDesignation_Master objdesignation)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityAdminLoginId.Value);

                    if (id != objdesignation.DesignationId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var designationInDb = db.tblDesignation_Master.SingleOrDefault(d => d.DesignationName.Trim().ToLower() == objdesignation.DesignationName.Trim().ToLower() &&
                        d.CompanyId == CompanyId &&
                        d.DesignationId != objdesignation.DesignationId);

                        if (designationInDb != null)
                            return BadRequest("Department Name already exists for selected Company");

                        db.Entry(objdesignation).State = EntityState.Modified;
                        db.Entry(objdesignation).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objdesignation).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblDesignation_MasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok(objdesignation);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/Designation
        [Authorize]
        [ResponseType(typeof(tblDesignation_Master))]
        [HttpPost]
        public IHttpActionResult saveDesignation(tblDesignation_Master objDesignation)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(identityAdminLoginId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    var DesignationInDb = db.tblDesignation_Master.SingleOrDefault(d => d.DesignationName.Trim().ToLower() == objDesignation.DesignationName.Trim().ToLower() &&
                    d.CompanyId == CompanyId &&
                    d.DesignationId != objDesignation.DesignationId);
                    if (DesignationInDb != null)
                        return BadRequest("Department Name already exists for selected Company");

                    objDesignation.CreatedBy = adminLoginId;
                    objDesignation.CompanyId = CompanyId;
                    db.tblDesignation_Master.Add(objDesignation);
                    db.SaveChanges();

                    return Ok(objDesignation);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/Designation/5
        [Authorize]
        [ResponseType(typeof(tblDesignation_Master))]
        public IHttpActionResult deleteDesignation(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDesignation_Master tblDesignation_Master = db.tblDesignation_Master.Find(id);
                    if (tblDesignation_Master == null)
                    {
                        return NotFound();
                    }

                    db.tblDesignation_Master.Remove(tblDesignation_Master);
                    db.SaveChanges();

                    return Ok(tblDesignation_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblDesignation_MasterExists(int id)
        {
            return db.tblDesignation_Master.Count(e => e.DesignationId == id) > 0;
        }
    }
}