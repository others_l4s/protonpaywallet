﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;

namespace application.webAPI.Controllers
{
    public class bankAccountTypeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/bankAccountType
        public IQueryable<tblBankAccountType> GettblBankAccountTypes()
        {
            return db.tblBankAccountType;
        }

        // GET: api/bankAccountType/5
        [ResponseType(typeof(tblBankAccountType))]
        public IHttpActionResult GettblBankAccountType(int id)
        {
            tblBankAccountType tblBankAccountType = db.tblBankAccountType.Find(id);
            if (tblBankAccountType == null)
            {
                return NotFound();
            }

            return Ok(tblBankAccountType);
        }

        // PUT: api/bankAccountType/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PuttblBankAccountType(int id, tblBankAccountType tblBankAccountType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblBankAccountType.BankAccountTypeId)
            {
                return BadRequest();
            }

            db.Entry(tblBankAccountType).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblBankAccountTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/bankAccountType
        [ResponseType(typeof(tblBankAccountType))]
        public IHttpActionResult PosttblBankAccountType(tblBankAccountType tblBankAccountType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblBankAccountType.Add(tblBankAccountType);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tblBankAccountType.BankAccountTypeId }, tblBankAccountType);
        }

        // DELETE: api/bankAccountType/5
        [ResponseType(typeof(tblBankAccountType))]
        public IHttpActionResult DeletetblBankAccountType(int id)
        {
            tblBankAccountType tblBankAccountType = db.tblBankAccountType.Find(id);
            if (tblBankAccountType == null)
            {
                return NotFound();
            }

            db.tblBankAccountType.Remove(tblBankAccountType);
            db.SaveChanges();

            return Ok(tblBankAccountType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblBankAccountTypeExists(int id)
        {
            return db.tblBankAccountType.Count(e => e.BankAccountTypeId == id) > 0;
        }
    }
}