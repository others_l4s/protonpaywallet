﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class SupportSystemProblemMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/SupportSystemProblemMaster
        [Authorize]
        [ResponseType(typeof(supportSystemProblemViewModel))]
        [HttpGet]
        public IHttpActionResult getSupportSystemProblemList()
        {

            supportSystemProblemViewModel objsupportSystemProblemViewModel = new supportSystemProblemViewModel()
            {
                objSupportSystemProblem = new tblSupportSystem_Problem_Master(),
                objProductList = db.tblProduct_Master.OrderBy(c => c.ProductCode).ToList(),
                objDepartmentList = db.tblDepartment_Master.OrderBy(c => c.DepartmentName).ToList(),
                objStatusMasterList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                objSupportSystemProblemList = new List<QrySupportSystemProblem>()
            };
            return Ok(objsupportSystemProblemViewModel);
        }

        // GET: api/SupportSystemProblemMaster/5
        [Authorize]
        [ResponseType(typeof(tblSupportSystem_Problem_Master))]
        [HttpGet]
        public IHttpActionResult getSupportSystemProblemById(int id)
        {
            tblSupportSystem_Problem_Master tblSupportSystem_Problem_Master = db.tblSupportSystem_Problem_Master.Find(id);
            if (tblSupportSystem_Problem_Master == null)
            {
                return NotFound();
            }

            return Ok(tblSupportSystem_Problem_Master);
        }

        [Authorize]
        [ResponseType(typeof(supportSystemProblemViewModel))]
        [HttpGet]
        public IHttpActionResult getSupportSystemProblemListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            supportSystemProblemViewModel objsupportSystemProblemViewModel = new supportSystemProblemViewModel();
            objsupportSystemProblemViewModel.totalRecords = db.tblSubscription_Master.Count();
            if (!String.IsNullOrEmpty(searchValue))
            {
                objsupportSystemProblemViewModel.objSupportSystemProblemList = db.QrySupportSystemProblem.Where(s => s.ProblemName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                || s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                || s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                objsupportSystemProblemViewModel.filterRecords = objsupportSystemProblemViewModel.objSupportSystemProblemList.Count();
            }
            else
                objsupportSystemProblemViewModel.filterRecords = objsupportSystemProblemViewModel.totalRecords;
            if (sortDirection == "asc")
                objsupportSystemProblemViewModel.objSupportSystemProblemList = String.IsNullOrEmpty(searchValue) ?
                    db.QrySupportSystemProblem.OrderBy(s => s.ProblemName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemProblem>() :
                    objsupportSystemProblemViewModel.objSupportSystemProblemList.OrderBy(s => s.ProblemName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemProblem>();
            else
                objsupportSystemProblemViewModel.objSupportSystemProblemList = String.IsNullOrEmpty(searchValue) ?
                    db.QrySupportSystemProblem.OrderByDescending(s => s.ProblemName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemProblem>() :
                    objsupportSystemProblemViewModel.objSupportSystemProblemList.OrderByDescending(s => s.ProblemName).ThenBy(s => s.DepartmentName).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemProblem>();

            return Ok(objsupportSystemProblemViewModel);
        }

        // PUT: api/SupportSystemProblemMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult UpdateSupportSystemProblemMaster(int id, tblSupportSystem_Problem_Master objSupportSystemProblemMaster)
        {
            
            if (id != objSupportSystemProblemMaster.ProblemId)
            {
                return BadRequest();
            }

            db.Entry(objSupportSystemProblemMaster).State = EntityState.Modified;

            try
            {
                var SupportSystemProblemInDb = db.tblSupportSystem_Problem_Master.SingleOrDefault(s => s.ProblemName.Trim().ToLower() == objSupportSystemProblemMaster.ProblemName && s.DepartmentId == objSupportSystemProblemMaster.DepartmentId && s.ProductId== objSupportSystemProblemMaster.ProductId && s.ProblemId!=objSupportSystemProblemMaster.ProblemId);
                if (SupportSystemProblemInDb != null)
                    return BadRequest("Problem already exists");
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblSupportSystem_Problem_MasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // POST: api/SupportSystemProblemMaster
        [Authorize]
        [ResponseType(typeof(tblSupportSystem_Problem_Master))]
        [HttpPost]
        public IHttpActionResult SaveSupportSystemProblemMaster(tblSupportSystem_Problem_Master objSupportSystemProblemMaster)
        {
            var SupportSystemProblemInDb = db.tblSupportSystem_Problem_Master.SingleOrDefault(s => s.ProblemName.Trim().ToLower() == objSupportSystemProblemMaster.ProblemName.Trim().ToLower() && s.DepartmentId == objSupportSystemProblemMaster.DepartmentId && s.ProductId == objSupportSystemProblemMaster.ProductId && s.ProblemId != objSupportSystemProblemMaster.ProblemId);
            if (SupportSystemProblemInDb != null)
                return BadRequest("Problem already exists");
            db.tblSupportSystem_Problem_Master.Add(objSupportSystemProblemMaster);
            db.SaveChanges();
            return Ok();
        }

        // DELETE: api/SupportSystemProblemMaster/5
        [Authorize]
        [ResponseType(typeof(tblSupportSystem_Problem_Master))]
        [HttpDelete]
        public IHttpActionResult deleteSupportSystemProblemMaster(int id)
        {
            tblSupportSystem_Problem_Master tblSupportSystem_Problem_Master = db.tblSupportSystem_Problem_Master.Find(id);
            if (tblSupportSystem_Problem_Master == null)
            {
                return NotFound();
            }

            db.tblSupportSystem_Problem_Master.Remove(tblSupportSystem_Problem_Master);
            db.SaveChanges();

            return Ok(tblSupportSystem_Problem_Master);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSupportSystem_Problem_MasterExists(int id)
        {
            return db.tblSupportSystem_Problem_Master.Count(e => e.ProblemId == id) > 0;
        }
    }
}