﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.instantpay.utility.response;
using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class instantPayRechargeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/instantPayRecharge
        [Authorize]
        [ResponseType(typeof(rechargeVM))]
        [Route("api/getRechargeServiceList")]
        [HttpGet]
        public IHttpActionResult getRechargeServiceList(string serviceType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                rechargeVM objRechargeVM = new rechargeVM();
                if (entityType != helper.defaultCompany)
                {
                    objRechargeVM.objInstantPayUtilityDataList = db.tblInstantPay_UtilityData.Where(x => x.ServiceType == serviceType).ToList();
                    objRechargeVM.objRechargeTransaction = new tblInstantPay_Recharge_Transaction();
                    return Ok(objRechargeVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/instantPayRecharge
        [Authorize]
        [ResponseType(typeof(string))]
        [Route("api/instantPayRecharge/rechargeMobile")]
        public IHttpActionResult rechargeMobile(tblInstantPay_Recharge_Transaction objRechargeTransaction)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    string agentId = DateTime.Now.ToString("ddMMyyyyHHmmss");
                    objRechargeTransaction.CreatedDateTime = helper.getIndianTime();
                    objRechargeTransaction.EntityId = entityId;
                    objRechargeTransaction.EntityUserId = entityUserId;
                    var utilityServiceInDb = db.tblInstantPay_UtilityData.Where(x => x.InstantPayRechargeDataId == objRechargeTransaction.InstantPayRechargeDataId).SingleOrDefault();
                    if (utilityServiceInDb != null)
                    {
                        IRestResponse validateRechargeResponse = validateRecharge(ConfigurationManager.AppSettings["InstantPayToken"], objRechargeTransaction.Amount, utilityServiceInDb.ServiceKey, objRechargeTransaction.MobileNo);
                        if (validateRechargeResponse.StatusCode == HttpStatusCode.OK)
                        {
                            validateDataResponse objValidateDataResponse = JsonConvert.DeserializeObject<validateDataResponse>(validateRechargeResponse.Content);
                            if (objValidateDataResponse.ipay_errorcode.Trim() == "TXN" && objValidateDataResponse.ipay_errordesc.Trim() == "Transaction Successful")
                            {
                                IRestResponse transactRechargeResponse = transactRecharge(ConfigurationManager.AppSettings["InstantPayToken"], objRechargeTransaction.Amount, utilityServiceInDb.ServiceKey.Trim(), objRechargeTransaction.MobileNo, objRechargeTransaction.CustomerMobile, agentId);
                                if (transactRechargeResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    if (transactRechargeResponse.Content.Contains("ipay_errorcode") == false)
                                    {
                                        rechargeResponse objRechargeResponse = JsonConvert.DeserializeObject<rechargeResponse>(transactRechargeResponse.Content);
                                        objRechargeTransaction.RechargeRequest = "transaction?format=json&token=" + ConfigurationManager.AppSettings["InstantPayToken"] + "&agentid=" + agentId + "&amount=" + objRechargeTransaction.Amount + "&spkey=" + utilityServiceInDb.ServiceKey.Trim() + "&account=" + objRechargeTransaction.MobileNo + "&customermobile=" + objRechargeTransaction.CustomerMobile;
                                        objRechargeTransaction.RechargeResponse = transactRechargeResponse.Content.ToString();
                                        objRechargeTransaction.status = objRechargeResponse.status;
                                        objRechargeTransaction.res_code = objRechargeResponse.res_code;
                                        objRechargeTransaction.res_msg = objRechargeResponse.res_msg;
                                        db.tblInstantPay_Recharge_Transaction.Add(objRechargeTransaction);
                                        db.SaveChanges();
                                        if (objRechargeResponse.status == "SUCCESS")
                                        {
                                            //Deducting Balance from wallet
                                            string description = utilityServiceInDb.ServiceType + " recharge";
                                            processAccounting(objRechargeTransaction.InstantPayRechargeDataId, objRechargeTransaction.Amount, entityId, entityUserId, objRechargeResponse.ipay_id, companyId, description);
                                            objRechargeTransaction.res_msg = objValidateDataResponse.ipay_errordesc;
                                            return Ok(objRechargeResponse.status);
                                        }
                                        else
                                        {
                                            return BadRequest(objRechargeTransaction.status);
                                        }
                                    }
                                    else
                                    {
                                        return BadRequest(transactRechargeResponse.Content.ToString());
                                    }

                                }
                                else
                                {
                                    return BadRequest(transactRechargeResponse.Content.ToString());
                                }
                            }
                            else
                            {
                                return BadRequest(objValidateDataResponse.ipay_errordesc);
                            }
                        }
                        else
                        {
                            return BadRequest("Unable to do recharge at this moment. Please contact administrator (Error Code : Validate Recharge Error)");
                        }
                    }
                    else
                    {
                        return BadRequest("Unable to do recharge at this moment. Please contact administrator (Error Code : UtilityData Missing)");
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        #region InstantPay Utility Payment API

        [Authorize]
        [ResponseType(typeof(string))]
        [HttpGet]
        [Route("api/instantPayRecharge/checkavailableBalance")]
        public IHttpActionResult checkavailableBalance()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    string URL = "checkWallet?format=json&token=" + ConfigurationManager.AppSettings["InstantPayToken"];
                    IRestResponse response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
                    walletBalance objBalanceResponse = JsonConvert.DeserializeObject<walletBalance>(response.Content);
                    return Ok(objBalanceResponse.Wallet);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(List<tblInstantPay_UtilityData_Circle>))]
        [HttpGet]
        [Route("api/instantPayRecharge/circleList")]
        public IHttpActionResult circleList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    List<tblInstantPay_UtilityData_Circle> objCircleList = new List<tblInstantPay_UtilityData_Circle>();
                    objCircleList = db.tblInstantPay_UtilityData_Circle.ToList();
                    return Ok(objCircleList);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(List<browsePlanData>))]
        [HttpGet]
        [Route("api/instantPayRecharge/browsePlan")]
        public IHttpActionResult browsePlan(string spKey, string circle)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    string URL = "rechargeplans?format=json&token=" + ConfigurationManager.AppSettings["InstantPayToken"] + "&spkey=" + spKey + "&circle=" + circle;
                    IRestResponse response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
                    if (response.Content.ToString().Contains("ipay_errorcode"))
                    {
                        return BadRequest("Plan not found" + response.Content.ToString());
                    }
                    else
                    {
                        List<browsePlanData> objBrowsePlanData = JsonConvert.DeserializeObject<List<browsePlanData>>(response.Content);
                        return Ok(objBrowsePlanData);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        protected IRestResponse validateRecharge(string token, int amount, string spKey, string mobileNo)
        {
            IRestResponse response;
            string URL = "transaction?format=json&token=" + token + "&agentid=1&amount=" + amount + "&spkey=" + spKey.Trim() + "&account=" + mobileNo + "&mode=VALIDATE";
            response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
            return response;
        }

        protected IRestResponse transactRecharge(string token, int amount, string spKey, string accountToRecharge, string customerMobile, string agentId)
        {
            IRestResponse response;
            string URL = "transaction?format=json&token=" + token + "&agentid=" + agentId + "&amount=" + amount + "&spkey=" + spKey.Trim() + "&account=" + accountToRecharge + "&customermobile=" + customerMobile;
            response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
            return response;
        }


        #endregion

        private Boolean processAccounting(long InstantPayRechargeDataId, int transactionAmount, long entityId, long entityUserId, string clientRefId, int companyId, string description)
        {
            int productId = 3;
            tblProduct_Master objProductMaster = db.tblProduct_Master.SingleOrDefault(x => x.ProductId == productId);
            QryInstantPay_UtilityData_Default_Commission objDefaultCommission = db.QryInstantPay_UtilityData_Default_Commission.Where(x => x.IsCustom == false && x.CompanyId == companyId && x.InstantPayRechargeDataId == InstantPayRechargeDataId).SingleOrDefault();
            if (objProductMaster != null && objDefaultCommission != null)
            {
                if (objProductMaster.CommissionOn == "Net Amount")
                {
                    decimal TDSOnServiceCharge = (objProductMaster.TDSOnServiceCharge == null ? 0 : (decimal)objProductMaster.TDSOnServiceCharge);
                    decimal otherChargesByBank = (objProductMaster.OtherChargesByVendor == null ? 0 : (decimal)objProductMaster.OtherChargesByVendor);
                    decimal TDSOnCommission = objProductMaster.TDSRateOnCommission;
                    decimal agentCommission = objDefaultCommission.AgentCommissionValue;
                    decimal distcommission = objDefaultCommission.DistCommissionValue;
                    decimal mdCommission = objDefaultCommission.MDCommissionValue;
                    string distMDEntityIds = helper.getDistMDEntityIdByAgentId(entityId);
                    long distEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[0]);
                    long mdEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[1]);
                    mdCommission = mdCommission - distcommission;
                    distcommission = distcommission - agentCommission;
                    string walletUpdateStatus = "";
                    string transactStatus = "";

                    walletUpdateStatus = helper.updateWalletBalance(entityId, -transactionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, -transactionAmount, clientRefId, description, Convert.ToInt32(helper.transactionTypes.Booking), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;

                    // Calculating Service Charge Amount         
                    #region Agent Taxes and Commission
                    decimal agentCommissionAmount = transactionAmount * agentCommission;
                    if (agentCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(entityId, agentCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(entityId, agentCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;

                        decimal tdsOnAgentCommissionAmount = agentCommissionAmount * TDSOnCommission;

                        walletUpdateStatus = helper.updateWalletBalance(entityId, -tdsOnAgentCommissionAmount);

                        transactStatus = helper.transactEntityAccount(entityId, -tdsOnAgentCommissionAmount, clientRefId, "TDS On Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }
                    #endregion

                    // Distributor Commission & TDS
                    decimal distributorCommissionAmount = transactionAmount * distcommission;
                    if (distributorCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(distEntityId, distributorCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(distEntityId, distributorCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }

                    decimal distributorTDSOnCommissionAmount = distributorCommissionAmount * TDSOnCommission;
                    if (distributorTDSOnCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(distEntityId, -distributorTDSOnCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(distEntityId, -distributorTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }

                    //Master Distributor Commission & TDS
                    decimal MDCommissionAmount = transactionAmount * mdCommission;
                    if (MDCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(mdEntityId, MDCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(mdEntityId, MDCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }


                    decimal MDTDSOnCommissionAmount = MDCommissionAmount * TDSOnCommission;
                    if (MDTDSOnCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(mdEntityId, -MDTDSOnCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(mdEntityId, -MDTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        public static string getRechargeResponse(string fileName)
        {
            string credentialsFilePath = System.Web.HttpContext.Current.Request.MapPath("~/crFiles/" + fileName);
            return System.IO.File.ReadAllText(credentialsFilePath);
        }

        [Authorize]
        [ResponseType(typeof(instantPayRechargeTransactionVM))]
        [Route("api/instantPayRecharge/rechargeHistory")]
        [HttpGet]
        public IHttpActionResult rechargeHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                var companyIdentity = identity.Claims.Where(c => c.Type == "companyId").Single();
                long entityId = Convert.ToInt64(entityIdentity.Value);
                Int32 companyId = Convert.ToInt32(companyIdentity.Value);

                instantPayRechargeTransactionVM objRechargeTransactionVM = new instantPayRechargeTransactionVM();
                objRechargeTransactionVM.objRechargeTransactionVM = db.QryInstantPay_Recharge_Transaction.Where(c => c.EntityId == entityId &&
                (c.CreatedDateTime >= dFromDate &&
                c.CreatedDateTime <= dToDate)).ToList();
                objRechargeTransactionVM.totalRecords = objRechargeTransactionVM.objRechargeTransactionVM.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objRechargeTransactionVM.objRechargeTransactionVM = objRechargeTransactionVM.objRechargeTransactionVM.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.PersonName.ToString().Contains(searchValue) ||
                    x.MobileNo.ToString().Contains(searchValue) ||
                    x.UserName.ToString().Contains(searchValue) ||
                    x.ServiceName.Trim().ToLower().Contains(searchValue)).ToList();

                    objRechargeTransactionVM.filterRecords = objRechargeTransactionVM.objRechargeTransactionVM.Count();
                }
                else
                    objRechargeTransactionVM.filterRecords = objRechargeTransactionVM.totalRecords;

                if (sortDirection == "asc")
                    objRechargeTransactionVM.objRechargeTransactionVM = objRechargeTransactionVM.objRechargeTransactionVM.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.EntityId).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_Recharge_Transaction>();
                else
                    objRechargeTransactionVM.objRechargeTransactionVM = objRechargeTransactionVM.objRechargeTransactionVM.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.EntityId).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_Recharge_Transaction>();
                return Ok(objRechargeTransactionVM);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}