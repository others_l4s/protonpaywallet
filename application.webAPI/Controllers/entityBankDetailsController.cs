﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class entityBankDetailsController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entityBankDetails
        [Authorize]
        [ResponseType(typeof(entityBankDetailsViewModel))]
        [HttpGet]
        public IHttpActionResult GettblEntity_Bank_Details()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    entityBankDetailsViewModel objEntityBankDetailsViewModel = new entityBankDetailsViewModel()
                    {
                        objEntityBankDetails = new tblEntity_Bank_Details(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objBankAccountTypeList = db.tblBankAccountType.OrderBy(c => c.BankAccountTypeName).ToList()
                    };
                    return Ok(objEntityBankDetailsViewModel);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        // GET: api/entityBankDetails/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Bank_Details))]
        [HttpGet]
        public IHttpActionResult GettblEntity_Bank_Details(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    tblEntity_Bank_Details objEntityBankDetails = db.tblEntity_Bank_Details.Find(id);
                    if (objEntityBankDetails == null)
                    {
                        return NotFound();
                    }

                    return Ok(objEntityBankDetails);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else { return Unauthorized(); }
        }

        [Authorize]
        [ResponseType(typeof(entityBankDetailsViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    entityBankDetailsViewModel objEntityBankDetailsViewModel = new entityBankDetailsViewModel();
                    objEntityBankDetailsViewModel.totalRecords = db.tblEntity_Bank_Details.Where(e => e.EntityId == entityId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objEntityBankDetailsViewModel.objEntityBankDetailsList = db.QryEntityBankDetails.Where(s => s.EntityId == entityId && (s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankIFSCCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.PanCard.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objEntityBankDetailsViewModel.filterRecords = objEntityBankDetailsViewModel.objEntityBankDetailsList.Count();
                    }
                    else
                        objEntityBankDetailsViewModel.filterRecords = objEntityBankDetailsViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objEntityBankDetailsViewModel.objEntityBankDetailsList = String.IsNullOrEmpty(searchValue) ?
                            db.QryEntityBankDetails.Where(s => s.EntityId == entityId).OrderBy(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryEntityBankDetails>() :
                            objEntityBankDetailsViewModel.objEntityBankDetailsList.OrderBy(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryEntityBankDetails>();
                    else
                        objEntityBankDetailsViewModel.objEntityBankDetailsList = String.IsNullOrEmpty(searchValue) ?
                            db.QryEntityBankDetails.Where(s => s.EntityId == entityId).OrderByDescending(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryEntityBankDetails>() :
                            objEntityBankDetailsViewModel.objEntityBankDetailsList.OrderByDescending(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryEntityBankDetails>();

                    return Ok(objEntityBankDetailsViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/entityBankDetails/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PuttblEntity_Bank_Details(long id, tblEntity_Bank_Details objEntityBankDetails)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (id != objEntityBankDetails.BankId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        var entityBankdetailsInDb = db.tblEntity_Bank_Details.SingleOrDefault(c => c.EntityId == entityId &&
                        c.BankName == objEntityBankDetails.BankName && c.BankAccountNumber == objEntityBankDetails.BankAccountNumber
                        && c.BankAccountTitle == objEntityBankDetails.BankAccountTitle && c.BankId != objEntityBankDetails.BankId);
                        if (entityBankdetailsInDb != null)
                            return BadRequest("Bank Detail already exists");

                        db.Entry(objEntityBankDetails).State = EntityState.Modified;
                        db.Entry(objEntityBankDetails).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objEntityBankDetails).Property(x => x.EntityId).IsModified = false;
                        db.Entry(objEntityBankDetails).Property(x => x.CreatedDateTime).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblEntity_Bank_DetailsExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else { return Unauthorized(); }
        }

        // POST: api/entityBankDetails
        [Authorize]
        [ResponseType(typeof(tblEntity_Bank_Details))]
        [HttpPost]
        public IHttpActionResult PosttblEntity_Bank_Details(tblEntity_Bank_Details objEntityBankDetails)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);

                    var entityBankdetailsInDb = db.tblEntity_Bank_Details.SingleOrDefault(c => c.EntityId == entityId &&
                    c.BankName == objEntityBankDetails.BankName &&
                    c.BankAccountNumber == objEntityBankDetails.BankAccountNumber &&
                    c.BankAccountTitle == objEntityBankDetails.BankAccountTitle && c.BankId != objEntityBankDetails.BankId);
                    if (entityBankdetailsInDb != null)
                        return BadRequest("Bank Detail already exists");

                    objEntityBankDetails.EntityId = entityId;
                    objEntityBankDetails.CreatedBy = entityUserId;
                    objEntityBankDetails.CreatedDateTime = helper.getIndianTime();
                    db.tblEntity_Bank_Details.Add(objEntityBankDetails);
                    db.SaveChanges();
                    return Ok();
                }
                else
                { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // DELETE: api/entityBankDetails/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Bank_Details))]
        [HttpDelete]
        public IHttpActionResult DeletetblEntity_Bank_Details(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    tblEntity_Bank_Details objEntityBankDetails = db.tblEntity_Bank_Details.Find(id);
                    if (objEntityBankDetails == null)
                    {
                        return NotFound();
                    }

                    db.tblEntity_Bank_Details.Remove(objEntityBankDetails);
                    db.SaveChanges();
                    return Ok(objEntityBankDetails);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            { return Unauthorized(); }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_Bank_DetailsExists(long id)
        {
            return db.tblEntity_Bank_Details.Count(e => e.BankId == id) > 0;
        }
    }
}