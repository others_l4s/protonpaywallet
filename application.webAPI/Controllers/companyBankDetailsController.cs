﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class companyBankDetailsController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/companyBankDetails
        [Authorize]
        [ResponseType(typeof(companyBankDetailsController))]
        [HttpGet]
        public IHttpActionResult GetQryCompanyBankDetail()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    companyBankDetailsViewModel objBankDetailsViewModel = new companyBankDetailsViewModel()
                    {
                        objCompanyBankDetails = new tblCompany_Bank_Details(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objBankAccountTypeList = db.tblBankAccountType.OrderBy(c => c.BankAccountTypeName).ToList()
                    };
                    return Ok(objBankDetailsViewModel);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        // GET: api/companyBankDetails/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Bank_Details))]
        public IHttpActionResult GetQryCompanyBankDetail(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Bank_Details objCompanyBankDetails = db.tblCompany_Bank_Details.Find(id);
                    if (objCompanyBankDetails == null)
                    {
                        return NotFound();
                    }
                    return Ok(objCompanyBankDetails);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(companyBankDetailsViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanyBankDetailsListbyPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    companyBankDetailsViewModel objCompanyBankDetailsViewModel = new companyBankDetailsViewModel();
                    objCompanyBankDetailsViewModel.totalRecords = db.tblCompany_Bank_Details.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCompanyBankDetailsViewModel.objCompanyBankDetailsList = db.QryCompanyBankDetail.Where(s => s.CompanyId == companyId && (s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankIFSCCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.PanCard.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objCompanyBankDetailsViewModel.filterRecords = objCompanyBankDetailsViewModel.objCompanyBankDetailsList.Count();
                    }
                    else
                        objCompanyBankDetailsViewModel.filterRecords = objCompanyBankDetailsViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objCompanyBankDetailsViewModel.objCompanyBankDetailsList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompanyBankDetail.Where(s => s.CompanyId == companyId).OrderBy(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyBankDetail>() :
                            objCompanyBankDetailsViewModel.objCompanyBankDetailsList.OrderBy(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyBankDetail>();
                    else
                        objCompanyBankDetailsViewModel.objCompanyBankDetailsList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompanyBankDetail.Where(s => s.CompanyId == companyId).OrderByDescending(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyBankDetail>() :
                            objCompanyBankDetailsViewModel.objCompanyBankDetailsList.OrderByDescending(s => s.BankName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyBankDetail>();

                    return Ok(objCompanyBankDetailsViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/companyBankDetails/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateCompanyBankDetails(long id, tblCompany_Bank_Details objCompanyBankDetail)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    if (id != objCompanyBankDetail.BankId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var companyBankDetailsInDb = db.tblCompany_Bank_Details.SingleOrDefault(c => c.CompanyId == companyId &&
                        c.BankName == objCompanyBankDetail.BankName && c.BankAccountNumber == objCompanyBankDetail.BankAccountNumber
                        && c.BankAccountTitle == objCompanyBankDetail.BankAccountTitle && c.BankId != objCompanyBankDetail.BankId);
                        if (companyBankDetailsInDb != null)
                            return BadRequest("Bank Detail already exists");

                        db.Entry(objCompanyBankDetail).State = EntityState.Modified;
                        db.Entry(objCompanyBankDetail).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objCompanyBankDetail).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!QryCompanyBankDetailExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        // POST: api/companyBankDetails
        [Authorize]
        [ResponseType(typeof(tblCompany_Bank_Details))]
        [HttpPost]
        public IHttpActionResult saveCompanyBankDetail(tblCompany_Bank_Details objCompanyBankDetails)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt32(identityAdminLoginId.Value);

                    var companyBankDetailsInDb = db.tblCompany_Bank_Details.SingleOrDefault(c => c.CompanyId == companyId &&
                        c.BankName == objCompanyBankDetails.BankName && c.BankAccountNumber == objCompanyBankDetails.BankAccountNumber
                        && c.BankAccountTitle == objCompanyBankDetails.BankAccountTitle && c.BankId != objCompanyBankDetails.BankId);
                    if (companyBankDetailsInDb != null)
                        return BadRequest("Bank Detail already exists");

                    objCompanyBankDetails.CompanyId = companyId;
                    objCompanyBankDetails.CreatedBy = adminLoginId;
                    db.tblCompany_Bank_Details.Add(objCompanyBankDetails);
                    db.SaveChanges();
                    return Ok();
                }
                else
                { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // DELETE: api/companyBankDetails/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult deleteCompanyBankDetails(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Bank_Details objCompanyBankDetails = db.tblCompany_Bank_Details.Find(id);
                    if (objCompanyBankDetails == null)
                    {
                        return NotFound();
                    }

                    db.tblCompany_Bank_Details.Remove(objCompanyBankDetails);
                    db.SaveChanges();

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QryCompanyBankDetailExists(long id)
        {
            return db.QryCompanyBankDetail.Count(e => e.BankId == id) > 0;
        }
    }
}