﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class companyWalletController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/companyWallet/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Wallet))]
        [Route("api/getVirtualBalance")]
        public IHttpActionResult getVirtualBalance()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt32(identityEntityType.Value) == Convert.ToInt32(helper.entityTypes.company))
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    tblCompany_Wallet objCompanyWallet = db.tblCompany_Wallet.Where(x => x.CompanyId == companyId).SingleOrDefault();
                    if (objCompanyWallet == null)
                    {
                        return NotFound();
                    }
                    return Ok(objCompanyWallet);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        [Authorize]
        [ResponseType(typeof(companyAccountVM))]
        [Route("api/getVirtualBalanceAccount")]
        public IHttpActionResult getVirtualBalanceAccount(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                companyAccountVM objCompanyAccountVM = new companyAccountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var entityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(entityCompanyId.Value);
                    objCompanyAccountVM.objCompanyAccountList = db.QryCompanyAccount.Where(c => c.CompanyId == companyId &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                    objCompanyAccountVM.totalRecords = objCompanyAccountVM.objCompanyAccountList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCompanyAccountVM.objCompanyAccountList = objCompanyAccountVM.objCompanyAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                        x.Amount.ToString().Contains(searchValue) ||
                        x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();
                        objCompanyAccountVM.filterRecords = objCompanyAccountVM.objCompanyAccountList.Count();
                    }
                    else
                        objCompanyAccountVM.filterRecords = objCompanyAccountVM.totalRecords;

                    if (sortDirection == "asc")
                        objCompanyAccountVM.objCompanyAccountList = objCompanyAccountVM.objCompanyAccountList.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.AccountId).Skip(pageIndex).Take(pageSize).ToList<QryCompanyAccount>();
                    else
                        objCompanyAccountVM.objCompanyAccountList = objCompanyAccountVM.objCompanyAccountList.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.AccountId).Skip(pageIndex).Take(pageSize).ToList<QryCompanyAccount>();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
                return Ok(objCompanyAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/companyWallet
        [Authorize]
        [ResponseType(typeof(tblCompany_Wallet))]
        [Route("api/addVirtualBalance")]
        public IHttpActionResult addVirtualBalance(tblCompany_Wallet objCompanyWallet)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt32(identityEntityType.Value) == Convert.ToInt32(helper.entityTypes.company))
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);

                    objCompanyWallet.CompanyId = companyId;
                    tblCompany_Wallet objCompanyWalletInDb = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId);
                    if (objCompanyWalletInDb != null)
                    {
                        objCompanyWalletInDb.WalletBalance += objCompanyWallet.WalletBalance;
                        db.Entry(objCompanyWalletInDb).State = EntityState.Modified;
                    }
                    else
                    {
                        db.tblCompany_Wallet.Add(objCompanyWallet);

                    }
                    db.SaveChanges();
                    helper.transactCompanyAccount(companyId, 0, objCompanyWallet.WalletBalance, "", "Add Virtual Balance", Convert.ToInt32(helper.transactionTypes.WalletTopUp), true, adminLoginId);
                    return Ok(objCompanyWallet);
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}