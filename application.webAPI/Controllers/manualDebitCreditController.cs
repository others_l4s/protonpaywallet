﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class manualDebitCreditController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();


        // GET: api/manualDebitCredit
        [Authorize]
        [HttpGet]
        public IHttpActionResult getManualDebitCredit()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                manualDebitCreditVM objManualDebitCreditVM = new manualDebitCreditVM();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    objManualDebitCreditVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1).ToList();
                }
                else
                {
                    int entityTypeMD = Convert.ToInt16(helper.entityTypes.masterDistributor);
                    int entityTypeDist = Convert.ToInt16(helper.entityTypes.distributor);
                    int entityTypeAgent = Convert.ToInt16(helper.entityTypes.agent);

                    if (Convert.ToInt16(identityEntityType.Value) == entityTypeMD)
                        objManualDebitCreditVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1 && x.EntityTypeId != entityTypeMD).ToList();
                    else if (Convert.ToInt16(identityEntityType.Value) == entityTypeDist)
                        objManualDebitCreditVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1 &&
                        x.EntityTypeId != entityTypeMD && x.EntityTypeId != entityTypeDist).ToList();
                }
                objManualDebitCreditVM.objManualDebitCreditList = db.QryManual_DebitCredit.Where(x => x.CompanyId == companyId).ToList();
                objManualDebitCreditVM.objManualDebitCredit = new tblManual_DebitCredit();
                objManualDebitCreditVM.objProductList = db.tblProduct_Master.Where(x => x.StatusId == 1).OrderBy(x => x.ProductCode).ToList();
                return Ok(objManualDebitCreditVM);
            }
            else
                return Unauthorized();
        }

        // GET: api/manualDebitCredit/5
        [Authorize]
        [HttpGet]
        public IHttpActionResult getManualDebitCreditById(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblManual_DebitCredit objManualDebitCredit = db.tblManual_DebitCredit.Find(id);
                    if (objManualDebitCredit == null)
                    {
                        return NotFound();
                    }
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    manualDebitCreditVM objManualDebitCreditVM = new manualDebitCreditVM();
                    objManualDebitCreditVM.objManualDebitCreditList = db.QryManual_DebitCredit.Where(x => x.CompanyId == companyId).ToList();
                    objManualDebitCreditVM.objEntityTypeList = db.tblEntity_Type.ToList();
                    objManualDebitCreditVM.objManualDebitCredit = objManualDebitCredit;
                    return Ok(objManualDebitCreditVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return BadRequest();
            }
        }

        // POST: api/manualDebitCredit
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult saveManualDebitCredit(tblManual_DebitCredit objManualDebitCredit)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                string clientTransactionId = helper.GetRandomPasswordUsingGUID(5).ToUpper() + helper.InstantPayDMT_agentId();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);

                    objManualDebitCredit.CreatedBy = adminLoginId;
                    objManualDebitCredit.CreatedDateTime = helper.getIndianTime();
                    objManualDebitCredit.CompanyId = companyId;
                    objManualDebitCredit.DoneByParent = false;
                    objManualDebitCredit.ParentEntityId = 0;
                    db.tblManual_DebitCredit.Add(objManualDebitCredit);
                    db.SaveChanges();

                    ///Checking whether Company has enough Virtual Balance whlie Crediting to Entity Account
                    if (objManualDebitCredit.IsCredit)
                    {
                        if (helper.hasEnoughCompanyWalletBalance(companyId, objManualDebitCredit.Amount) == false)
                            return BadRequest("Company does not have enought Virtual Balance");
                    }
                    //Checking whether Entity has enough Balance for Debit into Company Account
                    if (objManualDebitCredit.IsCredit == false)
                    {
                        if (helper.checkWalletBalance(objManualDebitCredit.EntityId, objManualDebitCredit.Amount) == false)
                            return BadRequest("Agency does not have enough balance to Debit");
                    }
                    //Account 
                    helper.updateWalletBalance(objManualDebitCredit.EntityId, (objManualDebitCredit.IsCredit ? objManualDebitCredit.Amount : -objManualDebitCredit.Amount));
                    helper.transactEntityAccount(objManualDebitCredit.EntityId, objManualDebitCredit.Amount, clientTransactionId, objManualDebitCredit.Remark, Convert.ToInt32(helper.transactionTypes.Refund), objManualDebitCredit.IsCredit, objManualDebitCredit.ProductId, 0, objManualDebitCredit.Amount);

                    // Updating Company Wallet
                    helper.updateCompanyWalletBalance(companyId, (objManualDebitCredit.IsCredit ? -objManualDebitCredit.Amount : objManualDebitCredit.Amount));
                    return Ok();
                }
                else
                {
                    // Manual Debit/Credit by MD or Distributor
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);

                    objManualDebitCredit.CreatedBy = entityUserId;
                    objManualDebitCredit.CreatedDateTime = helper.getIndianTime();
                    objManualDebitCredit.CompanyId = companyId;
                    objManualDebitCredit.DoneByParent = true;
                    objManualDebitCredit.ParentEntityId = entityId;
                    db.tblManual_DebitCredit.Add(objManualDebitCredit);
                    db.SaveChanges();

                    ///Checking whether Parent Agency has enough Wallet Balance whlie Crediting to Entity Account
                    if (objManualDebitCredit.IsCredit)
                    {
                        if (helper.checkWalletBalance(entityId, objManualDebitCredit.Amount) == false)
                            return BadRequest("You do not have enought Wallet Balance to Credit");
                    }
                    //Checking whether Entity has enough Balance for Debit into Company Account
                    if (objManualDebitCredit.IsCredit == false)
                    {
                        if (helper.checkWalletBalance(objManualDebitCredit.EntityId, objManualDebitCredit.Amount) == false)
                            return BadRequest("Agency does not have enough balance to Debit");
                    }

                    //Updating Account of EntityId
                    helper.updateWalletBalance(objManualDebitCredit.EntityId, (objManualDebitCredit.IsCredit ? objManualDebitCredit.Amount : -objManualDebitCredit.Amount));
                    helper.transactEntityAccount(objManualDebitCredit.EntityId, (objManualDebitCredit.IsCredit ? objManualDebitCredit.Amount : -objManualDebitCredit.Amount), clientTransactionId, objManualDebitCredit.Remark, Convert.ToInt32(helper.transactionTypes.Refund), objManualDebitCredit.IsCredit, objManualDebitCredit.ProductId, 0, objManualDebitCredit.Amount);

                    //Updating Account of Self MD or Distributor
                    helper.updateWalletBalance(entityId, (objManualDebitCredit.IsCredit ? -objManualDebitCredit.Amount : objManualDebitCredit.Amount));
                    helper.transactEntityAccount(entityId, (objManualDebitCredit.IsCredit ? -objManualDebitCredit.Amount : objManualDebitCredit.Amount), clientTransactionId, objManualDebitCredit.Remark, Convert.ToInt32(helper.transactionTypes.Refund), (objManualDebitCredit.IsCredit ? false : true), objManualDebitCredit.ProductId, 0, objManualDebitCredit.Amount);
                    return Ok();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(manualDebitCreditVM))]
        [HttpGet]
        public IHttpActionResult getManualDebitCredit(long entityId, int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                manualDebitCreditVM objManualDebitCreditVM = new manualDebitCreditVM();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    objManualDebitCreditVM.objManualDebitCreditList = db.QryManual_DebitCredit.Where(x => x.CompanyId == companyId && (entityId == 0 || x.EntityId == entityId) && x.DoneByParent == false).ToList();
                    objManualDebitCreditVM.totalRecords = objManualDebitCreditVM.objManualDebitCreditList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.Where(x => x.CompanyId == companyId &&
                        (x.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objManualDebitCreditVM.filterRecords = objManualDebitCreditVM.objManualDebitCreditList.Count();
                    }
                    else
                        objManualDebitCreditVM.filterRecords = objManualDebitCreditVM.totalRecords;
                    if (sortDirection == "asc")
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryManual_DebitCredit>();
                    else
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryManual_DebitCredit>();

                    return Ok(objManualDebitCreditVM);
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long loggedInEntityId = Convert.ToInt64(identityEntityId.Value);

                    objManualDebitCreditVM.objManualDebitCreditList = db.QryManual_DebitCredit.Where(x => x.CompanyId == companyId &&
                    (entityId == 0 || x.EntityId == entityId) &&
                    x.ParentEntityId == loggedInEntityId &&
                    x.DoneByParent == true).ToList();
                    objManualDebitCreditVM.totalRecords = objManualDebitCreditVM.objManualDebitCreditList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.Where(x => x.CompanyId == companyId &&
                        (x.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objManualDebitCreditVM.filterRecords = objManualDebitCreditVM.objManualDebitCreditList.Count();
                    }
                    else
                        objManualDebitCreditVM.filterRecords = objManualDebitCreditVM.totalRecords;
                    if (sortDirection == "asc")
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryManual_DebitCredit>();
                    else
                        objManualDebitCreditVM.objManualDebitCreditList = objManualDebitCreditVM.objManualDebitCreditList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryManual_DebitCredit>();

                    return Ok(objManualDebitCreditVM);
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblManual_DebitCreditExists(long id)
        {
            return db.tblManual_DebitCredit.Count(e => e.ManualWalletUpdateId == id) > 0;
        }
    }
}