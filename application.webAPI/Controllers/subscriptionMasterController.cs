﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class subscriptionMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/subscriptionMaster
        [Authorize]
        [ResponseType(typeof(subscriptionMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getSubscriptionList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subscriptionMasterViewModel objCompanyViewModel = new subscriptionMasterViewModel()
                    {
                        objSubscriptionMaster = new tblSubscription_Master(),
                        objCompanyList = db.tblCompany_Master.Where(c => c.CompanyId == helper.defaultCompany).OrderBy(c => c.CompanyName).ToList(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objSubscriptionList = new List<QrySubscription>()
                    };
                    return Ok(objCompanyViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/subscriptionMaster/5
        [Authorize]
        [ResponseType(typeof(tblSubscription_Master))]
        [HttpGet]
        public IHttpActionResult getSubscriptionListById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblSubscription_Master objSubscriptionMaster = db.tblSubscription_Master.Find(id);
                    if (objSubscriptionMaster == null)
                    {
                        return NotFound();
                    }

                    return Ok(objSubscriptionMaster);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(subscriptionMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getSubscriptionMasterListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subscriptionMasterViewModel objSubscriptionViewModel = new subscriptionMasterViewModel();
                    objSubscriptionViewModel.totalRecords = db.tblSubscription_Master.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objSubscriptionViewModel.objSubscriptionList = db.QrySubscription.Where(s => s.SubscriptionName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.SubscriptionRemark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objSubscriptionViewModel.filterRecords = objSubscriptionViewModel.objSubscriptionList.Count();
                    }
                    else
                        objSubscriptionViewModel.filterRecords = objSubscriptionViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objSubscriptionViewModel.objSubscriptionList = String.IsNullOrEmpty(searchValue) ?
                            db.QrySubscription.OrderBy(s => s.SubscriptionName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QrySubscription>() :
                            objSubscriptionViewModel.objSubscriptionList.OrderBy(s => s.CompanyName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QrySubscription>();
                    else
                        objSubscriptionViewModel.objSubscriptionList = String.IsNullOrEmpty(searchValue) ?
                            db.QrySubscription.OrderByDescending(s => s.SubscriptionName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QrySubscription>() :
                            objSubscriptionViewModel.objSubscriptionList.OrderByDescending(s => s.SubscriptionName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QrySubscription>();

                    return Ok(objSubscriptionViewModel);
                }
                else
                    return Unauthorized();
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/subscriptionMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateSubscriptionMaster(int id, tblSubscription_Master objSubscriptionMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objSubscriptionMaster.SubscriptionId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var subscriptionInDb = db.tblSubscription_Master.SingleOrDefault(s => s.SubscriptionName.Trim().ToLower() == objSubscriptionMaster.SubscriptionName && s.SubscriptionId != objSubscriptionMaster.SubscriptionId);
                        if (subscriptionInDb != null)
                            return BadRequest("Subscription name already exists");

                        db.Entry(objSubscriptionMaster).State = EntityState.Modified;
                        db.Entry(objSubscriptionMaster).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objSubscriptionMaster).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesSubscriptionExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                { return Unauthorized(); }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/subscriptionMaster
        [Authorize]
        [ResponseType(typeof(tblSubscription_Master))]
        [HttpPost]
        public IHttpActionResult saveSubscriptionMaster(tblSubscription_Master objSubscriptionMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var subscriptionInDb = db.tblSubscription_Master.SingleOrDefault(s => s.SubscriptionName.Trim().ToLower() == objSubscriptionMaster.SubscriptionName && s.SubscriptionId != objSubscriptionMaster.SubscriptionId);
                    if (subscriptionInDb != null)
                        return BadRequest("Subscription name already exists");

                    objSubscriptionMaster.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    objSubscriptionMaster.CompanyId = helper.defaultCompany;
                    db.tblSubscription_Master.Add(objSubscriptionMaster);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/subscriptionMaster/5
        [Authorize]
        [ResponseType(typeof(tblSubscription_Master))]
        [HttpDelete]
        public IHttpActionResult deleteSubscriptionMaster(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblSubscription_Master objSubscriptionMaster = db.tblSubscription_Master.Find(id);
                    if (objSubscriptionMaster == null)
                    {
                        return NotFound();
                    }

                    db.tblSubscription_Master.Remove(objSubscriptionMaster);
                    db.SaveChanges();

                    return Ok(objSubscriptionMaster);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesSubscriptionExists(int id)
        {
            return db.tblSubscription_Master.Count(e => e.SubscriptionId == id) > 0;
        }
    }
}