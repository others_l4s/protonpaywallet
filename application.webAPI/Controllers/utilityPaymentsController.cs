﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO.Pipes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ValueProviders;
using application.webAPI.instantpay.utility.response;
using application.webAPI.Models;
using application.webAPI.mrobotics.recharge.response;
using application.webAPI.utilityPayment.request;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using RestSharp;

namespace application.webAPI.Controllers
{
    public class utilityPaymentsController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/utilityPayments
        [Route("api/utilityPayments/getOperatorTypeList")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult getOperatorTypeList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                List<tblMobileRecharge_OperatorType> objOperatorTypeList = db.tblMobileRecharge_OperatorType.Where(x => x.Status == true).OrderBy(x => x.OperatorTypeName).ToList();
                return Ok(objOperatorTypeList);
            }
            else { return Unauthorized(); }
        }

        [Route("api/utilityPayments/getOperatorList")]
        [Authorize]
        [HttpGet]
        public IHttpActionResult getOperatorList(Int32 operatorTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                List<QryMobileRecharge_Vendor> objOperatorList = db.QryMobileRecharge_Vendor.Where(x => x.Status == true && x.OperatorTypeId == operatorTypeId && x.DefaultGateway == true).OrderBy(x => x.OperatorName).ToList();
                return Ok(objOperatorList);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Route("api/utilityPayments/recharge")]
        [Authorize]
        [HttpPost]
        public IHttpActionResult rechargeUtilityBills(rechargeRequest objRechargeRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                string[] arrResponse = new string[6];
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                QryMobileRecharge_Vendor objMobileRechargeVendor = db.QryMobileRecharge_Vendor.Where(x => x.OperatorVendorId == objRechargeRequest.OperatorVendorId && x.Status == true).SingleOrDefault();
                if (objMobileRechargeVendor != null)
                {
                    if (validateDTHConsumerNoLength(objMobileRechargeVendor.OperatorId, (objMobileRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo)))
                    {
                        tblMobileRecharge_Transactions objMobileRechargeTransactions = new tblMobileRecharge_Transactions();
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                        int companyId = Convert.ToInt32(identityCompanyId.Value);
                        string agentId = DateTime.Now.ToString("ddMMyyyyHHmmssfff");
                        objMobileRechargeTransactions.CreatedDateTime = helper.getIndianTime();
                        objMobileRechargeTransactions.EntityId = entityId;
                        objMobileRechargeTransactions.EntityUserId = entityUserId;
                        objMobileRechargeTransactions.IPAddress = objRechargeRequest.IPAddress;
                        objMobileRechargeTransactions.OperatorVendorId = objRechargeRequest.OperatorVendorId;
                        objMobileRechargeTransactions.MobileNo = (objMobileRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo);
                        objMobileRechargeTransactions.OrderId = agentId;
                        objMobileRechargeTransactions.RechargeAmount = objRechargeRequest.Amount;
                        if (objMobileRechargeVendor.VendorName == "INSTANTPAY")
                        {
                            arrResponse = processInstantPayRecharge(objMobileRechargeVendor, objRechargeRequest, agentId);
                            if (arrResponse[1] == "TXN" || arrResponse[1] == "TUP")
                            {
                                string description = objMobileRechargeVendor.OperatorName + " recharge for " + (objMobileRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo);
                                //processAccounting(objRechargeTransaction.InstantPayRechargeDataId, objRechargeTransaction.Amount, entityId, entityUserId, objRechargeResponse.ipay_id, companyId, description);
                                //objRechargeTransaction.res_msg = objValidateDataResponse.ipay_errordesc;
                            }
                        }
                        if (objMobileRechargeVendor.VendorName == "M-ROBOTICS")
                        {
                            arrResponse = processMRoboticRecharge(objMobileRechargeVendor, objRechargeRequest, agentId);

                        }
                        if (objMobileRechargeVendor.VendorName == "Multilink")
                        { }

                        objMobileRechargeTransactions.TransactionStatus = arrResponse[0];
                        objMobileRechargeTransactions.TransactionStatusCode = arrResponse[1];
                        objMobileRechargeTransactions.RequestAPI = arrResponse[2];
                        objMobileRechargeTransactions.ResponseAPI = arrResponse[3];
                        objMobileRechargeTransactions.TxnId = arrResponse[4];
                        objMobileRechargeTransactions.OtherResponseValue1 = arrResponse[5];
                        db.tblMobileRecharge_Transactions.Add(objMobileRechargeTransactions);
                        db.SaveChanges();

                        if (arrResponse[0].ToUpper().Trim() == "SUCCESS" || arrResponse[0].ToUpper().Trim() == "PENDING")
                        {
                            return Ok(arrResponse[0].ToUpper().Trim());
                        }
                        else
                        {
                            return BadRequest(arrResponse[0].ToUpper().Trim());
                        }
                    }
                    else
                    {
                        return BadRequest(showDTHConsumerNoInputRange(objMobileRechargeVendor.OperatorVendorId, (objMobileRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo)));
                    }
                }
                else
                {
                    return BadRequest("Unable to Recharge at the moment, please try again later.");
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMobileRecharge_TransactionsExists(long id)
        {
            return db.tblMobileRecharge_Transactions.Count(e => e.TransactionHistoryId == id) > 0;
        }

        private string showDTHConsumerNoInputRange(long operatorId, string DTHNumber)
        {
            string DTHInputRange = "";
            switch (operatorId)
            {
                case 19: // Airtel Digital TV
                    DTHInputRange = "Enter 10 digit DTH No.";
                    break;
                case 20: // Videocon D2H
                    DTHInputRange = "Enter DTH No. between 4 to 14 Digit";
                    break;
                case 21: //Dish TV
                    DTHInputRange = "Enter DTH No. between 10 to 11 Digit";
                    break;
                case 22: // Reliance Digital TV
                    DTHInputRange = "Enter 12 digit DTH No.";
                    break;
                case 23: // Sun Direct
                    DTHInputRange = "Enter DTH No. between 10 to 11 Digit";
                    break;
                case 24: // Tata Sky
                    DTHInputRange = "Enter 10 digit DTH No.";
                    break;
                case 25: // Tata Sky Online
                    DTHInputRange = "Enter 10 digit DTH No.";
                    break;
            }
            return DTHInputRange;
        }

        private Boolean validateDTHConsumerNoLength(long operatorId, string DTHNumber)
        {
            Boolean isValidateDTHConsumerNo = false;
            switch (operatorId)
            {
                case 19: // Airtel Digital TV
                    if (DTHNumber.Length == 10)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 20: // Videocon D2H
                    if (DTHNumber.Length >= 4 && DTHNumber.Length <= 14)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 21: //Dish TV
                    if (DTHNumber.Length >= 10 && DTHNumber.Length <= 11)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 22: // Reliance Digital TV
                    if (DTHNumber.Length == 12)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 23: // Sun Direct
                    if (DTHNumber.Length >= 10 && DTHNumber.Length <= 11)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 24: // Tata Sky
                    if (DTHNumber.Length == 10)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
                case 25: // Tata Sky Online
                    if (DTHNumber.Length == 10)
                        isValidateDTHConsumerNo = true;
                    else
                        isValidateDTHConsumerNo = false;
                    break;
            }
            return isValidateDTHConsumerNo;
        }

        #region InstantPay Recharge API Vendor

        private string[] processInstantPayRecharge(QryMobileRecharge_Vendor objRechargeVendor, rechargeRequest objRechargeRequest, string agentID)
        {
            string[] arrResponse = new string[6];
            IRestResponse validateRechargeResponse = instantPAYValidateRecharge(ConfigurationManager.AppSettings["InstantPayToken"], objRechargeRequest.Amount, objRechargeVendor.Key1.Trim(), (objRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo), agentID);
            if (validateRechargeResponse.StatusCode == HttpStatusCode.OK)
            {
                validateDataResponse objValidateDataResponse = JsonConvert.DeserializeObject<validateDataResponse>(validateRechargeResponse.Content);
                if (objValidateDataResponse.ipay_errorcode.Trim() == "TXN" && objValidateDataResponse.ipay_errordesc.Trim() == "Transaction Successful")
                {
                    IRestResponse transactRechargeResponse = instantPAYTransactRecharge(ConfigurationManager.AppSettings["InstantPayToken"], objRechargeRequest.Amount, objRechargeVendor.Key1.Trim(), (objRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo), objRechargeRequest.customermobile, agentID);
                    arrResponse[2] = "transaction?format=json&token=" + ConfigurationManager.AppSettings["InstantPayToken"] + "&agentid=" + agentID + "&amount=" + objRechargeRequest.Amount + "&spkey=" + objRechargeVendor.Key1.Trim() + "&account=" + (objRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo) + "&customermobile=" + objRechargeRequest.customermobile;
                    arrResponse[3] = transactRechargeResponse.Content.ToString();
                    if (transactRechargeResponse.StatusCode == HttpStatusCode.OK)
                    {
                        if (transactRechargeResponse.Content.Contains("ipay_errorcode") == false)
                        {
                            rechargeResponse objRechargeResponse = JsonConvert.DeserializeObject<rechargeResponse>(transactRechargeResponse.Content);
                            arrResponse[0] = (objRechargeResponse.status == "SUCCESS" ? "Success" : objRechargeResponse.status);
                            arrResponse[1] = objRechargeResponse.res_code;
                            arrResponse[4] = objRechargeResponse.ipay_id;
                            arrResponse[5] = objRechargeResponse.res_msg;
                        }
                        else
                        {
                            arrResponse[0] = transactRechargeResponse.Content.ToString();
                        }
                    }
                    else
                    {
                        arrResponse[0] = transactRechargeResponse.Content.ToString();
                    }
                }
                else
                {
                    arrResponse[0] = objValidateDataResponse.ipay_errordesc;
                }
            }
            else
            {
                arrResponse[0] = "Unable to do recharge at this moment. Please contact administrator (Error Code : Validate Recharge Error) Status Code : " + validateRechargeResponse.StatusCode;
            }
            return arrResponse;
        }

        protected IRestResponse instantPAYValidateRecharge(string token, int amount, string spKey, string mobileNo, string agentId)
        {
            IRestResponse response;
            string URL = "transaction?format=json&token=" + token + "&agentid=" + agentId + "&amount=" + amount + "&spkey=" + spKey.Trim() + "&account=" + mobileNo + "&mode=VALIDATE";
            response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
            return response;
        }

        protected IRestResponse instantPAYTransactRecharge(string token, int amount, string spKey, string accountToRecharge, string customerMobile, string agentId)
        {
            IRestResponse response;
            string URL = "transaction?format=json&token=" + token + "&agentid=" + agentId + "&amount=" + amount + "&spkey=" + spKey.Trim() + "&account=" + accountToRecharge + "&customermobile=" + customerMobile;
            response = helper.WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(URL, Method.GET, null);
            return response;
        }

        #endregion

        #region Multilink Recharge API Vendor

        private string[] processMRoboticRecharge(QryMobileRecharge_Vendor objRechargeVendor, rechargeRequest objRechargeRequest, string agentID)
        {
            string[] arrResponse = new string[6];
            string specialSTV = objRechargeVendor.OperatorVendorId == 7 ? "true" : objRechargeVendor.OperatorVendorId == 5 ? "true" : "false";
            string apiPArameter = objRechargeVendor.Key1 + "&mobile_no=" + (objRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo) +
                "&amount=" + objRechargeRequest.Amount + "&company_id=" + objRechargeVendor.Key2 + "&order_id=" + agentID + "&is_stv=" + specialSTV;
            IRestResponse rechargeResponse = helper.WebAPIClient_Via_RestSharp_4_MRoboticRecharge(objRechargeVendor.APIURL, apiPArameter, Method.GET, null);
            arrResponse[2] = apiPArameter;
            arrResponse[3] = rechargeResponse.Content.ToString();
            if (rechargeResponse.StatusCode == HttpStatusCode.OK)
            {
                mRoboticRechargeResponse objRechargeResponse = JsonConvert.DeserializeObject<mRoboticRechargeResponse>(rechargeResponse.Content);
                if (objRechargeResponse.status == "success")
                {
                    arrResponse[0] = objRechargeResponse.status;
                    arrResponse[1] = objRechargeResponse.response;
                    arrResponse[4] = objRechargeResponse.tnx_id;
                    arrResponse[5] = objRechargeResponse.id;
                }
                else
                {
                    arrResponse[0] = objRechargeResponse.status;
                }
            }
            else
            {
                arrResponse[0] = "Unable to do recharge at this moment. Please contact administrator (Error Code : Validate Recharge Error) Status Code : " + rechargeResponse.StatusCode;
            }
            return arrResponse;
        }

        #endregion

        #region M-Robotics API Vendor

        private string[] processMultilinkRecharge(QryMobileRecharge_Vendor objRechargeVendor, rechargeRequest objRechargeRequest, string agentID)
        {
            string[] arrResponse = new string[6];
            string specialSTV = objRechargeVendor.OperatorVendorId == 7 ? "true" : objRechargeVendor.OperatorVendorId == 5 ? "true" : "false";
            string apiPArameter = objRechargeVendor.Key1 + "&mobile_no=" + (objRechargeVendor.OperatorTypeId == 3 ? objRechargeRequest.DTHNumber : objRechargeRequest.MobileNo) +
                "&amount=" + objRechargeRequest.Amount + "&company_id=" + objRechargeVendor.Key2 + "&order_id=" + agentID + "&is_stv=" + specialSTV;
            IRestResponse rechargeResponse = helper.WebAPIClient_Via_RestSharp_4_MRoboticRecharge(objRechargeVendor.APIURL, apiPArameter, Method.GET, null);
            arrResponse[2] = apiPArameter;
            arrResponse[3] = rechargeResponse.Content.ToString();
            if (rechargeResponse.StatusCode == HttpStatusCode.OK)
            {
                mRoboticRechargeResponse objRechargeResponse = JsonConvert.DeserializeObject<mRoboticRechargeResponse>(rechargeResponse.Content);
                if (objRechargeResponse.status == "success")
                {
                    arrResponse[0] = objRechargeResponse.status;
                    arrResponse[1] = objRechargeResponse.response;
                    arrResponse[4] = objRechargeResponse.tnx_id;
                    arrResponse[5] = objRechargeResponse.id;
                }
                else
                {
                    arrResponse[0] = objRechargeResponse.status;
                }
            }
            else
            {
                arrResponse[0] = "Unable to do recharge at this moment. Please contact administrator (Error Code : Validate Recharge Error) Status Code : " + rechargeResponse.StatusCode;
            }
            return arrResponse;
        }

        #endregion
    }
}