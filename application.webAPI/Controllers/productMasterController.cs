﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class productMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/productMaster
        public IQueryable<tblProduct_Master> getProductList()
        {
            return db.tblProduct_Master;
        }

        // GET: api/productMaster/5
        [Authorize]
        [ResponseType(typeof(tblProduct_Master))]
        [HttpGet]
        public IHttpActionResult getProductById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblProduct_Master tblProduct_Master = db.tblProduct_Master.Find(id);
                if (tblProduct_Master == null)
                {
                    return NotFound();
                }

                return Ok(tblProduct_Master);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(productMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getProductListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    productMasterViewModel objProduct = new productMasterViewModel();
                    objProduct.totalRecords = db.QryProductMaster.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objProduct.objProductList = db.QryProductMaster.Where(s => s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.ContactPersonName1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.ContactPersonName2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.EmailId2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Address1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Address2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Telephone2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Website.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.GSTNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objProduct.filterRecords = objProduct.objProductList.Count();
                    }
                    else
                        objProduct.filterRecords = objProduct.totalRecords;
                    if (sortDirection == "asc")
                        objProduct.objProductList = String.IsNullOrEmpty(searchValue) ?
                            db.QryProductMaster.OrderBy(s => s.ProductCode).ThenBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryProductMaster>() :
                            objProduct.objProductList.OrderBy(s => s.ProductCode).ThenBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryProductMaster>();
                    else
                        objProduct.objProductList = String.IsNullOrEmpty(searchValue) ?
                            db.QryProductMaster.OrderByDescending(s => s.ProductCode).ThenBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryProductMaster>() :
                            objProduct.objProductList.OrderByDescending(s => s.ProductCode).ThenBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryProductMaster>();

                    return Ok(objProduct);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/productMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateProduct(int id, tblProduct_Master objProduct)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objProduct.ProductId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var productInDb = db.tblProduct_Master.SingleOrDefault(p => p.ProductCode.Trim().ToLower() == objProduct.ProductCode.Trim().ToLower() && p.ProductId != objProduct.ProductId);
                        if (productInDb != null)
                            return BadRequest("Product Code already exists");

                        db.Entry(objProduct).State = EntityState.Modified;
                        db.Entry(objProduct).Property(x => x.CreatedBy).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesProductExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/productMaster
        [Authorize]
        [ResponseType(typeof(tblProduct_Master))]
        [HttpPost]
        public IHttpActionResult saveProduct(tblProduct_Master objProduct)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var productInDb = db.tblProduct_Master.SingleOrDefault(p => p.ProductCode.Trim().ToLower() == objProduct.ProductCode.Trim().ToLower() && p.ProductId != objProduct.ProductId);
                    if (productInDb != null)
                        return BadRequest("Product Code already exists");

                    objProduct.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    objProduct.CreatedDateTime = helper.getIndianTime();
                    db.tblProduct_Master.Add(objProduct);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/productMaster/5
        [Authorize]
        [ResponseType(typeof(tblProduct_Master))]
        [HttpDelete]
        public IHttpActionResult deleteProduct(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblProduct_Master objProduct = db.tblProduct_Master.Find(id);
                    if (objProduct == null)
                    {
                        return NotFound();
                    }

                    db.tblProduct_Master.Remove(objProduct);
                    db.SaveChanges();

                    return Ok(objProduct);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesProductExists(int id)
        {
            return db.tblProduct_Master.Count(e => e.ProductId == id) > 0;
        }
    }
}