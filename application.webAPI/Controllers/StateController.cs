﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class StateController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/State
        [Authorize]
        [ResponseType(typeof(stateViewModel))]
        [HttpGet]
        public IHttpActionResult getStateList(int id = 0, int countryId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                stateViewModel objStateviewModel = new stateViewModel();
                if (countryId == 0)
                {
                    objStateviewModel.objCountryMasterList = db.tblCountry_Master.ToList();
                    objStateviewModel.objStatusMasterList = db.tblStatus_Master.ToList();
                    objStateviewModel.objStateMaster = new tblState_Master();
                }
                else
                {
                    objStateviewModel.objStateMasterList = db.QryState.Where(s => s.CountryId == countryId).ToList();
                }

                return Ok(objStateviewModel);
            }
            else
            {
                return Unauthorized();
            }

        }

        [Authorize]
        [ResponseType(typeof(stateViewModel))]
        [HttpGet]
        public IHttpActionResult getStateByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    stateViewModel objStateViewModel = new stateViewModel();
                    objStateViewModel.totalRecords = db.tblState_Master.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objStateViewModel.objStateMasterList = db.QryState.Where(s => s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objStateViewModel.filterRecords = objStateViewModel.objStateMasterList.Count();
                    }
                    else
                        objStateViewModel.filterRecords = objStateViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objStateViewModel.objStateMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryState.OrderBy(s => s.StateName).Skip(pageIndex).Take(pageSize).ToList<QryState>() :
                            objStateViewModel.objStateMasterList.OrderBy(s => s.StateName).Skip(pageIndex).Take(pageSize).ToList<QryState>();
                    else
                        objStateViewModel.objStateMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryState.OrderByDescending(s => s.StateName).Skip(pageIndex).Take(pageSize).ToList<QryState>() :
                            objStateViewModel.objStateMasterList.OrderByDescending(s => s.StateName).Skip(pageIndex).Take(pageSize).ToList<QryState>();

                    return Ok(objStateViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }


        // GET: api/State/5
        [Authorize]
        [ResponseType(typeof(tblState_Master))]
        public IHttpActionResult getStateListById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblState_Master tblState_Master = db.tblState_Master.Find(id);
                    if (tblState_Master == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblState_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/State/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateState(int id, tblState_Master objState)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objState.StateId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var StateInBb = db.tblState_Master.SingleOrDefault(s => s.StateName.Trim().ToLower() == objState.StateName.Trim().ToLower() && s.StateId != objState.StateId && s.CountryId == id);
                        if (StateInBb != null)
                            return BadRequest("State already exits!");

                        db.Entry(objState).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblState_MasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/State
        [Authorize]
        [ResponseType(typeof(tblState_Master))]
        [HttpPost]
        public IHttpActionResult saveState(tblState_Master objState)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var StateInBb = db.tblState_Master.SingleOrDefault(s => s.StateName.Trim().ToLower() == objState.StateName.Trim().ToLower() && s.StateId != objState.StateId && s.CountryId == objState.CountryId);
                    if (StateInBb != null)
                        return BadRequest("State already exits!");
                    db.tblState_Master.Add(objState);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/State/5
        [Authorize]
        [ResponseType(typeof(tblState_Master))]
        [HttpDelete]
        public IHttpActionResult deleteState(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblState_Master tblState_Master = db.tblState_Master.Find(id);
                    if (tblState_Master == null)
                    {
                        return NotFound();
                    }

                    db.tblState_Master.Remove(tblState_Master);
                    db.SaveChanges();
                    return Ok(tblState_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblState_MasterExists(int id)
        {
            return db.tblState_Master.Count(e => e.StateId == id) > 0;
        }
    }
}