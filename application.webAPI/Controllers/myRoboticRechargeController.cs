﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.instantpay.utility.response;
using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using application.webAPI.viewModel;
using application.webAPI.myroboticrecharge.response;

namespace application.webAPI.Controllers
{
    public class myRoboticRechargeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/myRoboticRecharge
        [Authorize]
        [ResponseType(typeof(myRoboticRechargeVM))]
        [Route("api/myRoboticRecharge/getRechargeServiceList")]
        [HttpGet]
        public IHttpActionResult getRechargeServiceList(string serviceType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                myRoboticRechargeVM objRechargeVM = new myRoboticRechargeVM();
                if (entityType != helper.defaultCompany)
                {
                    objRechargeVM.objMyRoboticUtilityDataList = db.tblMyRobotic_Recharge_UtilityData.ToList();
                    objRechargeVM.objMyRoboticRechargeTransaction = new tblMyRobotic_Recharge_Transaction();
                    return Ok(objRechargeVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/myRoboticRecharge/5
        [Authorize]
        [ResponseType(typeof(string))]
        [Route("api/myRoboticRecharge/rechargeMobile")]
        public IHttpActionResult rechargeMobile(tblMyRobotic_Recharge_Transaction objRechargeTransaction)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {

                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    objRechargeTransaction.rechargedatetime = helper.getIndianTime();
                    objRechargeTransaction.EntityId = entityId;
                    objRechargeTransaction.EntityUserId = entityUserId;
                    var utilityServiceInDb = db.tblMyRobotic_Recharge_UtilityData.Where(x => x.MyRoboticRechargeDataId == objRechargeTransaction.MyRoboticRechargeDataId).SingleOrDefault();
                    if (utilityServiceInDb != null)
                    {
                        if (helper.hasEnoughBalance(entityId))
                        {
                            objRechargeTransaction.orderid = helper.getRandomNumericDateTimeValue();
                            objRechargeTransaction.OperatorCode = utilityServiceInDb.OperatorCode;
                            IRestResponse transactRechargeResponse = transactRecharge(ConfigurationManager.AppSettings["MyRoboticToken"], objRechargeTransaction.amount, objRechargeTransaction.OperatorCode, objRechargeTransaction.mobileno, objRechargeTransaction.orderid);
                            if (transactRechargeResponse.StatusCode == HttpStatusCode.OK)
                            {
                                myRoboticRechargeResponse objRechargeResponse = JsonConvert.DeserializeObject<myRoboticRechargeResponse>(transactRechargeResponse.Content);
                                objRechargeTransaction.rechargeRequest = "token=" + ConfigurationManager.AppSettings["MyRoboticToken"] + "&operatorcode=" + objRechargeTransaction.OperatorCode + "&number=" + objRechargeTransaction.mobileno + "&amount=" + objRechargeTransaction.amount.ToString() + "&orderid=" + objRechargeTransaction.orderid;
                                objRechargeTransaction.rechargeResponse = transactRechargeResponse.Content.ToString();
                                objRechargeTransaction.status = objRechargeResponse.status;
                                objRechargeTransaction.txid = objRechargeResponse.txid.ToString();
                                objRechargeTransaction.opid = objRechargeResponse.opid;
                                objRechargeTransaction.r_offer = objRechargeResponse.r_offer;
                                objRechargeTransaction.balance = (decimal)objRechargeResponse.balance;
                                objRechargeTransaction.robotic_id = objRechargeResponse.robotic_id.ToString();
                                objRechargeTransaction.lapu_no = objRechargeResponse.lapu_no;
                                db.tblMyRobotic_Recharge_Transaction.Add(objRechargeTransaction);
                                db.SaveChanges();
                                if (objRechargeResponse.status.Trim().ToUpper() == "SUCCESS")
                                {
                                    //Deducting Balance from wallet
                                    string description = utilityServiceInDb.OperatorName + " recharge";
                                    processAccounting(objRechargeTransaction.MyRoboticRechargeDataId, objRechargeTransaction.amount, entityId, entityUserId, objRechargeTransaction.opid, companyId, description);
                                    return Ok(objRechargeResponse.status);
                                }
                                else
                                {
                                    return BadRequest(objRechargeTransaction.status);
                                }
                            }
                            else
                            {
                                return BadRequest(transactRechargeResponse.Content.ToString());
                            }
                        }
                        else
                        {
                            return BadRequest(helper.entityLowBalance);
                        }

                    }
                    else
                    {
                        return BadRequest("Unable to do recharge at this moment. Please contact administrator (Error Code : UtilityData Missing)");
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        private Boolean processAccounting(long RechargeDataId, int transactionAmount, long entityId, long entityUserId, string clientRefId, int companyId, string description)
        {
            int productId = 5;
            tblProduct_Master objProductMaster = db.tblProduct_Master.SingleOrDefault(x => x.ProductId == productId);
            QryMyRobotic_Recharge_Default_Commission objDefaultCommission = db.QryMyRobotic_Recharge_Default_Commission.Where(x => x.IsCustom == false && x.CompanyId == companyId && x.MyRoboticRechargeDataId == RechargeDataId).SingleOrDefault();
            if (objProductMaster != null && objDefaultCommission != null)
            {
                if (objProductMaster.CommissionOn == "Net Amount")
                {
                    decimal TDSOnServiceCharge = (objProductMaster.TDSOnServiceCharge == null ? 0 : (decimal)objProductMaster.TDSOnServiceCharge);
                    decimal otherChargesByBank = (objProductMaster.OtherChargesByVendor == null ? 0 : (decimal)objProductMaster.OtherChargesByVendor);
                    decimal TDSOnCommission = objProductMaster.TDSRateOnCommission;
                    decimal agentCommission = objDefaultCommission.AgentCommissionValue;
                    decimal distcommission = objDefaultCommission.DistCommissionValue;
                    decimal mdCommission = objDefaultCommission.MDCommissionValue;
                    string distMDEntityIds = helper.getDistMDEntityIdByAgentId(entityId);
                    long distEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[0]);
                    long mdEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[1]);
                    mdCommission = mdCommission - distcommission;
                    distcommission = distcommission - agentCommission;

                    string walletUpdateStatus = "";
                    string transactStatus = "";

                    walletUpdateStatus = helper.updateWalletBalance(entityId, -transactionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, -transactionAmount, clientRefId, description, Convert.ToInt32(helper.transactionTypes.Booking), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;

                    // Calculating Service Charge Amount         
                    #region Agent Taxes and Commission
                    decimal agentCommissionAmount = transactionAmount * agentCommission;
                    if (agentCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(entityId, agentCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(entityId, agentCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;

                        decimal tdsOnAgentCommissionAmount = agentCommissionAmount * TDSOnCommission;

                        walletUpdateStatus = helper.updateWalletBalance(entityId, -tdsOnAgentCommissionAmount);

                        transactStatus = helper.transactEntityAccount(entityId, -tdsOnAgentCommissionAmount, clientRefId, "TDS On Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }
                    #endregion

                    // Distributor Commission & TDS
                    decimal distributorCommissionAmount = transactionAmount * distcommission;
                    if (distributorCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(distEntityId, distributorCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(distEntityId, distributorCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }

                    decimal distributorTDSOnCommissionAmount = distributorCommissionAmount * TDSOnCommission;
                    if (distributorTDSOnCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(distEntityId, -distributorTDSOnCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(distEntityId, -distributorTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }

                    //Master Distributor Commission & TDS
                    decimal MDCommissionAmount = transactionAmount * mdCommission;
                    if (MDCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(mdEntityId, MDCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(mdEntityId, MDCommissionAmount, clientRefId, "Commission for " + description, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }


                    decimal MDTDSOnCommissionAmount = MDCommissionAmount * TDSOnCommission;
                    if (MDTDSOnCommissionAmount > 0)
                    {
                        walletUpdateStatus = helper.updateWalletBalance(mdEntityId, -MDTDSOnCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(mdEntityId, -MDTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + description, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        protected IRestResponse transactRecharge(string token, int amount, string operatorCode, string mobileNo, string orderNo)
        {
            IRestResponse response;
            string URL = "&token=" + token + "&operatorcode=" + operatorCode + "&number=" + mobileNo + "&amount=" + amount + "&orderid=" + orderNo;
            response = helper.WebAPIClient_Via_RestSharp_4_MyRobotic_UtilityPayments(URL, Method.GET, null);
            return response;
        }

        // PUT: api/myRoboticRecharge/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PuttblMyRobotic_Recharge_Transaction(long id, tblMyRobotic_Recharge_Transaction tblMyRobotic_Recharge_Transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblMyRobotic_Recharge_Transaction.MyRoboticRechargeId)
            {
                return BadRequest();
            }

            db.Entry(tblMyRobotic_Recharge_Transaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblMyRobotic_Recharge_TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/myRoboticRecharge
        [ResponseType(typeof(tblMyRobotic_Recharge_Transaction))]
        public IHttpActionResult PosttblMyRobotic_Recharge_Transaction(tblMyRobotic_Recharge_Transaction tblMyRobotic_Recharge_Transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblMyRobotic_Recharge_Transaction.Add(tblMyRobotic_Recharge_Transaction);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tblMyRobotic_Recharge_Transaction.MyRoboticRechargeId }, tblMyRobotic_Recharge_Transaction);
        }

        // DELETE: api/myRoboticRecharge/5
        [ResponseType(typeof(tblMyRobotic_Recharge_Transaction))]
        public IHttpActionResult DeletetblMyRobotic_Recharge_Transaction(long id)
        {
            tblMyRobotic_Recharge_Transaction tblMyRobotic_Recharge_Transaction = db.tblMyRobotic_Recharge_Transaction.Find(id);
            if (tblMyRobotic_Recharge_Transaction == null)
            {
                return NotFound();
            }

            db.tblMyRobotic_Recharge_Transaction.Remove(tblMyRobotic_Recharge_Transaction);
            db.SaveChanges();

            return Ok(tblMyRobotic_Recharge_Transaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMyRobotic_Recharge_TransactionExists(long id)
        {
            return db.tblMyRobotic_Recharge_Transaction.Count(e => e.MyRoboticRechargeId == id) > 0;
        }
    }
}