﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class CountryController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/Country
        [Authorize]
        public List<tblCountry_Master> getCountry()
        {
            List<tblCountry_Master> objCountryList = new List<tblCountry_Master>();
            objCountryList = db.tblCountry_Master.ToList();
            return objCountryList;
        }

        [Authorize]
        [ResponseType(typeof(countryViewModel))]
        [HttpGet]
        public IHttpActionResult getCountryListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    countryViewModel opjCountryViewModel = new countryViewModel();
                    opjCountryViewModel.totalRecords = db.tblCountry_Master.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        opjCountryViewModel.objCountryMasterList = db.QryCountry.Where(s => s.CountryName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) || s.CountryCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        opjCountryViewModel.filterRecords = opjCountryViewModel.objCountryMasterList.Count();
                    }
                    else
                        opjCountryViewModel.filterRecords = opjCountryViewModel.totalRecords;
                    if (sortDirection == "asc")
                        opjCountryViewModel.objCountryMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCountry.OrderBy(s => s.CountryName).Skip(pageIndex).Take(pageSize).ToList<QryCountry>() :
                            opjCountryViewModel.objCountryMasterList.OrderBy(s => s.CountryName).Skip(pageIndex).Take(pageSize).ToList<QryCountry>();
                    else
                        opjCountryViewModel.objCountryMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCountry.OrderByDescending(s => s.CountryName).Skip(pageIndex).Take(pageSize).ToList<QryCountry>() :
                            opjCountryViewModel.objCountryMasterList.OrderByDescending(s => s.CountryName).Skip(pageIndex).Take(pageSize).ToList<QryCountry>();

                    return Ok(opjCountryViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/Country/5
        [Authorize]
        [ResponseType(typeof(tblCountry_Master))]
        [HttpGet]
        public IHttpActionResult getCountryListById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCountry_Master tblCountry_Master = db.tblCountry_Master.Find(id);
                    if (tblCountry_Master == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblCountry_Master);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/Country/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateCountry(int id, tblCountry_Master objCountry)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objCountry.CountryId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var countryInDb = db.tblCountry_Master.SingleOrDefault(c => c.CountryName == objCountry.CountryName && c.CountryId != objCountry.CountryId);
                        if (countryInDb != null)
                            return BadRequest("Country name already exists");

                        db.Entry(objCountry).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesCountryExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/Country
        [Authorize]
        [ResponseType(typeof(tblCountry_Master))]
        [HttpPost]
        public IHttpActionResult saveCountry(tblCountry_Master objCountry)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var countryInDb = db.tblCountry_Master.SingleOrDefault(c => c.CountryName == objCountry.CountryName && c.CountryId != objCountry.CountryId);
                    if (countryInDb != null)
                        return BadRequest("Country name already exists");

                    db.tblCountry_Master.Add(objCountry);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/Country/5
        [Authorize]
        [ResponseType(typeof(tblCountry_Master))]
        [HttpDelete]
        public IHttpActionResult deleteCountry(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCountry_Master objCountry = db.tblCountry_Master.Find(id);
                    if (objCountry == null)
                    {
                        return NotFound();
                    }
                    db.tblCountry_Master.Remove(objCountry);
                    db.SaveChanges();
                    return Ok(objCountry);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesCountryExists(int id)
        {
            return db.tblCountry_Master.Count(e => e.CountryId == id) > 0;
        }
    }
}