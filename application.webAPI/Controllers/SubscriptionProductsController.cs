﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class SubscriptionProductsController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/SubscriptionProducts
        [Authorize]
        [ResponseType(typeof(subscriptionProductViewModel))]
        [HttpGet]
        public IHttpActionResult getSubscriptionProductsList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subscriptionProductViewModel objsubscriptionProductViewModel = new subscriptionProductViewModel()
                    {
                        objSubscriptionProduct = new tblSubscription_Products(),
                        objProductList = db.tblProduct_Master.ToList(),
                        objStatusList = db.tblStatus_Master.ToList(),
                        objSubscriptionList = db.tblSubscription_Master.ToList()
                    };
                    return Ok(objsubscriptionProductViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/SubscriptionProducts/5
        [Authorize]
        [ResponseType(typeof(tblSubscription_Products))]
        public IHttpActionResult getSubscriptionProductsById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblSubscription_Products tblSubscription_Products = db.tblSubscription_Products.Find(id);
                    if (tblSubscription_Products == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblSubscription_Products);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(subscriptionProductViewModel))]
        [HttpGet]
        public IHttpActionResult getsubscriptionProductListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection, int subScriptionId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subscriptionProductViewModel objsubscriptionProductViewModel = new subscriptionProductViewModel();
                    objsubscriptionProductViewModel.totalRecords = db.QrySubscriptionProduct.Where(s => s.SubscriptionId == subScriptionId || s.SubscriptionId == 0).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objsubscriptionProductViewModel.objSubscriptionProductList = db.QrySubscriptionProduct.Where(s => s.SubscriptionId == subScriptionId &&
                        (s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.SubscriptionName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objsubscriptionProductViewModel.filterRecords = objsubscriptionProductViewModel.objSubscriptionProductList.Count();
                    }
                    else
                        objsubscriptionProductViewModel.filterRecords = objsubscriptionProductViewModel.totalRecords;

                    if (sortDirection == "asc")
                        objsubscriptionProductViewModel.objSubscriptionProductList = String.IsNullOrEmpty(searchValue) ?
                            db.QrySubscriptionProduct.Where(s => s.SubscriptionId == subScriptionId || s.SubscriptionId == 0).OrderBy(s => s.SubscriptionName).ThenBy(s => s.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QrySubscriptionProduct>() :
                            objsubscriptionProductViewModel.objSubscriptionProductList.Where(s => s.SubscriptionId == subScriptionId || s.SubscriptionId == 0).OrderBy(s => s.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QrySubscriptionProduct>();
                    else
                        objsubscriptionProductViewModel.objSubscriptionProductList = String.IsNullOrEmpty(searchValue) ?
                           db.QrySubscriptionProduct.Where(s => s.SubscriptionId == subScriptionId || s.SubscriptionId == 0).OrderByDescending(s => s.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QrySubscriptionProduct>() :
                           objsubscriptionProductViewModel.objSubscriptionProductList.Where(s => s.SubscriptionId == subScriptionId || s.SubscriptionId == 0).OrderByDescending(s => s.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QrySubscriptionProduct>();

                    return Ok(objsubscriptionProductViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/SubscriptionProducts/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateSubscriptionProducts(int id, tblSubscription_Products objSubscriptionProducts)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objSubscriptionProducts.SubscriptionProductId)
                {
                    return BadRequest();
                }
                db.Entry(objSubscriptionProducts).State = EntityState.Modified;
                try
                {
                    var SubscriptionProductsInDb = db.tblSubscription_Products.SingleOrDefault(s => s.ProductId == objSubscriptionProducts.ProductId && s.SubscriptionId == objSubscriptionProducts.SubscriptionId && s.SubscriptionProductId != objSubscriptionProducts.SubscriptionProductId);
                    if (SubscriptionProductsInDb != null)
                        return BadRequest("SubscriptionProduct already exists");
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblSubscription_ProductsExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok(objSubscriptionProducts);
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/SubscriptionProducts
        [Authorize]
        [ResponseType(typeof(tblSubscription_Products))]
        [HttpPost]
        public IHttpActionResult saveSubscriptionProduct(tblSubscription_Products objSubscriptionProducts)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var SubscriptionProductsInDb = db.tblSubscription_Products.SingleOrDefault(s => s.ProductId == objSubscriptionProducts.ProductId && s.SubscriptionId == objSubscriptionProducts.SubscriptionId && s.SubscriptionProductId != objSubscriptionProducts.SubscriptionProductId);
                    if (SubscriptionProductsInDb != null)
                        return BadRequest("Subscription Product already exists");

                    db.tblSubscription_Products.Add(objSubscriptionProducts);
                    db.SaveChanges();
                    return Ok(objSubscriptionProducts);
                }
                else
                    return Unauthorized();

            }
            else
                return Unauthorized();
        }

        // DELETE: api/SubscriptionProducts/5
        [Authorize]
        [ResponseType(typeof(tblSubscription_Products))]
        [HttpDelete]
        public IHttpActionResult deleteSubScriptionProduct(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblSubscription_Products tblSubscription_Products = db.tblSubscription_Products.Find(id);
                    if (tblSubscription_Products == null)
                    {
                        return NotFound();
                    }
                    db.tblSubscription_Products.Remove(tblSubscription_Products);
                    db.SaveChanges();
                    return Ok(tblSubscription_Products);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSubscription_ProductsExists(int id)
        {
            return db.tblSubscription_Products.Count(e => e.SubscriptionProductId == id) > 0;
        }
    }
}