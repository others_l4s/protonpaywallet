﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class roleController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/role
        [Authorize]
        [ResponseType(typeof(roleViewModel))]
        [HttpGet]
        public IHttpActionResult getRoleList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    roleViewModel objRoleViewModel = new roleViewModel
                    {
                        objRole = new tblRole_Master(),
                        objStatusList = db.tblStatus_Master.ToList()
                    };
                    return Ok(objRoleViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [HttpGet]
        [Route("api/role/getrolebycompanyid")]
        public List<tblRole_Master> getRoleByCompanyId(int companyId)
        {
            return db.tblRole_Master.Where(r => r.CompanyId == companyId).OrderBy(r => r.RoleName).ToList();
        }

        // GET: api/role/5
        [Authorize]
        [ResponseType(typeof(tblRole_Master))]
        public IHttpActionResult getRoleById(int id)
        {
            tblRole_Master tblRole_Master = db.tblRole_Master.Find(id);
            if (tblRole_Master == null)
            {
                return NotFound();
            }

            return Ok(tblRole_Master);
        }


        [Authorize]
        [ResponseType(typeof(roleViewModel))]
        [HttpGet]
        public IHttpActionResult getRoleListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                    roleViewModel objRoleViewModel = new roleViewModel();
                    objRoleViewModel.totalRecords = db.tblRole_Master.Where(r => r.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objRoleViewModel.objRoleList = db.QryRole.Where(s => s.CompanyId == CompanyId && (s.RoleName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) || s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();
                        objRoleViewModel.filterRecords = objRoleViewModel.objRoleList.Count();
                    }
                    else
                        objRoleViewModel.filterRecords = objRoleViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objRoleViewModel.objRoleList = String.IsNullOrEmpty(searchValue) ?
                            db.QryRole.Where(s => s.CompanyId == CompanyId).OrderBy(s => s.RoleName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryRole>() :
                            objRoleViewModel.objRoleList.OrderBy(s => s.RoleName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryRole>();
                    else
                        objRoleViewModel.objRoleList = String.IsNullOrEmpty(searchValue) ?
                            db.QryRole.Where(s => s.CompanyId == CompanyId).OrderByDescending(s => s.RoleName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryRole>() :
                            objRoleViewModel.objRoleList.OrderByDescending(s => s.RoleName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryRole>();

                    return Ok(objRoleViewModel);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // PUT: api/role/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateRole(int id, tblRole_Master objRole)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {

                    if (id != objRole.RoleId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        int CompanyId = Convert.ToInt32(identityCompanyId.Value);

                        var roleInDb = db.tblRole_Master.SingleOrDefault(r => r.RoleName.Trim().ToLower() == objRole.RoleName.Trim().ToLower() && r.CompanyId == CompanyId && r.RoleId != id);
                        if (roleInDb != null)
                            return BadRequest("Role Name already exists for selected Company");

                        db.Entry(objRole).State = EntityState.Modified;
                        db.Entry(objRole).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objRole).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesRoleExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/role
        [Authorize]
        [ResponseType(typeof(tblRole_Master))]
        [HttpPost]
        public IHttpActionResult saveRole(tblRole_Master objRole)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(identityAdminLoginId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    var roleInDb = db.tblRole_Master.SingleOrDefault(r => r.RoleName.Trim().ToLower() == objRole.RoleName.Trim().ToLower() && r.CompanyId == CompanyId && r.RoleId != objRole.RoleId);
                    if (roleInDb != null)
                        return BadRequest("Role Name already exists for selected Company");

                    objRole.CompanyId = CompanyId;
                    objRole.CreatedBy = adminLoginId;
                    db.tblRole_Master.Add(objRole);
                    db.SaveChanges();
                    return Ok(objRole);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/role/5
        [Authorize]
        [ResponseType(typeof(tblRole_Master))]
        [HttpDelete]
        public IHttpActionResult deleteRole(int id)
        {
            tblRole_Master objRole = db.tblRole_Master.Find(id);
            if (objRole == null)
            {
                return NotFound();
            }

            db.tblRole_Master.Remove(objRole);
            db.SaveChanges();
            return Ok(objRole);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesRoleExists(int id)
        {
            return db.tblRole_Master.Count(e => e.RoleId == id) > 0;
        }
    }
}