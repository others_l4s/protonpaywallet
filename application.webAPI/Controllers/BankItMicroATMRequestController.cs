﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.bankit.microatm.request;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class BankItMicroATMRequestController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/BankItMicroATMRequest
        [Authorize]
        [ResponseType(typeof(bankITMicroATMDbRequest))]
        [Route("api/getBankITMicroATMRequest")]
        [HttpGet]
        public IHttpActionResult getBankITMicroATMRequest()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                bankITMicroATMDbRequest objMicroATMRequest = new bankITMicroATMDbRequest();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var objMicroATMRequestInDb = db.tblBankIt_MicroATM_Request.Where(x => x.EntityId == entityId).SingleOrDefault();
                    objMicroATMRequest = new bankITMicroATMDbRequest();
                    objMicroATMRequest.EntityId = entityId;
                    objMicroATMRequest.AccessAmount = Convert.ToInt32(ConfigurationManager.AppSettings["MicroATMJoiningAmount"]);
                    if (objMicroATMRequestInDb != null)
                    {
                        objMicroATMRequest.partnerAgentId = objMicroATMRequestInDb.partnerAgentId;
                        objMicroATMRequest.RequestDateTime = objMicroATMRequestInDb.RequestDateTime;
                        objMicroATMRequest.ApprovedDateTime = objMicroATMRequestInDb.ApprovedDateTime;
                        objMicroATMRequest.agent_id = ConfigurationManager.AppSettings["BankITMaster_AgendId"];
                        objMicroATMRequest.mobileNo = ConfigurationManager.AppSettings["BankITMaster_MobileNo"];
                        objMicroATMRequest.developer_id = ConfigurationManager.AppSettings["BankITMaster_AgentAuthId"];
                        objMicroATMRequest.password = ConfigurationManager.AppSettings["BankITMaster_AgentAuthPwd"];
                    }
                    return Ok(objMicroATMRequest);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/BankItMicroATMRequest/5
        [Authorize]
        [ResponseType(typeof(BankItMicroATMRequestVM))]
        [Route("api/getBnkITMicroATMRequestQueue")]
        [HttpGet]
        public IHttpActionResult getBnkITMicroATMRequestQueue(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    BankItMicroATMRequestVM objBankItMicroATMRequestVM = new BankItMicroATMRequestVM();
                    objBankItMicroATMRequestVM.objBankItMicroATMRequestList = db.QryBankITMicroATM_Request.Where(x => x.CompanyId == companyId && (x.ApprovedDateTime == null || x.IsRejected == null)).ToList();
                    objBankItMicroATMRequestVM.totalRecords = objBankItMicroATMRequestVM.objBankItMicroATMRequestList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objBankItMicroATMRequestVM.objBankItMicroATMRequestList = objBankItMicroATMRequestVM.objBankItMicroATMRequestList.Where(x => x.AgencyName.Trim().ToLower() == searchValue.Trim().ToLower()
                        || x.OfficeAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.OfficeAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.Landmark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.CityName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objBankItMicroATMRequestVM.filterRecords = objBankItMicroATMRequestVM.objBankItMicroATMRequestList.Count();
                    }
                    else
                        objBankItMicroATMRequestVM.filterRecords = objBankItMicroATMRequestVM.totalRecords;

                    if (sortDirection == "asc")
                        objBankItMicroATMRequestVM.objBankItMicroATMRequestList = objBankItMicroATMRequestVM.objBankItMicroATMRequestList.OrderBy(s => s.BankItMicroATMId).Skip(pageIndex).Take(pageSize).ToList<QryBankITMicroATM_Request>();
                    else
                        objBankItMicroATMRequestVM.objBankItMicroATMRequestList = objBankItMicroATMRequestVM.objBankItMicroATMRequestList.OrderByDescending(s => s.BankItMicroATMId).Skip(pageIndex).Take(pageSize).ToList<QryBankITMicroATM_Request>();

                    return Ok(objBankItMicroATMRequestVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/BankItMicroATMRequest/5
        [Authorize]
        [ResponseType(typeof(void))]
        //[Route("api/BankITAEPSResponseSave")]
        [HttpPut]
        public IHttpActionResult BankItMicroATMRequestUpdate(long id, tblBankIt_MicroATM_Request objRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objRequest.BankItMicroATMId)
                    {
                        return BadRequest();
                    }

                    objRequest.ApprovedDateTime = helper.getIndianTime();
                    objRequest.IsRejected = false;
                    objRequest.Remark = "Successfully Approved & Activated";
                    db.Entry(objRequest).State = EntityState.Modified;
                    db.Entry(objRequest).Property(x => x.RequestDateTime).IsModified = false;
                    db.Entry(objRequest).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objRequest).Property(x => x.AccessAmount).IsModified = false;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblBankIt_MicroATM_RequestExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
                return Unauthorized();
        }

        // POST: api/BankItMicroATMRequest
        [Authorize]
        [ResponseType(typeof(tblBankIt_AEPS_Request))]
        [Route("api/BankITMicroATMRequestSave")]
        [HttpPost]
        public IHttpActionResult BankITMicroATMRequestSave(tblBankIt_MicroATM_Request objRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    //Checking where Retailer has added KYE Details
                    var entityKYCAdded = db.tblEntity_KYC.Where(x => x.EntityId == entityId &&
                    String.IsNullOrEmpty(x.PanCardNo) == false &&
                    String.IsNullOrEmpty(x.PanCardPhoto) == false && String.IsNullOrEmpty(x.PanCardPhoto2) == false).SingleOrDefault();
                    if (entityKYCAdded == null)
                        return BadRequest("Please complete your KYC Details by adding Pan Card No. and upload Pan Card photo of both sides.");
                    if (helper.hasEnoughBalance(entityId))
                    {
                        var microATMRequestInDb = db.tblBankIt_MicroATM_Request.Where(x => x.EntityId == entityId).Take(1).SingleOrDefault();
                        if (microATMRequestInDb == null)
                        {
                            string walletUpdateStatus = helper.updateWalletBalance(entityId, -objRequest.AccessAmount);
                            string transactStatus = helper.transactEntityAccount(entityId, -objRequest.AccessAmount, "", "Micro ATM Activation Amount", Convert.ToInt32(helper.transactionTypes.Booking), false, 2, entityUserId, objRequest.AccessAmount);
                            if (walletUpdateStatus == "success" && transactStatus == "success")
                            {
                                objRequest.EntityId = entityId;
                                objRequest.RequestDateTime = helper.getIndianTime();
                                objRequest.ApprovedDateTime = helper.getIndianTime();
                                objRequest.partnerAgentId = helper.GetRandomPasswordUsingGUID(8).ToUpper();
                                objRequest.Remark = "Successfully Approved & Activated";
                                db.tblBankIt_MicroATM_Request.Add(objRequest);
                                db.SaveChanges();
                            }
                            else
                            {
                                return BadRequest(walletUpdateStatus + " " + transactStatus);
                            }
                        }
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Oops! Wallet Balance is too Low");
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblBankIt_MicroATM_RequestExists(long id)
        {
            return db.tblBankIt_MicroATM_Request.Count(e => e.BankItMicroATMId == id) > 0;
        }
    }
}