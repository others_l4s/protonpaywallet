﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class StatusController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/Status
        [Authorize]
        [HttpGet]
        public List<tblStatus_Master> GettblStatus_Master()
        {
            return db.tblStatus_Master.ToList();
        }

        [Authorize]
        [ResponseType(typeof(statusViewModel))]
        [HttpGet]
        public IHttpActionResult getStatusByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    statusViewModel objStatusViewModel = new statusViewModel();
                    objStatusViewModel.totalRecords = db.tblStatus_Master.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objStatusViewModel.objStatusMasterList = db.tblStatus_Master.Where(s => s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objStatusViewModel.filterRecords = objStatusViewModel.objStatusMasterList.Count();
                    }
                    else
                        objStatusViewModel.filterRecords = objStatusViewModel.totalRecords;

                    if (sortDirection == "asc")
                        objStatusViewModel.objStatusMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.tblStatus_Master.OrderBy(s => s.StatusName).Skip(pageIndex).Take(pageSize).ToList<tblStatus_Master>() :
                            objStatusViewModel.objStatusMasterList.OrderBy(s => s.StatusName).Skip(pageIndex).Take(pageSize).ToList<tblStatus_Master>();
                    else
                        objStatusViewModel.objStatusMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.tblStatus_Master.OrderByDescending(s => s.StatusName).Skip(pageIndex).Take(pageSize).ToList<tblStatus_Master>() :
                            objStatusViewModel.objStatusMasterList.OrderByDescending(s => s.StatusName).Skip(pageIndex).Take(pageSize).ToList<tblStatus_Master>();

                    return Ok(objStatusViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/Status/5
        [Authorize]
        [ResponseType(typeof(tblStatus_Master))]
        [HttpGet]
        public IHttpActionResult GettblStatus_Master(int id)
        {
            tblStatus_Master objStatus = db.tblStatus_Master.Find(id);
            if (objStatus == null)
            {
                return NotFound();
            }

            return Ok(objStatus);
        }

        // PUT: api/Status/5

        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PuttblStatus_Master(int id, tblStatus_Master objStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objStatus.StatusId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var statusInBb = db.tblStatus_Master.SingleOrDefault(s => s.StatusName.Trim().ToLower() == objStatus.StatusName.Trim().ToLower() && s.StatusId != id);
                        if (statusInBb != null)
                            return BadRequest("Status Name already exists!");

                        db.Entry(objStatus).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblStatus_MasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else { return Unauthorized(); }
            }
            else
            {
                return Unauthorized();
            }

        }

        // POST: api/Status
        [Authorize]
        [ResponseType(typeof(tblStatus_Master))]
        [HttpPost]
        public IHttpActionResult PosttblStatus_Master(tblStatus_Master objStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var statusInBb = db.tblStatus_Master.SingleOrDefault(s => s.StatusName.Trim().ToLower() == objStatus.StatusName.Trim().ToLower() && s.StatusId != objStatus.StatusId);
                    if (statusInBb != null)
                        return BadRequest("Status Name already exists!");
                    db.tblStatus_Master.Add(objStatus);
                    db.SaveChanges();
                    //return CreatedAtRoute("DefaultApi", new { id = objstatus.StatusId }, objstatus);
                    return Ok();
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();

        }

        // DELETE: api/Status/5
        [Authorize]
        [ResponseType(typeof(tblStatus_Master))]
        [HttpDelete]
        public IHttpActionResult DeletetblStatus_Master(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblStatus_Master objStatus = db.tblStatus_Master.Find(id);
                    if (objStatus == null)
                    {
                        return NotFound();
                    }
                    db.tblStatus_Master.Remove(objStatus);
                    db.SaveChanges();
                    return Ok(objStatus);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblStatus_MasterExists(int id)
        {
            return db.tblStatus_Master.Count(e => e.StatusId == id) > 0;
        }
    }
}
