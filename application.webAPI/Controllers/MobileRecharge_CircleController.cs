﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class MobileRecharge_CircleController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/MobileRecharge_Circle/5
        [Authorize]
        public IHttpActionResult getMobileRechargeCirclebyId(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_Circle objMobileRecharge_Circle = db.tblMobileRecharge_Circle.Find(id);
                    if (objMobileRecharge_Circle == null)
                    {
                        return NotFound();
                    }
                    return Ok(objMobileRecharge_Circle);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/MobileRecharge_Circle/5
        [Authorize]
        [HttpPut]
        public IHttpActionResult updateMobileRechargeCircle(long id, tblMobileRecharge_Circle objMobileRecharge_Circle)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objMobileRecharge_Circle.CircleId)
                    {
                        return BadRequest();
                    }

                    var circleInDb = db.tblMobileRecharge_Circle.SingleOrDefault(x => x.CircleId != id && x.CircleName == objMobileRecharge_Circle.CircleName);
                    if (circleInDb != null)
                        return BadRequest("Circle Name already found.");

                    db.Entry(objMobileRecharge_Circle).State = EntityState.Modified;
                    db.Entry(objMobileRecharge_Circle).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(objMobileRecharge_Circle).Property(x => x.OperatorId).IsModified = false;

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblMobileRecharge_CircleExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        // POST: api/MobileRecharge_Circle
        [Authorize]
        [HttpPost]
        public IHttpActionResult saveMobileRechargeCircle(tblMobileRecharge_Circle objMobileRecharge_Circle)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var circleInDb = db.tblMobileRecharge_Circle.SingleOrDefault(x => x.CircleId != objMobileRecharge_Circle.CircleId && x.CircleName == objMobileRecharge_Circle.CircleName);
                    if (circleInDb != null)
                        return BadRequest("Circle Name already found.");

                    objMobileRecharge_Circle.CreatedDateTime = helper.getIndianTime();
                    db.tblMobileRecharge_Circle.Add(objMobileRecharge_Circle);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(mobileRechargeCircleVM))]
        [HttpGet]
        public IHttpActionResult getCircleList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeCircleVM objMobileRechargeCircleVM = new mobileRechargeCircleVM();
                    objMobileRechargeCircleVM.objMobileRechargeCircleList = db.tblMobileRecharge_Circle.ToList();
                    objMobileRechargeCircleVM.totalRecords = objMobileRechargeCircleVM.objMobileRechargeCircleList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMobileRechargeCircleVM.objMobileRechargeCircleList = objMobileRechargeCircleVM.objMobileRechargeCircleList.Where(s => s.CircleName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objMobileRechargeCircleVM.filterRecords = objMobileRechargeCircleVM.objMobileRechargeCircleList.Count();
                    }
                    else
                        objMobileRechargeCircleVM.filterRecords = objMobileRechargeCircleVM.totalRecords;

                    if (sortDirection == "asc")
                        objMobileRechargeCircleVM.objMobileRechargeCircleList = objMobileRechargeCircleVM.objMobileRechargeCircleList.OrderBy(s => s.CircleName).Skip(pageIndex).Take(pageSize).ToList<tblMobileRecharge_Circle>();
                    else
                        objMobileRechargeCircleVM.objMobileRechargeCircleList = objMobileRechargeCircleVM.objMobileRechargeCircleList.OrderByDescending(s => s.CircleName).Skip(pageIndex).Take(pageSize).ToList<tblMobileRecharge_Circle>();

                    return Ok(objMobileRechargeCircleVM);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // DELETE: api/MobileRecharge_Circle/5
        [Authorize]
        [HttpDelete]
        public IHttpActionResult deleteMobileRechargeCircle(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_Circle objMobileRecharge_Circle = db.tblMobileRecharge_Circle.Find(id);
                    if (objMobileRecharge_Circle == null)
                    {
                        return NotFound();
                    }
                    db.tblMobileRecharge_Circle.Remove(objMobileRecharge_Circle);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMobileRecharge_CircleExists(long id)
        {
            return db.tblMobileRecharge_Circle.Count(e => e.CircleId == id) > 0;
        }
    }
}