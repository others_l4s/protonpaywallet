﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class BookingStatusController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/BookingStatus
        [Authorize]
        [ResponseType(typeof(bookingStatusViewModel))]
        [HttpGet]
        public IHttpActionResult getBookingStatus()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                bookingStatusViewModel bookingStatusViewModel = new bookingStatusViewModel()
                {
                    objBookingStatusMaster = new tblBookingStatus_Master(),
                    objStatusMasterList = db.tblStatus_Master.ToList()
                };

                return Ok(bookingStatusViewModel);
            }
            else
            {
                return Unauthorized();
            }
        }


        // GET: api/BookingStatus/5
        [Authorize]
        [ResponseType(typeof(tblBookingStatus_Master))]
        public IHttpActionResult getBookingStatusById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblBookingStatus_Master objBookingStatus = db.tblBookingStatus_Master.Find(id);
                if (objBookingStatus == null)
                {
                    return NotFound();
                }

                return Ok(objBookingStatus);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(bookingStatusViewModel))]
        [HttpGet]
        public IHttpActionResult getBookingStatusPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                bookingStatusViewModel objBookingStatusViewModel = new bookingStatusViewModel();
                objBookingStatusViewModel.totalRecords = db.tblBookingStatus_Master.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objBookingStatusViewModel.objBookingStatusMasterList = db.QryBookingStatus.Where(b => b.BookingStatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                    objBookingStatusViewModel.filterRecords = objBookingStatusViewModel.objBookingStatusMasterList.Count();
                }
                else
                    objBookingStatusViewModel.filterRecords = objBookingStatusViewModel.totalRecords;
                if (sortDirection == "asc")
                    objBookingStatusViewModel.objBookingStatusMasterList = String.IsNullOrEmpty(searchValue) ?
                        db.QryBookingStatus.OrderBy(b => b.BookingStatusName).Skip(pageIndex).Take(pageSize).ToList<QryBookingStatus>() :
                        objBookingStatusViewModel.objBookingStatusMasterList.OrderBy(b => b.BookingStatusName).Skip(pageIndex).Take(pageSize).ToList<QryBookingStatus>();
                else
                    objBookingStatusViewModel.objBookingStatusMasterList = String.IsNullOrEmpty(searchValue) ?
                        db.QryBookingStatus.OrderByDescending(b => b.BookingStatusName).Skip(pageIndex).Take(pageSize).ToList<QryBookingStatus>() :
                        objBookingStatusViewModel.objBookingStatusMasterList.OrderByDescending(b => b.BookingStatusName).Skip(pageIndex).Take(pageSize).ToList<QryBookingStatus>();

                return Ok(objBookingStatusViewModel);
            }
            else
                return Unauthorized();
        }

        // PUT: api/BookingStatus/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateBookingStatus(int id, tblBookingStatus_Master objBookingStatus)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objBookingStatus.BookingStatusId)
                {
                    return BadRequest();
                }
                try
                {
                    var BookingStatusInBb = db.tblBookingStatus_Master.SingleOrDefault(b => b.BookingStatusName.Trim().ToLower() == objBookingStatus.BookingStatusName.Trim().ToLower() && b.BookingStatusId != objBookingStatus.BookingStatusId);
                    if (BookingStatusInBb != null)
                        return BadRequest("BookingStatusName already exits!");

                    db.Entry(objBookingStatus).State = EntityState.Modified;
                    db.Entry(objBookingStatus).Property(x => x.CreatedBy).IsModified = false;
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblBookingStatus_MasterExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok();
            }
            else
                return Unauthorized();
        }

        // POST: api/BookingStatus
        [Authorize]
        [ResponseType(typeof(tblBookingStatus_Master))]
        public IHttpActionResult saveBookingStatus(tblBookingStatus_Master objBookingStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "AdminLoginId").Single();
                    var BookingStatusInBb = db.tblBookingStatus_Master.SingleOrDefault(b => b.BookingStatusName.Trim().ToLower() == objBookingStatus.BookingStatusName.Trim().ToLower() && b.BookingStatusId != objBookingStatus.BookingStatusId);
                    if (BookingStatusInBb != null)
                        return BadRequest("BookingStatusName already exits!");

                    objBookingStatus.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    db.tblBookingStatus_Master.Add(objBookingStatus);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/BookingStatus/5
        [Authorize]
        [ResponseType(typeof(tblBookingStatus_Master))]
        public IHttpActionResult deleteBookingStatus(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblBookingStatus_Master tblBookingStatus_Master = db.tblBookingStatus_Master.Find(id);
                if (tblBookingStatus_Master == null)
                {
                    return NotFound();
                }

                db.tblBookingStatus_Master.Remove(tblBookingStatus_Master);
                db.SaveChanges();

                return Ok(tblBookingStatus_Master);
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblBookingStatus_MasterExists(int id)
        {
            return db.tblBookingStatus_Master.Count(e => e.BookingStatusId == id) > 0;
        }
    }
}