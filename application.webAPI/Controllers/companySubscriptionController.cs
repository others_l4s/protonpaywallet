﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class companySubscriptionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/companySubscription
        [Authorize]
        [ResponseType(typeof(companySubscriptionViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanySubscriptionData()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    companySubscriptionViewModel objCompanySubscriptionVM = new companySubscriptionViewModel()
                    {
                        objCompanySubscription = new tblCompany_Subscription(),
                        objCompanySubscriptionList = db.QryCompany_Subscription.OrderByDescending(c => c.CreatedDateTime).ToList(),
                        objCompanyList = db.tblCompany_Master.Where(c => c.IsDefaultCompany == false && c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CompanyName).ToList(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objSubscriptionList = db.QrySubscriptionProduct.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.SubscriptionName).ToList()
                    };
                    return Ok(objCompanySubscriptionVM);
                }
                else
                    return Unauthorized();
            }
            else
                return Unauthorized();
        }

        // GET: api/companySubscription/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Subscription))]
        [HttpGet]
        public IHttpActionResult getCompanySubscriptionById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Subscription objCompanySubscription = db.tblCompany_Subscription.Find(id);
                    if (objCompanySubscription == null)
                    {
                        return NotFound();
                    }
                    return Ok(objCompanySubscription);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(companySubscriptionViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanySubscriptionListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    companySubscriptionViewModel objCompanySubscriptionVM = new companySubscriptionViewModel();
                    objCompanySubscriptionVM.totalRecords = db.tblCompany_Subscription.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCompanySubscriptionVM.objCompanySubscriptionList = db.QryCompany_Subscription.Where(s => s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.SubscriptionName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objCompanySubscriptionVM.filterRecords = objCompanySubscriptionVM.objCompanySubscriptionList.Count();
                    }
                    else
                        objCompanySubscriptionVM.filterRecords = objCompanySubscriptionVM.totalRecords;
                    if (sortDirection == "asc")
                        objCompanySubscriptionVM.objCompanySubscriptionList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompany_Subscription.OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany_Subscription>() :
                            objCompanySubscriptionVM.objCompanySubscriptionList.OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany_Subscription>();
                    else
                        objCompanySubscriptionVM.objCompanySubscriptionList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompany_Subscription.OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany_Subscription>() :
                            objCompanySubscriptionVM.objCompanySubscriptionList.OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany_Subscription>();

                    return Ok(objCompanySubscriptionVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/companySubscription/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateCompanySubscription(int id, tblCompany_Subscription objCompanySubscription)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objCompanySubscription.CompanySubscriptionId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var objCompanySubscriptionInDb = db.tblCompany_Subscription.SingleOrDefault(c => c.CompanyId == objCompanySubscription.CompanyId && c.SubscriptionId == objCompanySubscription.SubscriptionId && c.CompanySubscriptionId != id);
                        if (objCompanySubscriptionInDb != null)
                            return BadRequest("Company Subscription already exists");

                        db.Entry(objCompanySubscription).State = EntityState.Modified;
                        db.Entry(objCompanySubscription).Property(x => x.CreatedBy).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblCompany_SubscriptionExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/companySubscription
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult saveCompanySubscription(tblCompany_Subscription objCompanySubscription)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLogin = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var objCompanySubscriptionInDb = db.tblCompany_Subscription.SingleOrDefault(c => c.CompanyId == objCompanySubscription.CompanyId && c.SubscriptionId == objCompanySubscription.SubscriptionId && c.CompanySubscriptionId != objCompanySubscription.CompanySubscriptionId);
                    if (objCompanySubscriptionInDb != null)
                        return BadRequest("Company Subscription already exists");

                    objCompanySubscription.CreatedBy = Convert.ToInt16(identityAdminLogin.Value);
                    db.tblCompany_Subscription.Add(objCompanySubscription);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/companySubscription/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Subscription))]
        public IHttpActionResult deleteCompany(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Subscription objCompanySubscription = db.tblCompany_Subscription.Find(id);
                    if (objCompanySubscription == null)
                    {
                        return NotFound();
                    }
                    db.tblCompany_Subscription.Remove(objCompanySubscription);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblCompany_SubscriptionExists(int id)
        {
            return db.tblCompany_Subscription.Count(e => e.CompanySubscriptionId == id) > 0;
        }
    }
}