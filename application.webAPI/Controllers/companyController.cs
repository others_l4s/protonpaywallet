﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class companyController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/company
        [Authorize]
        [ResponseType(typeof(companyViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanyList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    companyViewModel objCompanyViewModel = new companyViewModel()
                    {
                        objCompany = new tblCompany_Master(),
                        objCompanyUser = new tblAdminLogin(),
                        objCompanyList = (companyId == helper.defaultCompany ? db.QryCompany.OrderByDescending(c => c.CreatedDateTime).ToList() : db.QryCompany.Where(c => c.CompanyId == companyId).OrderByDescending(c => c.CreatedDateTime).ToList()),
                        objCompanyTypeList = db.tblCompany_Type.OrderBy(c => c.CompanyTypeName).ToList(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objBankAccountTypeList = db.tblBankAccountType.OrderBy(c => c.BankAccountTypeName).ToList()
                    };
                    return Ok(objCompanyViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/company/5
        [Authorize]
        [ResponseType(typeof(companyViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanyById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Master objCompanyInDb = db.tblCompany_Master.Find(id);
                    if (objCompanyInDb == null)
                    {
                        return NotFound();
                    }

                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    companyViewModel objCompanyViewModel = new companyViewModel()
                    {
                        objCompany = objCompanyInDb,
                        objCompanyUser = db.tblAdminLogin.Where(x => x.CompanyId == companyId && x.IsLeadUser == true).Take(1).SingleOrDefault(),
                        objCompanyList = (companyId == helper.defaultCompany ? db.QryCompany.OrderByDescending(c => c.CreatedDateTime).ToList() : db.QryCompany.Where(c => c.CompanyId == companyId).OrderByDescending(c => c.CreatedDateTime).ToList()),
                        objCompanyTypeList = db.tblCompany_Type.OrderBy(c => c.CompanyTypeName).ToList(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objBankAccountTypeList = db.tblBankAccountType.OrderBy(c => c.BankAccountTypeName).ToList()
                    };
                    return Ok(objCompanyViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [Route("api/company/companyprofile")]
        [ResponseType(typeof(tblCompany_Master))]
        [HttpGet]
        public IHttpActionResult getCompanyProfile()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    tblCompany_Master objCompany = db.tblCompany_Master.Find(companyId);
                    if (objCompany == null)
                    {
                        return NotFound();
                    }

                    return Ok(objCompany);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [Route("api/company/saveCompanyLogo")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult saveCompanyLogo(tblCompany_Master objCompany)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    try
                    {
                        tblCompany_Master objCompanyInDb = db.tblCompany_Master.SingleOrDefault(c => c.CompanyId == objCompany.CompanyId);
                        if (objCompanyInDb != null)
                        {
                            objCompanyInDb.CompanyLogo = objCompany.CompanyLogo;
                            db.Entry(objCompanyInDb).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesCompanyExists(objCompany.CompanyId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(companyViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanyListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt16(identityCompanyId.Value);

                    companyViewModel objCompanyViewModel = new companyViewModel();
                    objCompanyViewModel.totalRecords = db.QryCompany.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCompanyViewModel.objCompanyList = db.QryCompany.Where(s => s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyTelephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyEmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyGSTNo.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyPANNo.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CompanyTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objCompanyViewModel.filterRecords = objCompanyViewModel.objCompanyList.Count();
                    }
                    else
                        objCompanyViewModel.filterRecords = objCompanyViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objCompanyViewModel.objCompanyList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompany.OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany>() :
                            objCompanyViewModel.objCompanyList.OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany>();
                    else
                        objCompanyViewModel.objCompanyList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompany.OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany>() :
                            objCompanyViewModel.objCompanyList.OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryCompany>();

                    return Ok(objCompanyViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/company/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Master))]
        [HttpPut]
        public IHttpActionResult updateCompany(int id, tblCompany_Master objCompany)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objCompany.CompanyId)
                    {
                        return BadRequest();
                    }

                    try
                    {
                        var countryInBb = db.tblCompany_Master.SingleOrDefault(c => c.CompanyName.Trim().ToLower() == objCompany.CompanyName.Trim().ToLower() && c.CompanyId != objCompany.CompanyId);
                        if (countryInBb != null)
                            return BadRequest("Company name already exists");

                        db.Entry(objCompany).State = EntityState.Modified;
                        db.Entry(objCompany).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objCompany).Property(x => x.StatusId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesCompanyExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok(objCompany);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/company
        [Authorize]
        [ResponseType(typeof(tblCompany_Master))]
        [HttpPost]
        public IHttpActionResult saveCompany(tblCompany_Master objCompany)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var countryInBb = db.tblCompany_Master.SingleOrDefault(c => c.CompanyName.Trim().ToLower() == objCompany.CompanyName.Trim().ToLower() && c.CompanyId != objCompany.CompanyId);
                    if (countryInBb != null)
                        return BadRequest("Company name already exists");

                    objCompany.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    objCompany.CompanyLogo = (String.IsNullOrEmpty(objCompany.CompanyLogo) ? String.Empty : objCompany.CompanyLogo);
                    db.tblCompany_Master.Add(objCompany);
                    db.SaveChanges();
                    return Ok(objCompany);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/company/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Master))]
        [HttpDelete]
        public IHttpActionResult deleteCompany(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblCompany_Master tblCompany_Master = db.tblCompany_Master.Find(id);
                if (tblCompany_Master == null)
                {
                    return NotFound();
                }

                db.tblCompany_Master.Remove(tblCompany_Master);
                db.SaveChanges();
                return Ok();
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesCompanyExists(int id)
        {
            return db.tblCompany_Master.Count(e => e.CompanyId == id) > 0;
        }
    }
}