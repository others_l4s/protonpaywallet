﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Compilation;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;
using Newtonsoft.Json;
using RestSharp;

namespace application.webAPI.Controllers
{
    public class loginController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: login
        [ResponseType(typeof(tokenViewModel))]
        [HttpPost]
        public IHttpActionResult login(mobileLoginVM objLoginVM)
        {
            tokenViewModel objToken = new tokenViewModel();
            var client = new RestClient("" + ConfigurationManager.AppSettings["APIDomainName"] + "validateuserlogin");
            var request = new RestRequest(Method.POST);
            //request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("Connection", "keep-alive");
            //request.AddHeader("Accept-Encoding", "gzip, deflate");
            //request.AddHeader("Host", "api.protonpay.net");
            //request.AddHeader("Cache-Control", "no-cache");
            //request.AddHeader("Accept", "*/*");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("bodyParam", "username=" + objLoginVM.UserName + "&password=" + objLoginVM.Password + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            objToken = JsonConvert.DeserializeObject<tokenViewModel>(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (!String.IsNullOrEmpty(objToken.access_token))
                {
                    var userLoginInDb = db.QryEntityUser.SingleOrDefault(c => c.UserName == objLoginVM.UserName);
                    objToken.entityUserId = userLoginInDb.EntityUserId;
                    objToken.entityId = userLoginInDb.EntityId;
                    objToken.agencyName = userLoginInDb.AgencyName;
                    objToken.entityType = (userLoginInDb.EntityTypeId == Convert.ToInt32(helper.entityTypes.agent) ? "Agent" : userLoginInDb.EntityTypeId == Convert.ToInt32(helper.entityTypes.distributor) ? "Distributor" : "MasterDistributor");
                    decimal walletBalance = helper.getWalletBalance(userLoginInDb.EntityId);
                    objToken.walletBalance = walletBalance.ToString();
                    objToken.lowBalanceAlert = (walletBalance <= userLoginInDb.MinimumBalanceMaintain ? true : false);
                    objToken.mobileNo = userLoginInDb.Mobile;
                    objToken.FingerPrintLoginEnabled = (userLoginInDb.FingerPrintLoginEnabled == null ? false : userLoginInDb.FingerPrintLoginEnabled);
                    objToken.OTPVerificationRequired = false;

                    //Saving data in LoginSession
                    tblLogin_Session objLoginSession = new tblLogin_Session();
                    objLoginSession.LoginId = userLoginInDb.EntityUserId;
                    objLoginSession.accessToken = objToken.access_token;
                    objLoginSession.accessTokenValidity = helper.getIndianTime();
                    objLoginSession.accessTokenValidity = objLoginSession.accessTokenValidity.AddDays(1);
                    objLoginSession.accessTokenGeneratedDateTime = helper.getIndianTime();
                    objLoginSession.FromMobileDevice = true;
                    objLoginSession.EntityTypeId = userLoginInDb.EntityTypeId;
                    objLoginSession.IPAddress = helper.getClientIPAddress();
                    objLoginSession.IsAdminLogin = false;
                    objLoginSession.IsLogout = false;
                    db.tblLogin_Session.Add(objLoginSession);
                    db.SaveChanges();

                    if (userLoginInDb.ActivateMobileOTP.Value)
                    {
                        Boolean OTPHistoryFoundForDay = false;
                        Boolean isOTPVerified = false;
                        string OTPForday = "";
                        var otpHistoryInDb = db.tblOTP_Session.Where(x => x.EntityUserId == userLoginInDb.EntityUserId &&
                                                                    x.MobileNo == userLoginInDb.Mobile &&
                                                                    x.OTPVerifiedDateTime.Value.Day == DateTime.Today.Day &&
                                                                    x.OTPVerifiedDateTime.Value.Month == DateTime.Today.Month &&
                                                                    x.OTPVerifiedDateTime.Value.Year == DateTime.Today.Year &&
                                                                    x.FromMobileDevice == true).Take(1).SingleOrDefault();
                        OTPHistoryFoundForDay = otpHistoryInDb == null ? false : true;
                        isOTPVerified = OTPHistoryFoundForDay ? otpHistoryInDb.OTPVerified : false;
                        OTPForday = (OTPHistoryFoundForDay ? otpHistoryInDb.OTP : helper.generateRandom8DigitNumber());
                        if (!OTPHistoryFoundForDay)
                        {
                            //Generating mobile OTP and saving it to database.
                            tblOTP_Session objOTPSession = new tblOTP_Session();
                            objOTPSession.EntityTypeId = userLoginInDb.EntityTypeId;
                            objOTPSession.UserName = userLoginInDb.UserName;
                            objOTPSession.EntityUserId = userLoginInDb.EntityUserId;
                            objOTPSession.MobileNo = userLoginInDb.Mobile;
                            objOTPSession.OTP = OTPForday;
                            objOTPSession.OTPVerified = false;
                            objOTPSession.OTPSendDateTime = helper.getIndianTime();
                            objOTPSession.FromMobileDevice = true;
                            db.tblOTP_Session.Add(objOTPSession);
                            db.SaveChanges();
                            helper.sendSMS_OTP(userLoginInDb.Mobile, helper.loginOTPTemplate(OTPForday), userLoginInDb.EntityUserId);
                            objToken.OTPVerificationRequired = true;
                        }
                        else
                        {
                            if (!isOTPVerified)
                                helper.sendSMS_OTP(userLoginInDb.Mobile, helper.loginOTPTemplate(OTPForday), userLoginInDb.EntityUserId);
                        }
                    }

                    return Ok(objToken);
                }
                else
                {
                    return BadRequest("Invalid Username or Password");
                }
            }
            else
            {
                return BadRequest("Invalid Username or Password");
            }
        }

        [HttpPost]
        [Route("api/loginviafingerprint")]
        public IHttpActionResult loginviafinterprint(mobileLoginFingerPrintVM objLoginVM)
        {
            tokenViewModel objToken = new tokenViewModel();
            var client = new RestClient("" + ConfigurationManager.AppSettings["APIDomainName"] + "validateuserviafingerprintlogin");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("Host", "api.protonpay.net");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Accept", "*/*");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("bodyParam", "username=" + objLoginVM.Username + "&password=" + objLoginVM.IMEINo + "&grant_type=password", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            objToken = JsonConvert.DeserializeObject<tokenViewModel>(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (!String.IsNullOrEmpty(objToken.access_token))
                {
                    var userLoginInDb = db.QryEntityUser.SingleOrDefault(c => c.UserName == objLoginVM.Username);
                    objToken.entityUserId = userLoginInDb.EntityUserId;
                    objToken.entityId = userLoginInDb.EntityId;
                    objToken.agencyName = userLoginInDb.AgencyName;
                    objToken.entityType = (userLoginInDb.EntityTypeId == Convert.ToInt32(helper.entityTypes.agent) ? "Agent" : userLoginInDb.EntityTypeId == Convert.ToInt32(helper.entityTypes.distributor) ? "Distributor" : "MasterDistributor");
                    decimal walletBalance = helper.getWalletBalance(userLoginInDb.EntityId);
                    objToken.walletBalance = walletBalance.ToString();
                    objToken.lowBalanceAlert = (walletBalance <= userLoginInDb.MinimumBalanceMaintain ? true : false);
                    objToken.mobileNo = userLoginInDb.Mobile;
                    objToken.FingerPrintLoginEnabled = (userLoginInDb.FingerPrintLoginEnabled == null ? false : userLoginInDb.FingerPrintLoginEnabled);
                    //Saving data in LoginSession
                    tblLogin_Session objLoginSession = new tblLogin_Session();
                    objLoginSession.LoginId = userLoginInDb.EntityUserId;
                    objLoginSession.accessToken = objToken.access_token;
                    objLoginSession.accessTokenValidity = helper.getIndianTime();
                    objLoginSession.accessTokenValidity = objLoginSession.accessTokenValidity.AddDays(1);
                    objLoginSession.accessTokenGeneratedDateTime = helper.getIndianTime();
                    objLoginSession.FromMobileDevice = true;
                    objLoginSession.EntityTypeId = userLoginInDb.EntityTypeId;
                    objLoginSession.IPAddress = helper.getClientIPAddress();
                    objLoginSession.IsAdminLogin = false;
                    objLoginSession.IsLogout = false;
                    db.tblLogin_Session.Add(objLoginSession);
                    db.SaveChanges();


                    var otpHistoryInDb = db.tblOTP_Session.Where(x => x.EntityUserId == userLoginInDb.EntityUserId &&
                    x.MobileNo == userLoginInDb.Mobile &&
                    x.OTPVerifiedDateTime.Value.Day == DateTime.Today.Day &&
                    x.OTPVerifiedDateTime.Value.Month == DateTime.Today.Month &&
                    x.OTPVerifiedDateTime.Value.Year == DateTime.Today.Year &&
                    x.OTPVerified == true &&
                    x.FromMobileDevice == true).Take(1).SingleOrDefault();
                    if (otpHistoryInDb == null)
                    {
                        string OTP = helper.generateRandom8DigitNumber();
                        //Generating mobile OTP and saving it to database.
                        tblOTP_Session objOTPSession = new tblOTP_Session();
                        objOTPSession.EntityTypeId = userLoginInDb.EntityTypeId;
                        objOTPSession.UserName = userLoginInDb.UserName;
                        objOTPSession.EntityUserId = userLoginInDb.EntityUserId;
                        objOTPSession.MobileNo = userLoginInDb.Mobile;
                        objOTPSession.OTP = OTP;
                        objOTPSession.OTPVerified = false;
                        objOTPSession.OTPSendDateTime = helper.getIndianTime();
                        objOTPSession.FromMobileDevice = true;
                        db.tblOTP_Session.Add(objOTPSession);
                        db.SaveChanges();
                        helper.sendSMS_OTP(userLoginInDb.Mobile, helper.loginOTPTemplate(OTP), userLoginInDb.EntityUserId);
                        objToken.OTPVerificationRequired = true;
                    }
                    else
                        objToken.OTPVerificationRequired = false;

                    return Ok(objToken);
                }
                else
                {
                    return BadRequest("Invalid Username or Password");
                }
            }
            else
            {
                return BadRequest("Invalid Username or Password");
            }
        }

        [Authorize]
        [ResponseType(typeof(Boolean))]
        [Route("api/verifyLoginOTP")]
        [HttpPost]
        public IHttpActionResult verifyLoginOTP(mobileOTPVM objmobileOTPVM)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt16(identityEntityType.Value);
                if (entityTypeId != helper.defaultCompany)
                {
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    if (objmobileOTPVM.entityUserId == entityUserId)
                    {
                        var mobileOTPSessionInDb = db.tblOTP_Session.Where(x => x.EntityUserId == entityUserId &&
                        x.MobileNo == objmobileOTPVM.mobileNo &&
                        x.OTP == objmobileOTPVM.OTP &&
                        x.OTPVerified == false).Take(1).SingleOrDefault();
                        if (mobileOTPSessionInDb != null)
                        {
                            mobileOTPSessionInDb.OTPVerified = true;
                            mobileOTPSessionInDb.OTPVerifiedDateTime = helper.getIndianTime();

                            db.Entry(mobileOTPSessionInDb).State = EntityState.Modified;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.EntityTypeId).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.EntityUserId).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.UserName).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.MobileNo).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.OTP).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.OTPSendDateTime).IsModified = false;
                            db.Entry(mobileOTPSessionInDb).Property(x => x.FromMobileDevice).IsModified = false;
                            db.SaveChanges();
                            return Ok(true);
                        }
                        else
                            return BadRequest("Invalid OTP");
                    }
                    else
                    {
                        return BadRequest(helper.invalidModuleAccess);
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(Boolean))]
        [Route("api/resendLoginOTP")]
        [HttpPost]
        public IHttpActionResult resendLoginOTP(mobileOTPVM objmobileOTPVM)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt16(identityEntityType.Value);
                if (entityTypeId != helper.defaultCompany)
                {
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    if (objmobileOTPVM.entityUserId == entityUserId)
                    {
                        var mobileOTPSessionInDb = db.tblOTP_Session.Where(x => x.EntityUserId == entityUserId &&
                        x.MobileNo == objmobileOTPVM.mobileNo &&
                        x.OTPVerified == false).Take(1).SingleOrDefault();
                        if (mobileOTPSessionInDb != null)
                        {
                            helper.sendSMS_OTP(objmobileOTPVM.mobileNo, helper.loginOTPTemplate(mobileOTPSessionInDb.OTP), objmobileOTPVM.entityUserId);
                        }
                        else
                        {
                            string OTP = helper.generateRandom8DigitNumber();
                            var entityUserInDb = db.QryEntityUser.SingleOrDefault(x => x.EntityUserId == objmobileOTPVM.entityUserId);
                            tblOTP_Session objOTPSession = new tblOTP_Session();
                            objOTPSession.EntityTypeId = entityUserInDb.EntityTypeId;
                            objOTPSession.UserName = entityUserInDb.UserName;
                            objOTPSession.EntityUserId = entityUserInDb.EntityUserId;
                            objOTPSession.MobileNo = entityUserInDb.Mobile;
                            objOTPSession.OTP = OTP;
                            objOTPSession.OTPVerified = false;
                            objOTPSession.OTPSendDateTime = helper.getIndianTime();
                            objOTPSession.FromMobileDevice = true;
                            db.tblOTP_Session.Add(objOTPSession);
                            db.SaveChanges();
                            helper.sendSMS_OTP(entityUserInDb.Mobile, helper.loginOTPTemplate(OTP), entityUserInDb.EntityUserId);
                        }
                        return Ok(true);
                    }
                    else
                    {
                        return BadRequest(helper.invalidModuleAccess);
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Route("api/validateUsername4ForgotPassword")]
        [ResponseType(typeof(mobileOTPVM))]
        [HttpGet]
        public IHttpActionResult validateUsername4ForgotPassword(string userName, string fromMobileDevice)
        {
            mobileOTPVM objMobileOTPVM = new mobileOTPVM();
            var userLoginInDb = db.QryEntityUser.SingleOrDefault(c => c.UserName == userName || c.EmailId == userName);
            if (userLoginInDb != null)
            {
                string OTP = helper.generateRandom8DigitNumber();
                tblForgotPassword_OTP_Session objOTPSession = new tblForgotPassword_OTP_Session();
                objOTPSession.EntityTypeId = userLoginInDb.EntityTypeId;
                objOTPSession.EntityUserId = userLoginInDb.EntityUserId;
                objOTPSession.MobileNo = userLoginInDb.Mobile;
                objOTPSession.OTP = OTP;
                objOTPSession.OTPVerified = false;
                objOTPSession.OTPSendDateTime = helper.getIndianTime();
                objOTPSession.FromMobileDevice = (fromMobileDevice == "1" ? true : false);
                db.tblForgotPassword_OTP_Session.Add(objOTPSession);
                db.SaveChanges();

                helper.sendSMS_OTP(userLoginInDb.Mobile, OTP, userLoginInDb.EntityUserId);
                objMobileOTPVM.entityUserId = userLoginInDb.EntityUserId;
                objMobileOTPVM.mobileNo = userLoginInDb.Mobile;
                objMobileOTPVM.userName = userLoginInDb.UserName;
                return Ok(objMobileOTPVM);
            }
            else
            {
                return BadRequest("Invalid Username");
            }
        }

        [ResponseType(typeof(Boolean))]
        [Route("api/resendForgotPasswordOTP")]
        [HttpPost]
        public IHttpActionResult resendForgotPasswordOTP(mobileOTPVM objmobileOTPVM)
        {
            var forgotPasswordOTPSession = db.tblForgotPassword_OTP_Session.Where(x => x.EntityUserId == objmobileOTPVM.entityUserId &&
                x.MobileNo == objmobileOTPVM.mobileNo &&
                x.OTPVerified == false).OrderByDescending(x => x.OTPSendDateTime).Take(1).SingleOrDefault();
            if (forgotPasswordOTPSession != null)
            {
                helper.sendSMS_OTP(objmobileOTPVM.mobileNo, forgotPasswordOTPSession.OTP, objmobileOTPVM.entityUserId);
                return Ok(true);
            }
            else
                return BadRequest("Invalid details. Please try again later.");
        }

        [Route("api/verifyForgotPasswordOTP")]
        [HttpPost]
        public IHttpActionResult verifyForgotPasswordOTP(mobileOTPVM objMobileOTPVM)
        {
            if (objMobileOTPVM.OTP.Length > 0)
            {
                var forgotPasswordOTPSessionInDb = db.tblForgotPassword_OTP_Session.Where(x => x.EntityUserId == objMobileOTPVM.entityUserId &&
                x.MobileNo == objMobileOTPVM.mobileNo && x.OTPVerified == false).OrderByDescending(x => x.OTPSendDateTime).Take(1).SingleOrDefault();
                if (forgotPasswordOTPSessionInDb != null)
                {
                    if (forgotPasswordOTPSessionInDb.OTP == objMobileOTPVM.OTP)
                    {
                        //Updating ForgotPassword OTP Session Flag
                        forgotPasswordOTPSessionInDb.OTPVerified = true;
                        forgotPasswordOTPSessionInDb.OTPVerifiedDateTime = helper.getIndianTime();

                        db.Entry(forgotPasswordOTPSessionInDb).State = EntityState.Modified;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.EntityTypeId).IsModified = false;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.EntityUserId).IsModified = false;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.MobileNo).IsModified = false;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.OTP).IsModified = false;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.OTPSendDateTime).IsModified = false;
                        db.Entry(forgotPasswordOTPSessionInDb).Property(x => x.FromMobileDevice).IsModified = false;
                        db.SaveChanges();

                        var entityDetailInDb = db.tblEntity_Users.Where(x => x.EntityUserId == objMobileOTPVM.entityUserId).SingleOrDefault();
                        string rawPassword = helper.unProtectString(entityDetailInDb.Password);
                        helper.sendForgot_Password(entityDetailInDb.Mobile, entityDetailInDb.UserName, rawPassword);
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Invalid OTP");
                    }
                }
                else
                {
                    return BadRequest("Invalid OTP or OTP cannot be blank");
                }
            }
            else
                return BadRequest("Invalid OTP or OTP cannot be blank");
        }

        [ResponseType(typeof(Boolean))]
        [Route("api/verifyRegisterOTP")]
        [HttpPost]
        public IHttpActionResult verifyRegisterOTP(mobileOTPVM objmobileOTPVM)
        {

            string verifyOTPResponse = helper.verifySMS_OTP(objmobileOTPVM.mobileNo, objmobileOTPVM.OTP);
            verifyOTPResponse objVerifyOTPResponse = JsonConvert.DeserializeObject<verifyOTPResponse>(verifyOTPResponse);
            if (objVerifyOTPResponse.type == "success")
                return Ok(true);
            else
                return BadRequest(objVerifyOTPResponse.message);
        }

        [ResponseType(typeof(Boolean))]
        [Route("api/resendRegisterOTP")]
        [HttpPost]
        public IHttpActionResult resendRegisterOTP(mobileOTPVM objmobileOTPVM)
        {
            helper.resendSMS_OTP(objmobileOTPVM.mobileNo);
            return Ok(true);
        }

    }

    public class verifyOTPResponse
    {
        public string message { get; set; }
        public string type { get; set; }
    }
}