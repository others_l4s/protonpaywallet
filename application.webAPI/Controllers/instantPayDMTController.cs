﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.viewModel;
using application.webAPI.instantpay.dmt.request;
using application.webAPI.instantpay.dmt.response;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Claims;
using application.webAPI.Models;
using System.Globalization;
using System.Data.Entity;
using application.webAPI.instantpay.utility.request;
using System.Security.AccessControl;
using System.Data.SqlClient;
using System.Net.Http.Headers;

namespace application.webAPI.Controllers
{
    public class instantPayDMTController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        #region InstantPay API
        protected IRestResponse fetchRemitterDetails(remitter_details_request objremitter_details)
        {
            IRestResponse response;
            string URL = "dmi/remitter_details";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objremitter_details);
            return response;
        }

        protected IRestResponse addRemitter(remitter_register_request objremitter)
        {
            IRestResponse response;
            string URL = "dmi/remitter";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objremitter);
            return response;
        }

        protected IRestResponse Remitter_Validate(remitter_validate_request objremittervalidaterequest)
        {
            IRestResponse response;
            string URL = "dmi/remitter_validate";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objremittervalidaterequest);
            return response;
        }
        protected IRestResponse BankDetailsGet(string accountNumber)
        {
            bank_details_request objbankDetails = new bank_details_request();
            bank_details_req objbankrequest = new bank_details_req();
            objbankDetails.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            objbankrequest.outletid = 1;
            objbankrequest.account = accountNumber;
            objbankDetails.request = objbankrequest;
            IRestResponse response;
            string URL = "dmi/bank_details";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objbankDetails);
            return response;
        }

        protected IRestResponse addBeneficiary(beneficiary_register_request objbeneficiaryRequest)
        {
            IRestResponse response;
            string URL = "dmi/beneficiary_register";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objbeneficiaryRequest);
            return response;
        }
        protected IRestResponse Beneficiary_Validate(beneficiary_register_validate_request objbeneficiaryValidateRequest)
        {
            IRestResponse response;
            string URL = "dmi/beneficiary_register_validate";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objbeneficiaryValidateRequest);
            return response;
        }

        protected IRestResponse DeleteBeneficiary(beneficiary_remove_request objBEneficiaryDeleteRequest)
        {
            IRestResponse response;
            string URL = "dmi/beneficiary_remove";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objBEneficiaryDeleteRequest);
            return response;
        }

        protected IRestResponse DeleteValidateBeneficiary(beneficiary_remove_validate_request objBeneficiaryValidateRequest)
        {
            IRestResponse response;
            string URL = "dmi/beneficiary_remove_validate";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objBeneficiaryValidateRequest);
            return response;
        }

        protected IRestResponse TransferMoney(transfer_request objtransferRequest)
        {
            IRestResponse response;
            string URL = "dmi/transfer";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objtransferRequest);
            return response;
        }

        protected IRestResponse beneficiaryAccountVerification(account_verification_request objAccountVerificationRequest)
        {
            IRestResponse response;
            string URL = "imps/account_validate";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objAccountVerificationRequest);
            return response;
        }

        protected IRestResponse balanceCheck(string token)
        {
            IRestResponse response;
            var client = new RestClient("https://www.instantpay.in/ws/userresources/instantpay");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Cookie", "SERVERID=A");
            request.AddParameter("application/json", "{\"token\":\"" + token + "\"}", ParameterType.RequestBody);
            response = client.Execute(request);
            return response;
        }

        protected IRestResponse checkTransactionStatus(transactionStatusRequest objTransactionSttus)
        {
            IRestResponse response;
            string URL = "status/checkbyorderid";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objTransactionSttus);
            return response;
        }

        protected IRestResponse payoutDirect(payoutDirect_Request objPayoutDirectRequest)
        {
            IRestResponse response;
            string URL = "payouts/direct";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objPayoutDirectRequest);
            return response;
        }
        #endregion

        #region InstantPayDMT Process in Database

        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getAgencyDetail")]
        public IHttpActionResult getAgencyDetail()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                List<tblEntity_Master> objEntityMasterList = new List<tblEntity_Master>();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                Int32 entityTypeAgent = Convert.ToInt32(helper.entityTypes.agent);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityMasterList = db.tblEntity_Master.Where(x => x.ParentEntityId == entityId
                    && x.StatusId == helper.defaultActiveStatusId).OrderBy(x => x.AgencyName).ToList();
                }
                else
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    objEntityMasterList = db.tblEntity_Master.Where(x => x.CompanyId == companyId && x.EntityTypeId == entityTypeAgent && x.StatusId == helper.defaultActiveStatusId).OrderBy(x => x.AgencyName).ToList();
                }
                return Ok(objEntityMasterList);
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterDetail")]
        public IHttpActionResult getRemitterDetail(string mobileno, string accountno)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    string RemitterId = "";
                    Boolean detailsFound = false;
                    string RemitterMobileNo = mobileno;
                    Boolean otpVerified = false;
                    if (!String.IsNullOrEmpty(RemitterMobileNo))
                    {
                        var RemitterDetailInDb = db.tblInstantPay_Remitter.Where(x => x.MobileNo.Trim() == RemitterMobileNo.Trim()).Take(1).SingleOrDefault();
                        if (RemitterDetailInDb != null)
                        {
                            detailsFound = true;
                            RemitterId = RemitterDetailInDb.RemitterId;
                            otpVerified = RemitterDetailInDb.IsVerified == 1 ? true : false;
                        }
                    }
                    else
                    {
                        tblInstantPay_Beneficiary objBeneficiaryDetails = db.tblInstantPay_Beneficiary.Where(x => x.AccountNo.Trim() == accountno.Trim()).Take(1).SingleOrDefault();
                        if (objBeneficiaryDetails != null)
                        {
                            detailsFound = true;
                            RemitterId = objBeneficiaryDetails.RemitterId;
                        }
                    }

                    if (detailsFound == false) // Checking remitter detail in API
                    {
                        remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
                        objRemitterDetailsRequest.request = new remitter_details_req();
                        objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        objRemitterDetailsRequest.request.mobile = mobileno;
                        IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
                        if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
                        {
                            remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                            if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN")
                            {
                                tblInstantPay_Remitter objRemitterDetailsDB = new tblInstantPay_Remitter();
                                objRemitterDetailsDB.InstantPayRemitterId = 0;
                                objRemitterDetailsDB.EntityId = entityId;
                                objRemitterDetailsDB.EntityUserId = entityUserId;
                                objRemitterDetailsDB.Name = (RemitterFetchAllResp.data.remitter.name == null ? string.Empty : RemitterFetchAllResp.data.remitter.name);
                                objRemitterDetailsDB.Surname = "";
                                objRemitterDetailsDB.Pincode = (RemitterFetchAllResp.data.remitter.pincode == null ? string.Empty : RemitterFetchAllResp.data.remitter.pincode);
                                objRemitterDetailsDB.IsVerified = RemitterFetchAllResp.data.remitter.is_verified;
                                objRemitterDetailsDB.RemitterId = RemitterFetchAllResp.data.remitter.id;
                                objRemitterDetailsDB.ipay_uuid = RemitterFetchAllResp.ipay_uuid;
                                objRemitterDetailsDB.OTP = "0";
                                objRemitterDetailsDB.CreatedDateTime = helper.getIndianTime();
                                objRemitterDetailsDB.MobileNo = mobileno;
                                db.tblInstantPay_Remitter.Add(objRemitterDetailsDB);
                                db.SaveChanges();
                                otpVerified = (RemitterFetchAllResp.data.remitter.is_verified == 1 ? true : false);
                                return Ok(true + "|" + RemitterFetchAllResp.data.remitter.id + "|" + otpVerified);
                            }
                            else
                            {
                                return Ok(false);
                            }
                        }
                        else
                        {
                            return BadRequest(remitterDetailResponse.StatusCode.ToString());
                        }
                    }
                    else
                    {

                        return Ok(detailsFound + "|" + RemitterId + "|" + otpVerified);
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterDetailByMobile")]
        public IHttpActionResult getRemitterDetailByMobile(string mobileno)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    string RemitterMobileNo = mobileno;
                    remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
                    objRemitterDetailsRequest.request = new remitter_details_req();
                    objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objRemitterDetailsRequest.request.mobile = mobileno;
                    IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
                    if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                        if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN")
                        {
                            return Ok(RemitterFetchAllResp);
                        }
                        else
                        {
                            return Ok(false);
                        }
                    }
                    else
                    {
                        return BadRequest(remitterDetailResponse.StatusCode.ToString());
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        /// <summary>
        /// This service will help to see Sender Transfer limit per month (he can transfer only 2 lac per month)
        /// </summary>
        /// <param name="mobileno"></param>
        /// <returns></returns>
        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterPayoutDetailPerMonth")]
        public IHttpActionResult getRemitterPayoutDetailPerMonth(string mobileno)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    string RemitterMobileNo = mobileno;
                    remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
                    objRemitterDetailsRequest.request = new remitter_details_req();
                    objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objRemitterDetailsRequest.request.mobile = mobileno;
                    IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
                    if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                        if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN")
                        {
                            //Getting Sender Transfer Amount via Payout
                            RemitterFetchAllResp.data.remitter.remaininglimit = getSenderLimit4PayoutDirect(mobileno);
                            return Ok(RemitterFetchAllResp);
                        }
                        else
                        {
                            return Ok(false);
                        }
                    }
                    else
                    {
                        return BadRequest(remitterDetailResponse.StatusCode.ToString());
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [ResponseType(typeof(remitter_register_request))]
        [Route("api/instantPayDMT/saveRemitterDetails")]
        [HttpPost]
        public IHttpActionResult saveRemitterDetails(remitter_register_request objremitterRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    remitter_register_request RemitterRequest = new remitter_register_request();
                    RemitterRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    remitter_req request = new remitter_req();
                    request.mobile = objremitterRequest.request.mobile;
                    request.name = objremitterRequest.request.name;
                    request.surname = objremitterRequest.request.surname;
                    request.pincode = objremitterRequest.request.pincode;
                    request.outletid = 1;
                    RemitterRequest.request = request;
                    string IsVerified = "0";
                    string IsOTPSend = "0";
                    IRestResponse AddremitterResponse = addRemitter(RemitterRequest);
                    if (AddremitterResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_register_response RemitterResponse = JsonConvert.DeserializeObject<remitter_register_response>(AddremitterResponse.Content);
                        if (RemitterResponse.statuscode.ToLower() == "txn")
                        {
                            var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                            var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                            var remitterDetailInDb = db.tblInstantPay_Remitter.SingleOrDefault(c => c.RemitterId.Trim() == RemitterResponse.data.remitter.id);
                            if (remitterDetailInDb != null)
                                return BadRequest("Remitter Details already exists");
                            tblInstantPay_Remitter objRemitterDetailsDB = new tblInstantPay_Remitter();
                            objRemitterDetailsDB.InstantPayRemitterId = 0;
                            objRemitterDetailsDB.EntityId = Convert.ToInt64(identityEntityId.Value);
                            objRemitterDetailsDB.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                            objRemitterDetailsDB.Name = RemitterRequest.request.name;
                            objRemitterDetailsDB.Surname = RemitterRequest.request.surname;
                            objRemitterDetailsDB.Pincode = RemitterRequest.request.pincode;
                            objRemitterDetailsDB.IsVerified = RemitterResponse.data.remitter.is_verified;
                            objRemitterDetailsDB.RemitterId = RemitterResponse.data.remitter.id;
                            objRemitterDetailsDB.ipay_uuid = RemitterResponse.ipay_uuid;
                            objRemitterDetailsDB.OTP = "0";
                            objRemitterDetailsDB.CreatedDateTime = helper.getIndianTime();
                            objRemitterDetailsDB.MobileNo = RemitterRequest.request.mobile;
                            db.tblInstantPay_Remitter.Add(objRemitterDetailsDB);
                            db.SaveChanges();
                            if (objRemitterDetailsDB.IsVerified == 1)
                            {
                                IsVerified = "1";

                            }
                            if (RemitterResponse.status.ToLower().Trim() == "otp sent successfully")
                            {
                                IsOTPSend = "1";
                            }
                            return Ok(IsVerified + "|" + IsOTPSend + "|" + objRemitterDetailsDB.RemitterId + "|" + objRemitterDetailsDB.MobileNo);
                        }
                        else
                            return Ok(RemitterResponse.status.ToString());
                    }
                    else
                        return Ok(AddremitterResponse.ErrorException.InnerException.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(remitter_register_request))]
        [Route("api/instantPayDMT/RemitterValidated")]
        [HttpPost]
        public IHttpActionResult RemitterValidated(remitter_validate_request objremitterValidateRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var DBRemitterCheck = db.tblInstantPay_Remitter.SingleOrDefault(e => e.RemitterId == objremitterValidateRequest.request.remitterid && e.IsVerified == 0);
                    if (DBRemitterCheck != null)
                    {
                        remitter_validate_request RemitterValidateReq = new remitter_validate_request();
                        RemitterValidateReq.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        remitter_validate_req request = new remitter_validate_req();
                        request.mobile = objremitterValidateRequest.request.mobile;
                        request.remitterid = objremitterValidateRequest.request.remitterid;
                        request.otp = objremitterValidateRequest.request.otp;
                        request.outletid = 1;
                        RemitterValidateReq.request = request;

                        IRestResponse remitterValidResponse = Remitter_Validate(RemitterValidateReq);
                        if (remitterValidResponse.StatusCode == HttpStatusCode.OK)
                        {
                            remitter_validate_response RemitterValidateResponse = JsonConvert.DeserializeObject<remitter_validate_response>(remitterValidResponse.Content);
                            if (RemitterValidateResponse.statuscode.ToLower() == "txn")
                            {
                                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                                var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                                tblInstantPay_Remitter remitterDetailInDb = db.tblInstantPay_Remitter.SingleOrDefault(c => c.RemitterId.Trim() == RemitterValidateResponse.data.remitter.id && c.OTP == "0");
                                if (remitterDetailInDb != null)
                                {
                                    remitterDetailInDb.OTP = RemitterValidateReq.request.otp;
                                    remitterDetailInDb.IsVerified = RemitterValidateResponse.data.remitter.is_verified;
                                    db.Entry(remitterDetailInDb).State = EntityState.Modified;
                                    db.Entry(remitterDetailInDb).Property(x => x.Name).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.Surname).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.MobileNo).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.RemitterId).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.ipay_uuid).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.EntityId).IsModified = false;
                                    db.Entry(remitterDetailInDb).Property(x => x.EntityUserId).IsModified = false;
                                    db.SaveChanges();
                                }
                            }
                            return Ok(RemitterValidateResponse.statuscode.ToString());
                        }
                        else
                        {
                            return Ok("Remitter already Validated");
                        }
                    }
                    else
                        return Ok("Remitter Not Founded. Or Already Validate");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(bank_details_response))]
        [Route("api/instantPayDMT/GetBankDetails")]
        [HttpGet]
        public IHttpActionResult GetBankDetails(string accountNumber)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    IRestResponse BankDetailsResponse = BankDetailsGet(accountNumber);
                    if (BankDetailsResponse.StatusCode == HttpStatusCode.OK)
                    {
                        bank_details_response bankResponse = JsonConvert.DeserializeObject<bank_details_response>(BankDetailsResponse.Content);
                        List<bank_details_data_resp> objBankResponseList = bankResponse.data.Where(x => x.is_down == "0").ToList();
                        //bankResponse.data = objBankResponseList.OrderBy(x => x.bank_name).ToList();
                        return Ok(bankResponse);
                    }
                    else
                        return Ok(BankDetailsResponse.StatusDescription.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(beneficiary_register_request))]
        [Route("api/instantPayDMT/saveBeneficiaryDetails")]
        [HttpPost]
        public IHttpActionResult saveBeneficiaryDetails(beneficiary_register_request objbeneficiaryRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    beneficiary_register_request BeneficiaryRequest = new beneficiary_register_request();
                    BeneficiaryRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objbeneficiaryRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    benefical_register_req request = new benefical_register_req();
                    request.mobile = objbeneficiaryRequest.request.mobile;
                    request.name = objbeneficiaryRequest.request.name;
                    request.remitterid = objbeneficiaryRequest.request.remitterid;
                    request.ifsc = objbeneficiaryRequest.request.ifsc;
                    request.account = objbeneficiaryRequest.request.account;
                    request.outletid = 1;
                    BeneficiaryRequest.request = request;
                    Boolean beneficiaryAccountVerified = false;
                    Boolean beneficiaryOTPVerified = false;
                    Boolean beneficiaryAddedInAPI = false;
                    Boolean beneficiaryExistsInDb = false;
                    string beneficiaryId = "";
                    beneficiary_register_response BeneficiaryResponse = new beneficiary_register_response();
                    account_verification_request objAccountVerificationRequest = new account_verification_request();
                    account_verification_response objAccountVerificationResponse = new account_verification_response();
                    string remitterMobileNo = "";
                    var remitterMobileNoInDb = db.tblInstantPay_Remitter.Where(x => x.RemitterId.Trim() == objbeneficiaryRequest.request.remitterid.Trim()).Take(1).SingleOrDefault();
                    if (remitterMobileNoInDb != null)
                        remitterMobileNo = remitterMobileNoInDb.MobileNo;
                    //Validating Beneficiary Account by Bank Account Verification API
                    objAccountVerificationRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objAccountVerificationRequest.request = new beneficiary_account_request();
                    objAccountVerificationRequest.request.remittermobile = objbeneficiaryRequest.request.mobile;
                    objAccountVerificationRequest.request.account = objbeneficiaryRequest.request.account;
                    objAccountVerificationRequest.request.ifsc = objbeneficiaryRequest.request.ifsc;
                    objAccountVerificationRequest.request.agentid = objbeneficiaryRequest.request.account;
                    var beneficiaryAccountNumberInDb = new tblInstantPay_Beneficiary();
                    var objBeneficiaryInAPI = getBeneficiaryDetailInRemitterDatabaseAPI(remitterMobileNo, objbeneficiaryRequest.request.account);
                    if (objBeneficiaryInAPI != null)
                        beneficiaryAddedInAPI = true;
                    var beneficiaryDetailFromDb = beneficiaryAccountInDbIfExists(objbeneficiaryRequest.request.account, objbeneficiaryRequest.request.remitterid);
                    if (beneficiaryDetailFromDb != null)
                        beneficiaryExistsInDb = true;

                    if (beneficiaryExistsInDb && beneficiaryAddedInAPI)
                    {
                        if (beneficiaryDetailFromDb.RemitterId == objbeneficiaryRequest.request.remitterid)
                        {
                            if (beneficiaryDetailFromDb.IsAccountVerified.Value == true)
                                return Ok("TXN|" + beneficiaryAccountNumberInDb.BeneficiaryId);
                            else
                                beneficiaryAccountVerified = false;
                        }
                        else
                        {
                            if (objBeneficiaryInAPI.imps == "1")
                            {
                                beneficiaryAccountVerified = true;
                                BeneficiaryRequest.request.name = objBeneficiaryInAPI.name;
                            }
                        }
                    }
                    if (beneficiaryAddedInAPI == false)
                    {
                        if (beneficiaryExistsInDb)
                        {
                            beneficiaryAccountVerified = beneficiaryDetailFromDb.IsOTPVerified;
                        }
                        //string beneficiaryActualName = (beneficiaryAccountVerified == false ? verifyBeneficiary_Account(objAccountVerificationRequest, entityId, entityUserId) : beneficiaryDetailFromDb.Name);
                        //if (beneficiaryActualName.Length > 0)
                        //{
                        //    beneficiaryAccountVerified = true;
                        //    //Adding Beneficiary in API
                        //    BeneficiaryRequest.request.name = beneficiaryActualName;
                        //}
                        //else
                        //    beneficiaryActualName = BeneficiaryRequest.request.name;
                        string beneficiaryActualName = BeneficiaryRequest.request.name;
                        IRestResponse AddBeneficiaryResponse = addBeneficiary(BeneficiaryRequest);
                        if (AddBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                        {
                            BeneficiaryResponse = JsonConvert.DeserializeObject<beneficiary_register_response>(AddBeneficiaryResponse.Content);
                            if (BeneficiaryResponse.statuscode.ToLower().Trim() == "txn" && BeneficiaryResponse.status.Trim() == "Transaction Successful")
                            {
                                beneficiaryAddedInAPI = true;
                                beneficiaryOTPVerified = (BeneficiaryResponse.data.beneficiary.status.ToString() == "1" ? true : false);
                                beneficiaryId = BeneficiaryResponse.data.beneficiary.id;
                                addUpdateBeneficiaryInDb(beneficiaryId,
                                    beneficiaryActualName, BeneficiaryRequest.request.mobile, BeneficiaryRequest.request.ifsc, BeneficiaryRequest.request.account,
                                    objbeneficiaryRequest.request.remitterid, "0", beneficiaryOTPVerified,
                                    beneficiaryAccountVerified, entityId, entityUserId);
                                beneficiaryExistsInDb = true;
                            }
                        }
                    }

                    if (beneficiaryAddedInAPI && beneficiaryExistsInDb == false)
                    {
                        beneficiaryAccountVerified = objBeneficiaryInAPI.imps == "1" ? true : false;
                        beneficiaryOTPVerified = (objBeneficiaryInAPI.status == "1" ? true : false);
                        addUpdateBeneficiaryInDb(objBeneficiaryInAPI.id, objBeneficiaryInAPI.name, objBeneficiaryInAPI.mobile, objBeneficiaryInAPI.ifsc, objBeneficiaryInAPI.account, objbeneficiaryRequest.request.remitterid, "0", beneficiaryOTPVerified, beneficiaryAccountVerified, entityId, entityUserId);
                    }
                    return Ok((beneficiaryOTPVerified == true ? "1" : "0") + "|" + beneficiaryId);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        protected void addUpdateBeneficiaryInDb(string benId, string benName, string benMobileNo, string benIFSC, string benAccountNo, string remitterId, string OTP, Boolean OTPVerified, Boolean AccountVerified, long entityId, long entityUserId)
        {
            var beneficiaryDetailInDb = db.tblInstantPay_Beneficiary.Where(x => x.RemitterId == remitterId && x.AccountNo.Trim() == benAccountNo).Take(1).SingleOrDefault();
            if (beneficiaryDetailInDb == null)
            {
                tblInstantPay_Beneficiary objInstantPayBeneficiary = new tblInstantPay_Beneficiary();
                objInstantPayBeneficiary.instantPayBeneficiaryId = 0;
                objInstantPayBeneficiary.Name = benName;
                objInstantPayBeneficiary.MobileNo = benMobileNo;
                objInstantPayBeneficiary.ifsc = benIFSC;
                objInstantPayBeneficiary.Bank = getBankNameFromAPI(benAccountNo, benIFSC);
                objInstantPayBeneficiary.AccountNo = benAccountNo;
                objInstantPayBeneficiary.RemitterId = remitterId;
                objInstantPayBeneficiary.BeneficiaryId = benId;
                objInstantPayBeneficiary.CreatedDateTime = helper.getIndianTime();
                objInstantPayBeneficiary.OTP = OTP;
                objInstantPayBeneficiary.OTPVerifiedDateTime = helper.getIndianTime();
                objInstantPayBeneficiary.EntityId = entityId;
                objInstantPayBeneficiary.EntityUserId = entityUserId;
                objInstantPayBeneficiary.IsOTPVerified = OTPVerified;
                objInstantPayBeneficiary.IsAccountVerified = AccountVerified;
                db.tblInstantPay_Beneficiary.Add(objInstantPayBeneficiary);
                db.SaveChanges();
            }
            else
            {
                beneficiaryDetailInDb.BeneficiaryId = (beneficiaryDetailInDb.BeneficiaryId == "" ? benId : beneficiaryDetailInDb.BeneficiaryId);
                beneficiaryDetailInDb.Bank = (beneficiaryDetailInDb.Bank == "" ? getBankNameFromAPI(benAccountNo, benIFSC) : beneficiaryDetailInDb.Bank);
                db.Entry(beneficiaryDetailInDb).State = EntityState.Modified;
                db.Entry(beneficiaryDetailInDb).Property(x => x.MobileNo).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.ifsc).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.AccountNo).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.RemitterId).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.EntityId).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.Name).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.OTP).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.OTPVerifiedDateTime).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.IsOTPVerified).IsModified = false;
                db.Entry(beneficiaryDetailInDb).Property(x => x.IsAccountVerified).IsModified = false;
                db.SaveChanges();
            }
        }

        protected string getBankNameFromAPI(string accountNo, string IFSCCode)
        {
            string bankName = "";
            IRestResponse BankDetailsResponse = BankDetailsGet(accountNo);
            if (BankDetailsResponse.StatusCode == HttpStatusCode.OK)
            {
                bank_details_response bankResponse = JsonConvert.DeserializeObject<bank_details_response>(BankDetailsResponse.Content);
                List<bank_details_data_resp> objBankResponseList = bankResponse.data.Where(x => x.is_down == "0").ToList();
                if (objBankResponseList.Count > 0)
                {
                    var bankNameInList = objBankResponseList.Where(x => x.branch_ifsc.Trim().ToUpper() == IFSCCode.Trim().ToUpper()).Take(1).SingleOrDefault();
                    bankName = bankNameInList == null ? "" : bankNameInList.bank_name;
                }
            }
            return bankName;
        }

        [Authorize]
        [Route("api/instantpayDMT/verifyBeneficiaryAccount")]
        [HttpGet]
        public IHttpActionResult verifyBeneficiaryAccount(string remitterId, string beneficiaryAccountNo)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    Boolean beneficiaryAccountExistsInLocalDb = false;
                    string beneficiaryMobileNo = "";
                    string beneficiaryName = "";
                    string beneficiaryIFSC = "";
                    string beneficiaryId = "";
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    beneficiary_register_request BeneficiaryRequest = new beneficiary_register_request();
                    BeneficiaryRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    string remitterMobileNo = db.tblInstantPay_Remitter.SingleOrDefault(x => x.RemitterId == remitterId).MobileNo;
                    benefical_register_req request = new benefical_register_req();
                    tblInstantPay_Beneficiary objBeneficiaryDetailInDb = db.tblInstantPay_Beneficiary.Where(x => x.RemitterId == remitterId &&
                    x.AccountNo == beneficiaryAccountNo && x.EntityId == entityId).SingleOrDefault();
                    if (objBeneficiaryDetailInDb == null)
                    {
                        beneficiary_resp objBeneficiaryDataResponse = new beneficiary_resp();
                        objBeneficiaryDataResponse = getBeneficiaryDetailInRemitterDatabaseAPI(remitterMobileNo, beneficiaryAccountNo);
                        if (objBeneficiaryDataResponse != null)
                        {
                            beneficiaryMobileNo = objBeneficiaryDataResponse.mobile;
                            beneficiaryName = objBeneficiaryDataResponse.name;
                            beneficiaryIFSC = objBeneficiaryDataResponse.ifsc;
                            beneficiaryId = objBeneficiaryDataResponse.id;
                        }
                        else
                            return BadRequest("Unable to Verify Beneficiary Details. Kindly contact Support Team");
                    }
                    else
                        beneficiaryAccountExistsInLocalDb = true;

                    request.mobile = beneficiaryAccountExistsInLocalDb ? objBeneficiaryDetailInDb.MobileNo : beneficiaryMobileNo;
                    request.name = beneficiaryAccountExistsInLocalDb ? objBeneficiaryDetailInDb.Name : beneficiaryName;
                    request.remitterid = remitterId;
                    request.ifsc = beneficiaryAccountExistsInLocalDb ? objBeneficiaryDetailInDb.ifsc : beneficiaryIFSC;
                    request.account = beneficiaryAccountNo;
                    request.outletid = 1;
                    BeneficiaryRequest.request = request;

                    account_verification_request objAccountVerificationRequest = new account_verification_request();
                    objAccountVerificationRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objAccountVerificationRequest.request = new beneficiary_account_request();
                    objAccountVerificationRequest.request.remittermobile = remitterMobileNo;
                    objAccountVerificationRequest.request.account = beneficiaryAccountNo;
                    objAccountVerificationRequest.request.ifsc = beneficiaryAccountExistsInLocalDb ? objBeneficiaryDetailInDb.ifsc : beneficiaryIFSC;
                    objAccountVerificationRequest.request.agentid = beneficiaryAccountNo;
                    IRestResponse accountVerificationRestResponse = beneficiaryAccountVerification(objAccountVerificationRequest);
                    if (accountVerificationRestResponse.StatusCode == HttpStatusCode.OK)
                    {
                        account_verification_response objAccountVerificationResponse = JsonConvert.DeserializeObject<account_verification_response>(accountVerificationRestResponse.Content);
                        if (objAccountVerificationResponse.statuscode == "TXN" && objAccountVerificationResponse.status == "Transaction Successful")
                        {
                            if (objAccountVerificationResponse.data.verification_status.Trim().ToUpper() == "VERIFIED")
                            {
                                request.name = objAccountVerificationResponse.data.benename;
                                beneficiaryName = objAccountVerificationResponse.data.benename;
                                IRestResponse AddBeneficiaryResponse = addBeneficiary(BeneficiaryRequest);
                                if (AddBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    beneficiary_register_response BeneficiaryResponse = JsonConvert.DeserializeObject<beneficiary_register_response>(AddBeneficiaryResponse.Content);
                                    if (BeneficiaryResponse.statuscode.ToLower().Trim() == "txn" && BeneficiaryResponse.status.Trim() == "Transaction Successful")
                                    {
                                        string walletUpdateStatus = helper.updateWalletBalance(entityId, -3);
                                        string transactionStatus = helper.transactEntityAccount(entityId, 3, "", "Add Beneficiary in DMT", Convert.ToInt32(helper.transactionTypes.OtherCharges), false, 0, entityUserId, 3);
                                        addUpdateBeneficiaryInDb(beneficiaryId, beneficiaryName, beneficiaryMobileNo, beneficiaryIFSC, beneficiaryAccountNo,
                                            remitterId, "0", true,
                                            true, entityId, entityUserId);
                                        //if (objBeneficiaryDetailInDb == null)
                                        //{
                                        //    objBeneficiaryDetailInDb = new tblInstantPay_Beneficiary();
                                        //    objBeneficiaryDetailInDb = db.tblInstantPay_Beneficiary.Where(x => x.RemitterId == remitterId && x.AccountNo == beneficiaryAccountNo && x.EntityId == entityId).SingleOrDefault();
                                        //}

                                        //objBeneficiaryDetailInDb.Name = objAccountVerificationResponse.data.benename;
                                        //objBeneficiaryDetailInDb.BeneficiaryId = BeneficiaryResponse.data.beneficiary.id.ToString();
                                        //objBeneficiaryDetailInDb.OTP = "0";
                                        //objBeneficiaryDetailInDb.OTPVerifiedDateTime = helper.getIndianTime();
                                        //objBeneficiaryDetailInDb.IsOTPVerified = true;
                                        //objBeneficiaryDetailInDb.IsAccountVerified = true;
                                        //db.Entry(objBeneficiaryDetailInDb).State = EntityState.Modified;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.MobileNo).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.ifsc).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.AccountNo).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.RemitterId).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.EntityId).IsModified = false;
                                        //db.Entry(objBeneficiaryDetailInDb).Property(x => x.EntityUserId).IsModified = false;
                                        //db.SaveChanges();
                                        return Ok();
                                    }
                                    else
                                        return BadRequest(BeneficiaryResponse.statuscode);
                                }
                                else
                                    return BadRequest(AddBeneficiaryResponse.StatusCode.ToString());
                            }
                            else
                                return BadRequest("Unable to add Beneficiary Account Status : " + objAccountVerificationResponse.data.verification_status.Trim());
                        }
                        else
                            return BadRequest("Unable to add Beneficiary Account Status : " + objAccountVerificationResponse.status.Trim());
                    }
                    else
                        return BadRequest("Unable to add Beneficiary Account :" + accountVerificationRestResponse.StatusCode);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        protected beneficiary_resp getBeneficiaryDetailInRemitterDatabaseAPI(string remitterMobileNo, string beneficiaryAccountNo)
        {
            beneficiary_resp objBeneficiaryDataResponse = new beneficiary_resp();
            remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
            objRemitterDetailsRequest.request = new remitter_details_req();
            objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            objRemitterDetailsRequest.request.mobile = remitterMobileNo;
            IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
            if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
            {
                remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN")
                {
                    objBeneficiaryDataResponse = RemitterFetchAllResp.data.beneficiary.Where(x => x.account == beneficiaryAccountNo).SingleOrDefault();
                }
            }

            return objBeneficiaryDataResponse;
        }

        private tblInstantPay_Beneficiary beneficiaryAccountInDbIfExists(string accountNo, string remitterId)
        {
            var beneficiaryAccountNumberInDb = db.tblInstantPay_Beneficiary.Where(x => x.AccountNo == accountNo && x.RemitterId == remitterId).Take(1).SingleOrDefault();
            if (beneficiaryAccountNumberInDb == null)
                beneficiaryAccountNumberInDb = db.tblInstantPay_Beneficiary.Where(x => x.AccountNo == accountNo).Take(1).SingleOrDefault();
            return beneficiaryAccountNumberInDb;
        }

        private string verifyBeneficiary_Account(account_verification_request objAccountVerificationRequest, long entityId, long entityUserId)
        {
            string beneficiaryName = "";
            account_verification_response objAccountVerificationResponse = new account_verification_response();
            IRestResponse accountVerificationRestResponse = beneficiaryAccountVerification(objAccountVerificationRequest);
            if (accountVerificationRestResponse.StatusCode == HttpStatusCode.OK)
            {
                objAccountVerificationResponse = JsonConvert.DeserializeObject<account_verification_response>(accountVerificationRestResponse.Content);
                if (objAccountVerificationResponse.statuscode == "TXN" && objAccountVerificationResponse.status == "Transaction Successful")
                {
                    if (objAccountVerificationResponse.data.verification_status.Trim().ToUpper() == "VERIFIED")
                    {
                        beneficiaryName = objAccountVerificationResponse.data.benename;
                        string walletUpdateStatus = helper.updateWalletBalance(entityId, -3);
                        string transactionStatus = helper.transactEntityAccount(entityId, 3, "", "Add Beneficiary in DMT", Convert.ToInt32(helper.transactionTypes.OtherCharges), false, 0, entityUserId, 3);
                    }
                }
            }
            return beneficiaryName;
        }

        [Authorize]
        [ResponseType(typeof(beneficiary_register_validate_request))]
        [Route("api/instantPayDMT/BeneficiaryResendotp")]
        [HttpPost]
        public IHttpActionResult BeneficiaryResendotp(beneficiary_resend_otp_request objbeneficiaryResendotpRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    beneficiary_resend_otp_request objBeneficiaryRequest = new beneficiary_resend_otp_request();
                    beneficiary_resend_otp_req Request = new beneficiary_resend_otp_req();
                    objBeneficiaryRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    Request.remitterid = objbeneficiaryResendotpRequest.request.remitterid;
                    Request.beneficiaryid = objbeneficiaryResendotpRequest.request.beneficiaryid;
                    Request.outletid = 1;
                    objBeneficiaryRequest.request = Request;
                    IRestResponse response;
                    string URL = "dmi/beneficiary_resend_otp";
                    response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objBeneficiaryRequest);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        beneficiary_registration_resend_otp_response BeneficiaryResendResponse = JsonConvert.DeserializeObject<beneficiary_registration_resend_otp_response>(response.Content);
                        if (BeneficiaryResendResponse.statuscode.ToLower() == "txn")
                        {
                            return Ok(BeneficiaryResendResponse.data.ToLower());
                        }
                        else
                        {
                            return Ok(BeneficiaryResendResponse.status.ToString());
                        }
                    }
                    else
                    {
                        return BadRequest(response.StatusDescription.ToString());
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(beneficiary_register_validate_request))]
        [Route("api/instantPayDMT/BeneficiaryValidated")]
        [HttpPost]
        public IHttpActionResult BeneficiaryValidated(beneficiary_register_validate_request objbeneficiaryValidateRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var DBBeneficiaryCheck = db.tblInstantPay_Beneficiary.SingleOrDefault(e => e.BeneficiaryId == objbeneficiaryValidateRequest.request.beneficiaryid && e.IsOTPVerified == false);
                    if (DBBeneficiaryCheck != null)
                    {
                        beneficiary_register_validate_request BeneficiaryValidateReq = new beneficiary_register_validate_request();
                        BeneficiaryValidateReq.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        beneficiary_register_validate_req request = new beneficiary_register_validate_req();
                        request.beneficiaryid = objbeneficiaryValidateRequest.request.beneficiaryid;
                        request.remitterid = objbeneficiaryValidateRequest.request.remitterid;
                        request.otp = objbeneficiaryValidateRequest.request.otp;
                        request.outletid = 1;
                        BeneficiaryValidateReq.request = request;

                        IRestResponse BeneficiaryValidResponse = Beneficiary_Validate(BeneficiaryValidateReq);
                        if (BeneficiaryValidResponse.StatusCode == HttpStatusCode.OK)
                        {
                            Boolean detailsFound = false;
                            beneficiary_registration_validate_response BeneficiaryValidateResponse = JsonConvert.DeserializeObject<beneficiary_registration_validate_response>(BeneficiaryValidResponse.Content);
                            if (BeneficiaryValidateResponse.statuscode.ToLower() == "txn")
                            {
                                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                                var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                                tblInstantPay_Beneficiary BeneficiaryDetailInDb = db.tblInstantPay_Beneficiary.SingleOrDefault(e => e.BeneficiaryId.Trim() == BeneficiaryValidateReq.request.beneficiaryid && e.IsOTPVerified == false);
                                if (BeneficiaryDetailInDb != null)
                                {

                                    if (BeneficiaryValidateResponse.data.ToLower() == "success")
                                    {
                                        BeneficiaryDetailInDb.IsOTPVerified = true;
                                    }
                                    BeneficiaryDetailInDb.OTP = BeneficiaryValidateReq.request.otp;
                                    BeneficiaryDetailInDb.OTPVerifiedDateTime = helper.getIndianTime();
                                    db.Entry(BeneficiaryDetailInDb).State = EntityState.Modified;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.Name).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.MobileNo).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.ifsc).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.AccountNo).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.RemitterId).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.BeneficiaryId).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.EntityId).IsModified = false;
                                    db.Entry(BeneficiaryDetailInDb).Property(x => x.EntityUserId).IsModified = false;
                                    db.SaveChanges();
                                    detailsFound = true;

                                }
                                else
                                {
                                    detailsFound = false;
                                }
                                return Ok(detailsFound);
                            }
                            else
                                return Ok(BeneficiaryValidateResponse.status.ToString());
                        }
                        else
                        {
                            return Ok(BeneficiaryValidResponse.StatusDescription.ToString());
                        }
                    }
                    else
                        return Ok("Beneficiary Not Founded. Or Already Validate");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(InstantPayBeneficiaryVM))]
        [HttpGet]
        [Route("api/instantPayDMT/getBeneficiaryFetchAll")]
        public IHttpActionResult getBeneficiaryFetchAll(string RemitterMobileNo, int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                InstantPayBeneficiaryVM objInstantPayBeneficiaryVM = new InstantPayBeneficiaryVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    remitter_details_request objremitter_details = new remitter_details_request();
                    objremitter_details.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    remitter_details_req Request = new remitter_details_req();
                    Request.mobile = RemitterMobileNo;
                    objremitter_details.request = Request;
                    IRestResponse RemitterFetchAllResponse = fetchRemitterDetails(objremitter_details);
                    if (RemitterFetchAllResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(RemitterFetchAllResponse.Content);
                        if (RemitterFetchAllResp.statuscode.ToLower() == "txn")
                        {
                            objInstantPayBeneficiaryVM.objRemitterDetails = RemitterFetchAllResp.data.remitter;
                            if (RemitterFetchAllResp.data != null)
                            {
                                objInstantPayBeneficiaryVM.totalRecords = RemitterFetchAllResp.data.beneficiary.Count();
                                if (!String.IsNullOrEmpty(searchValue))
                                {
                                    objInstantPayBeneficiaryVM.objbeneficiaryList = RemitterFetchAllResp.data.beneficiary.Where(e => e.name.ToLower().Contains(searchValue.Trim().ToLower())
                                    || e.account.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                    || e.mobile.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                    || e.bank.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                    || e.name.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                    || e.ifsc.Trim().ToLower() == searchValue.Trim().ToLower()).ToList();

                                    objInstantPayBeneficiaryVM.filterRecords = objInstantPayBeneficiaryVM.objbeneficiaryList.Count();
                                }
                                else
                                    objInstantPayBeneficiaryVM.filterRecords = RemitterFetchAllResp.data.beneficiary.Count();

                                if (sortDirection == "asc")
                                    objInstantPayBeneficiaryVM.objbeneficiaryList = String.IsNullOrEmpty(searchValue) ?
                                        RemitterFetchAllResp.data.beneficiary.OrderBy(e => e.name).ThenBy(e => e.bank).Skip(pageIndex).Take(pageSize).ToList<beneficiary_resp>() :
                                        objInstantPayBeneficiaryVM.objbeneficiaryList.OrderBy(e => e.name).ThenBy(e => e.bank).Skip(pageIndex).Take(pageSize).ToList<beneficiary_resp>();
                                else
                                    objInstantPayBeneficiaryVM.objbeneficiaryList = String.IsNullOrEmpty(searchValue) ?
                                        RemitterFetchAllResp.data.beneficiary.OrderByDescending(e => e.name).ThenBy(e => e.bank).Skip(pageIndex).Take(pageSize).ToList<beneficiary_resp>() :
                                        objInstantPayBeneficiaryVM.objbeneficiaryList.OrderByDescending(e => e.name).ThenBy(e => e.bank).Skip(pageIndex).Take(pageSize).ToList<beneficiary_resp>();

                                //Adding Beneficiary List from API to local database (All verified List)
                                List<beneficiary_resp> APIbeneficiaryList = new List<beneficiary_resp>();
                                APIbeneficiaryList = RemitterFetchAllResp.data.beneficiary.Where(x => x.imps.Trim() == "1").ToList();
                                if (APIbeneficiaryList.Count > 0)
                                {
                                    foreach (beneficiary_resp objBeneficiaryData in APIbeneficiaryList)
                                    {
                                        int recordStatus = addUpdateBeneficiaryData(objBeneficiaryData, RemitterFetchAllResp.data.remitter.id, entityId, entityUserId);
                                    }
                                }

                                //Adding unverified beneficiary list from local database
                                //List<tblInstantPay_Beneficiary> localhostBeneficiaryList = db.tblInstantPay_Beneficiary.Where(x => x.RemitterId == RemitterFetchAllResp.data.remitter.id
                                //&& x.IsAccountVerified == false && x.EntityId == entityId).ToList();
                                //if (localhostBeneficiaryList.Count > 0)
                                //{
                                //    beneficiary_resp objBeneficiaryResp = new beneficiary_resp();
                                //    foreach (tblInstantPay_Beneficiary objData in localhostBeneficiaryList)
                                //    {
                                //        var detailFoundInMasterDb = objInstantPayBeneficiaryVM.objbeneficiaryList.Where(x => x.account == objData.AccountNo).Take(1).SingleOrDefault();
                                //        if (detailFoundInMasterDb == null)
                                //        {
                                //            objBeneficiaryResp = new beneficiary_resp();
                                //            objBeneficiaryResp.account = objData.AccountNo;
                                //            objBeneficiaryResp.ifsc = objData.ifsc;
                                //            objBeneficiaryResp.imps = "0";
                                //            objBeneficiaryResp.mobile = objData.MobileNo;
                                //            objBeneficiaryResp.name = objData.Name;
                                //            objBeneficiaryResp.bank = string.Empty;
                                //            objBeneficiaryResp.id = string.Empty;
                                //            objInstantPayBeneficiaryVM.objbeneficiaryList.Add(objBeneficiaryResp);
                                //        }
                                //    }

                                //    objInstantPayBeneficiaryVM.filterRecords = objInstantPayBeneficiaryVM.filterRecords + localhostBeneficiaryList.Count;
                                //    objInstantPayBeneficiaryVM.totalRecords = objInstantPayBeneficiaryVM.totalRecords + localhostBeneficiaryList.Count;
                                //}

                                return Ok(objInstantPayBeneficiaryVM);
                            }
                            else
                            {
                                return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                            }
                        }
                        else
                        {
                            return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                        }
                    }
                    else
                    {
                        return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                    }

                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(InstantPayBeneficiaryVM))]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterBeneficiaryDetail4Transfer")]
        public IHttpActionResult getRemitterBeneficiaryDetail4Transfer(string RemitterMobileNo)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                InstantPayBeneficiaryVM objInstantPayBeneficiaryVM = new InstantPayBeneficiaryVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    remitter_details_request objremitter_details = new remitter_details_request();
                    objremitter_details.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    remitter_details_req Request = new remitter_details_req();
                    Request.mobile = RemitterMobileNo;
                    objremitter_details.request = Request;
                    IRestResponse RemitterFetchAllResponse = fetchRemitterDetails(objremitter_details);
                    if (RemitterFetchAllResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(RemitterFetchAllResponse.Content);
                        objInstantPayBeneficiaryVM.objRemitterDetails = RemitterFetchAllResp.data.remitter;
                        objInstantPayBeneficiaryVM.objbeneficiaryList = RemitterFetchAllResp.data.beneficiary;
                        if (RemitterFetchAllResp.statuscode.ToLower() == "txn")
                        {
                            return Ok(objInstantPayBeneficiaryVM);
                        }
                        else
                        {
                            return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                        }
                    }
                    else
                    {
                        return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                    }

                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(InstantPayBeneficiaryVM))]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterBeneficiaryDetail4DirectPayout")]
        public IHttpActionResult getRemitterBeneficiaryDetail4DirectPayout(string RemitterMobileNo)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                InstantPayBeneficiaryVM objInstantPayBeneficiaryVM = new InstantPayBeneficiaryVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    remitter_details_request objremitter_details = new remitter_details_request();
                    objremitter_details.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    remitter_details_req Request = new remitter_details_req();
                    Request.mobile = RemitterMobileNo;
                    objremitter_details.request = Request;
                    IRestResponse RemitterFetchAllResponse = fetchRemitterDetails(objremitter_details);
                    if (RemitterFetchAllResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(RemitterFetchAllResponse.Content);
                        objInstantPayBeneficiaryVM.objRemitterDetails = RemitterFetchAllResp.data.remitter;
                        objInstantPayBeneficiaryVM.objbeneficiaryList = RemitterFetchAllResp.data.beneficiary;
                        if (RemitterFetchAllResp.statuscode.ToLower() == "txn")
                        {
                            objInstantPayBeneficiaryVM.objRemitterDetails.remaininglimit = getSenderLimit4PayoutDirect(RemitterMobileNo);
                            return Ok(objInstantPayBeneficiaryVM);
                        }
                        else
                        {
                            return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                        }
                    }
                    else
                    {
                        return BadRequest(RemitterFetchAllResponse.StatusDescription.ToString());
                    }

                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getBeneficiaryDetailByBeneficiaryId")]
        public IHttpActionResult getBeneficiaryDetailByBeneficiaryId(string beneficiaryId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    tblInstantPay_Beneficiary objBeneficiaryData = db.tblInstantPay_Beneficiary.Where(x => x.BeneficiaryId == beneficiaryId).SingleOrDefault();
                    if (objBeneficiaryData != null)
                        return Ok(objBeneficiaryData);
                    else
                        return BadRequest("Beneficiary data not found");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/getRemitterDetailByRemitterId")]
        public IHttpActionResult getRemitterDetailByRemitterId(string remitterId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    tblInstantPay_Remitter objRemitterData = db.tblInstantPay_Remitter.Where(x => x.RemitterId == remitterId).SingleOrDefault();
                    if (objRemitterData != null)
                        return Ok(objRemitterData);
                    else
                        return BadRequest("Remitter data not found");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(tblInstantPay_Remitter))]
        [Route("api/instantPayDMT/RemitterById")]
        [HttpGet]
        public IHttpActionResult RemitterById(string RemitterId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    tblInstantPay_Remitter objRemitter = db.tblInstantPay_Remitter.SingleOrDefault(e => e.RemitterId == RemitterId);
                    if (objRemitter != null)
                    {
                        return Ok(objRemitter);
                    }
                    else
                        return Ok("Remitter Not Founded");
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(beneficiary_remove_request))]
        [Route("api/instantPayDMT/removeBeneficiary")]
        [HttpPost]
        public IHttpActionResult removeBeneficiary(beneficiary_remove_request objBeneficiaryRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    beneficiary_remove_request objBeneficiaryDeleteRequest = new beneficiary_remove_request();
                    beneficiary_remove_req Request = new beneficiary_remove_req();
                    objBeneficiaryDeleteRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    Request.beneficiaryid = objBeneficiaryRequest.request.beneficiaryid;
                    Request.remitterid = objBeneficiaryRequest.request.remitterid;
                    Request.outletid = 1;
                    objBeneficiaryDeleteRequest.request = Request;

                    IRestResponse removeBeneficiaryResponse = DeleteBeneficiary(objBeneficiaryDeleteRequest);
                    if (removeBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                    {
                        beneficiary_remove_response beneficiaryResponse = JsonConvert.DeserializeObject<beneficiary_remove_response>(removeBeneficiaryResponse.Content);
                        if (beneficiaryResponse.statuscode.ToLower() == "txn")
                        {
                            if (beneficiaryResponse.data.otp == 1)
                            {
                                return Ok(true);
                            }
                            else
                            {
                                return Ok(false);
                            }
                        }
                        else
                            return Ok(removeBeneficiaryResponse.StatusDescription.ToString());
                    }
                    else
                        return Ok(removeBeneficiaryResponse.StatusDescription.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(beneficiary_remove_validate_request))]
        [Route("api/instantPayDMT/removeBeneficiaryValidate")]
        [HttpPost]
        public IHttpActionResult removeBeneficiaryValidate(beneficiary_remove_validate_request objBeneficiaryVAlidateRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    beneficiary_remove_validate_request objBeneficiaryValidRequest = new beneficiary_remove_validate_request();
                    beneficiary_remove_validate_req Request = new beneficiary_remove_validate_req();
                    objBeneficiaryValidRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    Request.beneficiaryid = objBeneficiaryVAlidateRequest.request.beneficiaryid;
                    Request.remitterid = objBeneficiaryVAlidateRequest.request.remitterid;
                    Request.otp = objBeneficiaryVAlidateRequest.request.otp;
                    Request.outletid = 1;
                    objBeneficiaryValidRequest.request = Request;

                    IRestResponse RemoveBeneficiaryValidateResponse = DeleteValidateBeneficiary(objBeneficiaryValidRequest);
                    if (RemoveBeneficiaryValidateResponse.StatusCode == HttpStatusCode.OK)
                    {
                        beneficiary_remove_validate_response beneficiaryValidateResponse = JsonConvert.DeserializeObject<beneficiary_remove_validate_response>(RemoveBeneficiaryValidateResponse.Content);
                        if (beneficiaryValidateResponse.statuscode.ToLower() == "txn")
                        {
                            var objBeneficiary = db.tblInstantPay_Beneficiary.SingleOrDefault(e => e.BeneficiaryId == objBeneficiaryVAlidateRequest.request.beneficiaryid);
                            if (objBeneficiary != null)
                            {
                                db.tblInstantPay_Beneficiary.Remove(objBeneficiary);
                                db.SaveChanges();
                                return Ok(true);
                            }
                            else
                            {
                                return Ok(false);
                            }
                        }
                        else
                            return Ok("Beneficiary Not Deleted");
                    }
                    else
                        return Ok(RemoveBeneficiaryValidateResponse.StatusDescription.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(transfer_request))]
        [Route("api/instantPayDMT/FundTransfer")]
        [HttpPost]
        public IHttpActionResult FundTransfer(transfer_request objtransferRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (helper.checkProductPartialStopStatus(4))
                        return BadRequest("We are upgrading our Money Transfer system. We will back LIVE soon!!!");
                    else
                    {
                        if (helper.checkEntityKYC(entityId))
                        {
                            transfer_request objFundTransferRequest = new transfer_request();
                            transfer_req Request = new transfer_req();
                            objFundTransferRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                            Request.remittermobile = objtransferRequest.request.remittermobile;
                            Request.beneficiaryid = objtransferRequest.request.beneficiaryid;
                            Request.agentid = objtransferRequest.request.agentid; //helper.GetRandomPasswordUsingGUID(8).ToUpper();
                            Request.mode = objtransferRequest.request.mode;
                            Request.amount = objtransferRequest.request.amount;

                            int companyId = Convert.ToInt32(identityCompanyId.Value);
                            decimal remitterRemainingLimit = checkRemitterMonthlyTransferLimit(objtransferRequest.request.remittermobile);
                            if (remitterRemainingLimit >= Convert.ToDecimal(objtransferRequest.request.amount))
                            {
                                decimal transferAmount = Convert.ToDecimal(objtransferRequest.request.amount);
                                transferAmount = transferAmount + (transferAmount * 1 / 100);
                                if (helper.checkWalletBalance(entityId, transferAmount))
                                {
                                    IRestResponse instantPayWalletResponse = balanceCheck(ConfigurationManager.AppSettings["InstantPayToken"].ToString());
                                    if (instantPayWalletResponse.StatusCode == HttpStatusCode.OK)
                                    {
                                        checkWallet objCheckWallet = new checkWallet();
                                        objCheckWallet = JsonConvert.DeserializeObject<checkWallet>(instantPayWalletResponse.Content);
                                        if (Convert.ToDouble(objCheckWallet.data.available) > Convert.ToDouble(objtransferRequest.request.amount))
                                        {
                                            Int64 amountCount = Convert.ToInt64(objtransferRequest.request.amount) / 5000;
                                            double totalAmount2Transfer = Convert.ToDouble(objtransferRequest.request.amount);
                                            double amountTransfered = 0;
                                            double finalPendingAmount2Transfer = 0;
                                            string parentClientRefId = objtransferRequest.request.agentid;
                                            string moneyTransferStatus = "";
                                            for (int i = 0; i < amountCount; i++)
                                            {
                                                if (helper.checkWalletBalance(entityId, 5000))
                                                {
                                                    Request.amount = "5000";
                                                    if (i == 0)
                                                    {
                                                        Request.agentid = parentClientRefId;
                                                    }
                                                    else
                                                    {
                                                        Request.agentid = parentClientRefId + "" + i.ToString();
                                                    }
                                                    objFundTransferRequest.request = Request;
                                                    string moneyTransferCommitStatus = instantPayMoneyTransferCommit(objFundTransferRequest, parentClientRefId, entityId, entityUserId, objtransferRequest.fromMobileDevice, companyId);
                                                    moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                                }
                                                else
                                                    return BadRequest("Oops! Wallet Balance is too Low");

                                                amountTransfered += 5000;
                                            }
                                            if (amountTransfered < totalAmount2Transfer)
                                            {
                                                finalPendingAmount2Transfer = totalAmount2Transfer - amountTransfered;
                                                if (helper.checkWalletBalance(entityId, (decimal)finalPendingAmount2Transfer))
                                                {
                                                    Request.amount = finalPendingAmount2Transfer.ToString();
                                                    if (amountCount == 0)
                                                    {
                                                        Request.agentid = parentClientRefId;
                                                    }
                                                    else
                                                    {
                                                        Request.agentid = parentClientRefId + "" + (amountCount).ToString();
                                                    }
                                                    objFundTransferRequest.request = Request;
                                                    string moneyTransferCommitStatus = instantPayMoneyTransferCommit(objFundTransferRequest, parentClientRefId, entityId, entityUserId, objtransferRequest.fromMobileDevice, companyId);
                                                    moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                                }
                                                else
                                                {
                                                    return BadRequest("Oops! Wallet Balance is too Low");
                                                }
                                            }
                                            return Ok(moneyTransferStatus);
                                        }
                                        else
                                        {
                                            return BadRequest("Something went wrong, balance is low. Status Code : AgencyBalance");
                                        }
                                    }
                                    else
                                    {
                                        return BadRequest("Something went wrong while checking wallet balance. Status Code : checkWallet");
                                    }
                                }
                                else
                                {
                                    return BadRequest("Oops! Wallet Balance is too Low");
                                }
                            }
                            else
                            {
                                return BadRequest("Oops! Remitter Transfer Limit is low.");
                            }
                        }
                        else
                        {
                            return BadRequest(helper.incompleteKYC);
                        }
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        protected decimal checkRemitterMonthlyTransferLimit(string remitterMobileNo)
        {
            remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
            objRemitterDetailsRequest.request = new remitter_details_req();
            objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            objRemitterDetailsRequest.request.mobile = remitterMobileNo;
            IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
            if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
            {
                remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN" && RemitterFetchAllResp.status.Trim().ToLower() == "transaction successful")
                {
                    return RemitterFetchAllResp.data.remitter.remaininglimit;
                }
                else
                    return 0;
            }
            else
                return 0;
        }

        protected string instantPayMoneyTransferCommit(transfer_request objFundTransferRequest, string parentClientRefId, long entityId, long entityUserId, Boolean fromMobileDevice, Int32 companyId)
        {
            IRestResponse FundTransferResponse = TransferMoney(objFundTransferRequest);
            if (FundTransferResponse.StatusCode == HttpStatusCode.OK)
            {
                transfer_response beneficiaryFundTransferResponse = JsonConvert.DeserializeObject<transfer_response>(FundTransferResponse.Content);
                tblInstantPay_Transaction objtblInstantPay_Transaction = new tblInstantPay_Transaction();
                objtblInstantPay_Transaction.InstantPayTransactionId = 0;
                objtblInstantPay_Transaction.RemitterMobileNo = objFundTransferRequest.request.remittermobile;
                objtblInstantPay_Transaction.Name = beneficiaryFundTransferResponse.data.name;
                objtblInstantPay_Transaction.Mode = objFundTransferRequest.request.mode;
                objtblInstantPay_Transaction.BeneficiaryId = objFundTransferRequest.request.beneficiaryid;
                objtblInstantPay_Transaction.ipay_id = beneficiaryFundTransferResponse.data.ipay_id;
                objtblInstantPay_Transaction.RequestApi = JsonConvert.SerializeObject(objFundTransferRequest);
                objtblInstantPay_Transaction.ResponseApi = JsonConvert.SerializeObject(beneficiaryFundTransferResponse);
                objtblInstantPay_Transaction.StatusCode = beneficiaryFundTransferResponse.statuscode;
                objtblInstantPay_Transaction.APIStatus = beneficiaryFundTransferResponse.status;
                objtblInstantPay_Transaction.Amount = beneficiaryFundTransferResponse.data.amount.ToString();
                objtblInstantPay_Transaction.Ref_no = beneficiaryFundTransferResponse.data.ref_no.ToString();
                objtblInstantPay_Transaction.Opr_id = beneficiaryFundTransferResponse.data.opr_id.ToString();
                objtblInstantPay_Transaction.Opening_bal = beneficiaryFundTransferResponse.data.opening_bal.ToString();
                objtblInstantPay_Transaction.Locked_amt = beneficiaryFundTransferResponse.data.locked_amt.ToString();
                objtblInstantPay_Transaction.CreatedDateTime = helper.getIndianTime();
                objtblInstantPay_Transaction.Transaction_Status = beneficiaryFundTransferResponse.status;
                objtblInstantPay_Transaction.EntityId = entityId;
                objtblInstantPay_Transaction.FromMobileDevice = fromMobileDevice;
                objtblInstantPay_Transaction.EntityUserId = entityUserId;
                objtblInstantPay_Transaction.ClientRefId = objFundTransferRequest.request.agentid;
                objtblInstantPay_Transaction.ParentClientRefId = parentClientRefId;
                objtblInstantPay_Transaction.IPAddress = helper.getClientIPAddress();
                objtblInstantPay_Transaction.Latitude = string.Empty;
                objtblInstantPay_Transaction.Longitude = string.Empty;
                objtblInstantPay_Transaction.Alert_Mobile = string.Empty;
                objtblInstantPay_Transaction.Alert_Email = string.Empty;
                objtblInstantPay_Transaction.PayoutDirectRemark = string.Empty;
                objtblInstantPay_Transaction.IsPayoutDirect = false;
                db.tblInstantPay_Transaction.Add(objtblInstantPay_Transaction);
                db.SaveChanges();
                if (beneficiaryFundTransferResponse.statuscode.ToLower().Trim() == "txn")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(beneficiaryFundTransferResponse.data.amount), entityId, entityUserId, beneficiaryFundTransferResponse.data.ipay_id, companyId, 4, false);
                }
                else if (beneficiaryFundTransferResponse.statuscode.ToLower().Trim() == "tup")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(beneficiaryFundTransferResponse.data.amount), entityId, entityUserId, beneficiaryFundTransferResponse.data.ipay_id, companyId, 4, false);
                }
                return beneficiaryFundTransferResponse.status;
            }
            else
                return FundTransferResponse.StatusDescription.ToString();
        }

        [Authorize]
        [ResponseType(typeof(InstantPayDMTTransactionsVM))]
        [HttpGet]
        [Route("api/instantPayDMT/transactions")]
        public IHttpActionResult transactions(string customerId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.RemitterMobileNo == customerId &&
                    x.EntityId == entityId &&
                    (transactionType == null || x.Mode.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.StatusCode == transactionStatus) &&
                        ((x.CreatedDateTime.Value >= dFromDate && x.CreatedDateTime.Value <= dToDate))).ToList();

                    objTransactions.totalRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.Where(s => s.EntityId == entityId &&
                        (s.RemitterMobileNo == searchValue.Trim()
                        || s.ipay_id == searchValue.Trim()
                        || s.RemitterName.Trim().Contains(searchValue.Trim())
                        || s.Surname.Trim().Contains(searchValue.Trim())
                        || s.Name.Trim().Contains(searchValue.Trim()))).ToList();

                        objTransactions.filterRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                    }
                    else
                        objTransactions.filterRecords = objTransactions.totalRecords;

                    if (sortDirection == "asc")
                    {
                        if (pageSize > 0)
                        {
                            objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                        }
                        else { objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderBy(s => s.CreatedDateTime).ToList<QryInstantPay_DMT_Transactions>(); }
                    }
                    else
                    {
                        if (pageSize > 0)
                        {
                            objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                        }
                        else { objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).ToList<QryInstantPay_DMT_Transactions>(); }
                    }
                    return Ok(objTransactions);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(InstantPayDMTTransactionsVM))]
        [HttpGet]
        [Route("api/instantPayDMT/transactionsHistory")]
        public IHttpActionResult transactionsHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.EntityId == entityId &&
                    (transactionType == null || x.Mode.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.StatusCode == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime.Value >= dFromDate && x.CreatedDateTime.Value <= dToDate))).ToList();
                }
                else
                {
                    objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.CompanyId == companyId &&
                    (transactionType == null || x.Mode.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.StatusCode == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime.Value >= dFromDate && x.CreatedDateTime.Value <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.Where(s => s.InstantPayTransactionId > 0 &&
                    (s.RemitterMobileNo == searchValue.Trim()
                    || s.AgencyName == searchValue.Trim()
                    || s.AccountNo == searchValue.Trim()
                    || s.Mode == searchValue.Trim()
                    || s.ipay_id == searchValue.Trim()
                    || s.RemitterName.Trim().Contains(searchValue.Trim())
                    || s.Surname.Trim().Contains(searchValue.Trim())
                    || s.Name.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(InstantPayDMTTransactionsVM))]
        [HttpGet]
        [Route("api/instantPayDMT/retailerTransactionHistory")]
        public IHttpActionResult retailerTransactionHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.masterDistributor))
                    {
                        var param = new SqlParameter[] {
                            new SqlParameter ("@EntityId", entityId),
                            new SqlParameter ("@TransactionType", transactionType),
                            new SqlParameter ("@TransactionStatus", transactionStatus),
                            new SqlParameter ("@FromDate", dFromDate),
                            new SqlParameter ("@ToDate", dToDate),
                            new SqlParameter ("@SearchEntityId", searchEntityId)
                        };
                        objTransactions.objInstantPayDMTTransactionsList = db.Database.SqlQuery<QryInstantPay_DMT_Transactions>("EXEC [dbo].[DMT2_Retailer_TransactionReport_4_MD] @EntityId={0}, @TransactionType={1}, @TransactionStatus={2}, @FromDate={3}, @ToDate={4}, @SearchEntityId={5}",
                            entityId, transactionType, transactionStatus, dFromDate, dToDate, searchEntityId).ToList();
                    }
                    else
                    {
                        objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.ParentEntityId == entityId &&
                        (transactionType == null || x.Mode.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                            (transactionStatus == null || x.StatusCode == transactionStatus) &&
                            (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                            ((x.CreatedDateTime.Value >= dFromDate && x.CreatedDateTime.Value <= dToDate))).ToList();
                    }
                }
                else
                {
                    objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.CompanyId == companyId &&
                    (transactionType == null || x.Mode.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.StatusCode == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime.Value >= dFromDate && x.CreatedDateTime.Value <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.Where(s => s.InstantPayTransactionId > 0 &&
                    (s.RemitterMobileNo == searchValue.Trim()
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.AccountNo.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Mode == searchValue.Trim()
                    || s.ipay_id == searchValue.Trim()
                    || s.RemitterName.Trim().Contains(searchValue.Trim())
                    || s.Bank.Trim().Contains(searchValue.Trim())
                    || s.Surname.Trim().Contains(searchValue.Trim())
                    || s.Name.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objInstantPayDMTTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objInstantPayDMTTransactionsList = objTransactions.objInstantPayDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_DMT_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(transactionReceipt))]
        [HttpGet]
        [Route("api/instantPayDMT/transactionsReceipt")]
        public IHttpActionResult transactionsReceipt(long instantPayTransactionId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                InstantPayDMTTransactionsVM objTransactions = new InstantPayDMTTransactionsVM();
                InstantPayDMTTransactionsVM objTransactionsWithParentClientRefId = new InstantPayDMTTransactionsVM();
                transactionReceipt objTransactionReceipt = new transactionReceipt();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    string txnIds = "";
                    decimal totalAmount = 0;
                    var transactionInDB = db.tblInstantPay_Transaction.Where(x => x.InstantPayTransactionId == instantPayTransactionId).SingleOrDefault();
                    if (transactionInDB != null)
                    {
                        objTransactions.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.ParentClientRefId == transactionInDB.ParentClientRefId).ToList();
                        objTransactionsWithParentClientRefId.objInstantPayDMTTransactionsList = db.QryInstantPay_DMT_Transactions.Where(x => x.ParentClientRefId == transactionInDB.ParentClientRefId).ToList();
                        if (objTransactionsWithParentClientRefId.objInstantPayDMTTransactionsList.Count > 0)
                        {
                            foreach (QryInstantPay_DMT_Transactions objData in objTransactionsWithParentClientRefId.objInstantPayDMTTransactionsList)
                            {
                                totalAmount += Convert.ToDecimal(objData.Amount);
                                txnIds = (txnIds.Length == 0 ? objData.Ref_no + " (" + objData.APIStatus + ")" : txnIds + ", " + objData.Ref_no + " (" + objData.APIStatus + ")");
                            }
                        }

                        if (txnIds.Length > 0)
                        {
                            decimal serviceCharge = totalAmount * Convert.ToDecimal(0.01);
                            totalAmount = totalAmount + (serviceCharge <= 10 ? 10 : serviceCharge);
                            objTransactions.objInstantPayDMTTransactionsList.Select(x => x.Ref_no = txnIds).ToList();
                            objTransactions.objInstantPayDMTTransactionsList.Select(x => x.Amount = totalAmount.ToString()).ToList();
                        }
                    }
                    QryInstantPay_DMT_Transactions objFinalData = objTransactionsWithParentClientRefId.objInstantPayDMTTransactionsList[objTransactionsWithParentClientRefId.objInstantPayDMTTransactionsList.Count - 1];
                    if (objFinalData != null)
                    {
                        //Fetching remitter Details
                        remitter_details_request objremitter_details = new remitter_details_request();
                        objremitter_details.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        remitter_details_req Request = new remitter_details_req();
                        Request.mobile = objFinalData.RemitterMobileNo;
                        objremitter_details.request = Request;
                        IRestResponse RemitterFetchAllResponse = fetchRemitterDetails(objremitter_details);
                        if (RemitterFetchAllResponse.StatusCode == HttpStatusCode.OK)
                        {
                            remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(RemitterFetchAllResponse.Content);
                            if (RemitterFetchAllResp.statuscode.ToLower() == "txn" && RemitterFetchAllResp.status.Trim().ToLower() == "transaction successful")
                            {
                                var beneficiaryDetail = RemitterFetchAllResp.data.beneficiary.Where(x => x.id == objFinalData.BeneficiaryId).SingleOrDefault();
                                objTransactionReceipt.account = (beneficiaryDetail == null ? "" : beneficiaryDetail.account);
                                objTransactionReceipt.Name = (beneficiaryDetail == null ? "" : beneficiaryDetail.name);
                                objTransactionReceipt.bankName = (beneficiaryDetail == null ? "" : beneficiaryDetail.bank);
                                objTransactionReceipt.RemitterName = RemitterFetchAllResp.data.remitter.name;
                                objTransactionReceipt.RemitterMobileNo = RemitterFetchAllResp.data.remitter.mobile;
                            }
                        }
                        objTransactionReceipt.Amount = objFinalData.Amount;
                        objTransactionReceipt.AgencyName = objFinalData.AgencyName;
                        objTransactionReceipt.InstantPayTransactionId = objFinalData.InstantPayTransactionId;
                        objTransactionReceipt.Mode = objFinalData.Mode;
                        objTransactionReceipt.BeneficiaryId = objFinalData.BeneficiaryId;
                        objTransactionReceipt.ipay_id = objFinalData.ipay_id;
                        objTransactionReceipt.RequestApi = objFinalData.RequestApi;
                        objTransactionReceipt.ResponseApi = objFinalData.ResponseApi;
                        objTransactionReceipt.StatusCode = objFinalData.StatusCode;
                        objTransactionReceipt.APIStatus = objFinalData.APIStatus;
                        objTransactionReceipt.Ref_no = objFinalData.Ref_no;
                        objTransactionReceipt.CreatedDateTime = objFinalData.CreatedDateTime;
                        objTransactionReceipt.Transaction_Status = objFinalData.Transaction_Status;
                        objTransactionReceipt.EntityId = objFinalData.EntityId;
                        objTransactionReceipt.EntityUserId = objFinalData.EntityUserId;
                        objTransactionReceipt.Surname = objFinalData.Surname;
                        objTransactionReceipt.RemitterId = objFinalData.RemitterId;
                        objTransactionReceipt.AgencyName = objFinalData.AgencyName;
                        objTransactionReceipt.Telephone1 = objFinalData.Telephone1;
                        objTransactionReceipt.FromMobileDevice = objFinalData.FromMobileDevice;
                        objTransactionReceipt.ClientRefId = objFinalData.ClientRefId;
                        objTransactionReceipt.ParentClientRefId = objFinalData.ParentClientRefId;
                    }
                    return Ok(objTransactionReceipt);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion

        [Authorize]
        [HttpGet]
        [Route("api/instantPayDMT/updateRemitterBeneficiaryDetails")]
        public IHttpActionResult updateRemitterBeneficiaryDetails()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                int recordInserted = 0;
                int recordUpdated = 0;
                int recordStatus = 0;
                beneficiary_resp objBeneficiaryDataResponse = new beneficiary_resp();
                List<beneficiary_resp> beneficiaryList = new List<beneficiary_resp>();
                List<tblInstantPay_Remitter> objRemitterList = db.tblInstantPay_Remitter.Where(x => x.IsVerified == 1).ToList();
                foreach (tblInstantPay_Remitter objData in objRemitterList)
                {
                    remitter_details_request objRemitterDetailsRequest = new remitter_details_request();
                    objRemitterDetailsRequest.request = new remitter_details_req();
                    objRemitterDetailsRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objRemitterDetailsRequest.request.mobile = objData.MobileNo;
                    IRestResponse remitterDetailResponse = fetchRemitterDetails(objRemitterDetailsRequest);
                    if (remitterDetailResponse.StatusCode == HttpStatusCode.OK)
                    {
                        remitter_details_response RemitterFetchAllResp = JsonConvert.DeserializeObject<remitter_details_response>(remitterDetailResponse.Content);
                        if (RemitterFetchAllResp.statuscode.Trim().ToUpper() == "TXN")
                        {
                            if (RemitterFetchAllResp.data.remitter.is_verified == 1)
                            {
                                beneficiaryList = new List<beneficiary_resp>();
                                if (RemitterFetchAllResp.data.beneficiary != null)
                                {
                                    beneficiaryList = RemitterFetchAllResp.data.beneficiary;
                                    if (beneficiaryList.Count > 0)
                                    {
                                        foreach (beneficiary_resp objBeneficiaryData in beneficiaryList)
                                        {
                                            recordStatus = addUpdateBeneficiaryData(objBeneficiaryData, objData.RemitterId, objData.EntityId, objData.EntityUserId);
                                            recordInserted += recordStatus == 1 ? 1 : 0;
                                            recordUpdated += recordStatus == 2 ? 1 : 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return Ok("Total Records Inserted :" + recordInserted.ToString() + ", Total Records Updated : " + recordUpdated.ToString());
            }
            else
            {
                return Unauthorized();
            }
        }

        protected int addUpdateBeneficiaryData(beneficiary_resp objBeneficiary, string remitterId, long entityId, long entityUserId)
        {
            int recordStatus = 0;
            var beneficiaryDetailInDb = db.tblInstantPay_Beneficiary.Where(x => x.RemitterId == remitterId && x.BeneficiaryId == objBeneficiary.id).Take(1).SingleOrDefault();
            if (beneficiaryDetailInDb == null)
            {
                recordStatus = 1;
                tblInstantPay_Beneficiary objInstantPayBeneficiary = new tblInstantPay_Beneficiary();
                objInstantPayBeneficiary.instantPayBeneficiaryId = 0;
                objInstantPayBeneficiary.Name = objBeneficiary.name;
                objInstantPayBeneficiary.MobileNo = objBeneficiary.mobile;
                objInstantPayBeneficiary.ifsc = objBeneficiary.ifsc;
                objInstantPayBeneficiary.Bank = objBeneficiary.bank;
                objInstantPayBeneficiary.AccountNo = objBeneficiary.account;
                objInstantPayBeneficiary.RemitterId = remitterId;
                objInstantPayBeneficiary.BeneficiaryId = objBeneficiary.id;
                objInstantPayBeneficiary.CreatedDateTime = helper.getIndianTime();
                objInstantPayBeneficiary.OTP = "0";
                objInstantPayBeneficiary.OTPVerifiedDateTime = helper.getIndianTime();
                objInstantPayBeneficiary.EntityId = entityId;
                objInstantPayBeneficiary.EntityUserId = entityUserId;
                objInstantPayBeneficiary.IsOTPVerified = (objBeneficiary.status == "1" ? true : false);
                objInstantPayBeneficiary.IsAccountVerified = (objBeneficiary.imps == "1" ? true : false);
                db.tblInstantPay_Beneficiary.Add(objInstantPayBeneficiary);
                db.SaveChanges();
            }
            //else
            //{
            //    beneficiaryDetailInDb.BeneficiaryId = objBeneficiary.id;
            //    beneficiaryDetailInDb.Bank = objBeneficiary.bank;
            //    db.Entry(beneficiaryDetailInDb).State = EntityState.Modified;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.MobileNo).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.ifsc).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.AccountNo).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.RemitterId).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.EntityId).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.EntityUserId).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.Name).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.OTP).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.OTPVerifiedDateTime).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.IsOTPVerified).IsModified = false;
            //    db.Entry(beneficiaryDetailInDb).Property(x => x.IsAccountVerified).IsModified = false;
            //    db.SaveChanges();
            //    recordStatus = 2;
            //}
            return recordStatus;
        }

        [Authorize]
        [HttpPut]
        [Route("api/instantPayDMT/checkNUpdatePendingTransactions")]
        public IHttpActionResult checkNUpdatePendingTransactions(string orderId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                string transactionResponseContent = "";
                string transactionStatus = "";
                //Fetching all pending transactions
                List<tblInstantPay_Transaction> objPendingTransactions = db.tblInstantPay_Transaction.Where(x => x.ipay_id == orderId).ToList();
                if (objPendingTransactions != null)
                {
                    foreach (tblInstantPay_Transaction objTransactionData in objPendingTransactions)
                    {
                        transactionStatusRequest objTransactionStatusRequest = new transactionStatusRequest();
                        objTransactionStatusRequest.token = helper.getInstantPayToken();
                        objTransactionStatusRequest.request = new request();
                        objTransactionStatusRequest.request.order_id = objTransactionData.ipay_id;
                        IRestResponse transactionResponse = checkTransactionStatus(objTransactionStatusRequest);
                        if (transactionResponse.StatusCode == HttpStatusCode.OK)
                        {
                            transactionStatusResponse objTransactionStatusResponse = JsonConvert.DeserializeObject<transactionStatusResponse>(transactionResponse.Content);
                            objTransactionData.UpdateResponseApi = transactionResponse.Content.ToString();
                            if (objTransactionStatusResponse.statuscode == "TXN")
                            {
                                transactionStatus = objTransactionStatusResponse.data.transaction_description;
                                transactionResponseContent = objTransactionStatusResponse.data.transaction_status;
                                var beneficiaryDetailInDb = db.tblInstantPay_Beneficiary.Where(x => x.BeneficiaryId == objTransactionData.BeneficiaryId.Trim()).Take(1).SingleOrDefault();
                                if (beneficiaryDetailInDb != null)
                                {
                                    objTransactionData.Name = beneficiaryDetailInDb.Name;
                                }
                                if (objTransactionStatusResponse.data.transaction_status == "SUCCESS")
                                {
                                    objTransactionData.Opr_id = objTransactionStatusResponse.data.serviceprovider_id;
                                    objTransactionData.Ref_no = objTransactionStatusResponse.data.serviceprovider_id;
                                    objTransactionData.StatusCode = "TXN";
                                    objTransactionData.Transaction_Status = objTransactionStatusResponse.data.transaction_description;
                                    objTransactionData.APIStatus = objTransactionStatusResponse.data.transaction_description;
                                }
                                else if (objTransactionStatusResponse.data.transaction_status == "REFUND")
                                {
                                    objTransactionData.StatusCode = objTransactionStatusResponse.data.transaction_status;
                                    objTransactionData.Transaction_Status = objTransactionStatusResponse.data.transaction_description;
                                    objTransactionData.APIStatus = objTransactionStatusResponse.data.transaction_description;
                                    helper.process_MoneyTransferRefund(objTransactionData.ipay_id, (long)objTransactionData.EntityId, (long)objTransactionData.EntityUserId, 4, Convert.ToDecimal(objTransactionData.Amount));
                                }
                                db.Entry(objTransactionData).State = EntityState.Modified;
                                db.Entry(objTransactionData).Property(x => x.RemitterMobileNo).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.Mode).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.BeneficiaryId).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.ipay_id).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.RequestApi).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.ResponseApi).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.Amount).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.Opening_bal).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.Locked_amt).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.CreatedDateTime).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.EntityId).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.EntityUserId).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.FromMobileDevice).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.ClientRefId).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.ParentClientRefId).IsModified = false;
                                db.Entry(objTransactionData).Property(x => x.IPAddress).IsModified = false;
                                db.SaveChanges();
                            }
                        }
                    }
                    return Ok(transactionStatus);
                }
                else
                {
                    return Ok("No Pending Transaction Found.");
                }
            }
            else
                return Unauthorized();
        }

        #region Payout Directs

        [Authorize]
        [ResponseType(typeof(transfer_request))]
        [Route("api/instantPayDMT/PayoutDirect")]
        [HttpPost]
        public IHttpActionResult PayoutDirect(payoutDirect_Request objPayoutRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                string senderMobileNo = "8141844222";
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (helper.checkProductPartialStopStatus(7))
                        return BadRequest("We are upgrading our Money Transfer system. We will back LIVE soon!!!");
                    else
                    {
                        //Fetching Beneficiary Details
                        tblInstantPay_Beneficiary objBeneficiaryDetail = db.tblInstantPay_Beneficiary.Where(x => x.BeneficiaryId == objPayoutRequest.request.beneficiaryId).Take(1).SingleOrDefault();
                        if (objBeneficiaryDetail == null)
                            return BadRequest("Invalid Beneficiary Details");
                        try
                        {
                            senderMobileNo = db.tblInstantPay_Remitter.Where(x => x.RemitterId == objBeneficiaryDetail.RemitterId).Take(1).SingleOrDefault().MobileNo;
                        }
                        catch (Exception)
                        {
                        }
                        payoutDirect_Request objFundTransferRequest = new payoutDirect_Request();
                        objFundTransferRequest.request = new Request();
                        objFundTransferRequest.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        objFundTransferRequest.request.sp_key = objPayoutRequest.request.sp_key;
                        objFundTransferRequest.request.external_ref = objPayoutRequest.request.external_ref;
                        objFundTransferRequest.request.credit_account = objBeneficiaryDetail.AccountNo;
                        objFundTransferRequest.request.credit_amount = objPayoutRequest.request.credit_amount;
                        objFundTransferRequest.request.ifs_code = objBeneficiaryDetail.ifsc;
                        objFundTransferRequest.request.bene_name = helper.RemoveSpecialChars(objBeneficiaryDetail.Name);
                        try
                        {
                            int latitudeRemainingLength = objPayoutRequest.request.latitude.Split('.')[1].Length;
                            int longitudeRemainingLength = objPayoutRequest.request.longitude.Split('.')[1].Length;
                            objFundTransferRequest.request.latitude = objPayoutRequest.request.latitude.Split('.')[0] + "." + (latitudeRemainingLength == 4 ? objPayoutRequest.request.latitude.Split('.')[1] : latitudeRemainingLength == 3 ? objPayoutRequest.request.latitude.Split('.')[1] + "0" : latitudeRemainingLength == 2 ? objPayoutRequest.request.latitude.Split('.')[1] + "00" : objPayoutRequest.request.latitude.Split('.')[1] + "000");
                            objFundTransferRequest.request.longitude = objPayoutRequest.request.longitude.Split('.')[0] + "." + (longitudeRemainingLength == 4 ? objPayoutRequest.request.longitude.Split('.')[1] : longitudeRemainingLength == 3 ? objPayoutRequest.request.longitude.Split('.')[1] + "0" : longitudeRemainingLength == 2 ? objPayoutRequest.request.longitude.Split('.')[1] + "00" : objPayoutRequest.request.longitude.Split('.')[1] + "000");
                        }
                        catch (Exception)
                        {
                            objFundTransferRequest.request.latitude = objPayoutRequest.request.latitude;
                            objFundTransferRequest.request.longitude = objPayoutRequest.request.longitude;
                        }
                        objFundTransferRequest.request.endpoint_ip = objPayoutRequest.request.endpoint_ip;
                        objFundTransferRequest.request.alert_email = "";
                        objFundTransferRequest.request.alert_mobile = senderMobileNo;//String.IsNullOrEmpty(objBeneficiaryDetail.MobileNo) ? "7990250615" : objBeneficiaryDetail.MobileNo;
                        objFundTransferRequest.request.remarks = "Payment Transfer to " + helper.RemoveSpecialChars(objBeneficiaryDetail.Name);
                        if (objFundTransferRequest.request.remarks.Length >= 25)
                        {
                            objFundTransferRequest.request.remarks = objFundTransferRequest.request.remarks.Substring(0, 15);
                        }

                        int companyId = Convert.ToInt32(identityCompanyId.Value);
                        decimal transferAmount = Convert.ToDecimal(objPayoutRequest.request.credit_amount);
                        transferAmount = transferAmount + (transferAmount * 1 / 100);
                        if (helper.checkWalletBalance(entityId, transferAmount))
                        {
                            IRestResponse instantPayWalletResponse = balanceCheck(ConfigurationManager.AppSettings["InstantPayToken"].ToString());
                            if (instantPayWalletResponse.StatusCode == HttpStatusCode.OK)
                            {
                                checkWallet objCheckWallet = new checkWallet();
                                objCheckWallet = JsonConvert.DeserializeObject<checkWallet>(instantPayWalletResponse.Content);
                                if (Convert.ToDouble(objCheckWallet.data.available) > Convert.ToDouble(objFundTransferRequest.request.credit_amount))
                                {
                                    double totalAmount2Transfer = Convert.ToDouble(objFundTransferRequest.request.credit_amount);
                                    string moneyTransferStatus = "";
                                    if (helper.checkWalletBalance(entityId, (decimal)totalAmount2Transfer))
                                    {
                                        objFundTransferRequest.request.endpoint_ip = objFundTransferRequest.request.endpoint_ip == "::1" ? "103.251.19.141" : objFundTransferRequest.request.endpoint_ip;
                                        string moneyTransferCommitStatus = instantDirectPayoutTransferCommit(objFundTransferRequest, entityId, entityUserId, objPayoutRequest.request.fromMobileDevice, companyId, objPayoutRequest.request.TransType, objPayoutRequest.request.remitterMobileNo, objPayoutRequest.request.beneficiaryId);
                                        moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                    }
                                    else
                                    {
                                        return BadRequest("Oops! Wallet Balance is too Low");
                                    }
                                    return Ok(moneyTransferStatus);
                                }
                                else
                                {
                                    return BadRequest("Something went wrong, balance is low. Status Code : AgencyBalance");
                                }
                            }
                            else
                            {
                                return BadRequest("Something went wrong while checking wallet balance. Status Code : checkWallet");
                            }
                        }
                        else
                        {
                            return BadRequest("Oops! Wallet Balance is too Low");
                        }
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        protected string instantDirectPayoutTransferCommit(payoutDirect_Request objFundTransferRequest, long entityId, long entityUserId, Boolean fromMobileDevice, Int32 companyId, string transType, string remitterMobileNo, string beneficiaryId)
        {
            //payoutDirect_Response beneficiaryFundTransferResponse = helper.DMT2PayoutTestResponse("dmt2response.json");//; payoutDirect(objFundTransferRequest);
            IRestResponse FundTransferResponse = payoutDirect(objFundTransferRequest);
            if (FundTransferResponse.StatusCode == HttpStatusCode.OK)
            {
                if (String.IsNullOrEmpty(remitterMobileNo))
                {
                    string remitterId = db.tblInstantPay_Beneficiary.Where(x => x.BeneficiaryId == beneficiaryId).Take(1).SingleOrDefault().RemitterId;
                    if (!String.IsNullOrEmpty(remitterId))
                        remitterMobileNo = db.tblInstantPay_Remitter.Where(x => x.RemitterId == remitterId).OrderByDescending(x => x.CreatedDateTime).Take(1).SingleOrDefault().MobileNo;
                }
                payoutDirect_Response beneficiaryFundTransferResponse = JsonConvert.DeserializeObject<payoutDirect_Response>(FundTransferResponse.Content);
                tblInstantPay_Transaction objtblInstantPay_Transaction = new tblInstantPay_Transaction();
                objtblInstantPay_Transaction.InstantPayTransactionId = 0;
                objtblInstantPay_Transaction.RemitterMobileNo = remitterMobileNo;
                objtblInstantPay_Transaction.Name = objFundTransferRequest.request.bene_name;
                objtblInstantPay_Transaction.Mode = transType;
                objtblInstantPay_Transaction.BeneficiaryId = beneficiaryId;
                objtblInstantPay_Transaction.ipay_id = beneficiaryFundTransferResponse.data == null ? string.Empty : beneficiaryFundTransferResponse.data.ipay_id;
                objtblInstantPay_Transaction.RequestApi = JsonConvert.SerializeObject(objFundTransferRequest);
                objtblInstantPay_Transaction.ResponseApi = JsonConvert.SerializeObject(beneficiaryFundTransferResponse);
                objtblInstantPay_Transaction.StatusCode = beneficiaryFundTransferResponse.statuscode;
                objtblInstantPay_Transaction.APIStatus = beneficiaryFundTransferResponse.status;
                objtblInstantPay_Transaction.Amount = objFundTransferRequest.request.credit_amount;
                objtblInstantPay_Transaction.Ref_no = beneficiaryFundTransferResponse.data == null ? beneficiaryFundTransferResponse.orderid : beneficiaryFundTransferResponse.data.payout.credit_refid;
                objtblInstantPay_Transaction.Opr_id = beneficiaryFundTransferResponse.data == null ? beneficiaryFundTransferResponse.orderid : beneficiaryFundTransferResponse.data.payout.credit_refid;
                objtblInstantPay_Transaction.Opening_bal = "";
                objtblInstantPay_Transaction.Locked_amt = "";
                objtblInstantPay_Transaction.CreatedDateTime = helper.getIndianTime();
                objtblInstantPay_Transaction.Transaction_Status = beneficiaryFundTransferResponse.status;
                objtblInstantPay_Transaction.EntityId = entityId;
                objtblInstantPay_Transaction.FromMobileDevice = fromMobileDevice;
                objtblInstantPay_Transaction.EntityUserId = entityUserId;
                objtblInstantPay_Transaction.ClientRefId = objFundTransferRequest.request.external_ref;
                objtblInstantPay_Transaction.ParentClientRefId = objFundTransferRequest.request.external_ref;
                objtblInstantPay_Transaction.IPAddress = objFundTransferRequest.request.endpoint_ip;
                objtblInstantPay_Transaction.Latitude = objFundTransferRequest.request.latitude;
                objtblInstantPay_Transaction.Longitude = objFundTransferRequest.request.longitude;
                objtblInstantPay_Transaction.Alert_Mobile = objFundTransferRequest.request.alert_mobile;
                objtblInstantPay_Transaction.Alert_Email = objFundTransferRequest.request.alert_email;
                objtblInstantPay_Transaction.PayoutDirectRemark = objFundTransferRequest.request.remarks;
                objtblInstantPay_Transaction.IsPayoutDirect = true;
                db.tblInstantPay_Transaction.Add(objtblInstantPay_Transaction);
                db.SaveChanges();
                if (beneficiaryFundTransferResponse.statuscode.ToLower().Trim() == "txn")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(beneficiaryFundTransferResponse.data.transfer_value), entityId, entityUserId, beneficiaryFundTransferResponse.data.ipay_id, companyId, 7, false);
                }
                else if (beneficiaryFundTransferResponse.statuscode.ToLower().Trim() == "tup")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(beneficiaryFundTransferResponse.data.transfer_value), entityId, entityUserId, beneficiaryFundTransferResponse.data.ipay_id, companyId, 7, false);
                }
                return beneficiaryFundTransferResponse.status;
            }
            else
                return FundTransferResponse.StatusDescription.ToString();
        }

        #endregion

        protected Int32 getSenderLimit4PayoutDirect(string remitterMobileNo)
        {
            //Getting Sender Transfer Amount via Payout
            List<tblInstantPay_Transaction> transactionInDbList = db.tblInstantPay_Transaction.Where(x => x.RemitterMobileNo == remitterMobileNo &&
            x.StatusCode == "TXN" &&
            x.CreatedDateTime.Value.Month == DateTime.Today.Month &&
            x.CreatedDateTime.Value.Year == DateTime.Today.Year).ToList();
            if (transactionInDbList.Count == 0)
                return 200000;
            else
            {
                int totalAmountTransfered = transactionInDbList.Sum(x => Convert.ToInt32(x.Amount));
                return 200000 - totalAmountTransfered;
            }
        }
    }
}
