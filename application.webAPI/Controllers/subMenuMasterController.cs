﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class subMenuMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/subMenuMaster
        [Authorize]
        [ResponseType(typeof(subMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult GettblSubMenu_Master()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subMenuMasterViewModel objSubMenuMasterViewModel = new subMenuMasterViewModel()
                    {
                        objSubMenu = new tblSubMenu_Master(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList()
                    };
                    return Ok(objSubMenuMasterViewModel);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // GET: api/subMenuMaster/5
        [Authorize]
        [ResponseType(typeof(subMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult GettblSubMenu_Master(int id, int mainMenuId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    QrySubMenu objQrySubMenuFromDb = new QrySubMenu();
                    tblSubMenu_Master objSubMenuFromDb = new tblSubMenu_Master();
                    if (id > 0)
                    {
                        objSubMenuFromDb = db.tblSubMenu_Master.Find(id);
                        if (objSubMenuFromDb == null)
                        {
                            return NotFound();
                        }
                    }
                    QryMainMenu objMainMenuMaster = db.QryMainMenu.SingleOrDefault(m => m.MainMenuId == mainMenuId);
                    if (objMainMenuMaster == null)
                        return NotFound();
                    subMenuMasterViewModel objSubMenuMasterViewModel = new subMenuMasterViewModel()
                    {
                        objSubMenu = objSubMenuFromDb,
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objMainMenuList = db.tblMainMenu_Master.OrderBy(r => r.MainMenuName).ToList()
                    };
                    return Ok(objSubMenuMasterViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/subMenuMaster/5
        [Authorize]
        [HttpGet]
        public IHttpActionResult GettblSubMenu_MasterByMenuId(int mainMenuId, int CompanyID)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    List<QrySubMenu> objSubMenuList = new List<QrySubMenu>();
                    objSubMenuList = db.QrySubMenu.Where(m => m.MainMenuId == mainMenuId).OrderBy(m => m.SubMenuName).ToList();
                    return Ok(objSubMenuList);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/subMenuMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateSubMenuMaster(int id, tblSubMenu_Master objSubMenu)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objSubMenu.SubMenuId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var subMenuInDb = db.tblSubMenu_Master.SingleOrDefault(s => s.MainMenuId == objSubMenu.MainMenuId && 
                        s.SubMenuName.Trim().ToLower() == objSubMenu.SubMenuName.Trim().ToLower() &&
                        s.ExternalURL.Trim().ToLower() == objSubMenu.ExternalURL.Trim().ToLower() &&
                        s.SubMenuId != objSubMenu.SubMenuId);
                        if (subMenuInDb != null)
                            return BadRequest("Sub Menu Name already exists.");
                        objSubMenu.ExternalURL = objSubMenu.ExternalURL == null ? "" : objSubMenu.ExternalURL;
                        objSubMenu.ExternalURLTarget = objSubMenu.ExternalURLTarget == null ? "" : objSubMenu.ExternalURLTarget;
                        db.Entry(objSubMenu).State = EntityState.Modified;
                        db.Entry(objSubMenu).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objSubMenu).Property(x => x.DisplayOrder).IsModified = false;
                        db.Entry(objSubMenu).Property(x => x.MainMenuId).IsModified = false;
                        db.Entry(objSubMenu).Property(x => x.CreatedDateTime).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesSubMenuExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/subMenuMaster
        [Authorize]
        [ResponseType(typeof(tblSubMenu_Master))]
        public IHttpActionResult saveSubMenu(tblSubMenu_Master objSubMenu)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    if (identityAdminLoginId != null)
                    {
                        var subMenuInDb = db.tblSubMenu_Master.SingleOrDefault(s => s.MainMenuId == objSubMenu.MainMenuId &&
                        s.SubMenuName.Trim().ToLower() == objSubMenu.SubMenuName.Trim().ToLower() &&
                        s.ExternalURL.Trim().ToLower() == objSubMenu.ExternalURL.Trim().ToLower() &&
                        s.SubMenuId != objSubMenu.SubMenuId);
                        if (subMenuInDb != null)
                            return BadRequest("Sub Menu Name already exists.");

                        var maxDisplayOrderInDb = db.tblSubMenu_Master.Where(m => m.MainMenuId == objSubMenu.MainMenuId).ToList();
                        int maxDisplayOrder = maxDisplayOrderInDb.Count == 0 ? 0 : maxDisplayOrderInDb.Max(m => m.DisplayOrder);
                        objSubMenu.DisplayOrder = maxDisplayOrder + 1;
                        objSubMenu.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                        objSubMenu.CreatedDateTime = helper.getIndianTime();
                        objSubMenu.ExternalURL = objSubMenu.ExternalURL == null ? "" : objSubMenu.ExternalURL;
                        objSubMenu.ExternalURLTarget = objSubMenu.ExternalURLTarget == null ? "" : objSubMenu.ExternalURLTarget;
                        db.tblSubMenu_Master.Add(objSubMenu);
                        db.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/subMenuMaster/5
        [Authorize]
        [ResponseType(typeof(tblSubMenu_Master))]
        public IHttpActionResult DeletetblSubMenu_Master(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblSubMenu_Master objSubMenu = db.tblSubMenu_Master.Find(id);
                    if (objSubMenu == null)
                    {
                        return NotFound();
                    }

                    //Removing role permission from submenurole_permission
                    db.tblSubMenu_Role_Permission.RemoveRange(db.tblSubMenu_Role_Permission.Where(s => s.SubMenuId == id));
                    db.tblSubMenu_Master.Remove(objSubMenu);
                    db.SaveChanges();
                    return Ok(objSubMenu);
                }
                else
                    return Unauthorized();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(subMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getSubMenuMasterByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection, int mainMenuId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    subMenuMasterViewModel objSubMenuMasterViewModel = new subMenuMasterViewModel();
                    objSubMenuMasterViewModel.objSubMenuList = db.QrySubMenu.Where(s => s.MainMenuId == mainMenuId).ToList();
                    objSubMenuMasterViewModel.totalRecords = objSubMenuMasterViewModel.objSubMenuList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objSubMenuMasterViewModel.objSubMenuList = objSubMenuMasterViewModel.objSubMenuList.Where(s => s.SubMenuName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.MainMenuName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objSubMenuMasterViewModel.filterRecords = objSubMenuMasterViewModel.objSubMenuList.Count();
                    }
                    else
                        objSubMenuMasterViewModel.filterRecords = objSubMenuMasterViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objSubMenuMasterViewModel.objSubMenuList = String.IsNullOrEmpty(searchValue) ?
                            db.QrySubMenu.Where(s => s.MainMenuId == mainMenuId).OrderBy(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QrySubMenu>() :
                            objSubMenuMasterViewModel.objSubMenuList.OrderBy(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QrySubMenu>();
                    else
                        objSubMenuMasterViewModel.objSubMenuList = String.IsNullOrEmpty(searchValue) ?
                            db.QrySubMenu.Where(s => s.MainMenuId == mainMenuId).OrderByDescending(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QrySubMenu>() :
                            objSubMenuMasterViewModel.objSubMenuList.OrderByDescending(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QrySubMenu>();
                    return Ok(objSubMenuMasterViewModel);
                }
                else { return Unauthorized(); }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesSubMenuExists(int id)
        {
            return db.tblSubMenu_Master.Count(e => e.SubMenuId == id) > 0;
        }
    }
}