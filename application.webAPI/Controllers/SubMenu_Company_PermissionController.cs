﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;


namespace application.webAPI.Controllers
{
    public class SubMenu_Company_PermissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/SubMenu_Company_Permission/5
        [Authorize]
        [ResponseType(typeof(SubMenuCompanyPermissionVM))]
        [HttpGet]
        public IHttpActionResult GettblSubMenu_Company_Permission(int companyId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {

                SubMenuCompanyPermissionVM objSubMenuCompanyPermissionVN = new SubMenuCompanyPermissionVM();
                objSubMenuCompanyPermissionVN.objCompanyList = (companyId == helper.defaultCompany ? db.tblCompany_Master.OrderBy(c => c.CompanyName).ToList() : db.tblCompany_Master.Where(c => c.CompanyId == companyId).ToList());
                objSubMenuCompanyPermissionVN.objMainMenuList = db.QryMainMenu_Permission_4_Company.Where(c => c.CompanyId == companyId).ToList();
                List<int> mainMenuIds = objSubMenuCompanyPermissionVN.objMainMenuList.Select(c => c.MainMenuId).ToList();
                List<tblMainMenu_Master> objMainMenuList = (from a in db.tblMainMenu_Master where !mainMenuIds.Contains(a.MainMenuId) select a).ToList();
                foreach (tblMainMenu_Master objData in objMainMenuList)
                {
                    QryMainMenu_Permission_4_Company objCompanyMenuPermission = new QryMainMenu_Permission_4_Company();
                    objCompanyMenuPermission.MainMenuId = objData.MainMenuId;
                    objCompanyMenuPermission.MainMenuName = objData.MainMenuName;
                    objCompanyMenuPermission.MenuPermissionId = 0;
                    objCompanyMenuPermission.CompanyId = companyId;
                    objCompanyMenuPermission.CanView = false;
                    objCompanyMenuPermission.CanEdit = false;
                    objCompanyMenuPermission.CanAdd = false;
                    objCompanyMenuPermission.CanDelete = false;
                    objSubMenuCompanyPermissionVN.objMainMenuList.Add(objCompanyMenuPermission);
                }

                objSubMenuCompanyPermissionVN.objMainMenuList = objSubMenuCompanyPermissionVN.objMainMenuList.OrderBy(c => c.MainMenuName).ToList();
                return Ok(objSubMenuCompanyPermissionVN);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [Route("api/SubMenu_Company_Permission/getSubMenuCompanyMainMenuList")]
        [ResponseType(typeof(SubMenuCompanyPermissionVM))]
        [HttpGet]
        public IHttpActionResult getSubMenuCompanyPermission(int companyId, int MainMenuId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                SubMenuCompanyPermissionVM objSubMenuCompanyPermissionVN = new SubMenuCompanyPermissionVM();
                objSubMenuCompanyPermissionVN.objCompanyList = (companyId == helper.defaultCompany ? db.tblCompany_Master.OrderBy(c => c.CompanyName).ToList() : db.tblCompany_Master.Where(c => c.CompanyId == companyId).ToList());
                objSubMenuCompanyPermissionVN.clsMainMenuList = db.tblMainMenu_Master.OrderBy(c => c.MainMenuName).ToList();
                objSubMenuCompanyPermissionVN.objSubMenuList = db.QrySubMenu_Permission_4_Company.Where(c => c.CompanyId == companyId && c.MainMenuId == MainMenuId).ToList();
                List<int> subMenuIds = objSubMenuCompanyPermissionVN.objSubMenuList.Select(c => c.SubMenuId).ToList();
                List<tblSubMenu_Master> objSubMenuList = (from a in db.tblSubMenu_Master where a.MainMenuId == MainMenuId && !subMenuIds.Contains(a.SubMenuId) select a).ToList();
                foreach (tblSubMenu_Master objData in objSubMenuList)
                {
                    QrySubMenu_Permission_4_Company objCompanySubMenuPermission = new QrySubMenu_Permission_4_Company();
                    objCompanySubMenuPermission.MainMenuId = objData.MainMenuId;
                    objCompanySubMenuPermission.SubMenuName = objData.SubMenuName;
                    objCompanySubMenuPermission.SubMenuId = objData.SubMenuId;
                    objCompanySubMenuPermission.MenuPermissionId = 0;
                    objCompanySubMenuPermission.CompanyId = companyId;
                    objCompanySubMenuPermission.CanView = false;
                    objCompanySubMenuPermission.CanEdit = false;
                    objCompanySubMenuPermission.CanAdd = false;
                    objCompanySubMenuPermission.CanDelete = false;
                    objSubMenuCompanyPermissionVN.objSubMenuList.Add(objCompanySubMenuPermission);
                }
                objSubMenuCompanyPermissionVN.objSubMenuList = objSubMenuCompanyPermissionVN.objSubMenuList.OrderBy(c => c.SubMenuName).ToList();
                return Ok(objSubMenuCompanyPermissionVN);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [Route("api/SubMenu_Company_Permission/getMainMenuByCompanyId")]
        // GET: api/RolePermission
        [ResponseType(typeof(List<tblMainMenu_Master>))]
        [HttpGet]
        public IHttpActionResult getMainMenuByCompanyPermission(int companyId = 0)
        {
            List<tblMainMenu_Master> objMainMenuList = new List<tblMainMenu_Master>();
            var menuListInDb = db.QryMainMenu_Permission_4_Company.Where(c => c.CompanyId == companyId).Select(c => new { c.MainMenuName, c.MainMenuId }).Distinct().ToList();
            for (int i = 0; i < menuListInDb.Count; i++)
            {
                tblMainMenu_Master objMainMenu = new tblMainMenu_Master();
                objMainMenu.MainMenuId = menuListInDb[i].MainMenuId;
                objMainMenu.MainMenuName = menuListInDb[i].MainMenuName;
                objMainMenuList.Add(objMainMenu);
            }
            return Ok(objMainMenuList);
        }

        [Authorize]
        [Route("api/SubMenu_Company_Permission/deleteSubMenuByCompanyId")]
        // GET: api/RolePermission
        [ResponseType(typeof(List<tblMainMenu_Master>))]
        [HttpDelete]
        public IHttpActionResult deleteSubMenuByCompanyId(int companyId, int mainMenuId)
        {
            db.tblSubMenu_Company_Permission.RemoveRange(db.tblSubMenu_Company_Permission.Where(c => c.CompanyId == companyId && c.SubMenuId > 0 && c.MainMenuId == mainMenuId));
            db.SaveChanges();
            return Ok();
        }



        // PUT: api/SubMenu_Company_Permission/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PuttblSubMenu_Company_Permission(long id, tblSubMenu_Company_Permission tblSubMenu_Company_Permission)
        {
            if (id != tblSubMenu_Company_Permission.MenuPermissionId)
            {
                return BadRequest();
            }
            db.Entry(tblSubMenu_Company_Permission).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblSubMenu_Company_PermissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SubMenu_Company_Permission
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PosttblSubMenu_Company_Permission(tblSubMenu_Company_Permission objCompanyPermission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                objCompanyPermission.CreatedDateTime = helper.getIndianTime();
                var companyPermissionInDb = db.tblSubMenu_Company_Permission.SingleOrDefault(p => p.CompanyId == objCompanyPermission.CompanyId &&
                p.MainMenuId == objCompanyPermission.MainMenuId &&
                p.SubMenuId == objCompanyPermission.SubMenuId &&
                p.MenuPermissionId != objCompanyPermission.MenuPermissionId);

                if (companyPermissionInDb != null)
                {
                    companyPermissionInDb.CanView = objCompanyPermission.CanView;
                    companyPermissionInDb.CanEdit = objCompanyPermission.CanEdit;
                    companyPermissionInDb.CanAdd = objCompanyPermission.CanAdd;
                    companyPermissionInDb.CanDelete = objCompanyPermission.CanDelete;
                    companyPermissionInDb.CreatedDateTime = objCompanyPermission.CreatedDateTime;
                    companyPermissionInDb.CompanyId = objCompanyPermission.CompanyId;
                    db.Entry(companyPermissionInDb).State = EntityState.Modified;
                    db.Entry(companyPermissionInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.SaveChanges();
                }
                else
                {
                    db.tblSubMenu_Company_Permission.Add(objCompanyPermission);
                    db.SaveChanges();
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/SubMenu_Company_Permission/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult DeletetblSubMenu_Company_Permission(long id)
        {
            db.tblSubMenu_Company_Permission.RemoveRange(db.tblSubMenu_Company_Permission.Where(c => c.CompanyId == id));
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSubMenu_Company_PermissionExists(long id)
        {
            return db.tblSubMenu_Company_Permission.Count(e => e.MenuPermissionId == id) > 0;
        }
    }
}