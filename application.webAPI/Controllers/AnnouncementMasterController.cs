﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class AnnouncementMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();
        // GET: api/AnnouncementMaster
        [Authorize]
        [ResponseType(typeof(AnnouncementViewModel))]
        [HttpGet]
        public IHttpActionResult getAnnouncementMasterList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                AnnouncementViewModel objAnnouncementViewModel = new AnnouncementViewModel()
                {
                    objAnnouncementMaster = new tblAnnouncement_Master(),
                    objStatusMasterList = db.tblStatus_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.StatusName).ToList()
                };
                return Ok(objAnnouncementViewModel);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(AnnouncementViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityMasterByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                AnnouncementViewModel objAnnouncementViewModel = new AnnouncementViewModel();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    objAnnouncementViewModel.totalRecords = db.QryAnnouncement.Where(c => c.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objAnnouncementViewModel.objAnnouncementMasterList = db.QryAnnouncement.Where(a => a.CompanyId == CompanyId && (a.AnnouncementTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.AnnouncementText.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objAnnouncementViewModel.filterRecords = objAnnouncementViewModel.objAnnouncementMasterList.Count();
                    }
                    else
                        objAnnouncementViewModel.filterRecords = objAnnouncementViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objAnnouncementViewModel.objAnnouncementMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryAnnouncement.Where(c => c.CompanyId == CompanyId).OrderBy(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>() :
                            objAnnouncementViewModel.objAnnouncementMasterList.OrderBy(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>();
                    else
                        objAnnouncementViewModel.objAnnouncementMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryAnnouncement.Where(c => c.CompanyId == CompanyId).OrderByDescending(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>() :
                            objAnnouncementViewModel.objAnnouncementMasterList.OrderByDescending(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>();
                }
                else
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAnnouncementViewModel.totalRecords = db.QryAnnouncement.Where(c => c.EntityId == entityId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objAnnouncementViewModel.objAnnouncementMasterList = db.QryAnnouncement.Where(a => a.EntityId == entityId && (a.AnnouncementTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.AnnouncementText.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || a.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objAnnouncementViewModel.filterRecords = objAnnouncementViewModel.objAnnouncementMasterList.Count();
                    }
                    else
                        objAnnouncementViewModel.filterRecords = objAnnouncementViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objAnnouncementViewModel.objAnnouncementMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryAnnouncement.Where(c => c.EntityId == entityId).OrderBy(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>() :
                            objAnnouncementViewModel.objAnnouncementMasterList.OrderBy(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>();
                    else
                        objAnnouncementViewModel.objAnnouncementMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryAnnouncement.Where(c => c.EntityId == entityId).OrderByDescending(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>() :
                            objAnnouncementViewModel.objAnnouncementMasterList.OrderByDescending(s => s.AnnouncementTitle).Skip(pageIndex).Take(pageSize).ToList<QryAnnouncement>();
                }
                return Ok(objAnnouncementViewModel);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/AnnouncementMaster/5
        [Authorize]
        [ResponseType(typeof(tblAnnouncement_Master))]
        public IHttpActionResult getAnnouncementMaster(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblAnnouncement_Master tblAnnouncement_Master = db.tblAnnouncement_Master.Find(id);
                if (tblAnnouncement_Master == null)
                {
                    return NotFound();
                }
                return Ok(tblAnnouncement_Master);
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/AnnouncementMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult PuttblAnnouncement_Master(int id, tblAnnouncement_Master objAnnouncementMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objAnnouncementMaster.AnnouncementId)
                {
                    return BadRequest();
                }
                try
                {
                    var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                    if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                    {
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                        var annoucementInBb = db.tblAnnouncement_Master.SingleOrDefault(a => a.AnnouncementTitle.Trim().ToLower() == objAnnouncementMaster.AnnouncementTitle.Trim().ToLower() && a.CompanyId == CompanyId && a.AnnouncementId != objAnnouncementMaster.AnnouncementId);
                        if (annoucementInBb != null)
                            return BadRequest("Announcement already exits!");
                    }
                    else
                    {
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        var annoucementInBb = db.tblAnnouncement_Master.SingleOrDefault(a => a.AnnouncementTitle.Trim().ToLower() == objAnnouncementMaster.AnnouncementTitle.Trim().ToLower() && a.EntityId == entityId && a.AnnouncementId != objAnnouncementMaster.AnnouncementId);
                        if (annoucementInBb != null)
                            return BadRequest("Announcement already exits!");
                    }

                    db.Entry(objAnnouncementMaster).State = EntityState.Modified;
                    db.Entry(objAnnouncementMaster).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(objAnnouncementMaster).Property(x => x.CompanyId).IsModified = false;
                    db.Entry(objAnnouncementMaster).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objAnnouncementMaster).Property(x => x.ForCompany2MD).IsModified = false;
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblAnnouncement_MasterExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/AnnouncementMaster
        [Authorize]
        [ResponseType(typeof(tblAnnouncement_Master))]
        public IHttpActionResult saveAnnouncementMaster(tblAnnouncement_Master objAnnouncementMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt32(identityAdminLoginId.Value);
                    var annoucementInBb = db.tblAnnouncement_Master.SingleOrDefault(a => a.AnnouncementTitle.Trim().ToLower() == objAnnouncementMaster.AnnouncementTitle.Trim().ToLower() && a.CompanyId == CompanyId && a.AnnouncementId != objAnnouncementMaster.AnnouncementId);
                    if (annoucementInBb != null)
                        return BadRequest("Announcement already exits!");

                    objAnnouncementMaster.CompanyId = CompanyId;
                    objAnnouncementMaster.ForCompany2MD = true;
                    objAnnouncementMaster.EntityId = 0;
                    objAnnouncementMaster.CreatedBy = adminLoginId;
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var annoucementInBb = db.tblAnnouncement_Master.SingleOrDefault(a => a.AnnouncementTitle.Trim().ToLower() == objAnnouncementMaster.AnnouncementTitle.Trim().ToLower() && a.EntityId == entityId && a.AnnouncementId != objAnnouncementMaster.AnnouncementId);
                    if (annoucementInBb != null)
                        return BadRequest("Announcement already exits!");

                    objAnnouncementMaster.CompanyId = 0;
                    objAnnouncementMaster.ForCompany2MD = false;
                    objAnnouncementMaster.EntityId = entityId;
                    objAnnouncementMaster.CreatedBy = Convert.ToInt32(identityEntityUserId.Value);
                }

                db.tblAnnouncement_Master.Add(objAnnouncementMaster);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/AnnouncementMaster/5
        [Authorize]
        [ResponseType(typeof(tblAnnouncement_Master))]
        public IHttpActionResult deleteAnnouncementMaster(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblAnnouncement_Master tblAnnouncement_Master = db.tblAnnouncement_Master.Find(id);
                if (tblAnnouncement_Master == null)
                {
                    return NotFound();
                }

                db.tblAnnouncement_Master.Remove(tblAnnouncement_Master);
                db.SaveChanges();

                return Ok(tblAnnouncement_Master);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblAnnouncement_MasterExists(int id)
        {
            return db.tblAnnouncement_Master.Count(e => e.AnnouncementId == id) > 0;
        }
    }
}