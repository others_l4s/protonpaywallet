﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class supportTicketController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/supportTicket
        [Authorize]
        [ResponseType(typeof(supportTicketVM))]
        [HttpGet]
        public IHttpActionResult getSupportTicket()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                Int32 entityTypeId = Convert.ToInt32(identityEntityType.Value);
                supportTicketVM objSupportTicketVM = new supportTicketVM()
                {
                    objSupportTicket = new tblSupportSystem_Tickets(),
                    objSupportProblemList = db.tblSupportSystem_Problem_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.ProblemName).ToList(),
                    objSupportTicketList = new List<QrySupportSystemTickets>()
                };
                return Ok(objSupportTicketVM);
            }
            else
                return Unauthorized();
        }

        // GET: api/supportTicket/5
        [Authorize]
        [ResponseType(typeof(supportTicketVM))]
        [HttpGet]
        public IHttpActionResult getSupportTicketById(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                supportTicketVM objSupportTicketVM = new supportTicketVM();
                objSupportTicketVM.objSupportTicketDetail = db.tblSupportSystem_Details.Where(x => x.TicketId == id).ToList();
                objSupportTicketVM.objSupportTicket = db.tblSupportSystem_Tickets.Where(x => x.TicketId == id).SingleOrDefault();
                objSupportTicketVM.objSupportTicketList = db.QrySupportSystemTickets.Where(x => x.TicketId == id).ToList();
                objSupportTicketVM.objSupportProblemList = db.tblSupportSystem_Problem_Master.Where(x => x.StatusId == helper.defaultActiveStatusId).ToList();
                return Ok(objSupportTicketVM);
            }
            else
                return Unauthorized();
        }

        // PUT: api/supportTicket/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateSupportTicket(Int32 id, tblSupportSystem_Tickets objSupportTicket)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                try
                {
                    tblSupportSystem_Details objSupportTicketDetail = new tblSupportSystem_Details();
                    var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                    if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                    {
                        var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                        long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                        objSupportTicketDetail.IsResponse = true;
                        objSupportTicketDetail.AdminUserId = adminLoginId;
                    }
                    else
                    {
                        objSupportTicketDetail.IsResponse = false;
                        objSupportTicketDetail.AdminUserId = 0;
                    }


                    objSupportTicketDetail.CreatedDateTime = helper.getIndianTime();
                    objSupportTicketDetail.TicketId = id;
                    objSupportTicketDetail.Comments = objSupportTicket.Comments;
                    db.tblSupportSystem_Details.Add(objSupportTicketDetail);

                    objSupportTicket.ReOpenedDateTime = (objSupportTicket.IsReopened == true ? helper.getIndianTime() : (DateTime?)null);
                    objSupportTicket.ClosedDateTime = (objSupportTicket.IsClosed == true ? helper.getIndianTime() : (DateTime?)null);
                    db.Entry(objSupportTicket).State = EntityState.Modified;
                    db.Entry(objSupportTicket).Property(x => x.EntityUserId).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.DepartmentId).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.ProblemId).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.Comments).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.AttachmentName).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.IsRaised).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.RaisedDateTime).IsModified = false;
                    db.Entry(objSupportTicket).Property(x => x.FromMobileDevice).IsModified = false;

                    db.SaveChanges();
                    return Ok();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblSupportSystem_TicketsExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/supportTicket
        [Authorize]
        [ResponseType(typeof(tblSupportSystem_Tickets))]
        [HttpPost]
        public IHttpActionResult saveSupportTicket(tblSupportSystem_Tickets objSupportTicket)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    objSupportTicket.EntityUserId = entityUserId;
                    objSupportTicket.IsRaised = true;
                    objSupportTicket.RaisedDateTime = helper.getIndianTime();
                    if (objSupportTicket.FromMobileDevice)
                    {
                        if (!String.IsNullOrEmpty(objSupportTicket.AttachmentName))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objSupportTicket.AttachmentName);
                            string ticketAttachment = "ticketAttachment_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objSupportTicket.AttachmentName = ticketAttachment;
                            string filePath = HttpContext.Current.Server.MapPath("~/supportTicketAttachments/" + ticketAttachment);
                            File.WriteAllBytes(filePath, bytes);
                        }
                    }
                    db.tblSupportSystem_Tickets.Add(objSupportTicket);
                    db.SaveChanges();
                    tblSupportSystem_Details objSupportTicketDetail = new tblSupportSystem_Details();
                    objSupportTicketDetail.TicketId = objSupportTicket.TicketId;
                    objSupportTicketDetail.Comments = objSupportTicket.Comments;
                    objSupportTicketDetail.IsResponse = false;
                    objSupportTicketDetail.AdminUserId = 0;
                    objSupportTicketDetail.CreatedDateTime = helper.getIndianTime();
                    db.tblSupportSystem_Details.Add(objSupportTicketDetail);
                    db.SaveChanges();
                    return Ok(objSupportTicket);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/supportTicket/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult deleteSupportTickets(Int32 id)
        {
            tblSupportSystem_Tickets objSupportTicket = db.tblSupportSystem_Tickets.Find(id);
            if (objSupportTicket == null)
            {
                return NotFound();
            }

            List<tblSupportSystem_Details> objSupportTicketDetail = new List<tblSupportSystem_Details>();
            objSupportTicketDetail = db.tblSupportSystem_Details.Where(x => x.TicketId == id).ToList();
            db.tblSupportSystem_Details.RemoveRange(objSupportTicketDetail);
            db.tblSupportSystem_Tickets.Remove(objSupportTicket);
            db.SaveChanges();
            return Ok();
        }

        [Authorize]
        [ResponseType(typeof(supportTicketListVM))]
        [HttpGet]
        public IHttpActionResult getSupportTicketList(long searchEntityId, Boolean isClosed, Boolean isRaised, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                supportTicketListVM objSupportTicketListVM = new supportTicketListVM();
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 CompanyId = Convert.ToInt32(identityCompanyId.Value);
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    objSupportTicketListVM.objSupportTicketList = db.QrySupportSystemTickets.Where(x => x.CompanyId == CompanyId &&
                    (x.RaisedDateTime >= dFromDate && x.RaisedDateTime <= dToDate) &&
                    (x.RaisedDateTime >= dFromDate && x.RaisedDateTime <= dToDate) &&
                    (isRaised == false || x.IsRaised == isRaised) &&
                    (isClosed == false || x.IsClosed == isClosed) &&
                    (searchEntityId == 0 || x.EntityId == searchEntityId)).ToList();
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objSupportTicketListVM.objSupportTicketList = db.QrySupportSystemTickets.Where(x => x.EntityId == entityId &&
                    (x.RaisedDateTime >= dFromDate && x.RaisedDateTime <= dToDate) &&
                    (isRaised == false || x.IsRaised == isRaised) &&
                    (isClosed == false || x.IsClosed == isClosed)).ToList();
                }
                objSupportTicketListVM.totalRecords = objSupportTicketListVM.objSupportTicketList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objSupportTicketListVM.objSupportTicketList = objSupportTicketListVM.objSupportTicketList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.ProblemName.ToString().Contains(searchValue) ||
                    x.EmailId.Trim().ToLower().Contains(searchValue)).ToList();

                    objSupportTicketListVM.filterRecords = objSupportTicketListVM.objSupportTicketList.Count();
                }
                else
                    objSupportTicketListVM.filterRecords = objSupportTicketListVM.totalRecords;

                if (sortDirection == "asc")
                    objSupportTicketListVM.objSupportTicketList = objSupportTicketListVM.objSupportTicketList.OrderBy(s => s.RaisedDateTime).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemTickets>();
                else
                    objSupportTicketListVM.objSupportTicketList = objSupportTicketListVM.objSupportTicketList.OrderByDescending(s => s.RaisedDateTime).Skip(pageIndex).Take(pageSize).ToList<QrySupportSystemTickets>();
                return Ok(objSupportTicketListVM);
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSupportSystem_TicketsExists(int id)
        {
            return db.tblSupportSystem_Tickets.Count(e => e.TicketId == id) > 0;
        }
    }
}