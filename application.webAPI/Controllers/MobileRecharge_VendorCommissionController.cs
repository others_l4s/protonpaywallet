﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding.Binders;
using application.webAPI.Models;
using application.webAPI.viewModel;
using WebGrease.Css.Ast.Selectors;

namespace application.webAPI.Controllers
{
    public class MobileRecharge_VendorCommissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/MobileRecharge_VendorCommission
        [Authorize]
        [HttpGet]
        public IHttpActionResult getVendorCommissionOperatorTypeNVendorDetail()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
                objMobileRechargeVendorCommissionVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1).ToList();
                objMobileRechargeVendorCommissionVM.objCommissionTypeList = db.tblCommissionType.ToList();
                objMobileRechargeVendorCommissionVM.objMobileRechargeDistinctVendorList = db.tblMobileRecharge_Vendor.Where(x => x.Status == true).Select(x => x.VendorName).Distinct().ToList();
                objMobileRechargeVendorCommissionVM.objMobileRechargeOperatorTypeList = db.tblMobileRecharge_OperatorType.OrderBy(x => x.OperatorTypeName).ToList();
                return Ok(objMobileRechargeVendorCommissionVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(mobileRechargeVendorCommissionVM))]
        [HttpGet]
        public IHttpActionResult getVendorCommissionListbyVendorEntityType(string vendorName, Int16 entityTypeId, Int16 operatorTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
                    List<QryMobileRecharge_Vendor> objMobileRechargeVendorList = db.QryMobileRecharge_Vendor.Where(x => x.Status == true &&
                    (String.IsNullOrEmpty(vendorName) || x.VendorName == vendorName) && (operatorTypeId == 0 || x.OperatorTypeId == operatorTypeId)).ToList();

                    objMobileRechargeVendorCommissionVM.objMobileRechargeVendorCommissionList = new List<QryMobileRecharge_VendorCommission>();
                    objMobileRechargeVendorCommissionVM.objCommissionTypeList = db.tblCommissionType.ToList();
                    objMobileRechargeVendorCommissionVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1).ToList();
                    foreach (QryMobileRecharge_Vendor objVendorData in objMobileRechargeVendorList)
                    {
                        var mobileRechargeVendorCommissionInDb = db.tblMobileRecharge_VendorCommission.Where(x => x.OperatorVendorId == objVendorData.OperatorVendorId &&
                        x.OperatorId == objVendorData.OperatorId && x.IsCustom == false &&
                        (entityTypeId == 0 || x.EntityTypeId == entityTypeId)).SingleOrDefault();
                        objMobileRechargeVendorCommissionVM.objMobileRechargeVendorCommissionList.Add(new QryMobileRecharge_VendorCommission
                        {
                            OperatorVendorCommissionId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.OperatorVendorCommissionId,
                            OperatorVendorId = mobileRechargeVendorCommissionInDb == null ? objVendorData.OperatorVendorId : mobileRechargeVendorCommissionInDb.OperatorVendorId,
                            VendorName = objVendorData.VendorName,
                            OperatorId = mobileRechargeVendorCommissionInDb == null ? objVendorData.OperatorId : mobileRechargeVendorCommissionInDb.OperatorId,
                            OperatorName = objVendorData.OperatorName,
                            OperatorAlias = objVendorData.OperatorAlias,
                            CommissionTypeId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.CommissionTypeId,
                            CommissionValue = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.CommissionValue,
                            EntityTypeId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.EntityTypeId,
                            EntityTypeName = String.Empty,
                            OperatorTypeName = objVendorData.OperatorTypeName,
                            FetchBill = objVendorData.FetchBill,
                            BBPSEnabled = objVendorData.BBPSEnabled
                        });
                    }
                    return Ok(objMobileRechargeVendorCommissionVM);
                }
                else { return BadRequest(helper.invalidModuleAccess); }
            }
            else { return Unauthorized(); }
        }

        [Authorize]
        [HttpPost]
        public IHttpActionResult saveVendorCommission(tblMobileRecharge_VendorCommission objVendorCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                Int32 entityTypeId = Convert.ToInt32(identityEntityType.Value);
                var vendorCommissionInDb = db.tblMobileRecharge_VendorCommission.SingleOrDefault(x => x.OperatorVendorId == objVendorCommission.OperatorVendorId &&
                    x.OperatorId == objVendorCommission.OperatorId &&
                    x.IsCustom == objVendorCommission.IsCustom &&
                    x.EntityId == objVendorCommission.EntityId &&
                    x.OperatorVendorCommissionId != objVendorCommission.OperatorVendorCommissionId &&
                    x.EntityTypeId == objVendorCommission.EntityTypeId);
                if (vendorCommissionInDb != null)
                    return BadRequest("Commission already set for selected Vendor and Operator");

                objVendorCommission.CreatedByEntityTypeId = entityTypeId;
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    objVendorCommission.CreatedBy = adminLoginId;
                }
                else
                {
                    var identityEntityUserId = identity.Claims.Where(x => x.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    objVendorCommission.CreatedBy = entityUserId;
                }
                if (objVendorCommission.OperatorVendorCommissionId == 0)
                {
                    objVendorCommission.CreatedDateTime = helper.getIndianTime();
                    db.tblMobileRecharge_VendorCommission.Add(objVendorCommission);
                    db.SaveChanges();
                }
                else
                {
                    db.Entry(objVendorCommission).State = EntityState.Modified;
                    db.Entry(objVendorCommission).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(objVendorCommission).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(objVendorCommission).Property(x => x.IsCustom).IsModified = false;
                    db.SaveChanges();
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Custom Commission

        [Authorize]
        [Route("api/MobileRecharge_VendorCommission/getVendorCommissionListByVendorEntityCustom")]
        [ResponseType(typeof(mobileRechargeVendorCommissionVM))]
        [HttpGet]
        public IHttpActionResult getVendorCommissionListByVendorEntityCustom(string vendorName, Int16 operatorTypeId, long entityId, Int32 entityTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                Int32 createdByEntityTypeId = Convert.ToInt32(identityEntityType.Value);
                long createdBy = 0;
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    createdBy = Convert.ToInt64(identityAdminLoginId.Value);
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    createdBy = Convert.ToInt64(identityEntityId.Value);
                }
                mobileRechargeVendorCommissionVM objMobileRechargeVendorCommissionVM = new mobileRechargeVendorCommissionVM();
                List<QryMobileRecharge_Vendor> objMobileRechargeVendorList = db.QryMobileRecharge_Vendor.Where(x => x.Status == true &&
                (String.IsNullOrEmpty(vendorName) || x.VendorName == vendorName) && (operatorTypeId == 0 || x.OperatorTypeId == operatorTypeId)).ToList();

                objMobileRechargeVendorCommissionVM.objMobileRechargeVendorCommissionList = new List<QryMobileRecharge_VendorCommission>();
                objMobileRechargeVendorCommissionVM.objCommissionTypeList = db.tblCommissionType.ToList();
                objMobileRechargeVendorCommissionVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1).ToList();
                foreach (QryMobileRecharge_Vendor objVendorData in objMobileRechargeVendorList)
                {
                    var mobileRechargeVendorCommissionInDb = db.tblMobileRecharge_VendorCommission.Where(x => x.OperatorVendorId == objVendorData.OperatorVendorId &&
                    x.OperatorId == objVendorData.OperatorId && x.IsCustom == true && x.CreatedByEntityTypeId == createdByEntityTypeId && x.CreatedBy == createdBy &&
                    (entityId == 0 || x.EntityId == entityId)).SingleOrDefault();
                    objMobileRechargeVendorCommissionVM.objMobileRechargeVendorCommissionList.Add(new QryMobileRecharge_VendorCommission
                    {
                        OperatorVendorCommissionId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.OperatorVendorCommissionId,
                        OperatorVendorId = mobileRechargeVendorCommissionInDb == null ? objVendorData.OperatorVendorId : mobileRechargeVendorCommissionInDb.OperatorVendorId,
                        VendorName = objVendorData.VendorName,
                        OperatorId = mobileRechargeVendorCommissionInDb == null ? objVendorData.OperatorId : mobileRechargeVendorCommissionInDb.OperatorId,
                        OperatorName = objVendorData.OperatorName,
                        OperatorAlias = objVendorData.OperatorAlias,
                        CommissionTypeId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.CommissionTypeId,
                        CommissionValue = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.CommissionValue,
                        EntityTypeId = mobileRechargeVendorCommissionInDb == null ? 0 : mobileRechargeVendorCommissionInDb.EntityTypeId,
                        EntityTypeName = String.Empty,
                        OperatorTypeName = objVendorData.OperatorTypeName,
                        FetchBill = objVendorData.FetchBill,
                        BBPSEnabled = objVendorData.BBPSEnabled
                    });
                }
                return Ok(objMobileRechargeVendorCommissionVM);
            }
            else { return Unauthorized(); }
        }


        #endregion

        private bool tblMobileRecharge_VendorCommissionExists(long id)
        {
            return db.tblMobileRecharge_VendorCommission.Count(e => e.OperatorVendorCommissionId == id) > 0;
        }
    }
}