﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class entityTypeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entityType
        [Authorize]
        [ResponseType(typeof(entityTypeViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityType()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    if (CompanyId == helper.defaultCompany)
                    {
                        entityTypeViewModel objentityTypeViewModel = new entityTypeViewModel()
                        {
                            objEntityType = new tblEntity_Type(),
                            objEntityTypeList = db.QryEntityType.ToList(),
                            objStatusList = db.tblStatus_Master.ToList()
                        };
                        return Ok(objentityTypeViewModel);
                    }
                    else
                        return Unauthorized();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entityTypeViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityTypePagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    if (CompanyId == helper.defaultCompany)
                    {
                        entityTypeViewModel objentityTypeViewModel = new entityTypeViewModel();
                        objentityTypeViewModel.totalRecords = db.tblEntity_Type.Count();
                        if (!String.IsNullOrEmpty(searchValue))
                        {
                            objentityTypeViewModel.objEntityTypeList = db.QryEntityType.Where(s => s.EntityTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                            objentityTypeViewModel.filterRecords = objentityTypeViewModel.objEntityTypeList.Count();
                        }
                        else
                            objentityTypeViewModel.filterRecords = objentityTypeViewModel.totalRecords;
                        if (sortDirection == "asc")
                            objentityTypeViewModel.objEntityTypeList = String.IsNullOrEmpty(searchValue) ?
                            db.QryEntityType.OrderBy(s => s.EntityTypeName).Skip(pageIndex).Take(pageSize).ToList<QryEntityType>() :
                            objentityTypeViewModel.objEntityTypeList.OrderBy(s => s.EntityTypeName).Skip(pageIndex).Take(pageSize).ToList<QryEntityType>();
                        else
                            objentityTypeViewModel.objEntityTypeList = String.IsNullOrEmpty(searchValue) ?
                                 db.QryEntityType.OrderByDescending(s => s.EntityTypeName).Skip(pageIndex).Take(pageSize).ToList<QryEntityType>() :
                                 objentityTypeViewModel.objEntityTypeList.OrderByDescending(s => s.EntityTypeName).Skip(pageIndex).Take(pageSize).ToList<QryEntityType>();

                        return Ok(objentityTypeViewModel);
                    }
                    else
                        return Unauthorized();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }

        // GET: api/entityType/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Type))]
        [HttpGet]
        public IHttpActionResult getEntityTypeById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblEntity_Type tblEntity_Type = db.tblEntity_Type.Find(id);
                    if (tblEntity_Type == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblEntity_Type);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/entityType/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateEntityType(int id, tblEntity_Type objEntityType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    if (CompanyId == helper.defaultCompany)
                    {
                        if (id != objEntityType.EntityTypeId)
                        {
                            return BadRequest();
                        }
                        try
                        {
                            var entityTypeInDb = db.tblEntity_Type.SingleOrDefault(e => e.EntityTypeName.Trim().ToLower() == objEntityType.EntityTypeName.Trim().ToLower() && e.EntityTypeId != objEntityType.EntityTypeId);
                            if (entityTypeInDb != null)
                                return BadRequest("Entity Type already exits!");

                            db.Entry(objEntityType).State = EntityState.Modified;
                            db.Entry(objEntityType).Property(x => x.CreatedBy).IsModified = false;
                            db.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            if (!doesEntityTypeExists(id))
                            {
                                return NotFound();
                            }
                            else
                            {
                                throw;
                            }
                        }
                        return Ok();
                    }
                    else
                        return Unauthorized();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/entityType
        [Authorize]
        [ResponseType(typeof(tblEntity_Type))]
        [HttpPost]
        public IHttpActionResult saveEntityType(tblEntity_Type objEntityType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(identityAdminLoginId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    if (CompanyId == helper.defaultCompany)
                    {
                        var entityTypeInDb = db.tblEntity_Type.SingleOrDefault(e => e.EntityTypeName.Trim().ToLower() == objEntityType.EntityTypeName.Trim().ToLower() && e.EntityTypeId != objEntityType.EntityTypeId);
                        if (entityTypeInDb != null)
                            return BadRequest("Entity Type already exits!");

                        objEntityType.CreatedBy = adminLoginId;
                        db.tblEntity_Type.Add(objEntityType);
                        db.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/entityType/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Type))]
        [HttpDelete]
        public IHttpActionResult deleteEntityType(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblEntity_Type objEntityType = db.tblEntity_Type.Find(id);
                    if (objEntityType == null)
                    {
                        return NotFound();
                    }

                    db.tblEntity_Type.Remove(objEntityType);
                    db.SaveChanges();

                    return Ok(objEntityType);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesEntityTypeExists(int id)
        {
            return db.tblEntity_Type.Count(e => e.EntityTypeId == id) > 0;
        }
    }
}