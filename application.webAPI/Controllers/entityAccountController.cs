﻿using application.webAPI.Models;
using application.webAPI.viewModel;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;

namespace application.webAPI.Controllers
{
    public class entityAccountController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // POST: api/entityAccount
        [Authorize]
        [ResponseType(typeof(tblEntity_Account))]
        [Route("api/saveAccount")]
        [HttpPost]
        public IHttpActionResult saveAccount(tblEntity_Account objEntityAccount)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                objEntityAccount.CreatedDateTime = helper.getIndianTime();
                objEntityAccount.EntityId = entityId;
                objEntityAccount.AccountTransactionId = helper.GetRandomPasswordUsingGUID(10).ToUpper() + DateTime.Now.ToString("ddMMyyyyMMHHSS");
                db.tblEntity_Account.Add(objEntityAccount);
                db.SaveChanges();
                return Ok(objEntityAccount);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getSalesReport")]
        [HttpGet]
        public IHttpActionResult getSalesReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                    objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                        x.UserName.Trim().ToLower().Contains(searchValue) ||
                        x.Amount.ToString().Contains(searchValue) ||
                        x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                        objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                    }
                    else
                        objAccountVM.filterRecords = objAccountVM.totalRecords;

                    if (sortDirection == "asc")
                        objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                    else
                        objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                }
                else
                {
                    return BadRequest("Invalid Module access");
                }
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getTransactionReport")]
        [HttpGet]
        public IHttpActionResult getTransactionReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeBankCharges = Convert.ToInt32(helper.transactionTypes.BankCharges);
                int transactionTypeOtherCharges = Convert.ToInt32(helper.transactionTypes.OtherCharges);
                int transactionTypeGST = Convert.ToInt32(helper.transactionTypes.GST);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId &&
                       c.TransactionTypeId != transactionTypeBankCharges &&
                       c.TransactionTypeId != transactionTypeOtherCharges &&
                       c.TransactionTypeId != transactionTypeGST &&
                       (c.CreatedDateTime >= dFromDate &&
                       c.CreatedDateTime <= dToDate)).ToList();
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    //c.TransactionTypeId != transactionTypeBankCharges &&
                    //c.TransactionTypeId != transactionTypeOtherCharges &&
                    //c.TransactionTypeId != transactionTypeGST &&
                    (c.CreatedDateTime >= dFromDate &&
                    c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();

                objAccountVM.totalAmount = (double)objAccountVM.objEntityAccountList.Sum(x => x.Amount);
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getTransactionReport4Parent")]
        [HttpGet]
        public IHttpActionResult getTransactionReport4Parent(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeBankCharges = Convert.ToInt32(helper.transactionTypes.BankCharges);
                int transactionTypeOtherCharges = Convert.ToInt32(helper.transactionTypes.OtherCharges);
                int transactionTypeGST = Convert.ToInt32(helper.transactionTypes.GST);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.masterDistributor))
                    {
                        var param = new SqlParameter[] {
                            new SqlParameter ("@EntityId", entityId),
                            new SqlParameter ("@FromDate", dFromDate),
                            new SqlParameter ("@ToDate", dToDate),
                            new SqlParameter ("@SearchEntityId", searchEntityId)
                        };
                        objAccountVM.objEntityAccountList = db.Database.SqlQuery<QryEntityAccounts>("EXEC [dbo].[EntityAccount_TransactionReport_4_MD] @EntityId={0}, @FromDate={1}, @ToDate={2}, @SearchEntityId={3}",
                            entityId, dFromDate, dToDate, searchEntityId).ToList();
                    }
                    else if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.distributor))
                    {
                        objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.ParentEntityId == entityId &&
                            c.TransactionTypeId != transactionTypeBankCharges &&
                            c.TransactionTypeId != transactionTypeOtherCharges &&
                            c.TransactionTypeId != transactionTypeGST &&
                            (c.CreatedDateTime >= dFromDate &&
                            c.CreatedDateTime <= dToDate)).ToList();
                    }
                    else
                    {
                        objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId &&
                        c.TransactionTypeId != transactionTypeBankCharges &&
                        c.TransactionTypeId != transactionTypeOtherCharges &&
                        c.TransactionTypeId != transactionTypeGST &&
                        (c.CreatedDateTime >= dFromDate &&
                        c.CreatedDateTime <= dToDate)).ToList();
                    }
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    //c.TransactionTypeId != transactionTypeBankCharges &&
                    //c.TransactionTypeId != transactionTypeOtherCharges &&
                    //c.TransactionTypeId != transactionTypeGST &&
                    (c.CreatedDateTime >= dFromDate &&
                    c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();

                objAccountVM.totalAmount = (double)objAccountVM.objEntityAccountList.Sum(x => x.Amount);
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getLedgerReport")]
        [HttpGet]
        public IHttpActionResult getLedgerReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeMoneyTransfer = Convert.ToInt32(helper.transactionTypes.MoneyTransfer);
                int transactionTypeBooking = Convert.ToInt32(helper.transactionTypes.Booking);
                int transactionTypeCommission = Convert.ToInt32(helper.transactionTypes.Commission);
                int transactionTypeRefund = Convert.ToInt32(helper.transactionTypes.Refund);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId &&
                    (c.TransactionTypeId == transactionTypeMoneyTransfer || c.TransactionTypeId == transactionTypeBooking || c.TransactionTypeId == transactionTypeCommission || c.TransactionTypeId == transactionTypeRefund) &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    (c.TransactionTypeId == transactionTypeMoneyTransfer || c.TransactionTypeId == transactionTypeBooking || c.TransactionTypeId == transactionTypeCommission || c.TransactionTypeId == transactionTypeRefund) &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).ThenBy(x => x.ClientTransactionId).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getCommissionReport")]
        [HttpGet]
        public IHttpActionResult getCommissionReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeCommission = Convert.ToInt32(helper.transactionTypes.Commission);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId && c.TransactionTypeId == transactionTypeCommission &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    c.TransactionTypeId == transactionTypeCommission &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getTDSReport")]
        [HttpGet]
        public IHttpActionResult getTDSReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeTDS = Convert.ToInt32(helper.transactionTypes.TDS);
                int transactionTypeBooking = Convert.ToInt32(helper.transactionTypes.Booking);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {

                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId &&
                    c.TransactionTypeId == transactionTypeTDS &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    c.TransactionTypeId == transactionTypeTDS &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                    x.UserName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.AccountTransactionId.ToString().Contains(searchValue.Trim().ToLower()) ||
                    x.ClientTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getGSTReport")]
        [HttpGet]
        public IHttpActionResult getGSTReport(int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                accountVM objAccountVM = new accountVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeGST = Convert.ToInt32(helper.transactionTypes.GST);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.EntityId == entityId && c.TransactionTypeId == transactionTypeGST &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                else
                {
                    objAccountVM.objEntityAccountList = db.QryEntityAccounts.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId) &&
                    c.TransactionTypeId == transactionTypeGST &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate)).ToList();
                }
                objAccountVM.totalRecords = objAccountVM.objEntityAccountList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.UserName.Trim().ToLower().Contains(searchValue) ||
                    x.Amount.ToString().Contains(searchValue) ||
                    x.TransactionType.Trim().ToLower().Contains(searchValue)).ToList();

                    objAccountVM.filterRecords = objAccountVM.objEntityAccountList.Count();
                }
                else
                    objAccountVM.filterRecords = objAccountVM.totalRecords;

                if (sortDirection == "asc")
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                else
                    objAccountVM.objEntityAccountList = objAccountVM.objEntityAccountList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityAccounts>();
                return Ok(objAccountVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(accountVM))]
        [Route("api/getAgencyWalletList")]
        [HttpGet]
        public IHttpActionResult getAgencyWalletList(int pageIndex, int pageSize, string searchValue, string sortDirection, long searchEntityId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityWalletVM objEntityWalletVM = new entityWalletVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                int transactionTypeGST = Convert.ToInt32(helper.transactionTypes.GST);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objEntityWalletVM.objEntityWalletList = db.QryEntityWallet.Where(c => c.ParentEntityId == entityId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId)).ToList();
                }
                else
                {
                    objEntityWalletVM.objEntityWalletList = db.QryEntityWallet.Where(c => c.CompanyId == companyId &&
                    (searchEntityId == 0 || c.EntityId == searchEntityId)).ToList();
                }
                objEntityWalletVM.totalRecords = objEntityWalletVM.objEntityWalletList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objEntityWalletVM.objEntityWalletList = objEntityWalletVM.objEntityWalletList.Where(x => x.AgencyName.Trim().ToLower().Contains(searchValue) ||
                    x.WalletBalance.ToString().Contains(searchValue)).ToList();

                    objEntityWalletVM.filterRecords = objEntityWalletVM.objEntityWalletList.Count();
                }
                else
                    objEntityWalletVM.filterRecords = objEntityWalletVM.totalRecords;

                if (sortDirection == "asc")
                    objEntityWalletVM.objEntityWalletList = objEntityWalletVM.objEntityWalletList.OrderBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityWallet>();
                else
                    objEntityWalletVM.objEntityWalletList = objEntityWalletVM.objEntityWalletList.OrderByDescending(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityWallet>();
                return Ok(objEntityWalletVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(string))]
        [Route("api/getAgencyWalletBalance")]
        [HttpGet]
        public IHttpActionResult getAgencyWalletBalance(long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    if (Convert.ToInt64(identityEntityId.Value) == entityId)
                    {
                        decimal entityWalletBalance = helper.getWalletBalance(entityId);
                        return Ok(entityWalletBalance.ToString());
                    }
                    else
                    {
                        return BadRequest(helper.invalidModuleAccess);
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [ResponseType(typeof(string))]
        [Route("api/getWalletBalance4Internal")]
        [HttpGet]
        public IHttpActionResult getWalletBalance4Internal(long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                decimal entityWalletBalance = helper.getWalletBalance(entityId);
                return Ok(entityWalletBalance.ToString());
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_AccountExists(long id)
        {
            return db.tblEntity_Account.Count(e => e.AccountId == id) > 0;
        }

        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/approvePayment")]
        [HttpGet]
        public IHttpActionResult approvePayment(long entityPaymentId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt32(identityEntityType.Value);
                var entityPaymentInDb = db.tblEntity_Payments.SingleOrDefault(x => x.EntityPaymentId == entityPaymentId);
                if (entityPaymentInDb == null)
                    return BadRequest("Oops something went wrong. Please try again later.");

                long receiverEntityId = entityPaymentInDb.EntityId;
                long receiverParentEntityId = 0;
                decimal amount = entityPaymentInDb.Amount;
                string updateWalletStatus = "";
                string updateEntityAccountStatus = "";
                var ReceiverEntity = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == receiverEntityId);
                if (entityTypeId == Convert.ToInt32(helper.entityTypes.company))
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    if (helper.hasEnoughCompanyWalletBalance(companyId, amount) == false)
                    {
                        return BadRequest("Low Virtual Balance!");
                    }

                    int ReceiverEntityTypeId = ReceiverEntity.EntityTypeId;
                    string entityTypeName = (ReceiverEntityTypeId == Convert.ToInt32(helper.entityTypes.masterDistributor) ? "MD" : ReceiverEntityTypeId == Convert.ToInt32(helper.entityTypes.distributor) ? "Distributor" : "Agent");
                    //Debiting Amount for Company Virtual Balance
                    helper.updateCompanyWalletBalance(companyId, -amount);
                    helper.transactCompanyAccount(companyId, receiverEntityId, -amount, "", "Add Wallet Balance in " + entityTypeName + " account", Convert.ToInt32(helper.transactionTypes.WalletTopUp), false, adminLoginId);

                    if (ReceiverEntityTypeId == Convert.ToInt32(helper.entityTypes.masterDistributor))
                    {
                        updateWalletStatus = helper.updateWalletBalance(receiverEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverEntityId, amount, "", "Wallet Topup Credit", 4, true, 0, 0, amount);
                        entityPaymentInDb.AccountingEntryStatus = updateEntityAccountStatus;
                        entityPaymentInDb.WalletUpdateStatus = updateWalletStatus;
                        entityPaymentInDb.IsPaymentReceived = true;
                        entityPaymentInDb.PaymentReceivedDateTime = helper.getIndianTime();
                        entityPaymentInDb.ParentEntityUserId = 0;
                        entityPaymentInDb.IsPaymentCredited2Wallet = true;
                        entityPaymentInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    }
                    if (ReceiverEntityTypeId == Convert.ToInt32(helper.entityTypes.distributor))
                    {
                        receiverParentEntityId = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == receiverEntityId).ParentEntityId;
                        //Credit Balance in MD
                        updateWalletStatus = helper.updateWalletBalance(receiverParentEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverParentEntityId, amount, "", "Wallet Topup Credit for " + ReceiverEntity.AgencyName, 4, true, 0, 0, amount);
                        // Debit Balance in MD
                        updateWalletStatus = helper.updateWalletBalance(receiverParentEntityId, -amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverParentEntityId, -amount, "", "Wallet Topup Debit for " + ReceiverEntity.AgencyName, 4, false, 0, 0, amount);
                        // Credit Balance in Distributor
                        updateWalletStatus = helper.updateWalletBalance(receiverEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverEntityId, amount, "", "Wallet Topup Credit", 4, true, 0, 0, amount);
                        entityPaymentInDb.AccountingEntryStatus = updateEntityAccountStatus;
                        entityPaymentInDb.WalletUpdateStatus = updateWalletStatus;
                        entityPaymentInDb.IsPaymentReceived = true;
                        entityPaymentInDb.PaymentReceivedDateTime = helper.getIndianTime();
                        entityPaymentInDb.ParentEntityUserId = 0;
                        entityPaymentInDb.IsPaymentCredited2Wallet = true;
                        entityPaymentInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    }
                    if (ReceiverEntityTypeId == Convert.ToInt32(helper.entityTypes.agent))
                    {
                        var receiverParentEntity = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == receiverEntityId);
                        receiverParentEntityId = receiverParentEntity.ParentEntityId;
                        long MDEntityId = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == receiverParentEntityId).ParentEntityId;
                        //Credit Balance in MD
                        updateWalletStatus = helper.updateWalletBalance(MDEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(MDEntityId, amount, "", "Wallet Topup Credit for " + receiverParentEntity.AgencyName, 4, true, 0, 0, amount);
                        // Debit Balance in MD
                        updateWalletStatus = helper.updateWalletBalance(MDEntityId, -amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(MDEntityId, -amount, "", "Wallet Topup Debit for " + receiverParentEntity.AgencyName, 4, false, 0, 0, amount);
                        // Credit Balance in Distributor
                        updateWalletStatus = helper.updateWalletBalance(receiverParentEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverParentEntityId, amount, "", "Wallet Topup Credit for " + ReceiverEntity.AgencyName, 4, true, 0, 0, amount);
                        // Debit Balance from Distributor Account
                        updateWalletStatus = helper.updateWalletBalance(receiverParentEntityId, -amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverParentEntityId, -amount, "", "Wallet Topup Debit for " + ReceiverEntity.AgencyName, 4, false, 0, 0, amount);
                        // Credit Balance in Agent Account
                        updateWalletStatus = helper.updateWalletBalance(receiverEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverEntityId, amount, "", "Wallet Topup Credit", 4, true, 0, 0, amount);
                        entityPaymentInDb.AccountingEntryStatus = updateEntityAccountStatus;
                        entityPaymentInDb.WalletUpdateStatus = updateWalletStatus;
                        entityPaymentInDb.IsPaymentReceived = true;
                        entityPaymentInDb.PaymentReceivedDateTime = helper.getIndianTime();
                        entityPaymentInDb.ParentEntityUserId = 0;
                        entityPaymentInDb.IsPaymentCredited2Wallet = true;
                        entityPaymentInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    }
                }
                else
                {
                    var parentEntityInDb = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == receiverEntityId);
                    if (parentEntityInDb != null)
                    {
                        receiverParentEntityId = parentEntityInDb.ParentEntityId;
                        Nullable<int> minimumBalance2Maintain = parentEntityInDb.MinimumBalanceMaintain;
                        decimal availableWalletBalance = helper.getWalletBalance(receiverParentEntityId) - (int)minimumBalance2Maintain;
                        if (availableWalletBalance <= amount)
                            return BadRequest("Oops! Wallet balance is low");

                        // Debit Balance in MD
                        updateWalletStatus = helper.updateWalletBalance(receiverParentEntityId, -amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverParentEntityId, -amount, "", "Wallet Topup Debit for " + ReceiverEntity.AgencyName, 4, false, 0, 0, amount);
                        // Credit Balance in Distributor
                        updateWalletStatus = helper.updateWalletBalance(receiverEntityId, amount);
                        updateEntityAccountStatus = helper.transactEntityAccount(receiverEntityId, amount, "", "Wallet Topup Credit", 4, true, 0, 0, amount);
                        entityPaymentInDb.AccountingEntryStatus = updateEntityAccountStatus;
                        entityPaymentInDb.WalletUpdateStatus = updateWalletStatus;
                        entityPaymentInDb.IsPaymentReceived = true;
                        entityPaymentInDb.PaymentReceivedDateTime = helper.getIndianTime();
                        entityPaymentInDb.ParentEntityUserId = 0;
                        entityPaymentInDb.IsPaymentCredited2Wallet = true;
                        entityPaymentInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    }
                    else
                        return BadRequest("Oops! Parent missing.");
                }

                db.Entry(entityPaymentInDb).State = EntityState.Modified;
                db.Entry(entityPaymentInDb).Property(x => x.EntityId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.PaymentModeId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.Amount).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankBranchCode).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankTransactionId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.ChequeNo).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.PaymentSlip).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.Remark).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.ParentEntityId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.RejectionRemark).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.RejectionDateTime).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.IsPaymentMade2Company).IsModified = false;
                db.SaveChanges();
                return Ok();
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/rejectPayment")]
        [HttpPost]
        public IHttpActionResult rejectPayment(tblEntity_Payments objEntityPayment)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt32(identityEntityType.Value);
                var entityPaymentInDb = db.tblEntity_Payments.SingleOrDefault(x => x.EntityPaymentId == objEntityPayment.EntityPaymentId);
                if (entityPaymentInDb == null)
                    return BadRequest("Oops something went wrong. Please try again later.");

                entityPaymentInDb.RejectionRemark = objEntityPayment.RejectionRemark;
                entityPaymentInDb.RejectionDateTime = helper.getIndianTime();
                db.Entry(entityPaymentInDb).State = EntityState.Modified;
                db.Entry(entityPaymentInDb).Property(x => x.EntityId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.PaymentModeId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.Amount).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankBranchCode).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.BankTransactionId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.ChequeNo).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.PaymentSlip).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.Remark).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.ParentEntityId).IsModified = false;
                db.Entry(entityPaymentInDb).Property(x => x.IsPaymentMade2Company).IsModified = false;
                db.SaveChanges();
                return Ok();
            }
            else
                return Unauthorized();
        }
    }
}