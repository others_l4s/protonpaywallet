﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;
using application.webAPI.slabcommission.mobile.response;

namespace application.webAPI.Controllers
{
    public class entitySlabCommissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entitySlabCommission
        /// <summary>
        /// This API will get list of all parameters and will work as single commission or slab wise commission
        /// </summary>
        /// <param name="commissionBy">Single Commission or Slab Wise Commission</param>
        /// <returns></returns>
        [Authorize]
        [ResponseType(typeof(entitySlabCommissionVM))]
        [HttpGet]
        public IHttpActionResult entitySlabCommission(int commissionBy)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                long companyId = Convert.ToInt64(identityCompanyId.Value);
                entitySlabCommissionVM objEntitySlabCommissionVM = new entitySlabCommissionVM();
                if (entityType == helper.defaultCompany)
                {
                    int entityTypeId = Convert.ToInt32(helper.entityTypes.masterDistributor);
                    objEntitySlabCommissionVM.objEntityList = db.tblEntity_Master.Where(x => x.CompanyId == companyId && x.StatusId == helper.defaultActiveStatusId && x.EntityTypeId == entityTypeId).ToList();
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntitySlabCommissionVM.objEntityList = db.tblEntity_Master.Where(x => x.ParentEntityId == entityId && x.StatusId == helper.defaultActiveStatusId).ToList();
                    objEntitySlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId).ToList();
                }
                if (commissionBy == 1) // Single Commission
                    objEntitySlabCommissionVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == false && x.CustomCommissionApplicable == false).ToList();
                else
                    objEntitySlabCommissionVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == true && x.CustomCommissionApplicable == false).ToList();
                objEntitySlabCommissionVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeName != helper.entityTypes.company.ToString()).ToList();
                objEntitySlabCommissionVM.objCommissionTypeList = db.tblCommissionType.ToList();
                objEntitySlabCommissionVM.objEntitySlabCommission = new tblEntity_Slab_Commission();
                return Ok(objEntitySlabCommissionVM);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entitySlabCommissionVM))]
        [HttpGet]
        public IHttpActionResult entitySlabCommissionList(int commissionBy, int searchProductId, Int32 searchEntityTypeId, int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (commissionBy == 1) // single Commission
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.SlabCommissionApplicable == false &&
                        (searchEntityTypeId == 0 || x.EntityTypeId == searchEntityTypeId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.ParentSlabCommissionId == 0 && x.IsCustom == false &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                    else
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.SlabCommissionApplicable == true &&
                        (searchEntityTypeId == 0 || x.EntityTypeId == searchEntityTypeId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.ParentSlabCommissionId == 0 && x.IsCustom == false &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (commissionBy == 1) // Single value Commission
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                        x.SlabCommissionApplicable == false &&
                        (searchEntityTypeId == 0 || x.EntityTypeId == searchEntityTypeId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.ParentSlabCommissionId == 0 && x.IsCustom == false &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                    else
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                        x.SlabCommissionApplicable == true &&
                        (searchEntityTypeId == 0 || x.EntityTypeId == searchEntityTypeId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.ParentSlabCommissionId > 0 && x.IsCustom == false &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                }
                objSlabCommissionVM.totalRecords = objSlabCommissionVM.objEntitySlabcommissionList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.Where(s => s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.EntityTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                    objSlabCommissionVM.filterRecords = objSlabCommissionVM.objEntitySlabcommissionList.Count();
                }
                else
                    objSlabCommissionVM.filterRecords = objSlabCommissionVM.totalRecords;

                if (sortDirection == "asc")
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntity_Slab_Commission>();
                else
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntity_Slab_Commission>();

                return Ok(objSlabCommissionVM);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entitySlabCommissionVM))]
        [HttpGet]
        public IHttpActionResult entitySlabCommissionList(int commissionBy, int searchProductId, long searchEntityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (commissionBy == 1) // single Commission
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.SlabCommissionApplicable == false &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                    else
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.SlabCommissionApplicable == true &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.CustomCommissionApplicable == false).ToList();
                    }
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (commissionBy == 1) // Single value Commission
                    {
                        objSlabCommissionVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == false && x.CustomCommissionApplicable == false).ToList();

                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.EntityId == entityId &&
                        x.SlabCommissionApplicable == false &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.CustomCommissionApplicable == false && x.ParentSlabCommissionId == 0).ToList();

                        objSlabCommissionVM.objEntityDownlineSlabCommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                        x.SlabCommissionApplicable == false &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.CustomCommissionApplicable == false && x.ParentSlabCommissionId > 0).ToList();
                    }
                    else
                    {

                        objSlabCommissionVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == true && x.CustomCommissionApplicable == false).ToList();

                        objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.EntityId == entityId &&
                      x.SlabCommissionApplicable == true &&
                          (searchProductId == 0 || x.ProductId == searchProductId) &&
                      x.CustomCommissionApplicable == false).ToList();

                        objSlabCommissionVM.objEntityDownlineSlabCommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                        x.SlabCommissionApplicable == true &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                            (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.CustomCommissionApplicable == false && x.ParentSlabCommissionId > 0).ToList();
                    }
                }
                return Ok(objSlabCommissionVM);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entitySlabCommissionVM_Mobile))]
        [Route("api/entitySlabCommissionList/slabCommissionList4MobileApp")]
        [HttpGet]
        public IHttpActionResult entitySlabCommissionList(int commissionBy, int searchProductId, long searchEntityId, int mobileDevice)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                entitySlabCommissionVM_Mobile objSlabCommissionVM = new entitySlabCommissionVM_Mobile();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);

                    List<QryEntity_Slab_Commission> objEntitySlabCommissionList = db.QryEntity_Slab_Commission.Where(x => x.EntityId == entityId &&
                  x.SlabCommissionApplicable == true &&
                      (searchProductId == 0 || x.ProductId == searchProductId) &&
                  x.CustomCommissionApplicable == false).ToList();

                    List<QryEntity_Slab_Commission> objEntityDownlineSlabCommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                   x.SlabCommissionApplicable == true &&
                   (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                       (searchProductId == 0 || x.ProductId == searchProductId) &&
                   x.CustomCommissionApplicable == false).ToList();

                    if (objEntitySlabCommissionList.Count > 0)
                    {
                        objSlabCommissionVM.objEntitySlabcommissionList = new List<tblEntitySlabCommission_Mobile>();
                        foreach (QryEntity_Slab_Commission objData in objEntitySlabCommissionList)
                        {
                            decimal commissionValue = 0;
                            long ParentSlabCommissionId = 0;
                            if (objEntityDownlineSlabCommissionList.Count > 0)
                            {
                                var dataInDownlineList = objEntityDownlineSlabCommissionList.Where(x => x.ParentSlabCommissionId == objData.SlabCommissionId && x.EntityId == searchEntityId).SingleOrDefault();
                                if (dataInDownlineList != null)
                                {
                                    commissionValue = dataInDownlineList.CommissionValue;
                                    ParentSlabCommissionId = dataInDownlineList.ParentSlabCommissionId;
                                }
                                else
                                {
                                    commissionValue = 0;
                                    ParentSlabCommissionId = objData.ParentSlabCommissionId;
                                }
                            }

                            tblEntitySlabCommission_Mobile objSlabMobileData = new tblEntitySlabCommission_Mobile();
                            objSlabMobileData.SlabCommissionId = objData.SlabCommissionId;
                            objSlabMobileData.CompanyId = objData.CompanyId;
                            objSlabMobileData.CompanyName = objData.CompanyName;
                            objSlabMobileData.ProductId = objData.ProductId;
                            objSlabMobileData.ProductCode = objData.ProductCode;
                            objSlabMobileData.CommissionTypeName = objData.CommissionTypeName;
                            objSlabMobileData.CommissionTypeId = objData.CommissionTypeId;
                            objSlabMobileData.FromAmount = objData.FromAmount;
                            objSlabMobileData.ToAmount = objData.ToAmount;
                            objSlabMobileData.CommissionValue = commissionValue;
                            objSlabMobileData.DefaultCommissionValue = objData.CommissionValue;
                            objSlabMobileData.CreatedDateTime = objData.CreatedDateTime;
                            objSlabMobileData.CreatedBy = objData.CreatedBy;
                            objSlabMobileData.EntityId = objData.EntityId;
                            objSlabMobileData.AgencyName = objData.AgencyName;
                            objSlabMobileData.IsCustom = objData.IsCustom;
                            objSlabMobileData.EntityTypeId = objData.EntityTypeId;
                            objSlabMobileData.EntityTypeName = objData.EntityTypeName;
                            objSlabMobileData.SlabCommissionApplicable = objData.SlabCommissionApplicable;
                            objSlabMobileData.CustomCommissionApplicable = objData.CustomCommissionApplicable;
                            objSlabMobileData.ParentSlabCommissionId = ParentSlabCommissionId;
                            objSlabCommissionVM.objEntitySlabcommissionList.Add(objSlabMobileData);
                        }
                    }
                }
                return Ok(objSlabCommissionVM);
            }
            else
                return Unauthorized();
        }

        // GET: api/entitySlabCommission/5
        [Authorize]
        [ResponseType(typeof(entitySlabCommissionVM))]
        [HttpGet]
        public IHttpActionResult entitySlabCommissionById(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                long companyId = Convert.ToInt64(identityCompanyId.Value);
                entitySlabCommissionVM objEntitySlabCommissionVM = new entitySlabCommissionVM();
                objEntitySlabCommissionVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == true && x.CustomCommissionApplicable == false).ToList();
                objEntitySlabCommissionVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeName != helper.entityTypes.company.ToString()).ToList();
                objEntitySlabCommissionVM.objCommissionTypeList = db.tblCommissionType.ToList();
                objEntitySlabCommissionVM.objEntitySlabCommission = db.tblEntity_Slab_Commission.Find(id);
                if (objEntitySlabCommissionVM.objEntitySlabCommission == null)
                {
                    return NotFound();
                }
                if (objEntitySlabCommissionVM.objEntitySlabCommission.EntityId > 0)
                    objEntitySlabCommissionVM.objEntityList = db.tblEntity_Master.Where(x => x.EntityTypeId == objEntitySlabCommissionVM.objEntitySlabCommission.EntityTypeId).OrderBy(x => x.AgencyName).ToList();
                else
                    objEntitySlabCommissionVM.objEntityList = new List<tblEntity_Master>();

                return Ok(objEntitySlabCommissionVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/entitySlabCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult entitySlabCommissionUpdate(long id, tblEntity_Slab_Commission objEntitySlabCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objEntitySlabCommission.SlabCommissionId)
                {
                    return BadRequest();
                }
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var entitySlabCommissionInDb = db.tblEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                x.FromAmount == objEntitySlabCommission.FromAmount &&
                x.ToAmount == objEntitySlabCommission.ToAmount &&
                x.CommissionTypeId == objEntitySlabCommission.CommissionTypeId &&
                x.CommissionValue == objEntitySlabCommission.CommissionValue && x.EntityTypeId == objEntitySlabCommission.EntityTypeId && x.IsCustom == false && x.SlabCommissionId != objEntitySlabCommission.SlabCommissionId).SingleOrDefault();

                if (entitySlabCommissionInDb != null)
                    return BadRequest("Commission data already exists.");

                db.Entry(objEntitySlabCommission).State = EntityState.Modified;
                db.Entry(objEntitySlabCommission).Property(x => x.CreatedBy).IsModified = false;
                db.Entry(objEntitySlabCommission).Property(x => x.CompanyId).IsModified = false;
                db.Entry(objEntitySlabCommission).Property(x => x.CreatedDateTime).IsModified = false;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblEntity_Slab_CommissionExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }

        }

        // POST: api/entitySlabCommission
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPost]

        public IHttpActionResult entitySlabCommissionSave(tblEntity_Slab_Commission objEntitySlabCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                if (entityType == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    var entitySlabCommissionInDb = db.tblEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                    x.FromAmount == objEntitySlabCommission.FromAmount &&
                    x.ToAmount == objEntitySlabCommission.ToAmount &&
                    x.CommissionTypeId == objEntitySlabCommission.CommissionTypeId &&
                    x.CommissionValue == objEntitySlabCommission.CommissionValue &&
                    x.EntityTypeId == objEntitySlabCommission.EntityTypeId &&
                    x.IsCustom == objEntitySlabCommission.IsCustom &&
                    x.ProductId == objEntitySlabCommission.ProductId &&
                    x.SlabCommissionId != objEntitySlabCommission.SlabCommissionId).SingleOrDefault();

                    if (entitySlabCommissionInDb != null)
                        return BadRequest("Commission data already exists.");


                    objEntitySlabCommission.CreatedBy = Convert.ToInt64(identityAdminLoginId.Value);
                    objEntitySlabCommission.CompanyId = companyId;
                    //objEntitySlabCommission.EntityTypeId = entityType;
                    objEntitySlabCommission.CreatedDateTime = helper.getIndianTime();
                    db.tblEntity_Slab_Commission.Add(objEntitySlabCommission);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException)
                    {
                        if (tblEntity_Slab_CommissionExists(objEntitySlabCommission.SlabCommissionId))
                        {
                            return Conflict();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    var entitySlabCommissionInDb = db.tblEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.FromAmount == objEntitySlabCommission.FromAmount &&
                        x.ToAmount == objEntitySlabCommission.ToAmount &&
                        x.CommissionTypeId == objEntitySlabCommission.CommissionTypeId &&
                        x.IsCustom == false &&
                        x.EntityId == objEntitySlabCommission.EntityId &&
                        x.ProductId == objEntitySlabCommission.ProductId &&
                        x.ParentSlabCommissionId == objEntitySlabCommission.ParentSlabCommissionId).SingleOrDefault();

                    if (entitySlabCommissionInDb != null)
                    {
                        entitySlabCommissionInDb.CommissionValue = objEntitySlabCommission.CommissionValue;
                        db.Entry(entitySlabCommissionInDb).State = EntityState.Modified;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.CompanyId).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.ProductId).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.CommissionTypeId).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.FromAmount).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.ToAmount).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.CreatedDateTime).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.EntityId).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.IsCustom).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.EntityTypeId).IsModified = false;
                        db.Entry(entitySlabCommissionInDb).Property(x => x.ParentSlabCommissionId).IsModified = false;
                        db.SaveChanges();
                    }
                    else
                    {
                        objEntitySlabCommission.CreatedBy = Convert.ToInt64(identityEntityId.Value);
                        objEntitySlabCommission.CompanyId = companyId;
                        objEntitySlabCommission.EntityTypeId = entityType;
                        objEntitySlabCommission.CreatedDateTime = helper.getIndianTime();
                        db.tblEntity_Slab_Commission.Add(objEntitySlabCommission);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbUpdateException)
                        {
                            if (tblEntity_Slab_CommissionExists(objEntitySlabCommission.SlabCommissionId))
                            {
                                return Conflict();
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/entitySlabCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult DeletetblEntity_Slab_Commission(long id)
        {
            tblEntity_Slab_Commission objEntitySlabCommission = db.tblEntity_Slab_Commission.Find(id);
            if (objEntitySlabCommission == null)
            {
                return NotFound();
            }
            db.tblEntity_Slab_Commission.Remove(objEntitySlabCommission);
            db.SaveChanges();
            return Ok(objEntitySlabCommission);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_Slab_CommissionExists(long id)
        {
            return db.tblEntity_Slab_Commission.Count(e => e.SlabCommissionId == id) > 0;
        }

        #region Custom Commission

        [Authorize]
        [Route("api/entitySlabCommission/getCustomCommissionList")]
        [ResponseType(typeof(entitySlabCommissionVM))]
        [HttpGet]
        public IHttpActionResult getCustomCommissionList(int searchProductId, long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                entitySlabCommissionVM objSlabCommissionVM = new entitySlabCommissionVM();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CompanyId == companyId &&
                        x.SlabCommissionApplicable == true &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        (searchProductId == 0 || x.ProductId == searchProductId) &&
                        x.ParentSlabCommissionId == 0 && x.IsCustom == true &&
                        x.CustomCommissionApplicable == false).ToList();
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objSlabCommissionVM.objEntitySlabcommissionList = db.QryEntity_Slab_Commission.Where(x => x.CreatedBy == entityId &&
                       x.SlabCommissionApplicable == true &&
                       (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                       (searchProductId == 0 || x.ProductId == searchProductId) &&
                       x.ParentSlabCommissionId > 0 && x.IsCustom == true &&
                       x.CustomCommissionApplicable == false).ToList();
                }
                objSlabCommissionVM.totalRecords = objSlabCommissionVM.objEntitySlabcommissionList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.Where(s => s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.EntityTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                    objSlabCommissionVM.filterRecords = objSlabCommissionVM.objEntitySlabcommissionList.Count();
                }
                else
                    objSlabCommissionVM.filterRecords = objSlabCommissionVM.totalRecords;

                if (sortDirection == "asc")
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntity_Slab_Commission>();
                else
                    objSlabCommissionVM.objEntitySlabcommissionList = objSlabCommissionVM.objEntitySlabcommissionList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntity_Slab_Commission>();

                return Ok(objSlabCommissionVM);
            }
            else
                return Unauthorized();
        }

        #endregion
    }
}
