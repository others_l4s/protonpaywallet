﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class myRoboticRechargeUtilityDataController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/myRoboticRechargeUtilityData
        [Authorize]
        [ResponseType(typeof(myRoboticRechargeUtilityDataVM))]
        [HttpGet]
        public IHttpActionResult getMyRoboticRechargeUtilityData()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                myRoboticRechargeUtilityDataVM objMyRoboticRechargeUtilityDataVM = new myRoboticRechargeUtilityDataVM();
                if (entityType == helper.defaultCompany)
                {
                    objMyRoboticRechargeUtilityDataVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == true).ToList();
                    objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityData = new tblMyRobotic_Recharge_UtilityData();
                    return Ok(objMyRoboticRechargeUtilityDataVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/myRoboticRechargeUtilityData/5
        [Authorize]
        [ResponseType(typeof(tblMyRobotic_Recharge_UtilityData))]
        [HttpGet]
        public IHttpActionResult getMyRoboticRechargeUtilityDataById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblMyRobotic_Recharge_UtilityData objMyRoboticRechargeUtilityData = db.tblMyRobotic_Recharge_UtilityData.Find(id);
                if (objMyRoboticRechargeUtilityData == null)
                {
                    return NotFound();
                }
                return Ok(objMyRoboticRechargeUtilityData);
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/myRoboticRechargeUtilityData/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult myRoboticRechargeUtilityDataUpdate(int id, tblMyRobotic_Recharge_UtilityData objMyRoboticRechargeUtilityData)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objMyRoboticRechargeUtilityData.MyRoboticRechargeDataId)
                {
                    return BadRequest();
                }
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var utilityDataInDb = db.tblMyRobotic_Recharge_UtilityData.Where(x => x.CompanyId == companyId && x.OperatorName == objMyRoboticRechargeUtilityData.OperatorName && x.ProductId == objMyRoboticRechargeUtilityData.ProductId
                && x.MyRoboticRechargeDataId != objMyRoboticRechargeUtilityData.MyRoboticRechargeDataId).SingleOrDefault();
                if (utilityDataInDb != null)
                    return BadRequest("Data already exists.");

                db.Entry(objMyRoboticRechargeUtilityData).State = EntityState.Modified;
                db.Entry(objMyRoboticRechargeUtilityData).Property(x => x.CompanyId).IsModified = false;
                db.Entry(objMyRoboticRechargeUtilityData).Property(x => x.CreatedDateTime).IsModified = false;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblMyRobotic_Recharge_UtilityDataExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/myRoboticRechargeUtilityData
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult myRoboticRechargeUtilityDataSave(tblMyRobotic_Recharge_UtilityData objMyRoboticRechargeUtilityData)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                if (entityType == helper.defaultCompany)
                {
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    var utilityDataInDb = db.tblMyRobotic_Recharge_UtilityData.Where(x => x.CompanyId == companyId && x.OperatorName == objMyRoboticRechargeUtilityData.OperatorName && x.ProductId == objMyRoboticRechargeUtilityData.ProductId
                    && x.OperatorCode == objMyRoboticRechargeUtilityData.OperatorCode &&
                    x.MyRoboticRechargeDataId != objMyRoboticRechargeUtilityData.MyRoboticRechargeDataId).SingleOrDefault();
                    if (utilityDataInDb != null)
                        return BadRequest("Data already exists.");

                    objMyRoboticRechargeUtilityData.CompanyId = companyId;
                    objMyRoboticRechargeUtilityData.CreatedDateTime = helper.getIndianTime();
                    db.tblMyRobotic_Recharge_UtilityData.Add(objMyRoboticRechargeUtilityData);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        // DELETE: api/myRoboticRechargeUtilityData/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult myRoboticRechargeUtilityDataDelete(int id)
        {
            tblMyRobotic_Recharge_UtilityData tblMyRobotic_Recharge_UtilityData = db.tblMyRobotic_Recharge_UtilityData.Find(id);
            if (tblMyRobotic_Recharge_UtilityData == null)
            {
                return NotFound();
            }

            db.tblMyRobotic_Recharge_UtilityData.Remove(tblMyRobotic_Recharge_UtilityData);
            db.SaveChanges();

            return Ok(tblMyRobotic_Recharge_UtilityData);
        }

        [Authorize]
        [ResponseType(typeof(myRoboticRechargeUtilityDataVM))]
        [HttpGet]
        public IHttpActionResult getMyRoboticRechargeUtilityDataList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    myRoboticRechargeUtilityDataVM objMyRoboticRechargeUtilityDataVM = new myRoboticRechargeUtilityDataVM();
                    objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList = db.QryMyRobotic_Recharge_UtilityData.Where(x => x.CompanyId == companyId).ToList();
                    objMyRoboticRechargeUtilityDataVM.totalRecords = objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList = objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList.Where(s => s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) || s.OperatorName.Trim().ToLower().Contains(searchValue.Trim().ToString())).ToList();

                        objMyRoboticRechargeUtilityDataVM.filterRecords = objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList.Count();
                    }
                    else
                        objMyRoboticRechargeUtilityDataVM.filterRecords = objMyRoboticRechargeUtilityDataVM.totalRecords;

                    if (sortDirection == "asc")
                        objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList = objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryMyRobotic_Recharge_UtilityData>();
                    else
                        objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList = objMyRoboticRechargeUtilityDataVM.objMyRoboticRechargeUtilityDataList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryMyRobotic_Recharge_UtilityData>();

                    return Ok(objMyRoboticRechargeUtilityDataVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMyRobotic_Recharge_UtilityDataExists(int id)
        {
            return db.tblMyRobotic_Recharge_UtilityData.Count(e => e.MyRoboticRechargeDataId == id) > 0;
        }
    }
}