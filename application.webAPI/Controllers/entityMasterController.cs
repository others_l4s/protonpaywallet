﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class entityMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entityMaster
        [Authorize]
        [ResponseType(typeof(entityMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityMasterList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                entityMasterViewModel objEntityMasterViewModel = new entityMasterViewModel()
                {
                    objEntityMaster = new tblEntity_Master(),
                    //objCompanyList = (CompanyId == helper.defaultCompany ? db.tblCompany_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderByDescending(c => c.CreatedDateTime).ToList() : db.tblCompany_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderByDescending(c => c.CreatedDateTime).ToList()),
                    objStatusList = db.tblStatus_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.StatusName).ToList(),
                    objCountryList = db.tblCountry_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CountryName).ToList(),
                    objStateList = new List<tblState_Master>(),
                    objCityList = new List<tblCity_Master>(),
                    //objEntityTypeList = db.tblEntity_Type.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(e => e.EntityTypeName).ToList(),
                    objAccountHandlerList = db.tblAdminLogin.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderByDescending(c => c.CreatedDateTime).ToList(),
                    //objBankAccountTypeList = db.tblBankAccountType.OrderBy(c => c.BankAccountTypeName).ToList()
                    //objParentList = (CompanyId == helper.defaultCompany ? db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderByDescending(c => c.CreatedDateTime).ToList() : db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderByDescending(c => c.CreatedDateTime).ToList())
                };
                return Ok(objEntityMasterViewModel);
            }
            else
            {
                return Unauthorized();
            }
        }


        [Authorize]
        [ResponseType(typeof(entityMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityMasterByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection, int EntityTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityMasterViewModel objEntityMasterViewModel = new entityMasterViewModel();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    int retailerEntityType = Convert.ToInt32(helper.entityTypes.agent);
                    objEntityMasterViewModel.objEntityMasterList = db.QryEntityMaster.Where(e => e.CompanyId == companyId && e.EntityTypeId == retailerEntityType).ToList();
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityMasterViewModel.objEntityMasterList = db.QryEntityMaster.Where(e => e.ParentEntityId == entityId).ToList();
                }
                objEntityMasterViewModel.totalRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.Where(s => s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Website.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.OfficeAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.OfficeAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Landmark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.CountryName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.PersonName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                    objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                }
                else
                    objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.totalRecords;
                if (sortDirection == "asc")
                    objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();
                else
                    objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderByDescending(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();

                return Ok(objEntityMasterViewModel);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(List<tblEntity_Master>))]
        [HttpGet]
        [Route("api/entity/getDistributorListByMD")]
        public IHttpActionResult getDistributorListByMD(long masterDistributorId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var userIdentityClaimValue = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                List<tblEntity_Master> objEntityMaster = new List<tblEntity_Master>();
                objEntityMaster = db.tblEntity_Master.Where(c => c.ParentEntityId == masterDistributorId && c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.AgencyName).ToList();
                return Ok(objEntityMaster);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(List<tblEntity_Master>))]
        [HttpGet]
        [Route("api/entity/getEntityByEntityTypeId")]
        public IHttpActionResult getEntityByEntityId(Int32 entityTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                Int32 entityType = Convert.ToInt32(identityEntityType.Value);
                List<tblEntity_Master> objEntityMaster = new List<tblEntity_Master>();
                if (entityType == helper.defaultCompany)
                    objEntityMaster = db.tblEntity_Master.Where(c => c.EntityTypeId == entityTypeId && c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.AgencyName).ToList();
                else
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityMaster = db.tblEntity_Master.Where(c => c.EntityTypeId == entityTypeId && c.ParentEntityId == entityId && c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.AgencyName).ToList();
                }
                return Ok(objEntityMaster);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [HttpGet]
        [Route("api/entity/getEntityType4Report")]
        public IHttpActionResult getEntityType4Report()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityReportVM objEntityReportVM = new entityReportVM();
                objEntityReportVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != 1).ToList();
                objEntityReportVM.objEntityList = new List<tblEntity_Master>();
                return Ok(objEntityReportVM);
            }
            else
                return Unauthorized();
        }

        // GET: api/entityMaster/5
        [Authorize]
        [ResponseType(typeof(entityMasterViewModel))]
        public IHttpActionResult getEntityMasterByEntityId(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                tblEntity_Master objEntityMaster = db.tblEntity_Master.Find(id);
                if (objEntityMaster == null)
                {
                    return NotFound();
                }

                entityMasterViewModel objEntityMasterViewModel = new entityMasterViewModel();
                objEntityMasterViewModel.objEntityMaster = objEntityMaster;
                objEntityMasterViewModel.objStatusList = db.tblStatus_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.StatusName).ToList();
                objEntityMasterViewModel.objCountryList = db.tblCountry_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CountryName).ToList();
                objEntityMasterViewModel.objStateList = db.tblState_Master.Where(c => c.CountryId == objEntityMaster.CountryId).ToList();
                objEntityMasterViewModel.objCityList = db.tblCity_Master.Where(c => c.StateId == objEntityMaster.StateId).ToList();
                objEntityMasterViewModel.objAccountHandlerList = (CompanyId == helper.defaultCompany ? db.tblAdminLogin.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.PersonName).ToList() : db.tblAdminLogin.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.PersonName).ToList());
                objEntityMasterViewModel.objEntityUser = db.tblEntity_Users.SingleOrDefault(c => c.EntityId == objEntityMaster.EntityId && c.LeadUser == true);
                var MDEntityIdInDb = db.tblEntity_Master.SingleOrDefault(md => md.EntityId == objEntityMaster.ParentEntityId);
                if (MDEntityIdInDb != null)
                    objEntityMasterViewModel.MDParentEntityId = MDEntityIdInDb.ParentEntityId;
                return Ok(objEntityMasterViewModel);
            }
            else
                return Unauthorized();
        }

        // PUT: api/entityMaster/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Master))]
        [HttpPut]
        public IHttpActionResult updateEntityMaster(int id, tblEntity_Master objEntityMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objEntityMaster.EntityId)
                {
                    return BadRequest();
                }
                try
                {
                    var entityInBb = db.tblEntity_Master.SingleOrDefault(e => e.AgencyName.Trim().ToLower() == objEntityMaster.AgencyName.Trim().ToLower() &&
                    e.EntityId != objEntityMaster.EntityId &&
                    e.CompanyId == objEntityMaster.CompanyId &&
                    e.EntityTypeId == objEntityMaster.EntityTypeId);

                    if (entityInBb != null)
                        return BadRequest("Agenty already exits!");

                    db.Entry(objEntityMaster).State = EntityState.Modified;
                    db.Entry(objEntityMaster).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(objEntityMaster).Property(x => x.CompanyId).IsModified = false;
                    db.Entry(objEntityMaster).Property(x => x.EntityTypeId).IsModified = false;
                    db.Entry(objEntityMaster).Property(x => x.ParentEntityId).IsModified = false;
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!doesEntityMasterExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok(objEntityMaster);
            }
            else
                return Unauthorized();
        }

        // POST: api/entityMaster
        [Authorize]
        [ResponseType(typeof(tblEntity_Master))]
        [Route("api/saveMasterDistributor")]
        [HttpPost]
        public IHttpActionResult saveMasterDistributor(tblEntity_Master objEntityMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    int entityTypeId = Convert.ToInt32(helper.entityTypes.masterDistributor);
                    var entityInBb = db.tblEntity_Master.SingleOrDefault(e => e.AgencyName.Trim().ToLower() == objEntityMaster.AgencyName.Trim().ToLower() &&
                    e.EntityTypeId == entityTypeId &&
                    e.CompanyId == companyId &&
                    e.Domain == objEntityMaster.Domain);
                    if (entityInBb != null)
                        return BadRequest("Agency Name already exits!");

                    objEntityMaster.CompanyId = companyId;
                    objEntityMaster.CreatedDateTime = helper.getIndianTime();
                    objEntityMaster.CreatedBy = adminLoginId;
                    db.tblEntity_Master.Add(objEntityMaster);
                    db.SaveChanges();
                    return Ok(objEntityMaster);
                }
                else
                {
                    return BadRequest("Invalid Module Access!");
                }
            }
            else return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(tblEntity_Master))]
        [Route("api/saveDistributor")]
        [HttpPost]
        public IHttpActionResult saveDistributor(tblEntity_Master objEntityMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long parentEntityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    int entityTypeId = Convert.ToInt32(helper.entityTypes.distributor);

                    var entityInBb = db.tblEntity_Master.SingleOrDefault(e => e.AgencyName.Trim().ToLower() == objEntityMaster.AgencyName.Trim().ToLower() &&
                    e.EntityTypeId == entityTypeId &&
                    e.ParentEntityId == parentEntityId &&
                    e.CompanyId == companyId &&
                    e.Domain == objEntityMaster.Domain);
                    if (entityInBb != null)
                        return BadRequest("Agency Name already exits!");

                    objEntityMaster.CompanyId = companyId;
                    objEntityMaster.CreatedDateTime = helper.getIndianTime();
                    objEntityMaster.CreatedBy = entityUserId;
                    objEntityMaster.ParentEntityId = parentEntityId;
                    db.tblEntity_Master.Add(objEntityMaster);
                    db.SaveChanges();
                    return Ok(objEntityMaster);
                }
                else
                {
                    return BadRequest("Invalid Module Access!");
                }
            }
            else return Unauthorized();
        }

        [ResponseType(typeof(tblEntity_Master))]
        [Route("api/saveAgent")]
        [HttpPost]
        public IHttpActionResult saveAgent(tblEntity_Master objEntityMaster)
        {
            objEntityMaster.CompanyId = 2;
            objEntityMaster.AgencyName = objEntityMaster.Telephone1;
            objEntityMaster.Website = string.Empty;
            objEntityMaster.Domain = string.Empty;
            objEntityMaster.OfficeAddress1 = string.Empty;
            objEntityMaster.OfficeAddress2 = string.Empty;
            objEntityMaster.Landmark = string.Empty;
            objEntityMaster.StateId = 0;
            objEntityMaster.CityId = 0;
            objEntityMaster.CountryId = 0;
            objEntityMaster.PinCode = string.Empty;
            objEntityMaster.Telephone2 = string.Empty;
            objEntityMaster.EmailId1 = string.Empty;
            objEntityMaster.EmailId2 = string.Empty;
            objEntityMaster.BankAccountTitle = string.Empty;
            objEntityMaster.BankAccountNumber = string.Empty;
            objEntityMaster.BankName = string.Empty;
            objEntityMaster.BankIFSCCode = string.Empty;
            objEntityMaster.BankBranch = string.Empty;
            objEntityMaster.BankAccountTypeId = 0;
            objEntityMaster.CreatedDateTime = helper.getIndianTime();
            objEntityMaster.CreatedBy = 2;
            objEntityMaster.ParentEntityId = 2;
            objEntityMaster.StatusId = 1;
            objEntityMaster.AccountHandlerUserId = 4;
            objEntityMaster.AgencyLogoName = string.Empty;
            objEntityMaster.MinimumBalanceMaintain = 50;
            db.tblEntity_Master.Add(objEntityMaster);
            db.SaveChanges();
            return Ok(objEntityMaster);
        }


        [Authorize]
        [HttpPost]
        [Route("api/entityMaster/validateentitydata")]
        public IHttpActionResult validateEntityMasterDataBeforeInsert(tblEntity_Master objEntityMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                var entityInBb = db.tblEntity_Master.SingleOrDefault(e => e.AgencyName.Trim().ToLower() == objEntityMaster.AgencyName.Trim().ToLower() &&
                e.EntityTypeId == objEntityMaster.EntityTypeId &&
                e.CompanyId == CompanyId &&
                e.Domain == objEntityMaster.Domain);

                if (entityInBb != null)
                    return BadRequest("Agency Name already exits!");
                else
                    return Ok();
            }
            else return Unauthorized();
        }


        // DELETE: api/entityMaster/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Master))]
        public IHttpActionResult deleteEntityMaster(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblEntity_Master objEntityMaster = db.tblEntity_Master.Find(id);
                if (objEntityMaster == null)
                {
                    return NotFound();
                }

                db.tblEntity_Master.Remove(objEntityMaster);
                db.SaveChanges();

                return Ok(objEntityMaster);
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesEntityMasterExists(int id)
        {
            return db.tblEntity_Master.Count(e => e.EntityId == id) > 0;
        }

        #region Bank Account Verfication by Company/Parent Entity

        [Authorize]
        [ResponseType(typeof(entityMasterViewModel))]
        [Route("api/pendingAccountVerificationListMD")]
        [HttpGet]
        public IHttpActionResult pendingAccountVerificationListMD(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    entityMasterViewModel objEntityMasterViewModel = new entityMasterViewModel();
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    int entityTypeId = Convert.ToInt32(helper.entityTypes.masterDistributor);
                    objEntityMasterViewModel.objEntityMasterList = db.QryEntityMaster.Where(x => x.EntityTypeId == entityTypeId && x.CompanyId == companyId && (x.IsBankVerified == false || x.IsBankVerified == null)).ToList();
                    objEntityMasterViewModel.totalRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.Where(s => s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Website.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.OfficeAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.OfficeAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Landmark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CountryName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.PersonName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                    }
                    else
                        objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();
                    else
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();

                    return Ok(objEntityMasterViewModel);
                }
                else
                {
                    return BadRequest("Invalid Module access.");
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entityMasterViewModel))]
        [Route("api/pendingAccountVerificationList")]
        [HttpGet]
        public IHttpActionResult pendingAccountVerificationList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityMasterViewModel objEntityMasterViewModel = new entityMasterViewModel();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityMasterViewModel.objEntityMasterList = db.QryEntityMaster.Where(x => x.ParentEntityId == entityId && (x.IsBankVerified == false || x.IsBankVerified == null)).ToList();
                    objEntityMasterViewModel.totalRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.Where(s => s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Website.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.OfficeAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.OfficeAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Landmark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CountryName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.BankBranch.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.PersonName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.objEntityMasterList.Count();
                    }
                    else
                        objEntityMasterViewModel.filterRecords = objEntityMasterViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();
                    else
                        objEntityMasterViewModel.objEntityMasterList = objEntityMasterViewModel.objEntityMasterList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryEntityMaster>();

                    return Ok(objEntityMasterViewModel);
                }
                else
                {
                    return BadRequest("Invalid module access");
                }
            }
            else
                return Unauthorized();
        }
        #endregion

        #region Verify Bank Account
        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/verifyBankAccountApprove")]
        [HttpGet]
        public IHttpActionResult verifyBankAccountApprove(long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    var entityInDb = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == entityId);
                    if (entityInDb == null)
                        return BadRequest("Oops something went wrong. Please try again later.");

                    entityInDb.IsBankVerified = true;
                    entityInDb.BankVerifiedDateTime = helper.getIndianTime();
                    db.Entry(entityInDb).State = EntityState.Modified;
                    db.Entry(entityInDb).Property(x => x.CompanyId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EntityTypeId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AgencyName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Website).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Domain).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.OfficeAddress1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.OfficeAddress2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Landmark).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.StateId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CityId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CountryId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.PinCode).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Telephone1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Telephone2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EmailId1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EmailId2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountTitle).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountNumber).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankIFSCCode).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankBranch).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountTypeId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.StatusId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AccountHandlerUserId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AgencyLogoName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.ParentEntityId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.MinimumBalanceMaintain).IsModified = false;
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid module access");
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/verifyBankAccountApproveMD")]
        [HttpGet]
        public IHttpActionResult verifyBankAccountApproveMD(long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    Int32 adminLoginId = Convert.ToInt32(identityAdminLoginId.Value);
                    var entityInDb = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == entityId);
                    if (entityInDb == null)
                        return BadRequest("Oops something went wrong. Please try again later.");

                    entityInDb.IsBankVerified = true;
                    entityInDb.BankVerifiedDateTime = helper.getIndianTime();
                    db.Entry(entityInDb).State = EntityState.Modified;
                    db.Entry(entityInDb).Property(x => x.CompanyId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EntityTypeId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AgencyName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Website).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Domain).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.OfficeAddress1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.OfficeAddress2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Landmark).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.StateId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CityId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CountryId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.PinCode).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Telephone1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.Telephone2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EmailId1).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.EmailId2).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountTitle).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountNumber).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankIFSCCode).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankBranch).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.BankAccountTypeId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.StatusId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AccountHandlerUserId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.AgencyLogoName).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.ParentEntityId).IsModified = false;
                    db.Entry(entityInDb).Property(x => x.MinimumBalanceMaintain).IsModified = false;
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Invalid module access");
                }
            }
            else
                return Unauthorized();
        }
        #endregion

        [Authorize]
        [Route("api/entityMaster/getDashboardDetails")]
        [HttpGet]
        public IHttpActionResult getDashboardDetails()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                if (!objUserIdentity.isCompany)
                {
                    int transactionTypeBankCharges = Convert.ToInt32(helper.transactionTypes.BankCharges);
                    int transactionTypeOtherCharges = Convert.ToInt32(helper.transactionTypes.OtherCharges);
                    int transactionTypeGST = Convert.ToInt32(helper.transactionTypes.GST);
                    entityDashboardVM objEntityDashboardVM = new entityDashboardVM();
                    var entityWalletInDb = db.tblEntity_Wallet.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
                    objEntityDashboardVM.WalletBalance = (entityWalletInDb == null ? 0 : (entityWalletInDb.WalletBalance < 0 ? 0 : entityWalletInDb.WalletBalance));
                    objEntityDashboardVM.objEntityAccountList = db.QryEntityAccounts.Where(x => x.EntityId == objUserIdentity.entityId &&
                      x.TransactionTypeId != transactionTypeBankCharges &&
                    x.TransactionTypeId != transactionTypeOtherCharges &&
                    x.TransactionTypeId != transactionTypeGST).OrderByDescending(x => x.CreatedDateTime).Take(10).ToList();
                    return Ok(objEntityDashboardVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}