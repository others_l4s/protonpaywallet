﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class CompanyTypeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/CompanyType
        [Authorize]
        [HttpGet]
        public List<tblCompany_Type> GettblCompany_Type()
        {
            List<tblCompany_Type> objCompanyType = new List<tblCompany_Type>();
            objCompanyType = db.tblCompany_Type.ToList();
            return objCompanyType;
        }

        [Authorize]
        [ResponseType(typeof(companyTypeViewModel))]
        [HttpGet]
        public IHttpActionResult getCompanyTypeByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    companyTypeViewModel objCompanyTypeViewModel = new companyTypeViewModel();
                    objCompanyTypeViewModel.totalRecords = db.tblCompany_Type.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCompanyTypeViewModel.objComapanyTypeMasterList = db.QryCompanyType.Where(s => s.CompanyTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objCompanyTypeViewModel.filterRecords = objCompanyTypeViewModel.objComapanyTypeMasterList.Count();
                    }
                    else
                        objCompanyTypeViewModel.filterRecords = objCompanyTypeViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objCompanyTypeViewModel.objComapanyTypeMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompanyType.OrderBy(s => s.CompanyTypeName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyType>() :
                            objCompanyTypeViewModel.objComapanyTypeMasterList.OrderBy(s => s.CompanyTypeName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyType>();
                    else
                        objCompanyTypeViewModel.objComapanyTypeMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCompanyType.OrderByDescending(s => s.CompanyTypeName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyType>() :
                            objCompanyTypeViewModel.objComapanyTypeMasterList.OrderByDescending(s => s.CompanyTypeName).Skip(pageIndex).Take(pageSize).ToList<QryCompanyType>();

                    return Ok(objCompanyTypeViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/CompanyType/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Type))]
        public IHttpActionResult getCompanyTypeListById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Type tblCompany_Type = db.tblCompany_Type.Find(id);
                    if (tblCompany_Type == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblCompany_Type);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/CompanyType/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateCompanyType(int id, tblCompany_Type objCompanyType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objCompanyType.CompanyTypeId)
                    {
                        return BadRequest();
                    }

                    try
                    {
                        var companyTypeInBb = db.tblCompany_Type.SingleOrDefault(s => s.CompanyTypeName.Trim().ToLower() == objCompanyType.CompanyTypeName.Trim().ToLower() && s.CompanyTypeId != objCompanyType.CompanyTypeId);
                        if (companyTypeInBb != null)
                            return BadRequest("Company Type Name already exits!");

                        db.Entry(objCompanyType).State = EntityState.Modified;
                        db.Entry(objCompanyType).Property(x => x.CreatedBy).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblCompany_TypeExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/CompanyType
        [Authorize]
        [ResponseType(typeof(tblCompany_Type))]
        [HttpPost]
        public IHttpActionResult saveCompanyType(tblCompany_Type objCompanyType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLogin = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var CompanyTypeInDb = db.tblCompany_Type.SingleOrDefault(c => c.CompanyTypeName == objCompanyType.CompanyTypeName && c.CompanyTypeId != objCompanyType.CompanyTypeId);
                    if (CompanyTypeInDb != null)
                        return BadRequest("Company Type Name already exits!");

                    objCompanyType.CreatedBy = Convert.ToInt16(identityAdminLogin.Value);
                    db.tblCompany_Type.Add(objCompanyType);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        // DELETE: api/CompanyType/5
        [Authorize]
        [ResponseType(typeof(tblCompany_Type))]
        [HttpDelete]
        public IHttpActionResult deleteCompanyType(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCompany_Type objCompanyType = db.tblCompany_Type.Find(id);
                    if (objCompanyType == null)
                    {
                        return NotFound();
                    }

                    db.tblCompany_Type.Remove(objCompanyType);
                    db.SaveChanges();
                    return Ok(objCompanyType);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblCompany_TypeExists(int id)
        {
            return db.tblCompany_Type.Count(e => e.CompanyTypeId == id) > 0;
        }
    }
}