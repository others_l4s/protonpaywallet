﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.viewModel;
using application.webAPI.bankit.dmt.request;
using application.webAPI.bankit.dmt.response;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Claims;
using application.webAPI.Models;
using System.Globalization;
using System.IO.IsolatedStorage;
using application.webAPI.instantpay.dmt.response;
using Microsoft.Ajax.Utilities;
using System.Data.Entity.Core;
using System.Data.Entity;
using System.Data.SqlClient;

namespace application.webAPI.Controllers
{
    public class bankITDMTController : ApiController
    {

        private applicationDbEntities db = new applicationDbEntities();
        // GET: bankITDMT

        #region BankIT Vendor API Call

        protected IRestResponse sendotp(generic_otp_request objotp, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "generic/otp";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objotp, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse addCustomer(customer_create_request objcustomer, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "customer/create";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objcustomer, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse fetchSenderDetail(customer_fetch_request objcustomer, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "customer/fetch";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objcustomer, clsCredentialsVM, onLiveMode);
            customer_fetch_response objresponse = JsonConvert.DeserializeObject<customer_fetch_response>(response.Content);
            return response;
        }

        protected IRestResponse fetchAllRecipients(recipient_all_request objcustomer, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "recipient/fetchAll";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objcustomer, clsCredentialsVM, onLiveMode);
            recipient_all_response objresponse = JsonConvert.DeserializeObject<recipient_all_response>(response.Content);
            return response;
        }

        protected IRestResponse addBeneficiary(recipient_add_request objBeneficiary, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "recipient/add";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objBeneficiary, clsCredentialsVM, onLiveMode);
            return response;
        }
        protected IRestResponse deleteBeneficiary(recipient_delete_request objBeneficiary, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "recipient/delete";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objBeneficiary, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse getBankList(BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "generic/bankList";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, null, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse checkBalance(BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "generic/balance";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.GET, null, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse getSingleBeneficiaryData(recipient_fetch_request clsBeneficiaryRequest, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "recipient/fetch";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, clsBeneficiaryRequest, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse MoneyTransferViaNEFT(MoneyTransferRequest objMoneyTransferRequest, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "transact/NEFT/remit";
            objMoneyTransferRequest.agentCode = "1";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objMoneyTransferRequest, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse MoneyTransferViaIMPS(MoneyTransferRequest objMoneyTransferRequest, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "transact/IMPS/remit";
            objMoneyTransferRequest.agentCode = "1";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objMoneyTransferRequest, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse VerifyBeneficiaryAccount(IMPS_accountverify_request objAccountVerifyRequest, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "transact/IMPS/accountverification";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objAccountVerifyRequest, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected IRestResponse searchTransaction(searchtxn_request objSearchTxnRequest, BankITDMTCredentialsVM clsCredentialsVM, Boolean onLiveMode)
        {
            IRestResponse response;
            string URL = "transact/searchtxn";
            response = helper.WebAPIClient_via_RestSharp_4_BankIT_DMT(URL, Method.POST, objSearchTxnRequest, clsCredentialsVM, onLiveMode);
            return response;
        }

        protected string checkDuplicateClientRefIDInDb(string clientRefId)
        {
            string newClientRefId = "";
            var clientRefIdInDb = db.tblBankit_Transaction.SingleOrDefault(w => w.ClientRefId.Trim() == clientRefId);
            if (clientRefIdInDb != null)
                newClientRefId = helper.bankITDMT_ClientRefId();
            else
                newClientRefId = clientRefId;

            return newClientRefId;
        }

        #endregion

        #region BankIT Process in Database

        [Authorize]
        [ResponseType(typeof(BankITSenderViewModel))]
        [HttpGet]
        public IHttpActionResult getBankITSenderByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    BankITSenderViewModel objBankItSenderVM = new BankITSenderViewModel();
                    objBankItSenderVM.totalRecords = db.tblBankit_Sender.Where(b => b.EntityId == entityId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objBankItSenderVM.clsBankItSenderList = db.tblBankit_Sender.Where(s => s.EntityId == entityId && (s.Name.Trim().ToLower() == searchValue.Trim().ToLower()
                        || s.Address.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.CustomerID.Trim().ToLower() == searchValue.Trim().ToLower())).ToList();

                        objBankItSenderVM.filterRecords = objBankItSenderVM.clsBankItSenderList.Count();
                    }
                    else
                        objBankItSenderVM.filterRecords = objBankItSenderVM.totalRecords;

                    if (sortDirection == "asc")
                        objBankItSenderVM.clsBankItSenderList = String.IsNullOrEmpty(searchValue) ?
                            db.tblBankit_Sender.Where(s => s.EntityId == entityId).OrderBy(s => s.CreatedDateTime).ThenBy(s => s.Name).Skip(pageIndex).Take(pageSize).ToList<tblBankit_Sender>() :
                            objBankItSenderVM.clsBankItSenderList.OrderBy(s => s.CreatedDateTime).ThenBy(s => s.Name).Skip(pageIndex).Take(pageSize).ToList<tblBankit_Sender>();
                    else
                        objBankItSenderVM.clsBankItSenderList = String.IsNullOrEmpty(searchValue) ?
                            db.tblBankit_Sender.Where(s => s.EntityId == entityId).OrderByDescending(s => s.CreatedDateTime).ThenBy(s => s.Name).Skip(pageIndex).Take(pageSize).ToList<tblBankit_Sender>() :
                            objBankItSenderVM.clsBankItSenderList.OrderByDescending(s => s.CreatedDateTime).ThenBy(s => s.Name).Skip(pageIndex).Take(pageSize).ToList<tblBankit_Sender>();

                    return Ok(objBankItSenderVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(tblBankit_Sender))]
        [HttpPost]
        public IHttpActionResult saveSenderDetails(tblBankit_Sender clsBankItSender)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    customer_create_request clsCreateCustomerRequest = new customer_create_request();
                    clsCreateCustomerRequest.name = clsBankItSender.Name;
                    clsCreateCustomerRequest.customerId = clsBankItSender.CustomerID;
                    clsCreateCustomerRequest.address = clsBankItSender.Address;
                    clsCreateCustomerRequest.dateOfBirth = (clsBankItSender.DateOfBirth.Value != null ? clsBankItSender.DateOfBirth.Value.ToString("yyyy-MM-dd") : "");
                    clsCreateCustomerRequest.otp = clsBankItSender.OTP;
                    IRestResponse createSenderResponse = addCustomer(clsCreateCustomerRequest, clsDMTCredentialsVM, true);
                    if (createSenderResponse.StatusCode == HttpStatusCode.OK)
                    {
                        customer_create_response clsCreateCustomerResponse = JsonConvert.DeserializeObject<customer_create_response>(createSenderResponse.Content);
                        if (clsCreateCustomerResponse.errorCode.Trim() == "00")
                        {
                            var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                            var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                            var senderDetailInDb = db.tblBankit_Sender.SingleOrDefault(c => c.CustomerID.Trim() == clsBankItSender.CustomerID && c.EntityId == clsBankItSender.EntityId && c.BankitSenderId != clsBankItSender.BankitSenderId);
                            if (senderDetailInDb != null)
                                return BadRequest("Sender Details already exists");

                            clsBankItSender.EntityId = Convert.ToInt64(identityEntityId.Value);
                            clsBankItSender.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                            clsBankItSender.IsOTPverified = true;
                            clsBankItSender.MobileNo = clsBankItSender.CustomerID;
                            clsBankItSender.CreatedDateTime = helper.getIndianTime();
                            db.tblBankit_Sender.Add(clsBankItSender);
                            db.SaveChanges();
                            return Ok(clsBankItSender.CustomerID);
                        }
                        else
                            return BadRequest(clsCreateCustomerResponse.errorMsg);
                    }
                    else
                        return BadRequest(createSenderResponse.ErrorMessage.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [HttpPost]
        [Route("api/bankITDMT/verifySenderMobileOTP")]
        public IHttpActionResult verifySenderMobileViaOTP(tblBankit_Sender clsBankItSender)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    generic_otp_request clsOTPRequest = new generic_otp_request();
                    clsOTPRequest.customerId = clsBankItSender.MobileNo;
                    IRestResponse optResponse = sendotp(clsOTPRequest, clsDMTCredentialsVM, true);
                    if (optResponse.StatusCode == HttpStatusCode.OK)
                    {
                        generic_otp_response clsOTPResponse = JsonConvert.DeserializeObject<generic_otp_response>(optResponse.Content);
                        if (clsOTPResponse.errorCode == "00")
                        {
                            return Ok("success");
                        }
                        else
                        {
                            if (clsOTPResponse.errorCode.Trim().ToLower() == "v0003")
                            {
                                //Checking sender data exists in database
                                var senderDataExistsInDb = db.tblBankit_Sender.SingleOrDefault(w => w.CustomerID == clsBankItSender.MobileNo);
                                if (senderDataExistsInDb == null)
                                {
                                    customer_fetch_request clsFetchCustomerRequest = new customer_fetch_request();
                                    clsFetchCustomerRequest.customerId = clsBankItSender.MobileNo;
                                    IRestResponse fetchCustomerResponse = fetchSenderDetail(clsFetchCustomerRequest, clsDMTCredentialsVM, true);
                                    if (fetchCustomerResponse.StatusCode == HttpStatusCode.OK)
                                    {
                                        customer_fetch_response clsFetchCustomerResponse = JsonConvert.DeserializeObject<customer_fetch_response>(fetchCustomerResponse.Content);
                                        if (clsFetchCustomerResponse.errorCode == "00")
                                        {
                                            var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                                            var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                                            clsBankItSender.BankitSenderId = 0;
                                            clsBankItSender.Name = clsFetchCustomerResponse.data.name;
                                            clsBankItSender.CustomerID = clsFetchCustomerResponse.data.customerId;
                                            clsBankItSender.KYCStatus = (clsFetchCustomerResponse.data.kycstatus == 0 ? false : true);
                                            clsBankItSender.DateOfBirth = Convert.ToDateTime(clsFetchCustomerResponse.data.dateOfBirth);
                                            clsBankItSender.WalletBalance = (decimal)clsFetchCustomerResponse.data.walletbal;
                                            clsBankItSender.EntityId = Convert.ToInt64(identityEntityId.Value);
                                            clsBankItSender.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                                            clsBankItSender.IsOTPverified = true;
                                            clsBankItSender.Address = "";
                                            db.tblBankit_Sender.Add(clsBankItSender);
                                            db.SaveChanges();
                                            return Ok("Saved");
                                        }
                                        else
                                            return BadRequest(clsFetchCustomerResponse.errorMsg.ToString());
                                    }
                                    else
                                        return BadRequest(fetchCustomerResponse.ErrorMessage.ToString());
                                }
                                else
                                    return Ok("Saved");
                            }
                            else
                            {
                                return BadRequest(clsOTPResponse.errorMsg);
                            }
                        }
                    }
                    else
                    {
                        return BadRequest("Unable to Verify Mobile No." + optResponse.ErrorException.Message);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [HttpPost]
        [Route("api/bankITDMT/getSenderDetail")]
        public IHttpActionResult getSenderDetail(customer_fetch_request objSenderRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    generic_otp_request clsOTPRequest = new generic_otp_request();
                    IRestResponse optResponse = fetchSenderDetail(objSenderRequest, clsDMTCredentialsVM, true);
                    if (optResponse.StatusCode == HttpStatusCode.OK)
                    {
                        customer_fetch_response clsResponse = JsonConvert.DeserializeObject<customer_fetch_response>(optResponse.Content);
                        if (clsResponse.errorCode == "00")
                        {
                            return Ok(clsResponse);
                        }
                        else
                        {
                            return BadRequest(clsResponse.errorMsg);
                        }
                    }
                    else
                    {
                        return BadRequest("Unable to fetch Sender Detail" + optResponse.ErrorException.Message);
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [HttpPost]
        [Route("api/bankITDMT/verifySenderMobileNoOrAccountNumberInDb")]
        public IHttpActionResult verifySenderMobileNoOrAccountNumberInDb(customer_fetch_request objSenderRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    Boolean detailsFound = false;
                    string senderMobileNo = objSenderRequest.customerId;
                    string senderBeneficiaryAccountNumber = objSenderRequest.accountNumber;
                    if (!String.IsNullOrEmpty(senderMobileNo))
                    {
                        var senderDetailInDb = db.tblBankit_Sender.Where(x => x.CustomerID.Trim() == senderMobileNo.Trim()).Take(1).SingleOrDefault();
                        if (senderDetailInDb != null)
                        {
                            detailsFound = true;
                        }
                    }
                    else
                    {
                        tblBankIt_Beneficiary_Account_Verification objBeneficiaryDetails = db.tblBankIt_Beneficiary_Account_Verification.Where(x => x.UDF2.Trim() == objSenderRequest.accountNumber.Trim()).Take(1).SingleOrDefault();
                        if (objBeneficiaryDetails != null)
                        {
                            detailsFound = true;
                            senderMobileNo = objBeneficiaryDetails.CustomerId;
                        }
                    }

                    if (detailsFound == false)
                        return Ok("Error:Sender Mobile No. not found");
                    else
                        return Ok(senderMobileNo);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }
        #endregion

        #region BankIT Recipient Process

        [Authorize]
        [ResponseType(typeof(BankITRecipientViewModel))]
        [HttpGet]
        [Route("api/bankITDMT/recipientFetchAll")]
        public IHttpActionResult getAllRecipients(string customerId, int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITRecipientViewModel objBankITRecipientVM = new BankITRecipientViewModel();
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    recipient_all_request clsRecipientAllRequest = new recipient_all_request();
                    clsRecipientAllRequest.customerId = customerId;
                    IRestResponse recipientFetchAllResponse = fetchAllRecipients(clsRecipientAllRequest, clsDMTCredentialsVM, true);
                    if (recipientFetchAllResponse.StatusCode == HttpStatusCode.OK)
                    {
                        recipient_all_response clsRecipientFetchAllResponse = JsonConvert.DeserializeObject<recipient_all_response>(recipientFetchAllResponse.Content);
                        if (clsRecipientFetchAllResponse.errorCode == "00")
                        {
                            objBankITRecipientVM.totalRecords = clsRecipientFetchAllResponse.data.recipientList.Count();
                            if (!String.IsNullOrEmpty(searchValue))
                            {
                                objBankITRecipientVM.clsRecipientResponseList = clsRecipientFetchAllResponse.data.recipientList.Where(s => s.recipientName.Contains(searchValue.Trim().ToLower())
                                || s.bankName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                || s.mobileNo.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                || s.udf1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                || s.udf2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                                || s.bankCode.Trim().ToLower() == searchValue.Trim().ToLower()).ToList();

                                objBankITRecipientVM.filterRecords = objBankITRecipientVM.clsRecipientResponseList.Count();
                            }
                            else
                                objBankITRecipientVM.filterRecords = objBankITRecipientVM.totalRecords;

                            if (sortDirection == "asc")
                                objBankITRecipientVM.clsRecipientResponseList = String.IsNullOrEmpty(searchValue) ?
                                    clsRecipientFetchAllResponse.data.recipientList.OrderBy(s => s.recipientName).ThenBy(s => s.bankName).Skip(pageIndex).Take(150).ToList<recipient_fetch_resp>() :
                                    objBankITRecipientVM.clsRecipientResponseList.OrderBy(s => s.recipientName).ThenBy(s => s.bankName).Skip(pageIndex).Take(pageSize).ToList<recipient_fetch_resp>();
                            else
                                objBankITRecipientVM.clsRecipientResponseList = String.IsNullOrEmpty(searchValue) ?
                                   clsRecipientFetchAllResponse.data.recipientList.OrderByDescending(s => s.recipientName).ThenBy(s => s.bankName).Skip(pageIndex).Take(150).ToList<recipient_fetch_resp>() :
                                   objBankITRecipientVM.clsRecipientResponseList.OrderByDescending(s => s.recipientName).ThenBy(s => s.bankName).Skip(pageIndex).Take(pageSize).ToList<recipient_fetch_resp>();

                            return Ok(objBankITRecipientVM);
                        }
                        else
                        {
                            return BadRequest(clsRecipientFetchAllResponse.errorMsg);
                        }
                    }
                    else
                    {
                        return BadRequest(recipientFetchAllResponse.ErrorMessage.ToString());
                    }

                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(BankITRecipientViewModel))]
        [HttpPost]
        [Route("api/bankITDMT/saveBeneficiaryDetails")]
        public IHttpActionResult saveBeneficiaryDetails(recipient_fetch_resp clsBeneficiary)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    //Verifying Beneficiary Account from saved records in database.
                    string beneficiaryAccountStatus = "";
                    string customerId = "", clientRefId = "", IFSCCode = "", AccountNo = "", errorCode = "", errorMsg = "", txnId = "", txnStatus = "", reason = "", beneficiaryName = "";
                    Boolean isAccountVerified = clsBeneficiary.verifyAccount;
                    customerId = clsBeneficiary.customerId;
                    clientRefId = helper.bankITDMT_ClientRefId();
                    IFSCCode = clsBeneficiary.udf1;
                    AccountNo = clsBeneficiary.udf2;
                    beneficiaryName = clsBeneficiary.recipientName;
                    var beneficiaryAccountInDb = db.tblBankIt_Beneficiary_Account_Verification.Where(c => c.UDF1 == clsBeneficiary.udf1 && c.UDF2 == clsBeneficiary.udf2).OrderByDescending(c => c.CreatedDateTime).FirstOrDefault();
                    if (beneficiaryAccountInDb != null)
                    {
                        beneficiaryName = beneficiaryAccountInDb.Name;
                        isAccountVerified = (Boolean)beneficiaryAccountInDb.IsAccountVerified;
                        AccountNo = beneficiaryAccountInDb.UDF2;
                        IFSCCode = beneficiaryAccountInDb.UDF1;
                        txnId = beneficiaryAccountInDb.TxnId;
                        txnStatus = beneficiaryAccountInDb.TxnStatus;
                        errorCode = beneficiaryAccountInDb.errorCode;
                        errorMsg = beneficiaryAccountInDb.errorMsg;
                        reason = beneficiaryAccountInDb.Reason;
                    }
                    if (clsBeneficiary.verifyAccount)
                    {
                        if (helper.hasEnoughBalance(entityId))
                        {
                            string walletUpdateStatus = helper.updateWalletBalance(entityId, -clsBeneficiary.IMPSAdditionCharge);
                            if (walletUpdateStatus == "success")
                            {
                                string transactionStatus = helper.transactEntityAccount(entityId, -clsBeneficiary.IMPSAdditionCharge, "", "Add Beneficiary via IMPS", Convert.ToInt32(helper.transactionTypes.OtherCharges), false, 0, entityUserId, clsBeneficiary.IMPSAdditionCharge);
                                if (transactionStatus == "success")
                                {
                                    IMPS_accountverify_request objBeneficiaryAaccountReq = new IMPS_accountverify_request();
                                    objBeneficiaryAaccountReq.amount = "1";
                                    objBeneficiaryAaccountReq.clientRefId = clientRefId;
                                    objBeneficiaryAaccountReq.customerId = customerId;
                                    objBeneficiaryAaccountReq.udf1 = AccountNo;
                                    objBeneficiaryAaccountReq.udf2 = IFSCCode;
                                    IRestResponse objBeneficiaryVerifyResponse = VerifyBeneficiaryAccount(objBeneficiaryAaccountReq, clsDMTCredentialsVM, true);
                                    if (objBeneficiaryVerifyResponse.StatusCode == HttpStatusCode.OK)
                                    {
                                        IMPS_accountverify_response objBeneficiaryData = JsonConvert.DeserializeObject<IMPS_accountverify_response>(objBeneficiaryVerifyResponse.Content);
                                        beneficiaryName = objBeneficiaryData.data.name;
                                        isAccountVerified = objBeneficiaryData.errorCode == "00" ? true : false;
                                        txnId = (String.IsNullOrEmpty(objBeneficiaryData.data.txnId) ? "" : objBeneficiaryData.data.txnId);
                                        txnStatus = (String.IsNullOrEmpty(objBeneficiaryData.data.txnStatus) ? "" : objBeneficiaryData.data.txnStatus);
                                        errorCode = objBeneficiaryData.errorCode;
                                        errorMsg = objBeneficiaryData.errorMsg;
                                        reason = objBeneficiaryData.Reason;
                                    }
                                }
                                else
                                {
                                    return BadRequest(transactionStatus);
                                }
                            }
                            else
                            {
                                return BadRequest(walletUpdateStatus);
                            }
                        }
                        else
                        {
                            return BadRequest(helper.entityLowBalance);
                        }
                    }
                    //Saving Data into bank
                    tblBankIt_Beneficiary_Account_Verification clsBeneficiaryAccount = new tblBankIt_Beneficiary_Account_Verification();
                    clsBeneficiaryAccount.CustomerId = clsBeneficiary.customerId;
                    clsBeneficiaryAccount.Amount = 1;
                    clsBeneficiaryAccount.ClientRefId = clientRefId;
                    clsBeneficiaryAccount.UDF1 = IFSCCode;
                    clsBeneficiaryAccount.UDF2 = AccountNo;
                    clsBeneficiaryAccount.errorCode = errorCode;
                    clsBeneficiaryAccount.errorMsg = errorMsg;
                    clsBeneficiaryAccount.Name = beneficiaryName;
                    clsBeneficiaryAccount.TxnId = txnId;
                    clsBeneficiaryAccount.TxnStatus = txnStatus;
                    clsBeneficiaryAccount.CreatedDateTime = helper.getIndianTime();
                    clsBeneficiaryAccount.EntityId = Convert.ToInt64(identityEntityId.Value);
                    clsBeneficiaryAccount.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    clsBeneficiaryAccount.Reason = reason;
                    clsBeneficiaryAccount.IsAccountVerified = isAccountVerified;
                    db.tblBankIt_Beneficiary_Account_Verification.Add(clsBeneficiaryAccount);
                    db.SaveChanges();
                    beneficiaryAccountStatus = errorCode;
                    if (errorCode != "00")
                        beneficiaryAccountStatus = errorCode + ", " + reason;
                    if (beneficiaryAccountStatus == "00" && isAccountVerified)
                    {
                        recipient_add_request objBeneficiary = new recipient_add_request();
                        objBeneficiary.bankName = clsBeneficiary.bankName;
                        objBeneficiary.customerId = customerId;
                        objBeneficiary.accountNo = AccountNo;
                        objBeneficiary.ifsc = IFSCCode;
                        objBeneficiary.mobileNo = clsBeneficiary.mobileNo;
                        objBeneficiary.recipientName = beneficiaryName;
                        IRestResponse createBeneficiaryResponse = addBeneficiary(objBeneficiary, clsDMTCredentialsVM, true);
                        if (createBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                        {
                            recipient_fetch_response clsBeneficiaryResponse = JsonConvert.DeserializeObject<recipient_fetch_response>(createBeneficiaryResponse.Content);
                            if (clsBeneficiaryResponse.errorCode.Trim() == "00")
                            {
                                return Ok();
                            }
                            else
                                return BadRequest(clsBeneficiaryResponse.errorMsg);
                        }
                        else
                            return BadRequest(createBeneficiaryResponse.ErrorMessage.ToString());
                    }
                    else if (isAccountVerified == false)
                    {
                        recipient_add_request objBeneficiary = new recipient_add_request();
                        objBeneficiary.bankName = clsBeneficiary.bankName;
                        objBeneficiary.customerId = customerId;
                        objBeneficiary.accountNo = AccountNo;
                        objBeneficiary.ifsc = IFSCCode;
                        objBeneficiary.mobileNo = clsBeneficiary.mobileNo;
                        objBeneficiary.recipientName = beneficiaryName;
                        IRestResponse createBeneficiaryResponse = addBeneficiary(objBeneficiary, clsDMTCredentialsVM, true);
                        if (createBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                        {
                            recipient_fetch_response clsBeneficiaryResponse = JsonConvert.DeserializeObject<recipient_fetch_response>(createBeneficiaryResponse.Content);
                            if (clsBeneficiaryResponse.errorCode.Trim() == "00")
                            {
                                return Ok();
                            }
                            else
                                return BadRequest(clsBeneficiaryResponse.errorMsg);
                        }
                        else
                            return BadRequest(createBeneficiaryResponse.ErrorMessage.ToString());
                    }
                    else
                        return BadRequest(beneficiaryAccountStatus);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(BankITRecipientViewModel))]
        [HttpPost]
        [Route("api/bankITDMT/deleteBeneficiaryDetails")]
        public IHttpActionResult deleteBeneficiaryDetails(recipient_fetch_resp clsBeneficiary)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    //Verifying Beneficiary Account from saved records in database.
                    string beneficiaryAccountStatus = "";
                    var beneficiaryAccountInDb = db.tblBankIt_Beneficiary_Account_Verification.Where(c => c.CustomerId == clsBeneficiary.customerId && c.UDF2 == clsBeneficiary.udf2).OrderByDescending(c => c.CreatedDateTime).FirstOrDefault();
                    if (beneficiaryAccountInDb != null)
                    {
                        beneficiaryAccountStatus = "Beneficiary Verification Status : " + beneficiaryAccountInDb.errorCode + ", Reason: " + beneficiaryAccountInDb.Reason;
                    }
                    else
                    {
                        // Verifying Beneficiary details at BankIT
                        if (clsBeneficiary.transferType.Trim().ToUpper() == "NEFT")
                        {
                            beneficiaryAccountStatus = "00";
                        }
                        else
                        {
                            if (helper.hasEnoughBalance(entityId))
                            {
                                string walletUpdateStatus = helper.updateWalletBalance(entityId, -clsBeneficiary.IMPSAdditionCharge);
                                if (walletUpdateStatus == "success")
                                {
                                    string transactionStatus = helper.transactEntityAccount(entityId, -clsBeneficiary.IMPSAdditionCharge, "", "Add Beneficiary via IMPS", Convert.ToInt32(helper.transactionTypes.OtherCharges), false, 0, entityUserId, clsBeneficiary.IMPSAdditionCharge);
                                    if (transactionStatus == "success")
                                    {
                                        IMPS_accountverify_request objBeneficiaryAaccountReq = new IMPS_accountverify_request();
                                        objBeneficiaryAaccountReq.amount = "1";
                                        objBeneficiaryAaccountReq.clientRefId = helper.bankITDMT_ClientRefId();
                                        objBeneficiaryAaccountReq.customerId = clsBeneficiary.customerId;
                                        objBeneficiaryAaccountReq.udf1 = clsBeneficiary.udf2;
                                        objBeneficiaryAaccountReq.udf2 = clsBeneficiary.udf1;
                                        IRestResponse objBeneficiaryVerifyResponse = VerifyBeneficiaryAccount(objBeneficiaryAaccountReq, clsDMTCredentialsVM, true);
                                        if (objBeneficiaryVerifyResponse.StatusCode == HttpStatusCode.OK)
                                        {
                                            IMPS_accountverify_response objBeneficiaryData = JsonConvert.DeserializeObject<IMPS_accountverify_response>(objBeneficiaryVerifyResponse.Content);
                                            //Saving Data into bank
                                            tblBankIt_Beneficiary_Account_Verification clsBeneficiaryAccount = new tblBankIt_Beneficiary_Account_Verification();
                                            clsBeneficiaryAccount.CustomerId = clsBeneficiary.customerId;
                                            clsBeneficiaryAccount.Amount = 1;
                                            clsBeneficiaryAccount.ClientRefId = objBeneficiaryData.data.clientRefId;
                                            clsBeneficiaryAccount.UDF1 = clsBeneficiary.udf1;
                                            clsBeneficiaryAccount.UDF2 = clsBeneficiary.udf2;
                                            clsBeneficiaryAccount.errorCode = objBeneficiaryData.errorCode;
                                            clsBeneficiaryAccount.errorMsg = objBeneficiaryData.errorMsg;
                                            clsBeneficiaryAccount.Name = objBeneficiaryData.data.name;
                                            clsBeneficiary.recipientName = objBeneficiaryData.data.name;
                                            clsBeneficiaryAccount.TxnId = (String.IsNullOrEmpty(objBeneficiaryData.data.txnId) ? "" : objBeneficiaryData.data.txnId);
                                            clsBeneficiaryAccount.TxnStatus = (String.IsNullOrEmpty(objBeneficiaryData.data.txnStatus) ? "" : objBeneficiaryData.data.txnStatus);
                                            clsBeneficiaryAccount.CreatedDateTime = helper.getIndianTime();
                                            clsBeneficiaryAccount.EntityId = Convert.ToInt64(identityEntityId.Value);
                                            clsBeneficiaryAccount.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                                            clsBeneficiaryAccount.Reason = objBeneficiaryData.Reason;
                                            db.tblBankIt_Beneficiary_Account_Verification.Add(clsBeneficiaryAccount);
                                            db.SaveChanges();
                                            beneficiaryAccountStatus = objBeneficiaryData.errorCode;
                                        }
                                    }
                                    else
                                    {
                                        return BadRequest(transactionStatus);
                                    }
                                }
                                else
                                {
                                    return BadRequest(walletUpdateStatus);
                                }
                            }
                            else
                            {
                                return BadRequest(helper.entityLowBalance);
                            }
                        }
                    }
                    if (beneficiaryAccountStatus == "00")
                    {
                        recipient_add_request objBeneficiary = new recipient_add_request();
                        objBeneficiary.bankName = clsBeneficiary.bankName;
                        objBeneficiary.customerId = clsBeneficiary.customerId;
                        objBeneficiary.accountNo = clsBeneficiary.udf2;
                        objBeneficiary.ifsc = clsBeneficiary.udf1;
                        objBeneficiary.mobileNo = clsBeneficiary.mobileNo;
                        objBeneficiary.recipientName = clsBeneficiary.recipientName;
                        IRestResponse createBeneficiaryResponse = addBeneficiary(objBeneficiary, clsDMTCredentialsVM, true);
                        if (createBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                        {
                            recipient_fetch_response clsBeneficiaryResponse = JsonConvert.DeserializeObject<recipient_fetch_response>(createBeneficiaryResponse.Content);
                            if (clsBeneficiaryResponse.errorCode.Trim() == "00")
                            {
                                return Ok();
                            }
                            else
                                return BadRequest(clsBeneficiaryResponse.errorMsg);
                        }
                        else
                            return BadRequest(createBeneficiaryResponse.ErrorMessage.ToString());
                    }
                    else
                    {
                        return BadRequest(beneficiaryAccountStatus);
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(BankListVM))]
        [HttpPost]
        [Route("api/bankITDMT/getBankList")]
        public IHttpActionResult BankList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    IRestResponse objBankListResponse = getBankList(clsDMTCredentialsVM, true);
                    if (objBankListResponse.StatusCode == HttpStatusCode.OK)
                    {
                        BankListVM objBankListVM = JsonConvert.DeserializeObject<BankListVM>(objBankListResponse.Content);
                        if (objBankListVM.errorCode == "00")
                            return Ok(objBankListVM);
                        else
                            return BadRequest(objBankListVM.errorMsg);
                    }
                    else
                        return BadRequest(objBankListResponse.ErrorMessage.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(BankListVM))]
        [HttpGet]
        [Route("api/bankITDMT/checkavailableBalance")]
        public IHttpActionResult checkavailableBalance()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    IRestResponse objBankBalanceResponse = checkBalance(clsDMTCredentialsVM, true);
                    if (objBankBalanceResponse.StatusCode == HttpStatusCode.OK)
                    {
                        balance_response objBalanceResponse = JsonConvert.DeserializeObject<balance_response>(objBankBalanceResponse.Content);
                        return Ok(objBalanceResponse.apibalance);
                    }
                    else
                        return BadRequest(objBankBalanceResponse.ErrorMessage.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(BankListVM))]
        [HttpPost]
        [Route("api/bankITDMT/getSingleBeneficiaryData")]
        public IHttpActionResult getSingleBeneficiaryData(recipient_fetch_request objBeneficiaryReq)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    IRestResponse objBeneficiaryResponse = getSingleBeneficiaryData(objBeneficiaryReq, clsDMTCredentialsVM, true);
                    if (objBeneficiaryResponse.StatusCode == HttpStatusCode.OK)
                    {
                        recipient_fetch_response objresponse = JsonConvert.DeserializeObject<recipient_fetch_response>(objBeneficiaryResponse.Content);
                        //Get Sender Details
                        objresponse.objSender = db.tblBankit_Sender.SingleOrDefault(s => s.CustomerID == objBeneficiaryReq.customerId);
                        if (objresponse.errorCode == "00")
                            return Ok(objresponse);
                        else
                            return BadRequest(objresponse.errorMsg);
                    }
                    else
                        return BadRequest(objBeneficiaryResponse.ErrorMessage.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        [Authorize]
        [ResponseType(typeof(BankListVM))]
        [HttpPost]
        [Route("api/bankITDMT/transacNEFT")]
        public IHttpActionResult moneyTransferViaNEFT(MoneyTransferRequest objBeneficiaryReq)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (helper.checkProductPartialStopStatus(1))
                        return BadRequest("We are upgrading our Money Transfer system. We will back LIVE soon!!!");
                    else
                    {
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                        int companyId = Convert.ToInt32(identityCompanyId.Value);

                        double remitterTransferLimit = checkRemitterTransferLimit(objBeneficiaryReq.customerId);
                        if (remitterTransferLimit >= Convert.ToDouble(objBeneficiaryReq.amount))
                        {
                            objBeneficiaryReq.agentCode = "1";
                            objBeneficiaryReq.clientRefId = checkDuplicateClientRefIDInDb(objBeneficiaryReq.clientRefId);
                            if (helper.hasEnoughBalance(entityId))
                            {
                                Int64 amountCount = Convert.ToInt64(objBeneficiaryReq.amount) / 5000;
                                double totalAmount2Transfer = Convert.ToDouble(objBeneficiaryReq.amount);
                                double amountTransfered = 0;
                                double finalPendingAmount2Transfer = 0;
                                string parentClientRefId = objBeneficiaryReq.clientRefId;
                                objBeneficiaryReq.ParentClientRefId = parentClientRefId;
                                string moneyTransferStatus = "";
                                for (int i = 0; i < amountCount; i++)
                                {
                                    objBeneficiaryReq.amount = "5000";
                                    if (i == 0)
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId;
                                    }
                                    else
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId + "" + i.ToString();
                                    }
                                    if (helper.checkWalletBalance(entityId, 5000))
                                    {
                                        string moneyTransferCommitStatus = moneyTransferViaNEFTCommit(objBeneficiaryReq, entityId, entityUserId, companyId);
                                        moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                    }
                                    else
                                    {
                                        return BadRequest("Oops! Wallet Balance is too Low");
                                    }
                                    amountTransfered += 5000;
                                }
                                if (amountTransfered < totalAmount2Transfer)
                                {
                                    finalPendingAmount2Transfer = totalAmount2Transfer - amountTransfered;
                                    objBeneficiaryReq.amount = finalPendingAmount2Transfer.ToString();
                                    if (amountCount == 0)
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId;
                                    }
                                    else
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId + "" + (amountCount).ToString();
                                    }
                                    if (helper.checkWalletBalance(entityId, (decimal)finalPendingAmount2Transfer))
                                    {
                                        string moneyTransferCommitStatus = moneyTransferViaNEFTCommit(objBeneficiaryReq, entityId, entityUserId, companyId);
                                        moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                    }
                                    else
                                        return BadRequest("Oops! Wallet Balance is too Low");
                                }
                                return Ok(moneyTransferStatus);
                            }
                            else
                            {
                                return BadRequest("Oops! Wallet Balance is too Low");
                            }
                        }
                        else { return BadRequest("Oops! Remitter Transfer Limit is Low"); }
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        protected string moneyTransferViaNEFTCommit(MoneyTransferRequest objBeneficiaryReq, long entityId, long entityUserId, Int32 companyId)
        {
            objBeneficiaryReq.agentCode = "1";
            BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
            IRestResponse objTransactionStatus = MoneyTransferViaNEFT(objBeneficiaryReq, clsDMTCredentialsVM, true);
            if (objTransactionStatus.StatusCode == HttpStatusCode.OK)
            {
                MoneyTransferResponseVM objresponse = JsonConvert.DeserializeObject<MoneyTransferResponseVM>(objTransactionStatus.Content);
                //Data entry at database side.
                tblBankit_Transaction objBankITTransaction = new tblBankit_Transaction();
                objBankITTransaction.BankItTransactionId = 0;
                objBankITTransaction.EntityId = entityId;
                objBankITTransaction.EntityUserId = entityUserId;
                objBankITTransaction.CustomerId = objBeneficiaryReq.customerId;
                objBankITTransaction.TransferType = "NEFT";
                objBankITTransaction.RecipientId = objBeneficiaryReq.recipientId;
                objBankITTransaction.RecipientName = objBeneficiaryReq.recipientName;
                objBankITTransaction.BankName = objBeneficiaryReq.bankName;
                objBankITTransaction.ClientRefId = objBeneficiaryReq.clientRefId;
                objBankITTransaction.TxnId = objresponse.data.txnId;
                objBankITTransaction.RequestApi = JsonConvert.SerializeObject(objBeneficiaryReq);
                objBankITTransaction.ResponseApi = objTransactionStatus.Content.ToString();
                objBankITTransaction.CreatedDateTime = helper.getIndianTime();
                objBankITTransaction.Status = objresponse.errorCode;
                objBankITTransaction.Amount = Convert.ToDecimal(objBeneficiaryReq.amount);
                objBankITTransaction.ParentClientRefId = objBeneficiaryReq.ParentClientRefId;
                objBankITTransaction.FromMobileDevice = objBeneficiaryReq.fromMobileDevice;
                objBankITTransaction.IPAddress = helper.getClientIPAddress();
                db.tblBankit_Transaction.Add(objBankITTransaction);
                db.SaveChanges();
                if (objresponse.errorCode == "00")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(objBeneficiaryReq.amount), entityId, entityUserId, objBeneficiaryReq.clientRefId, companyId, 1, false);
                    return "success";
                }
                else if (objresponse.errorCode == "01")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(objBeneficiaryReq.amount), entityId, entityUserId, objBeneficiaryReq.clientRefId, companyId, 1, false);
                    return "pending";
                }
                else
                {
                    return "Reason:" + objresponse.Reason + " - Error:" + objresponse.errorMsg;
                }
            }
            else
            {
                return objTransactionStatus.ErrorMessage.ToString();
            }
        }

        protected string moneyTransferViaIMPSCommit(MoneyTransferRequest objBeneficiaryReq, long entityId, long entityUserId, Int32 companyId)
        {
            objBeneficiaryReq.agentCode = "1";
            BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
            IRestResponse objTransactionStatus = MoneyTransferViaIMPS(objBeneficiaryReq, clsDMTCredentialsVM, true);
            if (objTransactionStatus.StatusCode == HttpStatusCode.OK)
            {
                MoneyTransferResponseVM objresponse = JsonConvert.DeserializeObject<MoneyTransferResponseVM>(objTransactionStatus.Content);
                //Data entry at database side.
                tblBankit_Transaction objBankITTransaction = new tblBankit_Transaction();
                objBankITTransaction.BankItTransactionId = 0;
                objBankITTransaction.EntityId = entityId;
                objBankITTransaction.EntityUserId = entityUserId;
                objBankITTransaction.CustomerId = objBeneficiaryReq.customerId;
                objBankITTransaction.TransferType = "IMPS";
                objBankITTransaction.RecipientId = objBeneficiaryReq.recipientId;
                objBankITTransaction.RecipientName = objBeneficiaryReq.recipientName;
                objBankITTransaction.BankName = objBeneficiaryReq.bankName;
                objBankITTransaction.ClientRefId = objBeneficiaryReq.clientRefId;
                objBankITTransaction.TxnId = objresponse.data.txnId;
                objBankITTransaction.RequestApi = JsonConvert.SerializeObject(objBeneficiaryReq);
                objBankITTransaction.ResponseApi = objTransactionStatus.Content.ToString();
                objBankITTransaction.CreatedDateTime = helper.getIndianTime();
                objBankITTransaction.Status = objresponse.errorCode;
                objBankITTransaction.Amount = Convert.ToDecimal(objBeneficiaryReq.amount);
                objBankITTransaction.ParentClientRefId = objBeneficiaryReq.ParentClientRefId;
                objBankITTransaction.FromMobileDevice = objBeneficiaryReq.fromMobileDevice;
                objBankITTransaction.IPAddress = helper.getClientIPAddress();
                db.tblBankit_Transaction.Add(objBankITTransaction);
                db.SaveChanges();
                if (objresponse.errorCode == "00")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(objBeneficiaryReq.amount), entityId, entityUserId, objBeneficiaryReq.clientRefId, companyId, 1, false);
                    return "success";
                }
                else if (objresponse.errorCode == "01")
                {
                    helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(objBeneficiaryReq.amount), entityId, entityUserId, objBeneficiaryReq.clientRefId, companyId, 1, false);
                    return "pending";
                }
                else
                {

                    return "Reason:" + objresponse.Reason + " - Error:" + objresponse.errorMsg;
                }
            }
            else
            {
                return objTransactionStatus.ErrorMessage.ToString();
            }
        }

        protected double checkRemitterTransferLimit(string mobileNo)
        {
            generic_otp_request clsOTPRequest = new generic_otp_request();
            customer_fetch_request objSenderRequest = new customer_fetch_request();
            objSenderRequest.customerId = mobileNo;
            BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
            IRestResponse optResponse = fetchSenderDetail(objSenderRequest, clsDMTCredentialsVM, true);
            if (optResponse.StatusCode == HttpStatusCode.OK)
            {
                customer_fetch_response clsResponse = JsonConvert.DeserializeObject<customer_fetch_response>(optResponse.Content);
                if (clsResponse.errorCode == "00")
                {
                    return clsResponse.data.walletbal;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }

        [Authorize]
        [ResponseType(typeof(BankListVM))]
        [HttpPost]
        [Route("api/bankITDMT/transacIMPS")]
        public IHttpActionResult moneyTransferViaIMPS(MoneyTransferRequest objBeneficiaryReq)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (helper.checkProductPartialStopStatus(1))
                        return BadRequest("We are upgrading our Money Transfer system. We will back LIVE soon!!!");
                    else
                    {
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                        int companyId = Convert.ToInt32(identityCompanyId.Value);
                        double remitterTransferLimit = checkRemitterTransferLimit(objBeneficiaryReq.customerId);
                        if (remitterTransferLimit >= Convert.ToDouble(objBeneficiaryReq.amount))
                        {
                            objBeneficiaryReq.clientRefId = checkDuplicateClientRefIDInDb(objBeneficiaryReq.clientRefId);
                            if (helper.hasEnoughBalance(entityId))
                            {
                                Int64 amountCount = Convert.ToInt64(objBeneficiaryReq.amount) / 5000;
                                double totalAmount2Transfer = Convert.ToDouble(objBeneficiaryReq.amount);
                                double amountTransfered = 0;
                                double finalPendingAmount2Transfer = 0;
                                string parentClientRefId = objBeneficiaryReq.clientRefId;
                                objBeneficiaryReq.ParentClientRefId = parentClientRefId;
                                string moneyTransferStatus = "";
                                for (int i = 0; i < amountCount; i++)
                                {
                                    objBeneficiaryReq.amount = "5000";
                                    if (i == 0)
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId;
                                    }
                                    else
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId + "" + i.ToString();
                                    }
                                    if (helper.checkWalletBalance(entityId, 5000))
                                    {
                                        string moneyTransferCommitStatus = moneyTransferViaIMPSCommit(objBeneficiaryReq, entityId, entityUserId, companyId);
                                        moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                    }
                                    else
                                    {
                                        return BadRequest("Oops! Wallet Balance is too Low");
                                    }
                                    amountTransfered += 5000;
                                }
                                if (amountTransfered < totalAmount2Transfer)
                                {
                                    finalPendingAmount2Transfer = totalAmount2Transfer - amountTransfered;
                                    objBeneficiaryReq.amount = finalPendingAmount2Transfer.ToString();
                                    if (amountCount == 0)
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId;
                                    }
                                    else
                                    {
                                        objBeneficiaryReq.clientRefId = parentClientRefId + "" + (amountCount).ToString();
                                    }
                                    if (helper.checkWalletBalance(entityId, (decimal)finalPendingAmount2Transfer))
                                    {
                                        string moneyTransferCommitStatus = moneyTransferViaIMPSCommit(objBeneficiaryReq, entityId, entityUserId, companyId);
                                        moneyTransferStatus = (moneyTransferStatus.Length == 0 ? moneyTransferCommitStatus : moneyTransferStatus + "|" + moneyTransferCommitStatus);
                                    }
                                    else
                                        return BadRequest("Oops! Wallet Balance is too Low");
                                }
                                return Ok(moneyTransferStatus);
                            }
                            else
                            {
                                return BadRequest("Oops! Wallet Balance is too Low");
                            }
                        }
                        else
                        {
                            return BadRequest("Oops! Remitter Transfer Limit is Low");
                        }
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(BankITDMTTransactionsVM))]
        [HttpGet]
        [Route("api/bankITDMT/transactions")]
        public IHttpActionResult transactions(string customerId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITDMTTransactionsVM objTransactions = new BankITDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.CustomerID == customerId && (transactionType == null || x.TransferType.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.Status == transactionStatus) &&
                        (x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate)).ToList();

                    objTransactions.totalRecords = objTransactions.objBankITDMTTransactionsList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.Where(s => s.CustomerID == customerId &&
                        (s.RecipientId == searchValue.Trim()
                        || s.ClientRefId == searchValue.Trim()
                        || s.RecipientName == searchValue.Trim()
                        || s.BankName == searchValue.Trim()
                        || s.TxnId.Trim() == searchValue)).ToList();

                        objTransactions.filterRecords = objTransactions.objBankITDMTTransactionsList.Count();
                    }
                    else
                        objTransactions.filterRecords = objTransactions.totalRecords;

                    if (sortDirection == "asc")
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();
                    else
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();

                    return Ok(objTransactions);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(BankITDMTTransactionsVM))]
        [HttpGet]
        [Route("api/bankITDMT/transactionsReceipt")]
        public IHttpActionResult transactionsReceipt(long BankItTransactionId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTTransactionsVM objTransactions = new BankITDMTTransactionsVM();
                BankItTransactionVM objTransactionsWithParentClientRefId = new BankItTransactionVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    string txnIds = "";
                    decimal totalAmount = 0;
                    var bankITTransactionIdInDb = db.tblBankit_Transaction.Where(x => x.BankItTransactionId == BankItTransactionId).SingleOrDefault();
                    if (bankITTransactionIdInDb != null)
                    {
                        objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.ParentClientRefId == bankITTransactionIdInDb.ParentClientRefId).ToList();
                        objTransactionsWithParentClientRefId.objBankItTransactionList = db.tblBankit_Transaction.Where(x => x.ParentClientRefId == bankITTransactionIdInDb.ParentClientRefId).ToList();
                        if (objTransactionsWithParentClientRefId.objBankItTransactionList.Count > 0)
                        {
                            foreach (tblBankit_Transaction objData in objTransactionsWithParentClientRefId.objBankItTransactionList)
                            {
                                totalAmount += objData.Amount;
                                txnIds = (txnIds.Length == 0 ? objData.TxnId + " (" + helper.BankITStatus(objData.Status) + ")" : txnIds + ", " + objData.TxnId + " (" + helper.BankITStatus(objData.Status) + ")");
                            }
                        }

                        if (txnIds.Length > 0)
                        {
                            totalAmount = totalAmount + (totalAmount * 1 / 100);
                            objTransactions.objBankITDMTTransactionsList.Select(x => x.TxnId = txnIds).ToList();
                            objTransactions.objBankITDMTTransactionsList.Select(x => x.Amount = totalAmount).ToList();
                        }
                    }
                    return Ok(objTransactions);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(BankITRecipientViewModel))]
        [HttpGet]
        [Route("api/bankITDMT/verifyBeneficiary")]
        public IHttpActionResult verifyBeneficiaryData(IMPS_accountverify_request objBeneficiaryData)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();

                    IRestResponse objBeneficiaryVerificationProcessResponse = VerifyBeneficiaryAccount(objBeneficiaryData, clsDMTCredentialsVM, true);
                    if (objBeneficiaryVerificationProcessResponse.StatusCode == HttpStatusCode.OK)
                    {
                        IMPS_accountverify_response objresponse = JsonConvert.DeserializeObject<IMPS_accountverify_response>(objBeneficiaryVerificationProcessResponse.Content);
                        //Saving Data into bank
                        tblBankIt_Beneficiary_Account_Verification clsBeneficiaryAccount = new tblBankIt_Beneficiary_Account_Verification();
                        clsBeneficiaryAccount.CustomerId = objresponse.data.customerId;
                        clsBeneficiaryAccount.Amount = 1;
                        clsBeneficiaryAccount.ClientRefId = objresponse.data.clientRefId;
                        clsBeneficiaryAccount.UDF1 = objBeneficiaryData.udf1;
                        clsBeneficiaryAccount.UDF2 = objBeneficiaryData.udf2;
                        clsBeneficiaryAccount.errorCode = objresponse.errorCode;
                        clsBeneficiaryAccount.errorMsg = objresponse.errorMsg;
                        clsBeneficiaryAccount.Name = objresponse.data.name;
                        clsBeneficiaryAccount.TxnId = objresponse.data.txnId;
                        clsBeneficiaryAccount.TxnStatus = objresponse.data.txnStatus;
                        clsBeneficiaryAccount.CreatedDateTime = helper.getIndianTime();
                        clsBeneficiaryAccount.EntityId = Convert.ToInt64(identityEntityId.Value);
                        clsBeneficiaryAccount.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                        clsBeneficiaryAccount.Reason = objresponse.Reason;
                        db.tblBankIt_Beneficiary_Account_Verification.Add(clsBeneficiaryAccount);
                        db.SaveChanges();
                        if (objresponse.errorCode == "00")
                            return Ok(objresponse);
                        else
                            return BadRequest(objresponse.errorMsg);
                    }
                    else
                        return BadRequest(objBeneficiaryVerificationProcessResponse.ErrorMessage.ToString());
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }
        #endregion

        #region DMT 1 Transaction History

        [Authorize]
        [ResponseType(typeof(BankITDMTTransactionsVM))]
        [HttpGet]
        [Route("api/bankITDMT/transactionsHistory")]
        public IHttpActionResult transactionsHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITDMTTransactionsVM objTransactions = new BankITDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.EntityId == entityId &&
                    (transactionType == null || x.TransferType.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.Status == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                else
                {
                    objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.CompanyId == companyId &&
                    (transactionType == null || x.TransferType.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.TransferType == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITDMTTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.Where(s => s.BankItTransactionId > 0 &&
                    (s.CustomerID == searchValue.Trim()
                    || s.AgencyName == searchValue.Trim()
                    || s.ResponseApi.Contains(searchValue.Trim())
                    || s.TransferType == searchValue.Trim()
                    || s.TxnId == searchValue.Trim()
                    || s.RecipientName.Trim().Contains(searchValue.Trim())
                    || s.BankName.Trim().Contains(searchValue.Trim())
                    || s.Name.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITDMTTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();
                }
                if (objTransactions.objBankITDMTTransactionsList.Count > 0)
                {
                    foreach (QryBankIT_DMT_Transactions objData in objTransactions.objBankITDMTTransactionsList)
                    {
                        if (objData.ResponseApi.Length > 0)
                        {
                            MoneyTransferResponseVM objresponse = JsonConvert.DeserializeObject<MoneyTransferResponseVM>(objData.ResponseApi);
                            objData.accountNumber = objresponse.data.accountNumber;
                            objData.TransactionStatus = objresponse.Reason;
                        }
                    }
                }

                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(BankITDMTTransactionsVM))]
        [HttpGet]
        [Route("api/bankITDMT/retailerTransactionHistory")]
        public IHttpActionResult retailerTransactionHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType, string transactionStatus)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITDMTTransactionsVM objTransactions = new BankITDMTTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.masterDistributor))
                    {
                        var param = new SqlParameter[] {
                            new SqlParameter ("@EntityId", entityId),
                            new SqlParameter ("@TransactionType", transactionType),
                            new SqlParameter ("@TransactionStatus", transactionStatus),
                            new SqlParameter ("@FromDate", dFromDate),
                            new SqlParameter ("@ToDate", dToDate),
                            new SqlParameter ("@SearchEntityId", searchEntityId)
                        };
                        objTransactions.objBankITDMTTransactionsList = db.Database.SqlQuery<QryBankIT_DMT_Transactions>("EXEC [dbo].[DMT1_Retailer_TransactionReport_4_MD] @EntityId={0}, @TransactionType={1}, @TransactionStatus={2}, @FromDate={3}, @ToDate={4}, @SearchEntityId={5}",
                            entityId, transactionType, transactionStatus, dFromDate, dToDate, searchEntityId).ToList();
                    }
                    else
                    {
                        objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.ParentEntityId == entityId &&
                        (transactionType == null || x.TransferType.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                            (transactionStatus == null || x.Status == transactionStatus) &&
                            (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                            ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                    }
                }
                else
                {
                    objTransactions.objBankITDMTTransactionsList = db.QryBankIT_DMT_Transactions.Where(x => x.CompanyId == companyId &&
                    (transactionType == null || x.TransferType.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                        (transactionStatus == null || x.Status == transactionStatus) &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITDMTTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.Where(s => s.BankItTransactionId > 0 &&
                    (s.CustomerID == searchValue.Trim()
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim())
                    || s.ResponseApi.Contains(searchValue.Trim())
                    || s.TransferType == searchValue.Trim()
                    || s.TxnId == searchValue.Trim()
                    || s.RecipientName.Trim().Contains(searchValue.Trim())
                    || s.BankName.Trim().Contains(searchValue.Trim())
                    || s.Name.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITDMTTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITDMTTransactionsList = objTransactions.objBankITDMTTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_DMT_Transactions>();
                }
                if (objTransactions.objBankITDMTTransactionsList.Count > 0)
                {
                    foreach (QryBankIT_DMT_Transactions objData in objTransactions.objBankITDMTTransactionsList)
                    {
                        if (objData.ResponseApi.Length > 0)
                        {
                            MoneyTransferResponseVM objresponse = JsonConvert.DeserializeObject<MoneyTransferResponseVM>(objData.ResponseApi);
                            objData.accountNumber = objresponse.data.accountNumber;
                            objData.TransactionStatus = objresponse.Reason;
                        }
                    }
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion

        #region Pending Transaction Status

        [Authorize]
        [HttpPost]
        [Route("api/bankITDMT/checkNUpdatePendingTransactions")]
        public IHttpActionResult checkNUpdatePendingTransactions(string clientRefId)
        {
            string transactionStatus = "";
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblBankit_Transaction objBankITTransaction = db.tblBankit_Transaction.Where(x => x.ClientRefId == clientRefId && x.Status == "01").Take(1).SingleOrDefault();
                if (objBankITTransaction != null)
                {
                    BankITDMTCredentialsVM clsDMTCredentialsVM = helper.getBankITDMT_Credentials("BankITDMT-1.json");
                    searchtxn_request objSearchTxnRequest = new searchtxn_request();
                    objSearchTxnRequest.clientRefId = clientRefId;
                    IRestResponse objTransactionStatusResponse = searchTransaction(objSearchTxnRequest, clsDMTCredentialsVM, true);
                    if (objTransactionStatusResponse.StatusCode == HttpStatusCode.OK)
                    {
                        searchtxn_response objTxnStatusResponse = JsonConvert.DeserializeObject<searchtxn_response>(objTransactionStatusResponse.Content);
                        objBankITTransaction.UpdateResponseApi = objTransactionStatusResponse.Content.ToString();
                        transactionStatus = objTxnStatusResponse.errorMsg;
                        objBankITTransaction.Status = objTxnStatusResponse.errorCode;
                        objBankITTransaction.TxnId = objTxnStatusResponse.data.txnId;
                        if (objTxnStatusResponse.errorCode == "02")
                        {
                            helper.process_MoneyTransferRefund(objBankITTransaction.ClientRefId, (long)objBankITTransaction.EntityId, (long)objBankITTransaction.EntityUserId, 4, Convert.ToDecimal(objBankITTransaction.Amount));
                        }
                        db.Entry(objBankITTransaction).State = EntityState.Modified;
                        db.Entry(objBankITTransaction).Property(x => x.CustomerId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.TransferType).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.RecipientId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.RecipientName).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.BankName).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.ClientRefId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.RequestApi).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.ResponseApi).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.CreatedDateTime).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.EntityId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.EntityUserId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.Amount).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.ParentClientRefId).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.FromMobileDevice).IsModified = false;
                        db.Entry(objBankITTransaction).Property(x => x.IPAddress).IsModified = false;
                        db.SaveChanges();
                    }
                    return Ok(transactionStatus);
                }
                else
                    return Ok((objBankITTransaction.Status == "01" ? "Transaction Pending" : objBankITTransaction.Status == "00" ? "Transaction Success" : "Transaction Fail"));
            }
            else
                return Unauthorized();
        }

        #endregion
    }
}