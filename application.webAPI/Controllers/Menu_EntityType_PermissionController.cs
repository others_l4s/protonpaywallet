﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class Menu_EntityType_PermissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/Menu_EntityType_Permission
        [Authorize]
        public IHttpActionResult getMenuSubMenuListByEntityTypeId(int entityTypeId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                MainMenuEntityTypePermissionVM objEntityTypePermissionVM = new MainMenuEntityTypePermissionVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                objEntityTypePermissionVM.objEntityTypeList = db.tblEntity_Type.ToList();
                //For Main Menu
                objEntityTypePermissionVM.objMainMenuList = db.QryMainMenu_EntityType_Permission.Where(c => c.EntityTypeId == entityTypeId && c.SubMenuId == 0).ToList();
                List<int> mainMenuIds = objEntityTypePermissionVM.objMainMenuList.Select(c => c.MainMenuId).ToList();
                List<tblMainMenu_Master> objMainMenuList = (from a in db.tblMainMenu_Master where !mainMenuIds.Contains(a.MainMenuId) select a).ToList();
                foreach (tblMainMenu_Master objData in objMainMenuList)
                {
                    QryMainMenu_EntityType_Permission objEntityTypePermission = new QryMainMenu_EntityType_Permission();
                    objEntityTypePermission.MainMenuId = objData.MainMenuId;
                    objEntityTypePermission.MainMenuName = objData.MainMenuName;
                    objEntityTypePermission.EntityTypeId = entityTypeId;
                    objEntityTypePermission.ExternalURL = objData.ExternalURL;
                    objEntityTypePermission.ExternalURLTarget = objData.ExternalURLTarget;
                    objEntityTypePermission.IconCSS = objData.IconCSS;
                    objEntityTypePermission.CanView = false;
                    objEntityTypePermissionVM.objMainMenuList.Add(objEntityTypePermission);
                }
                mainMenuIds = objEntityTypePermissionVM.objMainMenuList.Select(c => c.MainMenuId).ToList();
                //For Main Menu End
                //For Sub Menu
                objEntityTypePermissionVM.objSubMenuList = db.QrySubMenu_EntityType_Permission.Where(c => c.EntityTypeId == entityTypeId && mainMenuIds.Contains(c.MainMenuId)).ToList();
                List<int> subMenuIds = objEntityTypePermissionVM.objSubMenuList.Select(c => c.SubMenuId).ToList();
                List<tblSubMenu_Master> objSubMenuList = (from a in db.tblSubMenu_Master where mainMenuIds.Contains(a.MainMenuId) && !subMenuIds.Contains(a.SubMenuId) select a).ToList();
                foreach (tblSubMenu_Master objDataSubMenu in objSubMenuList)
                {
                    QrySubMenu_EntityType_Permission objEntityTypePermission = new QrySubMenu_EntityType_Permission();
                    objEntityTypePermission.MainMenuId = objDataSubMenu.MainMenuId;
                    objEntityTypePermission.SubMenuName = objDataSubMenu.SubMenuName;
                    objEntityTypePermission.SubMenuId = objDataSubMenu.SubMenuId;
                    objEntityTypePermission.EntityTypeId = entityTypeId;
                    objEntityTypePermission.CanView = false;
                    objEntityTypePermission.ExternalURL = objDataSubMenu.ExternalURL;
                    objEntityTypePermission.ExternalURLTarget = objDataSubMenu.ExternalURLTarget;
                    objEntityTypePermission.IconCSS = objDataSubMenu.IconCSS;
                    objEntityTypePermissionVM.objSubMenuList.Add(objEntityTypePermission);
                }
                //For Sub Menu End
                objEntityTypePermissionVM.objMainMenuList = objEntityTypePermissionVM.objMainMenuList.OrderBy(c => c.MainMenuName).ToList();
                objEntityTypePermissionVM.objSubMenuList = objEntityTypePermissionVM.objSubMenuList.OrderBy(c => c.SubMenuName).ToList();
                return Ok(objEntityTypePermissionVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/Menu_EntityType_Permission
        [Authorize]
        [Route("api/Menu_EntityType_Permission/horizontalNavBar")]
        public IHttpActionResult getMenuSubMenu4Front()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt32(identityEntityType.Value);
                int companyId = 0;
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    companyId = Convert.ToInt32(identityCompanyId.Value);
                }
                MainMenuEntityTypePermissionVM objEntityTypePermissionVM = new MainMenuEntityTypePermissionVM();
                horizontalNavBarViewModel objHorizontalNavBar = new horizontalNavBarViewModel();
                List<mainNavBarViewModel> objMainNavBarViewModel = new List<mainNavBarViewModel>();
                mainNavBarViewModel objMainNavBar = new mainNavBarViewModel();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany && companyId == helper.defaultCompany)
                {
                    //For Main Menu
                    List<tblMainMenu_Master> objMainMenuList = db.tblMainMenu_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.MainMenuName.Trim().ToLower() != "dashboard").OrderBy(c => c.DisplayOrder).ToList();
                    foreach (tblMainMenu_Master objMainMenu in objMainMenuList)
                    {
                        objMainNavBar = new mainNavBarViewModel();
                        objMainNavBar.CompanyId = 0;
                        objMainNavBar.MainMenuId = objMainMenu.MainMenuId;
                        objMainNavBar.MainMenuName = objMainMenu.MainMenuName;
                        objMainNavBar.ExternalURL = objMainMenu.ExternalURL;
                        objMainNavBar.ExternalURLTarget = objMainMenu.ExternalURLTarget;
                        objMainNavBar.IconCSS = objMainMenu.IconCSS;
                        List<tblSubMenu_Master> objSubMenuList = db.tblSubMenu_Master.Where(c => c.MainMenuId == objMainMenu.MainMenuId && c.SubMenuName.Trim().ToLower() != "make payment").OrderBy(c => c.DisplayOrder).ToList();
                        if (objSubMenuList.Count > 0)
                        {
                            subNavBarViewModel objSubNavBar = new subNavBarViewModel();
                            List<subNavBarViewModel> objSubNavBarList = new List<subNavBarViewModel>();
                            foreach (tblSubMenu_Master objSubMenu in objSubMenuList)
                            {
                                objSubNavBar = new subNavBarViewModel();
                                objSubNavBar.SubMenuId = objSubMenu.SubMenuId;
                                objSubNavBar.SubMenuName = objSubMenu.SubMenuName;
                                objSubNavBar.ExternalURL = objSubMenu.ExternalURL;
                                objSubNavBar.ExternalURLTarget = objSubMenu.ExternalURLTarget;
                                objSubNavBar.IconCSS = objSubMenu.IconCSS;
                                objSubNavBarList.Add(objSubNavBar);
                            }
                            objMainNavBar.objSubNavBarList = objSubNavBarList;
                        }
                        else
                            objMainNavBar.objSubNavBarList = new List<subNavBarViewModel>();

                        objMainNavBarViewModel.Add(objMainNavBar);
                    }
                    objHorizontalNavBar.objMainNavBarList = objMainNavBarViewModel;
                }
                else
                {
                    //For Main Menu
                    objEntityTypePermissionVM.objMainMenuList = db.QryMainMenu_EntityType_Permission.Where(c => c.EntityTypeId == entityTypeId && c.SubMenuId == 0).OrderBy(c => c.DisplayOrder).ToList();
                    foreach (QryMainMenu_EntityType_Permission objMainMenu in objEntityTypePermissionVM.objMainMenuList)
                    {
                        objMainNavBar = new mainNavBarViewModel();
                        objMainNavBar.CompanyId = 0;
                        objMainNavBar.MainMenuId = objMainMenu.MainMenuId;
                        objMainNavBar.MainMenuName = objMainMenu.MainMenuName;
                        objMainNavBar.ExternalURL = objMainMenu.ExternalURL;
                        objMainNavBar.ExternalURLTarget = objMainMenu.ExternalURLTarget;
                        objMainNavBar.IconCSS = objMainMenu.IconCSS;
                        objEntityTypePermissionVM.objSubMenuList = db.QrySubMenu_EntityType_Permission.Where(c => c.EntityTypeId == entityTypeId && c.MainMenuId == objMainMenu.MainMenuId).OrderBy(c => c.DisplayOrder).ToList();
                        if (objEntityTypePermissionVM.objSubMenuList.Count > 0)
                        {
                            subNavBarViewModel objSubNavBar = new subNavBarViewModel();
                            List<subNavBarViewModel> objSubNavBarList = new List<subNavBarViewModel>();
                            foreach (QrySubMenu_EntityType_Permission objSubMenu in objEntityTypePermissionVM.objSubMenuList)
                            {
                                objSubNavBar = new subNavBarViewModel();
                                objSubNavBar.SubMenuId = objSubMenu.SubMenuId;
                                objSubNavBar.SubMenuName = objSubMenu.SubMenuName;
                                objSubNavBar.ExternalURL = objSubMenu.ExternalURL;
                                objSubNavBar.ExternalURLTarget = objSubMenu.ExternalURLTarget;
                                objSubNavBar.IconCSS = objSubMenu.IconCSS;
                                objSubNavBarList.Add(objSubNavBar);
                            }
                            objMainNavBar.objSubNavBarList = objSubNavBarList;
                        }
                        else
                            objMainNavBar.objSubNavBarList = new List<subNavBarViewModel>();

                        objMainNavBarViewModel.Add(objMainNavBar);
                    }
                    objHorizontalNavBar.objMainNavBarList = objMainNavBarViewModel;
                }
                return Ok(objHorizontalNavBar);
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/Menu_EntityType_Permission
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult saveMenuEntityTypePermission(tblMenu_EntityType_Permission objEntityTypePermission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                objEntityTypePermission.CreatedDateTime = helper.getIndianTime();
                var entityTypePermissionInDb = db.tblMenu_EntityType_Permission.SingleOrDefault(p => p.EntityTypeId == objEntityTypePermission.EntityTypeId &&
                p.MainMenuId == objEntityTypePermission.MainMenuId &&
                p.SubMenuId == objEntityTypePermission.SubMenuId);

                if (entityTypePermissionInDb != null)
                {
                    entityTypePermissionInDb.CanView = objEntityTypePermission.CanView;
                    entityTypePermissionInDb.CreatedDateTime = objEntityTypePermission.CreatedDateTime;
                    entityTypePermissionInDb.EntityTypeId = objEntityTypePermission.EntityTypeId;
                    db.Entry(entityTypePermissionInDb).State = EntityState.Modified;
                    db.Entry(entityTypePermissionInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.SaveChanges();
                }
                else
                {
                    db.tblMenu_EntityType_Permission.Add(objEntityTypePermission);
                    db.SaveChanges();
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/Menu_EntityType_Permission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult deleteMenuEntityTypePermissionByEntityTypeId(long id)
        {
            db.tblMenu_EntityType_Permission.RemoveRange(db.tblMenu_EntityType_Permission.Where(c => c.EntityTypeId == id));
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMenu_EntityType_PermissionExists(long id)
        {
            return db.tblMenu_EntityType_Permission.Count(e => e.EntityMenuPermissionId == id) > 0;
        }
    }
}