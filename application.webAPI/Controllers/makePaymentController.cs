﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.paymentGateway;
using application.webAPI.viewModel;
using Newtonsoft.Json;
using RestSharp;

namespace application.webAPI.Controllers
{
    public class makePaymentController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        [Authorize]
        [Route("api/getEntityVM")]
        [HttpGet]
        public IHttpActionResult getEntityVM()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityVM objEntityVM = new entityVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                Int32 defaultCompany = Convert.ToInt32(helper.defaultCompany);
                if (entityType == helper.defaultCompany)
                {
                    objEntityVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != entityType && x.EntityTypeId != defaultCompany).ToList();
                }
                if (entityType == Convert.ToInt32(helper.entityTypes.masterDistributor))
                {
                    objEntityVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != entityType && x.EntityTypeId != defaultCompany).ToList();
                }
                if (entityType == Convert.ToInt32(helper.entityTypes.distributor))
                {
                    Int32 MDEntityType = Convert.ToInt32(helper.entityTypes.masterDistributor);
                    objEntityVM.objEntityTypeList = db.tblEntity_Type.Where(x => x.EntityTypeId != entityType && x.EntityTypeId != defaultCompany && x.EntityTypeId != MDEntityType).ToList();
                }
                objEntityVM.objEntity = new tblEntity_Master();
                return Ok(objEntityVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/makePayment
        [Authorize]
        [ResponseType(typeof(makePaymentVM))]
        [Route("api/getMakePaymentList")]
        [HttpGet]
        public IHttpActionResult getMakePaymentList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                List<tblEntity_Bank_Details> objEntityBankList = new List<tblEntity_Bank_Details>();
                tblEntity_Bank_Details objEntityBank = new tblEntity_Bank_Details();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                objMakePaymentVM.objPaymentModeList = db.tblPaymentMode_Master.ToList();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                long parentEntityId = 0;
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                if (entityType > 1)
                {
                    var parentEntityInDb = db.QryEntityMaster.SingleOrDefault(c => c.EntityId == entityId);
                    if (parentEntityInDb != null)
                    {
                        objMakePaymentVM.parentAgencyName = parentEntityInDb.ParentAgencyName;
                        parentEntityId = parentEntityInDb.ParentEntityId;
                    }
                }
                if (entityType == Convert.ToInt16(helper.entityTypes.masterDistributor))
                {
                    objMakePaymentVM.objCompanyBankList = db.tblCompany_Bank_Details.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == companyId).OrderBy(c => c.BankName).ToList();
                    objMakePaymentVM.parentAgencyName = db.QryEntityMaster.SingleOrDefault(c => c.EntityId == entityId).CompanyName;
                }
                else
                    objMakePaymentVM.objEntityBankList = db.tblEntity_Bank_Details.Where(c => c.EntityId == parentEntityId && c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.BankName).ToList();

                objMakePaymentVM.objEntityPayments = new tblEntity_Payments();
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(makePaymentVM))]
        [Route("api/getMakePaymentList2HO")]
        [HttpGet]
        public IHttpActionResult getMakePaymentList2HO()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                List<tblEntity_Bank_Details> objEntityBankList = new List<tblEntity_Bank_Details>();
                tblEntity_Bank_Details objEntityBank = new tblEntity_Bank_Details();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                objMakePaymentVM.objPaymentModeList = db.tblPaymentMode_Master.ToList();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                objMakePaymentVM.objCompanyBankList = db.tblCompany_Bank_Details.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == companyId).OrderBy(c => c.BankName).ToList();
                objMakePaymentVM.parentAgencyName = db.QryEntityMaster.SingleOrDefault(c => c.EntityId == entityId).CompanyName;
                objMakePaymentVM.objEntityPayments = new tblEntity_Payments();
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/makePayment/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Payments))]
        [Route("api/getMakePaymentById")]
        [HttpGet]
        public IHttpActionResult getMakePaymentById(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblEntity_Payments tblEntity_Payments = db.tblEntity_Payments.Find(id);
                if (tblEntity_Payments == null)
                {
                    return NotFound();
                }

                return Ok(tblEntity_Payments);
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/makePayment/5
        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/updateMakePayment")]
        [HttpPut]
        public IHttpActionResult updateMakePayment(long id, tblEntity_Payments objEntityPayments)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objEntityPayments.EntityPaymentId)
                {
                    return BadRequest();
                }

                db.Entry(objEntityPayments).State = EntityState.Modified;
                db.Entry(objEntityPayments).Property(x => x.ParentEntityId).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.EntityId).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.IsPaymentReceived).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.IsPaymentCredited2Wallet).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.IsPaymentMade2Company).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(objEntityPayments).Property(x => x.ParentEntityUserId).IsModified = false;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblEntity_PaymentsExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/makePayment
        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/saveMakePayment")]
        public IHttpActionResult saveMakePayment(tblEntity_Payments objEntityPayments)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (objEntityPayments.FromMobileDevice.Value == true)
                    {
                        if (!String.IsNullOrEmpty(objEntityPayments.PaymentSlip))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntityPayments.PaymentSlip);
                            string sDateTime = "productimage_" + System.DateTime.Now.ToString("ddMMyyyyhhmmss");
                            string paymentSlip = "PaymentSlip_" + DateTime.Now.ToString("ddMMyyyyhhss") + ".jpg";
                            objEntityPayments.PaymentSlip = paymentSlip;
                            string filePath = HttpContext.Current.Server.MapPath("~/paymentSlip/" + paymentSlip);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            objEntityPayments.PaymentSlip = (objEntityPayments.PaymentSlip == null ? "" : objEntityPayments.PaymentSlip);
                    }
                    else
                        objEntityPayments.PaymentSlip = (objEntityPayments.PaymentSlip == null ? "" : objEntityPayments.PaymentSlip);

                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long parentEntityId = db.tblEntity_Master.SingleOrDefault(c => c.EntityId == entityId).ParentEntityId;
                    objEntityPayments.CreatedDateTime = helper.getIndianTime();
                    objEntityPayments.EntityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityPayments.EntityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    objEntityPayments.ParentEntityId = parentEntityId;
                    objEntityPayments.IsPaymentCredited2Wallet = false;
                    objEntityPayments.IsPaymentReceived = false;
                    objEntityPayments.IsPaymentMade2Company = (Convert.ToInt16(identityEntityType.Value) == 4 ? true : false);
                    objEntityPayments.ChequeNo = (objEntityPayments.ChequeNo == null ? "" : objEntityPayments.ChequeNo);
                    objEntityPayments.BankBranchCode = (objEntityPayments.BankBranchCode == null ? "" : objEntityPayments.BankBranchCode);
                    objEntityPayments.BankTransactionId = (objEntityPayments.BankTransactionId == null ? "" : objEntityPayments.BankTransactionId);
                    db.tblEntity_Payments.Add(objEntityPayments);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Unable to create Wallet. Entity Id is invalid for wallet creation.");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/saveMakePayment2HO")]
        public IHttpActionResult saveMakePayment2HO(tblEntity_Payments objEntityPayments)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (objEntityPayments.FromMobileDevice.Value == true)
                    {
                        if (!String.IsNullOrEmpty(objEntityPayments.PaymentSlip))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntityPayments.PaymentSlip);
                            string sDateTime = "productimage_" + System.DateTime.Now.ToString("ddMMyyyyhhmmss");
                            string paymentSlip = "PaymentSlip_" + DateTime.Now.ToString("ddMMyyyyhhss") + ".jpg";
                            objEntityPayments.PaymentSlip = paymentSlip;
                            string filePath = HttpContext.Current.Server.MapPath("~/paymentSlip/" + paymentSlip);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            objEntityPayments.PaymentSlip = (objEntityPayments.PaymentSlip == null ? "" : objEntityPayments.PaymentSlip);
                    }
                    else
                        objEntityPayments.PaymentSlip = (objEntityPayments.PaymentSlip == null ? "" : objEntityPayments.PaymentSlip);

                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityPayments.CreatedDateTime = helper.getIndianTime();
                    objEntityPayments.EntityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityPayments.EntityUserId = entityUserId;
                    objEntityPayments.ParentEntityId = 0;
                    objEntityPayments.IsPaymentCredited2Wallet = false;
                    objEntityPayments.IsPaymentReceived = false;
                    objEntityPayments.IsPaymentMade2Company = true;
                    objEntityPayments.ChequeNo = (objEntityPayments.ChequeNo == null ? "" : objEntityPayments.ChequeNo);
                    objEntityPayments.BankBranchCode = (objEntityPayments.BankBranchCode == null ? "" : objEntityPayments.BankBranchCode);
                    objEntityPayments.BankTransactionId = (objEntityPayments.BankTransactionId == null ? "" : objEntityPayments.BankTransactionId);
                    db.tblEntity_Payments.Add(objEntityPayments);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Unable to create Wallet. Entity Id is invalid for wallet creation.");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(makePaymentVM))]
        [Route("api/getPaymentHistory")]
        [HttpGet]
        public IHttpActionResult getPaymentHistory(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objMakePaymentVM.objEntityPaymentList = db.QryEntityPayments.Where(c => c.EntityId == entityId && c.IsPaymentMade2Company == true && c.IsPaymentReceived == true).ToList();
                    objMakePaymentVM.totalRecords = objMakePaymentVM.objEntityPaymentList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMakePaymentVM.objEntityPaymentList = db.QryEntityPayments.Where(a => a.Amount.ToString().Contains(searchValue.Trim().ToLower()) ||
                        a.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.PaymentMode.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.PaymentTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.Remark.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objMakePaymentVM.filterRecords = objMakePaymentVM.objEntityPaymentList.Count();
                    }
                    else
                        objMakePaymentVM.filterRecords = objMakePaymentVM.totalRecords;
                    if (sortDirection == "asc")
                        objMakePaymentVM.objEntityPaymentList = objMakePaymentVM.objEntityPaymentList.OrderBy(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments>();
                    else
                        objMakePaymentVM.objEntityPaymentList = objMakePaymentVM.objEntityPaymentList.OrderByDescending(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments>();
                }
                else
                {
                    return BadRequest("Unable to create Wallet. Entity Id is invalid for wallet creation.");
                }
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(makePaymentVM))]
        [Route("api/getPaymentHistory2HO")]
        [HttpGet]
        public IHttpActionResult getPaymentHistory2HO(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objMakePaymentVM.objEntityPaymentListHO = db.QryEntityPayments_HO.Where(x => x.EntityId == entityId && x.IsPaymentMade2Company == true).ToList();
                    objMakePaymentVM.totalRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.Where(a => a.Amount.ToString().Contains(searchValue.Trim().ToLower()) ||
                        a.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.Remark.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.PaymentMode.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.ChequeNo.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankBranchCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objMakePaymentVM.filterRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    }
                    else
                        objMakePaymentVM.filterRecords = objMakePaymentVM.totalRecords;
                    if (sortDirection == "asc")
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderBy(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                    else
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderByDescending(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                }
                else
                {
                    return BadRequest("Unable to create Wallet. Entity Id is invalid for wallet creation.");
                }
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_PaymentsExists(long id)
        {
            return db.tblEntity_Payments.Count(e => e.EntityPaymentId == id) > 0;
        }

        #region Company MD Payment Queue

        [Authorize]
        [ResponseType(typeof(makePaymentVM))]
        [Route("api/paymentQueueHO")]
        [HttpGet]
        public IHttpActionResult paymentQueueHO(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    objMakePaymentVM.objEntityPaymentListHO = db.QryEntityPayments_HO.Where(c => c.CompanyId == companyId &&
                    c.IsPaymentMade2Company == true &&
                    c.ParentEntityUserId == 0 &&
                    c.RejectionRemark == null &&
                    c.RejectionDateTime == null &&
                    c.IsPaymentReceived == false).ToList();
                    objMakePaymentVM.totalRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.Where(a => a.Amount.ToString().Contains(searchValue.Trim().ToLower()) ||
                        a.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.Remark.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.PaymentMode.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.ChequeNo.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankBranchCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objMakePaymentVM.filterRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    }
                    else
                        objMakePaymentVM.filterRecords = objMakePaymentVM.totalRecords;
                    if (sortDirection == "asc")
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderBy(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                    else
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderByDescending(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                }
                else
                {
                    return BadRequest("Invalid Access to the Module.");
                }
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        #endregion

        [Authorize]
        [ResponseType(typeof(Boolean))]
        [HttpGet]
        [Route("api/checkWalletBalance")]
        public IHttpActionResult checkWalletBalance(decimal transactAmount, string checkBalanceFor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    if (checkBalanceFor == "DMT")
                        transactAmount = transactAmount + (transactAmount * 1 / 100);
                    Nullable<int> minimumBalance2Maintain = 0;
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var minimumBalanceMaintainInDb = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == entityId);
                    if (minimumBalanceMaintainInDb != null)
                        minimumBalance2Maintain = (minimumBalanceMaintainInDb.MinimumBalanceMaintain == null ? 0 : minimumBalanceMaintainInDb.MinimumBalanceMaintain);
                    var entityWalletInDb = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
                    decimal walletBalance = 0;
                    if (entityWalletInDb != null)
                        walletBalance = entityWalletInDb.WalletBalance;
                    if (walletBalance <= 0)
                        return Ok(false);
                    walletBalance = walletBalance - (int)minimumBalance2Maintain;
                    if (walletBalance >= transactAmount)
                    {
                        if ((walletBalance - transactAmount) <= minimumBalance2Maintain)
                            return Ok(false);
                        else
                            return Ok(true);
                    }
                    else
                    { return Ok(false); }
                }
                else
                {
                    return BadRequest("Access Denied!");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/makePaymentHistoryHO")]
        public IHttpActionResult makePaymentHistoryHO(long entityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                makePaymentVM objMakePaymentVM = new makePaymentVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    objMakePaymentVM.objEntityPaymentListHO = db.QryEntityPayments_HO.Where(c => c.CompanyId == companyId &&
                    (c.IsPaymentReceived == true || c.RejectionDateTime != null) &&
                    (c.CreatedDateTime >= dFromDate && c.CreatedDateTime <= dToDate) &&
                    (c.EntityId == entityId || entityId == 0)).ToList();
                    objMakePaymentVM.totalRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.Where(a => a.Amount.ToString().Contains(searchValue.Trim().ToLower()) ||
                        a.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.Remark.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.PaymentMode.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountTitle.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankAccountNumber.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.ChequeNo.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankTransactionId.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        a.BankBranchCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objMakePaymentVM.filterRecords = objMakePaymentVM.objEntityPaymentListHO.Count();
                    }
                    else
                        objMakePaymentVM.filterRecords = objMakePaymentVM.totalRecords;
                    if (sortDirection == "asc")
                    {
                        if (pageSize > 0)
                            objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderBy(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                    }
                    else
                    {
                        if (pageSize > 0)
                            objMakePaymentVM.objEntityPaymentListHO = objMakePaymentVM.objEntityPaymentListHO.OrderByDescending(s => s.EntityPaymentId).Skip(pageIndex).Take(pageSize).ToList<QryEntityPayments_HO>();
                    }
                }
                else
                {
                    return BadRequest("Invalid Access to the Module.");
                }
                return Ok(objMakePaymentVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost]
        [Route("api/makePaymentResponse")]
        public IHttpActionResult makePaymentResponse(paymentResponse objPaymentResponse)
        {
            paymentResponseMsg objPaymentResponseMsg = new paymentResponseMsg();
            if (!String.IsNullOrEmpty(objPaymentResponse.TransactionId))
            {
                var paymentRequestInDb = db.tblEntity_Payments.Where(x => x.PaymentTransactionId == objPaymentResponse.TransactionId).Take(1).SingleOrDefault();
                if (paymentRequestInDb != null)
                {
                    paymentRequestInDb.BankTransactionId = objPaymentResponse.Order_Id;
                    paymentRequestInDb.PaymentResponseOrderId = objPaymentResponse.Order_Id;
                    paymentRequestInDb.PaymentResponseString = objPaymentResponse.ResMessage;
                    paymentRequestInDb.IsPaymentReceived = true;
                    paymentRequestInDb.IsPaymentCredited2Wallet = true;
                    paymentRequestInDb.IsPaymentMade2Company = true;
                    paymentRequestInDb.PaymentReceivedDateTime = helper.getIndianTime();
                    paymentRequestInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    paymentRequestInDb.PaymentCredited2WalletDateTime = helper.getIndianTime();
                    paymentRequestInDb.WalletUpdateStatus = "Payment Credited to wallet";

                    db.Entry(paymentRequestInDb).State = EntityState.Modified;
                    db.Entry(paymentRequestInDb).Property(x => x.EntityId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.ParentEntityId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentModeId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.Amount).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.BankId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.BankBranchCode).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.ChequeNo).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentSlip).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.EntityUserId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.ParentEntityUserId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.RejectionRemark).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.RejectionDateTime).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.AccountingEntryStatus).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.FromMobileDevice).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentTransactionId).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.ClientCode).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentRequestURL).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentRequestMessage).IsModified = false;
                    db.Entry(paymentRequestInDb).Property(x => x.PaymentRequestString).IsModified = false;
                    db.SaveChanges();
                    helper.updateWalletBalance(paymentRequestInDb.EntityId, paymentRequestInDb.Amount);
                    helper.transactEntityAccount(paymentRequestInDb.EntityId, paymentRequestInDb.Amount, paymentRequestInDb.PaymentTransactionId, paymentRequestInDb.Remark, Convert.ToInt32(helper.transactionTypes.WalletTopUp), true, 0, 0, paymentRequestInDb.Amount);
                    objPaymentResponseMsg.returnMessage = "success";
                    objPaymentResponseMsg.code = 0;
                }
                else
                {
                    objPaymentResponseMsg.returnMessage = "Transaction Id not found in our system";
                    objPaymentResponseMsg.code = 1;
                }
            }
            else
            {
                objPaymentResponseMsg.returnMessage = "Invalid Transaction Id or Transaction Id is null";
                objPaymentResponseMsg.code = 1;
            }
            return Ok(objPaymentResponseMsg);
        }
    }
}