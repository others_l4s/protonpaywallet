﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;
using application.webAPI.bankit.aeps.request;
using application.webAPI.bankit.aeps.response;
using System.Configuration;
using RestSharp;
using Newtonsoft.Json;

namespace application.webAPI.Controllers
{
    public class BankITAEPSRequestController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/BankITAEPSRequest
        [Authorize]
        [ResponseType(typeof(tblBankIt_AEPS_Request))]
        [Route("api/getBankITAEPSRequest")]
        [HttpGet]
        public IHttpActionResult getBankITAEPSRequest(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblBankIt_AEPS_Request objAEPSRequest = new tblBankIt_AEPS_Request();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objAEPSRequest = db.tblBankIt_AEPS_Request.Where(x => x.EntityId == entityId).SingleOrDefault();
                    if (objAEPSRequest == null)
                    {
                        objAEPSRequest = new tblBankIt_AEPS_Request();
                        objAEPSRequest.EntityId = entityId;
                        objAEPSRequest.BankItAEPSId = 0;
                        objAEPSRequest.AccessAmount = Convert.ToInt32(ConfigurationManager.AppSettings["AEPSJoiningAmount"]);
                    }
                }
                else
                {
                    objAEPSRequest = db.tblBankIt_AEPS_Request.Where(x => x.BankItAEPSId == id).SingleOrDefault();
                }
                return Ok(objAEPSRequest);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(BankITAEPSRequestVM))]
        [Route("api/getBnkITAEPSRequestQueue")]
        [HttpGet]
        public IHttpActionResult getBnkITAEPSRequestQueue(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                    BankITAEPSRequestVM objBankITAEPSRequestVM = new BankITAEPSRequestVM();
                    objBankITAEPSRequestVM.objBankITAEPSRequestList = db.QryBankITAEPS_Request.Where(x => x.CompanyId == companyId && (x.AEPSApprovedOnDateTime == null || x.AEPSRejected == null)).ToList();
                    objBankITAEPSRequestVM.totalRecords = objBankITAEPSRequestVM.objBankITAEPSRequestList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objBankITAEPSRequestVM.objBankITAEPSRequestList = objBankITAEPSRequestVM.objBankITAEPSRequestList.Where(x => x.AgencyName.Trim().ToLower() == searchValue.Trim().ToLower()
                        || x.OfficeAddress1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.OfficeAddress2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.Landmark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.CityName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.Telephone1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || x.EmailId1.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objBankITAEPSRequestVM.filterRecords = objBankITAEPSRequestVM.objBankITAEPSRequestList.Count();
                    }
                    else
                        objBankITAEPSRequestVM.filterRecords = objBankITAEPSRequestVM.totalRecords;

                    if (sortDirection == "asc")
                        objBankITAEPSRequestVM.objBankITAEPSRequestList = objBankITAEPSRequestVM.objBankITAEPSRequestList.OrderBy(s => s.BankItAEPSId).Skip(pageIndex).Take(pageSize).ToList<QryBankITAEPS_Request>();
                    else
                        objBankITAEPSRequestVM.objBankITAEPSRequestList = objBankITAEPSRequestVM.objBankITAEPSRequestList.OrderByDescending(s => s.BankItAEPSId).Skip(pageIndex).Take(pageSize).ToList<QryBankITAEPS_Request>();

                    return Ok(objBankITAEPSRequestVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/BankITAEPSRequest/5
        [Authorize]
        [ResponseType(typeof(void))]
        //[Route("api/BankITAEPSResponseSave")]
        [HttpPut]
        public IHttpActionResult BankITAEPSResponseSave(long id, tblBankIt_AEPS_Request objAEPSRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objAEPSRequest.BankItAEPSId)
                    {
                        return BadRequest();
                    }

                    //var AEPSDetailsInDb = db.tblBankIt_AEPS_Request.Where(x => x.BankItAEPSId == 1).SingleOrDefault();
                    objAEPSRequest.AgentAuthId = ConfigurationManager.AppSettings["BankITMaster_AgentAuthId"];
                    objAEPSRequest.AgentAuthPassword = ConfigurationManager.AppSettings["BankITMaster_AgentAuthPwd"];
                    objAEPSRequest.APIId = ConfigurationManager.AppSettings["BankITMaster_APIId"];
                    objAEPSRequest.AEPSApprovedOnDateTime = helper.getIndianTime();
                    objAEPSRequest.AEPSRejected = false;
                    objAEPSRequest.Remark = "Successfully Approved & Activated";
                    db.Entry(objAEPSRequest).State = EntityState.Modified;
                    db.Entry(objAEPSRequest).Property(x => x.AEPSRequestedOnDateTime).IsModified = false;
                    db.Entry(objAEPSRequest).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objAEPSRequest).Property(x => x.AccessAmount).IsModified = false;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblBankIt_AEPS_RequestExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
                return Unauthorized();
        }

        // POST: api/BankITAEPSRequest
        [Authorize]
        [ResponseType(typeof(tblBankIt_AEPS_Request))]
        [Route("api/BankITAEPSRequestSave")]
        [HttpPost]
        public IHttpActionResult BankITAEPSRequestSave(tblBankIt_AEPS_Request objAEPSRequest)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    //Checking where Retailer has added KYE Details
                    var entityKYCAdded = db.tblEntity_KYC.Where(x => x.EntityId == entityId &&
                    String.IsNullOrEmpty(x.PanCardNo) == false &&
                    String.IsNullOrEmpty(x.PanCardPhoto) == false && String.IsNullOrEmpty(x.PanCardPhoto2) == false).SingleOrDefault();
                    if (entityKYCAdded == null)
                        return BadRequest("Please complete your KYC Details by adding Pan Card No. and upload Pan Card photo of both sides.");
                    if (helper.hasEnoughBalance(entityId))
                    {
                        string walletUpdateStatus = helper.updateWalletBalance(entityId, -objAEPSRequest.AccessAmount);
                        string transactStatus = helper.transactEntityAccount(entityId, -objAEPSRequest.AccessAmount, "", "AEPS Activation Amount", Convert.ToInt32(helper.transactionTypes.Booking), false, 2, entityUserId, objAEPSRequest.AccessAmount);
                        if (walletUpdateStatus == "success" && transactStatus == "success")
                        {
                            objAEPSRequest.EntityId = entityId;
                            objAEPSRequest.AEPSRequestedOnDateTime = helper.getIndianTime();
                            db.tblBankIt_AEPS_Request.Add(objAEPSRequest);
                            db.SaveChanges();
                            return Ok();
                        }
                        else
                        {
                            return BadRequest(walletUpdateStatus + " " + transactStatus);
                        }
                    }
                    else
                    {
                        return BadRequest("Oops! Wallet Balance is too Low");
                    }
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        [Authorize]
        [ResponseType(typeof(Get_Token_Response))]
        [Route("api/getAEPSLIVEToken")]
        [HttpGet]
        public IHttpActionResult getAEPSLIVEToken(getToken_Request objtokenRequest)
        {
            string URL = "generatetoken";
            var client = new RestClient(ConfigurationManager.AppSettings["BankITAEPSURL"]);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(objtokenRequest);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Get_Token_Response objTokenResponse = JsonConvert.DeserializeObject<Get_Token_Response>(response.Content);
                return Ok(objTokenResponse);
            }
            else
            {
                return BadRequest(response.Content.ToString());
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblBankIt_AEPS_RequestExists(long id)
        {
            return db.tblBankIt_AEPS_Request.Count(e => e.BankItAEPSId == id) > 0;
        }
    }
}