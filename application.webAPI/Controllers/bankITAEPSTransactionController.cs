﻿using application.webAPI.bankit.aeps.request;
using application.webAPI.bankit.aeps.response;
using application.webAPI.Models;
using application.webAPI.viewModel;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;

namespace application.webAPI.Controllers
{
    public class bankITAEPSTransactionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [ResponseType(typeof(AEPSUpdateWalletResponse))]
        [Route("api/creditWallet")]
        [HttpPost]
        public IHttpActionResult creditWallet(AEPSUpdateWalletRequest objAEPSUpdateWalletRequest)
        {
            var re = Request;
            var headers = re.Headers;
            string token = "";
            string responseAPI = "";
            AEPSUpdateWalletResponse objAEPSTransactionResponse = new AEPSUpdateWalletResponse();
            if (headers.Contains("developer_key"))
            {
                token = headers.GetValues("developer_key").First();
                if (token == "3HLA6m3xkr4oSm5P4bpR")
                {
                    responseAPI = JsonConvert.SerializeObject(objAEPSUpdateWalletRequest);
                    if (objAEPSUpdateWalletRequest.Service.Trim().ToLower() == "micro atm")
                    {
                        var entityByRetailerIdInDb = db.tblBankIt_MicroATM_Request.Where(x => x.partnerAgentId.Trim() == objAEPSUpdateWalletRequest.agentId.Trim()).Take(1).SingleOrDefault();
                        if (entityByRetailerIdInDb == null)
                        {
                            objAEPSTransactionResponse.Status = "1";
                            objAEPSTransactionResponse.Description = "Agent_Id not mapped in database.";
                        }
                        else
                        {
                            long entityUserId = 0;
                            int companyId = 0;
                            Boolean transactionFound = false;
                            var txnIdInTransactionDB = new tblBankIt_AEPS_Transaction();
                            tblBankIt_AEPS_Transaction objAEPSTransaction = new tblBankIt_AEPS_Transaction();
                            if (objAEPSUpdateWalletRequest.txnStatus.Trim().ToLower() == "success" && objAEPSUpdateWalletRequest.Amount > 0)
                            {
                                var entityLeadUser = db.QryEntityUser.Where(x => x.EntityId == entityByRetailerIdInDb.EntityId && x.LeadUser == true).Take(1).SingleOrDefault();
                                if (entityLeadUser != null)
                                {
                                    entityUserId = entityLeadUser.EntityUserId;
                                    companyId = entityLeadUser.CompanyId;
                                }
                                long entityId = entityByRetailerIdInDb.EntityId;
                                txnIdInTransactionDB = db.tblBankIt_AEPS_Transaction.Where(x => x.rrn == objAEPSUpdateWalletRequest.rrn && x.EntityId == entityId).Take(1).SingleOrDefault();
                                if (txnIdInTransactionDB != null)
                                {
                                    objAEPSTransaction.AmountCredited = txnIdInTransactionDB.AmountCredited;
                                    transactionFound = true;
                                }

                            }
                            else { objAEPSTransaction.AmountCredited = false; }

                            if (objAEPSTransaction.AmountCredited == false)
                            {
                                if (objAEPSUpdateWalletRequest.Amount > 0)
                                {
                                    helper.processEntityCommissionNTaxes_BANKITDMT(objAEPSUpdateWalletRequest.Amount, entityByRetailerIdInDb.EntityId, entityUserId, objAEPSUpdateWalletRequest.ClientRefID, companyId, 6, true);
                                    objAEPSTransaction.AmountCredited = true;
                                }
                            }

                            objAEPSTransaction.rrn = objAEPSUpdateWalletRequest.rrn == null ? string.Empty : objAEPSUpdateWalletRequest.rrn;
                            objAEPSTransaction.stanNo = objAEPSUpdateWalletRequest.stanNo == null ? string.Empty : objAEPSUpdateWalletRequest.stanNo;
                            objAEPSTransaction.txnId = objAEPSUpdateWalletRequest.ClientRefID;
                            objAEPSTransaction.aepstxnId = string.Empty;
                            objAEPSTransaction.action = "Credit";
                            objAEPSTransaction.device = objAEPSUpdateWalletRequest.deviceNo;
                            objAEPSTransaction.status = objAEPSUpdateWalletRequest.txnStatus;
                            objAEPSTransaction.txnStatus = objAEPSUpdateWalletRequest.txnStatus;
                            objAEPSTransaction.bankName = string.Empty;
                            objAEPSTransaction.uId = string.Empty;
                            objAEPSTransaction.Agent_Id = objAEPSUpdateWalletRequest.agentId;
                            objAEPSTransaction.Service = objAEPSUpdateWalletRequest.Service;
                            objAEPSTransaction.Amount = Convert.ToDecimal(objAEPSUpdateWalletRequest.Amount);
                            objAEPSTransaction.CreatedDateTime = Convert.ToDateTime(objAEPSUpdateWalletRequest.transactionDatetime); //helper.getIndianTime();
                            objAEPSTransaction.EntityId = entityByRetailerIdInDb.EntityId;
                            objAEPSTransaction.ClientRefID = objAEPSUpdateWalletRequest.ClientRefID;
                            objAEPSTransaction.ResponseAPI = responseAPI;
                            objAEPSTransaction.IsMicroATMService = true;
                            objAEPSTransaction.IPAddress = helper.getClientIPAddress();
                            if (transactionFound == false)
                            {
                                db.tblBankIt_AEPS_Transaction.Add(objAEPSTransaction);
                            }
                            else
                            {
                                db.Entry(txnIdInTransactionDB).State = EntityState.Modified;
                                txnIdInTransactionDB.AmountCredited = true;
                            }
                            db.SaveChanges();
                            objAEPSTransactionResponse.Status = "0";
                            objAEPSTransactionResponse.Description = "Transaction Success";
                        }
                        return Ok(objAEPSTransactionResponse);
                    }
                    else
                    {
                        var entityByRetailerIdInDb = db.tblBankIt_AEPS_Request.Where(x => x.RetailerId.Trim() == objAEPSUpdateWalletRequest.Agent_Id.Trim()).Take(1).SingleOrDefault();
                        if (entityByRetailerIdInDb == null)
                        {
                            objAEPSTransactionResponse.Status = "1";
                            objAEPSTransactionResponse.Description = "Agent_Id not mapped in database.";
                        }
                        else
                        {
                            long entityUserId = 0;
                            int companyId = 0;
                            Boolean transactionFound = false;
                            var txnIdInTransactionDB = new tblBankIt_AEPS_Transaction();
                            tblBankIt_AEPS_Transaction objAEPSTransaction = new tblBankIt_AEPS_Transaction();
                            if (objAEPSUpdateWalletRequest.txnStatus.Trim().ToLower() == "success" && objAEPSUpdateWalletRequest.action.Trim().ToLower() == "credit")
                            {
                                var entityLeadUser = db.QryEntityUser.Where(x => x.EntityId == entityByRetailerIdInDb.EntityId && x.LeadUser == true).Take(1).SingleOrDefault();
                                if (entityLeadUser != null)
                                {
                                    entityUserId = entityLeadUser.EntityUserId;
                                    companyId = entityLeadUser.CompanyId;
                                }
                                long entityId = entityByRetailerIdInDb.EntityId;
                                txnIdInTransactionDB = db.tblBankIt_AEPS_Transaction.Where(x => x.txnId == objAEPSUpdateWalletRequest.txnId && x.EntityId == entityId).Take(1).SingleOrDefault();
                                if (txnIdInTransactionDB != null)
                                {
                                    objAEPSTransaction.AmountCredited = txnIdInTransactionDB.AmountCredited;
                                    transactionFound = true;
                                }

                            }
                            else { objAEPSTransaction.AmountCredited = false; }

                            if (objAEPSTransaction.AmountCredited == false)
                            {
                                if (objAEPSUpdateWalletRequest.Amount > 0)
                                {
                                    helper.processEntityCommissionNTaxes_BANKITDMT(objAEPSUpdateWalletRequest.Amount, entityByRetailerIdInDb.EntityId, entityUserId, objAEPSUpdateWalletRequest.txnId, companyId, 2, true);
                                    objAEPSTransaction.AmountCredited = true;
                                }
                            }

                            objAEPSTransaction.rrn = objAEPSUpdateWalletRequest.rrn == null ? string.Empty : objAEPSUpdateWalletRequest.rrn;
                            objAEPSTransaction.stanNo = objAEPSUpdateWalletRequest.stanNo == null ? string.Empty : objAEPSUpdateWalletRequest.stanNo;
                            objAEPSTransaction.txnId = objAEPSUpdateWalletRequest.txnId;
                            objAEPSTransaction.aepstxnId = objAEPSUpdateWalletRequest.aepstxnId;
                            objAEPSTransaction.action = objAEPSUpdateWalletRequest.action;
                            objAEPSTransaction.device = objAEPSUpdateWalletRequest.device;
                            objAEPSTransaction.status = objAEPSUpdateWalletRequest.status;
                            objAEPSTransaction.txnStatus = objAEPSUpdateWalletRequest.txnStatus;
                            objAEPSTransaction.bankName = objAEPSUpdateWalletRequest.bankName;
                            objAEPSTransaction.uId = objAEPSUpdateWalletRequest.uId;
                            objAEPSTransaction.Agent_Id = objAEPSUpdateWalletRequest.Agent_Id;
                            objAEPSTransaction.Service = objAEPSUpdateWalletRequest.Service;
                            objAEPSTransaction.Amount = Convert.ToDecimal(objAEPSUpdateWalletRequest.Amount);
                            objAEPSTransaction.CreatedDateTime = helper.getIndianTime();
                            objAEPSTransaction.EntityId = entityByRetailerIdInDb.EntityId;
                            objAEPSTransaction.ResponseAPI = responseAPI;
                            objAEPSTransaction.ClientRefID = string.Empty;
                            objAEPSTransaction.IsMicroATMService = false;
                            if (transactionFound == false)
                            {
                                db.tblBankIt_AEPS_Transaction.Add(objAEPSTransaction);
                            }
                            else
                            {
                                db.Entry(txnIdInTransactionDB).State = EntityState.Modified;
                                txnIdInTransactionDB.AmountCredited = true;
                            }
                            db.SaveChanges();
                            objAEPSTransactionResponse.Status = "0";
                            objAEPSTransactionResponse.Description = "Transaction Success";
                        }
                        return Ok(objAEPSTransactionResponse);
                    }
                }
                else
                {
                    return BadRequest("Invalid developer_key");
                }
            }
            else
            {
                return BadRequest("developer_key missing");
            }
        }

        #region AEPS Transaction History
        [Authorize]
        [HttpGet]
        [Route("api/AEPS/transactionsHistory")]
        public IHttpActionResult transactionsHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate, string transactionType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.EntityId == entityId && x.Service != "Micro ATM" && x.Amount > 0 &&
                    (transactionType == null || x.Service.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                    (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                    ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                else
                {
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.CompanyId == companyId && x.Service != "Micro ATM" && x.Amount > 0 &&
                    (transactionType == null || x.Service.Trim().ToLower() == transactionType.Trim().ToLower()) &&
                    (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                    ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.Where(s => s.BankITAEPSTransactionId > 0 &&
                    (s.rrn == searchValue.Trim()
                    || s.AgencyName == searchValue.Trim()
                    || s.txnId.Contains(searchValue.Trim())
                    || s.device.Contains(searchValue.Trim())
                    || s.txnId == searchValue.Trim()
                    || s.ResponseAPI == searchValue.Trim()
                    || s.bankName.Trim().Contains(searchValue.Trim())
                    || s.Service.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/AEPS/retailerTransactionHistory")]
        public IHttpActionResult retailerTransactionHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.masterDistributor))
                    {
                        var param = new SqlParameter[] {
                            new SqlParameter ("@EntityId", entityId),
                            new SqlParameter ("@FromDate", dFromDate),
                            new SqlParameter ("@ToDate", dToDate),
                            new SqlParameter ("@SearchEntityId", searchEntityId)
                        };
                        objTransactions.objBankITAEPSTransactionsList = db.Database.SqlQuery<QryBankIT_AEPS_MicroATM_Transactions>("EXEC [dbo].[AEPS_Retailer_TransactionReport_4_MD] @EntityId={0}, @FromDate={1}, @ToDate={2}, @SearchEntityId={3}",
                            entityId, dFromDate, dToDate, searchEntityId).ToList();
                    }
                    else
                    {
                        objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.ParentEntityId == entityId && x.Service != "Micro ATM" && x.Amount > 0 &&
                            (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                            ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                    }
                }
                else
                {
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.CompanyId == companyId && x.Service != "Micro ATM" && x.Amount > 0 &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.Where(s => s.BankITAEPSTransactionId > 0 &&
                    (s.rrn == searchValue.Trim()
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim())
                    || s.txnId.Contains(searchValue.Trim())
                    || s.device.Contains(searchValue.Trim())
                    || s.txnId == searchValue.Trim()
                    || s.ResponseAPI == searchValue.Trim()
                    || s.bankName.Trim().Contains(searchValue.Trim())
                    || s.Service.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        #endregion

        #region Micro ATM Transaction History
        [Authorize]
        [HttpGet]
        [Route("api/MicroATM/MicroATMTransactionsHistory")]
        public IHttpActionResult MicroATMTransactionsHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.EntityId == entityId && x.Service == "Micro ATM" && x.Amount > 0 &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                else
                {
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.CompanyId == companyId && x.Service == "Micro ATM" && x.Amount > 0 &&
                    (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                    ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.Where(s => s.BankITAEPSTransactionId > 0 &&
                    (s.rrn == searchValue.Trim()
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim())
                    || s.txnId.Contains(searchValue.Trim())
                    || s.device.Contains(searchValue.Trim())
                    || s.ResponseAPI.Contains(searchValue.Trim())
                    || s.Service.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        [Route("api/MicroATM/MicroRetailerTransactionHistory")]
        public IHttpActionResult MicroRetailerTransactionHistory(long searchEntityId, int pageIndex, int pageSize, string searchValue, string sortDirection, string fromDate, string toDate)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                DateTime dFromDate = new DateTime(Convert.ToInt32(fromDate.Split('-')[2]), Convert.ToInt32(fromDate.Split('-')[1]), Convert.ToInt32(fromDate.Split('-')[0]), 0, 0, 0);
                DateTime dToDate = new DateTime(Convert.ToInt32(toDate.Split('-')[2]), Convert.ToInt32(toDate.Split('-')[1]), Convert.ToInt32(toDate.Split('-')[0]), 23, 0, 0);
                BankITAEPSTransactionsVM objTransactions = new BankITAEPSTransactionsVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                Int32 companyId = Convert.ToInt32(identityCompanyId.Value);
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(x => x.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    if (Convert.ToInt16(identityEntityType.Value) == Convert.ToInt16(helper.entityTypes.masterDistributor))
                    {
                        var param = new SqlParameter[] {
                            new SqlParameter ("@EntityId", entityId),
                            new SqlParameter ("@FromDate", dFromDate),
                            new SqlParameter ("@ToDate", dToDate),
                            new SqlParameter ("@SearchEntityId", searchEntityId)
                        };
                        objTransactions.objBankITAEPSTransactionsList = db.Database.SqlQuery<QryBankIT_AEPS_MicroATM_Transactions>("EXEC [dbo].[MicroATM_Retailer_TransactionReport_4_MD] @EntityId={0}, @FromDate={1}, @ToDate={2}, @SearchEntityId={3}",
                            entityId, dFromDate, dToDate, searchEntityId).ToList();
                    }
                    else
                    {
                        objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.EntityId == entityId && x.Service == "Micro ATM" && x.Amount > 0 &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                    }
                }
                else
                {
                    objTransactions.objBankITAEPSTransactionsList = db.QryBankIT_AEPS_MicroATM_Transactions.Where(x => x.CompanyId == companyId && x.Service == "Micro ATM" && x.Amount > 0 &&
                        (searchEntityId == 0 || x.EntityId == searchEntityId) &&
                        ((x.CreatedDateTime >= dFromDate && x.CreatedDateTime <= dToDate))).ToList();
                }
                objTransactions.totalRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.Where(s => s.BankITAEPSTransactionId > 0 &&
                    (s.rrn == searchValue.Trim()
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim())
                    || s.txnId.Contains(searchValue.Trim())
                    || s.device.Contains(searchValue.Trim())
                    || s.ResponseAPI.Contains(searchValue.Trim())
                    || s.Service.Trim().Contains(searchValue.Trim()))).ToList();

                    objTransactions.filterRecords = objTransactions.objBankITAEPSTransactionsList.Count();
                }
                else
                    objTransactions.filterRecords = objTransactions.totalRecords;

                if (sortDirection == "asc")
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                else
                {
                    if (pageSize > 0)
                        objTransactions.objBankITAEPSTransactionsList = objTransactions.objBankITAEPSTransactionsList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryBankIT_AEPS_MicroATM_Transactions>();
                }
                return Ok(objTransactions);
            }
            else
            {
                return Unauthorized();
            }
        }
        #endregion
    }
}