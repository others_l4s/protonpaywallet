﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class documentMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/documentMaster
        [Authorize]
        [ResponseType(typeof(documentMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getDocumentMasterList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    documentMasterViewModel objDocumentMasterViewModel = new documentMasterViewModel
                    {
                        objDocumentMaster = new tblDocument_Master(),
                        objStatusList = db.tblStatus_Master.ToList()
                    };
                    return Ok(objDocumentMasterViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();

        }

        // GET: api/documentMaster/5
        [Authorize]
        [ResponseType(typeof(tblDocument_Master))]
        public IHttpActionResult getDocumentMasterById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDocument_Master objDocumentMaster = db.tblDocument_Master.Find(id);
                    if (objDocumentMaster == null)
                    {
                        return NotFound();
                    }
                    return Ok(objDocumentMaster);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(documentMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getDocumentMasterListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    documentMasterViewModel objDocumentMasterViewModel = new documentMasterViewModel();
                    objDocumentMasterViewModel.totalRecords = db.tblDocument_Master.Where(s => s.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objDocumentMasterViewModel.objDocumentMasterList = db.QryDocumentMaster.Where(s => s.CompanyId == CompanyId && (s.DocumentName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();
                        objDocumentMasterViewModel.filterRecords = objDocumentMasterViewModel.objDocumentMasterList.Count();
                    }
                    else
                        objDocumentMasterViewModel.filterRecords = objDocumentMasterViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objDocumentMasterViewModel.objDocumentMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDocumentMaster.Where(s => s.CompanyId == CompanyId).OrderBy(s => s.DocumentName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryDocumentMaster>() :
                            objDocumentMasterViewModel.objDocumentMasterList.OrderBy(s => s.DocumentName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryDocumentMaster>();
                    else
                        objDocumentMasterViewModel.objDocumentMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryDocumentMaster.Where(s => s.CompanyId == CompanyId).OrderByDescending(s => s.DocumentName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryDocumentMaster>() :
                            objDocumentMasterViewModel.objDocumentMasterList.OrderByDescending(s => s.DocumentName).ThenBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryDocumentMaster>();

                    return Ok(objDocumentMasterViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }


        // PUT: api/documentMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateDocumentMaster(int id, tblDocument_Master objDocumentMaster)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objDocumentMaster.DocumentId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                        var documentInDbExists = db.tblDocument_Master.SingleOrDefault(d => d.DocumentName.Trim().ToLower() == objDocumentMaster.DocumentName.Trim().ToLower() &&
                        d.CompanyId == CompanyId &&
                        d.DocumentId != objDocumentMaster.DocumentId);
                        if (documentInDbExists != null)
                            return BadRequest("Document Name already exists for selected company");

                        db.Entry(objDocumentMaster).State = EntityState.Modified;
                        db.Entry(objDocumentMaster).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objDocumentMaster).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesDocumentExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok(objDocumentMaster);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/documentMaster
        [Authorize]
        [ResponseType(typeof(tblDocument_Master))]
        [HttpPost]
        public IHttpActionResult saveDocumentMaster(tblDocument_Master objDocumentMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(identityAdminLoginId.Value);
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);

                    var documentInDbExists = db.tblDocument_Master.SingleOrDefault(d => d.DocumentName.Trim().ToLower() == objDocumentMaster.DocumentName.Trim().ToLower() && 
                    d.CompanyId == CompanyId && 
                    d.DocumentId != objDocumentMaster.DocumentId);

                    if (documentInDbExists != null)
                        return BadRequest("Document Name already exists for selected company");


                    objDocumentMaster.CreatedBy = adminLoginId;
                    objDocumentMaster.CompanyId = CompanyId;
                    db.tblDocument_Master.Add(objDocumentMaster);
                    db.SaveChanges();
                    return Ok(objDocumentMaster);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/documentMaster/5
        [Authorize]
        [ResponseType(typeof(tblDocument_Master))]
        [HttpDelete]
        public IHttpActionResult deleteDocumentMaster(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblDocument_Master objDocumentMaster = db.tblDocument_Master.Find(id);
                    if (objDocumentMaster == null)
                    {
                        return NotFound();
                    }
                    db.tblDocument_Master.Remove(objDocumentMaster);
                    db.SaveChanges();

                    return Ok(objDocumentMaster);
                }
                else {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesDocumentExists(int id)
        {
            return db.tblDocument_Master.Count(e => e.DocumentId == id) > 0;
        }
    }
}