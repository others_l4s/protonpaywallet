﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class entityKYCController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entityKYC
        [Authorize]
        [ResponseType(typeof(entityKYCVM))]
        [Route("api/getEntityKYC")]
        [HttpGet]
        public IHttpActionResult getEntityKYCById(long Id = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                entityKYCVM objEntityKYCVM = new entityKYCVM();
                long entityId = objUserIdentity.isCompany ? Id : objUserIdentity.entityId;
                objEntityKYCVM.objEntityKYC = db.tblEntity_KYC.Where(x => x.EntityId == entityId).Take(1).SingleOrDefault();
                if (objEntityKYCVM.objEntityKYC == null)
                    objEntityKYCVM.objEntityKYC = new tblEntity_KYC();
                return Ok(objEntityKYCVM);
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/entityKYC/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateEntityKYC(long id, tblEntity_KYC objEntity_KYC)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                if (id != objEntity_KYC.EntityKYCId)
                {
                    return BadRequest();
                }

                db.Entry(objEntity_KYC).State = EntityState.Modified;
                try
                {
                    if (objEntity_KYC.FromMobileDevice.Value == true)
                    {
                        if (!String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.PanCardPhoto);
                            string panCardNoPhotoName = "pancard_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.PanCardPhoto = panCardNoPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + panCardNoPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto).IsModified = false;

                        if (!String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto2))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.PanCardPhoto2);
                            string panCardNoPhotoName = "pancard_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.PanCardPhoto2 = panCardNoPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + panCardNoPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto2).IsModified = false;

                        if (!String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.AadhaarCardPhoto);
                            string aadhaarCardPhotoName = "aadhaar_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.AadhaarCardPhoto = aadhaarCardPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + aadhaarCardPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto).IsModified = false;

                        if (!String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.AadhaarCardPhoto2);
                            string aadhaarCardPhotoName = "aadhaar_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.AadhaarCardPhoto2 = aadhaarCardPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + aadhaarCardPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;

                        if (!String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.GSTNoPhoto);
                            string GSTPhotoName = "gst_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.GSTNoPhoto = GSTPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + GSTPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto).IsModified = false;

                        if (!String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto2))
                        {
                            byte[] bytes = null;
                            bytes = Convert.FromBase64String(objEntity_KYC.GSTNoPhoto2);
                            string GSTPhotoName = "gst_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                            objEntity_KYC.GSTNoPhoto2 = GSTPhotoName;
                            string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + GSTPhotoName);
                            File.WriteAllBytes(filePath, bytes);
                        }
                        else
                            db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                    }

                    db.Entry(objEntity_KYC).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objEntity_KYC).Property(x => x.EntityUserId).IsModified = false;
                    db.Entry(objEntity_KYC).Property(x => x.AdminLoginId).IsModified = false;
                    db.Entry(objEntity_KYC).Property(x => x.IsKYCStatus).IsModified = false;
                    db.Entry(objEntity_KYC).Property(x => x.KYCStatusRemark).IsModified = false;
                    db.Entry(objEntity_KYC).Property(x => x.CreatedDateTime).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.PanDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.GSTDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.OutletId))
                        db.Entry(objEntity_KYC).Property(x => x.OutletId).IsModified = false;
                    if (objEntity_KYC.OutletId != null)
                        db.Entry(objEntity_KYC).Property(x => x.OutletId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.OutletDocumentGetStatusResponse))
                        db.Entry(objEntity_KYC).Property(x => x.OutletDocumentGetStatusResponse).IsModified = false;

                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblEntity_KYCExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/entityKYC
        [Authorize]
        [Route("api/saveEntityKYC")]
        [HttpPost]
        public IHttpActionResult saveEntityKYC(tblEntity_KYC objEntity_KYC)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                long entityId = (objUserIdentity.isCompany ? objEntity_KYC.EntityId : objUserIdentity.entityId);
                long entityUserId = (objUserIdentity.isCompany ? 0 : objUserIdentity.entityId);
                long adminLoginId = (objUserIdentity.isCompany ? objUserIdentity.adminLoginId : 0);
                var entityKYCInDb = db.tblEntity_KYC.Where(x => x.EntityId == entityId).Take(1).SingleOrDefault();

                if (objEntity_KYC.FromMobileDevice.Value == true)
                {
                    if (!String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.PanCardPhoto);
                        string panCardNoPhotoName = "pancard_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.PanCardPhoto = panCardNoPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + panCardNoPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }

                    if (!String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto2))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.PanCardPhoto2);
                        string panCardNoPhotoName = "pancard_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.PanCardPhoto2 = panCardNoPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + panCardNoPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }

                    if (!String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.AadhaarCardPhoto);
                        string aadhaarCardPhotoName = "aadhaar_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.AadhaarCardPhoto = aadhaarCardPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + aadhaarCardPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }

                    if (!String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.AadhaarCardPhoto2);
                        string aadhaarCardPhotoName = "aadhaar_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.AadhaarCardPhoto2 = aadhaarCardPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + aadhaarCardPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }

                    if (!String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.GSTNoPhoto);
                        string GSTPhotoName = "gst_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.GSTNoPhoto = GSTPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + GSTPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }

                    if (!String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto2))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEntity_KYC.GSTNoPhoto2);
                        string GSTPhotoName = "gst_side_2_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEntity_KYC.GSTNoPhoto2 = GSTPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + GSTPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                    }
                }

                objEntity_KYC.PanCardPhoto2 = (String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto2) ? string.Empty : objEntity_KYC.PanCardPhoto2);
                objEntity_KYC.GSTNo = (String.IsNullOrEmpty(objEntity_KYC.GSTNo) ? string.Empty : objEntity_KYC.GSTNo);
                objEntity_KYC.GSTNoPhoto = (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto) ? string.Empty : objEntity_KYC.GSTNoPhoto);
                objEntity_KYC.GSTNoPhoto2 = (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto2) ? string.Empty : objEntity_KYC.GSTNoPhoto2);
                objEntity_KYC.AadhaarCardNo = (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardNo) ? string.Empty : objEntity_KYC.AadhaarCardNo);
                objEntity_KYC.AadhaarCardPhoto = (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto) ? string.Empty : objEntity_KYC.AadhaarCardPhoto);
                objEntity_KYC.AadhaarCardPhoto2 = (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2) ? string.Empty : objEntity_KYC.AadhaarCardPhoto2);
                if (entityKYCInDb != null)
                {
                    db.Entry(objEntity_KYC).State = EntityState.Modified;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanCardPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.PanCardPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTNoPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardPhoto2))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.PanDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.PanDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.GSTDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.GSTDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.AadhaarCardDocumentId))
                        db.Entry(objEntity_KYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.OutletId))
                        db.Entry(objEntity_KYC).Property(x => x.OutletId).IsModified = false;
                    if (objEntity_KYC.OutletId != null)
                        db.Entry(objEntity_KYC).Property(x => x.OutletId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntity_KYC.OutletDocumentGetStatusResponse))
                        db.Entry(objEntity_KYC).Property(x => x.OutletDocumentGetStatusResponse).IsModified = false;
                }
                else
                {
                    objEntity_KYC.CreatedDateTime = helper.getIndianTime();
                    objEntity_KYC.EntityId = entityId;
                    objEntity_KYC.EntityUserId = entityUserId;
                    objEntity_KYC.AdminLoginId = adminLoginId;
                    db.tblEntity_KYC.Add(objEntity_KYC);
                }
                db.SaveChanges();
                objEntity_KYC.PanCardPhoto = HttpContext.Current.Server.MapPath("~/KYCImages/" + objEntity_KYC.PanCardPhoto);
                return Ok(objEntity_KYC);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_KYCExists(long id)
        {
            return db.tblEntity_KYC.Count(e => e.EntityKYCId == id) > 0;
        }
    }
}