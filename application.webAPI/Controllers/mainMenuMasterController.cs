﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class mainMenuMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/mainMenuMaster
        [Authorize]
        [ResponseType(typeof(mainMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult GettblMainMenu_Master()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mainMenuMasterViewModel objMainMenuMasterViewModel = new mainMenuMasterViewModel()
                    {
                        objMainMenu = new tblMainMenu_Master(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList()
                    };
                    return Ok(objMainMenuMasterViewModel);
                }
                else
                    return Unauthorized();

            }
            else { return Unauthorized(); }
        }


        // GET: api/mainMenuMaster/5
        [Authorize]
        [ResponseType(typeof(mainMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult GettblMainMenu_Master(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMainMenu_Master objMainMenuMaster = db.tblMainMenu_Master.Find(id);
                    if (objMainMenuMaster == null)
                    {
                        return NotFound();
                    }

                    mainMenuMasterViewModel objMainMenuMasterViewModel = new mainMenuMasterViewModel()
                    {
                        objMainMenu = objMainMenuMaster,
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList()
                    };
                    return Ok(objMainMenuMasterViewModel);
                }
                else
                    return Unauthorized();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        // PUT: api/mainMenuMaster/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateMainMenuMaster(int id, tblMainMenu_Master objMainMenuMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objMainMenuMaster.MainMenuId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var mainMenuInDb = db.tblMainMenu_Master.SingleOrDefault(m => m.MainMenuName == objMainMenuMaster.MainMenuName
                        && m.MainMenuId != objMainMenuMaster.MainMenuId
                        && m.ExternalURL == objMainMenuMaster.ExternalURL);
                        if (mainMenuInDb != null)
                            return BadRequest("Menu Name already exists.");

                        objMainMenuMaster.ExternalURL = (objMainMenuMaster.ExternalURL == null ? "" : objMainMenuMaster.ExternalURL);
                        db.Entry(objMainMenuMaster).State = EntityState.Modified;
                        db.Entry(objMainMenuMaster).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objMainMenuMaster).Property(x => x.DisplayOrder).IsModified = false;
                        db.Entry(objMainMenuMaster).Property(x => x.CreatedDateTime).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        return InternalServerError(e.InnerException);
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!doesMainMenuMasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(mainMenuMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getMainMenuMasterByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mainMenuMasterViewModel objMainMenuMasterViewModel = new mainMenuMasterViewModel();
                    objMainMenuMasterViewModel.totalRecords = db.QryMainMenu.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMainMenuMasterViewModel.objMainMenuList = db.QryMainMenu.Where(s => s.MainMenuName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objMainMenuMasterViewModel.filterRecords = objMainMenuMasterViewModel.objMainMenuList.Count();
                    }
                    else
                        objMainMenuMasterViewModel.filterRecords = objMainMenuMasterViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objMainMenuMasterViewModel.objMainMenuList = String.IsNullOrEmpty(searchValue) ?
                            db.QryMainMenu.OrderBy(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QryMainMenu>() :
                            objMainMenuMasterViewModel.objMainMenuList.OrderBy(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QryMainMenu>();
                    else
                        objMainMenuMasterViewModel.objMainMenuList = String.IsNullOrEmpty(searchValue) ?
                            db.QryMainMenu.OrderByDescending(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QryMainMenu>() :
                            objMainMenuMasterViewModel.objMainMenuList.OrderByDescending(s => s.DisplayOrder).Skip(pageIndex).Take(pageSize).ToList<QryMainMenu>();

                    return Ok(objMainMenuMasterViewModel);
                }
                else
                    return Unauthorized();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/mainMenuMaster
        [Authorize]
        [ResponseType(typeof(tblMainMenu_Master))]
        [HttpPost]
        public IHttpActionResult saveMainMenu(tblMainMenu_Master objMainMenuMaster)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var mainMenuInDb = db.tblMainMenu_Master.SingleOrDefault(m => m.MainMenuName == objMainMenuMaster.MainMenuName
                    && m.ExternalURL == objMainMenuMaster.ExternalURL
                    && m.MainMenuId != objMainMenuMaster.MainMenuId);
                    if (mainMenuInDb != null)
                        return BadRequest("Menu Name already exists.");

                    var maxDisplayOrderInDb = db.tblMainMenu_Master.ToList();
                    int maxDisplayOrder = maxDisplayOrderInDb.Count == 0 ? 0 : maxDisplayOrderInDb.Max(m => m.DisplayOrder);
                    objMainMenuMaster.DisplayOrder = maxDisplayOrder + 1;
                    objMainMenuMaster.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    objMainMenuMaster.CreatedDateTime = helper.getIndianTime();
                    objMainMenuMaster.ExternalURL = objMainMenuMaster.ExternalURL == null ? "" : objMainMenuMaster.ExternalURL;
                    objMainMenuMaster.ExternalURLTarget = objMainMenuMaster.ExternalURLTarget == null ? "" : objMainMenuMaster.ExternalURLTarget;

                    db.tblMainMenu_Master.Add(objMainMenuMaster);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        // DELETE: api/mainMenuMaster/5
        [ResponseType(typeof(tblMainMenu_Master))]
        [HttpDelete]
        public IHttpActionResult deleteMainMenu(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMainMenu_Master objMainMenuMaster = db.tblMainMenu_Master.Find(id);
                    if (objMainMenuMaster == null)
                    {
                        return NotFound();
                    }

                    db.tblSubMenu_Role_Permission.RemoveRange(db.tblSubMenu_Role_Permission.Where(s => s.SubMenuId == db.tblSubMenu_Master.Where(m => m.MainMenuId == s.MainMenuId).Max(m => m.SubMenuId)));
                    db.tblSubMenu_Master.RemoveRange(db.tblSubMenu_Master.Where(s => s.MainMenuId == id));
                    db.tblSubMenu_Role_Permission.RemoveRange(db.tblSubMenu_Role_Permission.Where(s => s.MainMenuId == id));
                    db.tblMainMenu_Master.Remove(objMainMenuMaster);
                    db.SaveChanges();

                    return Ok(objMainMenuMaster);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        //[Authorize]
        //[HttpGet]
        //[Route("api/mainMenu/getMainMenuByCompanyId")]
        //public List<tblMainMenu_Master> getMainMenuByCompanyId(int CompanyId)
        //{
        //    return db.tblMainMenu_Master.Where(r => r.CompanyId == CompanyId).OrderBy(r => r.MainMenuName).ToList();
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesMainMenuMasterExists(int id)
        {
            return db.tblMainMenu_Master.Count(e => e.MainMenuId == id) > 0;
        }
    }
}