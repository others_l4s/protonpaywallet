﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Http.ValueProviders;
using System.Web.WebPages.Scope;
using application.webAPI.Models;
using application.webAPI.prime.sdk;
using Newtonsoft.Json;
using RestSharp;
using System.Web.Script.Serialization;
using System.Web.WebPages;

namespace application.webAPI.Controllers
{
    [RoutePrefix("api/prime")]
    public class primeController : ApiController
    {
        public Dictionary<string, string> parameterList { get; private set; } = new Dictionary<string, string>();
        private applicationDbEntities db = new applicationDbEntities();

        [Route("balance")]
        [HttpPost]
        public IHttpActionResult balance(balanceWebhook objBalanceWebhook)
        {
            string jsonRequest = new JavaScriptSerializer().Serialize(objBalanceWebhook);
            balanceWebhookResponse objResponse = new balanceWebhookResponse();
            tblPrimeBalanceWebhook_Response objPrimeBalanceWebhookResponse = new tblPrimeBalanceWebhook_Response();
            objPrimeBalanceWebhookResponse.BalanceAppId = objBalanceWebhook.app_id;
            objPrimeBalanceWebhookResponse.BalanceRequestId = objBalanceWebhook.request_id;
            objPrimeBalanceWebhookResponse.BalanceSessionId = objBalanceWebhook.session_id;
            objPrimeBalanceWebhookResponse.OutletPanNo = objBalanceWebhook.outlet_pan;
            objPrimeBalanceWebhookResponse.BalanceWebhookResponse = jsonRequest;
            objPrimeBalanceWebhookResponse.CreatedDateTime = helper.getIndianTime();
            objPrimeBalanceWebhookResponse.EntityId = 0;
            tblEntity_KYC objEntityKYC = db.tblEntity_KYC.Where(x => x.PanCardNo.Trim().ToLower() == objBalanceWebhook.outlet_pan.Trim().ToLower()).Take(1).SingleOrDefault();
            if (objEntityKYC != null)
            {
                var entityWalletInDb = db.tblEntity_Wallet.Where(x => x.EntityId == objEntityKYC.EntityId).SingleOrDefault();
                objResponse.response_code = "TXN";
                objResponse.response_msg = "Transaction Successfull";
                objResponse.transactions = new List<transactionsResponse>();
                objResponse.transactions.Add(new transactionsResponse { balance = (entityWalletInDb == null ? 0 : Convert.ToDouble(entityWalletInDb.WalletBalance)) });
                objPrimeBalanceWebhookResponse.EntityId = objEntityKYC.EntityId;
            }
            else
            {
                objResponse.response_code = "ERR";
                objResponse.response_msg = "Outlet pan mismatch";
            }
            db.tblPrimeBalanceWebhook_Response.Add(objPrimeBalanceWebhookResponse);
            db.SaveChanges();
            return Ok(objResponse);
        }


        [Route("verifyTransaction")]
        [HttpPost]
        public IHttpActionResult verifyTransaction(verifyTransactionWebhook objVerifyTransaction)
        {
            verifyTransactionResponse objResponse = new verifyTransactionResponse();
            tblPrimeTransactionWebhook_Response objPrimeTransactionWebhookResponse = new tblPrimeTransactionWebhook_Response();
            try
            {
                string jsonRequest = new JavaScriptSerializer().Serialize(objVerifyTransaction);
                string agentID = helper.getRandomNumericDateTimeValue();
                objPrimeTransactionWebhookResponse.TransactionWebhookResponse = jsonRequest;
                objPrimeTransactionWebhookResponse.TransactionRequestId = objVerifyTransaction.request_id;
                objPrimeTransactionWebhookResponse.TransactionSessionId = objVerifyTransaction.session_id;
                objPrimeTransactionWebhookResponse.TransactionAppId = objVerifyTransaction.app_id;
                objPrimeTransactionWebhookResponse.OutletPanNo = objVerifyTransaction.outlet_pan;
                objPrimeTransactionWebhookResponse.CreatedDateTime = helper.getIndianTime();
                for (int i = 0; i < objVerifyTransaction.transactions.Count; i++)
                {
                    objPrimeTransactionWebhookResponse.TransactionSPKey = objVerifyTransaction.transactions[i].sp_key;
                    objPrimeTransactionWebhookResponse.TransactionAmount = Convert.ToDecimal(objVerifyTransaction.transactions[i].amount);
                    for (int j = 0; j < objVerifyTransaction.transactions[i].customer_params.Length; j++)
                    {
                        objPrimeTransactionWebhookResponse.TransactionCustomerParameter = objVerifyTransaction.transactions[i].customer_params[j];
                    }
                }
                objPrimeTransactionWebhookResponse.TransactionAgentId = agentID;
                objPrimeTransactionWebhookResponse.EntityId = 0;
                tblEntity_KYC objEntktyKYC = db.tblEntity_KYC.Where(x => x.PanCardNo.Trim().ToLower() == objVerifyTransaction.outlet_pan.Trim().ToLower()).Take(1).SingleOrDefault();
                if (objEntktyKYC != null)
                {
                    objResponse.response_code = "TXN";
                    objResponse.response_msg = "Transaction Successfull";
                    objResponse.transactions = new List<transactionsResponseAgentId>();
                    objResponse.transactions.Add(new transactionsResponseAgentId { agent_id = agentID });
                    objPrimeTransactionWebhookResponse.EntityId = objEntktyKYC.EntityId;
                }
                else
                {
                    objResponse.response_code = "ERR";
                    objResponse.response_msg = "outlet pan mismatch";
                }
                db.tblPrimeTransactionWebhook_Response.Add(objPrimeTransactionWebhookResponse);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                objPrimeTransactionWebhookResponse.TransactionStatus = ex.Message.ToString();
                db.tblPrimeTransactionWebhook_Response.Add(objPrimeTransactionWebhookResponse);
                db.SaveChanges();
            }
            return Ok(objResponse);
        }

        [Route("updateTransactionStatus")]
        [HttpGet]
        public IHttpActionResult updateTransactionStatus(string ipay_id = "", string agent_id = "", string opr_id = "", string status = "", string res_code = "", string res_msg = "", string txn_mode = "")
        {
            tblPrimeTransactionWebhook_Response objPrimeTransactionResponse = db.tblPrimeTransactionWebhook_Response.Where(x => x.TransactionAgentId.Trim() == agent_id.Trim()).Take(1).SingleOrDefault();
            if (objPrimeTransactionResponse != null)
            {
                objPrimeTransactionResponse.TransactionIPay_Id = ipay_id;
                objPrimeTransactionResponse.TransactionOpr_Id = opr_id;
                objPrimeTransactionResponse.TransactionStatus = status;
                objPrimeTransactionResponse.TransactionResponseCode = res_code;
                objPrimeTransactionResponse.TransactionResponseMsg = res_msg;
                objPrimeTransactionResponse.IPAddress = helper.getClientIPAddress();
                objPrimeTransactionResponse.TransactionConfirmDateTime = helper.getIndianTime();
                db.Entry(objPrimeTransactionResponse).State = EntityState.Modified;
                db.Entry(objPrimeTransactionResponse).Property(x => x.EntityId).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionRequestId).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionSessionId).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionAppId).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.OutletPanNo).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionSPKey).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionAmount).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionCustomerParameter).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionAgentId).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objPrimeTransactionResponse).Property(x => x.TransactionWebhookResponse).IsModified = false;
                db.SaveChanges();
                if (status == "SUCCESS" && res_code == "TXN") //Performing accounting
                {
                    string description = objPrimeTransactionResponse.TransactionSPKey + " recharge";
                    string walletUpdateStatus = helper.updateWalletBalance(objPrimeTransactionResponse.EntityId, -objPrimeTransactionResponse.TransactionAmount);
                    string transactStatus = helper.transactEntityAccount(objPrimeTransactionResponse.EntityId, -objPrimeTransactionResponse.TransactionAmount,
                        ipay_id, description, Convert.ToInt32(helper.transactionTypes.Booking), false, 8, 0, objPrimeTransactionResponse.TransactionAmount);
                }
            }
            return Ok(ipay_id);
        }

        [Route("accessPrime")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult accessPrimePortal()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                string result;
                primeURLs objPrimeURLs = new primeURLs();
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                tblEntity_KYC objEntityKYCInDb = db.tblEntity_KYC.Where(x => x.EntityId == objUserIdentity.entityId).Take(1).SingleOrDefault();
                if (objEntityKYCInDb != null)
                {
                    //if ((objEntityKYCInDb.IsKYCStatus == null ? false : objEntityKYCInDb.IsKYCStatus.Value) == true)
                    //{

                    //}
                    //else
                    //{
                    //    return BadRequest("KYC not approved");
                    //}
                    // Order of Keys is important. Don't change it.
                    parameterList.Add("ALLOWED_SERVICES", "");
                    parameterList.Add("APP_ID", "310");
                    parameterList.Add("BLOCKED_SERVICES", "WAP,BAP,SAP");
                    parameterList.Add("LANDING_SERVICE", ""); // optional, service to show first after login
                    parameterList.Add("PAN", objEntityKYCInDb.PanCardNo.Trim().ToUpper());

                    string checksumString = "";
                    foreach (var item in parameterList)
                    {
                        if (checksumString != "")
                        {
                            checksumString += "|";
                        }
                        checksumString += (item.Key) + ":" + (item.Value);
                    }

                    string iv = "1234567812345678";
                    checksumString = iv + checksumString;
                    string keyhmac = "3b96e13ed3b3759863d96a3625557332a6b1706652a478918d575a755db92eb0";
                    byte[] _keyhmac = Encoding.UTF8.GetBytes(keyhmac);
                    string key = keyhmac.Substring(0, 16);

                    byte[] _plainText = Encoding.UTF8.GetBytes(checksumString);
                    byte[] _key = Encoding.UTF8.GetBytes(key);
                    byte[] _iv = Encoding.UTF8.GetBytes(iv);

                    byte[] output_buffer = new byte[_plainText.Length];
                    using (AesManaged aesAlg = new AesManaged())
                    {
                        //If CBC, must initialize IV = O_{128}
                        aesAlg.Mode = CipherMode.CBC;
                        aesAlg.IV = _iv;
                        //aesAlg.Mode = CipherMode.ECB;

                        aesAlg.BlockSize = 128;
                        aesAlg.KeySize = 128;
                        //aesAlg.Padding = PaddingMode.None;

                        aesAlg.Key = _key;

                        // Create a decrytor to perform the stream transform.
                        ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                        output_buffer = encryptor.TransformFinalBlock(_plainText, 0, _plainText.Length);
                    }

                    Byte[] hashBytes;

                    using (HMACSHA256 hash = new HMACSHA256(_keyhmac))
                        hashBytes = hash.ComputeHash(output_buffer);

                    Byte[] b = Combine(_iv, hashBytes, output_buffer);

                    parameterList.Add("CHECKSUMHASH", Convert.ToBase64String(b));

                    // POST the parameterList to the CNAME with name = params
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(parameterList);
                    result = WebUtility.UrlEncode(result);
                    Dictionary<string, object> postData = new Dictionary<string, object>();
                    postData.Add("params", result);
                    objPrimeURLs.primePostData = result;
                    objPrimeURLs.primeCompleteURL = "https://prime.protonpay.net?" + postData;
                    return Ok(objPrimeURLs);
                }
                else
                {
                    return BadRequest("KYC not Uploaded");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Route("accessPrimeAndroidWebView")]
        [HttpGet]
        public IHttpActionResult accessPrimeAndroidWebView(long entityId)
        {
            string result;
            primeURLs objPrimeURLs = new primeURLs();
            tblEntity_KYC objEntityKYCInDb = db.tblEntity_KYC.Where(x => x.EntityId == entityId).Take(1).SingleOrDefault();
            if (objEntityKYCInDb != null)
            {
                //if ((objEntityKYCInDb.IsKYCStatus == null ? false : objEntityKYCInDb.IsKYCStatus.Value) == true)
                //{

                //}
                //else
                //{
                //    return BadRequest("KYC not approved");
                //}
                // Order of Keys is important. Don't change it.
                parameterList.Add("ALLOWED_SERVICES", "");
                parameterList.Add("APP_ID", "310");
                parameterList.Add("BLOCKED_SERVICES", "WAP,BAP,SAP");
                parameterList.Add("LANDING_SERVICE", ""); // optional, service to show first after login
                parameterList.Add("PAN", objEntityKYCInDb.PanCardNo.Trim().ToUpper());

                string checksumString = "";
                foreach (var item in parameterList)
                {
                    if (checksumString != "")
                    {
                        checksumString += "|";
                    }
                    checksumString += (item.Key) + ":" + (item.Value);
                }

                string iv = "1234567812345678";
                checksumString = iv + checksumString;
                string keyhmac = "3b96e13ed3b3759863d96a3625557332a6b1706652a478918d575a755db92eb0";
                byte[] _keyhmac = Encoding.UTF8.GetBytes(keyhmac);
                string key = keyhmac.Substring(0, 16);

                byte[] _plainText = Encoding.UTF8.GetBytes(checksumString);
                byte[] _key = Encoding.UTF8.GetBytes(key);
                byte[] _iv = Encoding.UTF8.GetBytes(iv);

                byte[] output_buffer = new byte[_plainText.Length];
                using (AesManaged aesAlg = new AesManaged())
                {
                    //If CBC, must initialize IV = O_{128}
                    aesAlg.Mode = CipherMode.CBC;
                    aesAlg.IV = _iv;
                    //aesAlg.Mode = CipherMode.ECB;

                    aesAlg.BlockSize = 128;
                    aesAlg.KeySize = 128;
                    //aesAlg.Padding = PaddingMode.None;

                    aesAlg.Key = _key;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    output_buffer = encryptor.TransformFinalBlock(_plainText, 0, _plainText.Length);
                }

                Byte[] hashBytes;

                using (HMACSHA256 hash = new HMACSHA256(_keyhmac))
                    hashBytes = hash.ComputeHash(output_buffer);

                Byte[] b = Combine(_iv, hashBytes, output_buffer);

                parameterList.Add("CHECKSUMHASH", Convert.ToBase64String(b));

                // POST the parameterList to the CNAME with name = params
                result = Newtonsoft.Json.JsonConvert.SerializeObject(parameterList);
                result = WebUtility.UrlEncode(result);
                Dictionary<string, object> postData = new Dictionary<string, object>();
                postData.Add("params", result);
                objPrimeURLs.primePostData = result;
                objPrimeURLs.primeCompleteURL = "https://prime.protonpay.net?" + postData;
                return Ok(objPrimeURLs);
            }
            else
            {
                return BadRequest("KYC not Uploaded");
            }
        }

        [Route("outletRegistrationOTP")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult outletRegistrationOTP()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                QryEntityKYC objEntityUserInDb = db.QryEntityKYC.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
                if (objEntityUserInDb != null)
                {
                    if ((objEntityUserInDb.OutletMobileVerified == null ? false : objEntityUserInDb.OutletMobileVerified.Value) == false)
                    {
                        registrationOTP objRegistrationOTP = new registrationOTP();
                        objRegistrationOTP.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        objRegistrationOTP.request = new registrationOTPRequest();
                        objRegistrationOTP.request.mobile = objEntityUserInDb.Mobile;
                        IRestResponse IRegistrationOTPResponse = registrationOTPForOutet(objRegistrationOTP);
                        if (IRegistrationOTPResponse.StatusCode == HttpStatusCode.OK)
                        {
                            registrationOTPResponse objRegistrationOTPResponse = JsonConvert.DeserializeObject<registrationOTPResponse>(IRegistrationOTPResponse.Content);
                            if (objRegistrationOTPResponse.statuscode == "TXN")
                                return Ok();
                            else
                                return BadRequest(objRegistrationOTPResponse.statuscode);
                        }
                        else
                        {
                            return BadRequest(IRegistrationOTPResponse.StatusCode.ToString());
                        }
                    }
                    else
                        return Ok();
                }
                else
                {
                    return BadRequest("Invalid User");
                }
            }
            else
            {
                return Unauthorized();
            }
        }


        [Route("outletRegistration")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult outletRegistration(string mobileOTP)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                QryEntityKYC objEntityUserInDb = db.QryEntityKYC.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
                if (objEntityUserInDb != null)
                {
                    if ((objEntityUserInDb.OutletMobileVerified == null ? false : objEntityUserInDb.OutletMobileVerified.Value) == false)
                    {
                        registerOutlet objRegisterOutlet = new registerOutlet();
                        objRegisterOutlet.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                        objRegisterOutlet.request = new registerOutletRequest();
                        objRegisterOutlet.request.mobile = objEntityUserInDb.Mobile;
                        objRegisterOutlet.request.email = objEntityUserInDb.EmailId;
                        objRegisterOutlet.request.company = objEntityUserInDb.PersonName;
                        objRegisterOutlet.request.name = objEntityUserInDb.PersonName;
                        objRegisterOutlet.request.pan = objEntityUserInDb.PanCardNo;
                        objRegisterOutlet.request.pincode = objEntityUserInDb.PinCode;
                        objRegisterOutlet.request.address = objEntityUserInDb.Address1;
                        objRegisterOutlet.request.otp = mobileOTP;

                        IRestResponse IRegistrationOTPResponse = registerOutlet(objRegisterOutlet);
                        if (IRegistrationOTPResponse.StatusCode == HttpStatusCode.OK)
                        {
                            registerOutletResponse objRegisterOutletResponse = JsonConvert.DeserializeObject<registerOutletResponse>(IRegistrationOTPResponse.Content);
                            if (objRegisterOutletResponse.statuscode == "TXN")
                            {
                                if (updateEntityKYC_Outlet_Registration(objUserIdentity.entityId, objRegisterOutletResponse.data.outlet_id))
                                {
                                    return Ok(true);
                                }
                                else
                                {
                                    return BadRequest("KYC Document submitted with exception.");
                                }
                            }
                            else
                            {
                                return BadRequest(objRegisterOutletResponse.status);
                            }
                        }
                        else
                        {
                            return BadRequest(IRegistrationOTPResponse.StatusCode.ToString());
                        }
                    }
                    else
                        return Ok();
                }
                else
                {
                    return BadRequest("Invalid User");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Route("outletGetKYCDocumentStatus")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult outletGetKYCDocumentStatus()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                QryEntityKYC objEntityUserInDb = db.QryEntityKYC.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
                if (objEntityUserInDb != null)
                {
                    uploadOutletDocument objUploadOutletDocument = new uploadOutletDocument();
                    objUploadOutletDocument.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    requiredDocs objRequiredDocs = new requiredDocs();
                    objRequiredDocs.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objRequiredDocs.request = new requiredDocsRequest();
                    objRequiredDocs.request.outletid = objEntityUserInDb.OutletId;
                    objRequiredDocs.request.pan_no = objEntityUserInDb.PanCardNo;
                    IRestResponse IRequiredDocsResponse = getKYCStatus(objRequiredDocs);
                    if (IRequiredDocsResponse.StatusCode == HttpStatusCode.OK)
                    {
                        requiredDocsResponse objRequiredDocsResponse = JsonConvert.DeserializeObject<requiredDocsResponse>(IRequiredDocsResponse.Content);
                        if (objRequiredDocsResponse.statuscode == "TXN")
                        {
                            if (objRequiredDocsResponse.data.REQUIRED.Length > 0)
                            {
                                for (int i = 0; i < objRequiredDocsResponse.data.REQUIRED.Length; i++)
                                {
                                    string documentRequired = objRequiredDocsResponse.data.REQUIRED[i][2];
                                    if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("pan card"))
                                    {
                                        objUploadOutletDocument.request = new uploadOutletDocumentRequest();
                                        objUploadOutletDocument.request.outletid = objEntityUserInDb.OutletId;
                                        objUploadOutletDocument.request.pan_no = objEntityUserInDb.PanCardNo;
                                        objUploadOutletDocument.request.document = new uploadOutletDocuments();
                                        objUploadOutletDocument.request.document.id = objRequiredDocsResponse.data.REQUIRED[i][0];
                                        objUploadOutletDocument.request.document.filename = objEntityUserInDb.PanCardPhoto;
                                        objUploadOutletDocument.request.document.link = (objEntityUserInDb.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["APIDomainName"] : ConfigurationManager.AppSettings["domainName"]) + "KYCImages/" + objEntityUserInDb.PanCardPhoto;
                                        IRestResponse uploadOutletDocumentResponse = uploadOutletDocuments(objUploadOutletDocument);
                                        db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_PanDocumentId] @EntityKYCId={0}, @PanDocumentId={1}", objEntityUserInDb.EntityKYCId, objRequiredDocsResponse.data.REQUIRED[i][0]);
                                    }
                                    else if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("address proof"))
                                    {
                                        objUploadOutletDocument.request = new uploadOutletDocumentRequest();
                                        objUploadOutletDocument.request.outletid = objEntityUserInDb.OutletId;
                                        objUploadOutletDocument.request.pan_no = objEntityUserInDb.PanCardNo;
                                        objUploadOutletDocument.request.document = new uploadOutletDocuments();
                                        objUploadOutletDocument.request.document.id = objRequiredDocsResponse.data.REQUIRED[i][0];
                                        objUploadOutletDocument.request.document.filename = objEntityUserInDb.AadhaarCardPhoto;
                                        objUploadOutletDocument.request.document.link = (objEntityUserInDb.FromMobileDevice.Value == true ? ConfigurationManager.AppSettings["APIDomainName"] : ConfigurationManager.AppSettings["domainName"]) + "KYCImages/" + objEntityUserInDb.AadhaarCardPhoto;
                                        IRestResponse uploadOutletDocumentResponse = uploadOutletDocuments(objUploadOutletDocument);
                                        db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_AadhaarDocumentId] @EntityKYCId={0}, @AadhaarCardDocumentId={1}", objEntityUserInDb.EntityKYCId, objRequiredDocsResponse.data.REQUIRED[i][0]);
                                    }
                                }
                            }
                            if (objRequiredDocsResponse.data.APPROVED.Length > 0)
                            {
                                db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_KYCStatus] @EntityKYCId={0}", objEntityUserInDb.EntityKYCId);
                            }
                            return Ok(true);
                        }
                        else
                        {
                            return BadRequest(objRequiredDocsResponse.status);
                        }
                    }
                    else
                    {
                        return BadRequest(IRequiredDocsResponse.StatusCode.ToString());
                    }
                }
                else
                {
                    return BadRequest("Invalid User");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Route("outletGetKYCDocumentApprovalStatus")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult outletGetKYCDocumentApprovalStatus()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                QryEntityKYC objEntityUserInDb = db.QryEntityKYC.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
                if (objEntityUserInDb != null)
                {
                    uploadOutletDocument objUploadOutletDocument = new uploadOutletDocument();
                    objUploadOutletDocument.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    requiredDocs objRequiredDocs = new requiredDocs();
                    objRequiredDocs.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
                    objRequiredDocs.request = new requiredDocsRequest();
                    objRequiredDocs.request.outletid = objEntityUserInDb.OutletId;
                    objRequiredDocs.request.pan_no = objEntityUserInDb.PanCardNo;
                    IRestResponse IRequiredDocsResponse = getKYCStatus(objRequiredDocs);
                    if (IRequiredDocsResponse.StatusCode == HttpStatusCode.OK)
                    {
                        outletKYCStatus objOutletKYCStatus = new outletKYCStatus();
                        requiredDocsResponse objRequiredDocsResponse = JsonConvert.DeserializeObject<requiredDocsResponse>(IRequiredDocsResponse.Content);
                        if (objRequiredDocsResponse.statuscode == "TXN")
                        {
                            if (objRequiredDocsResponse.status == "Transaction Successful")
                            {
                                if (objRequiredDocsResponse.data.APPROVED.Length > 0)
                                {
                                    objOutletKYCStatus.StatusCode = "Approved";
                                    objOutletKYCStatus.PanCardRequired = false;
                                    objOutletKYCStatus.AadhaarCardRequired = false;
                                }
                                if (objRequiredDocsResponse.data.SCREENING.Length > 0)
                                {
                                    objOutletKYCStatus.StatusCode = "Screening";
                                    objOutletKYCStatus.PanCardRequired = false;
                                    objOutletKYCStatus.AadhaarCardRequired = false;
                                }
                                if (objRequiredDocsResponse.data.REQUIRED.Length > 0)
                                {
                                    if (String.IsNullOrEmpty(objOutletKYCStatus.StatusCode))
                                        objOutletKYCStatus.StatusCode = "Required";
                                    for (int i = 0; i < objRequiredDocsResponse.data.REQUIRED.Length; i++)
                                    {
                                        if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("pan card"))
                                        {
                                            if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][2]).Trim().ToUpper() == "OPTIONAL")
                                                objOutletKYCStatus.PanCardRequired = false;
                                            else
                                            {
                                                objOutletKYCStatus.StatusCode = "Required";
                                                objOutletKYCStatus.PanCardRequired = true;
                                            }
                                        }
                                        if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("address proof"))
                                        {
                                            if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][2]).Trim().ToUpper() == "OPTIONAL")
                                                objOutletKYCStatus.AadhaarCardRequired = false;
                                            else
                                                objOutletKYCStatus.AadhaarCardRequired = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                objOutletKYCStatus.StatusCode = "Required";
                                objOutletKYCStatus.PanCardRequired = true;
                                objOutletKYCStatus.AadhaarCardRequired = true;
                            }
                            return Ok(objOutletKYCStatus);
                        }
                        else
                        {
                            return BadRequest(objRequiredDocsResponse.statuscode);
                        }
                    }
                    else
                    {
                        return BadRequest(IRequiredDocsResponse.StatusCode.ToString());
                    }
                }
                else
                {
                    return BadRequest("Invalid User");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        //[Route("outletUploadDocuments")]
        //[HttpGet]
        //[Authorize]
        //public IHttpActionResult outletUploadDocuments(string outletId, string panCard, string documentId, string documentLink, string documentFileName)
        //{
        //    var identity = (ClaimsIdentity)User.Identity;
        //    if (identity.IsAuthenticated)
        //    {
        //        userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
        //        QryEntityKYC objEntityUserInDb = db.QryEntityKYC.Where(x => x.EntityId == objUserIdentity.entityId).SingleOrDefault();
        //        if (objEntityUserInDb != null)
        //        {
        //            if (objEntityUserInDb.OutletMobileVerified.Value == false)
        //            {

        //            }
        //            else
        //                return Ok(true);
        //        }
        //        else
        //        {
        //            return BadRequest("Invalid User");
        //        }
        //    }
        //    else
        //    {
        //        return Unauthorized();
        //    }
        //}


        public Boolean updateEntityKYC_Outlet_Registration(long entityId, string outletId)
        {
            tblEntity_KYC objEntityKYC = db.tblEntity_KYC.Where(x => x.EntityId == entityId).SingleOrDefault();
            if (objEntityKYC != null)
            {
                objEntityKYC.OutletId = outletId;
                objEntityKYC.OutletMobileVerified = true;
                db.Entry(objEntityKYC).State = EntityState.Modified;
                db.Entry(objEntityKYC).Property(x => x.EntityId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.IsKYCStatus).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.KYCStatusRemark).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AdminLoginId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.FromMobileDevice).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.PanDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.PanDocumentId).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.AadhaarCardDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.GSTDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.GSTDocumentId).IsModified = false;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public Boolean updateEntityKYC_Outlet_DocumentStatus_Response(long entityId, string OutletDocumentGetStatusResponse)
        {
            tblEntity_KYC objEntityKYC = db.tblEntity_KYC.Where(x => x.EntityId == entityId).SingleOrDefault();
            if (objEntityKYC != null)
            {
                objEntityKYC.OutletDocumentGetStatusResponse = OutletDocumentGetStatusResponse;
                db.Entry(objEntityKYC).State = EntityState.Modified;
                db.Entry(objEntityKYC).Property(x => x.EntityId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.IsKYCStatus).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.KYCStatusRemark).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AdminLoginId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.FromMobileDevice).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanDocumentId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTDocumentId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.OutletId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.OutletMobileVerified).IsModified = false;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public static byte[] Combine(byte[] first, byte[] second, byte[] third)
        {
            byte[] ret = new byte[first.Length + second.Length + third.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            Buffer.BlockCopy(third, 0, ret, first.Length + second.Length,
                            third.Length);
            return ret;
        }

        #region User Onboarding

        protected IRestResponse registrationOTPForOutet(registrationOTP objRegistrationOTP)
        {
            IRestResponse response;
            string URL = "outlet/registrationOTP";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objRegistrationOTP);
            return response;
        }

        protected IRestResponse registerOutlet(registerOutlet objRegisterOutlet)
        {
            IRestResponse response;
            string URL = "outlet/registration";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objRegisterOutlet);
            return response;
        }

        protected IRestResponse getKYCStatus(requiredDocs objRequiredDocs)
        {
            IRestResponse response;
            string URL = "outlet/requiredDocs";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objRequiredDocs);
            return response;
        }

        protected IRestResponse uploadOutletDocuments(uploadOutletDocument objUploadOutletDocuments)
        {
            IRestResponse response;
            string URL = "outlet/uploadDocs";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objUploadOutletDocuments);
            return response;
        }

        #endregion
    }
}
