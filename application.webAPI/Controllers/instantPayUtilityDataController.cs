﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class instantPayUtilityDataController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/instantPayUtilityData
        [Authorize]
        [ResponseType(typeof(instantPayUtilityDataVM))]
        [HttpGet]
        public IHttpActionResult getInstantPayUtilityData()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                instantPayUtilityDataVM objInstantPayUtilityDataVM = new instantPayUtilityDataVM();
                if (entityType == helper.defaultCompany)
                {
                    objInstantPayUtilityDataVM.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == true).ToList();
                    objInstantPayUtilityDataVM.objInstantPayUtilityData = new tblInstantPay_UtilityData();
                    return Ok(objInstantPayUtilityDataVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/instantPayUtilityData/5
        [Authorize]
        [ResponseType(typeof(tblInstantPay_UtilityData))]
        [HttpGet]
        public IHttpActionResult getInstantPayUtilityDataById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblInstantPay_UtilityData objInstantPayUtilityData = db.tblInstantPay_UtilityData.Find(id);
                if (objInstantPayUtilityData == null)
                {
                    return NotFound();
                }
                return Ok(objInstantPayUtilityData);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(instantPayUtilityDataVM))]
        [HttpGet]
        public IHttpActionResult instantPayUtilityDataList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    instantPayUtilityDataVM objInstantPayUtilityDataVM = new instantPayUtilityDataVM();
                    objInstantPayUtilityDataVM.objInstantPayUtilityDataList = db.QryInstantPay_UtilityData.Where(x => x.CompanyId == companyId).ToList();
                    objInstantPayUtilityDataVM.totalRecords = objInstantPayUtilityDataVM.objInstantPayUtilityDataList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objInstantPayUtilityDataVM.objInstantPayUtilityDataList = objInstantPayUtilityDataVM.objInstantPayUtilityDataList.Where(s => s.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                        objInstantPayUtilityDataVM.filterRecords = objInstantPayUtilityDataVM.objInstantPayUtilityDataList.Count();
                    }
                    else
                        objInstantPayUtilityDataVM.filterRecords = objInstantPayUtilityDataVM.totalRecords;

                    if (sortDirection == "asc")
                        objInstantPayUtilityDataVM.objInstantPayUtilityDataList = objInstantPayUtilityDataVM.objInstantPayUtilityDataList.OrderBy(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_UtilityData>();
                    else
                        objInstantPayUtilityDataVM.objInstantPayUtilityDataList = objInstantPayUtilityDataVM.objInstantPayUtilityDataList.OrderByDescending(s => s.CreatedDateTime).Skip(pageIndex).Take(pageSize).ToList<QryInstantPay_UtilityData>();

                    return Ok(objInstantPayUtilityDataVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/instantPayUtilityData/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult instantPayUtilityDataUpdate(int id, tblInstantPay_UtilityData objInstantPayUtilityData)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objInstantPayUtilityData.InstantPayRechargeDataId)
                {
                    return BadRequest();
                }
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var utilityDataInDb = db.tblInstantPay_UtilityData.Where(x => x.CompanyId == companyId && x.ServiceName == objInstantPayUtilityData.ServiceName && x.ProductId == objInstantPayUtilityData.ProductId && x.ServiceType == objInstantPayUtilityData.ServiceType &&
                x.ServiceKey == objInstantPayUtilityData.ServiceKey && x.InstantPayRechargeDataId != objInstantPayUtilityData.InstantPayRechargeDataId).SingleOrDefault();
                if (utilityDataInDb != null)
                    return BadRequest("Commission data already exists.");

                db.Entry(objInstantPayUtilityData).State = EntityState.Modified;
                db.Entry(objInstantPayUtilityData).Property(x => x.CompanyId).IsModified = false;
                db.Entry(objInstantPayUtilityData).Property(x => x.CreatedDateTime).IsModified = false;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblInstantPay_UtilityDataExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/instantPayUtilityData
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult instantPayUtilityDataSave(tblInstantPay_UtilityData objInstantPayUtilityData)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                if (entityType == helper.defaultCompany)
                {
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    var utilityDataInDb = db.tblInstantPay_UtilityData.Where(x => x.CompanyId == companyId && x.ServiceName == objInstantPayUtilityData.ServiceName && x.ProductId == objInstantPayUtilityData.ProductId && x.ServiceType == objInstantPayUtilityData.ServiceType &&
                x.ServiceKey == objInstantPayUtilityData.ServiceKey && x.InstantPayRechargeDataId != objInstantPayUtilityData.InstantPayRechargeDataId).SingleOrDefault();
                    if (utilityDataInDb != null)
                        return BadRequest("Commission data already exists.");

                    objInstantPayUtilityData.CompanyId = companyId;
                    objInstantPayUtilityData.CreatedDateTime = helper.getIndianTime();
                    db.tblInstantPay_UtilityData.Add(objInstantPayUtilityData);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        // DELETE: api/instantPayUtilityData/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult instantPayUtilityDataDelete(int id)
        {
            tblInstantPay_UtilityData objInstantPayUtilityData = db.tblInstantPay_UtilityData.Find(id);
            if (objInstantPayUtilityData == null)
            {
                return NotFound();
            }

            db.tblInstantPay_UtilityData.Remove(objInstantPayUtilityData);
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblInstantPay_UtilityDataExists(int id)
        {
            return db.tblInstantPay_UtilityData.Count(e => e.InstantPayRechargeDataId == id) > 0;
        }
    }
}