﻿using application.webAPI.Models;
using application.webAPI.paymentGateway;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Xml;

namespace application.webAPI.Controllers
{
    [RoutePrefix("api/paymentGateway")]
    public class paymentGatewayController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        [Authorize]
        [HttpGet]
        [Route("makePaymentRequestURL")]
        public IHttpActionResult makePaymentRequest(int amount, Boolean fromMobileDevice, string IPAddress)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                long entityUserId = objUserIdentity.entityUserId;
                var entityUserInDb = db.tblEntity_Users.Where(x => x.EntityUserId == entityUserId).SingleOrDefault();
                paymentGatewayRequest objPaymentGatewayRequest = new paymentGatewayRequest();
                objPaymentGatewayRequest.TransactionId = db.Database.SqlQuery<string>("EXEC [dbo].[EntityPayment_Get_TransactionId]").FirstOrDefault<string>();
                objPaymentGatewayRequest.Amount = amount * 100;
                objPaymentGatewayRequest.ClientCode = "PPAY";
                objPaymentGatewayRequest.Description = "Payment Request @ " + helper.getIndianTime();
                objPaymentGatewayRequest.Email = (entityUserInDb == null ? "" : entityUserInDb.EmailId);
                objPaymentGatewayRequest.MobileNo = (entityUserInDb == null ? "" : entityUserInDb.Mobile);

                string paymentRequestURL = (ConfigurationManager.AppSettings["paymentGatewayMode"] == "TEST" ? "https://demopg.vicegamez.com/api/Payment/PaymentRequest" : "https://pg.vicegamez.com/api/Payment/PaymentRequest");
                var client = new RestClient(paymentRequestURL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("PrivateKey", (ConfigurationManager.AppSettings["paymentGatewayMode"] == "TEST" ? "zJvwX0YziylwbHY+GjdhhDHi0cKYzXvjaqBCn8mrz1C2y+swuRMpFA==" : ""));
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", request.AddJsonBody(objPaymentGatewayRequest), ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    paymentGatewayResponse objPaymentGatewayResponse = JsonConvert.DeserializeObject<paymentGatewayResponse>(response.Content);
                    savePaymentRequest(objPaymentGatewayRequest, objUserIdentity, paymentRequestURL, objPaymentGatewayResponse.status.returnMessage, "", fromMobileDevice, IPAddress);
                    if (objPaymentGatewayResponse.status.code == 0)
                    {
                        return Ok(objPaymentGatewayResponse.Url);
                    }
                    else
                        return BadRequest(objPaymentGatewayResponse.status.returnMessage);
                }
                else
                    return BadRequest(response.StatusCode + "-" + response.StatusDescription);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected void savePaymentRequest(paymentGatewayRequest objPaymentGatewayRequest, userIdentity objUserIdentity, string paymentRequestURL, string paymentRequestMsg, string paymentRequestString, Boolean fromMobileDevice, string IPAddress)
        {
            tblEntity_Payments objEntityPayments = new tblEntity_Payments();
            objEntityPayments.EntityId = objUserIdentity.entityId;
            objEntityPayments.ParentEntityId = 2;
            objEntityPayments.PaymentModeId = 1;
            objEntityPayments.Amount = objPaymentGatewayRequest.Amount / 100;
            objEntityPayments.BankId = 0;
            objEntityPayments.BankBranchCode = string.Empty;
            objEntityPayments.BankTransactionId = string.Empty;
            objEntityPayments.ChequeNo = string.Empty;
            objEntityPayments.PaymentSlip = string.Empty;
            objEntityPayments.Remark = objPaymentGatewayRequest.Description;
            objEntityPayments.CreatedDateTime = helper.getIndianTime();
            objEntityPayments.IsPaymentReceived = false;
            objEntityPayments.IsPaymentCredited2Wallet = false;
            objEntityPayments.IsPaymentMade2Company = true;
            objEntityPayments.IsPaymentMade2Company = true;
            objEntityPayments.PaymentReceivedDateTime = (DateTime?)null;
            objEntityPayments.PaymentCredited2WalletDateTime = (DateTime?)null;
            objEntityPayments.EntityUserId = objUserIdentity.entityUserId;
            objEntityPayments.ParentEntityUserId = 0;
            objEntityPayments.RejectionRemark = string.Empty;
            objEntityPayments.RejectionDateTime = (DateTime?)null;
            objEntityPayments.AccountingEntryStatus = string.Empty;
            objEntityPayments.WalletUpdateStatus = string.Empty;
            objEntityPayments.WalletUpdateStatus = string.Empty;
            objEntityPayments.FromMobileDevice = fromMobileDevice;
            objEntityPayments.PaymentTransactionId = objPaymentGatewayRequest.TransactionId;
            objEntityPayments.ClientCode = objPaymentGatewayRequest.ClientCode;
            objEntityPayments.PaymentRequestURL = paymentRequestURL;
            objEntityPayments.PaymentRequestMessage = paymentRequestMsg;
            objEntityPayments.PaymentRequestString = paymentRequestString;
            objEntityPayments.IPAddress = IPAddress;
            db.tblEntity_Payments.Add(objEntityPayments);
            db.SaveChanges();
        }
    }
}
