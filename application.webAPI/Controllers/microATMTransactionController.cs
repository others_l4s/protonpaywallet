﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.bankit.microatm.response;
using application.webAPI.bankit.microatm.request;

namespace application.webAPI.Controllers
{
    public class microATMTransactionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/microATMTransaction
        [ResponseType(typeof(microATMResponse))]
        [Route("api/microATMCreditWallet")]
        [HttpPost]
        public IHttpActionResult microATMCreditWallet(microATMRequest objRequest)
        {
            var re = Request;
            var headers = re.Headers;
            string token = "";
            microATMResponse objResponse = new microATMResponse();
            if (headers.Contains("developer_key"))
            {
                token = headers.GetValues("developer_key").First();
                if (token == "3HLA6m3xkr4oSm5P4bpR")
                {
                    var entityByRetailerIdInDb = db.tblBankIt_MicroATM_Request.Where(x => x.partnerAgentId.Trim() == objRequest.partnerAgentId.Trim()).Take(1).SingleOrDefault();
                    if (entityByRetailerIdInDb == null)
                    {
                        objResponse.Status = "1";
                        objResponse.Description = "partnerAgentId not mapped in database.";
                    }
                    else
                    {
                        long entityUserId = 0;
                        int companyId = 0;
                        Boolean transactionFound = false;
                        var txnIdInTransactionDB = new tblBankIT_MicroATM_Transaction();
                        tblBankIT_MicroATM_Transaction objTransaction = new tblBankIT_MicroATM_Transaction();
                        if (objRequest.txnStatus.Trim().ToLower() == "success" && objRequest.errorCode.Trim().ToLower() == "0")
                        {
                            var entityLeadUser = db.QryEntityUser.Where(x => x.EntityId == entityByRetailerIdInDb.EntityId && x.LeadUser == true).Take(1).SingleOrDefault();
                            if (entityLeadUser != null)
                            {
                                entityUserId = entityLeadUser.EntityUserId;
                                companyId = entityLeadUser.CompanyId;
                            }
                            long entityId = entityByRetailerIdInDb.EntityId;
                            txnIdInTransactionDB = db.tblBankIT_MicroATM_Transaction.Where(x => x.ClientRefID == objRequest.ClientRefID && x.EntityId == entityId).Take(1).SingleOrDefault();
                            if (txnIdInTransactionDB != null)
                            {
                                transactionFound = true;
                                objTransaction.AmountCredited = txnIdInTransactionDB.AmountCredited;
                            }
                        }
                        else { objTransaction.AmountCredited = false; }

                        if (objTransaction.AmountCredited == false && Convert.ToDecimal(objRequest.amount) > 0)
                        {
                            helper.processEntityCommissionNTaxes_BANKITDMT(Convert.ToDecimal(objRequest.amount), entityByRetailerIdInDb.EntityId, entityUserId, objRequest.ClientRefID, companyId, 6, true);
                        }
                        if (!transactionFound)
                        {
                            objTransaction.RRN = objRequest.RRN == null ? string.Empty : objRequest.RRN;
                            objTransaction.partnerAgentId = objRequest.partnerAgentId;
                            objTransaction.@operator = objRequest.Operator;
                            objTransaction.ClientRefID = objRequest.ClientRefID;
                            objTransaction.txnStatus = objRequest.txnStatus;
                            objTransaction.amount = Convert.ToDecimal(objRequest.amount);
                            objTransaction.CreatedDateTime = helper.getIndianTime();
                            objTransaction.EntityId = entityByRetailerIdInDb.EntityId;
                            objTransaction.errorCode = objRequest.errorCode;
                            objTransaction.service = objRequest.service;
                            db.tblBankIT_MicroATM_Transaction.Add(objTransaction);
                        }
                        else
                        {
                            db.Entry(txnIdInTransactionDB).State = EntityState.Modified;
                            txnIdInTransactionDB.AmountCredited = true;
                        }
                        db.SaveChanges();
                        objResponse.Status = "0";
                        objResponse.Description = "Transaction Success";
                    }
                    return Ok(objResponse);
                }
                else
                {
                    return BadRequest("Invalid developer_key");
                }
            }
            else
            {
                return BadRequest("developer_key missing");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}