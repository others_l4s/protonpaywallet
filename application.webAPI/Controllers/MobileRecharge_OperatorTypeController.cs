﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.WebPages;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class MobileRecharge_OperatorTypeController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/MobileRecharge_OperatorType/5
        [Authorize]
        public IHttpActionResult GettblMobileRecharge_OperatorType(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_OperatorType objMobileRecharge_OperatorType = db.tblMobileRecharge_OperatorType.Find(id);
                    if (objMobileRecharge_OperatorType == null)
                    {
                        return NotFound();
                    }

                    return Ok(objMobileRecharge_OperatorType);
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
                return Unauthorized();

        }

        [Authorize]
        [ResponseType(typeof(mobileRechargeOperatorTypeVM))]
        [HttpGet]
        public IHttpActionResult getOperatorTypeList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeOperatorTypeVM objMobileRechargeOperatorTypeVM = new mobileRechargeOperatorTypeVM();
                    objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList = db.tblMobileRecharge_OperatorType.ToList();
                    objMobileRechargeOperatorTypeVM.totalRecords = objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList = objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList.Where(s => s.OperatorTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objMobileRechargeOperatorTypeVM.filterRecords = objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList.Count();
                    }
                    else
                        objMobileRechargeOperatorTypeVM.filterRecords = objMobileRechargeOperatorTypeVM.totalRecords;

                    if (sortDirection == "asc")
                        objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList = objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList.OrderBy(s => s.OperatorTypeName).Skip(pageIndex).Take(pageSize).ToList<tblMobileRecharge_OperatorType>();
                    else
                        objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList = objMobileRechargeOperatorTypeVM.objMobileRechargeOperatorTypeList.OrderByDescending(s => s.OperatorTypeName).Skip(pageIndex).Take(pageSize).ToList<tblMobileRecharge_OperatorType>();

                    return Ok(objMobileRechargeOperatorTypeVM);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // PUT: api/MobileRecharge_OperatorType/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateOperatorType(long id, tblMobileRecharge_OperatorType objMobileRecharge_OperatorType)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objMobileRecharge_OperatorType.OperatorTypeId)
                    {
                        return BadRequest();
                    }

                    var operatorTypeInDb = db.tblMobileRecharge_OperatorType.SingleOrDefault(x => x.OperatorTypeId != objMobileRecharge_OperatorType.OperatorTypeId && x.OperatorTypeName == objMobileRecharge_OperatorType.OperatorTypeName);
                    if (operatorTypeInDb != null)
                        return BadRequest("Operator Type already found!");

                    db.Entry(objMobileRecharge_OperatorType).State = EntityState.Modified;
                    if (String.IsNullOrEmpty(objMobileRecharge_OperatorType.OperatorTypeImage))
                        db.Entry(objMobileRecharge_OperatorType).Property(x => x.OperatorTypeImage).IsModified = false;
                    db.Entry(objMobileRecharge_OperatorType).Property(x => x.CreatedDateTime).IsModified = false;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblMobileRecharge_OperatorTypeExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
                return Unauthorized();

        }

        // POST: api/MobileRecharge_OperatorType
        [Authorize]
        [HttpPost]
        public IHttpActionResult saveOperatorType(tblMobileRecharge_OperatorType objMobileRecharge_OperatorType)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var operatorTypeInDb = db.tblMobileRecharge_OperatorType.SingleOrDefault(x => x.OperatorTypeId != objMobileRecharge_OperatorType.OperatorTypeId && x.OperatorTypeName == objMobileRecharge_OperatorType.OperatorTypeName);
                    if (operatorTypeInDb != null)
                        return BadRequest("Operator Type already found");

                    objMobileRecharge_OperatorType.CreatedDateTime = helper.getIndianTime();
                    db.tblMobileRecharge_OperatorType.Add(objMobileRecharge_OperatorType);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/MobileRecharge_OperatorType/5
        [Authorize]
        public IHttpActionResult deleteOperatorType(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_OperatorType objMobileRecharge_OperatorType = db.tblMobileRecharge_OperatorType.Find(id);
                    if (objMobileRecharge_OperatorType == null)
                    {
                        return NotFound();
                    }
                    db.tblMobileRecharge_OperatorType.Remove(objMobileRecharge_OperatorType);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMobileRecharge_OperatorTypeExists(long id)
        {
            return db.tblMobileRecharge_OperatorType.Count(e => e.OperatorTypeId == id) > 0;
        }
    }
}