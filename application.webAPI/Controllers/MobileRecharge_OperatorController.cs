﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class MobileRecharge_OperatorController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/MobileRecharge_Operator
        [Authorize]
        [HttpGet]
        public IHttpActionResult getMobileRechargeOperatorDetail()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeOperatorVM objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
                    objMobileRechargeOperatorVM.objMobileRechargeOperatorTypeList = db.tblMobileRecharge_OperatorType.ToList();
                    objMobileRechargeOperatorVM.objMobileRechargeOperatorList = new List<QryMobileRecharge_Operator>();
                    objMobileRechargeOperatorVM.objMobileRechargeOperator = new tblMobileRecharge_Operator();
                    return Ok(objMobileRechargeOperatorVM);
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/MobileRecharge_Operator/5
        [Authorize]
        [HttpGet]
        public IHttpActionResult getMobileRechargeOperatorbyId(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeOperatorVM objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
                    objMobileRechargeOperatorVM.objMobileRechargeOperatorTypeList = db.tblMobileRecharge_OperatorType.ToList();
                    objMobileRechargeOperatorVM.objMobileRechargeOperatorList = new List<QryMobileRecharge_Operator>();
                    objMobileRechargeOperatorVM.objMobileRechargeOperator = db.tblMobileRecharge_Operator.Find(id);
                    if (objMobileRechargeOperatorVM.objMobileRechargeOperator == null)
                    {
                        return NotFound();
                    }

                    return Ok(objMobileRechargeOperatorVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/MobileRecharge_Operator/5
        [Authorize]
        [HttpPut]
        public IHttpActionResult updateMobileRechargeOperator(long id, tblMobileRecharge_Operator objMobileRecharge_Operator)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objMobileRecharge_Operator.OperatorId)
                    {
                        return BadRequest();
                    }

                    var operatorInDb = db.tblMobileRecharge_Operator.SingleOrDefault(x => x.OperatorId != id && x.OperatorTypeId == objMobileRecharge_Operator.OperatorTypeId && x.OperatorName == objMobileRecharge_Operator.OperatorName);
                    if (operatorInDb != null)
                        return BadRequest("Operator detail already found");

                    db.Entry(objMobileRecharge_Operator).State = EntityState.Modified;
                    db.Entry(objMobileRecharge_Operator).Property(x => x.CreatedDateTime).IsModified = false;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblMobileRecharge_OperatorExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        [Authorize]
        [ResponseType(typeof(mobileRechargeOperatorVM))]
        [HttpGet]
        public IHttpActionResult getOperatorList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeOperatorVM objMobileRechargeOperatorVM = new mobileRechargeOperatorVM();
                    objMobileRechargeOperatorVM.objMobileRechargeOperatorList = db.QryMobileRecharge_Operator.ToList();
                    objMobileRechargeOperatorVM.totalRecords = objMobileRechargeOperatorVM.objMobileRechargeOperatorList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMobileRechargeOperatorVM.objMobileRechargeOperatorList = objMobileRechargeOperatorVM.objMobileRechargeOperatorList.Where(s => s.OperatorName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        s.OperatorTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objMobileRechargeOperatorVM.filterRecords = objMobileRechargeOperatorVM.objMobileRechargeOperatorList.Count();
                    }
                    else
                        objMobileRechargeOperatorVM.filterRecords = objMobileRechargeOperatorVM.totalRecords;

                    if (sortDirection == "asc")
                        objMobileRechargeOperatorVM.objMobileRechargeOperatorList = objMobileRechargeOperatorVM.objMobileRechargeOperatorList.OrderBy(s => s.OperatorName).Skip(pageIndex).Take(pageSize).ToList<QryMobileRecharge_Operator>();
                    else
                        objMobileRechargeOperatorVM.objMobileRechargeOperatorList = objMobileRechargeOperatorVM.objMobileRechargeOperatorList.OrderByDescending(s => s.OperatorName).Skip(pageIndex).Take(pageSize).ToList<QryMobileRecharge_Operator>();

                    return Ok(objMobileRechargeOperatorVM);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }

        // POST: api/MobileRecharge_Operator
        [Authorize]
        [HttpPost]
        public IHttpActionResult saveMobileRechargeOperator(tblMobileRecharge_Operator objMobileRecharge_Operator)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var operatorInDb = db.tblMobileRecharge_Operator.SingleOrDefault(x => x.OperatorId != objMobileRecharge_Operator.OperatorId && x.OperatorTypeId == objMobileRecharge_Operator.OperatorTypeId && x.OperatorName == objMobileRecharge_Operator.OperatorName);
                    if (operatorInDb != null)
                        return BadRequest("Operator detail already found");

                    objMobileRecharge_Operator.CreatedDateTime = helper.getIndianTime();
                    db.tblMobileRecharge_Operator.Add(objMobileRecharge_Operator);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/MobileRecharge_Operator/5
        [Authorize]
        [HttpDelete]
        public IHttpActionResult deleteMobileRechargeOperator(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_Operator objMobileRecharge_Operator = db.tblMobileRecharge_Operator.Find(id);
                    if (objMobileRecharge_Operator == null)
                    {
                        return NotFound();
                    }

                    db.tblMobileRecharge_Operator.Remove(objMobileRecharge_Operator);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMobileRecharge_OperatorExists(long id)
        {
            return db.tblMobileRecharge_Operator.Count(e => e.OperatorId == id) > 0;
        }
    }
}