﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class HelplineController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();


        // GET: api/Helpline
        [Authorize]
        [ResponseType(typeof(helpLineViewModel))]
        [HttpGet]
        public IHttpActionResult getHelpLineList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                helpLineViewModel objhelplinenumber = new helpLineViewModel()
                {
                    objHelpLine = new tblHelpline_Number(),
                    objHelpLineList = db.QryHelpLineMaster.ToList(),
                    objStatusList = db.tblStatus_Master.ToList(),
                    objProductList = db.tblProduct_Master.ToList()
                };
                return Ok(objhelplinenumber);
            }
            else
                return Unauthorized();
        }

        // GET: api/Helpline/5
        [Authorize]
        [ResponseType(typeof(tblHelpline_Number))]
        public IHttpActionResult getHelpLineNumberById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblHelpline_Number tblHelpline_Number = db.tblHelpline_Number.Find(id);
                if (tblHelpline_Number == null)
                {
                    return NotFound();
                }

                return Ok(tblHelpline_Number);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(helpLineViewModel))]
        [HttpGet]
        public IHttpActionResult gethelpLineNumberListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                helpLineViewModel objhelpLineViewModel = new helpLineViewModel();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                    objhelpLineViewModel.totalRecords = db.QryHelpLineMaster.Where(c => c.CompanyId == CompanyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objhelpLineViewModel.objHelpLineList = db.QryHelpLineMaster.Where(h => h.CompanyId == CompanyId && (h.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || h.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || h.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objhelpLineViewModel.filterRecords = objhelpLineViewModel.objHelpLineList.Count();
                    }
                    else
                        objhelpLineViewModel.filterRecords = objhelpLineViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objhelpLineViewModel.objHelpLineList = String.IsNullOrEmpty(searchValue) ?
                            db.QryHelpLineMaster.Where(h => h.CompanyId == CompanyId).OrderBy(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>() :
                            objhelpLineViewModel.objHelpLineList.OrderBy(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>();
                    else
                        objhelpLineViewModel.objHelpLineList = String.IsNullOrEmpty(searchValue) ?
                            db.QryHelpLineMaster.Where(h => h.CompanyId == CompanyId).OrderByDescending(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>() :
                            objhelpLineViewModel.objHelpLineList.OrderByDescending(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>();

                }
                else
                {
                    var entityIdentity = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(entityIdentity.Value);
                    objhelpLineViewModel.totalRecords = db.QryHelpLineMaster.Where(c => c.EntityId == entityId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objhelpLineViewModel.objHelpLineList = db.QryHelpLineMaster.Where(h => h.EntityId == entityId && (h.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || h.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || h.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objhelpLineViewModel.filterRecords = objhelpLineViewModel.objHelpLineList.Count();
                    }
                    else
                        objhelpLineViewModel.filterRecords = objhelpLineViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objhelpLineViewModel.objHelpLineList = String.IsNullOrEmpty(searchValue) ?
                            db.QryHelpLineMaster.Where(h => h.EntityId == entityId).OrderBy(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>() :
                            objhelpLineViewModel.objHelpLineList.OrderBy(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>();
                    else
                        objhelpLineViewModel.objHelpLineList = String.IsNullOrEmpty(searchValue) ?
                            db.QryHelpLineMaster.Where(h => h.EntityId == entityId).OrderByDescending(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>() :
                            objhelpLineViewModel.objHelpLineList.OrderByDescending(h => h.ProductCode).Skip(pageIndex).Take(pageSize).ToList<QryHelpLineMaster>();


                }
                return Ok(objhelpLineViewModel);

            }
            else
                return Unauthorized();
        }


        // PUT: api/Helpline/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateHelpLineNumber(int id, tblHelpline_Number objHelLine)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objHelLine.HelplineId)
                {
                    return BadRequest();
                }
                try
                {
                    var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                    if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                    {
                        var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                        int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                        var HelpLineInDb = db.tblHelpline_Number.SingleOrDefault(h => h.ProductId == objHelLine.ProductId && h.CompanyId == CompanyId && h.HelplineId != objHelLine.HelplineId);
                        if (HelpLineInDb != null)
                            return BadRequest("HelpLineNumber already exists");
                    }
                    else
                    {
                        var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                        long entityId = Convert.ToInt64(identityEntityId.Value);
                        var HelpLineInDb = db.tblHelpline_Number.SingleOrDefault(h => h.ProductId == objHelLine.ProductId && h.EntityId == entityId && h.HelplineId != objHelLine.HelplineId);
                        if (HelpLineInDb != null)
                            return BadRequest("HelpLineNumber already exists");
                    }


                    db.Entry(objHelLine).State = EntityState.Modified;
                    db.Entry(objHelLine).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(objHelLine).Property(x => x.CompanyId).IsModified = false;
                    db.Entry(objHelLine).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objHelLine).Property(x => x.ForCompany2MD).IsModified = false;

                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblHelpline_NumberExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok(objHelLine);
            }
            else
                return Unauthorized();
        }

        // POST: api/Helpline
        [Authorize]
        [ResponseType(typeof(tblHelpline_Number))]
        [HttpPost]
        public IHttpActionResult saveHelpLineNumber(tblHelpline_Number objHelLine)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var HelpLineInDb = db.tblHelpline_Number.SingleOrDefault(h => h.ProductId == objHelLine.ProductId && h.CompanyId == CompanyId && h.HelplineId != objHelLine.HelplineId);
                    if (HelpLineInDb != null)
                        return BadRequest("HelpLineNumber already exists");

                    objHelLine.CompanyId = CompanyId;
                    objHelLine.CreatedBy = Convert.ToInt32(identityAdminLoginId.Value);
                    objHelLine.EntityId = 0;
                    objHelLine.ForCompany2MD = true;
                }
                else
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    var HelpLineInDb = db.tblHelpline_Number.SingleOrDefault(h => h.ProductId == objHelLine.ProductId && h.EntityId == entityId && h.HelplineId != objHelLine.HelplineId);
                    if (HelpLineInDb != null)
                        return BadRequest("HelpLineNumber already exists");

                    objHelLine.CompanyId = 0;
                    objHelLine.ForCompany2MD = false;
                    objHelLine.EntityId = entityId;
                    objHelLine.CreatedBy = Convert.ToInt32(identityEntityUserId.Value);
                }
                db.tblHelpline_Number.Add(objHelLine);
                db.SaveChanges();
                return Ok(objHelLine);
            }
            else
                return Unauthorized();
        }

        // DELETE: api/Helpline/5
        [Authorize]
        [ResponseType(typeof(tblHelpline_Number))]
        public IHttpActionResult deleteHelpLine(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblHelpline_Number objHelLine = db.tblHelpline_Number.Find(id);
                if (objHelLine == null)
                {
                    return NotFound();
                }

                db.tblHelpline_Number.Remove(objHelLine);
                db.SaveChanges();

                return Ok(objHelLine);
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblHelpline_NumberExists(int id)
        {
            return db.tblHelpline_Number.Count(e => e.HelplineId == id) > 0;
        }
    }
}