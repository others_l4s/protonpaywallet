﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;

namespace application.webAPI.Controllers
{
    public class myRoboticRechargeUtilityDataDefaultCommissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/myRoboticRechargeUtilityDataDefaultCommission
        [Authorize]
        [ResponseType(typeof(List<QryMyRobotic_Recharge_Default_Commission>))]
        [HttpGet]
        public IHttpActionResult myRoboticRechargeDefaultCommission()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                List<QryMyRobotic_Recharge_Default_Commission> objDefaultCommission = new List<QryMyRobotic_Recharge_Default_Commission>();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    objDefaultCommission = db.QryMyRobotic_Recharge_Default_Commission.Where(x => x.CompanyId == companyId).OrderBy(x => x.OperatorName).ToList();
                    return Ok(objDefaultCommission);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/myRoboticRechargeUtilityDataDefaultCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult myRoboticRechargeUtilityDataDefaultCommissionSave(tblMyRobotic_Recharge_UtilityData_Commission objDefaultCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    objDefaultCommission.CreatedDateTime = helper.getIndianTime();
                    objDefaultCommission.CreatedBy = adminLoginId;
                    db.tblMyRobotic_Recharge_UtilityData_Commission.Add(objDefaultCommission);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/myRoboticRechargeUtilityDataDefaultCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult DeletetblMyRobotic_Recharge_UtilityData_Commission(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    List<tblMyRobotic_Recharge_UtilityData_Commission> objDefaultCommissionList = db.tblMyRobotic_Recharge_UtilityData_Commission.Where(x => x.IsCustom == false).ToList();
                    if (objDefaultCommissionList == null)
                    {
                        return NotFound();
                    }
                    db.tblMyRobotic_Recharge_UtilityData_Commission.RemoveRange(objDefaultCommissionList);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMyRobotic_Recharge_UtilityData_CommissionExists(long id)
        {
            return db.tblMyRobotic_Recharge_UtilityData_Commission.Count(e => e.MyRoboticRechargeDataCommissionId == id) > 0;
        }
    }
}