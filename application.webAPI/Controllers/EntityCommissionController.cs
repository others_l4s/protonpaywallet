﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class EntityCommissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/EntityCommission
        [Authorize]
        [ResponseType(typeof(entityCommissionViewModel))]
        [Route("api/entityCommission")]
        [HttpGet]
        public IHttpActionResult entityCommission()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                entityCommissionViewModel objentityCommissionViewModel = new entityCommissionViewModel();
                objentityCommissionViewModel.objProductMasterList = db.tblProduct_Master.Where(x => x.SlabCommissionApplicable == false && x.CustomCommissionApplicable == true).ToList();
                objentityCommissionViewModel.objEntityTypeList = db.tblEntity_Type.ToList();
                objentityCommissionViewModel.objCommissionTypeList = db.tblCommissionType.ToList();
                int entityTypeCompany = Convert.ToInt32(helper.entityTypes.company);
                if (entityType != helper.defaultCompany)
                {
                    if (entityType == Convert.ToInt32(helper.entityTypes.masterDistributor))
                        objentityCommissionViewModel.objEntityTypeList = objentityCommissionViewModel.objEntityTypeList.Where(x => x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.distributor) || x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.agent) || x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.masterDistributor)).ToList();
                    if (entityType == Convert.ToInt32(helper.entityTypes.distributor))
                        objentityCommissionViewModel.objEntityTypeList = objentityCommissionViewModel.objEntityTypeList.Where(x => x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.agent) || x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.distributor)).ToList();
                }

                objentityCommissionViewModel.objEntityCommisionMasterList = new List<QryEntityCommission>();
                foreach (tblProduct_Master objProductData in objentityCommissionViewModel.objProductMasterList)
                {
                    foreach (tblEntity_Type objEntityTypeData in objentityCommissionViewModel.objEntityTypeList)
                    {
                        QryEntityCommission objEntityCommission = new QryEntityCommission();
                        objentityCommissionViewModel.clsEntityCommission = db.QryEntityCommission.Where(x => x.CompanyId == companyId && x.ProductId == objProductData.ProductId && x.EntityTypeId == objEntityTypeData.EntityTypeId && x.IsCustom == false && x.EntityId == 0).SingleOrDefault();
                        if (objentityCommissionViewModel.clsEntityCommission == null)
                        {
                            objEntityCommission.CommissionId = 0;
                            objEntityCommission.CompanyId = companyId;
                            objEntityCommission.EntityId = 0;
                            objEntityCommission.CommissionTypeId = 0;
                            objEntityCommission.ProductId = objProductData.ProductId;
                            objEntityCommission.CommissionSlabRangeValue = "";
                            objEntityCommission.IsCustom = false;
                            objEntityCommission.EntityTypeId = objEntityTypeData.EntityTypeId;
                            objEntityCommission.EntityTypeName = objEntityTypeData.EntityTypeName;
                            objentityCommissionViewModel.objEntityCommisionMasterList.Add(objEntityCommission);
                        }
                        else
                        {
                            objentityCommissionViewModel.CommissionTypeId = objentityCommissionViewModel.clsEntityCommission.CommissionTypeId;
                            objentityCommissionViewModel.objEntityCommisionMasterList.Add(objentityCommissionViewModel.clsEntityCommission);
                        }
                    }
                }
                return Ok(objentityCommissionViewModel);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entityCommissionViewModel))]
        [Route("api/entityCommissionCustom")]
        [HttpGet]
        public IHttpActionResult entityCommissionCustom()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt32(identityEntityId.Value);
                entityCommissionViewModel objentityCommissionViewModel = new entityCommissionViewModel();
                objentityCommissionViewModel.objProductMasterList = db.tblProduct_Master.ToList();
                objentityCommissionViewModel.objEntityTypeList = db.tblEntity_Type.ToList();
                objentityCommissionViewModel.objCommissionTypeList = db.tblCommissionType.ToList();
                int entityTypeCompany = Convert.ToInt32(helper.entityTypes.company);
                if (entityType != helper.defaultCompany)
                {
                    if (entityType == Convert.ToInt32(helper.entityTypes.masterDistributor))
                        objentityCommissionViewModel.objEntityTypeList = objentityCommissionViewModel.objEntityTypeList.Where(x => x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.distributor) || x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.agent) || x.EntityTypeName.Trim().ToLower() == "master distributor").ToList();
                    if (entityType == Convert.ToInt32(helper.entityTypes.distributor))
                        objentityCommissionViewModel.objEntityTypeList = objentityCommissionViewModel.objEntityTypeList.Where(x => x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.agent) || x.EntityTypeName.Trim().ToLower() == Convert.ToString(helper.entityTypes.distributor)).ToList();
                }

                objentityCommissionViewModel.objEntityCommisionMasterList = new List<QryEntityCommission>();
                foreach (tblProduct_Master objProductData in objentityCommissionViewModel.objProductMasterList)
                {
                    foreach (tblEntity_Type objEntityTypeData in objentityCommissionViewModel.objEntityTypeList)
                    {
                        QryEntityCommission objEntityCommission = new QryEntityCommission();
                        objentityCommissionViewModel.clsEntityCommission = db.QryEntityCommission.Where(x => x.CompanyId == companyId && x.ProductId == objProductData.ProductId && x.EntityTypeId == objEntityTypeData.EntityTypeId && x.IsCustom == true && x.EntityId == entityId).SingleOrDefault();
                        if (objentityCommissionViewModel.clsEntityCommission == null)
                            objentityCommissionViewModel.clsEntityCommission = db.QryEntityCommission.Where(x => x.CompanyId == companyId && x.ProductId == objProductData.ProductId && x.EntityTypeId == objEntityTypeData.EntityTypeId && x.EntityId == 0).SingleOrDefault();
                        if (objentityCommissionViewModel.clsEntityCommission == null)
                        {
                            objEntityCommission.CommissionId = 0;
                            objEntityCommission.CompanyId = companyId;
                            objEntityCommission.EntityId = 0;
                            objEntityCommission.CommissionTypeId = 0;
                            objEntityCommission.ProductId = objProductData.ProductId;
                            objEntityCommission.CommissionSlabRangeValue = "";
                            objEntityCommission.IsCustom = false;
                            objEntityCommission.EntityTypeId = objEntityTypeData.EntityTypeId;
                            objEntityCommission.EntityTypeName = objEntityTypeData.EntityTypeName;
                            objentityCommissionViewModel.objEntityCommisionMasterList.Add(objEntityCommission);
                        }
                        else
                        {
                            objentityCommissionViewModel.CommissionTypeId = objentityCommissionViewModel.clsEntityCommission.CommissionTypeId;
                            objentityCommissionViewModel.objEntityCommisionMasterList.Add(objentityCommissionViewModel.clsEntityCommission);
                        }
                    }
                }
                return Ok(objentityCommissionViewModel);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entityCommissionViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityCommissionByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);

                    entityCommissionViewModel objentityCommissionViewModel = new entityCommissionViewModel();
                    objentityCommissionViewModel.totalRecords = db.QryEntityCommission.Where(c => c.CompanyId == companyId).Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objentityCommissionViewModel.objEntityCommisionMasterList = db.QryEntityCommission.Where(e => e.CompanyId == companyId && (e.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || e.ProductCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || e.CommissionValue.ToString().Contains(searchValue.ToString())
                        || e.CompanyName.ToString().Contains(searchValue.ToString())
                        || e.CommissionTypeName.Trim().ToLower().Contains(searchValue.Trim().ToLower()))).ToList();

                        objentityCommissionViewModel.filterRecords = objentityCommissionViewModel.objEntityCommisionMasterList.Count();
                    }
                    else
                        objentityCommissionViewModel.filterRecords = objentityCommissionViewModel.totalRecords;
                    if (sortDirection == "asc")
                        objentityCommissionViewModel.objEntityCommisionMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryEntityCommission.Where(c => c.CompanyId == companyId).OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityCommission>() :
                            objentityCommissionViewModel.objEntityCommisionMasterList.OrderBy(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityCommission>();
                    else
                        objentityCommissionViewModel.objEntityCommisionMasterList = String.IsNullOrEmpty(searchValue) ?
                            db.QryEntityCommission.Where(c => c.CompanyId == companyId).OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityCommission>() :
                            objentityCommissionViewModel.objEntityCommisionMasterList.OrderByDescending(s => s.CompanyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityCommission>();

                    return Ok(objentityCommissionViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // GET: api/EntityCommission/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Commission))]
        public IHttpActionResult getentitycommission(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblEntity_Commission tblEntity_Commission = db.tblEntity_Commission.Find(id);
                    if (tblEntity_Commission == null)
                    {
                        return NotFound();
                    }

                    return Ok(tblEntity_Commission);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // PUT: api/EntityCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateEntityCommission(int id, tblEntity_Commission objEntityCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objEntityCommission.CommissionId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var entityInBb = db.tblEntity_Commission.SingleOrDefault(e => e.EntityId == objEntityCommission.EntityId &&
                        e.CommissionTypeId == objEntityCommission.CommissionTypeId &&
                        e.ProductId == objEntityCommission.ProductId &&
                        e.CommissionId != objEntityCommission.CommissionId &&
                        e.CompanyId == objEntityCommission.CompanyId);

                        if (entityInBb != null)
                            return BadRequest("Default Commission already exits!");

                        db.Entry(objEntityCommission).State = EntityState.Modified;
                        db.Entry(objEntityCommission).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objEntityCommission).Property(x => x.CompanyId).IsModified = false;
                        db.Entry(objEntityCommission).Property(x => x.EntityId).IsModified = false;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblEntity_CommissionExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        // POST: api/EntityCommission
        [Authorize]
        [ResponseType(typeof(tblEntity_Commission))]
        [Route("api/saveEntityCommission")]
        [HttpPost]
        public IHttpActionResult saveEntityCommission(tblEntity_Commission objEntityCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                long createdById = 0;
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    createdById = Convert.ToInt64(identityAdminLoginId.Value);
                }
                else
                {
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    createdById = Convert.ToInt64(identityEntityUserId.Value);
                }

                objEntityCommission.CreatedBy = createdById;
                db.tblEntity_Commission.Add(objEntityCommission);
                db.SaveChanges();
                return Ok();
            }
            else
                return Unauthorized();
        }

        // DELETE: api/EntityCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/deleteCommissionbyCompanyIdEntityTypeId")]
        [HttpDelete]
        public IHttpActionResult deleteCommissionbyCompanyIdEntityTypeId()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                List<tblEntity_Commission> objEntityCommissionList = db.tblEntity_Commission.Where(x => x.CompanyId == companyId).ToList();
                if (objEntityCommissionList.Count() > 0)
                {
                    db.tblEntity_Commission.RemoveRange(objEntityCommissionList);
                    db.SaveChanges();
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(long))]
        [Route("api/deleteCommissionbyCompanyIdEntityTypeIdCustom")]
        [HttpDelete]
        public IHttpActionResult deleteCommissionbyCompanyIdEntityTypeIdCustom()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityType = Convert.ToInt16(identityEntityType.Value);
                var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                int companyId = Convert.ToInt32(identityCompanyId.Value);
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);

                List<tblEntity_Commission> objEntityCommissionList = db.tblEntity_Commission.Where(x => x.CompanyId == companyId && x.EntityId == entityId && x.IsCustom == true).ToList();
                if (objEntityCommissionList.Count() > 0)
                {
                    db.tblEntity_Commission.RemoveRange(objEntityCommissionList);
                    db.SaveChanges();
                }
                return Ok(entityId);
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_CommissionExists(int id)
        {
            return db.tblEntity_Commission.Count(e => e.CommissionId == id) > 0;
        }
    }
}