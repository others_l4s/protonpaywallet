﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class instantPayUtilityDataDefaultCommissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/instantPayUtilityDataDefaultCommission
        [Authorize]
        [ResponseType(typeof(List<QryInstantPay_UtilityData_Default_Commission>))]
        public IHttpActionResult GettblInstantPay_UtilityData_Commission()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                List<QryInstantPay_UtilityData_Default_Commission> objDefaultCommission = new List<QryInstantPay_UtilityData_Default_Commission>();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    objDefaultCommission = db.QryInstantPay_UtilityData_Default_Commission.Where(x => x.CompanyId == companyId).OrderBy(x => x.ServiceType).ToList();
                    return Ok(objDefaultCommission);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }


        // POST: api/instantPayUtilityDataDefaultCommission
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult instantPayUtilityDataDefaultCommissionSave(tblInstantPay_UtilityData_Commission objDefaultCommission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    objDefaultCommission.CreatedDateTime = helper.getIndianTime();
                    objDefaultCommission.CreatedBy = adminLoginId;
                    db.tblInstantPay_UtilityData_Commission.Add(objDefaultCommission);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/instantPayUtilityDataDefaultCommission/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpDelete]
        public IHttpActionResult instantPayUtilityDataDefaultCommissionDelete(long id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    List<tblInstantPay_UtilityData_Commission> objDefaultCommissionList = db.tblInstantPay_UtilityData_Commission.Where(x => x.IsCustom == false).ToList();
                    if (objDefaultCommissionList == null)
                    {
                        return NotFound();
                    }
                    db.tblInstantPay_UtilityData_Commission.RemoveRange(objDefaultCommissionList);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblInstantPay_UtilityData_CommissionExists(long id)
        {
            return db.tblInstantPay_UtilityData_Commission.Count(e => e.InstantPayRechargeDataCommissionId == id) > 0;
        }
    }
}