﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using Antlr.Runtime.Misc;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class loginSessionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: loginSession
        [Authorize]
        [ResponseType(typeof(webLoginVM))]
        [HttpPost]
        public IHttpActionResult saveLoginSession(tblLogin_Session objLoginSession)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                webLoginVM objWebLoginVM = new webLoginVM();
                long LoginId = 0;
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                int entityTypeId = Convert.ToInt16(identityEntityType.Value);
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                    objLoginSession.IsAdminLogin = true;
                    using (applicationDbEntities updateInDatabase = new applicationDbEntities())
                    {
                        updateInDatabase.tblLogin_Session.Where(x => x.IsAdminLogin == true && x.IsLogout == false).ToList().ForEach(x => { x.IsLogout = true; x.LogoutDateTime = DateTime.Now; });
                        updateInDatabase.SaveChanges();
                    }
                }
                else
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                    objLoginSession.IsAdminLogin = false;
                    objLoginSession.OTPVerificationRequired = false;
                    objWebLoginVM.OTPVerificationRequired = false;
                    using (applicationDbEntities updateInDatabase = new applicationDbEntities())
                    {
                        updateInDatabase.tblLogin_Session.Where(x => x.IsAdminLogin == false && x.IsLogout == false).ToList().ForEach(x => { x.IsLogout = true; x.LogoutDateTime = DateTime.Now; });
                        updateInDatabase.SaveChanges();
                    }

                    //Checking OTP Validation
                    var userLoginInDb = db.QryEntityUser.SingleOrDefault(c => c.EntityUserId == LoginId);
                    if (userLoginInDb != null)
                    {
                        if (userLoginInDb.ActivateMobileOTP.Value)
                        {
                            Boolean OTPHistoryFoundForDay = false;
                            Boolean isOTPVerified = false;
                            string OTPForday = "";
                            var otpHistoryInDb = db.tblOTP_Session.Where(x => x.EntityUserId == userLoginInDb.EntityUserId &&
                                                                                    x.MobileNo == userLoginInDb.Mobile &&
                                                                                    x.OTPVerifiedDateTime.Value.Day == DateTime.Today.Day &&
                                                                                    x.OTPVerifiedDateTime.Value.Month == DateTime.Today.Month &&
                                                                                    x.OTPVerifiedDateTime.Value.Year == DateTime.Today.Year &&
                                                                                    x.FromMobileDevice == false).OrderByDescending(x => x.OTPVerifiedDateTime).Take(1).SingleOrDefault();
                            OTPHistoryFoundForDay = otpHistoryInDb == null ? false : true;
                            isOTPVerified = OTPHistoryFoundForDay ? otpHistoryInDb.OTPVerified : false;
                            OTPForday = (OTPHistoryFoundForDay ? otpHistoryInDb.OTP : helper.generateRandom8DigitNumber());
                            if (!OTPHistoryFoundForDay)
                            {
                                //Generating mobile OTP and saving it to database.
                                tblOTP_Session objOTPSession = new tblOTP_Session();
                                objOTPSession.EntityTypeId = userLoginInDb.EntityTypeId;
                                objOTPSession.UserName = userLoginInDb.UserName;
                                objOTPSession.EntityUserId = userLoginInDb.EntityUserId;
                                objOTPSession.MobileNo = userLoginInDb.Mobile;
                                objOTPSession.OTP = OTPForday;
                                objOTPSession.OTPVerified = false;
                                objOTPSession.OTPSendDateTime = helper.getIndianTime();
                                objOTPSession.FromMobileDevice = false;
                                db.tblOTP_Session.Add(objOTPSession);
                                db.SaveChanges();
                                helper.sendSMS_OTP(userLoginInDb.Mobile, helper.loginOTPTemplate(OTPForday), userLoginInDb.EntityUserId);
                                objLoginSession.OTPVerificationRequired = true;

                                objWebLoginVM.mobileNo = userLoginInDb.Mobile;
                                objWebLoginVM.OTPVerificationRequired = true;
                                objWebLoginVM.userName = userLoginInDb.UserName;
                                objWebLoginVM.entityUserId = userLoginInDb.EntityUserId;
                            }
                            else
                            {
                                if (!isOTPVerified)
                                    helper.sendSMS_OTP(userLoginInDb.Mobile, helper.loginOTPTemplate(OTPForday), userLoginInDb.EntityUserId);
                            }
                        }
                    }
                }
                objLoginSession.IsLogout = false;
                objLoginSession.LoginId = LoginId;
                objLoginSession.EntityTypeId = entityTypeId;
                db.tblLogin_Session.Add(objLoginSession);
                db.SaveChanges();

                objWebLoginVM.entityTypeId = entityTypeId;
                return Ok(objWebLoginVM);
            }
            else
            {
                return BadRequest("Authentication Failed");
            }
        }

        [Authorize]
        [ResponseType(typeof(tblLogin_Session))]
        [HttpGet]
        [Route("api/loginSession/getuserdata")]
        public IHttpActionResult getUserDetails()
        {
            tblLogin_Session objLoginSession = new tblLogin_Session();
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                long LoginId = 0;
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                }
                else
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                }
                var adminLoginSessionInDb = db.tblLogin_Session.Where(a => a.LoginId == objLoginSession.LoginId && a.IsLogout == false).OrderByDescending(a => a.accessTokenValidity).FirstOrDefault();
                return Ok(adminLoginSessionInDb);
            }
            else
            {
                return BadRequest("Authentication Failed");
            }
        }

        [Authorize]
        [HttpPut]
        [Route("api/loginSession/logoutUser")]
        public IHttpActionResult logoutUser()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                long LoginId = 0;
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                    identity.RemoveClaim(identityLoginId);
                    identity.RemoveClaim(identityCompanyId);
                }
                else
                {
                    var identityLoginId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    LoginId = Convert.ToInt64(identityLoginId.Value);
                    identity.RemoveClaim(identityLoginId);
                    identity.RemoveClaim(identityEntityId);
                }

                var adminLoginSessionInDb = db.tblLogin_Session.Where(a => a.LoginId == LoginId && a.IsLogout == false).OrderByDescending(a => a.accessTokenValidity).FirstOrDefault();
                if (adminLoginSessionInDb != null)
                {
                    adminLoginSessionInDb.IsLogout = true;
                    adminLoginSessionInDb.LogoutDateTime = helper.getIndianTime();
                }
                db.Entry(adminLoginSessionInDb).State = EntityState.Modified;
                db.SaveChanges();
                identity.RemoveClaim(identityEntityType);
                identity.AuthenticationType.Remove(0);
                return Ok();
            }
            else
            {
                return BadRequest("Authentication Failed");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        [Authorize]
        [ResponseType(typeof(HeaderNav))]
        [HttpGet]
        [Route("api/getNavBarItems")]
        public IHttpActionResult getNavBarItems(string domainName)
        {
            HeaderNav objHeaderNav = new HeaderNav();
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId.Value);
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt32(identityAdminLoginId.Value);
                    var objAdminLoginInDb = db.QryAdminLogins.SingleOrDefault(x => x.CompanyId == companyId && x.AdminLoginId == adminLoginId); // && x.CompanyDomain.Trim().ToLower() == domainName
                    if (objAdminLoginInDb != null)
                    {
                        objHeaderNav.companyDomain = objAdminLoginInDb.CompanyDomain;
                        objHeaderNav.companyLogo = objAdminLoginInDb.CompanyLogo;
                        objHeaderNav.userName = objAdminLoginInDb.PersonName;
                        objHeaderNav.GSTNumber = objAdminLoginInDb.CompanyGSTNo;
                        objHeaderNav.isCompanyProfile = true;
                        objHeaderNav.isLeadUser = objAdminLoginInDb.IsLeadUser;
                        var companyWalletInDb = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId);
                        objHeaderNav.walletBalance = (companyWalletInDb == null ? 0 : companyWalletInDb.WalletBalance);
                        objHeaderNav.lowBalanceAlert = (companyWalletInDb == null ? true : (companyWalletInDb.WalletBalance <= 1000 ? true : false));
                    }
                }
                else
                {
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);
                    var objEntityLoginIndb = db.QryEntityUserLoginSession.SingleOrDefault(x => x.EntityUserId == entityUserId); // && x.CompanyDomain.Trim().ToLower() == domainName
                    if (objEntityLoginIndb != null)
                    {
                        objHeaderNav.companyDomain = objEntityLoginIndb.CompanyDomain;
                        objHeaderNav.companyLogo = objEntityLoginIndb.CompanyLogo;
                        objHeaderNav.userName = objEntityLoginIndb.PersonName;
                        objHeaderNav.GSTNumber = objEntityLoginIndb.GSTNo;
                        objHeaderNav.isCompanyProfile = false;
                        objHeaderNav.isLeadUser = (objEntityLoginIndb.LeadUser == null ? false : (Boolean)objEntityLoginIndb.LeadUser);
                        objHeaderNav.walletBalance = (objEntityLoginIndb.WalletBalance == null ? 0 : (Decimal)objEntityLoginIndb.WalletBalance);
                        objHeaderNav.lowBalanceAlert = (objHeaderNav.walletBalance <= objEntityLoginIndb.MinimumBalanceMaintain ? true : false);
                    }
                }
                return Ok(objHeaderNav);
            }
            else
            {
                return BadRequest("Authentication Failed");
            }
        }
    }
}