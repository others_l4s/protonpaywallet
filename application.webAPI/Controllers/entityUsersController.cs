﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI;
using application.webAPI.Models;
using application.webAPI.prime.sdk;
using application.webAPI.viewModel;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using RestSharp;

namespace application.webAPI.Controllers
{
    public class entityUsersController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();


        // GET: api/entityUsers
        [Authorize]
        [ResponseType(typeof(entityUserMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityUserList()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var userIdentityClaimValue = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                int adminLoginId = Convert.ToInt16(userIdentityClaimValue.Value);
                int CompanyId = helper.getCompanyIdByAdminLoginId(adminLoginId);

                entityUserMasterViewModel entityUserMasterViewModel = new entityUserMasterViewModel()
                {
                    objEntityUser = new tblEntity_Users(),
                    objStatusList = db.tblStatus_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.StatusName).ToList(),
                    objCountryList = db.tblCountry_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CountryName).ToList(),
                    objStateList = new List<tblState_Master>(),
                    objCityList = new List<tblCity_Master>(),
                    objRoleList = (CompanyId == helper.defaultCompany ? db.tblRole_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.RoleName).ToList() : db.tblRole_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.RoleName).ToList()),
                    objDepartmentList = (CompanyId == helper.defaultCompany ? db.tblDepartment_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.DepartmentName).ToList() : db.tblDepartment_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.DepartmentName).ToList()),
                    objDesignationList = (CompanyId == helper.defaultCompany ? db.tblDesignation_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.DesignationName).ToList() : db.tblDesignation_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.DesignationName).ToList()),
                    objEntityMasterList = (CompanyId == helper.defaultCompany ? db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.AgencyName).ToList() : db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.AgencyName).ToList())
                };
                return Ok(entityUserMasterViewModel);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [ResponseType(typeof(entityUserMasterViewModel))]
        [HttpGet]
        public IHttpActionResult getEntityUserByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection, long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                entityUserMasterViewModel objentityUserMasterViewModel = new entityUserMasterViewModel();
                objentityUserMasterViewModel.totalRecords = db.QryEntityUser.Where(e => e.EntityId == entityId).Count();
                if (!String.IsNullOrEmpty(searchValue))
                {
                    objentityUserMasterViewModel.objEntityUserList = db.QryEntityUser.Where(s => s.UserName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.AgencyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.RoleName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.DesignationName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Mobile.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.CountryName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.EmailId.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Address1.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.Address2.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.PinCode.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.StatusRemark.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                    || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();

                    objentityUserMasterViewModel.filterRecords = objentityUserMasterViewModel.objEntityMasterList.Count();
                }
                else
                    objentityUserMasterViewModel.filterRecords = objentityUserMasterViewModel.totalRecords;
                if (sortDirection == "asc")
                    objentityUserMasterViewModel.objEntityUserList = String.IsNullOrEmpty(searchValue) ?
                    db.QryEntityUser.Where(e => e.EntityId == entityId).OrderBy(s => s.UserName).Skip(pageIndex).Take(pageSize).ToList<QryEntityUser>() :
                    objentityUserMasterViewModel.objEntityUserList.OrderBy(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityUser>();
                else
                    objentityUserMasterViewModel.objEntityUserList = String.IsNullOrEmpty(searchValue) ?
                    db.QryEntityUser.Where(e => e.EntityId == entityId).OrderByDescending(s => s.UserName).Skip(pageIndex).Take(pageSize).ToList<QryEntityUser>() :
                    objentityUserMasterViewModel.objEntityUserList.OrderByDescending(s => s.AgencyName).Skip(pageIndex).Take(pageSize).ToList<QryEntityUser>();

                return Ok(objentityUserMasterViewModel);
            }
            else
                return Unauthorized();
        }

        // GET: api/entityUsers/5
        [Authorize]
        [ResponseType(typeof(entityUserMasterViewModel))]
        public IHttpActionResult getEntityUserById(int id, long entityId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var userIdentityClaimValue = identity.Claims.Where(c => c.Type == "entityuserId").Single();
                int adminLoginId = Convert.ToInt16(userIdentityClaimValue.Value);
                int CompanyId = helper.getCompanyIdByAdminLoginId(adminLoginId);

                entityUserMasterViewModel objentityUserMasterViewModel = new entityUserMasterViewModel();
                tblEntity_Users objEntityUser = new tblEntity_Users();
                if (id > 0)
                {
                    objEntityUser = db.tblEntity_Users.Find(id);
                    if (objEntityUser == null)
                    {
                        return NotFound();
                    }
                }

                objentityUserMasterViewModel.objStatusList = db.tblStatus_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.StatusName).ToList();
                objentityUserMasterViewModel.objCountryList = db.tblCountry_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CountryName).ToList();
                objentityUserMasterViewModel.objStateList = (id > 0 ? db.tblState_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CountryId == objEntityUser.CountryId).OrderBy(c => c.StateName).ToList() : new List<tblState_Master>());
                objentityUserMasterViewModel.objCityList = (id > 0 ? db.tblCity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.StateId == objEntityUser.StateId).OrderBy(c => c.CityName).ToList() : new List<tblCity_Master>());
                objentityUserMasterViewModel.objRoleList = (CompanyId == helper.defaultCompany ? db.tblRole_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.RoleName).ToList() : db.tblRole_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.RoleName).ToList());
                objentityUserMasterViewModel.objDepartmentList = (CompanyId == helper.defaultCompany ? db.tblDepartment_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.DepartmentName).ToList() : db.tblDepartment_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.DepartmentName).ToList());
                objentityUserMasterViewModel.objDesignationList = (CompanyId == helper.defaultCompany ? db.tblDesignation_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.DesignationName).ToList() : db.tblDesignation_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.DesignationName).ToList());
                objentityUserMasterViewModel.objEntityMasterList = (CompanyId == helper.defaultCompany ? db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.AgencyName).ToList() : db.tblEntity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CompanyId == CompanyId).OrderBy(c => c.AgencyName).ToList());
                objentityUserMasterViewModel.objEntityUser = objEntityUser;
                objentityUserMasterViewModel.objEntityUser.EntityId = entityId;
                return Ok(objentityUserMasterViewModel);
            }
            else
                return Unauthorized();
        }

        // PUT: api/entityUsers/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateEntityUser(int id, tblEntity_Users objEntityUser)
        {
            var identity = (ClaimsIdentity)User.Identity;

            if (identity.IsAuthenticated)
            {
                if (id != objEntityUser.EntityUserId)
                {
                    return BadRequest();
                }
                try
                {
                    var entityUserInDB = db.tblEntity_Users.SingleOrDefault(e => e.UserName.Trim().ToLower() == objEntityUser.UserName.Trim().ToLower()
                    && e.EntityUserId != objEntityUser.EntityUserId);
                    if (entityUserInDB != null)
                        return BadRequest("User already exits!");

                    db.Entry(objEntityUser).State = EntityState.Modified;
                    db.Entry(objEntityUser).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.IsEmailVerified).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.EmailVerifiedDateTime).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.LeadUser).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.FingerPrintLoginEnabled).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.IMEINo).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.ActivateMobileOTP).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.UserName).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.StatusId).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.DepartmentId).IsModified = false;
                    db.Entry(objEntityUser).Property(x => x.DesignationId).IsModified = false;
                    if (String.IsNullOrEmpty(objEntityUser.Password.Trim()))
                        db.Entry(objEntityUser).Property(x => x.Password).IsModified = false;
                    else
                        objEntityUser.Password = helper.protectString(objEntityUser.Password);

                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!tblEntity_UsersExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Ok();
            }
            else return Unauthorized();
        }

        [HttpPost]
        [Route("api/entityUser/createleaduser")]
        public IHttpActionResult createLeadUser(tblEntity_Users objEntityUser)
        {
            webLoginVM objWebLoginVM = new webLoginVM();
            objEntityUser.Password = helper.protectString(objEntityUser.Password);
            //Disabling EmailId and Mobile for entity user by nimesh on 11 07 2020
            var entityUserInDB = db.tblEntity_Users.SingleOrDefault(e => e.UserName.Trim().ToLower() == objEntityUser.UserName.Trim().ToLower() &&
                e.EntityId == objEntityUser.EntityId &&
                e.EntityUserId != objEntityUser.EntityUserId);

            if (entityUserInDB != null)
                return BadRequest("User already exits with same mobile no.!");
            if (objEntityUser.EntityUserId == 0)
            {
                objEntityUser.Pin = 0;
                objEntityUser.PersonName = objEntityUser.Mobile;
                objEntityUser.EmailId = string.Empty;
                objEntityUser.Address1 = string.Empty;
                objEntityUser.Address2 = string.Empty;
                objEntityUser.StateId = 0;
                objEntityUser.CityId = 0;
                objEntityUser.CountryId = 0;
                objEntityUser.PinCode = string.Empty;
                objEntityUser.DOB = string.Empty;
                objEntityUser.DepartmentId = 0;
                objEntityUser.DesignationId = 0;
                objEntityUser.RoleId = 0;
                objEntityUser.LeadUser = true;
                objEntityUser.CreatedBy = 0;
                objEntityUser.FingerPrintLoginEnabled = false;
                objEntityUser.ActivateMobileOTP = false;
                objEntityUser.StateId = 1;
                objEntityUser.StatusRemark = "User Account Created";
                objEntityUser.CreatedDateTime = helper.getIndianTime();
                objEntityUser.IsEmailVerified = true;
                objEntityUser.EmailVerifiedDateTime = helper.getIndianTime();
                objEntityUser.IMEINo = string.Empty;
                db.tblEntity_Users.Add(objEntityUser);
                db.SaveChanges();

                string OTP = helper.generateRandom8DigitNumber();
                //tblOTP_Session objOTPSession = new tblOTP_Session();
                //objOTPSession.EntityTypeId = 2;
                //objOTPSession.UserName = objEntityUser.UserName;
                //objOTPSession.EntityUserId = objEntityUser.EntityUserId;
                //objOTPSession.MobileNo = objEntityUser.Mobile;
                //objOTPSession.OTP = OTP;
                //objOTPSession.OTPVerified = false;
                //objOTPSession.OTPSendDateTime = helper.getIndianTime();
                //objOTPSession.FromMobileDevice = true;
                //db.tblOTP_Session.Add(objOTPSession);
                //db.SaveChanges();
                helper.sendSMS_OTP(objEntityUser.Mobile, OTP, objEntityUser.EntityUserId);

                objWebLoginVM.OTPVerificationRequired = true;
                objWebLoginVM.entityTypeId = 2;
                objWebLoginVM.userName = objEntityUser.UserName;
                objWebLoginVM.mobileNo = objEntityUser.Mobile;
                objWebLoginVM.entityUserId = objEntityUser.EntityUserId;
            }
            else
            {
                db.Entry(objEntityUser).State = EntityState.Modified;
                db.Entry(objEntityUser).Property(x => x.UserName).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.EntityId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.Pin).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.Address1).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.Address2).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.StateId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.CityId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.CountryId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.PinCode).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.DOB).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.DepartmentId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.DesignationId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.RoleId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.CreatedBy).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.StatusId).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.StatusRemark).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.IsEmailVerified).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.EmailVerifiedDateTime).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.LeadUser).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.IMEINo).IsModified = false;
                db.Entry(objEntityUser).Property(x => x.ActivateMobileOTP).IsModified = false;
                db.SaveChanges();
            }
            return Ok(objWebLoginVM);
        }

        [Authorize]
        [ResponseType(typeof(tblEntity_Users))]
        [HttpPut]
        [Route("api/entityUser/enabledisablefingerprintaccess")]
        public IHttpActionResult enabledisablefingerprintaccess(entityUserFingerPrintAccess objUserFingerPrintAccess)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (objUserFingerPrintAccess.EntityUserId > 0)
                {
                    var entityUserInDB = db.tblEntity_Users.SingleOrDefault(e => e.EntityUserId == objUserFingerPrintAccess.EntityUserId);

                    if (entityUserInDB == null)
                        return BadRequest("Entity User does not exists!");
                    entityUserInDB.IMEINo = objUserFingerPrintAccess.IMEINo;
                    entityUserInDB.FingerPrintLoginEnabled = objUserFingerPrintAccess.FingerPrintLoginEnabled;
                    db.Entry(entityUserInDB).State = EntityState.Modified;
                    db.Entry(entityUserInDB).Property(x => x.EntityId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.UserName).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.Pin).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.Password).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.PersonName).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.EmailId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.Mobile).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.Address1).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.Address2).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.StateId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.CityId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.CountryId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.PinCode).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.DOB).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.DepartmentId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.DesignationId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.RoleId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.CreatedBy).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.StatusId).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.StatusRemark).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.IsEmailVerified).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.EmailVerifiedDateTime).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.LeadUser).IsModified = false;
                    db.Entry(entityUserInDB).Property(x => x.ActivateMobileOTP).IsModified = false;
                    db.SaveChanges();
                }
                return Ok();
            }
            else
                return Unauthorized();
        }

        [HttpPost]
        [Route("api/entityUser/validateentityuserdata")]
        public IHttpActionResult validateEntityUserDataBeforeInsert(tblEntity_Users objEntityUser)
        {
            var checkEntityUserMobileNoInDb = db.tblEntity_Users.FirstOrDefault(e => e.Mobile.Trim().ToLower() == objEntityUser.Mobile.Trim().ToLower());
            if (checkEntityUserMobileNoInDb != null)
                return BadRequest("Mobile No already registered, kindly use different Mobile No!");

            return Ok();
        }

        // POST: api/entityUsers
        [Authorize]
        [ResponseType(typeof(tblEntity_Users))]
        [HttpPost]
        public IHttpActionResult saveEntityUser(tblEntity_Users objEntityUser)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    var identityEntityUserId = identity.Claims.Where(c => c.Type == "entityUserId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    long entityUserId = Convert.ToInt64(identityEntityUserId.Value);

                    //Disabling EmailId and Mobile for entity user by nimesh on 06 07 2020
                    var entityUserInDB = db.tblEntity_Users.SingleOrDefault(e => e.UserName.Trim().ToLower() == objEntityUser.UserName.Trim().ToLower() &&
                    e.EntityId == objEntityUser.EntityId &&
                    //e.EmailId.Trim() == objEntityUser.EmailId.Trim() &&
                    //e.Mobile == objEntityUser.Mobile &&
                    e.EntityUserId != objEntityUser.EntityUserId);

                    if (entityUserInDB != null)
                        return BadRequest("Entity User already exits!");

                    objEntityUser.CreatedBy = entityUserId;
                    objEntityUser.ActivateMobileOTP = true;
                    db.tblEntity_Users.Add(objEntityUser);
                    db.SaveChanges();
                    return Ok(objEntityUser);
                }
                else
                {
                    return BadRequest("Invalid module access");
                }
            }
            else
                return Unauthorized();
        }

        // DELETE: api/entityUsers/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Users))]
        public IHttpActionResult deleteEntityUser(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblEntity_Users tblEntity_Users = db.tblEntity_Users.Find(id);
                if (tblEntity_Users == null)
                {
                    return NotFound();
                }
                db.tblEntity_Users.Remove(tblEntity_Users);
                db.SaveChanges();
                //tblEntity_Login objEntitylogin = db.tblEntity_Login.SingleOrDefault(x => x.EntityUserId == id);
                //if (objEntitylogin != null)
                //{
                //    db.tblEntity_Login.Remove(objEntitylogin);
                //    db.SaveChanges();
                //}
                return Ok(tblEntity_Users);
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEntity_UsersExists(int id)
        {
            return db.tblEntity_Users.Count(e => e.EntityUserId == id) > 0;
        }

        #region Edit Profile

        [Authorize]

        [Route("api/editProfile")]
        [HttpGet]
        public IHttpActionResult editProfile()
        {
            editProfileKYCVM objEditProfileKYCVM = new editProfileKYCVM();
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                tblEntity_Users objEntityUser = new tblEntity_Users();
                if (objUserIdentity.entityUserId > 0)
                {
                    objEntityUser = db.tblEntity_Users.Find(objUserIdentity.entityUserId);
                    if (objEntityUser == null)
                    {
                        return NotFound();
                    }
                }
                objEditProfileKYCVM.objEditProfile = new editProfileKYC();
                tblEntity_KYC objEntityKYC = db.tblEntity_KYC.Where(x => x.EntityId == objUserIdentity.entityId).Take(1).SingleOrDefault();
                objEditProfileKYCVM.objCountryList = db.tblCountry_Master.Where(c => c.StatusId == helper.defaultActiveStatusId).OrderBy(c => c.CountryName).ToList();
                objEditProfileKYCVM.objStateList = db.tblState_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.CountryId == objEntityUser.CountryId).OrderBy(c => c.StateName).ToList();
                objEditProfileKYCVM.objCityList = db.tblCity_Master.Where(c => c.StatusId == helper.defaultActiveStatusId && c.StateId == objEntityUser.StateId).OrderBy(c => c.CityName).ToList();
                objEditProfileKYCVM.objEditProfile.EntityUserId = objEntityUser.EntityUserId;
                objEditProfileKYCVM.objEditProfile.EntityId = objEntityUser.EntityId;
                objEditProfileKYCVM.objEditProfile.PersonName = objEntityUser.PersonName;
                objEditProfileKYCVM.objEditProfile.UserName = objEntityUser.UserName;
                objEditProfileKYCVM.objEditProfile.Password = helper.unProtectString(objEntityUser.Password);
                objEditProfileKYCVM.objEditProfile.EmailId = objEntityUser.EmailId;
                objEditProfileKYCVM.objEditProfile.Mobile = objEntityUser.Mobile;
                objEditProfileKYCVM.objEditProfile.Address1 = objEntityUser.Address1;
                objEditProfileKYCVM.objEditProfile.Address2 = objEntityUser.Address2;
                objEditProfileKYCVM.objEditProfile.StateId = objEntityUser.StateId;
                objEditProfileKYCVM.objEditProfile.CityId = objEntityUser.CityId;
                objEditProfileKYCVM.objEditProfile.CountryId = objEntityUser.CountryId;
                objEditProfileKYCVM.objEditProfile.PinCode = objEntityUser.PinCode;
                objEditProfileKYCVM.objEditProfile.DOB = objEntityUser.DOB;
                objEditProfileKYCVM.objEditProfile.CreatedDateTime = objEntityUser.CreatedDateTime;
                objEditProfileKYCVM.objEditProfile.MobileOTP = "";
                objEditProfileKYCVM.objEditProfile.EntityKYCId = (objEntityKYC != null ? objEntityKYC.EntityKYCId : 0);
                objEditProfileKYCVM.objEditProfile.PanDocumentId = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.PanDocumentId) ? string.Empty : objEntityKYC.PanDocumentId) : string.Empty);
                objEditProfileKYCVM.objEditProfile.PanCardNo = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.PanCardNo) ? string.Empty : objEntityKYC.PanCardNo) : string.Empty);
                objEditProfileKYCVM.objEditProfile.PanCardPhoto = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.PanCardPhoto) ? string.Empty : objEntityKYC.PanCardPhoto) : string.Empty);
                objEditProfileKYCVM.objEditProfile.AadhaarCardDocumentId = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.AadhaarCardDocumentId) ? string.Empty : objEntityKYC.AadhaarCardDocumentId) : string.Empty);
                objEditProfileKYCVM.objEditProfile.AadhaarCardNo = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.AadhaarCardNo) ? string.Empty : objEntityKYC.AadhaarCardNo) : string.Empty);
                objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.AadhaarCardPhoto) ? string.Empty : objEntityKYC.AadhaarCardPhoto) : string.Empty);
                objEditProfileKYCVM.objEditProfile.IsKYCStatus = (objEntityKYC != null ? (objEntityKYC.IsKYCStatus == null ? false : objEntityKYC.IsKYCStatus.Value) : false);
                objEditProfileKYCVM.objEditProfile.KYCStatusRemark = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.KYCStatusRemark) ? string.Empty : objEntityKYC.KYCStatusRemark) : string.Empty);
                objEditProfileKYCVM.objEditProfile.FromMobileDevice = (objEntityKYC != null ? (objEntityKYC.FromMobileDevice == null ? false : objEntityKYC.FromMobileDevice.Value) : false);
                objEditProfileKYCVM.objEditProfile.OutletId = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.OutletId) ? string.Empty : objEntityKYC.OutletId) : string.Empty);
                objEditProfileKYCVM.objEditProfile.OutletMobileVerified = (objEntityKYC != null ? (objEntityKYC.OutletMobileVerified == null ? false : objEntityKYC.OutletMobileVerified.Value) : false);
                objEditProfileKYCVM.objEditProfile.OutletDocumentGetStatusResponse = (objEntityKYC != null ? (String.IsNullOrEmpty(objEntityKYC.OutletDocumentGetStatusResponse) ? string.Empty : objEntityKYC.OutletDocumentGetStatusResponse) : string.Empty);

                objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto = (String.IsNullOrEmpty(objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto) ?
                    String.Empty :
                    (objEditProfileKYCVM.objEditProfile.FromMobileDevice ?
                    ConfigurationManager.AppSettings["APIDomainName"] + "KYCImages/" :
                    ConfigurationManager.AppSettings["domainName"] + "KYCImages/") + objEditProfileKYCVM.objEditProfile.AadhaarCardPhoto);
                objEditProfileKYCVM.objEditProfile.PanCardPhoto = (String.IsNullOrEmpty(objEditProfileKYCVM.objEditProfile.PanCardPhoto) ?
                    String.Empty :
                    (objEditProfileKYCVM.objEditProfile.FromMobileDevice ?
                    ConfigurationManager.AppSettings["APIDomainName"] + "KYCImages/" :
                    ConfigurationManager.AppSettings["domainName"] + "KYCImages/") + objEditProfileKYCVM.objEditProfile.PanCardPhoto);

                return Ok(objEditProfileKYCVM);
            }
            else
                return Unauthorized();
        }

        [Authorize]
        [Route("api/editProfileKYCSave")]
        [HttpPost]
        public IHttpActionResult editProfileKYCSave(editProfileKYC objEditProfileKYC)
        {
            string editProfileResponse = "";
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objUserIdentity = helper.getClaimsIdentityInfo(identity);
                if (objUserIdentity.entityUserId != objEditProfileKYC.EntityUserId)
                    return BadRequest("Invalidd User Access");
                var entityUserInDB = db.tblEntity_Users.SingleOrDefault(e => e.EntityUserId == objUserIdentity.entityUserId);
                if (entityUserInDB == null)
                    return BadRequest("User not found!");

                db.Entry(entityUserInDB).State = EntityState.Modified;
                db.Entry(entityUserInDB).Property(x => x.CreatedBy).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.IsEmailVerified).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.EmailVerifiedDateTime).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.LeadUser).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.EntityId).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.FingerPrintLoginEnabled).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.IMEINo).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.ActivateMobileOTP).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.UserName).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.StatusId).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.DepartmentId).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.DesignationId).IsModified = false;
                db.Entry(entityUserInDB).Property(x => x.Password).IsModified = false;
                db.SaveChanges();

                var panCardExistsInDB = db.tblEntity_KYC.Where(x => x.PanCardNo.Trim().ToUpper() == objEditProfileKYC.PanCardNo.Trim().ToUpper() &&
                x.EntityKYCId != objEditProfileKYC.EntityKYCId).Take(1).SingleOrDefault();
                if (panCardExistsInDB != null)
                    return BadRequest("Pancard No. already registered. You cannot use same pancard for 2 different account.");

                //Updating Entity KYC
                if (objEditProfileKYC.FromMobileDevice == true)
                {
                    if (!String.IsNullOrEmpty(objEditProfileKYC.PanCardPhoto))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEditProfileKYC.PanCardPhoto);
                        string panCardNoPhotoName = "pancard_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEditProfileKYC.PanCardPhoto = panCardNoPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + panCardNoPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                        objEditProfileKYC.PanCardPhoto = panCardNoPhotoName;
                    }

                    if (!String.IsNullOrEmpty(objEditProfileKYC.AadhaarCardPhoto))
                    {
                        byte[] bytes = null;
                        bytes = Convert.FromBase64String(objEditProfileKYC.AadhaarCardPhoto);
                        string aadhaarCardPhotoName = "aadhaar_side_1_" + helper.GetRandomPasswordUsingGUID(10) + ".jpg";
                        objEditProfileKYC.AadhaarCardPhoto = aadhaarCardPhotoName;
                        string filePath = HttpContext.Current.Server.MapPath("~/KYCImages/" + aadhaarCardPhotoName);
                        File.WriteAllBytes(filePath, bytes);
                        objEditProfileKYC.AadhaarCardPhoto = aadhaarCardPhotoName;
                    }
                }
                tblEntity_KYC objKYCDetailInDb = db.tblEntity_KYC.Where(x => x.EntityUserId == objUserIdentity.entityUserId).SingleOrDefault();
                if (objKYCDetailInDb != null)
                {

                    db.Entry(objKYCDetailInDb).State = EntityState.Modified;
                    db.Entry(objKYCDetailInDb).Property(x => x.EntityId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.EntityUserId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.PanDocumentId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.PanCardNo).IsModified = false;
                    if (String.IsNullOrEmpty(objEditProfileKYC.PanCardPhoto))
                        db.Entry(objKYCDetailInDb).Property(x => x.PanCardPhoto).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.PanCardPhoto2).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.GSTDocumentId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.GSTNo).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.GSTNoPhoto).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.GSTNoPhoto2).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                    if (String.IsNullOrEmpty(objEditProfileKYC.AadhaarCardNo))
                        db.Entry(objKYCDetailInDb).Property(x => x.AadhaarCardNo).IsModified = false;
                    if (String.IsNullOrEmpty(objEditProfileKYC.AadhaarCardPhoto))
                        db.Entry(objKYCDetailInDb).Property(x => x.AadhaarCardPhoto).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.IsKYCStatus).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.KYCStatusRemark).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.AdminLoginId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.FromMobileDevice).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.OutletId).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.OutletMobileVerified).IsModified = false;
                    db.Entry(objKYCDetailInDb).Property(x => x.OutletDocumentGetStatusResponse).IsModified = false;
                    db.SaveChanges();

                    objEditProfileKYC.PanCardPhoto = objKYCDetailInDb.PanCardPhoto;
                    objEditProfileKYC.AadhaarCardPhoto = objKYCDetailInDb.AadhaarCardPhoto;
                }
                else
                {
                    tblEntity_KYC objEntityKYC = new tblEntity_KYC();
                    objEntityKYC.EntityId = objUserIdentity.entityId;
                    objEntityKYC.EntityUserId = objUserIdentity.entityUserId;
                    objEntityKYC.PanDocumentId = string.Empty;
                    objEntityKYC.PanCardNo = objEditProfileKYC.PanCardNo;
                    objEntityKYC.PanCardPhoto = objEditProfileKYC.PanCardPhoto;
                    objEntityKYC.PanCardPhoto2 = string.Empty;
                    objEntityKYC.GSTDocumentId = string.Empty;
                    objEntityKYC.GSTNo = string.Empty;
                    objEntityKYC.GSTNoPhoto = string.Empty;
                    objEntityKYC.GSTNoPhoto2 = string.Empty;
                    objEntityKYC.AadhaarCardDocumentId = string.Empty;
                    objEntityKYC.AadhaarCardNo = objEditProfileKYC.AadhaarCardNo;
                    objEntityKYC.AadhaarCardPhoto = objEditProfileKYC.AadhaarCardPhoto;
                    objEntityKYC.AadhaarCardPhoto2 = string.Empty;
                    objEntityKYC.CreatedDateTime = helper.getIndianTime();
                    objEntityKYC.IsKYCStatus = false;
                    objEntityKYC.KYCStatusRemark = string.Empty;
                    objEntityKYC.AdminLoginId = 0;
                    objEntityKYC.FromMobileDevice = objEditProfileKYC.FromMobileDevice;
                    objEntityKYC.OutletId = string.Empty;
                    objEntityKYC.OutletMobileVerified = false;
                    objEntityKYC.OutletDocumentGetStatusResponse = string.Empty;
                    db.tblEntity_KYC.Add(objEntityKYC);
                    db.SaveChanges();

                    objEditProfileKYC.PanCardPhoto = objEntityKYC.PanCardPhoto;
                    objEditProfileKYC.AadhaarCardPhoto = objEntityKYC.AadhaarCardPhoto;
                }

                string[] outletCreationResponse = new string[2];
                string[] outletKYCDocumentResponse = new string[2];

                if (objEditProfileKYC.IsKYCStatus == false)
                {
                    if (String.IsNullOrEmpty(objEditProfileKYC.OutletId))
                    {
                        outletCreationResponse = outletRegistration(objEditProfileKYC);
                    }
                    else
                    {
                        outletCreationResponse[0] = "success";
                        outletCreationResponse[1] = "success";
                    }
                    if (outletCreationResponse[0] == "success")
                    {
                        outletKYCDocumentResponse = outletGetKYCDocumentStatus(objEditProfileKYC);
                        editProfileResponse = outletKYCDocumentResponse[1];
                    }
                    else
                    {
                        outletKYCDocumentResponse = outletCreationResponse;
                    }
                }
                else
                {
                    outletKYCDocumentResponse[0] = "success";
                    outletKYCDocumentResponse[1] = "Approved";
                    editProfileResponse = "Approved";
                }
                if (outletKYCDocumentResponse[0] == "success")
                    return Ok(outletKYCDocumentResponse[1]);
                else
                    return BadRequest(outletKYCDocumentResponse[1]);
            }
            else
            {
                return Unauthorized();
            }
        }

        #endregion

        [Authorize]
        [HttpPost]
        [Route("api/entityUser/changePassword")]
        public IHttpActionResult changePassword(changePassword objChangePassword)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                userIdentity objIdentityUsers = helper.getClaimsIdentityInfo(identity);
                if (!objIdentityUsers.isCompany)
                {
                    var entityUserInDb = db.tblEntity_Users.Where(x => x.EntityUserId == objIdentityUsers.entityUserId).SingleOrDefault();
                    if (entityUserInDb != null)
                    {
                        string oldPasswordIndb = helper.unProtectString(entityUserInDb.Password);
                        if (oldPasswordIndb == objChangePassword.oldPassword)
                        {
                            if (oldPasswordIndb == objChangePassword.newPassword)
                                return BadRequest("It seems you are using this password as your current password. Please change it to new password.");
                            entityUserInDb.Password = helper.protectString(objChangePassword.newPassword);
                            entityUserInDb.StatusRemark = "old password change @" + helper.getIndianTime();
                            db.Entry(entityUserInDb).State = EntityState.Modified;
                            db.Entry(entityUserInDb).Property(x => x.EntityId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.UserName).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.Pin).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.PersonName).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.EmailId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.Mobile).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.Address1).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.Address2).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.StateId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.CityId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.CountryId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.PinCode).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.DOB).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.DepartmentId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.DesignationId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.CreatedDateTime).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.CreatedBy).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.StatusId).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.IsEmailVerified).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.EmailVerifiedDateTime).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.LeadUser).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.IMEINo).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.FingerPrintLoginEnabled).IsModified = false;
                            db.Entry(entityUserInDb).Property(x => x.ActivateMobileOTP).IsModified = false;
                            db.SaveChanges();
                            return Ok("Password successfully changed.");
                        }
                        else
                            return BadRequest("Old password does not match");
                    }
                    else
                    {
                        return BadRequest("Invalid User Details. Please relogin and try again.");
                    }
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
            {
                return Unauthorized();
            }
        }

        #region Instantpay Prime SDK

        public string[] outletRegistration(editProfileKYC objEntityProfileKYC)
        {
            string[] outletRegistrationResponse = new string[2];
            registerOutlet objRegisterOutlet = new registerOutlet();
            objRegisterOutlet.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            objRegisterOutlet.request = new registerOutletRequest();
            objRegisterOutlet.request.mobile = objEntityProfileKYC.Mobile;
            objRegisterOutlet.request.email = objEntityProfileKYC.EmailId;
            objRegisterOutlet.request.company = objEntityProfileKYC.PersonName;
            objRegisterOutlet.request.name = objEntityProfileKYC.PersonName;
            objRegisterOutlet.request.pan = objEntityProfileKYC.PanCardNo;
            objRegisterOutlet.request.pincode = objEntityProfileKYC.PinCode;
            objRegisterOutlet.request.address = objEntityProfileKYC.Address1 + "" + (string.IsNullOrEmpty(objEntityProfileKYC.Address2) ? "" : ", " + objEntityProfileKYC.Address2);
            objRegisterOutlet.request.otp = objEntityProfileKYC.MobileOTP;
            IRestResponse IRegistrationOTPResponse = registerOutlet(objRegisterOutlet);
            if (IRegistrationOTPResponse.StatusCode == HttpStatusCode.OK)
            {
                registerOutletResponse objRegisterOutletResponse = JsonConvert.DeserializeObject<registerOutletResponse>(IRegistrationOTPResponse.Content);
                if (objRegisterOutletResponse.statuscode == "TXN")
                {
                    if (updateEntityKYC_Outlet_Registration(objEntityProfileKYC.EntityUserId, objRegisterOutletResponse.data.outlet_id))
                    {
                        outletRegistrationResponse[0] = "success";
                        outletRegistrationResponse[1] = "success";
                        return outletRegistrationResponse;
                    }
                    else
                    {
                        outletRegistrationResponse[0] = "error";
                        outletRegistrationResponse[1] = objRegisterOutletResponse.status;
                        return outletRegistrationResponse;
                    }
                }
                else
                {
                    outletRegistrationResponse[0] = "error";
                    outletRegistrationResponse[1] = objRegisterOutletResponse.status;
                    return outletRegistrationResponse;
                }
            }
            else
            {
                outletRegistrationResponse[0] = "error";
                outletRegistrationResponse[1] = IRegistrationOTPResponse.StatusDescription;
                return outletRegistrationResponse;
            }
        }

        protected IRestResponse registerOutlet(registerOutlet objRegisterOutlet)
        {
            IRestResponse response;
            string URL = "outlet/registration";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objRegisterOutlet);
            return response;
        }

        public string[] outletGetKYCDocumentStatus(editProfileKYC objEntityProfileKYC)
        {
            string[] outletDocumentStatus = new string[2];
            outletDocumentStatus[0] = "success";
            outletDocumentStatus[1] = "Under Process";
            uploadOutletDocument objUploadOutletDocument = new uploadOutletDocument();
            objUploadOutletDocument.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            requiredDocs objRequiredDocs = new requiredDocs();
            objRequiredDocs.token = ConfigurationManager.AppSettings["InstantPayToken"].ToString();
            objRequiredDocs.request = new requiredDocsRequest();
            objRequiredDocs.request.outletid = objEntityProfileKYC.OutletId;
            objRequiredDocs.request.pan_no = objEntityProfileKYC.PanCardNo;
            IRestResponse IRequiredDocsResponse = getKYCStatus(objRequiredDocs);
            if (IRequiredDocsResponse.StatusCode == HttpStatusCode.OK)
            {
                requiredDocsResponse objRequiredDocsResponse = JsonConvert.DeserializeObject<requiredDocsResponse>(IRequiredDocsResponse.Content);
                if (objRequiredDocsResponse.statuscode == "TXN")
                {
                    if (objRequiredDocsResponse.data.REQUIRED.Length > 0)
                    {
                        outletDocumentStatus[0] = "success";
                        outletDocumentStatus[1] = "Required";
                        for (int i = 0; i < objRequiredDocsResponse.data.REQUIRED.Length; i++)
                        {
                            string documentRequired = objRequiredDocsResponse.data.REQUIRED[i][2];
                            if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("pan card"))
                            {
                                objUploadOutletDocument.request = new uploadOutletDocumentRequest();
                                objUploadOutletDocument.request.outletid = objEntityProfileKYC.OutletId;
                                objUploadOutletDocument.request.pan_no = objEntityProfileKYC.PanCardNo;
                                objUploadOutletDocument.request.document = new uploadOutletDocuments();
                                objUploadOutletDocument.request.document.id = objRequiredDocsResponse.data.REQUIRED[i][0];
                                objUploadOutletDocument.request.document.filename = objEntityProfileKYC.PanCardPhoto;
                                objUploadOutletDocument.request.document.link = (objEntityProfileKYC.FromMobileDevice == true ? ConfigurationManager.AppSettings["APIDomainName"] : ConfigurationManager.AppSettings["domainName"]) + "KYCImages/" + objEntityProfileKYC.PanCardPhoto;
                                IRestResponse uploadOutletDocumentResponse = uploadOutletDocuments(objUploadOutletDocument);
                                db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_PanDocumentId] @EntityUserId={0}, @PanDocumentId={1}", objEntityProfileKYC.EntityUserId, objRequiredDocsResponse.data.REQUIRED[i][0]);
                            }
                            else if (Convert.ToString(objRequiredDocsResponse.data.REQUIRED[i][1]).Trim().ToLower().Contains("address proof"))
                            {
                                objUploadOutletDocument.request = new uploadOutletDocumentRequest();
                                objUploadOutletDocument.request.outletid = objEntityProfileKYC.OutletId;
                                objUploadOutletDocument.request.pan_no = objEntityProfileKYC.PanCardNo;
                                objUploadOutletDocument.request.document = new uploadOutletDocuments();
                                objUploadOutletDocument.request.document.id = objRequiredDocsResponse.data.REQUIRED[i][0];
                                objUploadOutletDocument.request.document.filename = objEntityProfileKYC.AadhaarCardPhoto;
                                objUploadOutletDocument.request.document.link = (objEntityProfileKYC.FromMobileDevice == true ? ConfigurationManager.AppSettings["APIDomainName"] : ConfigurationManager.AppSettings["domainName"]) + "KYCImages/" + objEntityProfileKYC.AadhaarCardPhoto;
                                IRestResponse uploadOutletDocumentResponse = uploadOutletDocuments(objUploadOutletDocument);
                                db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_AadhaarDocumentId] @EntityUserId={0}, @AadhaarCardDocumentId={1}", objEntityProfileKYC.EntityUserId, objRequiredDocsResponse.data.REQUIRED[i][0]);
                            }
                        }
                    }
                    if (objRequiredDocsResponse.data.APPROVED.Length > 0)
                    {
                        outletDocumentStatus[0] = "success";
                        outletDocumentStatus[1] = "Approved";
                        db.Database.ExecuteSqlCommand("EXEC [dbo].[Entity_KYC_Update_KYCStatus] @EntityUserId={0}", objEntityProfileKYC.EntityUserId);
                    }
                    return outletDocumentStatus;
                }
                else
                {
                    outletDocumentStatus[0] = "fail";
                    outletDocumentStatus[1] = objRequiredDocsResponse.status;
                }
            }
            else
            {
                outletDocumentStatus[0] = "fail";
                outletDocumentStatus[1] = IRequiredDocsResponse.StatusCode.ToString();
            }
            return outletDocumentStatus;
        }

        protected IRestResponse uploadOutletDocuments(uploadOutletDocument objUploadOutletDocuments)
        {
            IRestResponse response;
            string URL = "outlet/uploadDocs";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objUploadOutletDocuments);
            return response;
        }

        protected IRestResponse getKYCStatus(requiredDocs objRequiredDocs)
        {
            IRestResponse response;
            string URL = "outlet/requiredDocs";
            response = helper.WebAPIClient_via_RestSharp_4_INSTANT_DMT(URL, Method.POST, objRequiredDocs);
            return response;
        }

        public Boolean updateEntityKYC_Outlet_Registration(long entityUserId, string outletId)
        {
            tblEntity_KYC objEntityKYC = db.tblEntity_KYC.Where(x => x.EntityUserId == entityUserId).SingleOrDefault();
            if (objEntityKYC != null)
            {
                objEntityKYC.OutletId = outletId;
                objEntityKYC.OutletMobileVerified = true;
                db.Entry(objEntityKYC).State = EntityState.Modified;
                db.Entry(objEntityKYC).Property(x => x.EntityId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.PanCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.GSTNoPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardNo).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AadhaarCardPhoto2).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.EntityUserId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.IsKYCStatus).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.KYCStatusRemark).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.AdminLoginId).IsModified = false;
                db.Entry(objEntityKYC).Property(x => x.FromMobileDevice).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.PanDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.PanDocumentId).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.AadhaarCardDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.AadhaarCardDocumentId).IsModified = false;
                if (!String.IsNullOrEmpty(objEntityKYC.GSTDocumentId))
                    db.Entry(objEntityKYC).Property(x => x.GSTDocumentId).IsModified = false;
                db.SaveChanges();
                return true;
            }
            else
                return false;
        }

        #endregion

    }
}