﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;
using Newtonsoft.Json;
using RestSharp;

namespace application.webAPI.Controllers
{
    public class adminLoginsController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/adminLogins
        [Authorize]
        [ResponseType(typeof(adminLoginViewModel))]
        [HttpGet]
        public IHttpActionResult getAdminLogin()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var userIdentityClaimValue = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    int adminLoginId = Convert.ToInt16(userIdentityClaimValue.Value);
                    int CompanyId = helper.getCompanyIdByAdminLoginId(adminLoginId);
                    adminLoginViewModel objAdminLoginViewModel = new adminLoginViewModel()
                    {
                        objAdminLogin = new tblAdminLogin(),
                        objDepartmentList = (CompanyId == helper.defaultCompany ? db.tblDepartment_Master.OrderBy(c => c.DepartmentName).ToList() : db.tblDepartment_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.DepartmentName).ToList()),
                        objDesignationList = (CompanyId == helper.defaultCompany ? db.tblDesignation_Master.OrderBy(c => c.DesignationName).ToList() : db.tblDesignation_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.DesignationName).ToList()),
                        objRoleList = (CompanyId == helper.defaultCompany ? db.tblRole_Master.OrderBy(c => c.RoleName).ToList() : db.tblRole_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.RoleName).ToList()),
                        objCountryList = db.tblCountry_Master.OrderBy(c => c.CountryName).ToList(),
                        objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList(),
                        objStateList = new List<tblState_Master>(),
                        objCompanyList = (CompanyId == helper.defaultCompany ? db.tblCompany_Master.OrderBy(c => c.CompanyName).ToList() : db.tblCompany_Master.Where(c => c.CompanyId == CompanyId).OrderBy(c => c.CompanyName).ToList())
                    };
                    return Ok(objAdminLoginViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        [Authorize]
        // GET: api/adminLogins/5
        [ResponseType(typeof(tblAdminLogin))]
        [HttpGet]
        public IHttpActionResult getAdminLoginById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblAdminLogin objAdminLogin = db.tblAdminLogin.Find(id);
                if (objAdminLogin == null)
                {
                    return NotFound();
                }

                return Ok(objAdminLogin);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(adminLoginViewModel))]
        [HttpGet]
        public IHttpActionResult getAdminLoginListByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt16(identityCompanyId.Value);
                    adminLoginViewModel objAdminLoginViewModel = new adminLoginViewModel();
                    objAdminLoginViewModel.totalRecords = (CompanyId == helper.defaultCompany ? db.QryAdminLogins.Count() : db.QryAdminLogins.Where(c => c.CompanyId == CompanyId).Count());
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objAdminLoginViewModel.objAdminLoginList = (CompanyId == helper.defaultCompany ? db.QryAdminLogins.Where(s => s.LoginName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.DesignationName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.PersonName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.Mobile.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.EmailId.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.RoleName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList() :
                         db.QryAdminLogins.Where(s => s.CompanyId == CompanyId && s.LoginName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.DepartmentName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.CompanyName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.DesignationName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.PersonName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.Mobile.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.EmailId.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.RoleName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                         || s.StatusName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList());

                        objAdminLoginViewModel.filterRecords = objAdminLoginViewModel.objAdminLoginList.Count();
                    }
                    else
                        objAdminLoginViewModel.filterRecords = objAdminLoginViewModel.totalRecords;

                    if (sortDirection == "asc")
                        objAdminLoginViewModel.objAdminLoginList = String.IsNullOrEmpty(searchValue) ?
                           (CompanyId == helper.defaultCompany ? db.QryAdminLogins.OrderBy(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>() : db.QryAdminLogins.Where(c => c.CompanyId == CompanyId).OrderBy(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>()) :
                            objAdminLoginViewModel.objAdminLoginList.OrderBy(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>();
                    else
                        objAdminLoginViewModel.objAdminLoginList = String.IsNullOrEmpty(searchValue) ?
                           (CompanyId == helper.defaultCompany ? db.QryAdminLogins.OrderByDescending(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>() : db.QryAdminLogins.Where(c => c.CompanyId == CompanyId).OrderByDescending(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>()) :
                            objAdminLoginViewModel.objAdminLoginList.OrderByDescending(s => s.AdminLoginId).Skip(pageIndex).Take(pageSize).ToList<QryAdminLogins>();

                    return Ok(objAdminLoginViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/adminLogins/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateAdminLogin(int id, tblAdminLogin objAdminLogin)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                if (id != objAdminLogin.AdminLoginId)
                {
                    return BadRequest();
                }
                try
                {
                    var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                    if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                    {
                        var adminInDb = db.tblAdminLogin.SingleOrDefault(a => a.LoginName == objAdminLogin.LoginName && a.AdminLoginId != objAdminLogin.AdminLoginId);
                        if (adminInDb != null)
                            return BadRequest("Loginname already exists");

                        var adminEmailIdInDb = db.tblAdminLogin.SingleOrDefault(a => a.EmailId == objAdminLogin.EmailId && a.AdminLoginId != objAdminLogin.AdminLoginId);
                        if (adminEmailIdInDb != null)
                            return BadRequest("EmailId already exists.");

                        db.Entry(objAdminLogin).State = EntityState.Modified;
                        db.Entry(objAdminLogin).Property(x => x.CreatedBy).IsModified = false;
                        db.Entry(objAdminLogin).Property(x => x.CompanyId).IsModified = false;
                        db.SaveChanges();
                    }
                    else
                    {
                        return Unauthorized();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!doesAdminLoginExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/adminLogins
        [Authorize]
        [ResponseType(typeof(tblAdminLogin))]
        [HttpPost]
        public IHttpActionResult saveAdminLogin(tblAdminLogin objAdminLogin)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    objAdminLogin.CreatedBy = Convert.ToInt16(identityAdminLoginId.Value);
                    var adminInDb = db.tblAdminLogin.SingleOrDefault(a => a.LoginName == objAdminLogin.LoginName && a.AdminLoginId != objAdminLogin.AdminLoginId);
                    if (adminInDb != null)
                        return BadRequest("Login name already exists.");

                    var adminEmailIdInDb = db.tblAdminLogin.SingleOrDefault(a => a.EmailId == objAdminLogin.EmailId && a.AdminLoginId != objAdminLogin.AdminLoginId);
                    if (adminEmailIdInDb != null)
                        return BadRequest("EmailId already exists.");
                    db.tblAdminLogin.Add(objAdminLogin);
                    db.SaveChanges();

                    return Ok(objAdminLogin);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/adminLogins/5
        [Authorize]
        [ResponseType(typeof(tblAdminLogin))]
        [HttpDelete]
        public IHttpActionResult deleteAdminLogin(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                tblAdminLogin objAdminLogin = db.tblAdminLogin.Find(id);
                if (objAdminLogin == null)
                {
                    return NotFound();
                }
                db.tblAdminLogin.Remove(objAdminLogin);
                db.SaveChanges();
                return Ok(objAdminLogin);
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(adminLoginViewModel))]
        [Route("api/getEntityProfile")]
        [HttpGet]
        public IHttpActionResult getEntityProfile()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var userIdentityClaimValue = identity.Claims.Where(c => c.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(userIdentityClaimValue.Value);
                    var identityCompanyId = identity.Claims.Where(x => x.Type == "companyId").Single();
                    int CompanyId = Convert.ToInt32(identityCompanyId.Value);
                    adminLoginViewModel objAdminLoginViewModel = new adminLoginViewModel();
                    objAdminLoginViewModel.objAdminLogin = db.tblAdminLogin.Where(x => x.AdminLoginId == adminLoginId).SingleOrDefault();
                    objAdminLoginViewModel.objDepartmentList = (CompanyId == helper.defaultCompany ? db.tblDepartment_Master.OrderBy(c => c.DepartmentName).ToList() : db.tblDepartment_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.DepartmentName).ToList());
                    objAdminLoginViewModel.objDesignationList = (CompanyId == helper.defaultCompany ? db.tblDesignation_Master.OrderBy(c => c.DesignationName).ToList() : db.tblDesignation_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.DesignationName).ToList());
                    objAdminLoginViewModel.objRoleList = (CompanyId == helper.defaultCompany ? db.tblRole_Master.OrderBy(c => c.RoleName).ToList() : db.tblRole_Master.Where(c => c.CompanyId == CompanyId || c.CompanyId == helper.defaultCompany).OrderBy(c => c.RoleName).ToList());
                    objAdminLoginViewModel.objCountryList = db.tblCountry_Master.OrderBy(c => c.CountryName).ToList();
                    objAdminLoginViewModel.objStatusList = db.tblStatus_Master.OrderBy(c => c.StatusName).ToList();
                    objAdminLoginViewModel.objStateList = (objAdminLoginViewModel.objAdminLogin.CountryId == 0 ? new List<tblState_Master>() : db.tblState_Master.Where(x => x.CountryId == objAdminLoginViewModel.objAdminLogin.CountryId).ToList());
                    return Ok(objAdminLoginViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
                return Unauthorized();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool doesAdminLoginExists(int id)
        {
            return db.tblAdminLogin.Count(e => e.AdminLoginId == id) > 0;
        }
    }
}