﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;


namespace application.webAPI.Controllers
{
    public class cityMasterController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/cityMaster
        [Authorize]
        [ResponseType(typeof(cityViewModel))]
        [HttpGet]
        public IHttpActionResult GettblCity_Master(int id = 0, int stateId = 0)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                cityViewModel objCityViewModel = new cityViewModel();
                if (stateId == 0)
                {
                    objCityViewModel.objCity = new tblCity_Master();
                    objCityViewModel.objCityList = new List<QryCity>();
                    objCityViewModel.objStateList = db.tblState_Master.OrderBy(s => s.StateName).ToList();
                    objCityViewModel.objStatusList = db.tblStatus_Master.OrderBy(s => s.StatusName).ToList();
                }
                else
                {
                    objCityViewModel.objCityList = db.QryCity.Where(c => c.StateId == stateId).OrderBy(c => c.CityName).ToList();
                }

                return Ok(objCityViewModel);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/cityMaster/5
        [Authorize]
        [ResponseType(typeof(tblCity_Master))]
        public IHttpActionResult GettblCity_Master(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCity_Master objCity = db.tblCity_Master.Find(id);
                    if (objCity == null)
                    {
                        return NotFound();
                    }

                    return Ok(objCity);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(cityViewModel))]
        [HttpGet]
        public IHttpActionResult getCityByPagination(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    cityViewModel objCityViewModel = new cityViewModel();
                    objCityViewModel.totalRecords = db.tblCity_Master.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objCityViewModel.objCityList = db.QryCity.Where(s => s.CityName.Trim().ToLower() == searchValue.Trim().ToLower()
                        || s.StateName.Trim().ToLower().Contains(searchValue.Trim().ToLower())
                        || s.StatusName.Trim().ToLower() == searchValue.Trim().ToLower()).ToList();

                        objCityViewModel.filterRecords = objCityViewModel.objCityList.Count();
                    }
                    else
                        objCityViewModel.filterRecords = objCityViewModel.totalRecords;

                    if (sortDirection == "asc")
                        objCityViewModel.objCityList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCity.OrderBy(s => s.StateName).ThenBy(s => s.CityName).Skip(pageIndex).Take(pageSize).ToList<QryCity>() :
                            objCityViewModel.objCityList.OrderBy(s => s.StateName).ThenBy(s => s.CityName).Skip(pageIndex).Take(pageSize).ToList<QryCity>();
                    else
                        objCityViewModel.objCityList = String.IsNullOrEmpty(searchValue) ?
                            db.QryCity.OrderByDescending(s => s.StateName).ThenBy(s => s.CityName).Skip(pageIndex).Take(pageSize).ToList<QryCity>() :
                            objCityViewModel.objCityList.OrderByDescending(s => s.StateName).ThenBy(s => s.CityName).Skip(pageIndex).Take(pageSize).ToList<QryCity>();

                    return Ok(objCityViewModel);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/cityMaster/5
        [Authorize]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult updateCityData(int id, tblCity_Master objCity)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objCity.CityId)
                    {
                        return BadRequest();
                    }
                    try
                    {
                        var cityExistsInDb = db.tblCity_Master.SingleOrDefault(c => c.CityName == objCity.CityName && c.CityId != id);
                        if (cityExistsInDb != null)
                            return BadRequest("City Name already exists.");

                        db.Entry(objCity).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblCity_MasterExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }

                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/cityMaster
        [Authorize]
        [HttpPost]
        [ResponseType(typeof(tblCity_Master))]
        public IHttpActionResult PosttblCity_Master(tblCity_Master objCity)
        {

            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var cityExistsInDb = db.tblCity_Master.SingleOrDefault(c => c.CityName == objCity.CityName && c.CityId != objCity.CityId);
                    if (cityExistsInDb != null)
                        return BadRequest("City Name already exists");
                    db.tblCity_Master.Add(objCity);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/cityMaster/5
        [Authorize]
        [ResponseType(typeof(tblCity_Master))]
        [HttpDelete]
        public IHttpActionResult DeletetblCity_Master(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblCity_Master objCity = db.tblCity_Master.Find(id);
                    if (objCity == null)
                    {
                        return NotFound();
                    }

                    db.tblCity_Master.Remove(objCity);
                    db.SaveChanges();

                    return Ok(objCity);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblCity_MasterExists(int id)
        {
            return db.tblCity_Master.Count(e => e.CityId == id) > 0;
        }
    }
}