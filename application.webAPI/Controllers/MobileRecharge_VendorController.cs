﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class MobileRecharge_VendorController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/MobileRecharge_Vendor
        [Authorize]
        [HttpGet]
        public IHttpActionResult getMobileRechargeVendorDetail()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeVendorVM objMobileRechargeVendorVM = new mobileRechargeVendorVM();
                    objMobileRechargeVendorVM.objMobileRechargeOperatorList = db.QryMobileRecharge_Operator.ToList();
                    objMobileRechargeVendorVM.objMobileRechargeVendorList = new List<QryMobileRecharge_Vendor>();
                    //objMobileRechargeVendorVM.objMobileRechargeVendor = new tblMobileRecharge_Vendor();
                    return Ok(objMobileRechargeVendorVM);
                }
                else
                    return BadRequest(helper.invalidModuleAccess);
            }
            else
            {
                return Unauthorized();
            }
        }

        // GET: api/MobileRecharge_Vendor/5
        [Authorize]
        [HttpGet]
        public IHttpActionResult getMobileRechargeVendorById(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeVendorVM objMobileRechargeVendorVM = new mobileRechargeVendorVM();
                    objMobileRechargeVendorVM.objMobileRechargeOperatorList = db.QryMobileRecharge_Operator.ToList();
                    objMobileRechargeVendorVM.objMobileRechargeVendorList = new List<QryMobileRecharge_Vendor>();
                    objMobileRechargeVendorVM.objMobileRechargeVendor = db.tblMobileRecharge_Vendor.Find(id);
                    if (objMobileRechargeVendorVM.objMobileRechargeVendor == null)
                    {
                        return NotFound();
                    }
                    return Ok(objMobileRechargeVendorVM);
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // PUT: api/MobileRecharge_Vendor/5
        [Authorize]
        [HttpPut]
        public IHttpActionResult updateMobileRechargeVendor(int id, tblMobileRecharge_Vendor objMobileRecharge_Vendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    if (id != objMobileRecharge_Vendor.OperatorVendorId)
                    {
                        return BadRequest();
                    }

                    var vendorInDb = db.tblMobileRecharge_Vendor.SingleOrDefault(x => x.OperatorVendorId != id && x.OperatorId == objMobileRecharge_Vendor.OperatorId && x.VendorName == objMobileRecharge_Vendor.VendorName);
                    if (vendorInDb != null)
                        return BadRequest("Operator Vendor detail already found");

                    db.Entry(objMobileRecharge_Vendor).State = EntityState.Modified;
                    db.Entry(objMobileRecharge_Vendor).Property(x => x.CreatedDateTime).IsModified = false;
                    try
                    {
                        db.SaveChanges();
                        if (objMobileRecharge_Vendor.DefaultGateway)
                        {
                            var rowsAffected = db.Database.ExecuteSqlCommand("EXEC [dbo].[MobileRecharge_Vendor_Set_DefaultGateway] @OperatorId={0}, @OperatorVendorId={1}", Convert.ToString(objMobileRecharge_Vendor.OperatorId), Convert.ToString(objMobileRecharge_Vendor.OperatorVendorId));
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!tblMobileRecharge_VendorExists(id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [ResponseType(typeof(mobileRechargeVendorVM))]
        [HttpGet]
        public IHttpActionResult getMobileRechargeVendorList(int pageIndex, int pageSize, string searchValue, string sortDirection)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    mobileRechargeVendorVM objMobileRechargeVendorVM = new mobileRechargeVendorVM();
                    objMobileRechargeVendorVM.objMobileRechargeVendorList = db.QryMobileRecharge_Vendor.ToList();
                    objMobileRechargeVendorVM.totalRecords = objMobileRechargeVendorVM.objMobileRechargeVendorList.Count();
                    if (!String.IsNullOrEmpty(searchValue))
                    {
                        objMobileRechargeVendorVM.objMobileRechargeVendorList = objMobileRechargeVendorVM.objMobileRechargeVendorList.Where(s => s.OperatorName.Trim().ToLower().Contains(searchValue.Trim().ToLower()) ||
                        s.VendorName.Trim().ToLower().Contains(searchValue.Trim().ToLower())).ToList();
                        objMobileRechargeVendorVM.filterRecords = objMobileRechargeVendorVM.objMobileRechargeVendorList.Count();
                    }
                    else
                        objMobileRechargeVendorVM.filterRecords = objMobileRechargeVendorVM.totalRecords;

                    if (sortDirection == "asc")
                        objMobileRechargeVendorVM.objMobileRechargeVendorList = objMobileRechargeVendorVM.objMobileRechargeVendorList.OrderBy(s => s.VendorName).Skip(pageIndex).Take(pageSize).ToList<QryMobileRecharge_Vendor>();
                    else
                        objMobileRechargeVendorVM.objMobileRechargeVendorList = objMobileRechargeVendorVM.objMobileRechargeVendorList.OrderByDescending(s => s.VendorName).Skip(pageIndex).Take(pageSize).ToList<QryMobileRecharge_Vendor>();

                    return Ok(objMobileRechargeVendorVM);
                }
                else { return Unauthorized(); }
            }
            else { return Unauthorized(); }
        }


        // POST: api/MobileRecharge_Vendor
        [Authorize]
        [HttpPost]
        public IHttpActionResult saveMobileRechargeVendor(tblMobileRecharge_Vendor objMobileRecharge_Vendor)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityAdminLoginId = identity.Claims.Where(x => x.Type == "adminLoginId").Single();
                    long adminLoginId = Convert.ToInt64(identityAdminLoginId.Value);
                    var vendorInDb = db.tblMobileRecharge_Vendor.SingleOrDefault(x => x.OperatorVendorId != objMobileRecharge_Vendor.OperatorVendorId && x.OperatorId == objMobileRecharge_Vendor.OperatorId && x.VendorName == objMobileRecharge_Vendor.VendorName);
                    if (vendorInDb != null)
                        return BadRequest("Operator Vendor detail already found");

                    objMobileRecharge_Vendor.FetchBill = (objMobileRecharge_Vendor.FetchBill == null ? false : objMobileRecharge_Vendor.FetchBill);
                    objMobileRecharge_Vendor.BBPSEnabled = (objMobileRecharge_Vendor.BBPSEnabled == null ? false : objMobileRecharge_Vendor.BBPSEnabled);
                    objMobileRecharge_Vendor.OutletVerification = (objMobileRecharge_Vendor.OutletVerification == null ? false : objMobileRecharge_Vendor.OutletVerification);
                    objMobileRecharge_Vendor.CreatedDateTime = helper.getIndianTime();
                    objMobileRecharge_Vendor.CreatedBy = adminLoginId;
                    db.tblMobileRecharge_Vendor.Add(objMobileRecharge_Vendor);
                    db.SaveChanges();

                    if (objMobileRecharge_Vendor.DefaultGateway)
                    {
                        var rowsAffected = db.Database.ExecuteSqlCommand("EXEC [dbo].[MobileRecharge_Vendor_Set_DefaultGateway] @OperatorId={0}, @OperatorVendorId={1}", Convert.ToString(objMobileRecharge_Vendor.OperatorId), Convert.ToString(objMobileRecharge_Vendor.OperatorVendorId));
                    }

                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/MobileRecharge_Vendor/5
        [Authorize]
        [HttpDelete]
        public IHttpActionResult deleteMobileRechargeVendor(int id)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    tblMobileRecharge_Vendor objMobileRecharge_Vendor = db.tblMobileRecharge_Vendor.Find(id);
                    if (objMobileRecharge_Vendor == null)
                    {
                        return NotFound();
                    }
                    db.tblMobileRecharge_Vendor.Remove(objMobileRecharge_Vendor);
                    db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest(helper.invalidModuleAccess);
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblMobileRecharge_VendorExists(int id)
        {
            return db.tblMobileRecharge_Vendor.Count(e => e.OperatorVendorId == id) > 0;
        }
    }
}