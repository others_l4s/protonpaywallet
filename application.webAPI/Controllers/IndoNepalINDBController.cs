﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class IndoNepalINDBController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        [ResponseType(typeof(tblNepal_Remitter))]
        public IHttpActionResult IndoNepalINDB_saveRemitter(tblNepal_Remitter objNepal_Remitter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Remitter.SingleOrDefault(e => e.MobileNo == objNepal_Remitter.MobileNo);
            if (RemitterDB == null)
            {
                db.tblNepal_Remitter.Add(objNepal_Remitter);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_saveBeneficiary")]
        [ResponseType(typeof(tblNepal_Beneficiary))]
        public IHttpActionResult IndoNepalINDB_saveBeneficiary(tblNepal_Beneficiary objNepal_Beneficiary)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Beneficiary.SingleOrDefault(e => e.beneficiaryid == objNepal_Beneficiary.beneficiaryid && e.remitterid==objNepal_Beneficiary.remitterid);
            if (RemitterDB == null)
            {
                db.tblNepal_Beneficiary.Add(objNepal_Beneficiary);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_getBeneficiary")]
        [ResponseType(typeof(tblNepal_Beneficiary))]
        public IHttpActionResult IndoNepalINDB_getBeneficiary(tblNepal_Beneficiary objNepal_Beneficiary)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Beneficiary.SingleOrDefault(e => e.beneficiaryid == objNepal_Beneficiary.beneficiaryid && e.remitterid == objNepal_Beneficiary.remitterid);
            if (RemitterDB != null)
            {               
                return Ok(RemitterDB);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_deleteBeneficiary")]
        [ResponseType(typeof(tblNepal_Beneficiary))]
        public IHttpActionResult IndoNepalINDB_deleteBeneficiary(tblNepal_Beneficiary objNepal_Beneficiary)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Beneficiary.SingleOrDefault(e => e.beneficiaryid == objNepal_Beneficiary.beneficiaryid && e.remitterid == objNepal_Beneficiary.remitterid);
            if (RemitterDB != null)
            {
                db.tblNepal_Beneficiary.Remove(RemitterDB);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_saveTransaction")]
        [ResponseType(typeof(tblNepal_Transaction))]
        public IHttpActionResult IndoNepalINDB_saveTransaction(tblNepal_Transaction objtransaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Transaction.SingleOrDefault(e => e.NepalTransactionId == objtransaction.NepalTransactionId && e.ipay_id == objtransaction.ipay_id);
            if (RemitterDB == null)
            {
                db.tblNepal_Transaction.Add(objtransaction);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [ResponseType(typeof(tblNepal_Remitter))]
        [Route("api/IndoNepalINDB/GetRemitterDetailsById")]
        public IHttpActionResult GetRemitterDetailsById(tblNepal_Remitter objNepal_Remitter)
        {
            tblNepal_Remitter RemitterDB = db.tblNepal_Remitter.SingleOrDefault(e => e.MobileNo == objNepal_Remitter.MobileNo);
            if (RemitterDB != null)
            {                
                return Ok(RemitterDB);
            }
            else
            {
                return BadRequest();
            }
        }


        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_getTransaction")]
        public IHttpActionResult IndoNepalINDB_getTransaction(NepalTransactionView objTransaction)
        {
            List<NepalTransactionView> objTransactionList = new List<NepalTransactionView>();
            objTransactionList = db.NepalTransactionView.Where(e => e.RemitterID==objTransaction.RemitterID && e.BeneficiaryID==objTransaction.BeneficiaryID).OrderByDescending(e=>e.createddatetime).ToList();
            if (objTransactionList != null)
            {
                return Ok(objTransactionList);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("api/IndoNepalINDB/IndoNepalINDB_UpdateTransactionStatus")]
        [ResponseType(typeof(NepalTransactionView))]
        public IHttpActionResult IndoNepalINDB_UpdateTransactionStatus(NepalTransactionView objtransaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var RemitterDB = db.tblNepal_Transaction.SingleOrDefault(e => e.ipay_id == objtransaction.IpayID);
            if (RemitterDB != null)
            {
                
                RemitterDB.Status = true;
                RemitterDB.StatusCode = objtransaction.Statuscode;
                db.Entry(RemitterDB).State = EntityState.Modified;
                db.Entry(RemitterDB).Property(x => x.RemitterId).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.BeneficiaryId).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.ChargedAmount).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.ExchangeRate).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.TransactionAmount).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.Transfer_Amount).IsModified = false;                
                db.Entry(RemitterDB).Property(x => x.RequestApi).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.ResponseApi).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.CreatedDateTime).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.ChargedAmount).IsModified = false;
                db.Entry(RemitterDB).Property(x => x.ServiceCharge).IsModified = false;
                db.SaveChanges();
                return Ok(RemitterDB);
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
