﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;
using application.webAPI.viewModel;

namespace application.webAPI.Controllers
{
    public class SubMenu_Role_PermissionController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/SubMenu_Role_Permission
        [Authorize]
        [ResponseType(typeof(SubMenuRolePermissionVM))]
        [HttpGet]
        public IHttpActionResult getMenuSubMenuListByRoleId(int RoleId)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                SubMenuRolePermissionVM objRolePermissionVM = new SubMenuRolePermissionVM();
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) == helper.defaultCompany)
                {
                    var identityCompanyId = identity.Claims.Where(c => c.Type == "companyId").Single();
                    int companyId = Convert.ToInt32(identityCompanyId);
                    objRolePermissionVM.objCompanyList = (companyId == helper.defaultCompany ? db.tblCompany_Master.OrderBy(c => c.CompanyId).ToList() : db.tblCompany_Master.Where(c => c.CompanyId == companyId).OrderBy(c => c.CompanyId).ToList());
                    //For Main Menu
                    objRolePermissionVM.objMainMenuList = db.QryMainMenu_Permission_4_Role.Where(c => c.RoleId == RoleId).ToList();
                    List<int> mainMenuIds = objRolePermissionVM.objMainMenuList.Select(c => c.MainMenuId).ToList();
                    List<tblMainMenu_Master> objMainMenuList = (from a in db.tblMainMenu_Master where !mainMenuIds.Contains(a.MainMenuId) select a).ToList();
                    foreach (tblMainMenu_Master objData in objMainMenuList)
                    {
                        QryMainMenu_Permission_4_Role objRolePermission = new QryMainMenu_Permission_4_Role();
                        objRolePermission.MainMenuId = objData.MainMenuId;
                        objRolePermission.MainMenuName = objData.MainMenuName;
                        objRolePermission.RoleId = RoleId;
                        objRolePermission.CanView = false;
                        objRolePermission.CanEdit = false;
                        objRolePermission.CanAdd = false;
                        objRolePermission.CanDelete = false;
                        objRolePermissionVM.objMainMenuList.Add(objRolePermission);
                    }
                    //For Main Menu End
                    //For Sub Menu
                    objRolePermissionVM.objSubMenuList = db.QrySubMenu_Permission_4_Role.Where(c => c.RoleId == RoleId && mainMenuIds.Contains(c.MainMenuId)).ToList();
                    List<int> subMenuIds = objRolePermissionVM.objSubMenuList.Select(c => c.SubMenuId).ToList();
                    List<tblSubMenu_Master> objSubMenuList = (from a in db.tblSubMenu_Master where mainMenuIds.Contains(a.MainMenuId) && !subMenuIds.Contains(a.SubMenuId) select a).ToList();
                    foreach (tblSubMenu_Master objData in objSubMenuList)
                    {
                        QrySubMenu_Permission_4_Role objRolePermission = new QrySubMenu_Permission_4_Role();
                        objRolePermission.MainMenuId = objData.MainMenuId;
                        objRolePermission.SubMenuName = objData.SubMenuName;
                        objRolePermission.SubMenuId = objData.SubMenuId;
                        objRolePermission.RoleId = RoleId;
                        objRolePermission.CanView = false;
                        objRolePermission.CanEdit = false;
                        objRolePermission.CanAdd = false;
                        objRolePermission.CanDelete = false;
                        objRolePermissionVM.objSubMenuList.Add(objRolePermission);
                    }
                    //For Sub Menu End
                    objRolePermissionVM.objMainMenuList = objRolePermissionVM.objMainMenuList.OrderBy(c => c.MainMenuName).ToList();
                    objRolePermissionVM.objSubMenuList = objRolePermissionVM.objSubMenuList.OrderBy(c => c.SubMenuName).ToList();
                    return Ok(objRolePermissionVM);
                }
                else
                {
                    return Unauthorized();
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/SubMenu_Role_Permission
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult SaveSubMenuRolePermission(tblSubMenu_Role_Permission objRolePermission)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                objRolePermission.CreatedDateTime = helper.getIndianTime();
                var rolePermissionInDb = db.tblSubMenu_Role_Permission.SingleOrDefault(p => p.RoleId == objRolePermission.RoleId &&
                p.MainMenuId == objRolePermission.MainMenuId &&
                p.SubMenuId == objRolePermission.SubMenuId);

                if (rolePermissionInDb != null)
                {
                    rolePermissionInDb.CanView = objRolePermission.CanView;
                    rolePermissionInDb.CanEdit = objRolePermission.CanEdit;
                    rolePermissionInDb.CanAdd = objRolePermission.CanAdd;
                    rolePermissionInDb.CanDelete = objRolePermission.CanDelete;
                    rolePermissionInDb.CreatedDateTime = objRolePermission.CreatedDateTime;
                    rolePermissionInDb.RoleId = objRolePermission.RoleId;
                    db.Entry(rolePermissionInDb).State = EntityState.Modified;
                    db.Entry(rolePermissionInDb).Property(x => x.CreatedDateTime).IsModified = false;
                    db.SaveChanges();
                }
                else
                {
                    db.tblSubMenu_Role_Permission.Add(objRolePermission);
                    db.SaveChanges();
                }
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/SubMenu_Role_Permission/5
        [Authorize]
        [ResponseType(typeof(void))]
        public IHttpActionResult deleteSubMenuRolePermissionByRoleId(int RoleId)
        {
            db.tblSubMenu_Role_Permission.RemoveRange(db.tblSubMenu_Role_Permission.Where(c => c.RoleId == RoleId));
            db.SaveChanges();
            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSubMenu_Role_PermissionExists(long id)
        {
            return db.tblSubMenu_Role_Permission.Count(e => e.SubMenuPermissionId == id) > 0;
        }
    }
}