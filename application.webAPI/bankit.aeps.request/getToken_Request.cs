﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.aeps.request
{
    public class getToken_Request
    {
        public string agentAuthId { get; set; }
        public string agentAuthPassword { get; set; }
        public string retailerId { get; set; }
        public string apiId { get; set; }
    }
}