﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.aeps.request
{
    public class AEPSUpdateWalletRequest
    {
        public string rrn { get; set; }
        public string stanNo { get; set; }
        public string txnId { get; set; }
        public string aepstxnId { get; set; }
        public string action { get; set; }
        public string device { get; set; }
        public string status { get; set; }
        public string txnStatus { get; set; }
        public string bankName { get; set; }
        public string uId { get; set; }
        public string Agent_Id { get; set; }
        public string Service { get; set; }
        public decimal Amount { get; set; }

        public string ClientRefID { get; set; }
        public string agentId { get; set; }

        public string transactionDatetime { get; set; }

        public string deviceNo { get; set; }
    }
}