﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class remitter_details_request
    {
       
        public string token { get; set; }
        public remitter_details_req request { get; set; }
    }
}