﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class beneficiary_resend_otp_req
    {
        public string remitterid { get; set; }
        public string beneficiaryid { get; set; }
        public int outletid { get; set; }
    }
}