﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class account_validate_req
    {
        public string remittermobile { get; set; }
        public string account { get; set; }
        public string ifsc { get; set; }
        public string agentid { get; set; }
        public int outletid { get; set; }
    }
}