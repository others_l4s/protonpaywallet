﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class transaction_status_check_request
    {
        public string token { get; set; }
        public transaction_status_check_req request { get; set; }
    }
}