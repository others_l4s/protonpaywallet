﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class transactionStatusRequest
    {
        public string token { get; set; }
        public request request { get; set; }
    }

    public class request
    {
        public string order_id { get; set; }
    }
}