﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class transfer_req
    {
        public string remittermobile { get; set; }
        public string beneficiaryid { get; set; }
        public string agentid { get; set; }
        public string amount { get; set; }
        public string mode { get; set; }
    }
}