﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class InstantPayDMTTransactionView_Request
    {
        public string token { get; set; }
        public long InstantPayTransactionId { get; set; }
        public string ipay_id { get; set; }
        public string Amount { get; set; }
        public string Ref_no { get; set; }
        public string Opr_id { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string RemitterName { get; set; }
        public string RemitterSurname { get; set; }
        public string BeneficiaryName { get; set; }
        public string AccountNo { get; set; }
        public string Mode { get; set; }
        public string BeneficiaryId { get; set; }
        public string RemitterId { get; set; }
        public string APIStatus { get; set; }
        public string StatusCode { get; set; }
        public string Transaction_Status { get; set; }
    }
}