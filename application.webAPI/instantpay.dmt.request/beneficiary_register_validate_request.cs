﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class beneficiary_register_validate_request
    {
        public string token { get; set; }
        public beneficiary_register_validate_req request { get; set; }
    }
}