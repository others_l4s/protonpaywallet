﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class bank_details_req
    {
        public string account { get; set; }
        public int outletid { get; set; }
    }
}