﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class instantPayDMT_SaveBeneficiaryINDB
    {
        public string token { get; set; }
        public long instantPayBeneficiaryId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string ifsc { get; set; }
        public string AccountNo { get; set; }
        public string RemitterId { get; set; }
        public string BeneficiaryId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public string OTP { get; set; }
        public System.DateTime OTPVerifiedDateTime { get; set; }
        public long EntityId { get; set; }
        public long EntityUserId { get; set; }
        public bool IsOTPVerified { get; set; }
    }
}