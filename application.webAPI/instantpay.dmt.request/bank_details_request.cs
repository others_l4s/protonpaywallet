﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class bank_details_request
    {
        public string token { get; set; }
        public bank_details_req request { get; set; }
    }
}