﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class remitter_req
    {
        public string mobile { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string pincode { get; set; }
        public int outletid { get; set; }
    }
}