﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class RemitterDetailsReq
    {
        public string token { get; set; }
        public Request request { get; set; }
    }
}