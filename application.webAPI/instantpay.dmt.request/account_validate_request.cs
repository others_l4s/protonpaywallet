﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class account_validate_request
    {
        public string token { get; set; }
        public account_validate_req request { get; set; }
    }
}