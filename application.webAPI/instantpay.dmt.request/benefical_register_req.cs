﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class benefical_register_req
    {
        public string remitterid { get; set; }
        public string name { get; set; }
        public string mobile { get; set; }
        public string ifsc { get; set; }
        public string account { get; set; }
        public int outletid { get; set; }
    }
}