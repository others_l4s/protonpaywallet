﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class instantPayDMT_SaveTransaction
    {
        public string token { get; set; }
        public long InstantPayTransactionId { get; set; }
        public string RemitterMobileNo { get; set; }
        public string Name { get; set; }
        public string Mode { get; set; }
        public string BeneficiaryId { get; set; }
        public string ipay_id { get; set; }
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string StatusCode { get; set; }
        public string APIStatus { get; set; }
        public string Amount { get; set; }
        public string Ref_no { get; set; }
        public string Opr_id { get; set; }
        public string Opening_bal { get; set; }
        public string Locked_amt { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string Transaction_Status { get; set; }
    }
}