﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class beneficiary_remove_validate_req
    {
        public string beneficiaryid { get; set; }
        public string remitterid { get; set; }
        public string otp { get; set; }
        public int outletid { get; set; }
    }

}