﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class transfer_request
    {
        public string token { get; set; }
        public transfer_req request { get; set; }

        public Boolean fromMobileDevice { get; set; }
    }
}