﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class InstantPayDMT_SaveRemitterINDB
    {
        public string token { get; set; }
        public long InstantPayRemitterId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MobileNo { get; set; }
        public string Pincode { get; set; }
        public string RemitterId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public int IsVerified { get; set; }
        public string ipay_uuid { get; set; }
        public string OTP { get; set; }
        public long EntityUserId { get; set; }
        public long EntityId { get; set; }
    }
}