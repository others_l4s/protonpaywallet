﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public partial class Request
    {
        public string mobile { get; set; }
        public int outletid { get; set; } = 1;
    }
}