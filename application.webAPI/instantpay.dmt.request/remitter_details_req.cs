﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.instantpay.dmt.request
{
    public class remitter_details_req
    {
        public string mobile { get; set; }
        public int outletid { get; set; } = 1;
    }
}