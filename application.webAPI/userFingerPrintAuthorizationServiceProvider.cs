﻿using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Linq;
using Microsoft.Owin.Security.OAuth;
using application.webAPI.viewModel;
using application.webAPI.Models;
using Microsoft.Owin;
using System.Collections.Generic;

namespace application.webAPI
{
    public class userFingerPrintAuthorizationServiceProvider : OAuthAuthorizationServerProvider
    {
        private applicationDbEntities db = new applicationDbEntities();

        //Use for validate client authentication (Override)
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // have validated the client
        }

        //Validate user credentials
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            var validateInDb = db.QryEntityUser.Where(c => c.UserName.Trim().ToLower() == context.UserName.Trim().ToLower() 
            && c.IMEINo.Trim() == context.Password.Trim() && c.FingerPrintLoginEnabled == true).SingleOrDefault();
            if (validateInDb != null)
            {
                if (validateInDb.StatusName.Trim().ToLower() == "active")
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, validateInDb.RoleId.ToString()));
                    identity.AddClaim(new Claim("entityUserId", validateInDb.EntityUserId.ToString()));
                    identity.AddClaim(new Claim("entityId", validateInDb.EntityId.ToString()));
                    identity.AddClaim(new Claim("entityType", validateInDb.EntityTypeId.ToString()));
                    identity.AddClaim(new Claim("companyId", validateInDb.CompanyId.ToString()));
                    identity.AddClaim(new Claim(ClaimTypes.Name, validateInDb.PersonName));
                    context.Validated(identity);
                    context.Request.Context.Authentication.SignIn();
                }
                else
                {
                    context.SetError("Error", "Your status is " + validateInDb.StatusName + ", kindly contact administrator.");
                    return;
                }
            }
            else
            {
                context.SetError("Error", "Invalid Username or Password");
                return;
            }
        }
    }
}