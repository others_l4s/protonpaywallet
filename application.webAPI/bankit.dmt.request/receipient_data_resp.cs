﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class receipient_data_resp
    {
        public List<object> recipientList { get; set; }
    }
}