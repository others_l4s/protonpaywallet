﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class balance_request
    {
        [Required(ErrorMessage ="Please Enter UserToken")]
        public string token { get; set; }
    }
}