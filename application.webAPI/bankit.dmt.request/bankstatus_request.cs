﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class bankstatus_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        public string token { get; set; }
        public string bankName { get; set; }
        public string bankID { get; set; }
        public string status { get; set; }

    }
}