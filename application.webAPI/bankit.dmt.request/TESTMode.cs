﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class TESTMode
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string URL { get; set; }
        public string Token { get; set; }
        public string Host { get; set; }
    }
}