﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class bankitInDb_saveSender
    {
        public string token { get; set; }
        public long BankitSenderId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string CustomerID { get; set; }
        public string MobileNo { get; set; }
        public Nullable<bool> IsOTPverified { get; set; }
        public Nullable<int> EntityId { get; set; }
        public Nullable<decimal> WalletBalance { get; set; }
        public Nullable<bool> KYCStatus { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
    }
}