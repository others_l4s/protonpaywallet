﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class generic_otp_request
    {
        public string token { get; set; }
        public string agentCode { get; set; } = "1";
        public string customerId { get; set; }
    }
}