﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class recipient_add_request
    {
        public string agentCode { get; set; } = "1";
        public string bankName { get; set; }
        public string customerId { get; set; }
        public string accountNo { get; set; }
        public string ifsc { get; set; }
        public string mobileNo { get; set; }
        public string recipientName { get; set; }
    }
}