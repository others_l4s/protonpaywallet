﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class IMPS_remit_request
    {
        [Required(ErrorMessage = "Please Enter UserToken")]
        public string token { get; set; }
        public string agentCode { get; set; }
        public string recipientId { get; set; }
        public string customerId { get; set; }
        public string amount { get; set; }
        public string clientRefId { get; set; }
    }
}