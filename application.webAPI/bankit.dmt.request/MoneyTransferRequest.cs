﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class MoneyTransferRequest
    {
        public string agentCode { get; set; } = "1";
        public string recipientId { get; set; }
        public string recipientName { get; set; }
        public string bankName { get; set; }
        public string customerId { get; set; }
        public string amount { get; set; }
        public string clientRefId { get; set; }

        public string ParentClientRefId { get; set; }

        public Boolean fromMobileDevice { get; set; }
    }
}