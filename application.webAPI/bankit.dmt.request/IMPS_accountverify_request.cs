﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class IMPS_accountverify_request
    {
        public string agentCode { get; set; } = "1";
        public string customerId { get; set; }
        public string amount { get; set; }
        public string clientRefId { get; set; }
        public string udf1 { get; set; }
        public string udf2 { get; set; }
    }
}