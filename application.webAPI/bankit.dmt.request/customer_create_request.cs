﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.dmt.request
{
    public class customer_create_request
    {
        public string agentCode { get; set; } = "1";
        public string customerId { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public String dateOfBirth { get; set; }
        public string otp { get; set; }
    }
}