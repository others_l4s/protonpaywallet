﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class mobileRechargeOperatorTypeVM : commonProperties
    {
        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }
    }
}