﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityVM
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public tblEntity_Master objEntity { get; set; }
    }
}