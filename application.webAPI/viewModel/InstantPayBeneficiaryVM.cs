﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.instantpay.dmt.response;
namespace application.webAPI.viewModel
{
    public class InstantPayBeneficiaryVM : commonProperties
    {
        public remitter_resp objRemitterDetails { get; set; }
        public List<beneficiary_resp> objbeneficiaryList { get; set; }
    }
}