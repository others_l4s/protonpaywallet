﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class entityCommissionViewModel : commonProperties
    {
        public List<QryEntityCommission> objEntityCommisionMasterList { get; set; }
        public QryEntityCommission clsEntityCommission { get; set; }
        public tblEntity_Commission objEntityCommisionMaster { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }

        public int CommissionTypeId { get; set; }
    }
}