﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class MainMenuEntityTypePermissionVM
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<QryMainMenu_EntityType_Permission> objMainMenuList { get; set; }
        public List<QrySubMenu_EntityType_Permission> objSubMenuList { get; set; }
        public tblMenu_EntityType_Permission objMenuMenuPermissions { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }
    }
}