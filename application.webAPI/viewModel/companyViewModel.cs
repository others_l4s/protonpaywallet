﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class companyViewModel : commonProperties
    {
        public List<QryCompany> objCompanyList { get; set; }
        public tblCompany_Master objCompany { get; set; }

        public tblAdminLogin objCompanyUser { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblBankAccountType> objBankAccountTypeList { get; set; }
        public List<tblCompany_Type> objCompanyTypeList { get; set; }
    }
}