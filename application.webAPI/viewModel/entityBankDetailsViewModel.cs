﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityBankDetailsViewModel:commonProperties
    {
        public List<QryEntityBankDetails> objEntityBankDetailsList { get; set; }
        public tblEntity_Bank_Details objEntityBankDetails { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblBankAccountType> objBankAccountTypeList { get; set; }
    }
}