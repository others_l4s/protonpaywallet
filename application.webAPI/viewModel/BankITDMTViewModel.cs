﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.bankit.dmt.response;

namespace application.webAPI.viewModel
{
    public class BankITDMTViewModel
    {
        public banklist_response banklistdetailsMaster { get; set; }
        public banklist_resp bankMaster { get; set; }
        public List<banklist_resp> banklistMaster { get; set; }
    }
}