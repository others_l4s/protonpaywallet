﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class mobileLoginFingerPrintVM
    {
        public string IMEINo { get; set; }
        public string Username { get; set; }
        public long EntityUserId { get; set; }
    }
}