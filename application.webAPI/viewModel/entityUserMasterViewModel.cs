﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityUserMasterViewModel : commonProperties
    {
        public List<QryEntityUser> objEntityUserList { get; set; }

        public tblEntity_Users objEntityUser { get; set; }

        public List<tblRole_Master> objRoleList { get; set; }

        public List<tblCountry_Master> objCountryList { get; set; }

        public List<tblState_Master> objStateList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblEntity_Master> objEntityMasterList { get; set; }

        public List<tblDepartment_Master> objDepartmentList { get; set; }

        public List<tblDesignation_Master> objDesignationList { get; set; }

        public tblEntity_KYC objEntityKYC { get; set; }
    }
}