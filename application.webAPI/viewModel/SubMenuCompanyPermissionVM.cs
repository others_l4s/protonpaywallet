﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class SubMenuCompanyPermissionVM
    {
        public List<tblCompany_Master> objCompanyList { get; set; }
        public List<QryMainMenu_Permission_4_Company> objMainMenuList { get; set; }
        public List<QrySubMenu_Permission_4_Company> objSubMenuList { get; set; }
        public tblSubMenu_Company_Permission objSubMenuCompanyPermissions { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }
    }
}