﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;
namespace application.webAPI.viewModel
{
    public class NepalTransactionViewModel
    {
        public NepalTransactionView objTransaction { get; set; }
        public int totalRecords { get; set; }
        public int filterRecords { get; set; }
        public List<NepalTransactionView> objTransactionList { get; set; }

    }
}