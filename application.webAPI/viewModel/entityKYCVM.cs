﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityKYCVM : commonProperties
    {
        public List<tblEntity_KYC> objEntityKYCList { get; set; }
        public tblEntity_KYC objEntityKYC { get; set; }
    }
}