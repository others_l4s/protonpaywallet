﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class accountVM : commonProperties
    {
        public List<QryEntityAccounts> objEntityAccountList { get; set; }

        public double totalAmount { get; set; }
    }
}