﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class supportTicketListVM : commonProperties
    {
        public List<QrySupportSystemTickets> objSupportTicketList { get; set; }
    }
}