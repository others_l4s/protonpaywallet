﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class InstantPayDMTTransactionsVM : commonProperties
    {
        public List<QryInstantPay_DMT_Transactions> objInstantPayDMTTransactionsList { get; set; }
    }
}