﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class bookingStatusViewModel:commonProperties
    {
        public List<QryBookingStatus> objBookingStatusMasterList { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public tblBookingStatus_Master objBookingStatusMaster { get; set; }
    }
}