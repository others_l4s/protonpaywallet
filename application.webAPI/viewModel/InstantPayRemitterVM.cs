﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;
namespace application.webAPI.viewModel
{
    public class InstantPayRemitterVM : commonProperties
    {
        public List<tblInstantPay_Remitter> objInstantPayRemitterList { get; set; }
        public tblInstantPay_Remitter objInstantPayRemitter { get; set; }
    }
}