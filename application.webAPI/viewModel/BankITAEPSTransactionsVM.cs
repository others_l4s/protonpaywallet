﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class BankITAEPSTransactionsVM : commonProperties
    {
        public List<QryBankIT_AEPS_MicroATM_Transactions> objBankITAEPSTransactionsList { get; set; }
    }
}