﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class documentMasterViewModel : commonProperties
    {
        public List<QryDocumentMaster> objDocumentMasterList { get; set; }

        public tblDocument_Master objDocumentMaster { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}