﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class supportTicketVM 
    {
        public tblSupportSystem_Tickets objSupportTicket { get; set; }
        public List<tblSupportSystem_Problem_Master> objSupportProblemList { get; set; }
        public List<tblSupportSystem_Details> objSupportTicketDetail { get; set; }

        public List<QrySupportSystemTickets> objSupportTicketList { get; set; }
    }
}