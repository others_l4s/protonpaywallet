﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class BankITSenderViewModel : commonProperties
    {
        public List<tblBankit_Sender> clsBankItSenderList { get; set; }
        public tblBankit_Sender clsBankItSender { get; set; }
    }
}