﻿using application.webAPI.bankit.aeps.request;
using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace application.webAPI.viewModel
{
    public class editProfileKYCVM
    {
        public editProfileKYC objEditProfile { get; set; }
        public List<tblState_Master> objStateList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }
        public List<tblCountry_Master> objCountryList { get; set; }
    }

    public class editProfileKYC
    {
        public long EntityUserId { get; set; }
        public long EntityId { get; set; }
        public string UserName { get; set; }
        public string PersonName { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public long StateId { get; set; }
        public long CityId { get; set; }
        public long CountryId { get; set; }
        public string PinCode { get; set; }
        public string DOB { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string MobileOTP { get; set; }
        public string PanDocumentId { get; set; }
        public string PanCardNo { get; set; }
        public string PanCardPhoto { get; set; }
        public string AadhaarCardDocumentId { get; set; }
        public string AadhaarCardNo { get; set; }
        public string AadhaarCardPhoto { get; set; }
        public bool IsKYCStatus { get; set; }
        public string KYCStatusRemark { get; set; }
        public bool FromMobileDevice { get; set; }
        public string OutletId { get; set; }
        public bool OutletMobileVerified { get; set; }
        public string OutletDocumentGetStatusResponse { get; set; }
        public long EntityKYCId { get; set; }
    }
}