﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class webLoginVM
    {
        public Boolean OTPVerificationRequired { get; set; }
        public Int32 entityTypeId { get; set; }
        public string userName { get; set; }
        public string mobileNo { get; set; }
        public long entityUserId { get; set; }
    }
}