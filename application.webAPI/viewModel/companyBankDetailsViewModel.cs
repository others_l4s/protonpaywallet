﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class companyBankDetailsViewModel:commonProperties
    {
        public List<QryCompanyBankDetail> objCompanyBankDetailsList { get; set; }
        public tblCompany_Bank_Details objCompanyBankDetails { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblBankAccountType> objBankAccountTypeList { get; set; }
    }
}