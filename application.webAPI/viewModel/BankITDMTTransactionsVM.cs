﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class BankITDMTTransactionsVM : commonProperties
    {
        public List<QryBankIT_DMT_Transactions> objBankITDMTTransactionsList { get; set; }
    }
}