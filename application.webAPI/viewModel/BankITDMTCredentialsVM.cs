﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.bankit.dmt.request;

namespace application.webAPI.viewModel
{
    public class BankITDMTCredentialsVM
    {
        public LIVEMode LiveMode { get; set; }
        public TESTMode TestMode { get; set; }
    }
}