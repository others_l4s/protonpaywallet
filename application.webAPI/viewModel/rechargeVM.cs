﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class rechargeVM
    {
        public List<tblInstantPay_UtilityData> objInstantPayUtilityDataList { get; set; }
        public tblInstantPay_Recharge_Transaction objRechargeTransaction { get; set; }
    }
}