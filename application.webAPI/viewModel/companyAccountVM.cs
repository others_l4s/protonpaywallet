﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class companyAccountVM : commonProperties
    {
        public List<QryCompanyAccount> objCompanyAccountList { get; set; }
    }
}