﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;


namespace application.webAPI.viewModel
{
    public class entityMasterViewModel : commonProperties
    {
        public List<QryEntityMaster> objEntityMasterList { get; set; }

        public tblEntity_Master objEntityMaster { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblState_Master> objStateList { get; set; }

        public List<tblCity_Master> objCityList { get; set; }

        public List<tblCountry_Master> objCountryList { get; set; }

        public List<tblEntity_Type> objEntityTypeList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblAdminLogin> objAccountHandlerList { get; set; }

        public List<tblBankAccountType> objBankAccountTypeList { get; set; }

        public List<tblEntity_Master> objParentList { get; set; }
        public List<tblEntity_Master> objDistributorList { get; set; }

        public long MDParentEntityId { get; set; }

        public tblEntity_Users objEntityUser { get; set; }
    }
}