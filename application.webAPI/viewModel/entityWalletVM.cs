﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityWalletVM : commonProperties
    {
        public List<QryEntityWallet> objEntityWalletList { get; set; }
    }
}