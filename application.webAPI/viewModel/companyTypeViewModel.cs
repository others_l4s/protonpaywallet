﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace application.webAPI.viewModel
{
    public class companyTypeViewModel : commonProperties
    {
        public List<QryCompanyType> objComapanyTypeMasterList { get; set; }
        public tblCompany_Type objCompanyType { get; set; }

        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}