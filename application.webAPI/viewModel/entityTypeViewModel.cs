﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class entityTypeViewModel:commonProperties
    {
        public List<QryEntityType> objEntityTypeList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public tblEntity_Type objEntityType { get; set; }
    }
}