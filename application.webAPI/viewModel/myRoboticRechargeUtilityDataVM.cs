﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class myRoboticRechargeUtilityDataVM:commonProperties
    {
        public List<QryMyRobotic_Recharge_UtilityData> objMyRoboticRechargeUtilityDataList { get; set; }
        public tblMyRobotic_Recharge_UtilityData objMyRoboticRechargeUtilityData { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
    }
}