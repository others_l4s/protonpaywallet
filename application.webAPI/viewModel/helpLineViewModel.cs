﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class helpLineViewModel:commonProperties
    {
        public List<QryHelpLineMaster> objHelpLineList { get; set; }
        public tblHelpline_Number objHelpLine { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
        public List<tblCompany_Master> objCompanyMasterList { get; set; }
    }
}