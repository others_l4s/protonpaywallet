﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class BankITAEPSRequestVM : commonProperties
    {
        public List<QryBankITAEPS_Request> objBankITAEPSRequestList { get; set; }
    }
}