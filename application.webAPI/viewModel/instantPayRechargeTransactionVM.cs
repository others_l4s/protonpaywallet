﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class instantPayRechargeTransactionVM : commonProperties
    {
        public List<QryInstantPay_Recharge_Transaction> objRechargeTransactionVM { get; set; }
    }
}