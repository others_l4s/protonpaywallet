﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class instantPayUtilityDataVM : commonProperties
    {
        public List<QryInstantPay_UtilityData> objInstantPayUtilityDataList { get; set; }
        public tblInstantPay_UtilityData objInstantPayUtilityData { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
    }
}