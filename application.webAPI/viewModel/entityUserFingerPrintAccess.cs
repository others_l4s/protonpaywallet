﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class entityUserFingerPrintAccess
    {
        public long EntityUserId { get; set; }
        public string IMEINo { get; set; }
        public bool FingerPrintLoginEnabled { get; set; }
    }
}