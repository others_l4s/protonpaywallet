﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class productMasterViewModel : commonProperties
    {
        public List<QryProductMaster> objProductList { get; set; }
    }
}