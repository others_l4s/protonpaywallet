﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;

namespace application.webAPI.viewModel
{
    public class AnnouncementViewModel : commonProperties
    {
        public List<QryAnnouncement> objAnnouncementMasterList { get; set; }
        public tblAnnouncement_Master objAnnouncementMaster { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblCompany_Master> objCompanyMasterList { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}