﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.bankit.dmt.response;

namespace application.webAPI.viewModel
{
    public class BankITRecipientViewModel : commonProperties
    {
        public List<recipient_fetch_resp> clsRecipientResponseList { get; set; }
    }
}