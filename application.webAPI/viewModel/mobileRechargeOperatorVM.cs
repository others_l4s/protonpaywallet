﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class mobileRechargeOperatorVM : commonProperties
    {
        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }
        public List<QryMobileRecharge_Operator> objMobileRechargeOperatorList { get; set; }

        public tblMobileRecharge_Operator objMobileRechargeOperator { get; set; }
    }
}