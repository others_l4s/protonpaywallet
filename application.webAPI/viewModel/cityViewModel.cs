﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;


namespace application.webAPI.viewModel
{
    public class cityViewModel:commonProperties
    {
        public List<QryCity> objCityList { get; set; }
        public tblCity_Master objCity { get; set; }
        public List<tblState_Master> objStateList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
    }
}