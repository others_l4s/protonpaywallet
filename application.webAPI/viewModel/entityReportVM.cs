﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class entityReportVM
    {
        public List<tblEntity_Type> objEntityTypeList { get; set; }

        public List<tblEntity_Master> objEntityList { get; set; }
    }
}