﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class mobileRechargeVendorVM : commonProperties
    {
        public List<QryMobileRecharge_Operator> objMobileRechargeOperatorList { get; set; }
        public List<QryMobileRecharge_Vendor> objMobileRechargeVendorList { get; set; }
        public tblMobileRecharge_Vendor objMobileRechargeVendor { get; set; }
    }
}