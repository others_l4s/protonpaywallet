﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace application.webAPI.viewModel
{
    public class designationViewModel:commonProperties
    {
        public List<QryDesignation> objDesignationList { get; set; }
        public tblDesignation_Master objDesignation { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}