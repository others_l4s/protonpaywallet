﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class companySubscriptionViewModel : commonProperties
    {
        public List<QryCompany_Subscription> objCompanySubscriptionList { get; set; }
        public tblCompany_Subscription objCompanySubscription { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<QrySubscriptionProduct> objSubscriptionList { get; set; }
    }
}