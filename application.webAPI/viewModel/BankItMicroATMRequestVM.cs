﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class BankItMicroATMRequestVM : commonProperties
    {
        public List<QryBankITMicroATM_Request> objBankItMicroATMRequestList { get; set; }

    }
}