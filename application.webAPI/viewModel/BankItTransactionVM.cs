﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class BankItTransactionVM : commonProperties
    {
        public List<tblBankit_Transaction> objBankItTransactionList { get; set; }
    }
}