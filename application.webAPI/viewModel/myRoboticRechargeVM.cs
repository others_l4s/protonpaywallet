﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class myRoboticRechargeVM
    {
        public List<tblMyRobotic_Recharge_UtilityData> objMyRoboticUtilityDataList { get; set; }
        public tblMyRobotic_Recharge_Transaction objMyRoboticRechargeTransaction { get; set; }
    }
}