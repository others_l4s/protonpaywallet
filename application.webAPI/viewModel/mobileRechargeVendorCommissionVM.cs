﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class mobileRechargeVendorCommissionVM : commonProperties
    {
        public List<string> objMobileRechargeDistinctVendorList { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<QryMobileRecharge_VendorCommission> objMobileRechargeVendorCommissionList { get; set; }
        public IEnumerable<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblMobileRecharge_OperatorType> objMobileRechargeOperatorTypeList { get; set; }
        
    }
}