﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class subscriptionProductViewModel:commonProperties
    {
        public List<QrySubscriptionProduct> objSubscriptionProductList { get; set; }
        public tblSubscription_Products objSubscriptionProduct { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblSubscription_Master> objSubscriptionList { get; set; }
    }
}