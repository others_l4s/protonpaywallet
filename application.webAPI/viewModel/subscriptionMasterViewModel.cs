﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class subscriptionMasterViewModel : commonProperties
    {
        public List<QrySubscription> objSubscriptionList { get; set; }

        public tblSubscription_Master objSubscriptionMaster { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}