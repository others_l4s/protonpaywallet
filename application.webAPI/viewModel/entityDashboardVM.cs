﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class entityDashboardVM
    {
        public decimal WalletBalance { get; set; }

        public List<QryEntityAccounts> objEntityAccountList { get; set; }

        public QryEntityKYC objEntityKYC { get; set; }
    }
}