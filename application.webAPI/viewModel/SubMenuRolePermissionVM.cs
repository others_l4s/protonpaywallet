﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class SubMenuRolePermissionVM
    {
        public List<tblCompany_Master> objCompanyList { get; set; }
        public List<QryMainMenu_Permission_4_Role> objMainMenuList { get; set; }
        public List<QrySubMenu_Permission_4_Role> objSubMenuList { get; set; }
        public tblSubMenu_Role_Permission objSubMenuRolePermissions { get; set; }
        public List<tblMainMenu_Master> clsMainMenuList { get; set; }
    }
}