﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class statusViewModel : commonProperties
    {
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public tblStatus_Master objStatusMaster { get; set; }
    }
}