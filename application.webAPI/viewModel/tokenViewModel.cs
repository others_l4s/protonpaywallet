﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class tokenViewModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }

        public long entityUserId { get; set; }
        public long entityId { get; set; }
        public string entityType { get; set; }

        public string agencyName { get; set; }

        public string walletBalance { get; set; }
        public Boolean lowBalanceAlert { get; set; }

        public Boolean OTPVerificationRequired { get; set; }

        public string mobileNo { get; set; }
        public Boolean? FingerPrintLoginEnabled { get; set; }
    }
}