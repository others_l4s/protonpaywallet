﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class subMenuMasterViewModel : commonProperties
    {
        public List<QrySubMenu> objSubMenuList { get; set; }
        public tblSubMenu_Master objSubMenu { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
        public List<tblMainMenu_Master> objMainMenuList { get; set; }
      
    }
}