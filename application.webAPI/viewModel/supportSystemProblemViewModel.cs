﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class supportSystemProblemViewModel : commonProperties
    {
        public List<QrySupportSystemProblem> objSupportSystemProblemList { get; set; }
        public tblSupportSystem_Problem_Master objSupportSystemProblem { get; set; }
        public List<tblDepartment_Master> objDepartmentList { get; set; }       
        public List<tblStatus_Master> objStatusMasterList { get; set; }
        public List<tblProduct_Master> objProductList { get; set; }
    }
}