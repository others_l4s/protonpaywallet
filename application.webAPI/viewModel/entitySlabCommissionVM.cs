﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entitySlabCommissionVM : commonProperties
    {
        public List<QryEntity_Slab_Commission> objEntitySlabcommissionList { get; set; }
        public List<QryEntity_Slab_Commission> objEntityDownlineSlabCommissionList { get; set; }
        public QryEntity_Slab_Commission clsEntitySlabCommission { get; set; }
        public tblEntity_Slab_Commission objEntitySlabCommission { get; set; }
        public List<tblProduct_Master> objProductMasterList { get; set; }
        public List<tblCommissionType> objCommissionTypeList { get; set; }
        public List<tblEntity_Type> objEntityTypeList { get; set; }
        public List<tblEntity_Master> objEntityList { get; set; }
        public int CommissionTypeId { get; set; }
    }
}