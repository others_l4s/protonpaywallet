﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class countryViewModel : commonProperties
    {
        public List<QryCountry> objCountryMasterList { get; set; }
        public tblCountry_Master objCountry { get; set; }
    }
}