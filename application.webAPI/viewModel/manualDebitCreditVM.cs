﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class manualDebitCreditVM:commonProperties
    {
        public List<QryManual_DebitCredit> objManualDebitCreditList { get; set; }
        public tblManual_DebitCredit objManualDebitCredit { get; set; }

        public List<tblEntity_Type> objEntityTypeList { get; set; }

        public List<tblProduct_Master> objProductList { get; set;}
    }
}