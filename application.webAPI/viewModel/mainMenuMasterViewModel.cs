﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class mainMenuMasterViewModel : commonProperties
    {
        public List<QryMainMenu> objMainMenuList { get; set; }
        public tblMainMenu_Master objMainMenu { get; set; }
        public List<tblStatus_Master> objStatusList { get; set; }
    }
}