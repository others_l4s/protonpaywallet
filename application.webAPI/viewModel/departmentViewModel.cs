﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.viewModel
{
    public class departmentViewModel:commonProperties
    {
        public List<QryDepartment> objDepartmentList { get; set; }
        public tblDepartment_Master objDepartment { get; set; }

        public List<tblCompany_Master> objCompanyList { get; set; }

        public List<tblStatus_Master> objStatusList { get; set; }
    }
}