﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class makePaymentVM : commonProperties
    {
        public tblEntity_Payments objEntityPayments { get; set; }
        public List<tblEntity_Bank_Details> objEntityBankList { get; set; }
        public List<tblCompany_Bank_Details> objCompanyBankList { get; set; }
        public List<tblPaymentMode_Master> objPaymentModeList { get; set; }
        public List<QryEntityPayments> objEntityPaymentList { get; set; }

        public List<QryEntityPayments_HO> objEntityPaymentListHO { get; set; }

        public string parentAgencyName { get; set; }
    }
}