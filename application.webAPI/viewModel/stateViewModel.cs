﻿using application.webAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace application.webAPI.viewModel
{
    public class stateViewModel:commonProperties
    {
        public List<tblCountry_Master> objCountryMasterList { get; set; }
        public List<QryState> objStateMasterList { get; set; }
        public tblState_Master objStateMaster { get; set; }
        public List<tblStatus_Master> objStatusMasterList { get; set; }
    }
}