﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using application.webAPI.Models;

namespace application.webAPI.viewModel
{
    public class entityWalletController : ApiController
    {
        private applicationDbEntities db = new applicationDbEntities();

        // GET: api/entityWallet/5
        [Authorize]
        [ResponseType(typeof(tblEntity_Wallet))]
        [Route("api/getWalletBalance")]
        public IHttpActionResult getWalletBalance()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    tblEntity_Wallet objEntityWallet = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
                    if (objEntityWallet == null)
                    {
                        return NotFound();
                    }

                    return Ok(objEntityWallet);
                }
                else
                {
                    return BadRequest("Unable to fetch Wallet Balance. Invalid Access.");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // POST: api/entityWallet
        [Authorize]
        [ResponseType(typeof(tblEntity_Wallet))]
        [Route("api/updateWalletBalance")]
        [HttpPost]
        public IHttpActionResult updateWalletBalance(tblEntity_Wallet objEntityWallet)
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityType = identity.Claims.Where(c => c.Type == "entityType").Single();
                if (Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany)
                {
                    var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                    long entityId = Convert.ToInt64(identityEntityId.Value);
                    objEntityWallet.EntityId = entityId;
                    var entityWalletExists = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
                    if (entityWalletExists == null)
                    {
                        db.tblEntity_Wallet.Add(objEntityWallet);
                    }
                    else
                    {
                        db.Entry(objEntityWallet).State = EntityState.Modified;
                        db.Entry(objEntityWallet).Property(x => x.EntityId).IsModified = false;
                    }
                    db.SaveChanges();
                    return Ok(objEntityWallet);
                }
                else
                {
                    return BadRequest("Unable to update Wallet. Invalid Access");
                }
            }
            else
            {
                return Unauthorized();
            }
        }

        // DELETE: api/entityWallet/5
        [Authorize]
        [ResponseType(typeof(void))]
        [Route("api/deleteWallet")]
        public IHttpActionResult deleteWallet()
        {
            var identity = (ClaimsIdentity)User.Identity;
            if (identity.IsAuthenticated)
            {
                var identityEntityId = identity.Claims.Where(c => c.Type == "entityId").Single();
                long entityId = Convert.ToInt64(identityEntityId.Value);
                tblEntity_Wallet objEntityWallet = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
                if (objEntityWallet == null)
                {
                    return NotFound();
                }
                db.tblEntity_Wallet.Remove(objEntityWallet);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return Unauthorized();
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}