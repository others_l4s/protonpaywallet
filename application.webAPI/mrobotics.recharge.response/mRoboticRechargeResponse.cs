﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.mrobotics.recharge.response
{
    public class mRoboticRechargeResponse
    {
        public double balance { get; set; }

        public long roffer { get; set; }

        public string status { get; set; }
        
        public string recharge_date { get; set; }

        public string id { get; set; }

        public string response { get; set; }
        public long lapu_id { get; set; }

        public string mobile_no { get; set; }

        public Int16 amount { get; set; }
        public string tnx_id { get; set; }
    }
}