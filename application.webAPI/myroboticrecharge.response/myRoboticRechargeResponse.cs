﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.myroboticrecharge.response
{
    public class myRoboticRechargeResponse
    {
        public int txid { get; set; }
        public string status { get; set; }
        public string opid { get; set; }
        public string number { get; set; }
        public int amount { get; set; }
        public string orderid { get; set; }
        public int r_offer { get; set; }
        public double balance { get; set; }
        public int robotic_id { get; set; }
        public string lapu_no { get; set; }
        public string date { get; set; }
    }
}