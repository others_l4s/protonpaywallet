//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class QryMobileRecharge_Vendor
    {
        public int OperatorVendorId { get; set; }
        public long OperatorId { get; set; }
        public string OperatorName { get; set; }
        public long OperatorTypeId { get; set; }
        public string OperatorTypeName { get; set; }
        public string VendorName { get; set; }
        public string OperatorAlias { get; set; }
        public string Key1 { get; set; }
        public string Key2 { get; set; }
        public string Key3 { get; set; }
        public string Key4 { get; set; }
        public string Key5 { get; set; }
        public string Key6 { get; set; }
        public string Key7 { get; set; }
        public string Key8 { get; set; }
        public string Key9 { get; set; }
        public string Key10 { get; set; }
        public string APIURL { get; set; }
        public bool DefaultGateway { get; set; }
        public bool Status { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public decimal DefaultCommissionValue { get; set; }
        public string OperatorNType { get; set; }
        public Nullable<bool> FetchBill { get; set; }
        public Nullable<bool> BBPSEnabled { get; set; }
        public Nullable<bool> OutletVerification { get; set; }
        public string OperatorImage { get; set; }
        public string OperatorTypeImage { get; set; }
    }
}
