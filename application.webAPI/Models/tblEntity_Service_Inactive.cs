//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEntity_Service_Inactive
    {
        public long ServiceOffId { get; set; }
        public long EntityId { get; set; }
        public long ProductId { get; set; }
        public System.DateTime InactiveDateTime { get; set; }
    }
}
