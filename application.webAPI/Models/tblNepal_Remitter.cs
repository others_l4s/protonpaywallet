//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblNepal_Remitter
    {
        public long NepalRemitterID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string RemitterId { get; set; }
        public string MobileNo { get; set; }
        public Nullable<bool> IsOTPVerified { get; set; }
        public string DocumentType { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentUrl { get; set; }
        public string Gender { get; set; }
        public string Pincode { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
    }
}
