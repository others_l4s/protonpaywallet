//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class QryManual_DebitCredit
    {
        public long ManualWalletUpdateId { get; set; }
        public int CompanyId { get; set; }
        public long EntityId { get; set; }
        public decimal Amount { get; set; }
        public int ProductId { get; set; }
        public string Remark { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public bool IsCredit { get; set; }
        public string AgencyName { get; set; }
        public long ParentEntityId { get; set; }
        public bool DoneByParent { get; set; }
        public string ParentAgencyName { get; set; }
        public long DoneByParentEntityId { get; set; }
    }
}
