﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.Models
{
    public class userIdentity
    {
        public int entityTypeId { get; set; }
        public long entityId { get; set; }
        public long entityUserId { get; set; }
        public Boolean isCompany { get; set; }
        public long adminLoginId { get; set; }
        public int companyId { get; set; }
    }
}