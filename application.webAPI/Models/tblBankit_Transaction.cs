//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBankit_Transaction
    {
        public long BankItTransactionId { get; set; }
        public string CustomerId { get; set; }
        public string TransferType { get; set; }
        public string RecipientId { get; set; }
        public string RecipientName { get; set; }
        public string BankName { get; set; }
        public string ClientRefId { get; set; }
        public string TxnId { get; set; }
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long EntityId { get; set; }
        public long EntityUserId { get; set; }
        public decimal Amount { get; set; }
        public string ParentClientRefId { get; set; }
        public Nullable<bool> FromMobileDevice { get; set; }
        public string IPAddress { get; set; }
        public string UpdateResponseApi { get; set; }
    }
}
