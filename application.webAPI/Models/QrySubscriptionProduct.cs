//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class QrySubscriptionProduct
    {
        public int SubscriptionProductId { get; set; }
        public int SubscriptionId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int StatusId { get; set; }
        public string ProductCode { get; set; }
        public string SubscriptionName { get; set; }
        public string StatusName { get; set; }
    }
}
