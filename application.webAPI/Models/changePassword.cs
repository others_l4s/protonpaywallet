﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.Models
{
    public class changePassword
    {
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
    }
}