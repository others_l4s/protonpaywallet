//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSMS_Log
    {
        public long SMSID { get; set; }
        public string MobileNo { get; set; }
        public string SMSMessage { get; set; }
        public System.DateTime SendDateTime { get; set; }
        public string SMSSendResponse { get; set; }
        public long EntityUserId { get; set; }
    }
}
