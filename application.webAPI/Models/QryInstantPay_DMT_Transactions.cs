//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class QryInstantPay_DMT_Transactions
    {
        public long InstantPayTransactionId { get; set; }
        public string RemitterMobileNo { get; set; }
        public string Name { get; set; }
        public string Mode { get; set; }
        public string BeneficiaryId { get; set; }
        public string ipay_id { get; set; }
        public string RequestApi { get; set; }
        public string ResponseApi { get; set; }
        public string StatusCode { get; set; }
        public string APIStatus { get; set; }
        public string Amount { get; set; }
        public string Ref_no { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public string Transaction_Status { get; set; }
        public Nullable<long> EntityId { get; set; }
        public Nullable<long> EntityUserId { get; set; }
        public string RemitterName { get; set; }
        public string Surname { get; set; }
        public string RemitterId { get; set; }
        public string AgencyName { get; set; }
        public string Telephone1 { get; set; }
        public Nullable<bool> FromMobileDevice { get; set; }
        public string ClientRefId { get; set; }
        public string ParentClientRefId { get; set; }
        public int CompanyId { get; set; }
        public string ifsc { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public long ParentEntityId { get; set; }
        public string IPAddress { get; set; }
        public string UpdateResponseApi { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Alert_Mobile { get; set; }
        public string Alert_Email { get; set; }
        public string PayoutDirectRemark { get; set; }
        public Nullable<bool> IsPayoutDirect { get; set; }
    }
}
