//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblCompany_Account
    {
        public long AccountId { get; set; }
        public int CompanyId { get; set; }
        public decimal Amount { get; set; }
        public decimal RunningBalance { get; set; }
        public string AccountTransactionId { get; set; }
        public string Description { get; set; }
        public int TransactionTypeId { get; set; }
        public bool IsCredit { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long AdminUserId { get; set; }
        public long EntityId { get; set; }
    }
}
