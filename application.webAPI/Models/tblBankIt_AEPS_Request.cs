//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblBankIt_AEPS_Request
    {
        public long BankItAEPSId { get; set; }
        public long EntityId { get; set; }
        public string AgentAuthId { get; set; }
        public string AgentAuthPassword { get; set; }
        public string RetailerId { get; set; }
        public string APIId { get; set; }
        public System.DateTime AEPSRequestedOnDateTime { get; set; }
        public Nullable<System.DateTime> AEPSApprovedOnDateTime { get; set; }
        public string Remark { get; set; }
        public Nullable<bool> AEPSRejected { get; set; }
        public int AccessAmount { get; set; }
    }
}
