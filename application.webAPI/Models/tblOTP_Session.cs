//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblOTP_Session
    {
        public long OTPSessionId { get; set; }
        public int EntityTypeId { get; set; }
        public long EntityUserId { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string OTP { get; set; }
        public bool OTPVerified { get; set; }
        public Nullable<System.DateTime> OTPVerifiedDateTime { get; set; }
        public System.DateTime OTPSendDateTime { get; set; }
        public bool FromMobileDevice { get; set; }
    }
}
