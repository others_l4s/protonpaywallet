//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblInstantPay_Recharge_Transaction
    {
        public long InstantPayRechargeId { get; set; }
        public long EntityId { get; set; }
        public long EntityUserId { get; set; }
        public int InstantPayRechargeDataId { get; set; }
        public string MobileNo { get; set; }
        public string RechargeRequest { get; set; }
        public string RechargeResponse { get; set; }
        public string status { get; set; }
        public string res_code { get; set; }
        public string res_msg { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public int Amount { get; set; }
        public string CustomerMobile { get; set; }
    }
}
