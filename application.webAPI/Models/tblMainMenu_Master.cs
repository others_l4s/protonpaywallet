//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblMainMenu_Master
    {
        public int MainMenuId { get; set; }
        public string MainMenuName { get; set; }
        public string ExternalURL { get; set; }
        public string ExternalURLTarget { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public int StatusId { get; set; }
        public int DisplayOrder { get; set; }
        public string IconCSS { get; set; }
    }
}
