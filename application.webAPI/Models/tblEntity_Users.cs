//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace application.webAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblEntity_Users
    {
        public long EntityUserId { get; set; }
        public long EntityId { get; set; }
        public string UserName { get; set; }
        public int Pin { get; set; }
        public string Password { get; set; }
        public string PersonName { get; set; }
        public string EmailId { get; set; }
        public string Mobile { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int StateId { get; set; }
        public int CityId { get; set; }
        public int CountryId { get; set; }
        public string PinCode { get; set; }
        public string DOB { get; set; }
        public int DepartmentId { get; set; }
        public int DesignationId { get; set; }
        public int RoleId { get; set; }
        public System.DateTime CreatedDateTime { get; set; }
        public long CreatedBy { get; set; }
        public int StatusId { get; set; }
        public string StatusRemark { get; set; }
        public bool IsEmailVerified { get; set; }
        public Nullable<System.DateTime> EmailVerifiedDateTime { get; set; }
        public Nullable<bool> LeadUser { get; set; }
        public string IMEINo { get; set; }
        public Nullable<bool> FingerPrintLoginEnabled { get; set; }
        public Nullable<bool> ActivateMobileOTP { get; set; }
    }
}
