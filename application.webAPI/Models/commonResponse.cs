﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.Models
{
    public class commonResponse
    {
        public int statusCode { get; set; }
        public object data { get; set; }
        public object message { get; set; }
    }

    public class commonResponseForDataTable
    {
        public long Draw { get; set; }
        public long RecordsFiltered { get; set; }
        public long RecordsTotal { get; set; }
        public dynamic Data { get; set; }
    }
}