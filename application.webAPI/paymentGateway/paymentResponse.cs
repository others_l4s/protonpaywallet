﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.paymentGateway
{
    public class paymentResponse
    {
        public string TransactionId { get; set; }

        public string Description { get; set; }
        public string ResMessage { get; set; }
        public string GatewayName { get; set; }
        public int Amount { get; set; }

        public string Order_Id { get; set; }
        public string ClientCode { get; set; }
        public string IsSuccess { get; set; }
    }

    public class paymentGatewayRequest
    {
        public int Amount { get; set; }
        public string TransactionId { get; set; }
        public string ClientCode { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
    }

    public class paymentGatewayResponse
    {
        public string Url { get; set; }
        public paymentGatewayStatusResponse status { get; set; }
    }

    public class paymentGatewayStatusResponse
    {
        public string returnMessage { get; set; }
        public int code { get; set; }
    }

    public class paymentResponseMsg
    {
        public string returnMessage { get; set; }
        public int code { get; set; }
    }
}