﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Security.Claims;
using application.webAPI.Models;
using application.webAPI.viewModel;
using RestSharp;
using System.Configuration;
using Newtonsoft.Json;
using System.Data.Entity;
using application.webAPI.bankit.dmt.response;
using System.Net;
using System.IO;
using application.webAPI.instantpay.dmt.response;
using System.Web.Http.Description;
using System.Data.Entity.Core;

namespace application.webAPI
{
    public static class helper
    {
        public const int defaultActiveStatusId = 1;
        public const int defaultCompany = 1;
        public const string entityTypeCompany = "1";
        public const string entityLowBalance = "Your Wallet Balance is too Low. Kindly topup your balance.";
        public const string invalidModuleAccess = "Invalid Module Access!";
        public const string incompleteKYC = "Incomplete KYC Status. Please upload your KYC to continue your Transactions";
        public enum entityTypes
        {
            company = 1,
            agent = 2,
            distributor = 3,
            masterDistributor = 4
        }

        public enum transactionTypes
        {
            Booking = 1,
            Commission = 2,
            Refund = 3,
            WalletTopUp = 4,
            ServiceCharge = 5,
            GST = 6,
            TDS = 7,
            CompanyGST = 8,
            MoneyTransfer = 9,
            BankCharges = 10,
            OtherCharges = 11
        }

        public static string BankITStatus(string statusCode)
        {
            return (statusCode == "00" ? "Success" : statusCode == "02" ? "Failure" : "Pending");
        }

        public static string protectString(string value2Protect)
        {
            byte[] byteString = Encoding.UTF8.GetBytes(value2Protect);
            byte[] protectedString = MachineKey.Protect(byteString, "ProtectString");
            return Convert.ToBase64String(protectedString);
        }

        public static string unProtectString(string value2UnProtect)
        {
            byte[] byteString = Convert.FromBase64String(value2UnProtect);
            byte[] protectedString = MachineKey.Unprotect(byteString, "ProtectString");
            return Encoding.UTF8.GetString(protectedString);
        }

        public static int getCompanyIdByAdminLoginId(int adminLoginId)
        {
            applicationDbEntities db = new applicationDbEntities();
            return db.tblAdminLogin.SingleOrDefault(a => a.AdminLoginId == adminLoginId).CompanyId;
        }

        public static IRestResponse WebAPIClient_via_RestSharp_4_BankIT_DMT(string apiName, RestSharp.Method methodName, object objClass, BankITDMTCredentialsVM clsCredentials, Boolean onLiveMode)
        {
            string VendorAPIURL = (onLiveMode ? clsCredentials.LiveMode.URL : clsCredentials.TestMode.URL);
            Uri vendorAPIURL = new Uri(VendorAPIURL);
            string hostName = vendorAPIURL.Host;
            var client = new RestClient(VendorAPIURL + "/" + apiName);
            var request = new RestRequest(methodName);
            request.AddHeader("Host", hostName);
            request.AddHeader("Authorization", "Basic " + (onLiveMode ? clsCredentials.LiveMode.Token : clsCredentials.TestMode.Token));
            request.AddHeader("Connection", "keep-alive");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", " application/json");
            request.AddHeader("Accept-Encoding", "gzip, deflate");
            request.AddHeader("cache-control", "no-cache");

            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse WebAPIClient_via_RestSharp_4_INSTANT_DMT(string apiName, RestSharp.Method methodName, object objClass, string token = "")
        {
            var client = new RestClient(ConfigurationManager.AppSettings["webApiInstanyPayDMTUrl"] + apiName);
            var request = new RestRequest(methodName);
            client.Timeout = -1;
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            if (objClass != null)
                request.AddParameter("application/json", request.AddJsonBody(objClass), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public static IRestResponse WebAPIClient_Via_RestSharp_4_InstantPay_UtilityPayments(string apiName, RestSharp.Method methodName, object objClass)
        {
            string VendorAPIURL = "https://www.instantpay.in/ws/api";
            Uri vendorAPIURL = new Uri(VendorAPIURL);
            var client = new RestClient(VendorAPIURL + "/" + apiName);
            var request = new RestRequest(methodName);

            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static IRestResponse WebAPIClient_Via_RestSharp_4_MRoboticRecharge(string vendorAPIURL, string apiParameter, RestSharp.Method methodName, object objClass)
        {
            var client = new RestClient(vendorAPIURL + "?" + apiParameter);
            var request = new RestRequest(methodName);

            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public static IRestResponse WebAPIClient_Via_RestSharp_4_MyRobotic_UtilityPayments(string apiName, RestSharp.Method methodName, object objClass)
        {
            string VendorAPIURL = "https://myrobotic.in/recharge/api";
            Uri vendorAPIURL = new Uri(VendorAPIURL);
            var client = new RestClient(VendorAPIURL + "?" + apiName);
            var request = new RestRequest(methodName);

            if (objClass != null)
                request.AddJsonBody(objClass);
            IRestResponse response = client.Execute(request);
            return response;
        }

        public static BankITDMTCredentialsVM getBankITDMT_Credentials(string fileName)
        {
            string credentialsFilePath = System.Web.HttpContext.Current.Request.MapPath("~/crFiles/" + fileName);
            string credentialsFile = System.IO.File.ReadAllText(credentialsFilePath);
            return JsonConvert.DeserializeObject<BankITDMTCredentialsVM>(credentialsFile);
        }

        public static payoutDirect_Response DMT2PayoutTestResponse(string fileName)
        {
            string credentialsFilePath = System.Web.HttpContext.Current.Request.MapPath("~/crFiles/" + fileName);
            string credentialsFile = System.IO.File.ReadAllText(credentialsFilePath);
            return JsonConvert.DeserializeObject<payoutDirect_Response>(credentialsFile);
        }

        public static MoneyTransferResponseVM readJsonFile(string fileName)
        {
            string credentialsFilePath = System.Web.HttpContext.Current.Request.MapPath("~/crFiles/" + fileName);
            string credentialsFile = System.IO.File.ReadAllText(credentialsFilePath);
            //credentialsFile = credentialsFile.Replace("\\","");
            return JsonConvert.DeserializeObject<MoneyTransferResponseVM>(credentialsFile);
        }

        public static transfer_response readJsonFile_InstantPay(string fileName)
        {
            string credentialsFilePath = System.Web.HttpContext.Current.Request.MapPath("~/crFiles/" + fileName);
            string credentialsFile = System.IO.File.ReadAllText(credentialsFilePath);
            //credentialsFile = credentialsFile.Replace("\\","");
            return JsonConvert.DeserializeObject<transfer_response>(credentialsFile);
        }

        public static String GetRandomPasswordUsingGUID(int length)
        {
            //Get the GUID
            string guidResult = System.Guid.NewGuid().ToString();

            //Remove the hyphens
            guidResult = guidResult.Replace("-", string.Empty);

            //Make sure length is valid
            if (length <= 0 || length > guidResult.Length)
            {
                throw new ArgumentException("Length must be between 1 and " + guidResult.Length);
            }

            //Return the first length bytes
            return guidResult.Substring(0, length);
        }

        public static string getRandomNumericDateTimeValue()
        {
            return DateTime.Today.Year.ToString() + "" + DateTime.Today.Month.ToString() + "" + DateTime.Today.Day.ToString() + "" + DateTime.Now.Hour.ToString() + "" + DateTime.Now.Minute.ToString() + "" + DateTime.Now.Second.ToString() + "" + DateTime.Now.Millisecond.ToString();
        }
        public static string bankITDMT_ClientRefId()
        {
            return "10084" + DateTime.Now.ToString("ddMMyyyyHHmmssffff");
        }

        public static DateTime getIndianTime()
        {
            TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            return indianTime;
        }

        public static string getDistMDEntityIdByAgentId(long entityId)
        {
            applicationDbEntities db = new applicationDbEntities();
            long distEntityId = db.tblEntity_Master.Where(x => x.EntityId == entityId).SingleOrDefault().ParentEntityId;
            long mdEntityId = db.tblEntity_Master.Where(x => x.EntityId == distEntityId).SingleOrDefault().ParentEntityId;
            return distEntityId + "|" + mdEntityId;
        }

        public static string InstantPayDMT_agentId()
        {
            return DateTime.Now.ToString("ddMMyyyyHHmmss");
        }

        #region Company Virtual Wallet & Account
        public static string updateCompanyWalletBalance(Int32 companyId, decimal amount)
        {
            try
            {
                tblCompany_Wallet objCompanyWallet = new tblCompany_Wallet();
                objCompanyWallet.CompanyId = companyId;
                objCompanyWallet.WalletBalance = amount;
                applicationDbEntities db = new applicationDbEntities();
                var companyWalletInDb = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId);
                if (companyWalletInDb == null)
                {
                    db.tblCompany_Wallet.Add(objCompanyWallet);
                }
                else
                {
                    decimal walletActualBalance = companyWalletInDb.WalletBalance;
                    companyWalletInDb.WalletBalance = walletActualBalance + amount;
                    db.Entry(companyWalletInDb).State = EntityState.Modified;
                    db.Entry(companyWalletInDb).Property(x => x.CompanyId).IsModified = false;
                }
                db.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public static decimal getCompanyWalletBalance(Int32 companyId)
        {
            applicationDbEntities db = new applicationDbEntities();
            var companyWalletInDb = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId);
            if (companyWalletInDb == null)
            {
                return 0;
            }
            else
            {
                return companyWalletInDb.WalletBalance;
            }
        }

        public static Boolean hasEnoughCompanyWalletBalance(Int32 companyId, decimal amount)
        {
            applicationDbEntities db = new applicationDbEntities();
            var companyWalletInDb = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId);
            if (companyWalletInDb == null)
            {
                return false;
            }
            else
            {
                decimal companyWalletBalance = companyWalletInDb.WalletBalance;
                if (companyWalletBalance <= 0)
                    return false;
                if (companyWalletBalance > amount)
                    return true;
                else
                    return false;
            }
        }

        public static string transactCompanyAccount(Int32 companyId, long entityId, decimal amount, string accountTransactionId, string description, int transactionTypeId, Boolean isCredit, long adminLoginId)
        {
            applicationDbEntities db = new applicationDbEntities();
            try
            {
                tblCompany_Account objCompanyAccount = new tblCompany_Account();
                objCompanyAccount.EntityId = entityId;
                objCompanyAccount.CompanyId = companyId;
                objCompanyAccount.Amount = amount;
                objCompanyAccount.RunningBalance = db.tblCompany_Wallet.SingleOrDefault(x => x.CompanyId == companyId).WalletBalance;
                objCompanyAccount.AccountTransactionId = accountTransactionId;
                objCompanyAccount.Description = description;
                objCompanyAccount.TransactionTypeId = transactionTypeId;
                objCompanyAccount.IsCredit = isCredit;
                objCompanyAccount.CreatedDateTime = helper.getIndianTime();
                objCompanyAccount.AdminUserId = adminLoginId;
                db.tblCompany_Account.Add(objCompanyAccount);
                db.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        #endregion

        #region Entity Wallet

        public static string updateWalletBalance(long entityId, decimal amount)
        {
            try
            {
                tblEntity_Wallet objEntityWallet = new tblEntity_Wallet();
                objEntityWallet.EntityId = entityId;
                objEntityWallet.WalletBalance = amount;
                applicationDbEntities db = new applicationDbEntities();
                var entityWalletInDb = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == objEntityWallet.EntityId);
                if (entityWalletInDb == null)
                {
                    db.tblEntity_Wallet.Add(objEntityWallet);
                }
                else
                {
                    decimal walletActualBalance = entityWalletInDb.WalletBalance;
                    entityWalletInDb.WalletBalance = walletActualBalance + amount;
                    db.Entry(entityWalletInDb).State = EntityState.Modified;
                    db.Entry(entityWalletInDb).Property(x => x.EntityId).IsModified = false;
                }
                db.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public static decimal getWalletBalance(long entityId)
        {
            applicationDbEntities db = new applicationDbEntities();
            var entityWalletInDb = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
            if (entityWalletInDb == null)
            {
                return 0;
            }
            else
            {
                return entityWalletInDb.WalletBalance;
            }
        }

        public static Boolean hasEnoughBalance(long entityId)
        {
            applicationDbEntities db = new applicationDbEntities();
            var entityWalletInDb = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId);
            if (entityWalletInDb == null)
            {
                return false;
            }
            else
            {
                decimal walletBalance = entityWalletInDb.WalletBalance;
                if (walletBalance <= 0)
                    return false;
                int minimumBalance2Maintain = (int)db.tblEntity_Master.Where(x => x.EntityId == entityId).Take(1).SingleOrDefault().MinimumBalanceMaintain;
                if (walletBalance <= minimumBalance2Maintain)
                    return false;
                else
                    return true;
            }
        }

        public static Boolean checkWalletBalance(long entityId, decimal transactAmount)
        {
            applicationDbEntities db = new applicationDbEntities();
            Nullable<int> minimumBalance2Maintain = 0;
            var minimumBalanceMaintainInDb = db.tblEntity_Master.SingleOrDefault(x => x.EntityId == entityId);
            if (minimumBalanceMaintainInDb != null)
                minimumBalance2Maintain = (minimumBalanceMaintainInDb.MinimumBalanceMaintain == null ? 0 : minimumBalanceMaintainInDb.MinimumBalanceMaintain);
            decimal walletBalance = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId).WalletBalance;
            if (walletBalance <= 0)
                return false;

            walletBalance = walletBalance - (int)minimumBalance2Maintain;
            if (transactAmount <= walletBalance)
            {
                if ((walletBalance - transactAmount) <= minimumBalance2Maintain)
                    return false;
                else
                    return true;
            }
            else
            { return false; }
        }


        #endregion

        #region Entity Account

        public static string transactEntityAccount(long entityId, decimal amount, string clientTransactionId, string description, int transactionTypeId, Boolean isCredit, int productId, long entityUserId, decimal bookingAmount)
        {
            applicationDbEntities db = new applicationDbEntities();
            try
            {
                tblEntity_Account objEntityAccount = new tblEntity_Account();
                objEntityAccount.EntityId = entityId;
                objEntityAccount.Amount = amount;
                objEntityAccount.RunningBalance = db.tblEntity_Wallet.SingleOrDefault(x => x.EntityId == entityId).WalletBalance;
                objEntityAccount.ClientTransactionId = clientTransactionId;
                objEntityAccount.AccountTransactionId = helper.GetRandomPasswordUsingGUID(10).ToUpper() + helper.getIndianTime().ToString("ddMMyyyyMMHHss");
                objEntityAccount.Description = description;
                objEntityAccount.TransactionTypeId = transactionTypeId;
                objEntityAccount.IsCredit = isCredit;
                objEntityAccount.ProductId = productId;
                objEntityAccount.CreatedDateTime = helper.getIndianTime();
                objEntityAccount.EntityUserId = entityUserId;
                objEntityAccount.BookingAmount = bookingAmount;
                db.tblEntity_Account.Add(objEntityAccount);
                db.SaveChanges();
                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        #endregion

        #region SMS Integration
        public static void sendSMS_Transactional(string mobileNo, string smsTemplate, long entityUserId)
        {
            string responseString = "";
            applicationDbEntities db = new applicationDbEntities();
            try
            {
                //Prepare you post parameters change sms vendor from 26th Dec 2019
                StringBuilder sbPostData = new StringBuilder();
                sbPostData.AppendFormat("authentic-key={0}", ConfigurationManager.AppSettings["SMSToken"]);
                sbPostData.AppendFormat("&senderid={0}", "MISGRT");
                sbPostData.AppendFormat("&number={0}", mobileNo);
                sbPostData.AppendFormat("&message={0}", smsTemplate);
                sbPostData.AppendFormat("&route={0}", "7");

                string sendSMSUri = ConfigurationManager.AppSettings["SMSURL"] + sbPostData;
                HttpWebRequest WRE = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                HttpWebResponse WRS = (HttpWebResponse)WRE.GetResponse();
                if (WRS.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader SR = new StreamReader(WRS.GetResponseStream());
                    responseString = SR.ReadToEnd();
                }
            }
            catch (SystemException ex)
            {
                responseString = "Error:" + ex.Message.ToString();
            }
            tblSMS_Log objSMSLog = new tblSMS_Log();
            objSMSLog.MobileNo = mobileNo;
            objSMSLog.SMSMessage = smsTemplate;
            objSMSLog.SendDateTime = getIndianTime();
            objSMSLog.SMSSendResponse = responseString;
            objSMSLog.EntityUserId = entityUserId;
            db.tblSMS_Log.Add(objSMSLog);
            db.SaveChanges();
        }

        public static void sendSMS_OTP(string mobileNo, string smsTemplate, long entityUserId)
        {
            string responseString = "";
            applicationDbEntities db = new applicationDbEntities();
            try
            {
                //Prepare you post parameters change sms vendor from 26th Dec 2019
                StringBuilder sbPostData = new StringBuilder();
                sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["SMSToken"]);
                sbPostData.AppendFormat("&template_id={0}", "5f098693d6fc05675273a7ab");
                sbPostData.AppendFormat("&mobile={0}", "91" + mobileNo);
                sbPostData.AppendFormat("&extra_param={0}", smsTemplate);
                sbPostData.AppendFormat("&otp={0}", smsTemplate);
                sbPostData.AppendFormat("&otp_length={0}", 6);

                string sendSMSUri = ConfigurationManager.AppSettings["SMSURL"] + sbPostData;
                HttpWebRequest WRE = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                HttpWebResponse WRS = (HttpWebResponse)WRE.GetResponse();
                if (WRS.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader SR = new StreamReader(WRS.GetResponseStream());
                    responseString = SR.ReadToEnd();
                }
            }
            catch (SystemException ex)
            {
                responseString = "Error:" + ex.Message.ToString();
            }
            tblSMS_Log objSMSLog = new tblSMS_Log();
            objSMSLog.MobileNo = mobileNo;
            objSMSLog.SMSMessage = smsTemplate;
            objSMSLog.SendDateTime = getIndianTime();
            objSMSLog.SMSSendResponse = responseString;
            objSMSLog.EntityUserId = entityUserId;
            db.tblSMS_Log.Add(objSMSLog);
            db.SaveChanges();
        }

        public static void sendForgot_Password(string mobileNo, string value1, string value2)
        {
            var client = new RestClient("https://api.msg91.com/api/v5/flow/");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("authkey", ConfigurationManager.AppSettings["SMSToken"]);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("application/json", "{\"flow_id\": \"5f310775d6fc053aff61ebc0\",\"sender\": \"VGAMEZ\",\"recipients\": [\"mobiles\": \"91" + mobileNo + "\",\"VAR1\": \"" + value1 + "\",\"VAR2\": \"" + value2 + "\"}]}",
                ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
        }

        public static string verifySMS_OTP(string mobileNo, string OTP)
        {
            string responseString = "";
            try
            {
                //Prepare you post parameters change sms vendor from 26th Dec 2019
                StringBuilder sbPostData = new StringBuilder();
                sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["SMSToken"]);
                sbPostData.AppendFormat("&mobile={0}", "91" + mobileNo);
                sbPostData.AppendFormat("&otp={0}", OTP);

                string sendSMSUri = "https://api.msg91.com/api/v5/otp/verify?" + sbPostData;
                HttpWebRequest WRE = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                HttpWebResponse WRS = (HttpWebResponse)WRE.GetResponse();
                if (WRS.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader SR = new StreamReader(WRS.GetResponseStream());
                    responseString = SR.ReadToEnd();
                }
            }
            catch (SystemException ex)
            {
                responseString = "Error:" + ex.Message.ToString();
            }
            return responseString;
        }

        public static string resendSMS_OTP(string mobileNo)
        {
            string responseString = "";
            try
            {
                //Prepare you post parameters change sms vendor from 26th Dec 2019
                StringBuilder sbPostData = new StringBuilder();
                sbPostData.AppendFormat("authkey={0}", ConfigurationManager.AppSettings["SMSToken"]);
                sbPostData.AppendFormat("&mobile={0}", "91" + mobileNo);

                string sendSMSUri = "https://api.msg91.com/api/v5/otp/retry?" + sbPostData;
                HttpWebRequest WRE = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                HttpWebResponse WRS = (HttpWebResponse)WRE.GetResponse();
                if (WRS.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader SR = new StreamReader(WRS.GetResponseStream());
                    responseString = SR.ReadToEnd();
                }
            }
            catch (SystemException ex)
            {
                responseString = "Error:" + ex.Message.ToString();
            }
            return responseString;
        }

        public static string loginOTPTemplate(string OTP)
        {
            return OTP + " is your OTP to login your protonpay account. Never share your OTP or account details to anyone.";
        }

        public static string forgotPasswordOTPTemplate(string OTP)
        {
            return OTP + " is your OTP to verify your account on protonpay account. Never share your OTP or account details to anyone.";
        }

        public static string forgotPasswordSendPasswordDetailTemplate(string username, string password)
        {
            return "username:" + username + " and password:" + password + " to access your account on protonpay account. Never share your account details to anyone and keep changing password from my profile section.";
        }
        #endregion

        public static string generateRandom8DigitNumber()
        {
            Random rnd = new Random();
            int myRandomNo = rnd.Next(100000, 999999);
            return myRandomNo.ToString();
        }

        public static Boolean processEntityCommissionNTaxes_BANKITDMT(decimal transactionAmount, long entityId, long entityUserId, string clientRefId, int companyId, int productId, Boolean walletCredit)
        {
            string walletUpdateStatus = "";
            string transactStatus = "";
            applicationDbEntities db = new applicationDbEntities();
            tblProduct_Master objProductMaster = db.tblProduct_Master.SingleOrDefault(x => x.ProductId == productId);
            List<tblEntity_Slab_Commission> objEntitySlabCommissionList = db.tblEntity_Slab_Commission.Where(x => x.ProductId == productId && x.CompanyId == companyId).ToList();
            if (objProductMaster != null)
            {
                if (walletCredit)
                {
                    walletUpdateStatus = helper.updateWalletBalance(entityId, transactionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, transactionAmount, clientRefId, "Money Transfer Amount from " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.MoneyTransfer), true, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }
                else
                {
                    walletUpdateStatus = helper.updateWalletBalance(entityId, -transactionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, -transactionAmount, clientRefId, "Money Transfer Amount from " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.MoneyTransfer), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }
                int entityTypeId = 0;
                decimal agentCommission = 0;
                decimal distcommission = 0;
                decimal mdCommission = 0;
                int serviceChargeType = (objProductMaster.ServiceChargeType == null ? 0 : (int)objProductMaster.ServiceChargeType);
                decimal serviceChargeValue = (objProductMaster.ServiceChargeValue == null ? 0 : (decimal)objProductMaster.ServiceChargeValue);
                Boolean serviceChargeApplicable = objProductMaster.ServiceChargeApplicable;
                decimal gstOnServiceCharge = (objProductMaster.GSTOnServiceCharge == null ? 0 : (decimal)objProductMaster.GSTOnServiceCharge);
                decimal TDSOnServiceCharge = (objProductMaster.TDSOnServiceCharge == null ? 0 : (decimal)objProductMaster.TDSOnServiceCharge);
                decimal otherChargesByBank = (objProductMaster.OtherChargesByVendor == null ? 0 : (decimal)objProductMaster.OtherChargesByVendor);
                decimal TDSOnCommission = objProductMaster.TDSRateOnCommission;
                // Getting default and custom commission type and agentcommission for Agent/Retailer changes done on 19 06 2020
                entityTypeId = Convert.ToInt32(helper.entityTypes.agent);
                int defaultCommissionType = 0;
                if (objEntitySlabCommissionList.Count > 0)
                {
                    defaultCommissionType = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId).Take(1).SingleOrDefault().CommissionTypeId;
                    var agentCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount) && x.EntityId == entityId && x.IsCustom == true).Take(1).SingleOrDefault();
                    if (agentCommissionInDb == null)
                        agentCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount)).Take(1).SingleOrDefault();
                    if (agentCommissionInDb != null)
                        agentCommission = agentCommissionInDb.CommissionValue;
                }

                string distMDEntityIds = helper.getDistMDEntityIdByAgentId(entityId);
                long distEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[0]);
                long mdEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[1]);
                // Getting Commission for Distributor (default and custom) changes done on 19 06 2020
                entityTypeId = Convert.ToInt32(helper.entityTypes.distributor);
                if (objEntitySlabCommissionList.Count > 0)
                {
                    var distCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount) && x.EntityId == distEntityId && x.IsCustom == true).Take(1).SingleOrDefault();
                    if (distCommissionInDb == null)
                        distCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount)).Take(1).SingleOrDefault();
                    if (distCommissionInDb != null)
                        distcommission = distCommissionInDb.CommissionValue;
                }

                //Getting commission for Master Distributor (default and custom) changes done on 19 06 2020
                entityTypeId = Convert.ToInt32(helper.entityTypes.masterDistributor);
                if (objEntitySlabCommissionList.Count > 0)
                {
                    var mdCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount) && x.EntityId == mdEntityId && x.IsCustom == true).Take(1).SingleOrDefault();
                    if (mdCommissionInDb == null)
                        mdCommissionInDb = objEntitySlabCommissionList.Where(x => x.EntityTypeId == entityTypeId && (transactionAmount >= x.FromAmount && transactionAmount <= x.ToAmount)).Take(1).SingleOrDefault();
                    if (mdCommissionInDb != null)
                        mdCommission = mdCommissionInDb.CommissionValue;
                }

                int commissionOnServiceCharge = (objProductMaster.CommissionOn == "Service Charge Amount" ? 1 : 0);
                mdCommission = (mdCommission < 0 ? 0 : mdCommission);
                distcommission = (distcommission < 0 ? 0 : distcommission);

                decimal bankCharges = 0;
                decimal serviceChargeAmount = 0;
                decimal agentCommissionAmount = 0;
                decimal remainingServiceChargeAmount = 0;
                decimal distributorCommissionAmount = 0;
                decimal MDCommissionAmount = 0;

                // Calculating Service Charge Amount         
                #region Agent Taxes and Commission
                if (serviceChargeApplicable)
                {
                    serviceChargeAmount = (serviceChargeType == 0 ? transactionAmount * serviceChargeValue : serviceChargeValue);
                    serviceChargeAmount = (serviceChargeAmount < 10 ? 10 : serviceChargeAmount);
                    walletUpdateStatus = helper.updateWalletBalance(entityId, -serviceChargeAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, -serviceChargeAmount, clientRefId, "Service Charge for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.ServiceCharge), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }

                if (otherChargesByBank > 0)
                {
                    bankCharges = otherChargesByBank;
                    transactStatus = helper.transactEntityAccount(entityId, -bankCharges, clientRefId, "Bank Charges for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.BankCharges), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }

                if (serviceChargeApplicable && agentCommission > 0)
                {
                    agentCommissionAmount = (defaultCommissionType == 1 ? serviceChargeAmount * agentCommission : agentCommission);
                    remainingServiceChargeAmount = serviceChargeAmount - agentCommissionAmount;
                    walletUpdateStatus = helper.updateWalletBalance(entityId, agentCommissionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(entityId, agentCommissionAmount, clientRefId, "Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;

                    // Calculating GST Amount
                    serviceChargeAmount = remainingServiceChargeAmount - bankCharges;
                    decimal companyGSTAmount = serviceChargeAmount * gstOnServiceCharge;
                    transactStatus = helper.transactEntityAccount(entityId, -companyGSTAmount, clientRefId, "Company GST Charges for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.GST), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                    serviceChargeAmount = serviceChargeAmount - companyGSTAmount;
                }
                else
                {
                    if (agentCommission > 0)
                    {
                        agentCommissionAmount = (defaultCommissionType == 1 ? transactionAmount * agentCommission : agentCommission);
                        walletUpdateStatus = helper.updateWalletBalance(entityId, agentCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                        transactStatus = helper.transactEntityAccount(entityId, agentCommissionAmount, clientRefId, "Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                        if (transactStatus != "success")
                            return false;
                    }

                }

                if (agentCommission > 0)
                {
                    decimal tdsOnAgentCommissionAmount = agentCommissionAmount * TDSOnCommission;
                    transactStatus = helper.transactEntityAccount(entityId, -tdsOnAgentCommissionAmount, clientRefId, "TDS On Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }


                #endregion

                // Distributor Commission & TDS
                if (serviceChargeApplicable && distcommission > 0)
                {
                    distributorCommissionAmount = (defaultCommissionType == 1 ? remainingServiceChargeAmount * distcommission : distcommission);
                    walletUpdateStatus = helper.updateWalletBalance(distEntityId, distributorCommissionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                }
                else
                {
                    if (distcommission > 0)
                    {
                        distributorCommissionAmount = (defaultCommissionType == 1 ? transactionAmount * distcommission : distcommission);
                        walletUpdateStatus = helper.updateWalletBalance(distEntityId, distributorCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                    }
                }

                if (distcommission > 0)
                {
                    transactStatus = helper.transactEntityAccount(distEntityId, distributorCommissionAmount, clientRefId, "Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;

                    decimal distributorTDSOnCommissionAmount = distributorCommissionAmount * TDSOnCommission;
                    walletUpdateStatus = helper.updateWalletBalance(distEntityId, -distributorTDSOnCommissionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(distEntityId, -distributorTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }


                //Master Distributor Commission & TDS

                if (serviceChargeApplicable && mdCommission > 0)
                {
                    MDCommissionAmount = (defaultCommissionType == 1 ? remainingServiceChargeAmount * mdCommission : mdCommission);
                    walletUpdateStatus = helper.updateWalletBalance(mdEntityId, MDCommissionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                }
                else
                {
                    if (mdCommission > 0)
                    {
                        MDCommissionAmount = (defaultCommissionType == 1 ? transactionAmount * mdCommission : mdCommission);
                        walletUpdateStatus = helper.updateWalletBalance(mdEntityId, MDCommissionAmount);
                        if (walletUpdateStatus != "success")
                            return false;
                    }
                }

                if (mdCommission > 0)
                {
                    transactStatus = helper.transactEntityAccount(mdEntityId, MDCommissionAmount, clientRefId, "Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.Commission), true, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;

                    decimal MDTDSOnCommissionAmount = MDCommissionAmount * TDSOnCommission;
                    walletUpdateStatus = helper.updateWalletBalance(mdEntityId, -MDTDSOnCommissionAmount);
                    if (walletUpdateStatus != "success")
                        return false;
                    transactStatus = helper.transactEntityAccount(mdEntityId, -MDTDSOnCommissionAmount, clientRefId, "TDS on Commission for " + objProductMaster.ProductCode, Convert.ToInt32(helper.transactionTypes.TDS), false, productId, entityUserId, transactionAmount);
                    if (transactStatus != "success")
                        return false;
                }
            }
            return true;
        }

        public static string getClientIPAddress()
        {
            string ip = "";
            try
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(ip))
                {
                    ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
            }
            catch (Exception ex)
            {
            }
            return ip;
        }

        public static string getInstantPayToken()
        {
            return ConfigurationManager.AppSettings["InstantPayToken"].ToString();
        }

        public static void process_MoneyTransferRefund(string clientTransactionId, long entityId, long entityUserId, int productId, decimal bookingAmount)
        {
            applicationDbEntities db = new applicationDbEntities();
            List<tblEntity_Account> objEntityAccountList = db.tblEntity_Account.Where(x => x.ClientTransactionId == clientTransactionId).ToList();
            if (objEntityAccountList.Count > 0)
            {
                tblProduct_Master objProductMaster = db.tblProduct_Master.SingleOrDefault(x => x.ProductId == productId);
                string distMDEntityIds = helper.getDistMDEntityIdByAgentId(entityId);
                long distEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[0]);
                long mdEntityId = Convert.ToInt64(distMDEntityIds.Split('|')[1]);
                decimal agentTotalAmount = objEntityAccountList.Where(x => x.EntityId == entityId && x.ClientTransactionId == clientTransactionId).Sum(x => x.Amount); // Agent Total Amount
                decimal distTotalAmount = objEntityAccountList.Where(x => x.EntityId == distEntityId && x.ClientTransactionId == clientTransactionId).Sum(x => x.Amount); // Dist Total Amount
                decimal mdTotalAmount = objEntityAccountList.Where(x => x.EntityId == mdEntityId && x.ClientTransactionId == clientTransactionId).Sum(x => x.Amount); // MD Total Amount
                if (agentTotalAmount < 0)
                {
                    agentTotalAmount = Math.Abs(agentTotalAmount);
                    helper.updateWalletBalance(entityId, agentTotalAmount);
                    helper.transactEntityAccount(entityId, agentTotalAmount, clientTransactionId, "Amount Refund for " + objProductMaster.ProductCode + " OrderId : " + clientTransactionId, Convert.ToInt32(helper.transactionTypes.Refund), true, productId, entityUserId, bookingAmount);
                }
                if (distTotalAmount > 0)
                {
                    helper.updateWalletBalance(distEntityId, -distTotalAmount);
                    helper.transactEntityAccount(distEntityId, -distTotalAmount, clientTransactionId, "Amount Debited for " + objProductMaster.ProductCode + " OrderId : " + clientTransactionId + " Refund", Convert.ToInt32(helper.transactionTypes.Refund), false, productId, entityUserId, bookingAmount);
                }
                if (mdTotalAmount > 0)
                {
                    helper.updateWalletBalance(mdEntityId, -mdTotalAmount);
                    helper.transactEntityAccount(mdEntityId, -mdTotalAmount, clientTransactionId, "Amount Debited for " + objProductMaster.ProductCode + " OrderId : " + clientTransactionId + " Refund", Convert.ToInt32(helper.transactionTypes.Refund), false, productId, entityUserId, bookingAmount);
                }
            }
        }

        public static string RemoveSpecialChars(string str)
        {
            // Create  a string array and add the special characters you want to remove
            string[] chars = new string[] { ",", ".", "/", "!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", "_", "(", ")", ":", "|", "[", "]", "-" };
            //Iterate the number of times based on the String array length.
            for (int i = 0; i < chars.Length; i++)
            {
                if (str.Contains(chars[i]))
                {
                    str = str.Replace(chars[i], "");
                }
            }
            return str;
        }

        public static Boolean checkProductPartialStopStatus(Int32 productId)
        {
            applicationDbEntities db = new applicationDbEntities();
            return db.tblProduct_Master.Where(x => x.ProductId == productId).SingleOrDefault().PartialStop;
        }

        public static Boolean checkProductFullStopStatus(Int32 productId)
        {
            applicationDbEntities db = new applicationDbEntities();
            return db.tblProduct_Master.Where(x => x.ProductId == productId).SingleOrDefault().FullStop;
        }

        public static userIdentity getClaimsIdentityInfo(ClaimsIdentity claimsIdentity)
        {
            userIdentity objIdentityUsers = new userIdentity();
            try
            {
                var identityEntityType = claimsIdentity.Claims.Where(c => c.Type == "entityType").Single();
                objIdentityUsers.isCompany = Convert.ToInt16(identityEntityType.Value) != helper.defaultCompany ? false : true;
                objIdentityUsers.companyId = Convert.ToInt32(claimsIdentity.Claims.Where(c => c.Type == "companyId").Single().Value);
                objIdentityUsers.adminLoginId = (objIdentityUsers.isCompany ? Convert.ToInt32(claimsIdentity.Claims.Where(c => c.Type == "adminLoginId").Single().Value) : 0);
                objIdentityUsers.entityTypeId = Convert.ToInt16(identityEntityType.Value);
                objIdentityUsers.entityId = (objIdentityUsers.isCompany ? 0 : Convert.ToInt64(claimsIdentity.Claims.Where(c => c.Type == "entityId").Single().Value));
                objIdentityUsers.entityUserId = (objIdentityUsers.isCompany ? 0 : Convert.ToInt64(claimsIdentity.Claims.Where(c => c.Type == "entityUserId").Single().Value));
            }
            catch (Exception ex)
            {

            }
            return objIdentityUsers;
        }

        public static Boolean checkEntityKYC(long entityId)
        {
            try
            {
                applicationDbEntities db = new applicationDbEntities();
                var entityKYCStatus = db.tblEntity_KYC.Where(x => x.EntityId == entityId).SingleOrDefault();
                if (entityKYCStatus != null)
                {
                    return (entityKYCStatus.IsKYCStatus == null ? false : entityKYCStatus.IsKYCStatus.Value);
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}