﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.aeps.response
{
    public class Get_Token_Response
    {
        public string errorMsg { get; set; }
        public string errorCode { get; set; }
        public Get_Token_Data_resp data { get; set; }
    }
}