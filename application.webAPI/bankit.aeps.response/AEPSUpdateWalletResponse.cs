﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace application.webAPI.bankit.aeps.response
{
    public class AEPSUpdateWalletResponse
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}