﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcNew.Models
{
    public class mvcStatusModel
    {
        public byte StatusId { get; set; }
        public string StatusName { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; }
        public bool isSelected { get; set; }
    }
}