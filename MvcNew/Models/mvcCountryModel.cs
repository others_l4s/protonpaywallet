﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcNew.Models
{
    public class mvcCountryModel
    {
        public int CountryId { get; set; }
        [Required(ErrorMessage ="Please Enter Coutry Name")]
        [Display(Name ="Name")]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Invalid")]
        public string CountryName { get; set; }
        [Display(Name = "Code")]
        [StringLength(5, MinimumLength = 2, ErrorMessage = "Invalid")]
        [Required(ErrorMessage = "Please Enter Coutry Code")]
        public string CountryCode { get; set; }
        public DateTime CreatedDateTime { get; set; } = System.DateTime.Now;
        [Display(Name = "Status")]        
        public byte StatusId { get; set; }=0;
        public bool Status { get; set; }

    }
}