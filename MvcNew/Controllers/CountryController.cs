﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using MvcNew.Models;
namespace MvcNew.Controllers
{
    public class CountryController : Controller
    {
        // GET: Country
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<mvcCountryModel> CountryList;
            HttpResponseMessage response = GlobalVariable.WebApiClient.GetAsync("tblCountry_Master").Result;
            CountryList = response.Content.ReadAsAsync<IEnumerable<mvcCountryModel>>().Result;
            return View(CountryList);
        }
        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new mvcCountryModel());
            }
            else
            {
                HttpResponseMessage response = GlobalVariable.WebApiClient.GetAsync("tblCountry_Master/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcCountryModel>().Result);
            }
        }
        [HttpPost]
        public ActionResult AddOrEdit(mvcCountryModel country)
        {
            if (country.CountryId == 0)
            {
                HttpResponseMessage response = GlobalVariable.WebApiClient.PostAsJsonAsync("tblCountry_Master", country).Result;
                TempData["SuccessMessage"] = "Saved Successfully..";
            }
            else
            {
                HttpResponseMessage response = GlobalVariable.WebApiClient.PutAsJsonAsync("tblCountry_Master/" + country.CountryId, country).Result;
                TempData["SuccessMessage"] = "Update Successfully..";
            }
            return RedirectToAction("Index");
        }
       
        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariable.WebApiClient.DeleteAsync("tblCountry_Master/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Delete Successfully..";
            return RedirectToAction("Index");
        }
    }
}