﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using MvcNew.Models;

namespace MvcNew.Controllers
{
    public class StatusController : Controller
    {
        // GET: Status
        [HttpGet]
        public ActionResult Index()
        {
            IEnumerable<mvcStatusModel> statusList;
            HttpResponseMessage response = GlobalVariable.WebApiClient.GetAsync("tblStatus_Master").Result;
            statusList = response.Content.ReadAsAsync<IEnumerable<mvcStatusModel>>().Result;
            return View(statusList);
        }
    }
}