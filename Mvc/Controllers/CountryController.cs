﻿using Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
namespace Mvc.Controllers
{
    public class CountryController : Controller
    {
        // GET: Country
        public ActionResult Index()
        {
            IEnumerable<mvcCountryModel> CountryList;
            HttpResponseMessage response = GlobalVariable.WebApiClient.GetAsync("tblCountry_Master").Result;
            CountryList = response.Content.ReadAsAsync<IEnumerable<mvcCountryModel>>().Result;
            return View(CountryList);
        }
        [HttpGet]
        public ActionResult AddOrEdit(int id = 0)
        {
            return View(new mvcCountryModel());
        }
        [HttpPost]
        public ActionResult AddOrEdit(mvcCountryModel country)
        {
            HttpResponseMessage response = GlobalVariable.WebApiClient.PostAsJsonAsync("tblCountry_Master", country).Result;
            TempData["SuccessMessage"] = "Saved Successfully..";
            return RedirectToAction("Index");
        }
    }
}