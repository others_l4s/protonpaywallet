﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using application.WebAPI;

namespace Mvc.Models
{
    public partial class mvcCountryModel
    {
        
        public int CountryId { get; set; }
        [Required(ErrorMessage ="This Field Is Required")]
        public string CountryName { get; set; }
        [Required(ErrorMessage = "This Field Is Required")]
        [Display(Name ="Code")]
        public string CountryCode { get; set; }
        public Nullable<System.DateTime> CreatedDateTime { get; set; } = System.DateTime.Now;
        public Nullable<byte> StatusId { get; set; } = 0;

    }

}